﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.RegMCC
{
    public class clsReg
    {
        //private string _sActiveKey = "";
        internal string _sUserCode = "";
        internal string _SerialNumber = "";
        internal string _sRegisteredName = "";

        public clsReg(string SerialNumber)
        {
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                var attribute = (AssemblyNtbSoftAttribute)assembly.GetCustomAttributes(typeof(AssemblyNtbSoftAttribute), true)[0];
                _sRegisteredName = attribute.Value;
                this._SerialNumber = SerialNumber;
                _sUserCode = _sRegisteredName + _SerialNumber;
            }
            catch (Exception ex)
            {
            }
        }

        private bool IsKeyOK(string sActiveKey, string sUserCode)
        {
            string text = Microsoft.VisualBasic.Strings.Left(sActiveKey, 9);
            clsCMD5 cMD = new clsCMD5();
            clsCMD5 arg_20_0 = cMD;
            string text2 = text + sUserCode;
            string str = arg_20_0.MD5(ref text2);
            string text3 = text;
            int num = 1;
            checked
            {
                int arg_72_0;
                int num3;
                do
                {
                    int num2 = Microsoft.VisualBasic.CompilerServices.Conversions.ToInteger("&H" + Microsoft.VisualBasic.Strings.Mid(str, num * 2 - 1, 2)) % 32;
                    text3 += Microsoft.VisualBasic.Strings.Mid("0123456789ABCDEFGHJKLMNPQRTUVWXY", num2 + 1, 1);
                    num++;
                    arg_72_0 = num;
                    num3 = 16;
                }
                while (arg_72_0 <= num3);
                return Microsoft.VisualBasic.CompilerServices.Operators.CompareString(text3, sActiveKey, false) == 0;
            }
        }


        /// <summary>
        /// Lấy chuỗi UserCode
        /// </summary>
        /// <returns></returns>
        public string GenerateKey()
        {
            clsCMD5 cMD = new clsCMD5();
            clsCMD5 arg_1A_0 = cMD;
            string text = Strings.UCase(this._sRegisteredName) + _SerialNumber;
            string str = arg_1A_0.MD5(ref text);
            string text2 = "";
            int num = 1;
            checked
            {
                int arg_6F_0;
                int num3;
                do
                {
                    int num2 = Microsoft.VisualBasic.CompilerServices.Conversions.ToInteger("&H" + Strings.Mid(str, num * 2 - 1, 2)) % 32;
                    text2 += Strings.Mid("0123456789ABCDEFGHJKLMNPQRTUVWXY", num2 + 1, 1);
                    num++;
                    arg_6F_0 = num;
                    num3 = 16;
                }
                while (arg_6F_0 <= num3);
                StringBuilder s = new StringBuilder(text2.Insert(8, _SerialNumber));
                Encrypt(ref s, _sRegisteredName);
                //return ReverseString(text2).Insert(8, ReverseString(_SerialNumber));
                return s.ToString();
            }
        }
 

        //VigenereEncrypt
        /// <summary>
        /// ma chuoi
        /// </summary>
        /// <param name="s"></param>
        /// <param name="key"></param>
        private void Encrypt(ref StringBuilder s, string key)
        {
            for (int i = 0; i < s.Length; i++) s[i] = Char.ToUpper(s[i]);
            key = key.ToUpper();
            int j = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (Char.IsLetter(s[i]))
                {
                    s[i] = (char)(s[i] + key[j] - 'A');
                    if (s[i] > 'Z') s[i] = (char)(s[i] - 'Z' + 'A' - 1);
                }
                j = j + 1 == key.Length ? 0 : j + 1;
            }
        }

        public bool IsKeyOK(string ActiveKey)
        {
            return this.IsKeyOK(ActiveKey, _sUserCode);
        }


    }
}
