﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.RegMCC
{
    internal class clsCMD5
    {
        private const int BITS_TO_A_BYTE = 8;

        private const int BYTES_TO_A_WORD = 4;

        private const int BITS_TO_A_WORD = 32;

        private int[] m_lOnBits;

        private int[] m_l2Power;

        private void Class_Initialize_Renamed()
        {
            this.m_lOnBits[0] = 1;
            this.m_lOnBits[1] = 3;
            this.m_lOnBits[2] = 7;
            this.m_lOnBits[3] = 15;
            this.m_lOnBits[4] = 31;
            this.m_lOnBits[5] = 63;
            this.m_lOnBits[6] = 127;
            this.m_lOnBits[7] = 255;
            this.m_lOnBits[8] = 511;
            this.m_lOnBits[9] = 1023;
            this.m_lOnBits[10] = 2047;
            this.m_lOnBits[11] = 4095;
            this.m_lOnBits[12] = 8191;
            this.m_lOnBits[13] = 16383;
            this.m_lOnBits[14] = 32767;
            this.m_lOnBits[15] = 65535;
            this.m_lOnBits[16] = 131071;
            this.m_lOnBits[17] = 262143;
            this.m_lOnBits[18] = 524287;
            this.m_lOnBits[19] = 1048575;
            this.m_lOnBits[20] = 2097151;
            this.m_lOnBits[21] = 4194303;
            this.m_lOnBits[22] = 8388607;
            this.m_lOnBits[23] = 16777215;
            this.m_lOnBits[24] = 33554431;
            this.m_lOnBits[25] = 67108863;
            this.m_lOnBits[26] = 134217727;
            this.m_lOnBits[27] = 268435455;
            this.m_lOnBits[28] = 536870911;
            this.m_lOnBits[29] = 1073741823;
            this.m_lOnBits[30] = 2147483647;
            this.m_l2Power[0] = 1;
            this.m_l2Power[1] = 2;
            this.m_l2Power[2] = 4;
            this.m_l2Power[3] = 8;
            this.m_l2Power[4] = 16;
            this.m_l2Power[5] = 32;
            this.m_l2Power[6] = 64;
            this.m_l2Power[7] = 128;
            this.m_l2Power[8] = 256;
            this.m_l2Power[9] = 512;
            this.m_l2Power[10] = 1024;
            this.m_l2Power[11] = 2048;
            this.m_l2Power[12] = 4096;
            this.m_l2Power[13] = 8192;
            this.m_l2Power[14] = 16384;
            this.m_l2Power[15] = 32768;
            this.m_l2Power[16] = 65536;
            this.m_l2Power[17] = 131072;
            this.m_l2Power[18] = 262144;
            this.m_l2Power[19] = 524288;
            this.m_l2Power[20] = 1048576;
            this.m_l2Power[21] = 2097152;
            this.m_l2Power[22] = 4194304;
            this.m_l2Power[23] = 8388608;
            this.m_l2Power[24] = 16777216;
            this.m_l2Power[25] = 33554432;
            this.m_l2Power[26] = 67108864;
            this.m_l2Power[27] = 134217728;
            this.m_l2Power[28] = 268435456;
            this.m_l2Power[29] = 536870912;
            this.m_l2Power[30] = 1073741824;
        }

        public clsCMD5()
        {
            this.m_lOnBits = new int[31];
            this.m_l2Power = new int[31];
            this.Class_Initialize_Renamed();
        }

        private int LShift(int lValue, short iShiftBits)
        {
            bool flag = iShiftBits == 0;
            checked
            {
                int result;
                if (flag)
                {
                    result = lValue;
                }
                else
                {
                    flag = (iShiftBits == 31);
                    if (flag)
                    {
                        bool flag2 = (lValue & 1) != 0;
                        if (flag2)
                        {
                            result = -2147483648;
                        }
                        else
                        {
                            result = 0;
                        }
                    }
                    else
                    {
                        bool flag2 = iShiftBits < 0 | iShiftBits > 31;
                        if (flag2)
                        {
                            Information.Err().Raise(6, null, null, null, null);
                        }
                        flag2 = ((lValue & this.m_l2Power[(int)(31 - iShiftBits)]) != 0);
                        if (flag2)
                        {
                            result = (int)((long)Math.Round(unchecked((double)(lValue & this.m_lOnBits[(int)(checked(31 - (iShiftBits + 1)))]) * (double)this.m_l2Power[(int)iShiftBits])) | -2147483648L);
                        }
                        else
                        {
                            result = (int)Math.Round(unchecked((double)(lValue & this.m_lOnBits[(int)(checked(31 - iShiftBits))]) * (double)this.m_l2Power[(int)iShiftBits]));
                        }
                    }
                }
                return result;
            }
        }

        private int RShift(int lValue, short iShiftBits)
        {
            bool flag = iShiftBits == 0;
            int num;
            if (flag)
            {
                num = lValue;
            }
            else
            {
                flag = (iShiftBits == 31);
                if (flag)
                {
                    bool flag2 = (lValue & -2147483648) != 0;
                    if (flag2)
                    {
                        num = 1;
                    }
                    else
                    {
                        num = 0;
                    }
                }
                else
                {
                    bool flag2 = iShiftBits < 0 | iShiftBits > 31;
                    if (flag2)
                    {
                        Information.Err().Raise(6, null, null, null, null);
                    }
                    num = (lValue & 2147483646) / this.m_l2Power[(int)iShiftBits];
                    flag2 = ((lValue & -2147483648) != 0);
                    if (flag2)
                    {
                        num |= 1073741824 / this.m_l2Power[(int)(checked(iShiftBits - 1))];
                    }
                }
            }
            return num;
        }

        private int RShiftSigned(int lValue, short iShiftBits)
        {
            bool flag = iShiftBits == 0;
            int result;
            if (flag)
            {
                result = lValue;
            }
            else
            {
                flag = (iShiftBits == 31);
                if (flag)
                {
                    bool flag2 = (lValue & -2147483648) != 0;
                    if (flag2)
                    {
                        result = -1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                else
                {
                    bool flag2 = iShiftBits < 0 | iShiftBits > 31;
                    if (flag2)
                    {
                        Information.Err().Raise(6, null, null, null, null);
                    }
                    result = checked((int)Math.Round(Conversion.Int((double)lValue / (double)this.m_l2Power[(int)iShiftBits])));
                }
            }
            return result;
        }

        private int RotateLeft(int lValue, short iShiftBits)
        {
            return this.LShift(lValue, iShiftBits) | this.RShift(lValue, checked((short)(32 - iShiftBits)));
        }

        private int AddUnsigned(int lX, int lY)
        {
            int num = lX & -2147483648;
            int num2 = lY & -2147483648;
            int num3 = lX & 1073741824;
            int num4 = lY & 1073741824;
            int num5 = checked((int)Math.Round(unchecked((double)(lX & 1073741823) + (double)(lY & 1073741823))));
            bool flag = (num3 & num4) != 0;
            if (flag)
            {
                num5 = (num5 ^ -2147483648 ^ num ^ num2);
            }
            else
            {
                flag = ((num3 | num4) != 0);
                if (flag)
                {
                    bool flag2 = (num5 & 1073741824) != 0;
                    if (flag2)
                    {
                        num5 = (num5 ^ -1073741824 ^ num ^ num2);
                    }
                    else
                    {
                        num5 = (num5 ^ 1073741824 ^ num ^ num2);
                    }
                }
                else
                {
                    num5 = (num5 ^ num ^ num2);
                }
            }
            return num5;
        }

        private int F(int x, int y, int z)
        {
            return (x & y) | (~x & z);
        }

        private int G(int x, int y, int z)
        {
            return (x & z) | (y & ~z);
        }

        private int H(int x, int y, int z)
        {
            return x ^ y ^ z;
        }

        private int I(int x, int y, int z)
        {
            return y ^ (x | ~z);
        }

        private void FF(ref int a, int b, int c, int d, int x, int s, int ac)
        {
            a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.F(b, c, d), x), ac));
            a = this.RotateLeft(a, checked((short)s));
            a = this.AddUnsigned(a, b);
        }

        private void GG(ref int a, int b, int c, int d, int x, int s, int ac)
        {
            a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.G(b, c, d), x), ac));
            a = this.RotateLeft(a, checked((short)s));
            a = this.AddUnsigned(a, b);
        }

        private void HH(ref int a, int b, int c, int d, int x, int s, int ac)
        {
            a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.H(b, c, d), x), ac));
            a = this.RotateLeft(a, checked((short)s));
            a = this.AddUnsigned(a, b);
        }

        private void II(ref int a, int b, int c, int d, int x, int s, int ac)
        {
            a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.I(b, c, d), x), ac));
            a = this.RotateLeft(a, checked((short)s));
            a = this.AddUnsigned(a, b);
        }

        private int[] ConvertToWordArray(ref string sMessage)
        {
            int num = Strings.Len(sMessage);
            checked
            {
                int num2 = ((num + 8) / 64 + 1) * 16;
                int[] array = new int[num2 - 1 + 1];
                int i;
                int num3;
                int num4;
                for (i = 0; i < num; i++)
                {
                    num3 = i / 4;
                    num4 = i % 4 * 8;
                    int lValue = Strings.Asc(Strings.Mid(sMessage, i + 1, 1));
                    array[num3] |= this.LShift(lValue, (short)num4);
                }
                num3 = i / 4;
                num4 = i % 4 * 8;
                array[num3] |= this.LShift(128, (short)num4);
                array[num2 - 2] = this.LShift(num, 3);
                array[num2 - 1] = this.RShift(num, 29);
                return array;

                //return (int[]) Support.CopyArray(array);
            }
        }

        private string WordToHex(int lValue)
        {
            int num = 0;
            checked
            {
                string text = string.Empty;
                int arg_3E_0;
                int num2;
                do
                {
                    int number = this.RShift(lValue, (short)(num * 8)) & this.m_lOnBits[7];
                    text += Strings.Right("0" + Conversion.Hex(number), 2);
                    num++;
                    arg_3E_0 = num;
                    num2 = 3;
                }
                while (arg_3E_0 <= num2);
                return text;
            }
        }

        public string MD5(ref string sMessage)
        {
            int[] array = this.ConvertToWordArray(ref sMessage);
            int num = 1732584193;
            int num2 = -271733879;
            int num3 = -1732584194;
            int num4 = 271733878;
            int arg_2F_0 = 0;
            int num5 = Information.UBound(array, 1);
            int num6 = arg_2F_0;
            checked
            {
                while (true)
                {
                    int arg_768_0 = num6;
                    int num7 = num5;
                    if (arg_768_0 > num7)
                    {
                        break;
                    }
                    int lY = num;
                    int lY2 = num2;
                    int lY3 = num3;
                    int lY4 = num4;
                    this.FF(ref num, num2, num3, num4, array[num6 + 0], 7, -680876936);
                    this.FF(ref num4, num, num2, num3, array[num6 + 1], 12, -389564586);
                    this.FF(ref num3, num4, num, num2, array[num6 + 2], 17, 606105819);
                    this.FF(ref num2, num3, num4, num, array[num6 + 3], 22, -1044525330);
                    this.FF(ref num, num2, num3, num4, array[num6 + 4], 7, -176418897);
                    this.FF(ref num4, num, num2, num3, array[num6 + 5], 12, 1200080426);
                    this.FF(ref num3, num4, num, num2, array[num6 + 6], 17, -1473231341);
                    this.FF(ref num2, num3, num4, num, array[num6 + 7], 22, -45705983);
                    this.FF(ref num, num2, num3, num4, array[num6 + 8], 7, 1770035416);
                    this.FF(ref num4, num, num2, num3, array[num6 + 9], 12, -1958414417);
                    this.FF(ref num3, num4, num, num2, array[num6 + 10], 17, -42063);
                    this.FF(ref num2, num3, num4, num, array[num6 + 11], 22, -1990404162);
                    this.FF(ref num, num2, num3, num4, array[num6 + 12], 7, 1804603682);
                    this.FF(ref num4, num, num2, num3, array[num6 + 13], 12, -40341101);
                    this.FF(ref num3, num4, num, num2, array[num6 + 14], 17, -1502002290);
                    this.FF(ref num2, num3, num4, num, array[num6 + 15], 22, 1236535329);
                    this.GG(ref num, num2, num3, num4, array[num6 + 1], 5, -165796510);
                    this.GG(ref num4, num, num2, num3, array[num6 + 6], 9, -1069501632);
                    this.GG(ref num3, num4, num, num2, array[num6 + 11], 14, 643717713);
                    this.GG(ref num2, num3, num4, num, array[num6 + 0], 20, -373897302);
                    this.GG(ref num, num2, num3, num4, array[num6 + 5], 5, -701558691);
                    this.GG(ref num4, num, num2, num3, array[num6 + 10], 9, 38016083);
                    this.GG(ref num3, num4, num, num2, array[num6 + 15], 14, -660478335);
                    this.GG(ref num2, num3, num4, num, array[num6 + 4], 20, -405537848);
                    this.GG(ref num, num2, num3, num4, array[num6 + 9], 5, 568446438);
                    this.GG(ref num4, num, num2, num3, array[num6 + 14], 9, -1019803690);
                    this.GG(ref num3, num4, num, num2, array[num6 + 3], 14, -187363961);
                    this.GG(ref num2, num3, num4, num, array[num6 + 8], 20, 1163531501);
                    this.GG(ref num, num2, num3, num4, array[num6 + 13], 5, -1444681467);
                    this.GG(ref num4, num, num2, num3, array[num6 + 2], 9, -51403784);
                    this.GG(ref num3, num4, num, num2, array[num6 + 7], 14, 1735328473);
                    this.GG(ref num2, num3, num4, num, array[num6 + 12], 20, -1926607734);
                    this.HH(ref num, num2, num3, num4, array[num6 + 5], 4, -378558);
                    this.HH(ref num4, num, num2, num3, array[num6 + 8], 11, -2022574463);
                    this.HH(ref num3, num4, num, num2, array[num6 + 11], 16, 1839030562);
                    this.HH(ref num2, num3, num4, num, array[num6 + 14], 23, -35309556);
                    this.HH(ref num, num2, num3, num4, array[num6 + 1], 4, -1530992060);
                    this.HH(ref num4, num, num2, num3, array[num6 + 4], 11, 1272893353);
                    this.HH(ref num3, num4, num, num2, array[num6 + 7], 16, -155497632);
                    this.HH(ref num2, num3, num4, num, array[num6 + 10], 23, -1094730640);
                    this.HH(ref num, num2, num3, num4, array[num6 + 13], 4, 681279174);
                    this.HH(ref num4, num, num2, num3, array[num6 + 0], 11, -358537222);
                    this.HH(ref num3, num4, num, num2, array[num6 + 3], 16, -722521979);
                    this.HH(ref num2, num3, num4, num, array[num6 + 6], 23, 76029189);
                    this.HH(ref num, num2, num3, num4, array[num6 + 9], 4, -640364487);
                    this.HH(ref num4, num, num2, num3, array[num6 + 12], 11, -421815835);
                    this.HH(ref num3, num4, num, num2, array[num6 + 15], 16, 530742520);
                    this.HH(ref num2, num3, num4, num, array[num6 + 2], 23, -995338651);
                    this.II(ref num, num2, num3, num4, array[num6 + 0], 6, -198630844);
                    this.II(ref num4, num, num2, num3, array[num6 + 7], 10, 1126891415);
                    this.II(ref num3, num4, num, num2, array[num6 + 14], 15, -1416354905);
                    this.II(ref num2, num3, num4, num, array[num6 + 5], 21, -57434055);
                    this.II(ref num, num2, num3, num4, array[num6 + 12], 6, 1700485571);
                    this.II(ref num4, num, num2, num3, array[num6 + 3], 10, -1894986606);
                    this.II(ref num3, num4, num, num2, array[num6 + 10], 15, -1051523);
                    this.II(ref num2, num3, num4, num, array[num6 + 1], 21, -2054922799);
                    this.II(ref num, num2, num3, num4, array[num6 + 8], 6, 1873313359);
                    this.II(ref num4, num, num2, num3, array[num6 + 15], 10, -30611744);
                    this.II(ref num3, num4, num, num2, array[num6 + 6], 15, -1560198380);
                    this.II(ref num2, num3, num4, num, array[num6 + 13], 21, 1309151649);
                    this.II(ref num, num2, num3, num4, array[num6 + 4], 6, -145523070);
                    this.II(ref num4, num, num2, num3, array[num6 + 11], 10, -1120210379);
                    this.II(ref num3, num4, num, num2, array[num6 + 2], 15, 718787259);
                    this.II(ref num2, num3, num4, num, array[num6 + 9], 21, -343485551);
                    num = this.AddUnsigned(num, lY);
                    num2 = this.AddUnsigned(num2, lY2);
                    num3 = this.AddUnsigned(num3, lY3);
                    num4 = this.AddUnsigned(num4, lY4);
                    num6 += 16;
                }
                return Strings.LCase(this.WordToHex(num) + this.WordToHex(num2) + this.WordToHex(num3) + this.WordToHex(num4));
            }
        }
    }
}
