﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Libs
{
    public class clsDBConstant
    {
        public enum eDIEM_DUNG : byte
        {
            A = 3,
            B = 10,
            C = 21
        }

        public enum eLOAI_BO_PHAN : byte
        {
            May = 1,
            Cắt,
            Hoàn_tất,
            Khác
        }

        public enum eLUC_TAC_DUNG : byte
        {
            Luc_10N = 10,
            Luc_15N = 15,
            Luc_20N = 20
        }

        public enum eTHAO_TAC : byte
        {
            Thêm = 1,
            Xóa,
            Sửa,
            Duyệt,
            Khóa,
            Bỏ_khóa
        }

        public enum eTRANG_THAI : byte
        {
            Thử_nghiệm = 1,
            Đã_duyệt
        }

        protected const double cDonGiaChuan = 0.800000011920929;

        protected const double cTMUPhut = 0.00060000002849847078;

        public const string cTHAO_TAC_DAC_BIET = "SP";

        public const string cCHIEU_DAI_DUONG_MAY = "SE";

        public const string cTHAO_TAC_CAT = "C";

        public static double DON_GIA_CHUAN
        {
            get
            {
                //if (DBConstant.mHeThong.Bang_du_lieu().Rows.Count != 0)
                //{
                //    return DBConstant.mHeThong.DonGiaChuan;
                //}
                return 0.800000011920929;
            }
        }

        public static double TMU_PHUT
        {
            get
            {
                //if (DBConstant.mHeThong.Bang_du_lieu().Rows.Count != 0)
                //{
                //    return DBConstant.mHeThong.TMUPhut;
                //}
                return 0.00060000002849847078;
            }
        }

        public static string LayDienGiai(Enum pEnumValue)
        {
            string result;
            try
            {
                result = Enum.GetName(pEnumValue.GetType(), pEnumValue).Replace("_", " ");
            }
            catch (Exception ex)
            {
                string arg_25_0 = ex.Message;
                result = "";
            }
            return result;
        }

        public static byte LayGiaTri(Type pEnumType, string pDien_giai)
        {
            foreach (byte b in Enum.GetValues(pEnumType))
            {
                if (pDien_giai.Replace(" ", "_") == Enum.GetName(pEnumType, b))
                {
                    return b;
                }
            }
            return 0;
        }
    }
}
