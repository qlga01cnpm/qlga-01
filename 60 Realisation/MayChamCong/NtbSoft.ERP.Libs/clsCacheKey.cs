﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NtbSoft.ERP.Libs
{
    public class clsCacheKey
    {
        public const string LineXChangeCollection = "LineXChangeCollection";
        public const string LineCollection = "LineCollection";
        public const string SoundCtrlCollection = "SoundCtrlCollection";

        public const string StyleCheckingCollection = "StyleCheckingCollection";
    }
}
