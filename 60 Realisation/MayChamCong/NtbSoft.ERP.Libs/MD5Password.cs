﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace NtbSoft.ERP.Libs
{
    public class MD5Password
    {
        /// <summary>
        /// Mã hóa 1 chiều, nghĩa là bạn chỉ có thể mã hóa, ko thể giải mã.
        /// Kết quả của hash theo Md5 là 1 chuỗi 32 ký tự (32-bit)
        /// input: Chuỗi cần được mã hóa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        
        /// <summary>
        /// So sánh chuỗi nhập vào với một chuỗi đã đượng mã hóa bằng MD5 
        /// input: Chuỗi nhập vào. Md5Hash: là chuỗi đã được mã hóa bằng MD5
        /// Verify a hash against a string.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="Md5Hash"></param>
        /// <returns></returns>
        public static bool verifyMd5Hash(string input, string Md5Hash)
        {
            // Hash the input.
            string hashOfInput = getMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, Md5Hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// So sánh 2 chuỗi đã được mã hóa bằng Md5
        /// </summary>
        /// <param name="Md5HashA"></param>
        /// <param name="Md5HashB"></param>
        /// <returns></returns>
        public static bool CompareMd5(string Md5HashA, string Md5HashB)
        {
            if (Md5HashA.Equals(Md5HashB))
                return true;

            return false;
        }

        /// <summary>
        /// Validates a MD5 hash string.
        /// </summary>
        /// <param name="Md5Hash">The string to test.</param>
        /// <returns><c>true</c>, in case <paramref name="Md5Hash"/> is valid; otherwise <c>false</c>.</returns>
        public static bool IsValidMD5(string Md5Hash)
        {
            if (Md5Hash == null || Md5Hash.Length != 32) return false;
            foreach (var x in Md5Hash)
            {
                if ((x < '0' || x > '9') && (x < 'a' || x > 'f') && (x < 'A' || x > 'F'))
                {
                    return false;
                }
            }
            return true;
        }


    }
}
