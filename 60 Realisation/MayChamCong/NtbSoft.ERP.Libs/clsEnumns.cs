﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NtbSoft.ERP.Libs
{
    //(*-*) ReadSound: tin hieu doc am thanh. 
    //(*-*) Connected: Trang thai ket noi. 
    //(*-*) Disconnected: Trang thai ngat ket noi
    //(*-*) DisplaySCtrl: Thay doi du lieu Sound Control
    //(*-*) DisplayData: Thay doi du lieu LineName (khi nhan duoc tin hieu co thay doi Data tu man hinh LCD, thi gui du lieu thay doi den cac Client)
    public enum Controls
    {
        Insert, Update, Delete, SendData, SendTime, SendTimeWork, TestConnect, OK, True, SendCommand, ConfigSeri,
        False, Start, Finish, ReadSound, Connected, Disconnected, DisplaySCtrl, DisplayData, ChangeTimeWork, ChangeRate, Refresh,
        RefreshSP_TP, F_KeyBoard, IniClientPlaylist
    }

    public enum ELoading
    {
        IsCurrent, IsTotal, IsDetail
    }

    public enum ColumnName { None, Style, KH_Tong, TH_Tong, KH_Ngay, TH_Ngay, SP_Dat, SP_Loi, CreatedEnd, SL_Giao, SL_TH, NDSX_KH }

    [Serializable]
    public enum EColumnName
    {
        None, Style, KH_Tong, TH_Tong, KH_Ngay, TH_Ngay, SP_Dat, SP_Loi, CreatedEnd,
        SL_Giao, SL_TH, NDSX_KH, NDSX_TH, UnitPrice, NoWorker, HeSo, Description
    }
    [Serializable]
    public enum LcdCommand
    {
        /// <summary>
        /// -1
        /// </summary>
        UpdateAll = -1,
        /// <summary>
        /// 0
        /// </summary>
        None = 0, 
        /// <summary>
        /// 1
        /// </summary>
        ChangeData = 1, ChangeDateTime = 2, ChangeTimeWork = 3, ChangeLayout = 5, BackMainLayout = 6,
        SettingLCD = 7, CapBTP = 8, SetSerialNo = 10, Trainning = 11, SetTimeDelayLayout = 12, SetLabelCD = 13,
        Cmd404 = 404, CmdError = 405
    }

    public enum ETypeChange { None, AddNew, Edit, Delete, View }

    public enum ECacheManagerName
    {
        CacheData, CacheLineXChange, CacheSCtrlCollection
    }

    public enum DataState { None, AddNew, Edit, Delete, View }

    public class StepDefaults
    {
        /// <summary>
        /// "fd1a02eb-1eee-4ce8-af9f-021af05ea5cb"
        /// </summary>
        public const string RA_CHUYEN = "fd1a02eb-1eee-4ce8-af9f-021af05ea5cb";
        
        /// <summary>
        /// 31468586-c0d2-4c4a-a025-f7978b0d8ab3
        /// </summary>
        public const string QC_KIEM = "31468586-c0d2-4c4a-a025-f7978b0d8ab3";
        public const string KCS = "31468586-c0d2-4c4a-a025-f7978b0d8ab6";
        
        /// <summary>
        /// "96a3cd99-0de5-460b-93dd-2c1a5eafab77"
        /// </summary>
        public const string BTP = "96a3cd99-0de5-460b-93dd-2c1a5eafab77";
    }

    public enum Sorts { ASC, DESC }

    public enum QueryAction
    {
        NONE,

        /// <summary>
        /// Cap nhat tu lcd (cac ban phim nhan san luong)
        /// </summary>
        U_DOING,

        /// <summary>
        /// Cap nhat tu phan mem
        /// </summary>
        U_SOFT,
        /// <summary>
        /// Cập nhật khi chỉnh sủa ngày cũ
        /// </summary>
        U_EDIT,

        GET,
        GETALL,
        GETPLAN,
        INSERT,
        UPDATE,
        DEL_BYDATE,
        CONFIG
    }

    public enum ApiAction
    {
        /// <summary>
        /// Read: Lấy dữ liệu về
        /// </summary>
        GET,
        /// <summary>
        /// Create: Tạo dữ liệu mới
        /// </summary>
        POST,
        /// <summary>
        /// Update: Cập nhật dữ liệu
        /// </summary>
        PUT,
        /// <summary>
        /// Xóa dữ liệu
        /// </summary>
        DELETE
    }

    [Flags]
    public enum Permissions
    {
        View = (1 << 0),
        Add = (1 << 1),
        Edit = (1 << 2),
        Delete = (1 << 3),
        Admin = (View | Add | Edit | Delete)
    }

    
}
