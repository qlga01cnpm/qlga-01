﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Libs
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyNtbSoftAttribute : Attribute
    {
        /// <summary>
        /// Get
        /// </summary>
        public string Value { get; private set; }

        public AssemblyNtbSoftAttribute() : this("") { }
        public AssemblyNtbSoftAttribute(string value) { Value = value; }
    }
}
