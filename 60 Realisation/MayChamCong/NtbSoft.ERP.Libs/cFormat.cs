﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Libs
{
    public class cFormat
    {
        /// <summary>
        /// Format TimeSpan hh:mm
        /// </summary>
        public const string Hour = @"hh\:mm";
        /// <summary>
        /// "HH:mm:ss"
        /// </summary>
        public const string Hour24 = "HH:mm:ss";
        /// <summary>
        /// "HH:mm:00"
        /// </summary>
        public const string HHmm0 = "HH:mm:00";

        /// <summary>
        /// "HH:mm"
        /// </summary>
        public const string HHmm = @"HH\:mm";

        /// <summary>
        /// "dd/MM/yyyy"
        /// </summary>
        public const string DMY = "dd/MM/yyyy";
        /// <summary>
        /// "MM/dd/yyyy"
        /// </summary>
        public const string MDY = "MM/dd/yyyy";
        /// <summary>
        /// "d MMM"
        /// </summary>
        public const string DMMM = "dd MMM";

        /// <summary>
        /// "yyyy/MM/dd"
        /// </summary>
        public const string YMD = "yyyy/MM/dd";
    }
}
