﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NtbSoft.ERP.Libs
{
    public class appPath
    {
        public static readonly string _folderPlaylist = System.Windows.Forms.Application.StartupPath + "\\Playlist";
        public static readonly string _folderTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates";
        public static readonly string _folderSoundTemps = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\SoundControls\\Sounds";
        public static readonly string _imgEdit = System.Windows.Forms.Application.StartupPath + "\\icons\\edit.ico";
        public static readonly string _imgDele = System.Windows.Forms.Application.StartupPath + "\\icons\\remove.ico";
        public static readonly string _imgBgImage = System.Windows.Forms.Application.StartupPath + "\\icons\\bg_form.png";
        public static readonly string _folderIcons = System.Windows.Forms.Application.StartupPath + "\\icons";
        public static readonly string _imgExec = System.Windows.Forms.Application.StartupPath + "\\icons\\exec.png";
        
    }
}
