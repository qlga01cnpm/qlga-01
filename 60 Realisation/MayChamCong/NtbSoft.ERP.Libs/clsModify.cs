﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace NtbSoft.ERP.Libs
{
    /// <summary>
    /// Change value sql data (Insert,Update,...)
    /// </summary>
    public class clsModify
    {
        public bool Insert(Hashtable columns, string TableName)
        {
            if (columns == null || columns.Count <= 0)
                throw new Exception("Insert-01: Columns is null or Columns has not value");
            //SqlConnection sqlcnn = null;
            try
            {
                string _Columns = string.Empty;
                string _Values = string.Empty;
                foreach (DictionaryEntry item in columns)
                {
                    _Columns += item.Key + ",";
                    _Values += string.Format("{0},", item.Value);
                }
                _Columns = _Columns.TrimEnd(new char[1] { ',' });
                _Values = _Values.TrimEnd(new char[1] { ',' });
                _Values = _Values.Insert(0, "(");
                _Values = _Values.Insert(_Values.Length, ")");

                using (SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlParameter[] sp = new SqlParameter[3];
                    sp[0] = new SqlParameter("@Columns", _Columns);
                    sp[1] = new SqlParameter("@TableName", TableName);
                    sp[2] = new SqlParameter("@Values", _Values);

                    int result = (int)SqlHelper.ExecuteNonQuery(_cnn, CommandType.StoredProcedure, "sp_Insert", sp);

                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Insert-01: " + ex.Message);
            }
            finally
            {
                //if (sqlcnn != null)
                //{
                //    sqlcnn.Close();
                //    sqlcnn.Dispose();
                //}
            }

        }

        public bool Update(Hashtable columns, string TableName, string whereClause)
        {
            if (string.IsNullOrEmpty(whereClause))
                throw new Exception("Update-01: whereClause is null");
            if (columns == null || columns.Count <= 0)
                throw new Exception("Update-01: Columns is null or Columns has not value");

            //SqlConnection _cnn = null;
            try
            {
                using (SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    string values = string.Empty;//= string.Format("Name='{0}',Per={1},Flo={2},Per1={3},Flo1={4},", name, per, flo, per1, flo1);
                    foreach (DictionaryEntry item in columns)
                    {
                        values += string.Format("{0}={1},", item.Key, item.Value);
                    }
                    values = values.TrimEnd(new char[1] { ',' });
                    string _whereClause = string.Format("Where {0}", whereClause);

                    SqlParameter[] sp = new SqlParameter[3];
                    sp[0] = new SqlParameter("@TableName", TableName);
                    sp[1] = new SqlParameter("@SetValues", values);
                    sp[2] = new SqlParameter("@WhereClause", _whereClause);

                    int result = SqlHelper.ExecuteNonQuery(_cnn, CommandType.StoredProcedure, "sp_Update", sp);
                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Update-01: {0}", ex.Message));
            }
            finally
            {
                //if (_cnn != null)
                //{
                //    _cnn.Close();
                //    _cnn.Dispose();
                //}
            }
        }

        public bool Update(Hashtable columns, string TableName)
        {
            if (columns == null || columns.Count <= 0)
                throw new Exception("Update-01: Columns is null or Columns has not value");

            //SqlConnection _cnn = null;
            try
            {
                using (SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    string values = string.Empty;//= string.Format("Name='{0}',Per={1},Flo={2},Per1={3},Flo1={4},", name, per, flo, per1, flo1);
                    foreach (DictionaryEntry item in columns)
                    {
                        values += string.Format("{0}={1},", item.Key, item.Value);
                    }
                    values = values.TrimEnd(new char[1] { ',' });
                    string _whereClause = string.Empty;

                    SqlParameter[] sp = new SqlParameter[3];
                    sp[0] = new SqlParameter("@TableName", TableName);
                    sp[1] = new SqlParameter("@SetValues", values);
                    sp[2] = new SqlParameter("@WhereClause", _whereClause);

                    int result = SqlHelper.ExecuteNonQuery(_cnn, CommandType.StoredProcedure, "sp_Update", sp);

                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Update-01: {0}", ex.Message));
            }
            finally
            {
                
            }
        }
    }
}
