﻿DECLARE
@FromDate datetime,
@ToDate datetime

SELECT	@FromDate = '2018-11-16', -- datetime
    @ToDate = '2018-11-30' -- datetime

	-- sp_CALC_KHONGCHAMCONG '08/2/2018','08/15/2018'
	
	declare @tblDate as table(Workingday datetime)
	declare @tblTemp01 as table(Workingday datetime,MaNV varchar(50))
	declare @tblNhanVienCheDo as table(MaNV varchar(50),MaCheDo int)

	--set @FromDate='07/07/2018' set @ToDate='07/07/2018'
	/*** Tính tổng số ngày ***/
	--set @TongSoNgay=DATEDIFF (day, @FromDate,@ToDate)
	/*** Lấy những nhân viên không chấm công ***/
	--insert into @tblNhanVien
	INSERT INTO @tblTemp01
	        ( Workingday, MaNV )
	SELECT A1.Workingday, A.Id 
	from dbo.fncNHANVIENDEP() A
	INNER JOIN dbo.MCC_TIMEINOUT_SESSION_REAL A1 on A1.MaNV=A.Id
	and CONVERT(date,A1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
	and ISNULL(A1.TimeIn,'')=''
	group by A.Id, A1.Workingday

	--Lay nhung ngay lam viec
	insert into @tblDate
	select A.Workingday
	from	MCC_TIMEINOUT_SESSION_REAL A
	where CONVERT(date,A.Workingday,121) 
		BETWEEN CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
	group by A.Workingday
	
	INSERT INTO @tblTemp01
	        ( Workingday, MaNV )
	SELECT	b.Workingday,
			a.MaNV 
	FROM	@tblDate b
			LEFT JOIN @tblTemp01 a
				ON	1 = 1			
	GROUP	BY a.MaNV,
			b.Workingday 
			
	
	INSERT INTO MCC_TIMEINOUT_SESSION_REAL(Workingday, MaNV)
	SELECT	a.Workingday,
			a.MaNV 
	FROM	@tblTemp01 a
			LEFT JOIN MCC_TIMEINOUT_SESSION_REAL b
				ON	b.MaNV = a.MaNV
				AND b.Workingday = a.Workingday
				AND	a.Workingday BETWEEN @FromDate AND @ToDate
	WHERE	b.Id IS NULL				
	AND		a.Workingday BETWEEN @FromDate AND @ToDate
	
	--Lay nhan vien che do
	--insert into @tblNhanVienCheDo
	--select MaNV,A.MaCheDo
	--from DIC_DANGKY_CHEDO A
	--where CONVERT(date,@FromDate,121) >= CONVERT(date,A.TuNgay,121) 
	--and CONVERT(date,@ToDate,121)<= CONVERT(date,A.ToiNgay,121)
	--group by MaNV,MaCheDo;
	
	--Kiểm tra chế độ
	
	--Merge into dbo.MCC_TIMEINOUT_SESSION_REAL A
	--	using @tblNhanVienCheDo B 
	--	on A.MaNV=B.MaNV and convert(date, A.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		
	--	when matched then
	--		update set LyDoVang=case when B.MaCheDo=1 then N'Nuôi con nhỏ'
	--										 when B.MaCheDo=2 then N'Mang thai 7-9 tháng' 
	--										 when B.MaCheDo=3 then N'Vị thành niên'
	--										 when B.MaCheDo=4 then N'Thai sản' 
	--										 else '' END;