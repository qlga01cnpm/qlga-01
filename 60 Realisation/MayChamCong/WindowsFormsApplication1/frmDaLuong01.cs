﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo
{
    public partial class frmDaLuong01 : Form
    {
        public frmDaLuong01()
        {
            InitializeComponent();
        }

        private void btXuLy_Click(object sender, EventArgs e)
        {
            this.label1.Text = "Đang xử lý...";
            _rnd = new Random();

            // cho phép tính 10 + 10
            // thằng bé A bắt đầu tính              
            //Run("A", SumAsync(10, 10));

            // thằng bé B bắt đầu tính 
            Run("B", SumAsync(10, 10));


        }


        private static Random _rnd;
        private async void Run(string name, Task<int> task)
        {
            var result = await task;
            label1.Text = name + " has got the answer =" + result;
            //Console.WriteLine(name + " has got the answer =" + result);
        }
        private Task<int> SumAsync(int a, int b)
        {
            return Task.Factory.StartNew(() => Sum(a, b));
        }

        private int Sum(int a, int b)
        {

            var calculatingTime = _rnd.Next(30000);
            Thread.Sleep(calculatingTime);

            return a + b;
        }
       

        private void SetValues(string value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(SetValues), new object[] { value });
                return;
            }
            this.label1.Text = value;
        }

        public void SetValue(int vlaue)
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);

            }
        }



        //=================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = "Đang xử lý...";
            Send2();
            Main2();
        }

        #region Cach 2
        private  void Send()
        {
            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> responseTask = client.GetAsync("http://google.com");
            responseTask.ContinueWith(x => Print(x));
        }

        private void Print(Task<HttpResponseMessage> httpTask)
        {
            if (httpTask.Result == null) return;
            Task<string> task = httpTask.Result.Content.ReadAsStringAsync();
            Task continuation = task.ContinueWith(t =>
            {
                //Console.WriteLine("Result: " + t.Result);
            });
        }

        private void Send2()
        {
            Task<int> responseTask = Task<int>.Run(() => GetValue());
            responseTask.ContinueWith(x => Print(x));
        }

        private void Send3()
        {
            Task<int> responseTask = Task<int>.Factory.StartNew(() => GetValue());
            responseTask.ContinueWith(x => Print(x));
        }


        private void Print(Task<int> MyTask)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<Task<int>>(Print), new object[] { MyTask });
                return;
            }
            label2.Text = string.Format("Result: {0}", MyTask.Result);
           
        }

        private void Print(string value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(Print), new object[] { value });
                return;
            }
            label2.Text = value;

        }

        private int GetValue()
        {
            int k = 0;
            for (int i = 0; i < 10000; i++)
            {
                Print(string.Format("Đang xử lý...{0}", i));
                Thread.Sleep(100);
                k++;
            }
            return 100;
        }


        #endregion


        public static void Main1()
        {
            var t = Task<int>.Factory.StartNew(() =>
            {
                // Just loop.
                int max = 1000000;
                int ctr = 0;
                for (ctr = 0; ctr <= max; ctr++)
                {
                    if (ctr == max / 2 && DateTime.Now.Hour <= 12)
                    {
                        ctr++;
                        break;
                    }
                }
                return ctr;
            });
            Console.WriteLine("Finished {0:N0} iterations.", t.Result);
        }


        public static void Main2()
        {
            var t = Task<int>.Run(() =>
            {
                // Just loop.
                int max = 1000000;
                int ctr = 0;
                for (ctr = 0; ctr <= max; ctr++)
                {
                    if (ctr == max / 2 && DateTime.Now.Hour <= 12)
                    {
                        ctr++;
                        break;
                    }
                }
                return ctr;
            });
            //Console.WriteLine("Finished {0:N0} iterations.", t.Result);
        }
    }


    ////class Worker
    ////{
    ////    private string name;
    ////    private int loop;

    ////    public Worker(string name, int loop)
    ////    {
    ////        this.name = name;
    ////        this.loop = loop;
    ////    }

    ////    public void DoWork(object value)
    ////    {
    ////        for (int i = 0; i < loop; i++)
    ////        {
    ////           // Console.WriteLine(name + " working " + value);
    ////            System.Diagnostics.Debug.WriteLine(name + " working " + value);
    ////            Thread.Sleep(50);
    ////        }

    ////    }

    ////}
}
