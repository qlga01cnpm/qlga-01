﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        TimeSpan TimeIn = new TimeSpan(6, 0, 0);//Bắt đầu giờ làm việc
        TimeSpan BreakOut = new TimeSpan(11, 45, 00);//Ra: giờ nghỉ giữa ca (nghỉ trưa)

        TimeSpan BreakIn = new TimeSpan(12, 45, 0);//Vào: Sau khi nghỉ giữa ca(sau khi nghỉ trưa vào làm việc)
        TimeSpan TimeOut = new TimeSpan(17, 30, 0);//Ra: Giờ kết thúc ca làm việc 


        TimeSpan StartIn = new TimeSpan(7, 0, 0);//Bắt đầu giờ vào hiểu ca
        TimeSpan EndIn = new TimeSpan(11, 45, 00);//Kết thúc giờ vảo hiểu ca 

        TimeSpan StartOut = new TimeSpan(12, 45, 0);//Bắt đầu giờ ra để hiểu ca
        TimeSpan EndOut = new TimeSpan(17, 30, 0);//Kết thúc giờ ra để hiểu ca

        TimeSpan BatDauTinhSom = new TimeSpan(15, 0, 0);//Bắt đầu tính về sớm
        TimeSpan KetThucTinhSom = new TimeSpan(17, 29, 0);//Kết thúc tính về sớm
        private void button1_Click(object sender, EventArgs e)
        {
            string MinGio = "", MaxGio = "";
            int _mSoPhut2LanCham = 30;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            //mArrayGio.Add(new TimeSpan(7, 0, 0));
            //mArrayGio.Add(new TimeSpan(11, 50, 0));

            mArrayGio.Add(new TimeSpan(7, 30, 0));
            //mArrayGio.Add(new TimeSpan(8, 00, 0));
            //mArrayGio.Add(new TimeSpan(8, 10, 0));

            //mArrayGio.Add(new TimeSpan(9, 00, 0));
            //mArrayGio.Add(new TimeSpan(9, 40, 0));

            //mArrayGio.Add(new TimeSpan(17, 00, 0));

            //mArrayGio.Add(new TimeSpan(17, 30, 0));
            //mArrayGio.Add(new TimeSpan(17, 00, 0));

            //TimeSpan VaoHieuCa = mArrayGio.FirstOrDefault(f => f <= StartIn);
            //TimeSpan KetThucVaoHieuCa = mArrayGio.FirstOrDefault(f => f >= EndIn);

            List<clsDangKyRaVaoInfo> mArrayGioVaoRa = new List<clsDangKyRaVaoInfo>();
            TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
            int i = 0;
            if (mArrayGio.Count == 1)
            {
                if (mArrayGio[0] >= StartIn && mArrayGio[0] <= EndIn)
                {//Giờ vào hiểu ca (bắt đầu ca làm việc)
                    mTempGioVao = mArrayGio[0];
                    //Kiểm tra đi muộn
                    if (mTempGioVao > TimeIn)
                    {
                        mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                        {
                            GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                            GioRa = "",
                            ThoiGian = (mTempGioVao-TimeIn).TotalMinutes.ToString(),
                            GhiChu = "Đi trễ"
                        });
                    }
                    mArrayGio.Remove(mTempGioVao);
                }
                else if (mArrayGio[0] >= StartOut && mArrayGio[0] <= EndOut)
                {//Giờ ra hiểu ca (sau khi giữa ca, giờ vào sau nghỉ trưa)
                    mTempGioRa = mArrayGio[0]; //Giờ chấm ra
                    if(mTempGioRa < BatDauTinhSom)
                    {
                        mTempGioVao = mArrayGio[0];
                        mTempGioRa = TimeSpan.Zero;
                        mArrayGio.Remove(mArrayGio[0]);
                    }
                    if (mTempGioRa >= BatDauTinhSom && mTempGioRa <= KetThucTinhSom)
                    {
                        mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                        {
                            GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                            GioVao = "",
                            ThoiGian = (TimeOut - mTempGioRa).TotalMinutes.ToString(),
                            GhiChu = "Về sớm"
                        });
                    }
                }
            }
            while (mArrayGio.Count > 0)
            {
                #region Vào (Kiểm tra xem có có giờ vào hay không)
                //Lấy vào lần đầu tiên
                if (mTempGioVao == TimeSpan.Zero)
                {
                    if (mArrayGio[i] >= StartIn && mArrayGio[i] <= EndIn)
                    {
                        mTempGioRa = mArrayGio[i];
                        MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                        mArrayGio.Remove(mTempGioVao);
                        i = 0;
                        continue;
                    }
                }
                #endregion

                #region Ra (Kiểm tra ra giờ chấm ra)
                //mTempGioVao = mArrayGio[i];
                
                #endregion


                i++;
                //////if (string.IsNullOrEmpty(MinGio))
                //////{//Vao
                //////    if (mArrayGio[0] <= BreakOut) { }
                //////    mTempGioVao = mArrayGio[0];
                //////    MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                //////    //Lấy giờ vào
                //////    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                //////    {
                //////        GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                //////        GioRa = "",
                //////        GhiChu = "Đi trễ"
                //////    });

                //////    mArrayGio.Remove(mTempGioVao);
                //////    continue;
                //////}
                //////else if (mTempGioVao == TimeSpan.Zero)
                //////{//Vao
                //////tt:
                //////    MaxGio = "";
                //////    if (mArrayGio.Count > 0)
                //////    {
                //////        if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
                //////        {
                //////            mArrayGio.Remove(mTempGioVao);
                //////            goto tt;
                //////        }
                //////        mTempGioVao = mArrayGio[0];
                //////        mArrayGio.Remove(mTempGioVao);
                //////    }

                //////    if (mTempGioRa != TimeSpan.Zero && mTempGioVao != TimeSpan.Zero)
                //////    {
                //////        mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                //////        {
                //////            GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                //////            GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                //////            ThoiGian = (mTempGioVao - mTempGioRa).TotalMinutes.ToString()
                //////        });
                //////    }
                //////}
                //#endregion

                //////#region Ra
                //////if (mArrayGio.Count > 0)
                //////{//Ra
                //////    MaxGio = "";
                //////    //Kiểm tra giờ ra này có hợp lệ hay không
                //////    if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
                //////    {
                //////        mTempGioRa = mArrayGio[0];
                //////        mTempGioVao = TimeSpan.Zero;
                //////        MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);

                //////        //Kiểm tra giờ ra nếu nhỏ hơn giờ cho phép sẽ tính về sớm
                //////    }
                //////    mArrayGio.Remove(mArrayGio[0]);
                //////}

                //////#endregion

                //////if (mArrayGio.Count == 0 && MaxGio != "")
                //////{//Kiểm tra về sớm
                //////    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                //////    {
                //////        GioRa = MaxGio,
                //////        GhiChu = "Về sớm"
                //////    });
                //////}
            }

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = mArrayGioVaoRa;
        }



        private void buttonOK_Click(object sender, EventArgs e)
        {
            string MinGio = "", MaxGio = "";
            int _mSoPhut2LanCham = 30;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            //mArrayGio.Add(new TimeSpan(7, 0, 0));
            //mArrayGio.Add(new TimeSpan(7, 10, 0));

            //mArrayGio.Add(new TimeSpan(7, 30, 0));
            //mArrayGio.Add(new TimeSpan(8, 00, 0));
            ////mArrayGio.Add(new TimeSpan(8, 10, 0));

            //mArrayGio.Add(new TimeSpan(9, 00, 0));
            //mArrayGio.Add(new TimeSpan(9, 40, 0));

            mArrayGio.Add(new TimeSpan(17, 00, 0));

            TimeSpan VaoCa = mArrayGio.Find(f => f <= BreakOut);
            TimeSpan KetThucCa = mArrayGio.Find(f => f > BreakOut);

            List<clsDangKyRaVaoInfo> mArrayGioVaoRa = new List<clsDangKyRaVaoInfo>();

            TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
            while (mArrayGio.Count > 0)
            {
                #region Vào
                //Lấy vào lần đầu tiên
                if (string.IsNullOrEmpty(MinGio))
                {//Vao
                    if (mArrayGio[0] <= BreakOut) { }
                    mTempGioVao = mArrayGio[0];
                    MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                    //Lấy giờ vào
                    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                    {
                        GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                        GioRa = "",
                        GhiChu = "Đi trễ"
                    });

                    mArrayGio.Remove(mTempGioVao);
                    continue;
                }
                else if (mTempGioVao == TimeSpan.Zero)
                {//Vao
                tt:
                    MaxGio = "";
                    if (mArrayGio.Count > 0)
                    {
                        if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
                        {
                            mArrayGio.Remove(mTempGioVao);
                            goto tt;
                        }
                        mTempGioVao = mArrayGio[0];
                        mArrayGio.Remove(mTempGioVao);
                    }

                    if (mTempGioRa != TimeSpan.Zero && mTempGioVao != TimeSpan.Zero)
                    {
                        mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                        {
                            GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                            GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                            ThoiGian = (mTempGioVao - mTempGioRa).TotalMinutes.ToString()
                        });
                    }
                }
                #endregion

                #region Ra
                if (mArrayGio.Count > 0)
                {//Ra
                    MaxGio = "";
                    //Kiểm tra giờ ra này có hợp lệ hay không
                    if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
                    {
                        mTempGioRa = mArrayGio[0];
                        mTempGioVao = TimeSpan.Zero;
                        MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);

                        //Kiểm tra giờ ra nếu nhỏ hơn giờ cho phép sẽ tính về sớm
                    }
                    mArrayGio.Remove(mArrayGio[0]);
                }

                #endregion

                if (mArrayGio.Count == 0 && MaxGio != "")
                {//Kiểm tra về sớm
                    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                    {
                        GioRa = MaxGio,
                        GhiChu = "Về sớm"
                    });
                }
            }

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = mArrayGioVaoRa;
        }
    }




    public class clsDangKyRaVaoInfo
    {
        public double ID { get; set; }
        //public TimeSpan GioVao { get; set; }
        //public TimeSpan GioRa { get; set; }
        public string MaNV { get; set; }
        public DateTime NgayDangKy { get; set; }
        public string GioVao { get; set; }
        public string GioRa { get; set; }
        public string ThoiGian { get; set; }
        public string GhiChu { get; set; }
    }
}
