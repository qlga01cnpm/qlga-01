﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo
{
    public partial class frmDaLuong02 : Form
    {
        CancellationTokenSource cts;  
        
        class CustomData
        {
            public long CreationTime;
            public int Name;
            public int ThreadNum;
        }

        public frmDaLuong02()
        {
            InitializeComponent();
            cts = new CancellationTokenSource();  
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            //base.OnClosing(e);
            cts.Cancel();
        }
        private void btXuLy_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            if (cts != null)
            {
                cts.Dispose(); cts = new CancellationTokenSource();
            }
            Send3();
        }

        private void Send3()
        {
            Task<int> responseTask = Task<int>.Factory.StartNew(() => GetValue1(cts.Token));
            responseTask.ContinueWith(x => Print(x));
        }

        private void Print(Task<int> MyTask)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<Task<int>>(Print), new object[] { MyTask });
                return;
            }
            label1.Text = string.Format("Result: {0}", MyTask.Result);

        }

        private int GetValue1(CancellationToken Cancellation)
        {
            Thread.Sleep(1000);
            int k = 0;
            for (int i = 0; i < 10000; i++)
            {
                k++;
                SetValue(string.Format("Get value 1 đang xử lý...{0}", i));
                Thread.Sleep(80);
               
                if (Cancellation.IsCancellationRequested) break;
            }
            return k;
        }
        private int GetValue2(CancellationToken Cancellation)
        {
            Thread.Sleep(1000);
            int k = 0;
            for (int i = 0; i < 10000; i++)
            {
               
                SetValue(string.Format("Get value 2 đang xử lý...{0}", i));
                Thread.Sleep(80);
                
                k++;
               Cancellation.ThrowIfCancellationRequested();
            }
            return k;
        }

        private void SetValue(string value)
        {
            if (this.label1.InvokeRequired)
            {
                this.Invoke(new Action<string>(SetValue), new object[] { value });
                return;
            }

            label1.Text = value;
            listView1.Items.Add(value);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cts.Cancel();
        }
    }
}
