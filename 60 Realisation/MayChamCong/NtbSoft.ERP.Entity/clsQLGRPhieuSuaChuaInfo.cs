﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsQLGRPhieuSuaChuaInfo : Entity.Interfaces.IPhieuSuaChuaInfo
    {
        public int HoSoId { get; set; }
        public DateTime NgaySuaChua { get; set; }
        public int PhuTungId { get; set; }
        public int TienCongId { get; set; }
        public int SoLuong { get; set; }
        public string NoiDung { get; set; }
    }
}
