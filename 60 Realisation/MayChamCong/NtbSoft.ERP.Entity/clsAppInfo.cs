﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsAppInfo:Interfaces.IAppInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Tel { get; set; }

        public string Fax { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public string MaSoThue { get; set; }

        public byte[] Logo { get; set; }

        public bool IsAutoBackup { get; set; }

        public DateTime LastBackup { get; set; }

        public string Url { get; set; }

        /// <summary>
        /// Xem dữ liệu ảo
        /// </summary>
        public bool IsVirtualData { get; set; }
        /// <summary>
        /// Tự động chay quét dữ liệu tải dữ liệu từ máy chấm công về
        /// </summary>
        public bool IsRunMCC { get; set; }
    }
}
