﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsDangKyCheDoInfo : Interfaces.IDangKyCheDoInfo
    {
        public double ID { get; set; }
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string DepName { get; set; }
        public DateTime TuNgay { get; set; }
        public string StrTuNgay { get { return Libs.clsCommon.ConvertMMddyyy(this.TuNgay); } }
        public string StrTuNgayDMY { get { return Libs.clsCommon.ConvertddMMyyy(this.TuNgay); } }
        public DateTime ToiNgay { get; set; }
        public string StrToiNgay { get { return Libs.clsCommon.ConvertMMddyyy(this.ToiNgay); } }
        public string StrToiNgayDMY { get { return Libs.clsCommon.ConvertddMMyyy(this.ToiNgay); } }
        public string ShiftID { get; set; }
        public string LyDo { get; set; }
        public float SoNgay { get; set; }
        public int ThoiGian { get; set; }
        public int MaCheDo { get; set; }
        public string StrCheDo
        {
            get
            {
                switch (this.MaCheDo)
                {
                    case 1:
                        return "Nuôi con nhỏ";
                    case 2:
                        return "Mang thai 7-9 tháng";
                    case 3:
                        return "Vị thành niên";
                    case 4:
                        return "Thai sản";
                    case 5:
                        return "Chế độ người cao tuổi";
                    default:
                        return "";
                }
            }
        }

        public string KyHieu { get; set; }

        private int _SoNgayConLai = 0;
        public int SoNgayConLai
        {
            get
            {
                _SoNgayConLai = (int)(this.ToiNgay - DateTime.Now).TotalDays;
                //if (ID == 17)
                //    _SoNgayConLai = 3;
                return _SoNgayConLai;
            }
        }
    }
}
