﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsGTDMaHangInfo
    {
        public string Id_Dm { get; set; }
        public string MenuId { get; set; }
        public string MaSo { get; set; }
        public string KhachHang { get; set; }
        public string TenMaHang { get; set; }
        public double SoPhienBan { get; set; }
        public int TrangThai { get; set; }
        public bool DangKhoa { get; set; }
        public string MoTa { get; set; }
        public string MaTienTe { get; set; }
        public float TiGia { get; set; }
        public float ChiPhiKhauCat { get; set; }
        public float ChiPhiThietBi { get; set; }
        public float ChiPhiKiemTra { get; set; }
        public float ChiPhiKhauUi { get; set; }
        public float ChiPhiKhauDongGoi { get; set; }
        public float ChiPhiGiaCongNgoai { get; set; }
        public float DonGiaGiaCongNgoai { get; set; }
        public float ChiPhiSanXuatTrucTiep { get; set; }
        public float ChiPhiQuanLyTrucTiep { get; set; }
        public float ChiPhiVai { get; set; }
        public float ChiPhiPhuLieu { get; set; }
        public float ChiPhiChi { get; set; }
        public float ChiPhiPhuKien { get; set; }
        public float ChiPhiBaoBi { get; set; }
        public float ChiPhiGoc { get; set; }
        public float ChiPhiBanHang { get; set; }
        public float ChiPhiHanNgach { get; set; }
        public string MaChuyenTo { get; set; }
        public float DinhMucNangSuat { get; set; }
        public int TongSoCongNhan { get; set; }
        public float ChiPhiMaHang { get; set; }
        public float TongThoiGian { get; set; }
        public string NguoiTao { get; set; }
        public string NguoiCapNhat { get; set; }
        public DateTime NgayCapNhat { get; set; }
       

        /// <summary>
        /// Get cập nhật lần cuối (Người cập nhật + ngày giờ cập nhật)
        /// </summary>
        public string StrNgayCapNhat
        {
            get
            {
                return this.NguoiCapNhat + ": " + (this.NgayCapNhat.ToString(Libs.clsConsts.FormatMDY).Equals("01/01/0001") ? "" : this.NgayCapNhat.ToString("dd/MM/yyyy HH:mm:ss"));
            }
        }

        public clsGTDThaoTacMaHangCollection ThaoTacMaHangCollection { get; set; }

        /// <summary>
        /// Get tong thoi gian
        /// </summary>
        public float TongThoiGian2
        {
            get
            {
                if (this.ThaoTacMaHangCollection == null) return 0;
                return this.ThaoTacMaHangCollection.Sum(s => s.ThoiGianChuan);
            }
        }
        /// <summary>
        /// Get tổng số lượng công đoạn
        /// </summary>
        public int TongSoLuongCD { get { if (this.ThaoTacMaHangCollection == null)return 0; return this.ThaoTacMaHangCollection.Count; } }
        public clsGTDMaHangInfo()
        {
            if (this.ThaoTacMaHangCollection == null) this.ThaoTacMaHangCollection = new clsGTDThaoTacMaHangCollection();
        }
    }

    public class clsGTDThaoTacMaHangInfo:clsCongDoanBase
    {
        public double ThaoTacMaHangID { get; set; }
        public string Id_DmMaHang { get; set; }
        //public string Id_DmCd { get; set; }
        public double NhomCongDoanID { get; set; }
        public int ThuTu { get; set; }
        public float SoLanXuatHien { get; set; }

        public string MaThietBi { get; set; }
        public string MaBacTho { get; set; }
        public float HeSo { get; set; }
        public float ThoiGianChuan { get; set; }

        public Entity.clsCongDoanInfo CongDoanInfo { get; set; }
        public clsGTDThaoTacMaHangInfo() { }
    }

    public class clsGTDThaoTacMaHangCollection : List<clsGTDThaoTacMaHangInfo>
    {
        public clsGTDThaoTacMaHangCollection() { }
        public clsGTDThaoTacMaHangCollection(List<clsGTDThaoTacMaHangInfo> Items)
            : base(Items)
        {

        }
    }
}
