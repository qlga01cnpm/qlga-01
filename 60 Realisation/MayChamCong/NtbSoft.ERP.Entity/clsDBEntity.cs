﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public abstract class clsDBEntity
    {
        public abstract bool AddNew();

        public abstract bool Delete();

        public abstract bool Update();
        protected abstract void LoadField();
    }
}
