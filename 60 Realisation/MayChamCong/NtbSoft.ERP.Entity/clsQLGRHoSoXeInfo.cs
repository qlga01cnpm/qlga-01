﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsQLGRHoSoXeInfo : Entity.Interfaces.IQLGRHoSoXeInfo
    {
        public int MaHoSo { get; set; }
        public string BienSo { get; set; }
        public string MaHieuXe { get; set; }
        public string KhachHangId { get; set; }
        public DateTime NgayTiepNhan { get; set; }
        public string TienDo { get; set; }
    }
}
