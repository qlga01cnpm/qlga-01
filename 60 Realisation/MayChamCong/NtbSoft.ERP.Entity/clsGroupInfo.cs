﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NtbSoft.ERP.Entity
{
    public class clsGroupCollection : List<clsGroupInfo>
    {
        public clsGroupCollection() { }
        public clsGroupCollection(List<clsGroupInfo> Items) : base(Items) { }
    }

    public class clsGroupInfo
    {
        private string m_GroupID;
        private string m_GroupName;
        private string m_Note;
        private bool m_IsAdmin;
        private string m_Descriptions;

        public clsGroupInfo()
        {
            //m_GroupID = new Guid();
            m_GroupName = string.Empty;
            m_Note = string.Empty;
            m_IsAdmin = false;
        }
        public virtual string GroupID
        {
            get { return m_GroupID; }
            set { m_GroupID = value; }
        }

        public virtual string GroupName
        {
            get { return m_GroupName; }
            set { m_GroupName = value; }
        }

        public virtual string Note
        {
            get { return m_Note; }
            set { m_Note = value; }
        }

        public virtual bool IsAdmin
        {
            get { return m_IsAdmin; }
            set { m_IsAdmin = value; }
        }

        public virtual string Descriptions
        {
            get { return m_Descriptions; }
            set { m_Descriptions = value; }
        }
    }
}
