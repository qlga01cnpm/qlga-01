﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsScheduleShift2Info:Interfaces.IScheduleShift2Info
    {
        public int Id { get; set; }

        public string SchName { get; set; }

        public string ShiftName { get; set; }

        /// <summary>
        /// Ngày lễ
        /// </summary>
        public bool IsNL { get; set; }

        public bool IsCN { get; set; }

        public bool IsT2 { get; set; }

        public bool IsT3 { get; set; }

        public bool IsT4 { get; set; }
        public bool IsT5 { get; set; }

        public bool IsT6 { get; set; }

        public bool IsT7 { get; set; }
    }
}
