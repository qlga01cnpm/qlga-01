﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsWebServerCustomerInfo : Interfaces.IWebServerCustomerInfo
    {
        public string ServerID { get; set; }

        public string Name { get; set; }

        public string MChine { get; set; }

        public string IPLan { get; set; }

        public string IPWan { get; set; }

        public string Mac { get; set; }

        public int Port { get; set; }

        public string Url { get; set; }

        public string PageName { get; set; }

        public DateTime LastUpdated { get; set; }

        public string TypeService { get; set; }
        public Interfaces.IWebCustomerInfo WebCustomerInfo { get; set; }

        public clsWebServerCustomerInfo() { }
        public clsWebServerCustomerInfo(Interfaces.IWebCustomerInfo pWebCustomerInfo)
        { this.WebCustomerInfo = pWebCustomerInfo; }
    }
}
