﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsThietBiInfo 
    {
        public string MaThietBi { get; set; }
        public string MoTa { get; set; }
        public float SoMuiCM { get; set; }

        public int TocDo { get; set; }
        public float HaoPhiThietBi { get; set; }
        public float HaoPhiTayChan { get; set; }
        public byte[] AnhThietBi { get; set; }
        public string RootId { get; set; }

    }
    public class clsThietBiCollection : List<clsThietBiInfo> {
        public clsThietBiCollection() { }

        public clsThietBiCollection(List<clsThietBiInfo> Items):
            base(Items)
        {

        }


    }

}
