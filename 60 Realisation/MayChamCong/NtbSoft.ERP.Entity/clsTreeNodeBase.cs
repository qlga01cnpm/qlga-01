﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsTreeNodeBase
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string text { get; set; }
        public string iconCls { get; set; }
        public string state { get; set; }
        public string url { get; set; }
        public string parentId { get; set; }
        public int Sort { get; set; }

        public string value { get; set; }
        public object attributes { get; set; }
        public int Level { get; set; }
        
    }
}
