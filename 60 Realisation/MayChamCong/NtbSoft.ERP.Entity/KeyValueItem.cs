﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class KeyValueItem
    {
        public object Key { get; set; }
        public object Value { get; set; }
        public string EventName { get; set; }
    }
}
