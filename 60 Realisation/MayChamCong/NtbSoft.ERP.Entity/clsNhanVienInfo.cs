﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsNhanVienInfo:Entity.Interfaces.INhanVienInfo
    {
        public double Id { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public string CardID { get; set; }

        public string MaCC { get; set; }

        public DateTime BirthDate { get; set; }

        public string StrBirthDate
        {
            get { return Libs.clsCommon.ConvertddMMyyy(this.BirthDate); }
        }

        public string BirthPlace { get; set; }

        public int Sex { get; set; }
        public string StrSex
        {
            get
            {
                if (this.Sex == 0) return "Nam";
                if (this.Sex == 1) return "Nữ";

                return "Khác";
            }
        }

        public string Address { get; set; }

        public int PositionID { get; set; }

        public string PositionName { get; set; }

        public string Telephone { get; set; }

        public DateTime BeginningDate { get; set; }
        public string StrBeginningDate
        {
            get { return Libs.clsCommon.ConvertddMMyyy(this.BeginningDate); }
        }

        public DateTime StopWorkingday { get; set; }
        public string StrStopWorkingday
        {
            get { return Libs.clsCommon.ConvertddMMyyy(this.StopWorkingday); }
        }

        public DateTime NgayDu18Tuoi { get; set; }

        public bool IsVisible { get; set; }

        public bool IsNghiViec { get; set; }

        public string LyDoNghiViec { get; set; }

        public string SCHName { get; set; }

        public string ShiftID { get; set; }

        public int STT { get; set; }

        public bool IsShift { get; set; }
        public string DepID { get; set; }
        public string DepName { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }


        public string NguyenQuan { get; set; }

        public string CMND { get; set; }

        public string NoiCap { get; set; }

        public DateTime NgayKyHopDong { get; set; }

        public DateTime NgayQuayLai { get; set; }
        public int LoaiCongViec { get; set; }
        public string StrLoaiCongViec
        {
            get
            {
                switch (this.LoaiCongViec)
                {
                    case 1: return "Nặng nhọc - độc hại";
                    case 2: return "Nặng nhọc - độc hại - nguy hiểm";
                    default:
                        return "";
                }
            }
        }
    }
}
