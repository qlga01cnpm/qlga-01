﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsNgayLeInfo:Interfaces.INgayLeInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Ngay { get; set; }
        public string StrNgay { get { return Libs.clsCommon.ConvertddMMyyy(this.Ngay); } }
        public int Active { get; set; }
    }
}
