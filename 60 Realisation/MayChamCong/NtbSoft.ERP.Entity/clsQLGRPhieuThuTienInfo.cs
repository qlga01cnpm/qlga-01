﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.ERP.Entity.Interfaces;

namespace NtbSoft.ERP.Entity
{
    public class clsQLGRPhieuThuTienInfo : Entity.Interfaces.IQLGRPhieuThuTienInfo
    {
        public int HoSoId { get; set; }
        public DateTime NgayThuTien { get; set; }
        public double SoTienThu { get; set; }
        public double TongTien { get; set; }
    }
}
