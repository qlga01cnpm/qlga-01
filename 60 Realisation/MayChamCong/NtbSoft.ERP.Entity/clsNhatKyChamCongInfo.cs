﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsNhatKyChamCongInfo : Interfaces.INhatKyChamCongInfo
    {
        public int TerminalID { get; set; }

        public string MaCC { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public DateTime WorkingDay { get; set; }

        public string StrWorkingDay
        {
            get { return Libs.clsCommon.ConvertddMMyyy(this.WorkingDay); }
        }

        public DateTime CreateDateTime { get; set; }

        public string StrCreateDateTime
        {
            get { return Libs.clsCommon.ConvertddMMyyyHHmmss(this.CreateDateTime); }
        }

        public string GioQuet { get; set; }

        public string Door { get; set; }
    }
}
