﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Entity
{
    public class clsTreeDataInfo : clsTreeNodeBase
    {
        public List<clsTreeDataInfo> children { get; set; }

        public clsTreeDataInfo() { }
        public clsTreeDataInfo(System.Data.DataRow row)
        {
            this.id = row.Field<string>("Id");
            this.Name = row.Field<string>("Name");
            this.text = this.Name;
            this.iconCls = row.Field<string>("iconCls");
            this.url = row.Field<string>("url");
            this.Level = row.Field<int>("Node");
        }
        

    }

    public class clsTreeDataCollection : List<clsTreeDataInfo>
    {
        public clsTreeDataCollection() {}
        public clsTreeDataCollection(List<clsTreeDataInfo> Items)
            : base(Items)
        {

        }
    }
}
