﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsCoDienInfo:Interfaces.ICoDienInfo,Interfaces.ICardInfo
    {
        #region Thông tin cơ điện
        public string Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }
        #endregion

        #region Thông tin thẻ
        [JsonProperty("CardId")]
        public string CardId { get; set; }

        [JsonProperty("CardLabel")]
        public string CardLabel { get; set; }

        [JsonProperty("CardType")]
        public int CardType { get; set; }

        [JsonIgnore]
        [JsonProperty("LastEventTime")]
        public DateTime LastEventTime { get; set; }
        #endregion
    }
}
