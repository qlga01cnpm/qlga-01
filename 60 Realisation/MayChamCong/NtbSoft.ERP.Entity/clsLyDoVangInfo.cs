﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsLyDoVangInfo:Interfaces.ILyDoVangInfo
    {
        public int Id { get; set; }

        public string MaSo { get; set; }

        public string LyDo { get; set; }

        public float Phep { get; set; }

        public float BHXH { get; set; }

        public float HocHop { get; set; }

        public float LeTet { get; set; }

        public float KhongLuong { get; set; }

        public float CoLuong { get; set; }

        public float ThaiSan { get; set; }

        public float DieuDuong { get; set; }

        public float NghiTuDo { get; set; }

        public bool IsHienThi { get; set; }

        public bool IsVP { get; set; }


        public float NghiBu { get; set; }
    }
}
