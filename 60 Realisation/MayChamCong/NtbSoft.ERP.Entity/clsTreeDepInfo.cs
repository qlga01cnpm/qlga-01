﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsTreeDepInfo : clsTreeNodeBase
    {
        /// <summary>
        /// ContextMenuId cho thư mục. Khi Url="" thì dùng ContextMenu này
        /// </summary>
        public string ContextMenuId { get; set; }
        /// <summary>
        /// CntextMenuId cho File. Khi Url!="" thì dùng ContextMenu này
        /// </summary>
        public string ContextMenuIdFile { get; set; }
       
        public bool IsChain { get; set; }

        /// <summary>
        /// Trực tiếp may
        /// </summary>
        public bool IsChainSew { get; set; }

        /// <summary>
        /// Tổ cắt
        /// </summary>
        public bool IsChainCut { get; set; }

        /// <summary>
        /// Đóng gói - Thành phẩm
        /// </summary>
        public bool IsChainPackage { get; set; }

        /// <summary>
        /// Kho
        /// </summary>
        public bool IsWarehouse { get; set; }

        public List<clsTreeDepInfo> children { get; set; }
        //public List<clsTreeDepInfo> children1 { get; set; }
        public clsTreeDepInfo() { }
    }

    [Serializable]
    public class clsTreeDepCollection : List<clsTreeDepInfo> { }
}
