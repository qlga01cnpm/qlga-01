﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsTimeInOutSessionInfo : Interfaces.ITimeInOutSessionInfo
    {
        public double Id { get; set; }
        public DateTime Workingday { get; set; }
        public string StrWorkingDay { get { return Libs.clsCommon.ConvertddMMyyy(this.Workingday); } }

        private string _StrThu="";
        public string StrThu
        {
            get
            {
                if(string.IsNullOrEmpty(_StrThu))
                    _StrThu = Libs.clsCommon.GetDayOfWeekVn(Workingday);
                return _StrThu;
            }
        }
        public string DepID { get; set; }
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string TimeIn { get; set; }
        public string BreakOut { get; set; }
        public string BreakIn { get; set; }
        public string TimeOut { get; set; }
        public float OverTime { get; set; }
        public float TotalTime { get; set; }
        public string TotalTimeHM { get; set; }
        public float TotalHours { get; set; }
        public string LyDoVang { get; set; }
        public bool IsChuanHoa { get; set; }
        public string SchName { get; set; }
        public string StrShiftID { get; set; }
        public string Shift { get; set; }

        public string StrTimeIn { get; set; }
        public string StrBreakOut { get; set; }
        public string StrBreakIn { get; set; }
        public string StrTimeOut { get; set; }
        public double TotalTimeMinute { get; set; }
        public float LateTime { get; set; }
        public float EarlyTime { get; set; }
        public bool IsLate { get; set; }
        public bool IsEarly { get; set; }
        public DateTime DateTimeIn { get; set; }
        public DateTime DateBreakOut { get; set; }
        public DateTime DateBreakIn { get; set; }
        public DateTime DateTimeOut { get; set; }
        public float CongK { get; set; }
        public string StrCongK
        {
            get
            {
                if (this.CongK > 0) return this.CongK.ToString();
                return "";
            }
        }
        public float CongX { get; set; }
       
        public float OTK { get; set; }

        public float OTX { get; set; }
        public float OTXH
        {
            get
            {
                return (float)Math.Round(this.OTX / 60f, 1);
            }
        }
        public float mOTX { get; set; }
        public float mOTK { get; set; }
        public float CongCNK { get; set; }
        public float CongCNX { get; set; }
        public float OTCNK { get; set; }
        public float OTCNX { get; set; }
        public float Cong30P { get; set; }
        public float CongAnTrua { get; set; }
        public float CongAnChieu { get; set; }
        public float CongLe { get; set; }
        public float CongRo { get; set; }
        public float CongR { get; set; }
        public float CongOm { get; set; }
        public float CongConOm { get; set; }
        public float CongPhep { get; set; }
        public float CongKT { get; set; }
        public float CongDe { get; set; }
        public float CongTS { get; set; }
        public float CongNB { get; set; }
        public float CongCD { get; set; }
        public float CongV0 { get; set; }
        public float CongP { get; set; }
        public float CongCT { get; set; }
        public float CongThieu { get; set; }
        public float CongH { get; set; }
        public float GioConBu { get; set; }
        public float GioThai7Thang { get; set; }
        public bool IsTS { get; set; }
        public float RegNight { get; set; }
    }
}
