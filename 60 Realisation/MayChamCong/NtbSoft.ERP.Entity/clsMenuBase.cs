﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsMenuBase
    {
        public string id { get; set; }
        public string MenuParent { get; set; }

        public string text { get; set; }
        public string url { get; set; }
        public string PageName { get; set; }
        public int Sort { get; set; }
        public bool IsVisible { get; set; }
        public string iconCls { get; set; }
        /// <summary>
        /// ContextMenuId cho thư mục. Khi Url="" thì dùng ContextMenu này
        /// </summary>
        public string ContextMenuId { get; set; }
        /// <summary>
        /// CntextMenuId cho File. Khi Url!="" thì dùng ContextMenu này
        /// </summary>
        public string ContextMenuIdFile { get; set; }

        /// <summary>
        /// Url mặc định để khi tao File sẽ trỏ đến url đó.
        /// </summary>
        public string DefaultUrl { get; set; }

        public string MenuType { get; set; }

        /// <summary>
        /// Xác định chức năng; true: là menu chức năng; false: menu thường
        /// </summary>
        public bool IsFunc { get; set; }
        
        /// <summary>
        /// Mã số
        /// </summary>
        public string Code { get; set; }
    }
}
