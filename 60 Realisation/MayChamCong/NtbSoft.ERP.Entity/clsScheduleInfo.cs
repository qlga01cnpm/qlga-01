﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsScheduleInfo:Interfaces.IScheduleInfo
    {
        public int Id { get; set; }
        public string SchName { get; set; }
        public string Shift { get; set; }
        public int Chuky { get; set; }
        public string Descriptions { get; set; }

        public int NumberShift { get; set; }
        public string Shift1 { get; set; }
        public string Shift2 { get; set; }
        public string Shift3 { get; set; }
        public string TimeInS1 { get; set; }
        public string TimeInS2 { get; set; }
        public string TimeInS3 { get; set; }

        public clsScheduleInfo()
        {
            this.SchName = ""; this.Shift = ""; this.Shift1 = ""; this.Shift2 = "";
            this.Shift3 = ""; this.TimeInS1 = ""; this.TimeInS2 = ""; this.TimeInS3 = "";
        }
    }
}
