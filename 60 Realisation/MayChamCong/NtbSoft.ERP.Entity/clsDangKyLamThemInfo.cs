﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsDangKyLamThemInfo : Interfaces.IDangKyLamThemInfo
    {
        public double ID { get; set; }
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string DepName { get; set; }
        public DateTime NgayDangKy { get; set; }
        public string DenNgay
        {
            get;
            set;
        }
        public string StrNgayDangKy { get { return Libs.clsCommon.ConvertMMddyyy(this.NgayDangKy); } }
        public string StrNgayDangKyDMY { get { return Libs.clsCommon.ConvertddMMyyy(this.NgayDangKy); } }
        public string TuGio { get; set; }
        public string DenGio { get; set; }
        public decimal SoGio { get; set; }
    }
}
