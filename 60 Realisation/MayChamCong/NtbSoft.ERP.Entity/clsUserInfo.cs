﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NtbSoft.ERP.Entity
{
    public enum UserStatus
    {
        OK = 1,
        Locked = 2,
        ExpiredDate = 3,
        NotExists = 4
    }
    [Serializable]
    public class clsUserCollection : List<clsUserInfo>
    {
    }

    [Serializable]
    public class clsUserInfo
    {
        private string          m_LoginID;        
        private string          m_Password;
        private string          m_FullName;
        private string          m_Email;        
        private DateTime        m_CreatedDate;
        private bool            m_LockedUser;
        private DateTime        m_LockedDate;
        private string          m_LockedReason;
        private DateTime        m_LastLogIn;
        private DateTime        m_LastChangedPassword;
        private DateTime        m_DeadlineOfUsing;
        
        private string m_FirstName;
        private string m_LastName;
        private string m_GroupID;
        private bool m_IsAdmin;
        private bool m_IsLock;
        private bool m_Active;
        private DateTime m_LastView;
        private DateTime m_LastAccess;
        private int m_CountView;
        private bool m_IsNewRow;

        public virtual string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        public virtual string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public virtual string GroupID
        {
            get { return m_GroupID; }
            set { m_GroupID = value; }
        }
        public virtual bool IsAdmin
        {
            get { return m_IsAdmin; }
            set { m_IsAdmin = value; }
        }
        public virtual bool IsLock
        {
            get { return m_IsLock; }
            set { m_IsLock = value; }
        }
        public virtual bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        public virtual string StrLastView
        {
            get
            {
                if (!this.LastView.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return this.LastView.ToString("dd/MM/yyyy HH:mm:ss");
                return "";
            }
        }
        public virtual DateTime LastView
        {
            get { return m_LastView; }
            set { m_LastView = value; }
        }

        public virtual string StrLastAccess
        {
            get
            {
                if (!this.LastAccess.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return this.LastAccess.ToString("dd/MM/yyyy HH:mm:ss");
                return "";
            }
        }
        public virtual DateTime LastAccess
        {
            get { return m_LastAccess; }
            set { m_LastAccess = value; }
        }
        public virtual int CountView
        {
            get { return m_CountView; }
            set { m_CountView = value; }
        }
        public virtual bool IsNewRow
        {
            get { return m_IsNewRow; }
            set { m_IsNewRow = value; }
        }

        public clsUserInfo()
        {
            m_LoginID = string.Empty;
            m_Password = string.Empty;
            m_FullName = string.Empty;
            m_Email = string.Empty;

            DateTime dtmDefault = new DateTime(1900, 1, 1);
            m_CreatedDate = dtmDefault;
            m_LockedUser = false;
            m_LockedDate = dtmDefault;
            m_LockedReason = string.Empty;
            m_LastLogIn = dtmDefault;
            m_LastChangedPassword = dtmDefault;
            m_DeadlineOfUsing = dtmDefault;
            m_IsNewRow = false;
            
        }

        public virtual string UserID
        {
            get { return m_LoginID; }
            set { m_LoginID = value; }
        }
        
        public virtual string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public virtual string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

        public virtual string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
       
        public virtual DateTime CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }

        public virtual bool LockedUser
        {
            get { return m_LockedUser; }
            set { m_LockedUser = value; }
        }

        public virtual DateTime LockedDate
        {
            get { return m_LockedDate; }
            set { m_LockedDate = value; }
        }

        public virtual string LockedReason
        {
            get { return m_LockedReason; }
            set { m_LockedReason = value; }
        }

        public virtual DateTime LastLogIn
        {
            get { return m_LastLogIn; }
            set { m_LastLogIn = value; }
        }

        public virtual DateTime LastChangedPassword
        {
            get { return m_LastChangedPassword; }
            set { m_LastChangedPassword = value; }
        }

        public virtual DateTime DeadlineOfUsing
        {
            get { return m_DeadlineOfUsing; }
            set { m_DeadlineOfUsing = value; }
        }        
    }
}
