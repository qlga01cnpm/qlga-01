﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsScheduleShiftInfo:Interfaces.IScheduleShiftInfo
    {
        public clsScheduleShiftInfo() { }
        public clsScheduleShiftInfo(int Ngay) { this.Ngay = Ngay; }
        public int Ngay { get; set; }

        public string StrNgay
        {
            get
            {
                switch (this.Ngay)
                {
                    case 1: return "Chủ nhật";
                    case 2: return "Thứ 2";
                    case 3: return "Thứ 3";
                    case 4: return "Thứ 4";
                    case 5: return "Thứ 5";
                    case 6: return "Thứ 6";
                    case 7: return "Thứ 7";
                    case 8: return "Nghỉ Lễ";
                    default: return "";
                }
            }
        }
        public int SchID { get; set; }
        public string SchName { get; set; }
        public string Shift1 { get; set; }
        public string Shift2 { get; set; }
        public string Shift3 { get; set; }
        public string Shift4 { get; set; }
        public string Shift5 { get; set; }
        public string Shift6 { get; set; }
        public string Shift7 { get; set; }
        public string Shift8 { get; set; }
        public string Shift9 { get; set; }

        public int NumberShift { get; set; }
        public string Shift { get; set; }
        public string StrShift { get; set; }
        public bool IsUse { get; set; }   
    }
}
