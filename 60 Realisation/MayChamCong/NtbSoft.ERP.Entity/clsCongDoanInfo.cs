﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsCongDoanInfo:clsCongDoanBase
    {
        private Entity.clsThietBiInfo _mThietBiInfo = null;
        private Entity.clsThaoTacSpInfo _mThaoTacSPInfo = null;

        private double _TMU_Phut = 0.00060000002849847078;
       
        public string MaBoPhan { get; set; }
        public int LucTacDung { get; set; }
        public string MaBacTho { get; set; }
        public string MaThietBi { get; set; }
        public string MaThaoTacSp { get; set; }
        public float HaoPhiDacBiet { get; set; }
        public string MoTaNoiLamViec { get; set; }
        public float ThoiGianChuan { get; set; }

        public string StrLastUpdate
        {
            get
            {
                return this.NguoiCapNhat + ": " + (this.NgayCapNhat.ToString("MM/dd/yyyy") == "01/01/0001" ? "" : this.NgayCapNhat.ToString("dd/MM/yyyy HH:mm:ss"));
            }
        }

        /// <summary>
        /// Get thời gian thiết bị TMU
        /// </summary>
        public double THOI_GIAN_THIET_BI_TMU
        {
            get
            {
                if (this.ThaoTacCongDoan == null) return 0;
                return this.ThaoTacCongDoan.Sum(s => s.TMUThietBi);
            }
        }

        /// <summary>
        /// Get thời gian thao tác TMU
        /// </summary>
        public double THOI_GIAN_THAO_TAC_TMU
        {
            get
            {
                if (this.ThaoTacCongDoan == null) return 0;
                return this.ThaoTacCongDoan.Sum(s => s.TMUThaoTac);
            }
        }

       
        public double THOI_GIAN_THAO_TAC_PHUT
        {
            get
            {
                //return this.mTHOI_GIAN_THAO_TAC_TMU * DBConstant.TMU_PHUT;
                return this.THOI_GIAN_THAO_TAC_TMU * _TMU_Phut;
            }
        }

        /// <summary>
        /// THOI_GIAN_THIET_BI_TMU * TMU_PHUT
        /// </summary>
        public double THOI_GIAN_THIET_BI_PHUT
        {
            get
            {
                //return this.mTHOI_GIAN_THIET_BI_TMU * DBConstant.TMU_PHUT;
                return this.THOI_GIAN_THIET_BI_TMU * _TMU_Phut;
            }
        }

        /// <summary>
        /// Get: thoigianphut=thoi_gian_thiet_bi_phut + thoi_gian_thao_tac_phut
        /// </summary>
        public double TONG_THOI_GIAN_PHUT
        {
            get
            {
                
                return this.THOI_GIAN_THIET_BI_PHUT + THOI_GIAN_THAO_TAC_PHUT;
            }
        }

        /// <summary>
        /// Get: Tính tổng chiều dài đường may
        /// </summary>
        public double Tinh_TONG_CHIEU_DAI_DUONG_MAY
        {
            get
            {
                if (this.ThaoTacCongDoan == null || this.ThaoTacCongDoan.Count == 0) return 0;
                double mTong_chieu_dai_duong_may = 0;
                foreach (var item in this.ThaoTacCongDoan)
                {
                    if (string.IsNullOrEmpty(item.LoaiMaSo)) continue;

                    if(item.LoaiMaSo.Trim() !=Libs.clsDBConstant.cTHAO_TAC_DAC_BIET &&
                       item.LoaiMaSo.Trim() != Libs.clsDBConstant.cCHIEU_DAI_DUONG_MAY &&
                        item.LoaiMaSo.Trim() != Libs.clsDBConstant.cTHAO_TAC_CAT)
                    {
                        mTong_chieu_dai_duong_may += item.ChieuDaiDuongMay * item.SoLanXuatHien;
                    }
                    else if (item.LoaiMaSo.Trim() == Libs.clsDBConstant.cCHIEU_DAI_DUONG_MAY)
                    {
                        mTong_chieu_dai_duong_may += Convert.ToDouble(item.MaSo.Substring(2, item.MaSo.Length - 3)) * item.SoLanXuatHien;
                    }
                }

                return mTong_chieu_dai_duong_may;
            }
        }

        /// <summary>
        /// Get tổng thời gian chuẩn phút (Sản lượng mỗi giờ)
        /// </summary>
        public double TONG_THOI_GIAN_CHUAN_PHUT
        {
            get
            {
                return this.TONG_THOI_GIAN_PHUT + _mTHOI_GIAN_THAO_TAC_HAO_PHI_PHUT + _mTHOI_GIAN_THIET_BI_HAO_PHI_PHUT;
            }
        }

        private static double _mTHOI_GIAN_THAO_TAC_HAO_PHI_PHUT = 0;
        public double THOI_GIAN_THAO_TAC_HAO_PHI_PHUT(Entity.clsThietBiInfo pThietBi)
        {
            _mTHOI_GIAN_THAO_TAC_HAO_PHI_PHUT = this.THOI_GIAN_THAO_TAC_PHUT * pThietBi.HaoPhiTayChan / 100.0;
            return _mTHOI_GIAN_THAO_TAC_HAO_PHI_PHUT;
        }

        private static double _mTHOI_GIAN_THIET_BI_HAO_PHI_PHUT = 0;
        public double THOI_GIAN_THIET_BI_HAO_PHI_PHUT(Entity.clsThietBiInfo pThietBi)
        {
            _mTHOI_GIAN_THIET_BI_HAO_PHI_PHUT = this.THOI_GIAN_THIET_BI_PHUT * pThietBi.HaoPhiThietBi / 100.0;
            return _mTHOI_GIAN_THIET_BI_HAO_PHI_PHUT;
        }

        /// <summary>
        /// Get tổng thời gian TMU
        /// </summary>
        public double TONG_THOI_GIAN_TMU
        {
            get
            {
                //return this.mTHOI_GIAN_THIET_BI_TMU + this.mTHOI_GIAN_THAO_TAC_TMU;
                return this.TongTMUThaoTac + this.TongTMUThietBi;
            }
        }

        /// <summary>
        /// Get hao phí thiết bị. mTongTMUThietBi * (mThietBiInfo.HaoPhiThietBi / 100f);
        /// </summary>
        public double HaoPhiThietBi
        {
            get
            {
                double mHaoPhiThietBi = this._mThietBiInfo == null ? 0 : this._mThietBiInfo.HaoPhiThietBi;
                return this.TongTMUThietBi * (mHaoPhiThietBi / 100f);
                
            }
        }

        /// <summary>
        /// Get hao phí thiết bị. mTongTMUThaoTac * (mThietBiInfo.HaoPhiTayChan / 100f);
        /// </summary>
        public double HaoPhiThaoTac
        {
            get
            {
                double mHaoPhiTayChan = this._mThietBiInfo == null ? 0 : this._mThietBiInfo.HaoPhiTayChan;
                return this.TongTMUThaoTac * (mHaoPhiTayChan / 100f);
            }
        }

        /// <summary>
        /// Get tổng TMU Thiết bị
        /// </summary>
        public double TongTMUThietBi
        {
            get { return this.ThaoTacCongDoan == null ? 0 : this.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThietBiTemp, 2)); }
            //get { return this.ThaoTacCongDoan == null ? 0 : this.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThietBi, 2)); }
        }
        public double TongTMUThaoTac
        {
            get { return this.ThaoTacCongDoan == null ? 0 : this.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThaoTacTemp, 2)); }
            //get { return this.ThaoTacCongDoan == null ? 0 : this.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThaoTac, 2)); }
        }

        /// <summary>
        /// Get tổng thời gian tính bằng giây. ((mTongTMUThietBi + mHaoPhiThietBi) + (mTongTMUThaoTac + mHaoPhiThaoTac)) / 27.8f;
        /// </summary>
        public double TongThoiGianGiay
        {
            get
            {
                return ((this.TongTMUThietBi + this.HaoPhiThietBi) + (this.TongTMUThaoTac + this.HaoPhiThaoTac)) / 27.8;
            }
        }

        /// <summary>
        /// Get thời gian chuẩn bị. (mThaoTacSPInfo.ThoiGianThaoTac * 60f) / mThaoTacSPInfo.SoLuong;
        /// </summary>
        public double ThoiGianChuanBi
        {
            get
            {
                if (this._mThaoTacSPInfo == null || this._mThaoTacSPInfo.SoLuong == 0) return 0;
                return  (this._mThaoTacSPInfo.ThoiGianThaoTac * 60f) / this._mThaoTacSPInfo.SoLuong;
            }
        }
        /// <summary>
        /// Get: SanLuongMoiGio=3600/Tổng số giây
        /// </summary>
        public double SanLuongMoiGio
        {
            get
            {
                double kq = this.TongThoiGianGiay + this.ThoiGianChuanBi;
                if (kq == 0) return 0;

                return 3600f / kq;
            }
        }


        public clsThaoTacCongDoanCollection ThaoTacCongDoan { get; set; }

        public clsCongDoanInfo() {
            if (this.ThaoTacCongDoan == null)
                this.ThaoTacCongDoan = new clsThaoTacCongDoanCollection(new Entity.clsThaoTacCongDoanInfo());
        }

        public void SetThietBi_ThaoTac(Entity.clsThietBiInfo pThietBiInfo, Entity.clsThaoTacSpInfo pThaoTacSpInfo)
        {
            this._mThietBiInfo = pThietBiInfo;
            this._mThaoTacSPInfo = pThaoTacSpInfo;
        }


    }

    public class clsCongDoanCollection : List<clsCongDoanInfo> {
        public clsCongDoanCollection() { }
        public clsCongDoanCollection(List<clsCongDoanInfo> Items)
            : base(Items)
        {

        }
    }


    [Serializable]
    public class clsThaoTacCongDoanInfo
    {
        public double ThaoTacCongDoanID { get; set; }
        public string Id_DmCd { get; set; }
        public string MaSo { get; set; }
        public int ThuTu { get; set; }
        public float SoLanXuatHien { get; set; }
        public string MoTa { get; set; }
        public float TMUThaoTac { get; set; }

        /// <summary>
        /// Get
        /// </summary>
        public float TMUThietBiTemp { get { return this.SoLanXuatHien * this.TMUThietBi; } }

        /// <summary>
        /// Get 
        /// </summary>
        public float TMUThaoTacTemp { get { return this.SoLanXuatHien * this.TMUThaoTac; } }
        public string LoaiMaSo { get; set; }
        public float TMUThietBi { get; set; }

        
        public float ChieuDaiDuongMay { get; set; }
        public clsThaoTacCongDoanInfo() { }

        public clsThaoTacCongDoanInfo(clsThuVienInfo Item)
        {
            if (Item == null) return;
            this.MaSo = Item.MaThuVien.ToUpper();
            // MaSo: data.MaThuVien, MoTa: data.MoTa, TMUThietBi: data.TMUThietBi, TMUThaoTac: data.TMUThaoTac
            this.MoTa = Item.MoTa;
            this.SoLanXuatHien = 1;
            this.LoaiMaSo = Item.MenuType;
            if (Item.KemThietBi) this.TMUThietBi = Item.TMU;
            if (!Item.KemThietBi) this.TMUThaoTac = Item.TMU;
        }
    }

    [Serializable]
    public class clsThaoTacCongDoanCollection : List<clsThaoTacCongDoanInfo>
    {
        public clsThaoTacCongDoanCollection() { }
        public clsThaoTacCongDoanCollection(clsThaoTacCongDoanInfo Item) {
            this.Add(Item);
        }

        public clsThaoTacCongDoanCollection(List<clsThaoTacCongDoanInfo> Items)
            : base(Items)
        {

        }

    }
}
