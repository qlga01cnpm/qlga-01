﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsThaoTacSpInfo
    {
        public string MaThaoTacSP { get; set; }
        public string MoTa { get; set; }
        public float ThoiGianThaoTac { get; set; }
        public int SoLuong { get; set; }
        public string RootId { get; set; }

        /// <summary>
        /// Get thời gian tháo cột bó hàng
        /// </summary>
        public double THOI_GIAN_THAO_COT_BO_HANG
        {
            get
            {
                return this.ThoiGianThaoTac / (double)(this.SoLuong == 0 ? 1 : this.SoLuong);
            }
        }

        public clsThaoTacSpInfo() { }
    }

    [Serializable]
    public class clsThaoTacSpCollection:List<clsThaoTacSpInfo>
    {
        public clsThaoTacSpCollection() { }
        public clsThaoTacSpCollection(List<clsThaoTacSpInfo> Items)
            : base(Items)
        {

        }
    }
}
