﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsTheoDoiPhepNamInfo : Interfaces.ITheoDoiPhepNamInfo
    {
        public int sMonth { get; set; }

        public int sYear { get; set; }
        public string eID { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public string DepName { get; set; }

        public string M1 { get; set; }

        public string M2 { get; set; }

        public string M3 { get; set; }

        public string M4 { get; set; }

        public string M5 { get; set; }

        public string M6 { get; set; }

        public string M7 { get; set; }

        public string M8 { get; set; }

        public string M9 { get; set; }

        public string M10 { get; set; }

        public string M11 { get; set; }

        public string M12 { get; set; }



        public string SoNgayPhep { get; set; }

        public string SoNgayNghi { get; set; }

        private string _SoNgayConLai = "";
        public string SoNgayConLai
        {
            get
            {
                decimal mSoNgayNghi =
                    Libs.Utilities.clsFormat.DecimalConvert(SoNgayPhep) -
                     Libs.Utilities.clsFormat.DecimalConvert(SoNgayNghi);
                if (mSoNgayNghi == 0) _SoNgayConLai = "";
                else _SoNgayConLai = mSoNgayNghi.ToString();

                return _SoNgayConLai;
            }
        }

        public string NgayVao { get; set; }

        
    }
}
