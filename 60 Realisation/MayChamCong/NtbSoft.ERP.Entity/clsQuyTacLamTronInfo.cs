﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsQuyTacLamTronInfo:Interfaces.IQuyTacLamTronInfo
    {
        public clsQuyTacLamTronInfo() { }
        public int Id { get; set; }

        public int TuPhut { get; set; }

        public int DenPhut { get; set; }

        public int LamTron { get; set; }
    }
}
