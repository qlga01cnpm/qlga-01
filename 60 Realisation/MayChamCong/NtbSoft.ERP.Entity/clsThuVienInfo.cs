﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsThuVienInfo
    {
        public string MaThuVien { get; set; }
        public string MoTa { get; set; }
        public string GhiChu { get; set; }
        public bool KemThietBi { get; set; }

        
        public float ChieuDaiDuongMay { get; set; }
        public float TMU { get; set; }
        public byte[] Video { get; set; }
        public bool DuocTaoBoiHeThong { get; set; }
        public string MenuId { get; set; }
        public string  MenuType { get; set; }
        public double SoPhut
        {
            get { return this.TMU * Convert.ToDouble(0.0006); }
        }
        public clsThuVienInfo() { }
    }

    [Serializable]
    public class clsThuVienCollection : List<clsThuVienInfo>
    {
        public clsThuVienCollection() { }
        public clsThuVienCollection(List<clsThuVienInfo> Items)
            : base(Items)
        {

        }
    }
}
