﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsQLGRKhachHangInfo : Entity.Interfaces.IQLGRKhachHangInfo
    {
        public string KhachHangId { get; set; }
        public string TenKhachHang { get; set; }
        public string SoDienThoai { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public double TienNo { get; set; }
    }
    
}
