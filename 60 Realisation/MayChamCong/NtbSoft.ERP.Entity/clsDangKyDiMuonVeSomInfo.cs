﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsDangKyDiMuonVeSomInfo:Interfaces.IDangKyDiMuonVeSomInfo
    {
        public int sMonth { get; set; }

        public int sYear { get; set; }

        public string eID { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public string SCHName { get; set; }

        public string ShiftID { get; set; }


        public string M1 { get; set; }

        public string M2 { get; set; }

        public string M3 { get; set; }

        public string M4 { get; set; }

        public string M5 { get; set; }

        public string M6 { get; set; }

        public string M7 { get; set; }

        public string M8 { get; set; }

        public string M9 { get; set; }

        public string M10 { get; set; }

        public string M11 { get; set; }

        public string M12 { get; set; }

        public string M13 { get; set; }

        public string M14 { get; set; }

        public string M15 { get; set; }
        public string M16 { get; set; }

        public string M17 { get; set; }

        public string M18 { get; set; }

        public string M19 { get; set; }

        public string M20 { get; set; }

        public string M21 { get; set; }

        public string M22 { get; set; }

        public string M23 { get; set; }

        public string M24 { get; set; }

        public string M25 { get; set; }

        public string M26 { get; set; }

        public string M27 { get; set; }

        public string M28 { get; set; }

        public string M29 { get; set; }

        public string M30 { get; set; }

        public string M31 { get; set; }

        public string S1 { get; set; }

        public string S2 { get; set; }

        public string S3 { get; set; }

        public string S4 { get; set; }

        public string S5 { get; set; }

        public string S6 { get; set; }

        public string S7 { get; set; }

        public string S8 { get; set; }

        public string S9 { get; set; }

        public string S10 { get; set; }

        public string S11 { get; set; }

        public string S12 { get; set; }

        public string S13 { get; set; }

        public string S14 { get; set; }

        public string S15 { get; set; }

        public string S16 { get; set; }

        public string S17 { get; set; }

        public string S18 { get; set; }

        public string S19 { get; set; }

        public string S20 { get; set; }

        public string S21 { get; set; }

        public string S22 { get; set; }

        public string S23 { get; set; }

        public string S24 { get; set; }

        public string S25 { get; set; }

        public string S26 { get; set; }

        public string S27 { get; set; }

        public string S28 { get; set; }

        public string S29 { get; set; }

        public string S30 { get; set; }

        public string S31 { get; set; }

        public string GetMValue(int Day)
        {
            switch (Day)
            {
                case 1: return M1;
                case 2: return M2;
                case 3: return M3;
                case 4: return M4;
                case 5: return M5;
                case 6: return M6;
                case 7: return M7;
                case 8: return M8;
                case 9: return M9;
                case 10: return M10;

                case 11: return M11;
                case 12: return M12;
                case 13: return M13;
                case 14: return M14;
                case 15: return M15;
                case 16: return M16;
                case 17: return M17;
                case 18: return M18;
                case 19: return M19;
                case 20: return M20;

                case 21: return M21;
                case 22: return M22;
                case 23: return M23;
                case 24: return M24;
                case 25: return M25;
                case 26: return M26;
                case 27: return M27;
                case 28: return M28;
                case 29: return M29;
                case 30: return M30;

                case 31: return M31;
                default:
                    return "";
            }
        }

        public void SetMValue(int Day, string value)
        {
            switch (Day)
            {
                case 1: this.M1 = value; break;
                case 2: this.M2 = value; break;
                case 3: this.M3 = value; break;
                case 4: this.M4 = value; break;
                case 5: this.M5 = value; break;
                case 6: this.M6 = value; break;
                case 7: this.M7 = value; break;
                case 8: this.M8 = value; break;
                case 9: this.M9 = value; break;
                case 10: this.M10 = value; break;

                case 11: this.M11 = value; break;
                case 12: this.M12 = value; break;
                case 13: this.M13 = value; break;
                case 14: this.M14 = value; break;
                case 15: this.M15 = value; break;
                case 16: this.M16 = value; break;
                case 17: this.M17 = value; break;
                case 18: this.M18 = value; break;
                case 19: this.M19 = value; break;
                case 20: this.M20 = value; break;

                case 21: this.M21 = value; break;
                case 22: this.M22 = value; break;
                case 23: this.M23 = value; break;
                case 24: this.M24 = value; break;
                case 25: this.M25 = value; break;
                case 26: this.M26 = value; break;
                case 27: this.M27 = value; break;
                case 28: this.M28 = value; break;
                case 29: this.M29 = value; break;
                case 30: this.M30 = value; break;

                case 31: this.M31 = value; break;

            }
        }

        public string GetSValue(int Day)
        {
            switch (Day)
            {
                case 1: return S1;
                case 2: return S2;
                case 3: return S3;
                case 4: return S4;
                case 5: return S5;
                case 6: return S6;
                case 7: return S7;
                case 8: return S8;
                case 9: return S9;
                case 10: return S10;

                case 11: return S11;
                case 12: return S12;
                case 13: return S13;
                case 14: return S14;
                case 15: return S15;
                case 16: return S16;
                case 17: return S17;
                case 18: return S18;
                case 19: return S19;
                case 20: return S20;

                case 21: return S21;
                case 22: return S22;
                case 23: return S23;
                case 24: return S24;
                case 25: return S25;
                case 26: return S26;
                case 27: return S27;
                case 28: return S28;
                case 29: return S29;
                case 30: return S30;

                case 31: return S31;
                default:
                    return "";
            }
        }

        public void SetSValue(int Day, string value)
        {
            switch (Day)
            {
                case 1: this.S1 = value; break;
                case 2: this.S2 = value; break;
                case 3: this.S3 = value; break;
                case 4: this.S4 = value; break;
                case 5: this.S5 = value; break;
                case 6: this.S6 = value; break;
                case 7: this.S7 = value; break;
                case 8: this.S8 = value; break;
                case 9: this.S9 = value; break;
                case 10: this.S10 = value; break;

                case 11: this.S11 = value; break;
                case 12: this.S12 = value; break;
                case 13: this.S13 = value; break;
                case 14: this.S14 = value; break;
                case 15: this.S15 = value; break;
                case 16: this.S16 = value; break;
                case 17: this.S17 = value; break;
                case 18: this.S18 = value; break;
                case 19: this.S19 = value; break;
                case 20: this.S20 = value; break;

                case 21: this.S21 = value; break;
                case 22: this.S22 = value; break;
                case 23: this.S23 = value; break;
                case 24: this.S24 = value; break;
                case 25: this.S25 = value; break;
                case 26: this.S26 = value; break;
                case 27: this.S27 = value; break;
                case 28: this.S28 = value; break;
                case 29: this.S29 = value; break;
                case 30: this.S30 = value; break;

                case 31: this.S31 = value; break;

            }
        }
    }
}
