﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsMenuMasterInfo:clsMenuBase
    {
        public string MoTa { get; set; }
        public string ChuoiBatDau { get; set; }
        public string KyTuCach { get; set; }
        public int SoChuSo { get; set; }
        public int SoBatDau { get; set; }
        public int SoKetThuc { get; set; }

        public clsMenuMasterInfo() {  }
        public clsMenuMasterInfo(System.Data.DataRow row)
        {
            this.id = row["MenuId"].ToString().Trim();
            this.MenuParent = row["MenuParent"].ToString();
            this.text = row["MenuName"].ToString().Trim();
            this.url = row["Url"].ToString();

            this.PageName = row["PageName"].ToString().Trim();
            this.Sort = (int)row["Sort"];
            this.IsVisible = (bool)row["IsVisible"];
            this.iconCls = row["iconCls"].ToString().Trim();
            this.ContextMenuId = row["ContextMenuId"].ToString();
            this.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();
            this.DefaultUrl = row["DefaultUrl"].ToString();

            this.MoTa = row["MoTa"].ToString();

            this.ChuoiBatDau = row["ChuoiBatDau"].ToString();
            this.KyTuCach = row["KyTuCach"].ToString();
            this.SoChuSo = (int)row["SoChuSo"];
            this.SoBatDau = (int)row["SoBatDau"];
            this.SoKetThuc = (int)row["SoKetThuc"];
            this.MenuType = row["MenuType"].ToString();
        }
    }

    [Serializable]
    public class clsMenuMasterCollection : List<clsMenuMasterInfo>
    {
        public clsMenuMasterCollection() { }

        public clsMenuMasterCollection(List<clsMenuMasterInfo> Items) :
            base(Items)
        {

        }
    }
}
