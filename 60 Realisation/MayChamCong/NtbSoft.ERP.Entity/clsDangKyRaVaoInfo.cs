﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsDangKyRaVaoInfo : Interfaces.IDangKyRaVaoInfo
    {
        public double ID { get; set; }
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string DepName { get; set; }
        public DateTime NgayDangKy { get; set; }
        public string StrNgayDangKy { get { return Libs.clsCommon.ConvertddMMyyy(this.NgayDangKy); } }
        public string StrNgayDangKyDMY { get { return Libs.clsCommon.ConvertMMddyyy(this.NgayDangKy);  } }
        public string GioRa { get; set; }
        public string GioVao { get; set; }
        public string ThoiGian { get; set; }
        public string GhiChu { get; set; } 
        public string DiMuon { get; set; }
        public string VeSom { get; set; }
    }
}
