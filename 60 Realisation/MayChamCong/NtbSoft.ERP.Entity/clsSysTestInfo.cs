﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Table("SYS_TEST")]
    public class clsSysTestInfo
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }
        public string CardId { get; set; }
        public string Name { get; set; }

        public int Value { get; set; }
    }
}
