﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsWebCustomerInfo:Interfaces.IWebCustomerInfo
    {
        public string CusID { get; set; }
        public string CusName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
