﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsShiftInfo:Interfaces.IShiftInfo
    {
        public int Id { get; set; }
        public DateTime TuNgay { get; set; }
        public string TuNgayDMY { get { return Libs.clsCommon.ConvertddMMyyy(this.TuNgay); } }
        public string TuNgayMDY { get { return Libs.clsCommon.ConvertMMddyyy(this.TuNgay); } }
        public string ShiftId { get; set; }
        public string ShiftName { get; set; }

        #region Cặp vào ra 1
        /// <summary>
        /// Vào (giờ bắt đầu của một ca làm việc)
        /// </summary>
        public string TimeIn { get; set; }
        /// <summary>
        /// Ra: Giờ nghỉ giữa ca (nghỉ trưa) 
        /// </summary>
        public string BreakOut { get; set; }

        #endregion

        #region Cặp vào ra 2
        /// <summary>
        /// Vào: Giờ sau khi nghỉ giữa ca(sau nghỉ trưa vào làm việc lại)
        /// </summary>
        public string BreakIn { get; set; }

        /// <summary>
        /// Ra: Giờ kết thúc của một ca làm việc
        /// </summary>
        public string TimeOut { get; set; }
        #endregion

        /// <summary>
        /// Bắt đầu giờ vào để hiểu ca
        /// </summary>
        public string StartIn { get; set; }

        /// <summary>
        /// Kết thúc giờ vào để hiểu ca
        /// </summary>
        public string EndIn { get; set; }


        /// <summary>
        /// Bắt đầu giờ ra để hiểu ca
        /// </summary>
        public string StartOut { get; set; }

        /// <summary>
        /// Kết thúc giờ ra để hiểu ca
        /// </summary>
        public string EndOut { get; set; }
        public string Total { get; set; }

        /// <summary>
        /// Nghỉ giữa giờ
        /// </summary>
        public float TimeOff { get; set; }
        public bool CheckIn { get; set; }
        public bool CheckOut { get; set; }
        public int TotalTime { get; set; }
        public bool IsTinhLamThem { get; set; }
        /// <summary>
        /// True cho phép tăng ca trước giờ làm việc
        /// </summary>
        public bool IsSoPhutTruocCa { get; set; }
        /// <summary>
        /// Số phút sẽ tính tăng ca trước giờ làm việc
        /// </summary>
        public int SoPhutTruocCa { get; set; }
        /// <summary>
        /// True cho phép tăng ca sau giờ làm việc
        /// </summary>
        public bool IsSoPhutSauCa { get; set; }
        /// <summary>
        /// Số phút sẽ bắt đầu tính tăng ca sau giờ làm việc
        /// </summary>
        public int SoPhutSauCa { get; set; }
        public bool IsShiftNight { get; set; }
        public bool IsHanhChinh { get; set; }
        public bool IsCachQuyCong { get; set; }
        public bool Status { get; set; }
        public bool IsTinhAnToi { get; set; }
        public string BatDauAnToi { get; set; }
        public string KetThucAnToi { get; set; }
        public int ThoiGianAnToi { get; set; }

        public string BatDauTinhMuon { get; set; }
        public string KetThucTinhMuon { get; set; }

        public string BatDauTinhSom { get; set; }
        public string KetThucTinhSom { get; set; }
        public int FromMinute { get; set; }
        public float GioHienThi { get; set; }
        public float SoGioDuCong { get; set; }
        public bool IsLoaiCa { get; set; }
        public bool IsHienThiMaCa { get; set; }
        public bool IsBoQuaChuNhat { get; set; }

        public bool IsThaiSan { get; set; }
        public bool IsCaConBu { get; set; }
        public bool IsCaThai7Thang { get; set; }
        public bool IsCaCuoiTuan { get; set; }
        public float CaCuoiTuan { get; set; }
        public bool IsCaNgayLe { get; set; }
        public float CaNgayLe { get; set; }
        public int ChuyenNgay { get; set; }

        public bool IsTongSoPhutLam { get; set; }

        /// <summary>
        /// Tổng số phút đạt đến (n) phút sẽ tính tăng ca. Ví dụ số giờ làm việc là đặt đến 480 (đổi ra giờ là 8h), thì sau đó sẽ được tính tăng ca
        /// </summary>
        public int TongSoPhutLam { get; set; }


        /// <summary>
        /// Giới hạn tối đa tăng ca trước giờ làm việc (phút)
        /// </summary>
        public int MaxBeforeOTX { get; set; }

        /// <summary>
        /// Giới hạn tối đa tăng ca sau giờ làm việc (phút)
        /// </summary>
        public int MaxAfterOTX { get; set; }
    }
}
