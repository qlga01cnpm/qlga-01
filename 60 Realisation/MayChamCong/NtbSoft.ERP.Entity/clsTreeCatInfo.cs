﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsTreeCatInfo : clsTreeNodeBase
    {
        public List<clsTreeCatInfo> children { get; set; }

        public bool IsIn { get; set; }
        public bool IsTheu { get; set; }
        public bool IsGiat { get; set; }

    }
}
