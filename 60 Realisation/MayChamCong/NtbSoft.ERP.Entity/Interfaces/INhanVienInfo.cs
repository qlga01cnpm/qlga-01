﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface INhanVienInfo
    {
        double Id { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string CardID { get; set; }
        string MaCC { get; set; }
        
        DateTime BirthDate { get; set; }
        string StrBirthDate { get; }
        string BirthPlace { get; set; }
        string NguyenQuan { get; set; }
        int Sex { get; set; }
        string StrSex { get; }
        string Address { get; set; }
        string CMND { get; set; }
        string NoiCap { get; set; }
        int PositionID { get; set; }
        string PositionName { get; set; }
        string Telephone { get; set; }
        DateTime BeginningDate { get; set; }
        string StrBeginningDate { get; }
        DateTime NgayKyHopDong { get; set; }
        DateTime StopWorkingday { get; set; }
        string StrStopWorkingday { get; }

        DateTime NgayQuayLai { get; set; }
        DateTime NgayDu18Tuoi { get; set; }
        bool IsVisible { get; set; }
        bool IsNghiViec { get; set; }
        string LyDoNghiViec { get; set; }
        /// <summary>
        /// Lịch trình ca
        /// </summary>
        string SCHName { get; set; }

        /// <summary>
        /// Ca làm việc
        /// </summary>
        string ShiftID { get; set; }
        int STT { get; set; }
        bool IsShift { get; set; }
        string DepID { get; set; }
        string DepName { get; set; }
        string Password { get; set; }
        string UserName { get; set; }

        int LoaiCongViec { get; set; }
        string StrLoaiCongViec { get; }
    }
}
