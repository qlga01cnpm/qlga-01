﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IWebServerCustomerInfo
    {
        string ServerID { get; set; }
        string Name { get; set; }
        string MChine { get; set; }
        string IPLan { get; set; }
        string IPWan { get; set; }
        string Mac { get; set; }
        int Port { get; set; }
        string Url { get; set; }
        string PageName { get; set; }
        DateTime LastUpdated { get; set; }
        string TypeService { get; set; }
        IWebCustomerInfo WebCustomerInfo { get; set; }
    }
    public interface IWebServerCustomer
    {
        IEnumerable<IWebServerCustomerInfo> GetAll();
        List<IWebServerCustomerInfo> GetByCustumer();
        bool Save(List<IWebServerCustomerInfo> Items);
        bool Delete(string Id);
    }
}
