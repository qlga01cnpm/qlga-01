﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDangKyCheDoInfo
    {
        double ID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string DepName { get; set; }
        DateTime TuNgay { get; set; }

        string StrTuNgay { get; }
        string StrTuNgayDMY { get; }

        DateTime ToiNgay { get; set; }
        string StrToiNgay { get; }
        string StrToiNgayDMY { get; }
        string ShiftID { get; set; }
        string LyDo { get; set; }
        float SoNgay { get; set; }
        int ThoiGian { get; set; }
        int MaCheDo { get; set; }
        string StrCheDo { get; }

        string KyHieu {get;set;}

        int SoNgayConLai { get; }
    }
}
