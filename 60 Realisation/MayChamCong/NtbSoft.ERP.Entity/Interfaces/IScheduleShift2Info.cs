﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IScheduleShift2Info
    {
        int Id { get; set; }
        string SchName { get; set; }
        string ShiftName { get; set; }
        /// <summary>
        /// Ngày lễ
        /// </summary>
        bool IsNL { get; set; }
        bool IsCN { get; set; }
        bool IsT2 { get; set; }
        bool IsT3 { get; set; }
        bool IsT4 { get; set; }
        bool IsT5 { get; set; }
        bool IsT6 { get; set; }
        bool IsT7 { get; set; }
    }
}
