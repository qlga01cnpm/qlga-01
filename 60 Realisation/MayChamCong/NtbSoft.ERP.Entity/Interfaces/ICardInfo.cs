﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface ICardInfo
    {
        string CardId { get; set; }
        string CardLabel { get; set; }
        int CardType { get; set; }
        DateTime LastEventTime { get; set; }
    }
}
