﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IQuyTacInfo
    {
        int SoPhutTinhDiMuon { get; set; }
        int SoPhutTinhVeSom { get; set; }
        string SoPhutKhongChamCongVao { get; set; }
        string SoPhutKhongChamCongRa { get; set; }
        int SoPhutMuonTinhVang { get; set; }
        int SoPhutSomTinhVang { get; set; }
        int SoPhutTinhTangCa { get; set; }
        int SoPhutTongTGLam { get; set; }
        int ChonKhongChamCongVao { get; set; }
        int ChonKhongChamCongRa { get; set;}
        int ChonVaoMuonQua { get; set; }
        int ChonVeSomQua { get; set; }
        int ChonVeSau { get; set; }
        int ChonTongTGLam { get; set; }
        int KhongChamCongVaoTinhVang { get; set; }
        int KhongChamCongRaTinhVang { get; set; }
        int SoLanChamCong { get; set; }
        int TinhTGBatDau { get; set; }
        int SoPhutGiuaHaiLan { get; set; }
        bool IsTinhTongTG { get; set; }
        float HeSoOTNT { get; set; }
        float HeSoCN { get; set; }
        float HeSoOTCN { get; set; }
        float HeSoNgayNghi { get; set; }
        float HeSoOTNgayNghi { get; set; }
        float HeSoNgayLe { get; set; }
        float HeSoOTNgayLe { get; set; }
        float BatDauTinhPhepSau { get; set; }
        bool IsCongDonPhep { get; set; }
        int NgayBatDauTinhCong { get; set; }
        string BatDauMuaHe { get; set; }
        string KetThucMuaHe { get; set; }
        string BatDauMuaDong { get; set; }
        string KetThucMuaDong { get; set; }
        bool IsDangKyNgayNghi { get; set; }
        bool IsHuyChanGioBenSA { get; set; }
        bool IsChanDen { get; set; }
        bool IsChanVe { get; set; }
        string MocGioDen { get; set; }
        string MocGioVe { get; set; }
        bool InRaMay { get; set; }
        bool IsChoSAChuanHoa { get; set; }

        List<clsQuyTacLamTronInfo> QuyTacLamTron { get; set; }
        
    }
}
