﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IQLGRPhieuThuTienInfo
    {
        int HoSoId { get; set; }
        DateTime NgayThuTien { get; set; }
        double SoTienThu { get; set; }
        double TongTien { get; set; }
    }
}
