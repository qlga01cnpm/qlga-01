﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface ILyDoVangInfo
    {
        int Id { get; set; }
        string MaSo { get; set; }
        string LyDo { get; set; }
        float Phep { get; set; }
        float BHXH { get; set; }
        float HocHop { get; set; }
        float LeTet { get; set; }
        float KhongLuong { get; set; }
        float CoLuong { get; set; }
        float ThaiSan { get; set; }
        float DieuDuong { get; set; }
        float NghiTuDo { get; set; }
        bool IsHienThi { get; set; }
        bool IsVP { get; set; }
        float NghiBu { get; set; }
    }
}
