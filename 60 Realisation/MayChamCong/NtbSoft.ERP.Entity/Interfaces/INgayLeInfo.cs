﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    /// <summary>
    /// Ngày Lễ
    /// </summary>
    public interface INgayLeInfo
    {
        int Id { get; set; }
        string Name { get; set; }
        DateTime Ngay { get; set; }
        string StrNgay { get; }
    }
}
