﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IQuyTacLamTronInfo
    {
        int Id { get; set; }
        int TuPhut { get; set; }
        int DenPhut { get; set; }
        int LamTron { get; set; }
    }
}
