﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface ITheoDoiPhepNamInfo
    {
        int sMonth { get; set; }
        int sYear { get; set; }
        string eID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string DepName { get; set; }
        string M1 { get; set; }
        string M2 { get; set; }
        string M3 { get; set; }
        string M4 { get; set; }
        string M5 { get; set; }
        string M6 { get; set; }
        string M7 { get; set; }
        string M8 { get; set; }
        string M9 { get; set; }
        string M10 { get; set; }

        string M11 { get; set; }
        string M12 { get; set; }
        string SoNgayPhep { get; set; }
        string SoNgayNghi { get; set; }
        string SoNgayConLai { get; }
        string NgayVao { get; set; }
       
    }
}
