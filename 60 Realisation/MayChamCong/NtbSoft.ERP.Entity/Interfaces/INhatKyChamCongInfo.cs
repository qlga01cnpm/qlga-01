﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface INhatKyChamCongInfo
    {
        int TerminalID { get; set; }
        string MaCC { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }

        /// <summary>
        /// Ngày làm việc
        /// </summary>
        DateTime WorkingDay { get; set; }

        /// <summary>
        /// Get ngày làm vệc
        /// </summary>
        string StrWorkingDay { get; }
        /// <summary>
        /// Thời gian tải
        /// </summary>
        DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Get ngày làm việc
        /// </summary>
        string StrCreateDateTime { get; }
        string GioQuet { get; set; }
        string Door { get; set; }
        
    }
}
