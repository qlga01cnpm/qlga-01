﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public class IDSSinhVienInfo
    {
        string StudentID { get; set; }
        string Name { get; set; }
        int Age { get; set; }
        string Address { get; set; }
        string PhoneNumber { get; set; }
        string Email { get; set; }
    }
}
