﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDangKyLamThemInfo
    {
        double ID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string DepName { get; set; }
        DateTime NgayDangKy { get; set; }
        string StrNgayDangKy { get; }
        string StrNgayDangKyDMY { get; }
        string DenNgay { get; set; }
        string TuGio { get; set; }
        string DenGio { get; set; }
        decimal SoGio { get; set; }
    }
}
