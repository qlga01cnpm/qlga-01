﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    /// <summary>
    /// Lịch trình ca làm việc
    /// </summary>
    public interface IScheduleInfo
    {
        int Id { get; set; }
        string SchName { get; set; }
        string Shift { get; set; }
        int Chuky { get; set; }
        string Descriptions { get; set; }
        
        int NumberShift { get; set; }
        string Shift1 { get; set; }
        string Shift2 { get; set; }
        string Shift3 { get; set; }
        string TimeInS1 { get; set; }
        string TimeInS2 { get; set; }
        string TimeInS3 { get; set; }
    }
}
