﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IQLGRKhachHangInfo
    {
        string KhachHangId { get; set; }
        string TenKhachHang { get; set;}
        string SoDienThoai { get; set; }
        string DiaChi { get; set; }
        string Email { get; set; }
        double TienNo { get; set; }
    }
}
