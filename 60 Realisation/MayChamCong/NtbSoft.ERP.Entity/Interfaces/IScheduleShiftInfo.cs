﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IScheduleShiftInfo
    {
        int Ngay { get; set; }

        /// <summary>
        /// Get
        /// </summary>
        string StrNgay { get; }
        int SchID { get; set; }
        string SchName { get; set; }
        string Shift1 { get; set; }
        string Shift2 { get; set; }
        string Shift3 { get; set; }
        string Shift4 { get; set; }
        string Shift5 { get; set; }
        string Shift6 { get; set; }
        string Shift7 { get; set; }
        string Shift8 { get; set; }
        string Shift9 { get; set; }
        int NumberShift { get; set; }
        string Shift { get; set; }
        string StrShift { get; set; }
        bool IsUse { get; set; }   
    }
}
