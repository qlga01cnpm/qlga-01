﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface ITimeInOutSessionInfo
    {
        double Id { get; set; }
	    DateTime Workingday{get;set;}
        /// <summary>
        /// Get
        /// </summary>
        string StrWorkingDay { get; }
        /// <summary>
        /// Get thứ
        /// </summary>
        string StrThu { get; }
	    string DepID{get;set;}
	    string MaNV{get;set;}
	    string TenNV{get;set;}
	    string TimeIn{get;set;}
	    string BreakOut{get;set;}
	    string BreakIn{get;set;}
	    string TimeOut{get;set;}
	    float OverTime{get;set;}
	    float TotalTime{get;set;}
	    string TotalTimeHM{get;set;}
	    float TotalHours{get;set;}
	    string LyDoVang{get;set;}
	    bool IsChuanHoa{get;set;}
	    string SchName{get;set;}
	    string StrShiftID{get;set;}
	    string Shift{get;set;}
	    string StrTimeIn{get;set;}
	    string StrBreakOut{get;set;}
	    string StrBreakIn{get;set;}
	    string StrTimeOut{get;set;}
	    double TotalTimeMinute{get;set;}

        /// <summary>
        /// Đi muộn
        /// </summary>
	    float LateTime{get;set;}

        /// <summary>
        /// Về sớm
        /// </summary>
	    float EarlyTime{get;set;}
	    bool IsLate{get;set;}
	    bool IsEarly{get;set;}
	    DateTime DateTimeIn{get;set;}
	    DateTime DateBreakOut{get;set;}
	    DateTime DateBreakIn{get;set;}
	    DateTime DateTimeOut{get;set;}
	    float CongK{get;set;}
        string StrCongK { get; }
	    float CongX{get;set;}
	    float OTK{get;set;}
        /// <summary>
        /// Gio tang ca (phút)
        /// </summary>
	    Single OTX{get;set;}
        /// <summary>
        /// Get Giờ tăng cac (giờ)
        /// </summary>
        Single OTXH { get; }
	    float mOTX{get;set;}
	    float mOTK{get;set;}
	    float CongCNK {get;set;}
	    float CongCNX{get;set;}
	    float OTCNK{get;set;}
	    float OTCNX{get;set;}
	    float Cong30P{get;set;}
	    float CongAnTrua{get;set;}
	    float CongAnChieu{get;set;}
	    float CongLe{get;set;}
	    float CongRo{get;set;}
	    float CongR{get;set;}
	    float CongOm{get;set;}
	    float CongConOm{get;set;}
	    float CongPhep{get;set;}
	    float CongKT{get;set;}
	    float CongDe{get;set;}
	    float CongTS{get;set;}
	    float CongNB{get;set;}
	    float CongCD{get;set;}
	    float CongV0{get;set;}
	    float CongP{get;set;}
	    float CongCT{get;set;}
	    float CongThieu{get;set;}
	    float CongH{get;set;}
	    float GioConBu{get;set;}
	    float GioThai7Thang{get;set;}
        bool IsTS { get; set; }
	    float RegNight{get;set;}
	    
    }
}
