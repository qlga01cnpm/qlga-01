﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IQLGRHoSoXeInfo
    {
        int MaHoSo { get; set; }
        string BienSo { get; set; }
        string MaHieuXe { get; set; }
        string KhachHangId { get; set; }
        DateTime NgayTiepNhan { get; set; }
        string TienDo { get; set; }
    }
}
