﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IWebCustomerInfo
    {
        string CusID { get; set; }
        string CusName { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
    }

    public interface IWebCustomer
    {
        List<IWebCustomerInfo> GetAll();
        IWebCustomerInfo GetBy(string Id);
        bool Save(List<IWebCustomerInfo> pWebCustomer);

        bool Delete(string Id);
    }
}
