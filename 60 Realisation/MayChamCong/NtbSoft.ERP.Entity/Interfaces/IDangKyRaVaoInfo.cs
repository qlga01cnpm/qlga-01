﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDangKyRaVaoInfo
    {
        double ID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string DepName { get; set; }
        DateTime NgayDangKy { get; set; }
        string StrNgayDangKy { get; }
        string StrNgayDangKyDMY { get; }
        string GioRa { get; set; }
        string GioVao { get; set; }
        string ThoiGian { get; set; }
        string GhiChu { get; set; }

        string DiMuon { get; set; }
        string VeSom { get; set; }



    }
}
