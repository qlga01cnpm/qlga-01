﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDatabase
    {
        string ConnectionString { get; }
        int OpenConnection();
        int CloseConnection();
        int ExecuteSqlCommand(); 
    }
}
