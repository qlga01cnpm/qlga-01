﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IPhieuSuaChuaInfo
    {
        int HoSoId { get; set; }
        DateTime NgaySuaChua { get; set; }
        int PhuTungId { get; set; }
        int TienCongId { get; set; }
        int SoLuong { get; set; }
        string NoiDung { get; set; }
    }
}
