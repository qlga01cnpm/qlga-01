﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDangKyChanGioInfo
    {
        double AutoID { get; set; }
        int sMonth { get; set; }
        int sYear { get; set; }
        string eID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string GioVe { get; set; }
        string D1 { get; set; }
        string D2 { get; set; }
        string D3 { get; set; }
        string D4 { get; set; }
        string D5 { get; set; }
        string D6 { get; set; }
        string D7 { get; set; }
        string D8 { get; set; }
        string D9 { get; set; }
        string D10 { get; set; }

        string D11 { get; set; }
        string D12 { get; set; }
        string D13 { get; set; }
        string D14 { get; set; }
        string D15 { get; set; }
        string D16 { get; set; }
        string D17 { get; set; }
        string D18 { get; set; }
        string D19 { get; set; }
        string D20 { get; set; }
        string D21 { get; set; }
        string D22 { get; set; }
        string D23 { get; set; }
        string D24 { get; set; }
        string D25 { get; set; }
        string D26 { get; set; }
        string D27 { get; set; }
        string D28 { get; set; }
        string D29 { get; set; }
        string D30 { get; set; }
        string D31 { get; set; }

        string GetValue(int Day);
        void SetValue(int Day, string value);
        void SetValueAll( string value);
    }
}
