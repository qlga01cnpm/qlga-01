﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
   public interface ITimeSheetInfo
    {
       string BeginningDate { get; set; } 
    int Month {get;set;}
	int Year {get;set;}
	string    MaNV {get;set;}
	string TenNV {get;set;}
	string DepID{get;set;}
	string DepName{get;set;}
	decimal StartWorkingday {get;set;}
	decimal StopWorkingday{get;set;}
	string I1{get;set;}
	string I2{get;set;}
	string I3{get;set;}
	string I4{get;set;}
	string I5{get;set;}
	string I6{get;set;}
	string I7{get;set;}
	string I8{get;set;}
	string I9{get;set;}
	string I10{get;set;}
	string I11{get;set;}
	string I12{get;set;}
	string I13{get;set;}
	string I14{get;set;}
	string I15{get;set;}
	string I16{get;set;}
	string I17{get;set;}
	string I18{get;set;}
	string I19{get;set;}
	string I20{get;set;}
	string I21{get;set;}
	string I22{get;set;}
	string I23{get;set;}
	string I24{get;set;}
	string I25{get;set;}
	string I26{get;set;}
	string I27{get;set;}
	string I28{get;set;}
	string I29{get;set;}
	string I30{get;set;}
	string I31{get;set;}
	string I_1{get;set;}
	string I_2{get;set;}
	string I_3{get;set;}
	string I_4{get;set;}
	string I_5{get;set;}
	string I_6{get;set;}
	string I_7{get;set;}
	string I_8{get;set;}
	string I_9{get;set;}
	string I_10{get;set;}
	string I_11{get;set;}
	string I_12{get;set;}
	string I_13{get;set;}
	string I_14{get;set;}
	string I_15{get;set;}
	string I_16{get;set;}
	string I_17{get;set;}
	string I_18{get;set;}
	string I_19{get;set;}
	string I_20{get;set;}
	string I_21{get;set;}
	string I_22{get;set;}
	string I_23{get;set;}
	string I_24{get;set;}
	string I_25{get;set;}
	string I_26{get;set;}
	string I_27{get;set;}
	string I_28{get;set;}
	string I_29{get;set;}
	string I_30{get;set;}
	string I_31{get;set;}
	string O1{get;set;}
	string O2{get;set;}
	string O3{get;set;}
	string O4{get;set;}
	string O5{get;set;}
	string O6{get;set;}
	string O7{get;set;}
	string O8{get;set;}
	string O9{get;set;}
	string O10{get;set;}
	string O11{get;set;}
	string O12{get;set;}
	string O13{get;set;}
	string O14{get;set;}
	string O15{get;set;}
	string O16{get;set;}
	string O17{get;set;}
	string O18{get;set;}
	string O19{get;set;}
	string O20{get;set;}
	string O21{get;set;}
	string O22{get;set;}
	string O23{get;set;}
	string O24{get;set;}
	string O25{get;set;}
	string O26{get;set;}
	string O27{get;set;}
	string O28{get;set;}
	string O29{get;set;}
	string O30{get;set;}
	string O31{get;set;}
	string O_1{get;set;}
	string O_2{get;set;}
	string O_3{get;set;}
	string O_4{get;set;}
	string O_5{get;set;}
	string O_6{get;set;}
	string O_7{get;set;}
	string O_8{get;set;}
	string O_9{get;set;}
	string O_10{get;set;}
	string O_11{get;set;}
	string O_12{get;set;}
	string O_13{get;set;}
	string O_14{get;set;}
	string O_15{get;set;}
	string O_16{get;set;}
	string O_17{get;set;}
	string O_18{get;set;}
	string O_19{get;set;}
	string O_20{get;set;}
	string O_21{get;set;}
	string O_22{get;set;}
	string O_23{get;set;}
	string O_24{get;set;}
	string O_25{get;set;}
	string O_26{get;set;}
	string O_27{get;set;}
	string O_28{get;set;}
	string O_29{get;set;}
	string O_30{get;set;}
	string O_31{get;set;}
	string C1{get;set;}
	string C2{get;set;}
	string C3{get;set;}
	string C4{get;set;}
	string C5{get;set;}
	string C6{get;set;}
	string C7{get;set;}
	string C8{get;set;}
	string C9{get;set;}
	string C10{get;set;}
	string C11{get;set;}
	string C12{get;set;}
	string C13{get;set;}
	string C14{get;set;}
	string C15{get;set;}
	string C16{get;set;}
	string C17{get;set;}
	string C18{get;set;}
	string C19{get;set;}
	string C20{get;set;}
	string C21{get;set;}
	string C22{get;set;}
	string C23{get;set;}
	string C24{get;set;}
	string C25{get;set;}
	string C26{get;set;}
	string C27{get;set;}
	string C28{get;set;}
	string C29{get;set;}
	string C30{get;set;}
	string C31{get;set;}
	string T1{get;set;}
	string T2{get;set;}
	string T3{get;set;}
	string T4{get;set;}
	string T5{get;set;}
	string T6{get;set;}
	string T7{get;set;}
	string T8{get;set;}
	string T9{get;set;}
	string T10{get;set;}
	string T11{get;set;}
	string T12{get;set;}
	string T13{get;set;}
	string T14{get;set;}
	string T15{get;set;}
	string T16{get;set;}
	string T17{get;set;}
	string T18{get;set;}
	string T19{get;set;}
	string T20{get;set;}
	string T21{get;set;}
	string T22{get;set;}
	string T23{get;set;}
	string T24{get;set;}
	string T25{get;set;}
	string T26{get;set;}
	string T27{get;set;}
	string T28{get;set;}
	string T29{get;set;}
	string T30{get;set;}
	string T31{get;set;}
	decimal TotalHours{get;set;}
	decimal SoGioCong{get;set;}
	decimal PCXangXe{get;set;}
	decimal PCChuyenCan{get;set;}
	decimal PCThamNien{get;set;}
	decimal NghiCoHuongLuong{get;set;}
	decimal CongK{get;set;}
	decimal CongX{get;set;}
	decimal CongLe{get;set;}
	decimal CongPhep{get;set;}
	decimal CongThieu{get;set;}
	decimal Cong30P{get;set;}
	decimal CongAnTrua{get;set;}
	decimal CongAnChieu {get;set;}
	decimal SoLanDiMuon{get;set;}
	decimal SoLanVeSom{get;set;}
	decimal TotalEarlyTime{get;set;}
	decimal TotalLateTime{get;set;}
	string K1 {get;set;}
	string K2 {get;set;}
	string K3 {get;set;}
	string K4 {get;set;}
	string K5 {get;set;}
	string K6 {get;set;}
	string K7 {get;set;}
	string K8 {get;set;}
	string K9 {get;set;}
	string K10 {get;set;}
	string K11 {get;set;}
	string K12 {get;set;}
	string K13 {get;set;}
	string K14 {get;set;}
	string K15 {get;set;}
	string K16 {get;set;}
	string K17 {get;set;}
	string K18 {get;set;}
	string K19 {get;set;}
	string K20 {get;set;}
	string K21 {get;set;}
	string K22 {get;set;}
	string K23 {get;set;}
	string K24 {get;set;}
	string K25 {get;set;}
	string K26 {get;set;}
	string K27 {get;set;}
	string K28 {get;set;}
	string K29 {get;set;}
	string K30 {get;set;}
	string K31 {get;set;}
	string OT1 {get;set;}
	string OT2 {get;set;}
	string OT3 {get;set;}
	string OT4 {get;set;}
	string OT5 {get;set;}
	string OT6 {get;set;}
	string OT7 {get;set;}
	string OT8 {get;set;}
	string OT9 {get;set;}
    string OT10 { get; set; }
    string OT11 { get; set; }
    string OT12 { get; set; }
    string OT13 { get; set; }
    string OT14 { get; set; }
    string OT15 { get; set; }
    string OT16 { get; set; }
    string OT17 { get; set; }
    string OT18 { get; set; }
    string OT19 { get; set; }
    string OT20 { get; set; }
    string OT21 { get; set; }
    string OT22 { get; set; }
    string OT23 { get; set; }
    string OT24 { get; set; }
    string OT25 { get; set; }
    string OT26 { get; set; }
    string OT27 { get; set; }
    string OT28 { get; set; }
    string OT29 { get; set; }
    string OT30 { get; set; }
    string OT31 { get; set; }
    }
}
