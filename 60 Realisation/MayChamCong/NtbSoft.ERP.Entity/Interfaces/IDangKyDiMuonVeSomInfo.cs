﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IDangKyDiMuonVeSomInfo
    {
        int sMonth { get; set; }
        int sYear { get; set; }
        string eID { get; set; }
        string MaNV { get; set; }
        string TenNV { get; set; }
        string SCHName { get; set; }
        string ShiftID { get; set; }
        string M1 { get; set; }
        string M2 { get; set; }
        string M3 { get; set; }
        string M4 { get; set; }
        string M5 { get; set; }
        string M6 { get; set; }
        string M7 { get; set; }
        string M8 { get; set; }
        string M9 { get; set; }
        string M10 { get; set; }

        string M11 { get; set; }
        string M12 { get; set; }
        string M13 { get; set; }
        string M14 { get; set; }
        string M15 { get; set; }
        string M16 { get; set; }
        string M17 { get; set; }
        string M18 { get; set; }
        string M19 { get; set; }
        string M20 { get; set; }
        string M21 { get; set; }
        string M22 { get; set; }
        string M23 { get; set; }
        string M24 { get; set; }
        string M25 { get; set; }
        string M26 { get; set; }
        string M27 { get; set; }
        string M28 { get; set; }
        string M29 { get; set; }
        string M30 { get; set; }
        string M31 { get; set; }




        string S1 { get; set; }
        string S2 { get; set; }
        string S3 { get; set; }
        string S4 { get; set; }
        string S5 { get; set; }
        string S6 { get; set; }
        string S7 { get; set; }
        string S8 { get; set; }
        string S9 { get; set; }
        string S10 { get; set; }

        string S11 { get; set; }
        string S12 { get; set; }
        string S13 { get; set; }
        string S14 { get; set; }
        string S15 { get; set; }
        string S16 { get; set; }
        string S17 { get; set; }
        string S18 { get; set; }
        string S19 { get; set; }
        string S20 { get; set; }
        string S21 { get; set; }
        string S22 { get; set; }
        string S23 { get; set; }
        string S24 { get; set; }
        string S25 { get; set; }
        string S26 { get; set; }
        string S27 { get; set; }
        string S28 { get; set; }
        string S29 { get; set; }
        string S30 { get; set; }
        string S31 { get; set; }

        string GetMValue(int Day);
        void SetMValue(int Day, string value);

        string GetSValue(int Day);
        void SetSValue(int Day, string value);
    }
}
