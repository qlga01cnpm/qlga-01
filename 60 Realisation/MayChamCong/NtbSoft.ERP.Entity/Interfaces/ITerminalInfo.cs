﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface ITerminalInfo
    {
        int TerminalID { get; set; }
        string Name { get; set; }
        string Connection { get; set; }
        string IpAddress { get; set; }
        int Port { get; set; }
        int BaudRate { get; set; }
        bool Status { get; set; }
        DateTime LastTimeGetData { get; set; }
        string StrTimeGetData { get; }
        int RecordReaded { get; set; }
        string StrStatus { get; }
        int CommType { get; set; }
        DateTime LastRecordDate { get; set; }
        string StrLastRecordDate { get; }

        string ActiveKey { get; set; }
        string SerialNumber { get; set; }
        string UserCode { get; set; }
        bool IsActived { get; set; }

    }
}
