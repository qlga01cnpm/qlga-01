﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity.Interfaces
{
    public interface IShiftInfo
    {
        int Id { get; set;}
        DateTime TuNgay { get; set; }
        string ShiftId { get; set; }
        string ShiftName { get; set; }

        #region Cặp vào ra 1
        /// <summary>
        /// Vào (giờ bắt đầu của một ca làm việc)
        /// </summary>
        string TimeIn { get; set; }
        /// <summary>
        /// Ra: Giờ nghỉ giữa ca (nghỉ trưa) 
        /// </summary>
        string BreakOut { get; set; }

        #endregion

        #region Cặp vào ra 2
        /// <summary>
        /// Vào: Giờ sau khi nghỉ giữa ca(sau nghỉ trưa)
        /// </summary>
        string BreakIn { get; set; }

        /// <summary>
        /// Ra: Giờ kết thúc của một ca làm việc
        /// </summary>
        string TimeOut { get; set; }
        #endregion

        /// <summary>
        /// Bắt đầu giờ vào để hiểu ca
        /// </summary>
        string StartIn { get; set; }

        /// <summary>
        /// Kết thúc giờ vào để hiểu ca
        /// </summary>
        string EndIn { get; set; }


        /// <summary>
        /// Bắt đầu giờ ra để hiểu ca
        /// </summary>
        string StartOut { get; set; }

        /// <summary>
        /// Kết thúc giờ ra để hiểu ca
        /// </summary>
        string EndOut { get; set; }
        string Total { get; set; }
        
        /// <summary>
        /// Nghỉ giữa giờ
        /// </summary>
        float TimeOff { get; set; }
        bool CheckIn { get; set; }
        bool CheckOut { get; set; }
        int TotalTime { get; set; }
        bool IsTinhLamThem { get; set; }

        /// <summary>
        /// True cho phép tăng ca trước giờ làm việc
        /// </summary>
        bool IsSoPhutTruocCa { get; set; }

        /// <summary>
        /// Số phút sẽ tính tăng ca trước giờ làm việc
        /// </summary>
        int SoPhutTruocCa { get; set; }

        /// <summary>
        /// True cho phép tăng ca sau giờ làm việc
        /// </summary>
        bool IsSoPhutSauCa { get; set; }
        /// <summary>
        /// Số phút sẽ bắt đầu tính tăng ca sau giờ làm việc
        /// </summary>
        int SoPhutSauCa { get; set; }
        bool IsShiftNight { get; set; }
        bool IsHanhChinh { get; set; }
        bool IsCachQuyCong { get; set; }
        bool Status { get; set; }
        bool IsTinhAnToi { get; set; }
        string BatDauAnToi { get; set; }
        string KetThucAnToi { get; set; }
        int ThoiGianAnToi { get; set; }

        string BatDauTinhMuon { get; set; }
        string KetThucTinhMuon { get; set; }

        string BatDauTinhSom { get; set; }
        string KetThucTinhSom { get; set; }
        int FromMinute { get; set; }
        float GioHienThi { get; set; }
        float SoGioDuCong { get; set; }
        bool IsLoaiCa { get; set; }
        bool IsHienThiMaCa { get; set; }
        bool IsBoQuaChuNhat { get; set; }

        bool IsThaiSan { get; set; }
        bool IsCaConBu { get; set; }
        bool IsCaThai7Thang { get; set; }
        bool IsCaCuoiTuan { get; set; }
        float CaCuoiTuan { get; set; }
        bool IsCaNgayLe { get; set; }
        float CaNgayLe { get; set; }
        int ChuyenNgay { get; set; }

        bool IsTongSoPhutLam { get; set; }

        /// <summary>
        /// Tổng số phút đạt đến (n) phút sẽ tính tăng ca. Ví dụ số giờ làm việc là đặt đến 480 (đổi ra giờ là 8h), thì sau đó sẽ được tính tăng ca
        /// </summary>
        int TongSoPhutLam { get; set; }

        /// <summary>
        /// Giới hạn tối đa tăng ca trước giờ làm việc (phút)
        /// </summary>
        int MaxBeforeOTX { get; set; }

        /// <summary>
        /// Giới hạn tối đa tăng ca sau giờ làm việc (phút)
        /// </summary>
        int MaxAfterOTX { get; set; }

    }
}
