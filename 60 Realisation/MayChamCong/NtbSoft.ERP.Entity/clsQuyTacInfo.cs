﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsQuyTacInfo:Interfaces.IQuyTacInfo
    {
        public clsQuyTacInfo() {
        }
        public int SoPhutTinhDiMuon { get; set; }

        public int SoPhutTinhVeSom { get; set; }

        public string SoPhutKhongChamCongVao { get; set; }

        public string SoPhutKhongChamCongRa { get; set; }

        public int SoPhutMuonTinhVang { get; set; }

        public int SoPhutSomTinhVang { get; set; }

        public int SoPhutTinhTangCa { get; set; }

        public int SoPhutTongTGLam { get; set; }

        public int ChonKhongChamCongVao { get; set; }

        public int ChonKhongChamCongRa { get; set; }

        public int ChonVaoMuonQua { get; set; }

        public int ChonVeSomQua { get; set; }

        public int ChonVeSau { get; set; }

        public int ChonTongTGLam { get; set; }

        public int KhongChamCongVaoTinhVang { get; set; }

        public int KhongChamCongRaTinhVang { get; set; }

        public int SoLanChamCong { get; set; }

        public int TinhTGBatDau { get; set; }

        public int SoPhutGiuaHaiLan { get; set; }

        public bool IsTinhTongTG { get; set; }

        public float HeSoOTNT { get; set; }

        public float HeSoCN { get; set; }

        public float HeSoOTCN { get; set; }

        public float HeSoNgayNghi { get; set; }

        public float HeSoOTNgayNghi { get; set; }

        public float HeSoNgayLe { get; set; }

        public float HeSoOTNgayLe { get; set; }

        public float BatDauTinhPhepSau { get; set; }

        public bool IsCongDonPhep { get; set; }

        public int NgayBatDauTinhCong { get; set; }

        public string BatDauMuaHe { get; set; }

        public string KetThucMuaHe { get; set; }

        public string BatDauMuaDong { get; set; }

        public string KetThucMuaDong { get; set; }

        public bool IsDangKyNgayNghi { get; set; }

        public bool IsHuyChanGioBenSA { get; set; }

        public bool IsChanDen { get; set; }

        public bool IsChanVe { get; set; }

        public string MocGioDen { get; set; }

        public string MocGioVe { get; set; }

        public bool InRaMay { get; set; }

        public bool IsChoSAChuanHoa { get; set; }

        public List<clsQuyTacLamTronInfo> QuyTacLamTron { get; set; }
    }
}
