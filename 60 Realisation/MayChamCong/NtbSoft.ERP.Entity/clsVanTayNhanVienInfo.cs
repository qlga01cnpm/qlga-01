﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsVanTayNhanVienInfo:Entities.Interfaces.MCC.IUserFingerInfo
    {

        public string UserId { get; set; }

        public string UserName { get; set; }
        public string CardID { get; set; }

        public int FingerIndex { get; set; }

        public string TmpData { get; set; }

        public int Privilege { get; set; }
        public string StrPrivilege { get { if (this.Privilege == 1)return ""; if (this.Privilege == 2)return "Quản trị"; if (this.Privilege == 3)return "Quản trị"; return "Người dùng"; } }

        public string Password { get; set; }

        public bool Enabled { get; set; }

        public int Flag { get; set; }

        public DateTime DateInOut { get; set; }

        public int MachineNumber { get; set; }

        /// <summary>
        /// Get
        /// </summary>
        public string StrDate
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        public string StrTime
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("HH:mm");
            }
        }

        public string StrDateInOut
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("yyyy/MM/dd HH:mm");
            }
        }

        public int InOutMode { get; set; }
        public int BackupNumber { get; set; }
        public bool IsPassword { get; set; }
        public string TenNV { get; set; }

        /// <summary>
        /// 
        /// 0: không có dấu vân tay
        /// 1: có dấu vân tay
        /// </summary>
        public int FingerIndex0 { get; set; }

        public int FingerIndex1 { get; set; }
        public int FingerIndex2 { get; set; }
        public int FingerIndex3 { get; set; }
        public int FingerIndex4 { get; set; }
        public int FingerIndex5 { get; set; }
        public int FingerIndex6 { get; set; }
        public int FingerIndex7 { get; set; }
        public int FingerIndex8 { get; set; }
        public int FingerIndex9 { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class clsVanTayNhanVienDataInfo : Entities.Interfaces.MCC.IUserFingerprintInfo
    {
        public string UserId { get; set; }

        public string UserName { get; set; }
        public string CardID { get; set; }

        public int FingerIndex { get; set; }

        public string TmpData { get; set; }

        public int Privilege { get; set; }
        public string StrPrivilege { get { if (this.Privilege == 1)return ""; if (this.Privilege == 2)return "Quản trị"; if (this.Privilege == 3)return "Quản trị"; return "Người dùng"; } }

        public string Password { get; set; }

        public bool Enabled { get; set; }

        public int Flag { get; set; }

        public DateTime DateInOut { get; set; }

        public int MachineNumber { get; set; }

        /// <summary>
        /// Get
        /// </summary>
        public string StrDate
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        public string StrTime
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("HH:mm");
            }
        }

        public string StrDateInOut
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("yyyy/MM/dd HH:mm");
            }
        }

        public int InOutMode { get; set; }
        public int BackupNumber { get; set; }
        public bool IsPassword { get; set; }

        public string sTmpData0 { get; set; }
        public string sTmpData1 { get; set; }
        public string sTmpData2 { get; set; }
        public string sTmpData3 { get; set; }
        public string sTmpData4 { get; set; }
        public string sTmpData5 { get; set; }
        public string sTmpData6 { get; set; }
        public string sTmpData7 { get; set; }
        public string sTmpData8 { get; set; }
        public string sTmpData9 { get; set; }
    }
}
