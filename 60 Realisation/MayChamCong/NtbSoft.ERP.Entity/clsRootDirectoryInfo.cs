﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsRootDirectoryInfo:clsMenuBase
    {
    }

    [Serializable]
    public class clsRootDirectoryCollection : List<clsRootDirectoryInfo>
    {
        public clsRootDirectoryCollection() { }

        public clsRootDirectoryCollection(List<clsRootDirectoryInfo> Items)
            : base(Items)
        {

        }
    }
}
