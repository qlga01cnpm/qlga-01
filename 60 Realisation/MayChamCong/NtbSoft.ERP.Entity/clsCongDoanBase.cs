﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsCongDoanBase
    {
        public string Id_Dm { get; set; }
        public string MenuId { get; set; }
        public string MaSo { get; set; }
        public string TenCongDoan { get; set; }
        public double SoPhienBan { get; set; }
        public int TrangThai { get; set; }
        public bool DangKhoa { get; set; }
        public string MoTa { get; set; }


        public float ChiPhiCongDoan { get; set; }
        public float TongThoiGian { get; set; }
        public string NguoiTao { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiCapNhat { get; set; }
        public DateTime NgayCapNhat { get; set; }

    }
}
