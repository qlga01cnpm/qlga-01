﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    [Serializable]
    public class clsTreeMenuInfo:clsTreeNodeBase
    {
        public bool AllowView { get; set; }
        public bool AllowAdd { get; set; }
        public bool AllowEdit { get; set; }
        public bool AlowDelete { get; set; }
        public List<clsTreeMenuInfo> children { get; set; }

        public clsTreeMenuInfo() { }
        public clsTreeMenuInfo(string id, bool AllowView, bool AllowAdd, bool AllowEdit, bool AlowDelete)
        {
            this.id = id; this.AllowView = AllowView;
            this.AllowAdd = AllowAdd;
            this.AllowEdit = AllowEdit;
            this.AlowDelete = AlowDelete;
        }
    }

    [Serializable]
    public class clsTreeMenuCollection : List<clsTreeMenuInfo> { }
}
