﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsTerminalInfo:Interfaces.ITerminalInfo
    {
        public int TerminalID { get; set; }
        public string Name { get; set; }
        private string _Connection;
        public string Connection
        {
            get
            {
                if (this.CommType == 1) return "COM";
                if (this.CommType == 2) return "Ethernet";
                if (this.CommType == 3) return "Host";
                return "";
            }
            set { this._Connection = value; }
        }

        public string IpAddress { get; set; }

        public int Port { get; set; }

        public int BaudRate { get; set; }

        public bool Status { get; set; }

        public DateTime LastTimeGetData { get; set; }

        public int RecordReaded { get; set; }

        public string StrStatus
        {
            get
            {
                if (this.Status) return "OK";
                return "Fail";
            }
        }
        public int CommType { get; set; }


        public string StrTimeGetData
        {
            get
            {
                if (this.LastTimeGetData.ToString("MM/dd/yyyy").Equals("01/01/0001")) return "";
                return this.LastTimeGetData.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }


        public DateTime LastRecordDate { get; set; }

        public string StrLastRecordDate
        {
            get
            {
                if (this.LastRecordDate.ToString("MM/dd/yyyy").Equals("01/01/0001")) return "";
                return this.LastRecordDate.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public string ActiveKey { get; set; }
        public string SerialNumber { get; set; }
        public string UserCode { get; set; }

        public bool IsActived { get; set; }
    }
}
