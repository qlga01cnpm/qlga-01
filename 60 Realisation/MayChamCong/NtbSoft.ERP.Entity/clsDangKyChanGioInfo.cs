﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsDangKyChanGioInfo : Interfaces.IDangKyChanGioInfo
    {
        public double AutoID { get; set; }

        public int sMonth { get; set; }

        public int sYear { get; set; }

        public string eID { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public string GioVe { get; set; }

        public string D1 { get; set; }

        public string D2 { get; set; }

        public string D3 { get; set; }

        public string D4 { get; set; }

        public string D5 { get; set; }

        public string D6 { get; set; }

        public string D7 { get; set; }

        public string D8 { get; set; }

        public string D9 { get; set; }

        public string D10 { get; set; }

        public string D11 { get; set; }

        public string D12 { get; set; }

        public string D13 { get; set; }

        public string D14 { get; set; }

        public string D15 { get; set; }

        public string D16 { get; set; }

        public string D17 { get; set; }

        public string D18 { get; set; }

        public string D19 { get; set; }

        public string D20 { get; set; }

        public string D21 { get; set; }

        public string D22 { get; set; }

        public string D23 { get; set; }

        public string D24 { get; set; }

        public string D25 { get; set; }

        public string D26 { get; set; }

        public string D27 { get; set; }

        public string D28 { get; set; }

        public string D29 { get; set; }

        public string D30 { get; set; }

        public string D31 { get; set; }


        public string GetValue(int Day)
        {
            switch (Day)
            {
                case 1: return D1;
                case 2: return D2;
                case 3: return D3;
                case 4: return D4;
                case 5: return D5;
                case 6: return D6;
                case 7: return D7;
                case 8: return D8;
                case 9: return D9;
                case 10: return D10;

                case 11: return D11;
                case 12: return D12;
                case 13: return D13;
                case 14: return D14;
                case 15: return D15;
                case 16: return D16;
                case 17: return D17;
                case 18: return D18;
                case 19: return D19;
                case 20: return D20;

                case 21: return D21;
                case 22: return D22;
                case 23: return D23;
                case 24: return D24;
                case 25: return D25;
                case 26: return D26;
                case 27: return D27;
                case 28: return D28;
                case 29: return D29;
                case 30: return D30;

                case 31: return D31;
                default:
                    return "";
            }
        }

        public void SetValue(int Day, string value)
        {
            switch (Day)
            {
                case 1: this.D1 = value; break;
                case 2: this.D2 = value; break;
                case 3: this.D3 = value; break;
                case 4: this.D4 = value; break;
                case 5: this.D5 = value; break;
                case 6: this.D6 = value; break;
                case 7: this.D7 = value; break;
                case 8: this.D8 = value; break;
                case 9: this.D9 = value; break;
                case 10: this.D10 = value; break;

                case 11: this.D11 = value; break;
                case 12: this.D12 = value; break;
                case 13: this.D13 = value; break;
                case 14: this.D14 = value; break;
                case 15: this.D15 = value; break;
                case 16: this.D16 = value; break;
                case 17: this.D17 = value; break;
                case 18: this.D18 = value; break;
                case 19: this.D19 = value; break;
                case 20: this.D20 = value; break;

                case 21: this.D21 = value; break;
                case 22: this.D22 = value; break;
                case 23: this.D23 = value; break;
                case 24: this.D24 = value; break;
                case 25: this.D25 = value; break;
                case 26: this.D26 = value; break;
                case 27: this.D27 = value; break;
                case 28: this.D28 = value; break;
                case 29: this.D29 = value; break;
                case 30: this.D30 = value; break;

                case 31: this.D31 = value; break;
               
            }
        }

        public void SetValueAll(string value)
        {
            this.D2 = value;
            this.D3 = value;
            this.D4 = value;
            this.D5 = value;
            this.D6 = value;
            this.D7 = value;
            this.D8 = value;
            this.D9 = value;
            this.D10 = value;

            this.D11 = value;
            this.D12 = value;
            this.D13 = value;
            this.D14 = value;
            this.D15 = value;
            this.D16 = value;
            this.D17 = value;
            this.D18 = value;
            this.D19 = value;
            this.D20 = value;

            this.D21 = value;
            this.D22 = value;
            this.D23 = value;
            this.D24 = value;
            this.D25 = value;
            this.D26 = value;
            this.D27 = value;
            this.D28 = value;
            this.D29 = value;
            this.D30 = value;

            this.D31 = value;

        }
    }
}
