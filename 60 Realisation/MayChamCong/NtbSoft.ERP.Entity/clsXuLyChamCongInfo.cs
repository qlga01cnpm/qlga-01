﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Entity
{
    public class clsXuLyChamCongInfo : Interfaces.IXuLyChamCongInfo
    {
        public double Id { get; set; }
        public int TerminalID { get; set; }

        public string MaCC { get; set; }

        public string MaNV { get; set; }

        public string TenNV { get; set; }

        public DateTime WorkingDay { get; set; }

        public string StrWorkingDay
        {
            get { return Libs.clsCommon.ConvertddMMyyy(this.WorkingDay); }
        }
        public string StrWorkingDayMDY
        {
            get { return Libs.clsCommon.ConvertMMddyyy(this.WorkingDay); }
        }

        private string _StrThu="";
        public string StrThu
        {
            get
            {
                if (string.IsNullOrEmpty(_StrThu))
                    _StrThu = Libs.clsCommon.GetDayOfWeekVn(WorkingDay);

                return _StrThu;
            }
        }
        public DateTime CreateDateTime { get; set; }

        public string StrCreateDateTime
        {
            get { return Libs.clsCommon.ConvertddMMyyyHHmmss(this.CreateDateTime); }
        }

        public string Gio { get; set; }

        public string Door { get; set; }
        public bool IsHopLe { get; set; }
    }
}
