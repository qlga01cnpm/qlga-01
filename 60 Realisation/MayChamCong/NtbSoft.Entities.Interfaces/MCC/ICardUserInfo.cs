﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Entities.Interfaces.MCC
{
    /// <summary>
    /// Chứa thông tin người dùng
    /// </summary>
    public interface ICardUserInfo
    {
        string UserId { get; set; }
        string UserName { get; set; }
        string CardID { get; set; }
        int FingerIndex { get; set; }
        string TmpData { get; set; }
        /// <summary>
        /// Ưu tiên.
        /// 0: Common user
        /// 3: Super administrator
        /// </summary>
        int Privilege { get; set; }
        string StrPrivilege { get; }
        int InOutMode { get; set; }
        string Password { get; set; }
        bool Enabled { get; set; }
        int Flag { get; set; }
        DateTime DateInOut { get; set; }
        int MachineNumber { get; set; }
        string StrDate { get; }
        string StrTime { get; }
        string StrDateInOut { get; }
        int BackupNumber { get; set; }
        bool IsPassword { get; set; }
    }
}
