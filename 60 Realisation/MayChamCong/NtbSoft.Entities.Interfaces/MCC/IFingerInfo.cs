﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Entities.Interfaces.MCC
{
    /// <summary>
    /// Lớp mẫu đảm nhiệm chức năng chứa thông tin thứ tự các ngón tay (Finger Index)
    /// </summary>
    public interface IFingerInfo
    {
        /// <summary>
        /// 
        /// 0: không có dấu vân tay
        /// 1: có dấu vân tay
        /// </summary>
        int FingerIndex0 { get; set; }

        int FingerIndex1 { get; set; }
        int FingerIndex2 { get; set; }
        int FingerIndex3 { get; set; }
        int FingerIndex4 { get; set; }
        int FingerIndex5 { get; set; }
        int FingerIndex6 { get; set; }
        int FingerIndex7 { get; set; }
        int FingerIndex8 { get; set; }
        int FingerIndex9 { get; set; }
    }
}
