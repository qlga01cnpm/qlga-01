﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Entities.Interfaces.MCC
{
    /// <summary>
    /// Thông tin người dùng và dấu vân tay
    /// </summary>
    public interface IUserFingerprintInfo : ICardUserInfo, IFingerprintInfo
    {
    }
}
