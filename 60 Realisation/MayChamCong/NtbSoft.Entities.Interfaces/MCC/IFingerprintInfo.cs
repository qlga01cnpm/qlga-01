﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Entities.Interfaces.MCC
{
    /// <summary>
    /// Thông tin dữ liệu (data) dấu vân tay (Fingerprint)
    /// </summary>
    public interface IFingerprintInfo
    {
        string sTmpData0 { get; set; }
        string sTmpData1 { get; set; }
        string sTmpData2 { get; set; }
        string sTmpData3 { get; set; }
        string sTmpData4 { get; set; }
        string sTmpData5 { get; set; }
        string sTmpData6 { get; set; }
        string sTmpData7 { get; set; }
        string sTmpData8 { get; set; }
        string sTmpData9 { get; set; }
     
    }
}
