namespace NtbSoft.Demo.App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdencode = new System.Windows.Forms.Button();
            this.txtinfos = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtresult = new System.Windows.Forms.TextBox();
            this.cmdbrowse = new System.Windows.Forms.Button();
            this.txtkey_code = new System.Windows.Forms.TextBox();
            this.cmdsave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblpath_file_security = new System.Windows.Forms.Label();
            this.lblpath_file_save = new System.Windows.Forms.Label();
            this.cmd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdencode
            // 
            this.cmdencode.BackColor = System.Drawing.SystemColors.Control;
            this.cmdencode.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.cmdencode.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cmdencode.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.cmdencode.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.cmdencode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdencode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdencode.Location = new System.Drawing.Point(13, 30);
            this.cmdencode.Name = "cmdencode";
            this.cmdencode.Size = new System.Drawing.Size(113, 168);
            this.cmdencode.TabIndex = 0;
            this.cmdencode.Text = "Endcode";
            this.cmdencode.UseVisualStyleBackColor = false;
            this.cmdencode.UseWaitCursor = true;
            this.cmdencode.Click += new System.EventHandler(this.cmdencode_Click);
            // 
            // txtinfos
            // 
            this.txtinfos.Location = new System.Drawing.Point(132, 5);
            this.txtinfos.Multiline = true;
            this.txtinfos.Name = "txtinfos";
            this.txtinfos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtinfos.Size = new System.Drawing.Size(356, 114);
            this.txtinfos.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.World, ((byte)(0)), true);
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter code string";
            // 
            // txtresult
            // 
            this.txtresult.ForeColor = System.Drawing.SystemColors.Window;
            this.txtresult.Location = new System.Drawing.Point(132, 153);
            this.txtresult.Multiline = true;
            this.txtresult.Name = "txtresult";
            this.txtresult.ReadOnly = true;
            this.txtresult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtresult.Size = new System.Drawing.Size(356, 45);
            this.txtresult.TabIndex = 4;
            // 
            // cmdbrowse
            // 
            this.cmdbrowse.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.cmdbrowse.Location = new System.Drawing.Point(132, 124);
            this.cmdbrowse.Name = "cmdbrowse";
            this.cmdbrowse.Size = new System.Drawing.Size(75, 23);
            this.cmdbrowse.TabIndex = 5;
            this.cmdbrowse.Text = "Đọc file";
            this.cmdbrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdbrowse.UseVisualStyleBackColor = true;
            this.cmdbrowse.Click += new System.EventHandler(this.cmdbrowse_Click);
            // 
            // txtkey_code
            // 
            this.txtkey_code.AcceptsReturn = true;
            this.txtkey_code.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey_code.Location = new System.Drawing.Point(13, 208);
            this.txtkey_code.Name = "txtkey_code";
            this.txtkey_code.Size = new System.Drawing.Size(113, 20);
            this.txtkey_code.TabIndex = 8;
            // 
            // cmdsave
            // 
            this.cmdsave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.cmdsave.Location = new System.Drawing.Point(132, 208);
            this.cmdsave.Name = "cmdsave";
            this.cmdsave.Size = new System.Drawing.Size(75, 23);
            this.cmdsave.TabIndex = 9;
            this.cmdsave.Text = "Lưu File";
            this.cmdsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdsave.UseVisualStyleBackColor = true;
            this.cmdsave.Click += new System.EventHandler(this.cmdsave_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(13, 237);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 1);
            this.panel1.TabIndex = 11;
            // 
            // lblpath_file_security
            // 
            this.lblpath_file_security.BackColor = System.Drawing.Color.Transparent;
            this.lblpath_file_security.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpath_file_security.ForeColor = System.Drawing.Color.OldLace;
            this.lblpath_file_security.Location = new System.Drawing.Point(224, 128);
            this.lblpath_file_security.Name = "lblpath_file_security";
            this.lblpath_file_security.Size = new System.Drawing.Size(264, 19);
            this.lblpath_file_security.TabIndex = 12;
            this.lblpath_file_security.Text = "StrSqlCon.txt";
            // 
            // lblpath_file_save
            // 
            this.lblpath_file_save.BackColor = System.Drawing.Color.Transparent;
            this.lblpath_file_save.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpath_file_save.ForeColor = System.Drawing.Color.OldLace;
            this.lblpath_file_save.Location = new System.Drawing.Point(224, 212);
            this.lblpath_file_save.Name = "lblpath_file_save";
            this.lblpath_file_save.Size = new System.Drawing.Size(264, 19);
            this.lblpath_file_save.TabIndex = 13;
            this.lblpath_file_save.Text = "StrSqlCon.txt";
            this.lblpath_file_save.Click += new System.EventHandler(this.lblpath_file_save_Click);
            // 
            // cmd
            // 
            this.cmd.Location = new System.Drawing.Point(15, 243);
            this.cmd.Name = "cmd";
            this.cmd.Size = new System.Drawing.Size(75, 23);
            this.cmd.TabIndex = 17;
            this.cmd.Text = "Close";
            this.cmd.UseVisualStyleBackColor = true;
            this.cmd.Click += new System.EventHandler(this.cmd_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 275);
            this.Controls.Add(this.cmd);
            this.Controls.Add(this.lblpath_file_save);
            this.Controls.Add(this.lblpath_file_security);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cmdsave);
            this.Controls.Add(this.txtkey_code);
            this.Controls.Add(this.cmdbrowse);
            this.Controls.Add(this.txtresult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtinfos);
            this.Controls.Add(this.cmdencode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Licence";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdencode;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtinfos;
        public System.Windows.Forms.TextBox txtresult;
        private System.Windows.Forms.Button cmdbrowse;
        private System.Windows.Forms.TextBox txtkey_code;
        private System.Windows.Forms.Button cmdsave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblpath_file_security;
        private System.Windows.Forms.Label lblpath_file_save;
        private System.Windows.Forms.Button cmd;
    }
}

