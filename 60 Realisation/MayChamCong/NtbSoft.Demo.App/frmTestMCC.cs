﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App
{
    public partial class frmTestMCC : Form
    {
        //http://www.thietbichamcong.vn/dich-vu-khac/cung-cap-sdk-va-tich-hop-thiet-bi/cung-cap-bo-giao-tiep-dieu-khien-may-cham-cong-sdk-cho-may-ronald-jack-x628-u160-iclock-260.html
        public frmTestMCC()
        {
            InitializeComponent();
        }
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
        Devices.Dal.IAttLogs _clsDownload;
        Devices.Dal.IDeviceManager _DeviceManager;
        //Devices.MCC.DeviceManagerNET _DeviceManager;
        Devices.Dal.IUser _iUser;
        //ERP.Models.Interfaces.ITerminal _Terminal = new ERP.Models.clsTerminal();
        ERP.Models.Interfaces.IBase<ERP.Entity.Interfaces.ITerminalInfo> _Terminal
            = new ERP.Models.clsTerminal();

        //Create Standalone SDK class dynamicly.
        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
        
        Devices.MCC.Services.Interfaces.IAttLogs _attLog;

        Devices.MCC.Services.UserManagement _CardUser = null;
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            #region
            _clsDownload = new Devices.MCC.clsAttLogs2(axCZKEM1, iMachineNumber);
            _clsDownload.OnDownloading += _clsDownload_OnDownloading;
            _clsDownload.OnDownloadCompleted += clsDownload_DownloadDataCompleted;

            _DeviceManager = new Devices.MCC.DeviceManager(1, new Devices.MCC.ConnectNet(1));
            //_DeviceManager = new Devices.MCC.DeviceManagerNET(iMachineNumber);
            
            this._iUser = new Devices.MCC.clsUser(axCZKEM1, this.iMachineNumber);
            this._iUser.OnDownloading += _iUser_OnDownloading;
            this._iUser.OnDownloadCompleted += _iUser_OnDownloadCompleted;

            _Terminal.GetAll((items, ex) =>
            {
                if (ex == null)
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dataGridView1.DataSource = items;
                    });
                else
                    MessageBox.Show(ex.Message);
            });
            #endregion

        }

      

        void _Terminal_LoadCompleted(object sender, ERP.Models.Interfaces.TerminalDataEventArgs e)
        {
            if (e.Ex != null)
            {
                MessageBox.Show(e.Ex.Message);
                return;
            }
            this.dataGridView1.DataSource = e.TerminalCollection;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            //if (bIsConnected == false)
            //{
            //    MessageBox.Show("Please connect the device first", "Error");
            //    return;
            //}
            //_clsDownload.DownloadAttLogs();

            #region Cach 2
            this.dataGridView1.DataSource = null;
            this.button1.Enabled = false;
            _attLog = new Devices.MCC.Services.AttLogs(1,DateTime.Now, new Devices.MCC.Services.ConnectNet(txtIP.Text, Convert.ToInt32(txtPort.Text)));
            _attLog.OnDownloading -= _attLog_OnDownloading;
            _attLog.OnDownloading += _attLog_OnDownloading;
            _attLog.Connect((result, ex) =>
            {
                if (result)
                    _attLog.DownloadAttLogs((items, ex1) =>
                    {
                        this.Invoke((MethodInvoker)delegate()
                        {
                            this.dataGridView1.DataSource = items;
                            this.button1.Enabled = true;
                        });
                    });
            });
            #endregion

        }

        void _attLog_OnDownloading(object sender, Devices.MCC.Services.AttLogsEventArgs e)
        {
            if (e.Ex == null)
                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = e.RowIndex.ToString();
                });
        }

        void _clsDownload_OnDownloading(object sender, Devices.Dal.AttReadingEventArgs e)
        {
            if (e.Ex == null)
                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = e.RowIndex.ToString();
                });
        }

        void clsDownload_DownloadDataCompleted(object sender, Devices.Dal.AttLogsEventArgs e)
        {
            if (e.Ex == null)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    dataGridView1.DataSource = e.IUserCollection;
                });
            }
            
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            int idwErrorCode = 0;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();

                this.axCZKEM1.OnFinger -= new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                this.axCZKEM1.OnVerify -= new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                this.axCZKEM1.OnAttTransactionEx -= new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                this.axCZKEM1.OnFingerFeature -= new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                this.axCZKEM1.OnEnrollFingerEx -= new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                this.axCZKEM1.OnDeleteTemplate -= new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                this.axCZKEM1.OnNewUser -= new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                this.axCZKEM1.OnHIDNum -= new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                this.axCZKEM1.OnAlarm -= new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                this.axCZKEM1.OnDoor -= new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                this.axCZKEM1.OnWriteCard -= new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                this.axCZKEM1.OnEmptyCard -= new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);

                bIsConnected = false;
                btnConnect.Text = "Connect";
                Cursor = Cursors.Default;
                return;
            }

            bIsConnected = axCZKEM1.Connect_Net(txtIP.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                
                //iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);
                    this.axCZKEM1.OnFingerFeature += new zkemkeeper._IZKEMEvents_OnFingerFeatureEventHandler(axCZKEM1_OnFingerFeature);
                    this.axCZKEM1.OnEnrollFingerEx += new zkemkeeper._IZKEMEvents_OnEnrollFingerExEventHandler(axCZKEM1_OnEnrollFingerEx);
                    this.axCZKEM1.OnDeleteTemplate += new zkemkeeper._IZKEMEvents_OnDeleteTemplateEventHandler(axCZKEM1_OnDeleteTemplate);
                    this.axCZKEM1.OnNewUser += new zkemkeeper._IZKEMEvents_OnNewUserEventHandler(axCZKEM1_OnNewUser);
                    this.axCZKEM1.OnHIDNum += new zkemkeeper._IZKEMEvents_OnHIDNumEventHandler(axCZKEM1_OnHIDNum);
                    this.axCZKEM1.OnAlarm += new zkemkeeper._IZKEMEvents_OnAlarmEventHandler(axCZKEM1_OnAlarm);
                    this.axCZKEM1.OnDoor += new zkemkeeper._IZKEMEvents_OnDoorEventHandler(axCZKEM1_OnDoor);
                    this.axCZKEM1.OnWriteCard += new zkemkeeper._IZKEMEvents_OnWriteCardEventHandler(axCZKEM1_OnWriteCard);
                    this.axCZKEM1.OnEmptyCard += new zkemkeeper._IZKEMEvents_OnEmptyCardEventHandler(axCZKEM1_OnEmptyCard);
                    this.axCZKEM1.OnDisConnected += new zkemkeeper._IZKEMEvents_OnDisConnectedEventHandler(axCZKEM1_OnDisConnected);
                }
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        /*************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.*
        * This part is for demonstrating the RealTime Events that triggered  by your operations          *
        **************************************************************************************************/
        #region RealTime Events

        private void axCZKEM1_OnDisConnected()
        {
            throw new NotImplementedException();
        }
        //When you place your finger on sensor of the device,this event will be triggered
        private void axCZKEM1_OnFinger()
        {
            lbRTShow.Items.Add("RTEvent OnFinger Has been Triggered");
        }

        //After you have placed your finger on the sensor(or swipe your card to the device),this event will be triggered.
        //If you passes the verification,the returned value userid will be the user enrollnumber,or else the value will be -1;
        private void axCZKEM1_OnVerify(int iUserID)
        {
            lbRTShow.Items.Add("RTEvent OnVerify Has been Triggered,Verifying...");
            if (iUserID != -1)
            {
                lbRTShow.Items.Add("Verified OK,the UserID is " + iUserID.ToString());
            }
            else
            {
                lbRTShow.Items.Add("Verified Failed... ");
            }
        }

        //If your fingerprint(or your card) passes the verification,this event will be triggered
        private void axCZKEM1_OnAttTransactionEx(string sEnrollNumber, int iIsInValid, int iAttState, int iVerifyMethod, int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond, int iWorkCode)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                lbRTShow.Items.Add("RTEvent OnAttTrasactionEx Has been Triggered,Verified OK");
                lbRTShow.Items.Add("...UserID:" + sEnrollNumber);
                lbRTShow.Items.Add("...isInvalid:" + iIsInValid.ToString());
                lbRTShow.Items.Add("...attState:" + iAttState.ToString());
                lbRTShow.Items.Add("...VerifyMethod:" + iVerifyMethod.ToString());
                lbRTShow.Items.Add("...Workcode:" + iWorkCode.ToString());//the difference between the event OnAttTransaction and OnAttTransactionEx
                lbRTShow.Items.Add("...Time:" + iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString());

            });
        }

        //When you have enrolled your finger,this event will be triggered and return the quality of the fingerprint you have enrolled
        private void axCZKEM1_OnFingerFeature(int iScore)
        {
            if (iScore < 0)
            {
                lbRTShow.Items.Add("The quality of your fingerprint is poor");
            }
            else
            {
                lbRTShow.Items.Add("RTEvent OnFingerFeature Has been Triggered...Score:　" + iScore.ToString());
            }
        }

        //When you are enrolling your finger,this event will be triggered.
        private void axCZKEM1_OnEnrollFingerEx(string sEnrollNumber, int iFingerIndex, int iActionResult, int iTemplateLength)
        {
            if (iActionResult == 0)
            {
                lbRTShow.Items.Add("RTEvent OnEnrollFigerEx Has been Triggered....");
                lbRTShow.Items.Add(".....UserID: " + sEnrollNumber + " Index: " + iFingerIndex.ToString() + " tmpLen: " + iTemplateLength.ToString());
            }
            else
            {
                lbRTShow.Items.Add("RTEvent OnEnrollFigerEx Has been Triggered Error,actionResult=" + iActionResult.ToString());
            }
        }

        //When you have deleted one one fingerprint template,this event will be triggered.
        private void axCZKEM1_OnDeleteTemplate(int iEnrollNumber, int iFingerIndex)
        {
            lbRTShow.Items.Add("RTEvent OnDeleteTemplate Has been Triggered...");
            lbRTShow.Items.Add("...UserID=" + iEnrollNumber.ToString() + " FingerIndex=" + iFingerIndex.ToString());
        }

        //When you have enrolled a new user,this event will be triggered.
        private void axCZKEM1_OnNewUser(int iEnrollNumber)
        {
            lbRTShow.Items.Add("RTEvent OnNewUser Has been Triggered...");
            lbRTShow.Items.Add("...NewUserID=" + iEnrollNumber.ToString());
        }

        //When you swipe a card to the device, this event will be triggered to show you the card number.
        private void axCZKEM1_OnHIDNum(int iCardNumber)
        {
            lbRTShow.Items.Add("RTEvent OnHIDNum Has been Triggered...");
            lbRTShow.Items.Add("...Cardnumber=" + iCardNumber.ToString());
        }

        //When the dismantling machine or duress alarm occurs, trigger this event.
        private void axCZKEM1_OnAlarm(int iAlarmType, int iEnrollNumber, int iVerified)
        {
            lbRTShow.Items.Add("RTEvnet OnAlarm Has been Triggered...");
            lbRTShow.Items.Add("...AlarmType=" + iAlarmType.ToString());
            lbRTShow.Items.Add("...EnrollNumber=" + iEnrollNumber.ToString());
            lbRTShow.Items.Add("...Verified=" + iVerified.ToString());
        }

        //Door sensor event
        private void axCZKEM1_OnDoor(int iEventType)
        {
            lbRTShow.Items.Add("RTEvent Ondoor Has been Triggered...");
            lbRTShow.Items.Add("...EventType=" + iEventType.ToString());
        }

        //When you have emptyed the Mifare card,this event will be triggered.
        private void axCZKEM1_OnEmptyCard(int iActionResult)
        {
            lbRTShow.Items.Add("RTEvent OnEmptyCard Has been Triggered...");
            if (iActionResult == 0)
            {
                lbRTShow.Items.Add("...Empty Mifare Card OK");
            }
            else
            {
                lbRTShow.Items.Add("...Empty Failed");
            }
        }

        //When you have written into the Mifare card ,this event will be triggered.
        private void axCZKEM1_OnWriteCard(int iEnrollNumber, int iActionResult, int iLength)
        {
            lbRTShow.Items.Add("RTEvent OnWriteCard Has been Triggered...");
            if (iActionResult == 0)
            {
                lbRTShow.Items.Add("...Write Mifare Card OK");
                lbRTShow.Items.Add("...EnrollNumber=" + iEnrollNumber.ToString());
                lbRTShow.Items.Add("...TmpLength=" + iLength.ToString());
            }
            else
            {
                lbRTShow.Items.Add("...Write Failed");
            }
        }

        //After function GetRTLog() is called ,RealTime Events will be triggered. 
        //When you are using these two functions, it will request data from the device forwardly.
        private void rtTimer_Tick(object sender, EventArgs e)
        {
            if (axCZKEM1.ReadRTLog(iMachineNumber))
            {
                while (axCZKEM1.GetRTLog(iMachineNumber))
                {
                    ;
                }
            }
        }

        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            _DeviceManager.Connect(txtIP.Text, Convert.ToInt32(txtPort.Text),(result, ex) => { });
            _DeviceManager.GetFirmwareVersion((value, ex) =>
            {
                string _kq = "";
                if (ex != null)
                    _kq = ex.Message;
                else
                    _kq = value.ToString();

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = _kq;
                });

            });
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _DeviceManager.Connect(txtIP.Text, Convert.ToInt32(txtPort.Text), (result, ex) => { });
            _DeviceManager.GetDeviceMAC((value, ex) =>
            {
                if (ex != null)
                    value = ex.Message;

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = value;
                });
            });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetSerialNumber((value, ex) =>
            {
                if (ex != null)
                    value = ex.Message;

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = value;
                });
            });
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetProductCode((value, ex) =>
            {
                if (ex != null)
                    value = ex.Message;

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = value;
                });
            });
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetPlatform((value, ex) =>
            {
                if (ex != null)
                    value = ex.Message;

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = value;
                });
            });
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetDeviceIP((value, ex) =>
            {
                if (ex != null)
                    value = ex.Message;

                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = value;
                });
            });
        }

        private void btnGetSDKVersion_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetSDKVersion((value, ex) =>
            {
                if (ex == null)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.txtShow.Text = value;
                    });
                }
                else
                    MessageBox.Show(ex.Message);
            });
        }

        private void btnDownloadTmp_Click(object sender, EventArgs e)
        {
            this._iUser.DownloadFPTmp();
        }
        void _iUser_OnDownloading(object sender, Devices.Dal.UserUploadEventArgs e)
        {
            if (e.Ex == null)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    this.txtShow.Text = e.FingerIndex.ToString();
                });
            }
        }

        void _iUser_OnDownloadCompleted(object sender, Devices.Dal.AttLogsEventArgs e)
        {
            if (e.Ex == null)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    dataGridView1.DataSource = e.IUserCollection;
                });
            }
        }

        private void btnUpload9_Click(object sender, EventArgs e)
        {

        }

        private void btnGetVendor_Click(object sender, EventArgs e)
        {
            this._DeviceManager.GetVendor((result, Ex) =>
            {
                if (Ex == null)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.txtShow.Text = result;
                    });
                }
                else
                    MessageBox.Show(Ex.Message);
            });
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this._CardUser = new Devices.MCC.Services.UserManagement(1, new Devices.MCC.Services.ConnectNet(txtIP.Text, Convert.ToInt32(txtPort.Text)));
            _CardUser.Connect((result, ex) =>
            {
                if (result)
                    _CardUser.DownloadFPTmp((items, ex1) =>
                    {
                        this.Invoke((MethodInvoker)delegate()
                        {
                            this.dataGridView1.DataSource = items;
                            this.button1.Enabled = true;
                        });
                    });
            });
        }
    }
}
