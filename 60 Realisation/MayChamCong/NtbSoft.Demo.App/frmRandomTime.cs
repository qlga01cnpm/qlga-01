﻿using gcc;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NtbSoft.Demo.App
{
    
    public partial class frmRandomTime : Form
    {
        private string tblInOut = "";
        public frmRandomTime()
        {
            InitializeComponent();
            
        }
        protected override void OnLoad(EventArgs e)
        {
            //GetOccuranceOfWeekday(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.DayOfWeek);
            Dictionary<string, string> Errors = new Dictionary<string, string>();
            Errors.Add("1", "loi 1");
            Errors.Add("2", "loi 2");
            string s = string.Join(Environment.NewLine, Errors.Select(x => x.Value).ToArray());
            var dd = s;
            var ds = s;

            DateTime fistDay = NtbSoft.ERP.Libs.clsCommon.FirstDayOfMonth(DateTime.Now.Month, DateTime.Now.Year),
                    lastDay = NtbSoft.ERP.Libs.clsCommon.LastDayOfMonth(DateTime.Now.Month, DateTime.Now.Year);
            string day = "";
            for (int i = 1; i <= lastDay.Day; i++)
            {
                var d = i;
                var k = d;
                day += "I" + i.ToString() + "; ";
            }
            var sday = day.TrimEnd(new char[] { ';' });

            DateTime firstDayOfWeek = ERP.Libs.clsCommon.FirstDayOfWeek(DateTime.Now.Month,DateTime.Now.Day, DateTime.Now.Year);
            DateTime lasttDayOfWeek = ERP.Libs.clsCommon.LastDayOfWeek(DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);

            #region Test 1

            var kk = DateTime.Now.ToString().GetHashCode().ToString("x");
            ds = kk;
            double v1 = XuLyDuLieuTaiVe.fcnDateDiff("18:00", "18:00");
            var v2 = v1;
            var v3 = v2;
            string text12_GioQuet = "17:01";
            bool flag3 = (XuLyDuLieuTaiVe.fcnDateDiff("01:00", text12_GioQuet) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12_GioQuet, "09:00") >= 0.0);
            var f1 = flag3;
            var f2 = f1;


            mcc.frmTinhCong frm = new mcc.frmTinhCong();
            frm.TinhCong();

            //mcc.XuLyDuLieuTaiVe.XuLyDuLieuQuetThe("11119", new DateTime(2017, 11, 26));
            //XuLyDuLieuTaiVe.XuLyDuLieuQuetThe("4", DateTime.Now);
            #endregion

            //mcc.clsTemp cls = new mcc.clsTemp();
            //cls.LoadDataMonth("86", "1818");
        }

        #region ddddddddddddddd
        private void button1_Click(object sender, EventArgs e)
        {
            Random generator = new Random();
            String r = generator.Next(0, 1000000).ToString("D6");
            this.txtRan.Text = r;
        }

        private string RandomBill()
        {
            Random generator = new Random();
            return generator.Next(0, 1000000).ToString("D6");
        }

        //public string ToBase62(int Value, int Digits)
        //{
        //    char[] chars = new char[62];
        //    chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        //    if (Digits == 0) return "";

        //    return new StringBuilder(ToBase62((Value / (chars.Length)), Digits - 1));//.Append(chars[Value % (chars.Length)]);
        //}

        List<DateTime> ArrayDates = new List<DateTime>();
        private void btClickMe_Click(object sender, EventArgs e)
        {
            int dd = DateTimeOffset.Now.Minute;
            // string timeIn = string.Format(DateTime.Now.AddMinutes(DateInterval.Minute, (double)random7.Next(0, 7), Conversions.ToDate(text12)), "HH:mm");
            string timeIn = "";
            TimeSpan start = TimeSpan.FromHours(7);
            TimeSpan end = start.Add(TimeSpan.FromMinutes(-11));
            int maxMinutes = (int)((start - end).TotalMinutes);
            Random random = new Random();
            List<TimeSpan> listHours = new List<TimeSpan>();
            List<string> ListTimeIn = new List<string>();
            DateTime date = DateTime.Parse("11/01/2017");
            int minutes = random.Next(0, maxMinutes);
            for (int i = 0; i < 30; i++)
            {
                minutes = random.Next(0, maxMinutes);
                TimeSpan t = end.Add(TimeSpan.FromMinutes(minutes));
                listHours.Add(t);

                timeIn = Strings.Format(DateAndTime.DateAdd(DateInterval.Minute, (double)random.Next(0, 7), Conversions.ToDate("07:30")), "HH:mm");
                ListTimeIn.Add(timeIn);
                var kq = timeIn;
            }


           
            TimeSpan t1 = end.Add(TimeSpan.FromMinutes(minutes));
            DateTime _dateStart = DateTime.Parse("2017-11-01");
            DateTime _dateEnd = DateTime.Parse("2017-11-30");
            DateTime currentDay = new DateTime(_dateStart.Year, _dateStart.Month, _dateStart.Day);
            ArrayDates = new List<DateTime>();
            
            var countSun = 0;
            while (currentDay <= _dateEnd)
            {
                DateTime dtNew = new DateTime(currentDay.Year, currentDay.Month, currentDay.Day).Add(t1);
                if (dtNew.DayOfWeek != DayOfWeek.Sunday)
                    ArrayDates.Add(dtNew);
                else
                {
                    countSun = CountDayOfWeekInMonth(currentDay.Year, currentDay.Month, currentDay, currentDay.DayOfWeek);
                    
                    if (this.chkCN1.Checked && countSun == Convert.ToInt32(this.chkCN1.Tag))
                        ArrayDates.Add(dtNew);
                    if (this.chkCN2.Checked && countSun == Convert.ToInt32(this.chkCN2.Tag))
                        ArrayDates.Add(dtNew);
                    if (this.chkCN3.Checked && countSun == Convert.ToInt32(this.chkCN3.Tag))
                        ArrayDates.Add(dtNew);
                    if (this.chkCN4.Checked && countSun == Convert.ToInt32(this.chkCN4.Tag))
                        ArrayDates.Add(dtNew);

                }

                #region Lay ngay dau thang va ngay cuoi thang
                int lastDayOfMonth = DateTime.DaysInMonth(currentDay.Year, currentDay.Month);
                var l = lastDayOfMonth;
                DateTime firstOfNextMonth = new DateTime(currentDay.Year, currentDay.Month, 1).AddMonths(1);
                var kkkk = firstOfNextMonth;

                #endregion

                var ss = GetOccuranceOfWeekday(currentDay.Year, currentDay.Month, currentDay.DayOfWeek);
                var d = ss;


                minutes = random.Next(0, maxMinutes);
                t1 = end.Add(TimeSpan.FromMinutes(minutes));
                currentDay = currentDay.AddDays(1);
            }

            this.dataGridView1.DataSource = ArrayDates;
        }

        private int CountDays(DayOfWeek day, DateTime dt)
        {
            //First Day of Month
            DateTime start = new DateTime(dt.Year, dt.Month, 1);
            //Last days of month
            DateTime end = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
            
            TimeSpan ts = end - start;                       // Total duration
            int count = (int)Math.Floor(ts.TotalDays / 7);   // Number of whole weeks
            int remainder = (int)(ts.TotalDays % 7);         // Number of remaining days
            int sinceLastDay = (int)(end.DayOfWeek - day);   // Number of days since last [day]
            if (sinceLastDay < 0) sinceLastDay += 7;         // Adjust for negative days since last [day]

            // If the days in excess of an even week are greater than or equal to the number days since the last [day], then count this one, too.
            if (remainder >= sinceLastDay) count++;

            return count;
        }


        private void NoOfSpecificDaysThisMonth()
        {
            int iDayCnt = 4;
            // Defaults to four days 
            DayOfWeek dw = DayOfWeek.Wednesday;
            DateTime firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int iRemainder = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) % 7;
            if ((dw >= firstDay.DayOfWeek) & ((dw - firstDay.DayOfWeek) < iRemainder))
            {
                iDayCnt = iDayCnt + 1;
            }
            else
                if ((dw < firstDay.DayOfWeek) & ((7 + dw - firstDay.DayOfWeek) < iRemainder))
                {
                    iDayCnt = iDayCnt + 1;
                }
            //MessageBox.Show(iDayCnt.ToString());
        }


        private int GetOccuranceOfWeekday(int year, int month, DayOfWeek dayOfWeek)
        {
            DateTime startDate = new DateTime(year, month, 1);
            int totalDays = startDate.AddMonths(1).Subtract(startDate).Days;

            int answer = Enumerable.Range(1, totalDays)
                .Select(item => new DateTime(year, month, item))
                .Where(date => date.DayOfWeek == dayOfWeek)
                .Count();
            //MessageBox.Show(answer.ToString());
            return answer;
        }

        private int CountDayOfWeekInMonth(int year, int month, DateTime dtCurrent, DayOfWeek dayOfWeek)
        {
            DateTime startDate = new DateTime(year, month, 1);
            int days = DateTime.DaysInMonth(startDate.Year, startDate.Month);
            int weekDayCount = 0;
            for (int day = 0; day < days; ++day)
            {
                weekDayCount += startDate.AddDays(day).DayOfWeek == dayOfWeek ? 1 : 0;
                if (dtCurrent == startDate.AddDays(day) && dtCurrent.DayOfWeek == dayOfWeek)
                    break;
            }
            return weekDayCount;
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (e.RowIndex < 0 || ArrayDates == null) return;
            DateTime dt = ArrayDates[e.RowIndex];
            if (dt.DayOfWeek == DayOfWeek.Sunday)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = ColorTranslator.FromHtml("#87CEFA");
            
        }

        private string tblREG="";
        double iSecCal = 0;
        private void btnXLL_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //this.tblREG = "tblTas_TotalAbsence" + StartUp.MMyyyy;
            //this.TimerTinhCong.Enabled = true;
            this.iSecCal = 0.0;
            //this.ProgressBarNV.Value = 0;
            //this.ProgressBarNV.Maximum = this.SheetNhatKyCC.RowCount;
            int soPhutToiThieu2LanQuet = XuLyDuLieuTaiVe.fcnSoPhutToiThieuGiuaHaiLanQuet();
            int num2 = 0;
            int arg_7B_0 = 0;
            checked
            {
                int num3 = 0;// this.SheetNhatKyCC.RowCount - 1;
                int num4 = arg_7B_0;
                bool flag = false;
                while (true)
                {
                    int arg_AB_0 = num4;
                    int num5 = num3;
                    if (arg_AB_0 > num5)
                    {
                        break;
                    }
                    //flag = Conversions.ToBoolean(this.SheetNhatKyCC.GetValue(num4, 0));
                    if (flag)
                    {
                        num2 = 1;
                    }
                    num4++;
                }
                flag = (num2 == 0);
                if (flag)
                {
                    Interaction.MsgBox("Bạn chưa chọn dòng cần xử lý!", MsgBoxStyle.Information, null);
                }
                else
                {
                    clsDal clsDal = new clsDal("SQLSERVER");
                    string tbl_TrangThaiQuetTheMMyyyy="";// = "TrangThaiQuetThe" + this.NgayXemNK.Value.ToString("MMyyyy");
                    //flag = (this.SheetNhatKyCC.RowCount > 0);
                    bool flag2;
                    if (flag)
                    {
                        int arg_125_0 = 0;
                        int num6 = 0;// this.SheetNhatKyCC.RowCount - 1;
                        int num7 = arg_125_0;
                        while (true)
                        {
                            int arg_2ED_0 = num7;
                            int num5 = num6;
                            if (arg_2ED_0 > num5)
                            {
                                break;
                            }
                            string text2 = "";// Conversions.ToString(this.SheetNhatKyCC.GetValue(num7, 1));
                            string maCC = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num7, 2));
                            string text3 = "";// Conversions.ToString(this.SheetNhatKyCC.GetValue(num7, 3));
                            string text4 = "";// Conversions.ToString(this.SheetNhatKyCC.GetValue(num7, 5));
                            string d = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num7, 6));
                            string text5 = string.Concat(new string[]
							{
								"UPDATE ",
								tbl_TrangThaiQuetTheMMyyyy,
								" SET chkHopLe=1 WHERE ID=",
								text2,
								""
							});
                            clsDal.fcnExecNonQuery(text5);
                            flag = (num7 > 0);
                            if (flag)
                            {
                                string right = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num7 - 1, 3));
                                string right2 = "";// Conversions.ToString(this.SheetNhatKyCC.GetValue(num7 - 1, 5));
                                string d2 = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num7 - 1, 6));
                                flag = (Operators.CompareString(text3, right, false) == 0 & Operators.CompareString(text4, right2, false) == 0);
                                if (flag)
                                {
                                    flag2 = (XuLyDuLieuTaiVe.fcnDateDiff(d2, d) < (double)soPhutToiThieu2LanQuet & XuLyDuLieuTaiVe.fcnDateDiff(d2, d) >= 0.0);
                                    if (flag2)
                                    {
                                        text5 = string.Concat(new string[]
										{
											"UPDATE ",
											tbl_TrangThaiQuetTheMMyyyy,
											" SET chkHopLe=0 WHERE ID=",
											text2,
											""
										});
                                        clsDal.fcnExecNonQuery(text5);
                                    }
                                }
                            }
                            //this.ProgressBarNV.Value = this.ProgressBarNV.Value + 1;
                            System.Windows.Forms.Application.DoEvents();
                            num7++;
                        }
                    }
                    //this.ProgressBarNV.Value = 0;
                    flag2 = false;// (this.SheetNhatKyCC.RowCount > 0);
                    if (flag2)
                    {
                        int arg_327_0 = 0;
                        int num8 = 0;// this.SheetNhatKyCC.RowCount - 1;
                        int num9 = arg_327_0;
                        while (true)
                        {
                            int arg_47E_0 = num9;
                            int num5 = num8;
                            if (arg_47E_0 > num5)
                            {
                                break;
                            }
                            //flag = Conversions.ToBoolean(this.SheetNhatKyCC.GetValue(num9, 0));
                            if (flag)
                            {
                                string text2 = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num9, 1));
                                string maCC = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num9, 2));
                                string text3 = "";//Conversions.ToString(this.SheetNhatKyCC.GetValue(num9, 3));
                                string text6 = "";//XuLyDuLieuTaiVe.fcnEIDFromEmpID(text3);
                                string text4 = "";// Conversions.ToString(this.SheetNhatKyCC.GetValue(num9, 5));
                                this.tblInOut = "TimeIn_TimeOutReal" + Conversions.ToDate(text4).ToString("MMyyyy");
                                string text5 = "UPDATE " + this.tblInOut + " SET TimeIn='',BreakOut='',BreakIn='',TimeOut=''\r\n";
                                text5 = string.Concat(new string[]
								{
									text5,
									" WHERE EmployeeID='",
									text6,
									"' AND WorkingDay=Convert(DateTime,'",
									text4,
									"',103)"
								});
                                clsDal.fcnExecNonQuery(text5);
                                XuLyDuLieuTaiVe.XuLyDuLieuQuetThe(maCC, Conversions.ToDate(Conversions.ToDate(text4).ToString("dd/MM/yyyy")));
                                //this.ProgressBarNV.Value = this.ProgressBarNV.Value + 1;
                                System.Windows.Forms.Application.DoEvents();
                            }
                            num9++;
                        }
                    }
                    //this.TimerTinhCong.Enabled = false;
                    //this.ProgressBar2.Value = 0;
                    //this.ProgressBarNV.Value = 0;
                    //this.ProgressBarEnd.Value = 0;
                    System.Windows.Forms.Application.DoEvents();
                    //Interaction.MsgBox("Đã xử lý xong. Hãy Tính công để in báo cáo! " + Conversions.ToString(this.iSecCal / 10.0), MsgBoxStyle.Information, "Chú ý");
                    Cursor.Current = Cursors.Default;
                }
            }
        }


        #endregion

    }

    class XuLyDuLieuTaiVe
    {
        private static string qr;

        public static bool fcnCheckLock(string tenBang)
        {
            bool result = false;
            //clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT * FROM tblKhoaBang WHERE TenBang='" + tenBang + "' AND chkLock=1";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["chkLock"].ToString());
            }
            return result;
        }

        public static bool fcnCheckTonTaiBang(string tenBang)
        {
            DataSet dataSet = new DataSet();
            //clsDal clsDal = new clsDal("SQLSERVER");
            string sSQL = "select * from sysobjects where name= '" + tenBang + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count == 0;
            return !flag;
        }

        public static double fcnDateDiff(string D1, string D2)
        {
            double result = 0.0;
            bool flag = Information.IsDate(D1) & Information.IsDate(D2);
            if (flag)
            {
                result = (double)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(D1), Conversions.ToDate(D2), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
            }
            return result;
        }

        public static string fcnGioQuet1(string TenBang, string MaCC, string Ngay, string Gio)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            //clsDal clsDal = new clsDal("SQLSERVER");
            string text = string.Concat(new string[]
			{
				"SELECT GioQuet as Gio FROM ",
				TenBang,
				" WHERE MaCC='",
				MaCC,
				"' AND WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103)\r\n"
			});
            text = text + " AND DateDiff(mi,GioQuet,'" + Gio + "')>=0 order by GioQuet DESC";
            //dataSet = clsDal.fcnExecQuery(text, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnGioQuet2(string TenBang, string MaCC, string Ngay, string Gio)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            //clsDal clsDal = new clsDal("SQLSERVER");
            string text = string.Concat(new string[]
			{
				"SELECT GioQuet as Gio FROM ",
				TenBang,
				" WHERE MaCC='",
				MaCC,
				"' AND WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103)\r\n"
			});
            text = text + " AND DateDiff(mi,'" + Gio + "',GioQuet)>=0";
            //dataSet = clsDal.fcnExecQuery(text, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static bool fcnCheckInRaMay()
        {
            bool result = false;
            //clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT * FROM tblQuyTac ";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                bool flag2 = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["InRaMay"].ToString());
                if (flag2)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool fcnBanGhiHopLe(string TenBang, string MaCC, string Ngay, string Gio)
        {
            bool result = false;
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            //clsDal clsDal = new clsDal("SQLSERVER");
            string text = XuLyDuLieuTaiVe.fcnGioQuet1(TenBang, MaCC, Ngay, Gio);
            string text2 = XuLyDuLieuTaiVe.fcnGioQuet2(TenBang, MaCC, Ngay, Gio);
            bool flag = !Information.IsDate(text) & !Information.IsDate(text2);
            if (flag)
            {
                result = true;
            }
            flag = (Information.IsDate(text) & !Information.IsDate(text2));
            bool flag2;
            if (flag)
            {
                flag2 = (XuLyDuLieuTaiVe.fcnDateDiff(text, Gio) > 10.0);
                if (flag2)
                {
                    result = true;
                }
            }
            flag2 = (!Information.IsDate(text) & Information.IsDate(text2));
            if (flag2)
            {
                flag = (XuLyDuLieuTaiVe.fcnDateDiff(Gio, text2) > 10.0);
                if (flag)
                {
                    result = true;
                }
            }
            flag2 = (Information.IsDate(text) & Information.IsDate(text2));
            if (flag2)
            {
                flag = (XuLyDuLieuTaiVe.fcnDateDiff(text, Gio) > 10.0 & XuLyDuLieuTaiVe.fcnDateDiff(Gio, text2) > 10.0);
                if (flag)
                {
                    result = true;
                }
            }
            return result;
        }

        public static string fcnTimeInOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 TimeIn FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeIn"].ToString();
            }
            return result;
        }

        public static string fcnBreakOutOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BreakOut FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakOut"].ToString();
            }
            return result;
        }

        public static string fcnBreakInOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BreakIn FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakIn"].ToString();
            }
            return result;
        }

        public static string fcnTimeOutOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 TimeOut FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeOut"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhMuon1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhMuon FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhMuon"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhMuon1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhMuon FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhMuon"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhMuon2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhMuon2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhMuon2"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhMuon2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhMuon2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhMuon2"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhSom1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhSom FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhSom"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhSom1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhSom FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhSom"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhSom2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhSom2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhSom2"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhSom2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhSom2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhSom2"].ToString();
            }
            return result;
        }

        public static string fcnSCHFromEmpID(string EmpID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT SCHName FROM Employees WHERE EmployeeID=N'" + EmpID + "'";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["SCHName"].ToString();
            }
            return result;
        }

        public static int fcnLate_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            int result;
            if (flag)
            {
                result = 0;
            }
            else
            {
                int num = 0;
                //clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [M",
					Ngay.ToString(),
					"] as VL FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["VL"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        num = Conversions.ToInteger(dataSet.Tables[0].Rows[0]["VL"].ToString().Trim());
                    }
                }
                result = num;
            }
            return result;
        }

        public static int fcnEarly_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            int result;
            if (flag)
            {
                result = 0;
            }
            else
            {
                int num = 0;
                //clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [S",
					Ngay.ToString(),
					"] as VL FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["VL"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        num = Conversions.ToInteger(dataSet.Tables[0].Rows[0]["VL"].ToString().Trim());
                    }
                }
                result = num;
            }
            return result;
        }

        public static string fcnSCH_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            string result;
            if (flag)
            {
                result = "";
            }
            else
            {
                string text = "";
                //clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [",
					Ngay.ToString(),
					"] as Ngay FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["Ngay"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        text = dataSet.Tables[0].Rows[0]["Ngay"].ToString().Trim();
                    }
                }
                result = text;
            }
            return result;
        }

        public static bool fcnCheckExistInOut(string TenBang, string EmpID, string WorkingDay)
        {
            bool result = false;
            //clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT * FROM ",
				TenBang,
				" WHERE EmployeeID=N'",
				EmpID,
				"' AND WorkingDay=Convert(DateTime,'",
				WorkingDay,
				"',103) "
			});
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = true;
            }
            return result;
        }

        public static bool fcnCheckOverNight(string TenBang, string EmpID, DateTime Workingday)
        {
            DataSet dataSet = new DataSet();
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            bool flag = false;
            bool flag2 = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            bool result;
            if (flag2)
            {
                result = false;
            }
            else
            {
                XuLyDuLieuTaiVe.qr = string.Concat(new string[]
				{
					"SELECT TOP 1 [",
					Workingday.Day.ToString(),
					"] as Chk FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
                flag2 = (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0].Rows[0]["Chk"].ToString().Trim().Length > 0);
                if (flag2)
                {
                    bool flag3 = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["Chk"].ToString().Trim());
                    if (flag3)
                    {
                        flag = true;
                    }
                }
                result = flag;
            }
            return result;
        }

        public static int fcnSoPhutToiThieuGiuaHaiLanQuet()
        {
            int result = 0;
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT SoPhutGiuaHaiLan FROM tblQuyTac";
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["SoPhutGiuaHaiLan"].ToString())));
            }
            return result;
        }

        public static int fcnSoLanQuetThe(string TenBang, string CardID, string Ngay)
        {
            int result = 0;
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT COUNT(WorkingDay) as SL FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkHopLe=1"
			});
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["SL"].ToString())));
            }
            return result;
        }

        public static string fcnMinGioQuet(string TenBang, string CardID, string Ngay)
        {
            string result = "";
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT Min(GioQuet) as Gio FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkUsed=0"
			});
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnMaxGioQuet(string TenBang, string CardID, string Ngay)
        {
            string result = "";
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT Max(GioQuet) as Gio FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkUsed=0"
			});
            //dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnBreakOutGioQuet(string TenBang, string CardID, string Ngay, string MinGioQuet, string MaxGioQuet)
        {
            string result = "";
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            bool flag = Information.IsDate(MinGioQuet) & Information.IsDate(MaxGioQuet);
            if (flag)
            {
                string text = string.Concat(new string[]
				{
					"SELECT Min(GioQuet) as Gio FROM ",
					TenBang,
					" WHERE WorkingDay=Convert(DateTime,'",
					Ngay,
					"',103) AND MaCC='",
					CardID,
					"'\r\n"
				});
                text = string.Concat(new string[]
				{
					text,
					" AND GioQuet >'",
					MinGioQuet,
					"' AND GioQuet < '",
					MaxGioQuet,
					"' AND chkUsed=0"
				});
                //dataSet = clsDal.fcnExecQuery(text, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
                }
            }
            return result;
        }

        public static string fcnBreakInGioQuet(string TenBang, string CardID, string Ngay, string MinGioQuet, string MaxGioQuet, string BreakOutGioQuet)
        {
            string result = "";
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            bool flag = Information.IsDate(MaxGioQuet) & Information.IsDate(BreakOutGioQuet);
            if (flag)
            {
                string text = string.Concat(new string[]
				{
					"SELECT Max(GioQuet) as Gio FROM ",
					TenBang,
					" WHERE WorkingDay=Convert(DateTime,'",
					Ngay,
					"',103) AND MaCC='",
					CardID,
					"'\r\n"
				});
                text = string.Concat(new string[]
				{
					text,
					" AND GioQuet < '",
					MaxGioQuet,
					"'  AND GioQuet > '",
					BreakOutGioQuet,
					"' AND chkUsed=0"
				});
                //dataSet = clsDal.fcnExecQuery(text, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
                }
            }
            return result;
        }

        public static string fcnEIDFromEmpID(string EmpID)
        {
            string result = "";
            //clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE EmployeeID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            return result;
        }

        public static string fcnEmpIDFromeID(string EmpID)
        {
            string result = "";
            //clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT EmployeeID FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["EmployeeID"].ToString();
            }
            return result;
        }

        public static string fcnNgayVaoFromEID(string EmpID)
        {
            string result = "";
            //clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT BeginningDate FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BeginningDate"].ToString();
            }
            return result;
        }

        public static string fcnNgayNVFromEID(string EmpID)
        {
            string text = "01/01/1900";
            //clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT StopWorkingDay FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                text = dataSet.Tables[0].Rows[0]["StopWorkingDay"].ToString();
            }
            flag = (text.Trim().Length == 0);
            if (flag)
            {
                text = "01/01/1900";
            }
            return text;
        }

        public static string fcnEIDFromMaCC(string MaCC)
        {
            string result = "";
            //clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE MaCC=N'" + MaCC + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            return result;
        }

        public static string fcnReturnEmpID(string CardID)
        {
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE MaCC='" + CardID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnStrShiftID(string SCH)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT strShift FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["strShift"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift3(string SCH)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift3 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift3"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift1(string SCH)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift1 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift1"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift2(string SCH)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift2 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift2"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static int fcnNumberShift(string SCH)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT NumberShift FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            //dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            int result;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["NumberShift"].ToString())));
            }
            else
            {
                result = 0;
            }
            return result;
        }

        public static string fcnGioKetThucCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 TimeOut FROM tblShiftDuty WHERE ShiftID='" + shiftID + "' ";
            DataSet dataSet = new DataSet();
            //dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeOut"].ToString();
            }
            return result;
        }

        public static string fcnGioVaoGiuaCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 BreakIn FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakIn"].ToString();
            }
            return result;
        }

        public static string fcnGioRaGiuaCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 BreakOut FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakOut"].ToString();
            }
            return result;
        }

        public static string fcnGioBatDauVaoCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 TimeIn FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            //dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeIn"].ToString();
            }
            return result;
        }

        public static string GetShift(string strShift, string TimeIn)
        {
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            string result = "***";
            bool flag = Information.IsDate(TimeIn);
            checked
            {
                if (flag)
                {
                    bool flag2 = Strings.InStr(strShift, ",", CompareMethod.Binary) > 0;
                    if (flag2)
                    {
                        string[] array = Strings.Split(strShift, ",", -1, CompareMethod.Binary);
                        flag2 = (Information.UBound(array, 1) == 1);
                        bool flag3;
                        if (flag2)
                        {
                            result = Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2);
                        }
                        else
                        {
                            flag2 = (array[0].Trim().Length > 0 & Operators.CompareString(array[0].Trim(), "''", false) != 0);
                            int value = 0;
                            if (flag2)
                            {
                                result = Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2);
                                value = (int)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2))), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
                            }
                            int arg_146_0 = Information.LBound(array, 1);
                            int num = Information.UBound(array, 1);
                            int num2 = arg_146_0;
                            while (true)
                            {
                                int arg_1F9_0 = num2;
                                int num3 = num;
                                if (arg_1F9_0 > num3)
                                {
                                    break;
                                }
                                string text = Strings.Mid(array[num2].Trim(), 2, Strings.Len(array[num2].Trim()) - 2);
                                flag2 = (text.Trim().Length > 0);
                                if (flag2)
                                {
                                    flag = Information.IsDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text));
                                    if (flag)
                                    {
                                        flag3 = (unchecked((long)Math.Abs(value)) > Math.Abs(DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text)), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1)));
                                        if (flag3)
                                        {
                                            value = (int)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text)), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
                                            result = text;
                                        }
                                    }
                                }
                                num2++;
                            }
                        }
                        flag3 = (strShift.Trim().Length == 17);
                        if (flag3)
                        {
                            result = "***";
                        }
                    }
                    else
                    {
                        bool flag3 = strShift.Trim().Length > 0;
                        if (flag3)
                        {
                            result = strShift;
                        }
                        flag3 = (strShift.Trim().Length == 17);
                        if (flag3)
                        {
                            result = "***";
                        }
                    }
                }
                else
                {
                    result = "***";
                }
                return result;
            }
        }

        public static void XuLyDuLieuQuetThe(string MaCC, DateTime CurDay)
        {
            Cursor.Current = Cursors.WaitCursor;
            string text = "";
            string text2 = "";
            string text3 = "";
            //clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string text4_tblTrangThaiQuetTheTMP = "tblTrangThaiQuetTheTMP";
            string text5_TrangThaiQuetThe = "TrangThaiQuetThe" + CurDay.ToString("MMyyyy");
            string text6_TrangThaiQuetThe = "TrangThaiQuetThe" + DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy"))).ToString("MMyyyy");
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(text5_TrangThaiQuetThe);
            checked
            {
                if (!flag)
                {
                    string tenBang = "tblRegisterOverNight_" + CurDay.ToString("MMyyyy");
                    string tenBang2 = "tblRegisterOverNight_" + CurDay.AddMonths(-1).ToString("MMyyyy");
                    string text7_TimeIn_TimeOutReal = "TimeIn_TimeOutReal" + CurDay.ToString("MMyyyy");
                    //clsDal.fcnExecNonQuery("Exec sp_TaoBangInOut '" + text7 + "'");
                    string text8 = "tblRegisterSCH_Day_" + CurDay.ToString("MMyyyy");
                    string text9 = "DELETE FROM " + text4_tblTrangThaiQuetTheTMP;
                    //clsDal.fcnExecNonQuery(text9);
                    flag = (CurDay.Day == DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy")).AddDays(-1.0)).Day);
                    if (flag)
                    {
                        bool flag2 = XuLyDuLieuTaiVe.fcnCheckTonTaiBang(text6_TrangThaiQuetThe);
                        if (flag2)
                        {
                            text9 = "INSERT INTO " + text4_tblTrangThaiQuetTheTMP + "(MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe)";
                            text9 += " SELECT MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe";
                            text9 = text9 + " FROM " + text6_TrangThaiQuetThe;
                            text9 = text9 + " WHERE  MaCC='" + MaCC + "' ";
                            text9 = text9 + " AND WorkingDay ='" + DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy"))).ToString("yyyyMMdd") + "'";
                            //clsDal.fcnExecNonQuery(text9);
                        }
                    }
                    text9 = "INSERT INTO " + text4_tblTrangThaiQuetTheTMP + "(MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe)";
                    text9 += " SELECT MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe";
                    text9 = text9 + " FROM " + text5_TrangThaiQuetThe;
                    text9 = text9 + " WHERE chkHopLe=1 AND MaCC='" + MaCC + "' ";
                    text9 = text9 + " AND WorkingDay >='" + CurDay.AddDays(-1.0).ToString("yyyyMMdd") + "'";
                    text9 = text9 + " AND WorkingDay <='" + CurDay.AddDays(1.0).ToString("yyyyMMdd") + "'";
                    //clsDal.fcnExecNonQuery(text9);
                    DataSet dataSet2 = new DataSet();
                    text9 = string.Concat(new string[]
					{
						"SELECT DISTINCT *,DAY(WorkingDay) as iNgay, MONTH(WorkingDAy) as iThang , YEAR(WorkingDAy) as iNam FROM ",
						text4_tblTrangThaiQuetTheTMP,
						" WHERE MaCC='",
						MaCC,
						"' AND DAY(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Day),
						"'  AND MONTH(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Month),
						"'  AND YEAR(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Year),
						"' Order by MaCC,WorkingDay,GioQuet"
					});
                    //dataSet2 = clsDal.fcnExecQuery(text9, "tbl");
                    int arg_485_0 = 0;
                    int num = dataSet2.Tables[0].Rows.Count - 1;
                    int num2 = arg_485_0;
                    while (true)
                    {
                        int arg_1861_0 = num2;
                        int num3 = num;
                        if (arg_1861_0 > num3)
                        {
                            break;
                        }
                        string text10_Workingday = dataSet2.Tables[0].Rows[num2]["Workingday"].ToString();
                        bool flag2 = Information.IsDate(text3);
                        if (!flag2)
                        {
                            goto IL_4E5;
                        }
                        flag = (DateTime.Compare(Conversions.ToDate(text10_Workingday), Conversions.ToDate(text3)) == 0);
                        if (!flag)
                        {
                            goto IL_4E5;
                        }
                    IL_1851:
                        num2++;
                        continue;
                    IL_4E5:
                        DateTime dateTime_Workingday = Conversions.ToDate(Strings.Left(text10_Workingday, 10));
                        string text11_Workingday = Strings.Format(dateTime_Workingday, "dd/MM/yyyy");
                        int num4_iNgay = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iNgay"].ToString());
                        int value_iThang = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iThang"].ToString());
                        int value2_iNam = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iNam"].ToString());
                        string text12_GioQuet = dataSet2.Tables[0].Rows[num2]["GioQuet"].ToString();
                        string text13 = Strings.Format(Conversions.ToDate(text10_Workingday), "yyyy-MM-dd ") + text12_GioQuet;
                        string text14_eID = XuLyDuLieuTaiVe.fcnReturnEmpID(MaCC);
                        DateTime dateTime2_Workingday = DateAndTime.DateAdd(DateInterval.Day, -1.0, Conversions.ToDate(text10_Workingday));
                        DateTime dateTime3_Workingday = DateAndTime.DateAdd(DateInterval.Day, 1.0, Conversions.ToDate(text10_Workingday));
                        flag2 = (num4_iNgay == 1);
                        bool flag3;
                        if (flag2)
                        {
                            flag = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang2, text14_eID, dateTime2_Workingday);
                            if (flag)
                            {
                                flag3 = (XuLyDuLieuTaiVe.fcnDateDiff("01:00", text12_GioQuet) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12_GioQuet, "09:00") >= 0.0);
                                if (flag3)
                                {
                                    text9 = string.Concat(new string[]
									{
										"DELETE FROM ",
										text4_tblTrangThaiQuetTheTMP,
										" WHERE MaCC='",
										MaCC,
										"' AND DAY(WorkingDay) = '",
										Conversions.ToString(num4_iNgay),
										"'  AND MONTH(WorkingDay) = '",
										Conversions.ToString(value_iThang),
										"'  AND YEAR(WorkingDay) = '",
										Conversions.ToString(value2_iNam),
										"' AND GioQuet='",
										text12_GioQuet.Trim(),
										"'"
									});
                                    //clsDal.fcnExecNonQuery(text9);
                                }
                            }
                        }
                        text9 = "UPDATE " + text7_TimeIn_TimeOutReal + " SET TimeIn='',BreakOut='',BreakIn='', TimeOut=''";
                        text9 += ", strTimeIn=''";
                        text9 += ", strBreakOut=''";
                        text9 += ", strBreakIn=''";
                        text9 += ", strTimeOut=''";
                        text9 = string.Concat(new string[]
						{
							text9,
							" WHERE EmployeeID = N'",
							text14_eID,
							"' AND WorkingDay='",
							Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
							"'"
						});
                        //clsDal.fcnExecNonQuery(text9);
                        int num5_SoLanQuetThe = XuLyDuLieuTaiVe.fcnSoLanQuetThe(text4_tblTrangThaiQuetTheTMP, MaCC, text11_Workingday);
                        string text15_MinGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11_Workingday);
                        string text16_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11_Workingday);
                        string text17_BreakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11_Workingday, text15_MinGioQuet, text16_MaxGioQuet);
                        string text18_BreakInGioQuet = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11_Workingday, text15_MinGioQuet, text16_MaxGioQuet, text17_BreakOutGioQuet);
                        flag3 = (num5_SoLanQuetThe == 1);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, dateTime2_Workingday);
                            if (flag2)
                            {
                                flag = (XuLyDuLieuTaiVe.fcnDateDiff("01:00", text12_GioQuet) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12_GioQuet, "09:00") >= 0.0);
                                if (flag)
                                {
                                    text15_MinGioQuet = "";
                                }
                            }
                            flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, Conversions.ToDate(text10_Workingday));
                            if (flag3)
                            {
                                flag2 = (XuLyDuLieuTaiVe.fcnDateDiff("18:00", text12_GioQuet) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12_GioQuet, "23:00") >= 0.0);
                                if (flag2)
                                {
                                    text15_MinGioQuet = text12_GioQuet;
                                }
                                text17_BreakOutGioQuet = "";
                                text18_BreakInGioQuet = "";
                            }
                            else
                            {
                                text16_MaxGioQuet = "";
                                text17_BreakOutGioQuet = "";
                                text18_BreakInGioQuet = "";
                            }
                        }
                        flag3 = (num5_SoLanQuetThe == 2);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, dateTime2_Workingday);
                            if (flag2)
                            {
                                text15_MinGioQuet = text16_MaxGioQuet;
                                text16_MaxGioQuet = "";
                            }
                        }
                        flag3 = (num5_SoLanQuetThe == 3);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, dateTime2_Workingday);
                            if (flag2)
                            {
                                text15_MinGioQuet = text17_BreakOutGioQuet;
                                text17_BreakOutGioQuet = "";
                            }
                            else
                            {
                                text18_BreakInGioQuet = text16_MaxGioQuet;
                                text16_MaxGioQuet = "";
                            }
                        }
                        flag3 = Information.IsDate(text15_MinGioQuet);
                        string text19;
                        if (flag3)
                        {
                            text19 = Strings.Format(Conversions.ToDate(text10_Workingday), "yyyy-MM-dd ") + text15_MinGioQuet;
                        }
                        else
                        {
                            text19 = "";
                        }
                        flag3 = Information.IsDate(text17_BreakOutGioQuet);
                        string text20;
                        if (flag3)
                        {
                            text20 = Strings.Format(Conversions.ToDate(text10_Workingday), "yyyy-MM-dd ") + text17_BreakOutGioQuet;
                        }
                        else
                        {
                            text20 = "";
                        }
                        flag3 = Information.IsDate(text18_BreakInGioQuet);
                        string text21;
                        if (flag3)
                        {
                            text21 = Strings.Format(Conversions.ToDate(text10_Workingday), "yyyy-MM-dd ") + text18_BreakInGioQuet;
                        }
                        else
                        {
                            text21 = "";
                        }
                        flag3 = Information.IsDate(text16_MaxGioQuet);
                        string text22;
                        if (flag3)
                        {
                            text22 = Strings.Format(Conversions.ToDate(text10_Workingday), "yyyy-MM-dd ") + text16_MaxGioQuet;
                        }
                        else
                        {
                            text22 = "";
                        }
                        string minGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2_Workingday.ToString("dd/MM/yyyy"));
                        string text23_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2_Workingday.ToString("dd/MM/yyyy"));
                        flag3 = Information.IsDate(text23_MaxGioQuet);
                        if (flag3)
                        {
                            text = Strings.Format(dateTime2_Workingday, "yyyy-MM-dd ") + text23_MaxGioQuet;
                        }
                        string breakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2_Workingday.ToString("dd/MM/yyyy"), minGioQuet, text23_MaxGioQuet);
                        string text24_BreakInGioQuet = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2_Workingday.ToString("dd/MM/yyyy"), minGioQuet, text23_MaxGioQuet, breakOutGioQuet);
                        string text25_MinGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3_Workingday.ToString("dd/MM/yyyy"));
                        string text26_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3_Workingday.ToString("dd/MM/yyyy"));
                        string text27_BreakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3_Workingday.ToString("dd/MM/yyyy"), text25_MinGioQuet, text26_MaxGioQuet);
                        string str = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3_Workingday.ToString("dd/MM/yyyy"), text25_MinGioQuet, text26_MaxGioQuet, text27_BreakOutGioQuet);
                        flag3 = Information.IsDate(text25_MinGioQuet);
                        if (flag3)
                        {
                            text2 = Strings.Format(dateTime3_Workingday, "yyyy-MM-dd ") + text25_MinGioQuet;
                        }
                        string text28 = Strings.Format(dateTime3_Workingday, "yyyy-MM-dd ") + text27_BreakOutGioQuet;
                        string text29 = Strings.Format(dateTime3_Workingday, "yyyy-MM-dd ") + str;
                        string text30 = Strings.Format(dateTime3_Workingday, "yyyy-MM-dd ") + text26_MaxGioQuet;
                        flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, Conversions.ToDate(text10_Workingday).AddDays(-1.0));
                        if (flag3)
                        {
                            flag2 = ((num5_SoLanQuetThe == 1 && Information.IsDate(text) && XuLyDuLieuTaiVe.fcnDateDiff(text, text13) <= 780.0) 
                                & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text13)), Conversions.ToString(Conversions.ToDate("10:00"))) > 0.0 
                                & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text13)), Conversions.ToString(Conversions.ToDate("04:00"))) <= 0.0);
                            if (flag2)
                            {
                                text15_MinGioQuet = "";
                                text19 = "";
                            }
                        }
                        flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14_eID, Conversions.ToDate(text10_Workingday));
                        if (flag3)
                        {
                            flag2 = Information.IsDate(text25_MinGioQuet);
                            if (flag2)
                            {
                                flag = (XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text25_MinGioQuet)), Conversions.ToString(Conversions.ToDate("19:00"))) < 0.0 
                                    & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text25_MinGioQuet)), Conversions.ToString(Conversions.ToDate("23:59"))) >= 0.0);
                                if (flag)
                                {
                                    text25_MinGioQuet = "";
                                    text2 = "";
                                }
                            }
                            flag3 = XuLyDuLieuTaiVe.fcnCheckExistInOut(text7_TimeIn_TimeOutReal, text14_eID, Conversions.ToDate(text10_Workingday).ToString("dd/MM/yyyy"));
                            if (flag3)
                            {
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text7_TimeIn_TimeOutReal,
									" SET TimeIn='",
									text15_MinGioQuet,
									"',BreakOut='",
									text17_BreakOutGioQuet,
									"',BreakIn='",
									text18_BreakInGioQuet,
									"', TimeOut='",
									text25_MinGioQuet,
									"'"
								});
                                text9 = text9 + ", strTimeIn='" + text19 + "'";
                                text9 = text9 + ", strBreakOut='" + text20 + "'";
                                text9 = text9 + ", strBreakIn='" + text21 + "'";
                                text9 = text9 + ", strTimeOut='" + text2 + "'";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE EmployeeID = N'",
									text14_eID,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"'"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"')"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = "UPDATE " + text4_tblTrangThaiQuetTheTMP + " SET chkUsed=1 \r\n";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									dateTime3_Workingday.ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = text9 + " AND GioQuet IN ('" + text25_MinGioQuet + "')";
                                //clsDal.fcnExecNonQuery(text9);
                            }
                            else
                            {
                                text9 = "INSERT INTO " + text7_TimeIn_TimeOutReal + "(EmployeeID,WorkingDay,TimeIn,BreakOut,BreakIn,TimeOut\r\n";
                                text9 += ",strTimeIn,strBreakOut,strBreakIn,strTimeOut";
                                text9 += ",SCHName,strShiftID,Shift";
                                text9 += ",sTimeIn,sBreakOut,sBreakIn,sTimeOut";
                                text9 += ",ModifyDate";
                                text9 += ")";
                                text9 = string.Concat(new string[]
								{
									text9,
									" SELECT N'",
									text14_eID,
									"',Convert(DateTime,'",
									Conversions.ToDate(text10_Workingday).ToString("dd/MM/yyyy"),
									"',103),'",
									text15_MinGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text25_MinGioQuet,
									"'\r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" ,'",
									text19,
									"','",
									text20,
									"','",
									text21,
									"','",
									text2,
									"'"
								});
                                string text31 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",N'",
									text31,
									"',dbo.fcnStrShiftFromSCH('",
									text31,
									"'),dbo.fcnShiftFromSCH('",
									text31,
									"')"
								});
                                string text32 = "";
                                string text33 = "";
                                string text34 = "";
                                string text35 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",'",
									text32,
									"','",
									text33,
									"','",
									text34,
									"','",
									text35,
									"','",
									DateAndTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
									"'"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"')"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = "UPDATE " + text4_tblTrangThaiQuetTheTMP + " SET chkUsed=1 \r\n";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									dateTime3_Workingday.ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = text9 + " AND GioQuet IN ('" + text25_MinGioQuet + "')";
                                //clsDal.fcnExecNonQuery(text9);
                            }
                        }
                        else
                        {
                            flag3 = XuLyDuLieuTaiVe.fcnCheckExistInOut(text7_TimeIn_TimeOutReal, text14_eID, Conversions.ToDate(text10_Workingday).ToString("dd/MM/yyyy"));
                            if (flag3)
                            {
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text7_TimeIn_TimeOutReal,
									" SET TimeIn='",
									text15_MinGioQuet,
									"',BreakOut='",
									text17_BreakOutGioQuet,
									"',BreakIn='",
									text18_BreakInGioQuet,
									"',TimeOut='",
									text16_MaxGioQuet,
									"'"
								});
                                text9 = text9 + ", strTimeIn='" + text22 + "'";
                                text9 = text9 + ", strBreakOut='" + text20 + "'";
                                text9 = text9 + ", strBreakIn='" + text21 + "'";
                                text9 = text9 + ", strTimeOut='" + text22 + "'";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE EmployeeID = N'",
									text14_eID,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"'"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text15_MinGioQuet,
									"')"
								});
                                //clsDal.fcnExecNonQuery(text9);
                            }
                            else
                            {
                                flag3 = (num5_SoLanQuetThe <= 1);
                                if (flag3)
                                {
                                    text16_MaxGioQuet = "";
                                    text22 = "";
                                }
                                text9 = "INSERT INTO " + text7_TimeIn_TimeOutReal + "(EmployeeID,WorkingDay,TimeIn,BreakOut,BreakIn,TimeOut\r\n";
                                text9 += ",strTimeIn,strBreakOut,strBreakIn,strTimeOut";
                                text9 += ",SCHName,strShiftID,Shift";
                                text9 += ",sTimeIn,sBreakOut,sBreakIn,sTimeOut";
                                text9 += ")";
                                text9 = string.Concat(new string[]
								{
									text9,
									" SELECT N'",
									text14_eID,
									"',Convert(DateTime,'",
									text11_Workingday,
									"',103),'",
									text15_MinGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text16_MaxGioQuet,
									"'\r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" ,'",
									text19,
									"','",
									text20,
									"','",
									text21,
									"','",
									text22,
									"'"
								});
                                string text31 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",N'",
									text31,
									"',dbo.fcnStrShiftFromSCH('",
									text31,
									"'),dbo.fcnShiftFromSCH('",
									text31,
									"')"
								});
                                string text32 = "";
                                string text33 = "";
                                string text34 = "";
                                string text35 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",'",
									text32,
									"','",
									text33,
									"','",
									text34,
									"','",
									text35,
									"'"
								});
                                //clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(text10_Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text15_MinGioQuet,
									"')"
								});
                                //clsDal.fcnExecNonQuery(text9);
                            }
                        }
                        text3 = text10_Workingday;
                        goto IL_1851;
                    }
                    text9 = "DELETE FROM " + text4_tblTrangThaiQuetTheTMP + "";
                    text9 = string.Concat(new string[]
					{
						text9,
						" WHERE MaCC ='",
						MaCC,
						"' AND Workingday='",
						CurDay.ToString("yyyyMMdd"),
						"'"
					});
                    //clsDal.fcnExecNonQuery(text9);
                    Cursor.Current = Cursors.Default;
                }
            }
        }
    }
}
