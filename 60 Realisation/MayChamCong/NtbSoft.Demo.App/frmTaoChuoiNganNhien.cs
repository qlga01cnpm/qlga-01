﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App
{
    public partial class frmTaoChuoiNganNhien : Form
    {
        public frmTaoChuoiNganNhien()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            string value = GetString();
            string value2 = RandomStrings(10, false);

            RandomGenerator r = new RandomGenerator();
            string kq = r.Next();
            Random r2 = new Random();
            string sss = r2.Next().ToString();
        }

        private string RandomStrings(int size, bool lowerCase)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            if (lowerCase)
                return sb.ToString().ToLower();
            return sb.ToString();

        }
        /// <summary>
        /// Tạo ra một chuỗi ngẫu nhiên với độ dài cho trước
        /// </summary>
        /// <param name="size">Kích thước của chuỗi </param>
        /// <param name="lowerCase">Nếu đúng, tạo ra chuỗi chữ thường</param>
        /// <returns>Random string</returns>
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        public string GetString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }
    }


    class RandomGenerator
    {
        public const int DEFAULT_CAPACITY = 6;
        private string[] _store;
        private Random _rdmizer;
        private int _capacity;
        private static int _current = 0;

        /*
         * PROPERTIES
         */
        public int Capacity
        {
            get { return _capacity; }
            set { this._capacity = value; }
        }

        public int Current
        {
            get { return _current; }
        }

        /*
         * CONSTRUCTOR
         */
        public RandomGenerator()
        {
            _store = new string[100];
            for (int cnt = 0; cnt < 100; cnt++)
                _store[cnt] = "";
            _rdmizer = new Random();
            _capacity = DEFAULT_CAPACITY;
        }

        /*
         * METHODS
         */

        // Just only use to generate a random string
        public string Generate()
        {
            StringBuilder builder = new StringBuilder();
            while (builder.Length < _capacity)
            {
                builder.Append(_GenerateChar());
            }
            return builder.ToString();
        }

        // Add a string to store
        public void Add(string _in_str)
        {
            _store[_current++] = _in_str;
        }
        // Generate the next string to the store
        public string Next()
        {
            string _str = Generate();
            if (IsStored(_str) == true)
                Next();
            Add(_str);
            return _str;
        }
        // check whether a string stored already
        public bool IsStored(string _in_str)
        {
            for (int _cnt = 0; _cnt < _store.Length; _cnt++)
            {
                if (_store[_cnt].CompareTo(_in_str) == 0)
                    return true;
            }
            return false;
        }
        // get all stored strings
        public string[] GetStore()
        {
            return _store;
        }
        // generate a random character
        private char _GenerateChar()
        {
            int _num;
            do
            {
                _num = _rdmizer.Next(48, 122);
            } while ((_num < 48) || (_num > 57 && _num < 65) || (_num > 90 && _num < 97));

            char chr = Convert.ToChar(_num);
            return chr;
        }
    }
}
