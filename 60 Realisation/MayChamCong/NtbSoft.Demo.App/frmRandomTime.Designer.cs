﻿namespace NtbSoft.Demo.App
{
    partial class frmRandomTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btClickMe = new System.Windows.Forms.Button();
            this.chkCN1 = new System.Windows.Forms.CheckBox();
            this.chkCN2 = new System.Windows.Forms.CheckBox();
            this.chkCN3 = new System.Windows.Forms.CheckBox();
            this.chkCN4 = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.txtRan = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btClickMe
            // 
            this.btClickMe.Location = new System.Drawing.Point(73, 81);
            this.btClickMe.Name = "btClickMe";
            this.btClickMe.Size = new System.Drawing.Size(75, 23);
            this.btClickMe.TabIndex = 0;
            this.btClickMe.Text = "Click Me!";
            this.btClickMe.UseVisualStyleBackColor = true;
            this.btClickMe.Click += new System.EventHandler(this.btClickMe_Click);
            // 
            // chkCN1
            // 
            this.chkCN1.AutoSize = true;
            this.chkCN1.Location = new System.Drawing.Point(73, 58);
            this.chkCN1.Name = "chkCN1";
            this.chkCN1.Size = new System.Drawing.Size(47, 17);
            this.chkCN1.TabIndex = 1;
            this.chkCN1.Tag = "1";
            this.chkCN1.Text = "CN1";
            this.chkCN1.UseVisualStyleBackColor = true;
            // 
            // chkCN2
            // 
            this.chkCN2.AutoSize = true;
            this.chkCN2.Location = new System.Drawing.Point(126, 58);
            this.chkCN2.Name = "chkCN2";
            this.chkCN2.Size = new System.Drawing.Size(47, 17);
            this.chkCN2.TabIndex = 1;
            this.chkCN2.Tag = "2";
            this.chkCN2.Text = "CN2";
            this.chkCN2.UseVisualStyleBackColor = true;
            // 
            // chkCN3
            // 
            this.chkCN3.AutoSize = true;
            this.chkCN3.Location = new System.Drawing.Point(179, 58);
            this.chkCN3.Name = "chkCN3";
            this.chkCN3.Size = new System.Drawing.Size(47, 17);
            this.chkCN3.TabIndex = 1;
            this.chkCN3.Tag = "3";
            this.chkCN3.Text = "CN3";
            this.chkCN3.UseVisualStyleBackColor = true;
            // 
            // chkCN4
            // 
            this.chkCN4.AutoSize = true;
            this.chkCN4.Location = new System.Drawing.Point(232, 58);
            this.chkCN4.Name = "chkCN4";
            this.chkCN4.Size = new System.Drawing.Size(47, 17);
            this.chkCN4.TabIndex = 1;
            this.chkCN4.Tag = "4";
            this.chkCN4.Text = "CN4";
            this.chkCN4.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 124);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(827, 321);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Random 6 ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtRan
            // 
            this.txtRan.Location = new System.Drawing.Point(528, 54);
            this.txtRan.Name = "txtRan";
            this.txtRan.Size = new System.Drawing.Size(131, 20);
            this.txtRan.TabIndex = 4;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(414, 72);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // frmRandomTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 445);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txtRan);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.chkCN4);
            this.Controls.Add(this.chkCN3);
            this.Controls.Add(this.chkCN2);
            this.Controls.Add(this.chkCN1);
            this.Controls.Add(this.btClickMe);
            this.Name = "frmRandomTime";
            this.Text = "frmRandomTime";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btClickMe;
        private System.Windows.Forms.CheckBox chkCN1;
        private System.Windows.Forms.CheckBox chkCN2;
        private System.Windows.Forms.CheckBox chkCN3;
        private System.Windows.Forms.CheckBox chkCN4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtRan;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}