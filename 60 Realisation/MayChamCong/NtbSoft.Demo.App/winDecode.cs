using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace NtbSoft.Demo.App
{
    public partial class MainForm : Form
    {
        private NtbSoft.Security.Security Sec;

        public MainForm()
        {
            InitializeComponent();
            this.Sec = new NtbSoft.Security.Security();
        }

        private void cmdencode_Click(object sender, EventArgs e)
        {
            string StrCode = "", StrKey = "";

            StrCode = this.txtinfos.Text.Trim();
            StrKey = this.txtkey_code.Text.Trim();
            //this.txtresult.Text = Sec.EndcodeSqlString(StrCode, StrKey);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            String[] arr_str = new String[1024];
            try
            {
                string FILE_NAME = "licence.lic";
                this.txtinfos.Text = Sec.DecodeSqlString(FILE_NAME);
                this.lblpath_file_security.Text = FILE_NAME;
                //this.txtkey_code.Text = Sec.DecodeStrKey(FILE_NAME);

                arr_str = Sec.DecodeSql_Array_String(FILE_NAME);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not load file licence.lic");
            }

            string str_sql = arr_str[0];
        }

        private string palth_security;
        private void cmdbrowse_Click(object sender, EventArgs e)
        {
            this.palth_security = "";

            OpenFileDialog myDialog = new OpenFileDialog();

            myDialog.Filter = ".lic file (*.lic)|*.lic|All file(*.*)|*.*";

            if (myDialog.ShowDialog() == DialogResult.OK)
            {
                palth_security = myDialog.FileName;
            }
            this.lblpath_file_security.Text = palth_security;

            if (palth_security != "")
            {
                this.txtinfos.Text = Sec.DecodeSqlString(palth_security);
                this.txtkey_code.Text = Sec.DecodeStrKey(palth_security);
                this.txtresult.Text = "";
            }

        }

        private string path_save;
        private void cmdsave_Click(object sender, EventArgs e)
        {
            // Chọn file lưu trữ thông tin
            this.path_save = "";
            SaveFileDialog myDialogsave = new SaveFileDialog();

            myDialogsave.Filter = ".lic file (*.lic)|*.lic|All file(*.*)|*.*";

            // Tạo nội dung lưu trữ bao gồm phần khóa và phần mã hóa
            string StrCodingResult = "";
            string _keycode = "";
            string un_keycode = "";
            
   
            // Tạo phần khóa đảo ngược với những ký tự gõa vào
            _keycode = (string) this.txtkey_code.Text;

            if (_keycode != "")
            {
                for (int s = 0; s < _keycode.Length; s++)
                {
                    un_keycode = _keycode.Substring(s, 1) + un_keycode;
                }
                StrCodingResult = "#" + un_keycode + "#";
            }

            if (myDialogsave.ShowDialog() == DialogResult.OK)            
                path_save = myDialogsave.FileName;
            
            this.lblpath_file_save.Text = path_save;
            // Lưu đoạn cấu hình đã được mã hóa vào file "*.SHQ"

            if (path_save != "")
            {
                StreamWriter strw = new StreamWriter(path_save);
                StrCodingResult = StrCodingResult + this.txtresult.Text;
                strw.Write((string)(StrCodingResult));
                strw.Flush();
                strw.Close();
            }
                //EndcodeSqlString(path_save);
        }

        private void lblpath_file_save_Click(object sender, EventArgs e)
        {

        }

        private void cmd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}