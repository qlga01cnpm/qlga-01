﻿using LicenseSpot.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App
{
    public partial class Form1 : Form
    {
        string key = "2giotoitaigoccayda";
        ExtendedLicense license;
        bool isActivated;
        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            try
            {
                var kq = NtbSoft.ERP.Libs.MD5Password.getMd5Hash("@dmin@338");
                kq = Encrypt("0adf asdf asdfasdfasd dsf");
                var kq1 = kq;
                var kq2 = NtbSoft.ERP.Libs.CryptorEnginePro.Decrypt("bQB/dy2onOKQI+3VZPqsJQ==");
                var k = kq2;

                this.license = ExtendedLicenseManager.GetLicense(typeof(Form1), this, "PUBLIC_KEY"); //paste your public key from Step 6 here

                // Check if we're activated, and every 90 days verify it with
                // the activation servers. 
                GenuineResult result = license.IsGenuineEx();

                isActivated = result == GenuineResult.Genuine ||
                    // an internet error means the user is activated but
                    // IPManager failed to contact the LicenseSpot servers
                              result == GenuineResult.InternetError;

                if (result == GenuineResult.InternetError)
                {
                    //TODO: give the user the option to retry the genuine checking
                    //      immediately. For example a dialog box. In the dialog
                    //      call IsGenuineEx() to retry immediately.
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to check if activated: " + ex.Message);
            }
        }



        /// <summary>
        /// Mã hóa chuỗi có mật khẩu
        /// </summary>
        /// <param name="toEncrypt">Chuỗi cần mã hóa</param>
        /// <returns>Chuỗi đã mã hóa</returns>
        public string Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public string Decrypt(string toDecrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}
