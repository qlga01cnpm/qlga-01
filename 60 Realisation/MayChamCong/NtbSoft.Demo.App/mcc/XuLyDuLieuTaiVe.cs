﻿using gcc;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vnisc2;

namespace NtbSoft.Demo.App.mcc
{
    internal sealed class XuLyDuLieuTaiVe
    {
        private static string qr;

        public static bool fcnCheckLock(string tenBang)
        {
            bool result = false;
            clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT * FROM tblKhoaBang WHERE TenBang='" + tenBang + "' AND chkLock=1";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["chkLock"].ToString());
            }
            return result;
        }

        public static bool fcnCheckTonTaiBang(string tenBang)
        {
            DataSet dataSet = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            string sSQL = "select * from sysobjects where name= '" + tenBang + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count == 0;
            return !flag;
        }

        public static double fcnDateDiff(string D1, string D2)
        {
            double result = 0.0;
            bool flag = Information.IsDate(D1) & Information.IsDate(D2);
            if (flag)
            {
                result = (double)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(D1), Conversions.ToDate(D2), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
            }
            return result;
        }

        public static string fcnGioQuet1(string TenBang, string MaCC, string Ngay, string Gio)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            string text = string.Concat(new string[]
			{
				"SELECT GioQuet as Gio FROM ",
				TenBang,
				" WHERE MaCC='",
				MaCC,
				"' AND WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103)\r\n"
			});
            text = text + " AND DateDiff(mi,GioQuet,'" + Gio + "')>=0 order by GioQuet DESC";
            dataSet = clsDal.fcnExecQuery(text, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnGioQuet2(string TenBang, string MaCC, string Ngay, string Gio)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            string text = string.Concat(new string[]
			{
				"SELECT GioQuet as Gio FROM ",
				TenBang,
				" WHERE MaCC='",
				MaCC,
				"' AND WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103)\r\n"
			});
            text = text + " AND DateDiff(mi,'" + Gio + "',GioQuet)>=0";
            dataSet = clsDal.fcnExecQuery(text, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static bool fcnCheckInRaMay()
        {
            bool result = false;
            clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT * FROM tblQuyTac ";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                bool flag2 = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["InRaMay"].ToString());
                if (flag2)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool fcnBanGhiHopLe(string TenBang, string MaCC, string Ngay, string Gio)
        {
            bool result = false;
            DataSet dataSet = new DataSet();
            DataSet dataSet2 = new DataSet();
            DataSet dataSet3 = new DataSet();
            DataSet dataSet4 = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            string text = XuLyDuLieuTaiVe.fcnGioQuet1(TenBang, MaCC, Ngay, Gio);
            string text2 = XuLyDuLieuTaiVe.fcnGioQuet2(TenBang, MaCC, Ngay, Gio);
            bool flag = !Information.IsDate(text) & !Information.IsDate(text2);
            if (flag)
            {
                result = true;
            }
            flag = (Information.IsDate(text) & !Information.IsDate(text2));
            bool flag2;
            if (flag)
            {
                flag2 = (XuLyDuLieuTaiVe.fcnDateDiff(text, Gio) > 10.0);
                if (flag2)
                {
                    result = true;
                }
            }
            flag2 = (!Information.IsDate(text) & Information.IsDate(text2));
            if (flag2)
            {
                flag = (XuLyDuLieuTaiVe.fcnDateDiff(Gio, text2) > 10.0);
                if (flag)
                {
                    result = true;
                }
            }
            flag2 = (Information.IsDate(text) & Information.IsDate(text2));
            if (flag2)
            {
                flag = (XuLyDuLieuTaiVe.fcnDateDiff(text, Gio) > 10.0 & XuLyDuLieuTaiVe.fcnDateDiff(Gio, text2) > 10.0);
                if (flag)
                {
                    result = true;
                }
            }
            return result;
        }

        public static string fcnTimeInOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 TimeIn FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeIn"].ToString();
            }
            return result;
        }

        public static string fcnBreakOutOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BreakOut FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakOut"].ToString();
            }
            return result;
        }

        public static string fcnBreakInOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BreakIn FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakIn"].ToString();
            }
            return result;
        }

        public static string fcnTimeOutOfShift(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 TimeOut FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeOut"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhMuon1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhMuon FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhMuon"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhMuon1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhMuon FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhMuon"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhMuon2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhMuon2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhMuon2"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhMuon2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhMuon2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhMuon2"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhSom1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhSom FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhSom"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhSom1(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhSom FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhSom"].ToString();
            }
            return result;
        }

        public static string fcnBatDauTinhSom2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 BatDauTinhSom2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BatDauTinhSom2"].ToString();
            }
            return result;
        }

        public static string fcnKetThucTinhSom2(string ShiftID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT Top 1 KetThucTinhSom2 FROM tblShiftDuty WHERE ShiftID=N'" + ShiftID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["KetThucTinhSom2"].ToString();
            }
            return result;
        }

        public static string fcnSCHFromEmpID(string EmpID)
        {
            string result = "";
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string sSQL = "SELECT SCHName FROM Employees WHERE EmployeeID=N'" + EmpID + "'";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["SCHName"].ToString();
            }
            return result;
        }

        public static int fcnLate_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            int result;
            if (flag)
            {
                result = 0;
            }
            else
            {
                int num = 0;
                clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [M",
					Ngay.ToString(),
					"] as VL FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["VL"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        num = Conversions.ToInteger(dataSet.Tables[0].Rows[0]["VL"].ToString().Trim());
                    }
                }
                result = num;
            }
            return result;
        }

        public static int fcnEarly_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            int result;
            if (flag)
            {
                result = 0;
            }
            else
            {
                int num = 0;
                clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [S",
					Ngay.ToString(),
					"] as VL FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["VL"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        num = Conversions.ToInteger(dataSet.Tables[0].Rows[0]["VL"].ToString().Trim());
                    }
                }
                result = num;
            }
            return result;
        }

        public static string fcnSCH_Day(string TenBang, string EmpID, int Ngay)
        {
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            string result;
            if (flag)
            {
                result = "";
            }
            else
            {
                string text = "";
                clsDal2 clsDal = new clsDal2("SQLSERVER");
                string sSQL = string.Concat(new string[]
				{
					"SELECT TOP 1 [",
					Ngay.ToString(),
					"] as Ngay FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                DataSet dataSet = new DataSet();
                dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    bool flag2 = dataSet.Tables[0].Rows[0]["Ngay"].ToString().Trim().Length > 0;
                    if (flag2)
                    {
                        text = dataSet.Tables[0].Rows[0]["Ngay"].ToString().Trim();
                    }
                }
                result = text;
            }
            return result;
        }

        public static bool fcnCheckExistInOut(string TenBang, string EmpID, string WorkingDay)
        {
            bool result = false;
            clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT * FROM ",
				TenBang,
				" WHERE EmployeeID=N'",
				EmpID,
				"' AND WorkingDay=Convert(DateTime,'",
				WorkingDay,
				"',103) "
			});
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = true;
            }
            return result;
        }

        public static bool fcnCheckOverNight(string TenBang, string EmpID, DateTime Workingday)
        {
            DataSet dataSet = new DataSet();
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            bool flag = false;
            bool flag2 = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(TenBang);
            bool result;
            if (flag2)
            {
                result = false;
            }
            else
            {
                XuLyDuLieuTaiVe.qr = string.Concat(new string[]
				{
					"SELECT TOP 1 [",
					Workingday.Day.ToString(),
					"] as Chk FROM ",
					TenBang,
					" WHERE EmployeeID=N'",
					EmpID,
					"'"
				});
                dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
                flag2 = (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0].Rows[0]["Chk"].ToString().Trim().Length > 0);
                if (flag2)
                {
                    bool flag3 = Conversions.ToBoolean(dataSet.Tables[0].Rows[0]["Chk"].ToString().Trim());
                    if (flag3)
                    {
                        flag = true;
                    }
                }
                result = flag;
            }
            return result;
        }

        public static int fcnSoPhutToiThieuGiuaHaiLanQuet()
        {
            int result = 0;
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT SoPhutGiuaHaiLan FROM tblQuyTac";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["SoPhutGiuaHaiLan"].ToString())));
            }
            return result;
        }

        public static int fcnSoLanQuetThe(string TenBang, string CardID, string Ngay)
        {
            int result = 0;
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT COUNT(WorkingDay) as SL FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkHopLe=1"
			});
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["SL"].ToString())));
            }
            return result;
        }

        public static string fcnMinGioQuet(string TenBang, string CardID, string Ngay)
        {
            string result = "";
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT Min(GioQuet) as Gio FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkUsed=0"
			});
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnMaxGioQuet(string TenBang, string CardID, string Ngay)
        {
            string result = "";
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = string.Concat(new string[]
			{
				"SELECT Max(GioQuet) as Gio FROM ",
				TenBang,
				" WHERE WorkingDay=Convert(DateTime,'",
				Ngay,
				"',103) AND MaCC='",
				CardID,
				"' AND chkUsed=0"
			});
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            }
            return result;
        }

        public static string fcnBreakOutGioQuet(string TenBang, string CardID, string Ngay, string MinGioQuet, string MaxGioQuet)
        {
            string result = "";
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            bool flag = Information.IsDate(MinGioQuet) & Information.IsDate(MaxGioQuet);
            if (flag)
            {
                string text = string.Concat(new string[]
				{
					"SELECT Min(GioQuet) as Gio FROM ",
					TenBang,
					" WHERE WorkingDay=Convert(DateTime,'",
					Ngay,
					"',103) AND MaCC='",
					CardID,
					"'\r\n"
				});
                text = string.Concat(new string[]
				{
					text,
					" AND GioQuet >'",
					MinGioQuet,
					"' AND GioQuet < '",
					MaxGioQuet,
					"' AND chkUsed=0"
				});
                dataSet = clsDal.fcnExecQuery(text, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
                }
            }
            return result;
        }

        public static string fcnBreakInGioQuet(string TenBang, string CardID, string Ngay, string MinGioQuet, string MaxGioQuet, string BreakOutGioQuet)
        {
            string result = "";
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            bool flag = Information.IsDate(MaxGioQuet) & Information.IsDate(BreakOutGioQuet);
            if (flag)
            {
                string text = string.Concat(new string[]
				{
					"SELECT Max(GioQuet) as Gio FROM ",
					TenBang,
					" WHERE WorkingDay=Convert(DateTime,'",
					Ngay,
					"',103) AND MaCC='",
					CardID,
					"'\r\n"
				});
                text = string.Concat(new string[]
				{
					text,
					" AND GioQuet < '",
					MaxGioQuet,
					"'  AND GioQuet > '",
					BreakOutGioQuet,
					"' AND chkUsed=0"
				});
                dataSet = clsDal.fcnExecQuery(text, "tbl");
                flag = (dataSet.Tables[0].Rows.Count > 0);
                if (flag)
                {
                    result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
                }
            }
            return result;
        }

        public static string fcnEIDFromEmpID(string EmpID)
        {
            string result = "";
            clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE EmployeeID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            return result;
        }

        public static string fcnEmpIDFromeID(string EmpID)
        {
            string result = "";
            clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT EmployeeID FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["EmployeeID"].ToString();
            }
            return result;
        }

        public static string fcnNgayVaoFromEID(string EmpID)
        {
            string result = "";
            clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT BeginningDate FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BeginningDate"].ToString();
            }
            return result;
        }

        public static string fcnNgayNVFromEID(string EmpID)
        {
            string text = "01/01/1900";
            clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT StopWorkingDay FROM Employees WHERE eID=N'" + EmpID + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                text = dataSet.Tables[0].Rows[0]["StopWorkingDay"].ToString();
            }
            flag = (text.Trim().Length == 0);
            if (flag)
            {
                text = "01/01/1900";
            }
            return text;
        }

        public static string fcnEIDFromMaCC(string MaCC)
        {
            string result = "";
            clsDal clsDal = new clsDal("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE MaCC=N'" + MaCC + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            return result;
        }

        public static string fcnReturnEmpID(string CardID)
        {
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT eID FROM Employees WHERE MaCC='" + CardID + "'";
            DataSet dataSet = new DataSet();
            dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["eID"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnStrShiftID(string SCH)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT strShift FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["strShift"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift3(string SCH)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift3 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift3"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift1(string SCH)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift1 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift1"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static string fcnShift2(string SCH)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT Shift2 FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            string result;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["Shift2"].ToString();
            }
            else
            {
                result = "";
            }
            return result;
        }

        public static int fcnNumberShift(string SCH)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            XuLyDuLieuTaiVe.qr = "SELECT NumberShift FROM Schedule WHERE SCHName='" + SCH + "'";
            DataSet dataSet = new DataSet();
            dataSet = clsDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            int result;
            if (flag)
            {
                result = checked((int)Math.Round(Conversion.Val(dataSet.Tables[0].Rows[0]["NumberShift"].ToString())));
            }
            else
            {
                result = 0;
            }
            return result;
        }

        public static string fcnGioKetThucCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 TimeOut FROM tblShiftDuty WHERE ShiftID='" + shiftID + "' ";
            DataSet dataSet = new DataSet();
            dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeOut"].ToString();
            }
            return result;
        }

        public static string fcnGioVaoGiuaCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 BreakIn FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakIn"].ToString();
            }
            return result;
        }

        public static string fcnGioRaGiuaCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 BreakOut FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["BreakOut"].ToString();
            }
            return result;
        }

        public static string fcnGioBatDauVaoCa(string shiftID)
        {
            string result = "";
            DataTable dataTable = new DataTable();
            XuLyDuLieuTaiVe.qr = "SELECT top 1 TimeIn FROM tblShiftDuty WHERE ShiftID='" + shiftID + "'";
            DataSet dataSet = new DataSet();
            dataSet = StartUp.objDal.fcnExecQuery(XuLyDuLieuTaiVe.qr, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = dataSet.Tables[0].Rows[0]["TimeIn"].ToString();
            }
            return result;
        }

        public static string GetShift(string strShift, string TimeIn)
        {
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            string result = "***";
            bool flag = Information.IsDate(TimeIn);
            checked
            {
                if (flag)
                {
                    bool flag2 = Strings.InStr(strShift, ",", CompareMethod.Binary) > 0;
                    if (flag2)
                    {
                        string[] array = Strings.Split(strShift, ",", -1, CompareMethod.Binary);
                        flag2 = (Information.UBound(array, 1) == 1);
                        bool flag3;
                        if (flag2)
                        {
                            result = Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2);
                        }
                        else
                        {
                            flag2 = (array[0].Trim().Length > 0 & Operators.CompareString(array[0].Trim(), "''", false) != 0);
                            int value = 0;
                            if (flag2)
                            {
                                result = Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2);
                                value = (int)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(Strings.Mid(array[0].Trim(), 2, Strings.Len(array[0].Trim()) - 2))), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
                            }
                            int arg_146_0 = Information.LBound(array, 1);
                            int num = Information.UBound(array, 1);
                            int num2 = arg_146_0;
                            while (true)
                            {
                                int arg_1F9_0 = num2;
                                int num3 = num;
                                if (arg_1F9_0 > num3)
                                {
                                    break;
                                }
                                string text = Strings.Mid(array[num2].Trim(), 2, Strings.Len(array[num2].Trim()) - 2);
                                flag2 = (text.Trim().Length > 0);
                                if (flag2)
                                {
                                    flag = Information.IsDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text));
                                    if (flag)
                                    {
                                        flag3 = (unchecked((long)Math.Abs(value)) > Math.Abs(DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text)), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1)));
                                        if (flag3)
                                        {
                                            value = (int)DateAndTime.DateDiff(DateInterval.Minute, Conversions.ToDate(XuLyDuLieuTaiVe.fcnGioBatDauVaoCa(text)), Conversions.ToDate(TimeIn), FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
                                            result = text;
                                        }
                                    }
                                }
                                num2++;
                            }
                        }
                        flag3 = (strShift.Trim().Length == 17);
                        if (flag3)
                        {
                            result = "***";
                        }
                    }
                    else
                    {
                        bool flag3 = strShift.Trim().Length > 0;
                        if (flag3)
                        {
                            result = strShift;
                        }
                        flag3 = (strShift.Trim().Length == 17);
                        if (flag3)
                        {
                            result = "***";
                        }
                    }
                }
                else
                {
                    result = "***";
                }
                return result;
            }
        }

        public static void XuLyDuLieuQuetThe(string MaCC, DateTime CurDay)
        {
            //Cursor.Current = Cursors.WaitCursor;
            string text = "";
            string _strTimeOut = "";
            string text3 = "";
            clsDal2 clsDal = new clsDal2("SQLSERVER");
            DataSet dataSet = new DataSet();
            string text4_tblTrangThaiQuetTheTMP = "tblTrangThaiQuetTheTMP";
            string text5_TrangThaiQuetThe = "TrangThaiQuetThe" + CurDay.ToString("MMyyyy");
            string text6_TrangThaiQuetThe = "TrangThaiQuetThe" + DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy"))).ToString("MMyyyy");
            bool flag = !XuLyDuLieuTaiVe.fcnCheckTonTaiBang(text5_TrangThaiQuetThe);
            checked
            {
                if (!flag)
                {
                    string tenBang = "tblRegisterOverNight_" + CurDay.ToString("MMyyyy");
                    string tenBang2 = "tblRegisterOverNight_" + CurDay.AddMonths(-1).ToString("MMyyyy");
                    string text7_TimeIn_TimeOutReal = "TimeIn_TimeOutReal" + CurDay.ToString("MMyyyy");
                    clsDal.fcnExecNonQuery("Exec sp_TaoBangInOut '" + text7_TimeIn_TimeOutReal + "'");
                    string text8 = "tblRegisterSCH_Day_" + CurDay.ToString("MMyyyy");
                    string text9 = "DELETE FROM " + text4_tblTrangThaiQuetTheTMP;
                    clsDal.fcnExecNonQuery(text9);
                    flag = (CurDay.Day == DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy")).AddDays(-1.0)).Day);
                    if (flag)
                    {
                        bool flag2 = XuLyDuLieuTaiVe.fcnCheckTonTaiBang(text6_TrangThaiQuetThe);
                        if (flag2)
                        {
                            text9 = "INSERT INTO " + text4_tblTrangThaiQuetTheTMP + "(MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe)";
                            text9 += " SELECT MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe";
                            text9 = text9 + " FROM " + text6_TrangThaiQuetThe;
                            text9 = text9 + " WHERE  MaCC='" + MaCC + "' ";
                            text9 = text9 + " AND WorkingDay ='" + DateAndTime.DateAdd(DateInterval.Month, 1.0, Conversions.ToDate("01/" + CurDay.ToString("MM/yyyy"))).ToString("yyyyMMdd") + "'";
                            clsDal.fcnExecNonQuery(text9);
                        }
                    }
                    text9 = "INSERT INTO " + text4_tblTrangThaiQuetTheTMP + "(MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe)";
                    text9 += " SELECT MaCC,WorkingDay,GioQuet,TrangThai,Door,strDateTime,chkUsed,chkHopLe";
                    text9 = text9 + " FROM " + text5_TrangThaiQuetThe;
                    text9 = text9 + " WHERE chkHopLe=1 AND MaCC='" + MaCC + "' ";
                    text9 = text9 + " AND WorkingDay >='" + CurDay.AddDays(-1.0).ToString("yyyyMMdd") + "'";
                    text9 = text9 + " AND WorkingDay <='" + CurDay.AddDays(1.0).ToString("yyyyMMdd") + "'";
                    clsDal.fcnExecNonQuery(text9);
                    DataSet dataSet2 = new DataSet();
                    text9 = string.Concat(new string[]
					{
						"SELECT DISTINCT *,DAY(WorkingDay) as iNgay, MONTH(WorkingDAy) as iThang , YEAR(WorkingDAy) as iNam FROM ",
						text4_tblTrangThaiQuetTheTMP,
						" WHERE MaCC='",
						MaCC,
						"' AND DAY(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Day),
						"'  AND MONTH(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Month),
						"'  AND YEAR(WorkingDay) = '",
						Conversions.ToString(CurDay.Date.Year),
						"' Order by MaCC,WorkingDay,GioQuet"
					});
                    dataSet2 = clsDal.fcnExecQuery(text9, "tbl");
                    int arg_485_0 = 0;
                    int num = dataSet2.Tables[0].Rows.Count - 1;
                    int num2 = arg_485_0;
                    while (true)
                    {
                        int arg_1861_0 = num2;
                        int num3 = num;
                        if (arg_1861_0 > num3)
                        {
                            break;
                        }
                        string Workingday = dataSet2.Tables[0].Rows[num2]["Workingday"].ToString();
                        bool flag2 = Information.IsDate(text3);
                        if (!flag2)
                        {
                            goto IL_4E5;
                        }
                        flag = (DateTime.Compare(Conversions.ToDate(Workingday), Conversions.ToDate(text3)) == 0);
                        if (!flag)
                        {
                            goto IL_4E5;
                        }
                    IL_1851:
                        num2++;
                        continue;
                    IL_4E5:
                        DateTime dateTime = Conversions.ToDate(Strings.Left(Workingday, 10));
                        string text11 = Strings.Format(dateTime, "dd/MM/yyyy");
                        int num4 = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iNgay"].ToString());
                        int value = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iThang"].ToString());
                        int value2 = Conversions.ToInteger(dataSet2.Tables[0].Rows[num2]["iNam"].ToString());
                        string text12 = dataSet2.Tables[0].Rows[num2]["GioQuet"].ToString();
                        string text13 = Strings.Format(Conversions.ToDate(Workingday), "yyyy-MM-dd ") + text12;
                        string text14 = XuLyDuLieuTaiVe.fcnReturnEmpID(MaCC);
                        DateTime dateTime2 = DateAndTime.DateAdd(DateInterval.Day, -1.0, Conversions.ToDate(Workingday));
                        DateTime dateTime3 = DateAndTime.DateAdd(DateInterval.Day, 1.0, Conversions.ToDate(Workingday));
                        flag2 = (num4 == 1);
                        bool flag3;
                        if (flag2)
                        {
                            flag = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang2, text14, dateTime2);
                            if (flag)
                            {
                                flag3 = (XuLyDuLieuTaiVe.fcnDateDiff("01:00", text12) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12, "09:00") >= 0.0);
                                if (flag3)
                                {
                                    text9 = string.Concat(new string[]
									{
										"DELETE FROM ",
										text4_tblTrangThaiQuetTheTMP,
										" WHERE MaCC='",
										MaCC,
										"' AND DAY(WorkingDay) = '",
										Conversions.ToString(num4),
										"'  AND MONTH(WorkingDay) = '",
										Conversions.ToString(value),
										"'  AND YEAR(WorkingDay) = '",
										Conversions.ToString(value2),
										"' AND GioQuet='",
										text12.Trim(),
										"'"
									});
                                    clsDal.fcnExecNonQuery(text9);
                                }
                            }
                        }
                        text9 = "UPDATE " + text7_TimeIn_TimeOutReal + " SET TimeIn='',BreakOut='',BreakIn='', TimeOut=''";
                        text9 += ", strTimeIn=''";
                        text9 += ", strBreakOut=''";
                        text9 += ", strBreakIn=''";
                        text9 += ", strTimeOut=''";
                        text9 = string.Concat(new string[]
						{
							text9,
							" WHERE EmployeeID = N'",
							text14,
							"' AND WorkingDay='",
							Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
							"'"
						});
                        clsDal.fcnExecNonQuery(text9);
                        int num5_SoLanQuetThe = XuLyDuLieuTaiVe.fcnSoLanQuetThe(text4_tblTrangThaiQuetTheTMP, MaCC, text11);
                        string text15_MinGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11);
                        string text16_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11);
                        string text17_BreakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11, text15_MinGioQuet, text16_MaxGioQuet);
                        string text18_BreakInGioQuet = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, text11, text15_MinGioQuet, text16_MaxGioQuet, text17_BreakOutGioQuet);
                        
                        /*** Trường hợp số lần chấm công = 1 ***/
                        flag3 = (num5_SoLanQuetThe == 1);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, dateTime2);
                            if (flag2)
                            {
                                flag = (XuLyDuLieuTaiVe.fcnDateDiff("01:00", text12) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12, "09:00") >= 0.0);
                                if (flag)
                                {
                                    text15_MinGioQuet = "";
                                }
                            }
                            flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, Conversions.ToDate(Workingday));
                            if (flag3)
                            {
                                flag2 = (XuLyDuLieuTaiVe.fcnDateDiff("18:00", text12) >= 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(text12, "23:00") >= 0.0);
                                if (flag2)
                                {
                                    text15_MinGioQuet = text12;
                                }
                                text17_BreakOutGioQuet = "";
                                text18_BreakInGioQuet = "";
                            }
                            else
                            {
                                text16_MaxGioQuet = "";
                                text17_BreakOutGioQuet = "";
                                text18_BreakInGioQuet = "";
                            }
                        }
                        /*** Trường hợp số lần chấm công = 2 ***/
                        flag3 = (num5_SoLanQuetThe == 2);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, dateTime2);
                            if (flag2)
                            {
                                text15_MinGioQuet = text16_MaxGioQuet;
                                text16_MaxGioQuet = "";
                            }
                        }

                        /*** Trường hợp số lần chấm công = 3 ***/
                        flag3 = (num5_SoLanQuetThe == 3);
                        if (flag3)
                        {
                            flag2 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, dateTime2);
                            if (flag2)
                            {
                                text15_MinGioQuet = text17_BreakOutGioQuet;
                                text17_BreakOutGioQuet = "";
                            }
                            else
                            {
                                text18_BreakInGioQuet = text16_MaxGioQuet;
                                text16_MaxGioQuet = "";
                            }
                        }


                        flag3 = Information.IsDate(text15_MinGioQuet);
                        string _strTimeIn;
                        if (flag3)
                        {
                            _strTimeIn = Strings.Format(Conversions.ToDate(Workingday), "yyyy-MM-dd ") + text15_MinGioQuet;
                        }
                        else
                        {
                            _strTimeIn = "";
                        }
                        flag3 = Information.IsDate(text17_BreakOutGioQuet);
                        string _strBreakOut;
                        if (flag3)
                        {
                            _strBreakOut = Strings.Format(Conversions.ToDate(Workingday), "yyyy-MM-dd ") + text17_BreakOutGioQuet;
                        }
                        else
                        {
                            _strBreakOut = "";
                        }
                        flag3 = Information.IsDate(text18_BreakInGioQuet);
                        string _strBreakIn;
                        if (flag3)
                        {
                            _strBreakIn = Strings.Format(Conversions.ToDate(Workingday), "yyyy-MM-dd ") + text18_BreakInGioQuet;
                        }
                        else
                        {
                            _strBreakIn = "";
                        }
                        flag3 = Information.IsDate(text16_MaxGioQuet);
                        string text22;
                        if (flag3)
                        {
                            text22 = Strings.Format(Conversions.ToDate(Workingday), "yyyy-MM-dd ") + text16_MaxGioQuet;
                        }
                        else
                        {
                            text22 = "";
                        }
                        string minGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2.ToString("dd/MM/yyyy"));
                        string text23_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2.ToString("dd/MM/yyyy"));
                        flag3 = Information.IsDate(text23_MaxGioQuet);
                        if (flag3)
                        {
                            text = Strings.Format(dateTime2, "yyyy-MM-dd ") + text23_MaxGioQuet;
                        }
                        string breakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2.ToString("dd/MM/yyyy"), minGioQuet, text23_MaxGioQuet);
                        string text24_BreakInGioQuet = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime2.ToString("dd/MM/yyyy"), minGioQuet, text23_MaxGioQuet, breakOutGioQuet);
                        string text25_MinGioQuet = XuLyDuLieuTaiVe.fcnMinGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3.ToString("dd/MM/yyyy"));
                        string text26_MaxGioQuet = XuLyDuLieuTaiVe.fcnMaxGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3.ToString("dd/MM/yyyy"));
                        string text27_BreakOutGioQuet = XuLyDuLieuTaiVe.fcnBreakOutGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3.ToString("dd/MM/yyyy"), text25_MinGioQuet, text26_MaxGioQuet);
                        string str_BreakInGioQuet = XuLyDuLieuTaiVe.fcnBreakInGioQuet(text4_tblTrangThaiQuetTheTMP, MaCC, dateTime3.ToString("dd/MM/yyyy"), text25_MinGioQuet, text26_MaxGioQuet, text27_BreakOutGioQuet);
                        flag3 = Information.IsDate(text25_MinGioQuet);
                        if (flag3)
                        {
                            _strTimeOut = Strings.Format(dateTime3, "yyyy-MM-dd ") + text25_MinGioQuet;
                        }
                        string text28 = Strings.Format(dateTime3, "yyyy-MM-dd ") + text27_BreakOutGioQuet;
                        string text29 = Strings.Format(dateTime3, "yyyy-MM-dd ") + str_BreakInGioQuet;
                        string text30 = Strings.Format(dateTime3, "yyyy-MM-dd ") + text26_MaxGioQuet;
                        flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, Conversions.ToDate(Workingday).AddDays(-1.0));
                        if (flag3)
                        {
                            flag2 = ((num5_SoLanQuetThe == 1 && Information.IsDate(text) && XuLyDuLieuTaiVe.fcnDateDiff(text, text13) <= 780.0) & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text13)), Conversions.ToString(Conversions.ToDate("10:00"))) > 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text13)), Conversions.ToString(Conversions.ToDate("04:00"))) <= 0.0);
                            if (flag2)
                            {
                                text15_MinGioQuet = "";
                                _strTimeIn = "";
                            }
                        }
                        flag3 = XuLyDuLieuTaiVe.fcnCheckOverNight(tenBang, text14, Conversions.ToDate(Workingday));
                        if (flag3)
                        {
                            flag2 = Information.IsDate(text25_MinGioQuet);
                            if (flag2)
                            {
                                flag = (XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text25_MinGioQuet)), Conversions.ToString(Conversions.ToDate("19:00"))) < 0.0 & XuLyDuLieuTaiVe.fcnDateDiff(Conversions.ToString(Conversions.ToDate(text25_MinGioQuet)), Conversions.ToString(Conversions.ToDate("23:59"))) >= 0.0);
                                if (flag)
                                {
                                    text25_MinGioQuet = "";
                                    _strTimeOut = "";
                                }
                            }
                            flag3 = XuLyDuLieuTaiVe.fcnCheckExistInOut(text7_TimeIn_TimeOutReal, text14, Conversions.ToDate(Workingday).ToString("dd/MM/yyyy"));
                            if (flag3)
                            {
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text7_TimeIn_TimeOutReal,
									" SET TimeIn='",
									text15_MinGioQuet,
									"',BreakOut='",
									text17_BreakOutGioQuet,
									"',BreakIn='",
									text18_BreakInGioQuet,
									"', TimeOut='",
									text25_MinGioQuet,
									"'"
								});
                                text9 = text9 + ", strTimeIn='" + _strTimeIn + "'";
                                text9 = text9 + ", strBreakOut='" + _strBreakOut + "'";
                                text9 = text9 + ", strBreakIn='" + _strBreakIn + "'";
                                text9 = text9 + ", strTimeOut='" + _strTimeOut + "'";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE EmployeeID = N'",
									text14,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"'"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"')"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = "UPDATE " + text4_tblTrangThaiQuetTheTMP + " SET chkUsed=1 \r\n";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									dateTime3.ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = text9 + " AND GioQuet IN ('" + text25_MinGioQuet + "')";
                                clsDal.fcnExecNonQuery(text9);
                            }
                            else
                            {
                                text9 = "INSERT INTO " + text7_TimeIn_TimeOutReal + "(EmployeeID,WorkingDay,TimeIn,BreakOut,BreakIn,TimeOut\r\n";
                                text9 += ",strTimeIn,strBreakOut,strBreakIn,strTimeOut";
                                text9 += ",SCHName,strShiftID,Shift";
                                text9 += ",sTimeIn,sBreakOut,sBreakIn,sTimeOut";
                                text9 += ",ModifyDate";
                                text9 += ")";
                                text9 = string.Concat(new string[]
								{
									text9,
									" SELECT N'",
									text14,
									"',Convert(DateTime,'",
									Conversions.ToDate(Workingday).ToString("dd/MM/yyyy"),
									"',103),'",
									text15_MinGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text25_MinGioQuet,
									"'\r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" ,'",
									_strTimeIn,
									"','",
									_strBreakOut,
									"','",
									_strBreakIn,
									"','",
									_strTimeOut,
									"'"
								});
                                string text31 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",N'",
									text31,
									"',dbo.fcnStrShiftFromSCH('",
									text31,
									"'),dbo.fcnShiftFromSCH('",
									text31,
									"')"
								});
                                string text32 = "";
                                string text33 = "";
                                string text34 = "";
                                string text35 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",'",
									text32,
									"','",
									text33,
									"','",
									text34,
									"','",
									text35,
									"','",
									DateAndTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
									"'"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"')"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = "UPDATE " + text4_tblTrangThaiQuetTheTMP + " SET chkUsed=1 \r\n";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									dateTime3.ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = text9 + " AND GioQuet IN ('" + text25_MinGioQuet + "')";
                                clsDal.fcnExecNonQuery(text9);
                            }
                        }
                        else
                        {
                            flag3 = XuLyDuLieuTaiVe.fcnCheckExistInOut(text7_TimeIn_TimeOutReal, text14, Conversions.ToDate(Workingday).ToString("dd/MM/yyyy"));
                            if (flag3)
                            {
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text7_TimeIn_TimeOutReal,
									" SET TimeIn='",
									text15_MinGioQuet,
									"',BreakOut='",
									text17_BreakOutGioQuet,
									"',BreakIn='",
									text18_BreakInGioQuet,
									"',TimeOut='",
									text16_MaxGioQuet,
									"'"
								});
                                text9 = text9 + ", strTimeIn='" + text22 + "'";
                                text9 = text9 + ", strBreakOut='" + _strBreakOut + "'";
                                text9 = text9 + ", strBreakIn='" + _strBreakIn + "'";
                                text9 = text9 + ", strTimeOut='" + text22 + "'";
                                text9 = string.Concat(new string[]
								{
									text9,
									" WHERE EmployeeID = N'",
									text14,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"'"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text15_MinGioQuet,
									"')"
								});
                                clsDal.fcnExecNonQuery(text9);
                            }
                            else
                            {
                                flag3 = (num5_SoLanQuetThe <= 1);
                                if (flag3)
                                {
                                    text16_MaxGioQuet = "";
                                    text22 = "";
                                }
                                text9 = "INSERT INTO " + text7_TimeIn_TimeOutReal + "(EmployeeID,WorkingDay,TimeIn,BreakOut,BreakIn,TimeOut\r\n";
                                text9 += ",strTimeIn,strBreakOut,strBreakIn,strTimeOut";
                                text9 += ",SCHName,strShiftID,Shift";
                                text9 += ",sTimeIn,sBreakOut,sBreakIn,sTimeOut";
                                text9 += ")";
                                text9 = string.Concat(new string[]
								{
									text9,
									" SELECT N'",
									text14,
									"',Convert(DateTime,'",
									text11,
									"',103),'",
									text15_MinGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text16_MaxGioQuet,
									"'\r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" ,'",
									_strTimeIn,
									"','",
									_strBreakOut,
									"','",
									_strBreakIn,
									"','",
									text22,
									"'"
								});
                                string text31 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",N'",
									text31,
									"',dbo.fcnStrShiftFromSCH('",
									text31,
									"'),dbo.fcnShiftFromSCH('",
									text31,
									"')"
								});
                                string text32 = "";
                                string text33 = "";
                                string text34 = "";
                                string text35 = "";
                                text9 = string.Concat(new string[]
								{
									text9,
									",'",
									text32,
									"','",
									text33,
									"','",
									text34,
									"','",
									text35,
									"'"
								});
                                clsDal.fcnExecNonQuery(text9);
                                text9 = string.Concat(new string[]
								{
									"UPDATE ",
									text4_tblTrangThaiQuetTheTMP,
									" SET chkUsed=1 WHERE MaCC='",
									MaCC,
									"' AND WorkingDay='",
									Conversions.ToDate(Workingday).ToString("yyyyMMdd"),
									"' \r\n"
								});
                                text9 = string.Concat(new string[]
								{
									text9,
									" AND GioQuet IN ('",
									text16_MaxGioQuet,
									"','",
									text17_BreakOutGioQuet,
									"','",
									text18_BreakInGioQuet,
									"','",
									text15_MinGioQuet,
									"')"
								});
                                clsDal.fcnExecNonQuery(text9);
                            }
                        }
                        text3 = Workingday;
                        goto IL_1851;
                    }
                    text9 = "DELETE FROM " + text4_tblTrangThaiQuetTheTMP + "";
                    text9 = string.Concat(new string[]
					{
						text9,
						" WHERE MaCC ='",
						MaCC,
						"' AND Workingday='",
						CurDay.ToString("yyyyMMdd"),
						"'"
					});
                    //clsDal.fcnExecNonQuery(text9);
                }
            }
        }
    }
}
