﻿using gcc;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Demo.App.mcc
{
    public class clsTemp
    {
        private System.Windows.Forms.DateTimePicker tuNgay = new System.Windows.Forms.DateTimePicker();
        string tableTBLName = "";
        public void LoadDataMonth(string strDept, string strEmpID)
        {
            tuNgay.Value = DateTime.Now;
            StartUp.qryEmployeesRP = "qryEmployees";
            tableTBLName = "qryEmployees";

            this.tableTBLName = "TBL" + Strings.Right("00" + Conversions.ToString(this.tuNgay.Value.Month), 2) + Conversions.ToString(this.tuNgay.Value.Year) + StartUp.sLastTblTBL;
            //this.TaoBangCong(this.tableTBLName);
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            System.Data.DataTable dataTable = new System.Data.DataTable();
            DataSet dataSet = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            dataTable.Clear();
            string text = "Select  [dbo].[fcnEmpIDFromID](A.EmployeeID) as EmployeeID, [dbo].[fcnEmpNameFromID](A.EmployeeID) as EmployeeName, [dbo].[fcnReturnNgayVao](A.EmployeeID),[dbo].[fcnReturnNgayThoiViec](A.EmployeeID),\r\n";
            int num = 1;
            checked
            {
                int arg_BD_0;
                int num2;
                do
                {
                    text = text + " rtrim(C" + num.ToString() + ")  ,";
                    num++;
                    arg_BD_0 = num;
                    num2 = 31;
                }
                while (arg_BD_0 <= num2);
                text += "Case when CongX >0 then CongX End\r\n";
                text += ",Case when CongK >0 then CongK End\r\n";
                text += ",Case when OTX >0 then OTX End\r\n";
                text += ",Case when OTK >0 then OTK End\r\n";
                text += ",Case when CongCNX >0 then CongCNX End\r\n";
                text += ",Case when CongCNK >0 then CongCNK End\r\n";
                text += ",Case when Cong30P >0 then Cong30P End\r\n";
                text += ",Case when CongLe >0 then CongLe End\r\n";
                text += ",Case when CongRo >0 then CongRo End\r\n";
                text += ",Case when CongR >0 then CongR End\r\n";
                text += ",Case when CongOm >0 then CongOm End\r\n";
                text += ",Case when CongConOm >0 then CongConOm End\r\n";
                text += ",Case when CongPhep >0 then CongPhep End\r\n";
                text += ",Case when CongKT >0 then CongKT End\r\n";
                text += ",Case when CongDe >0 then CongDe End\r\n";
                text += ",Case when CongTS >0 then CongTS End\r\n";
                text += ",Case when CongNB >0 then CongNB End\r\n";
                text += ",Case when CongCD >0 then CongCD End\r\n";
                text += ",Case when CongV0 >0 then CongV0 End\r\n";
                text += ",Case when CongP >0 then CongP End\r\n";
                text += ",Case when CongCT >0 then CongCT End\r\n";
                text += ",Case when CongAnTrua >0 then CongAnTrua End\r\n";
                text += ",Case when CongAnChieu >0 then CongAnChieu End\r\n";
                text += ",STTDeptID,E.STT as STT,Cast([dbo].[fcnEmpIDFromID](A.EmployeeID) as int) as EmpID";
                text = string.Concat(new string[]
                {
                    text,
                    " FROM   ",
                    this.tableTBLName,
                    " A INNER JOIN ",
                    StartUp.qryEmployeesRP,
                    " E ON A.EmployeeID=E.eID "
                });
                text += " WHERE ";
                bool flag = strDept.Length > 0;
                if (flag)
                {
                    text = text + " E.DepartmentID IN (" + strDept + ")";
                }
                flag = (strEmpID.Length > 0);
                if (flag)
                {
                    text = text + " And A.EmployeeID IN(" + strEmpID + ")";
                }
                sqlDataAdapter = new SqlDataAdapter(text, StartUp.sqlConn());
                sqlDataAdapter.Fill(dataTable);
                text = " Select E.EmployeeID,E.EmployeeName , [dbo].[fcnReturnNgayVao](E.EmployeeID),[dbo].[fcnReturnNgayThoiViec](E.EmployeeID),";
                int num3 = 1;
                int arg_2EC_0;
                do
                {
                    text += "0,";
                    num3++;
                    arg_2EC_0 = num3;
                    num2 = 31;
                }
                while (arg_2EC_0 <= num2);
                int num4 = 1;
                int arg_30E_0;
                do
                {
                    text += "0,";
                    num4++;
                    arg_30E_0 = num4;
                    num2 = 23;
                }
                while (arg_30E_0 <= num2);
                text += "STTDeptID,STT,Cast(E.EmployeeID as int) as EmpID";
                text = text + " FROM " + StartUp.qryEmployeesRP + " E WHERE  ";
                flag = (strDept.Length > 0);
                if (flag)
                {
                    text = text + " DepartmentID IN (" + strDept + ") AND E.eID ";
                    text = text + " NOT IN (Select EmployeeID From " + this.tableTBLName + " A Where  EmployeeID is not Null)";
                }
                flag = (strEmpID.Length > 0);
                if (flag)
                {
                    text = string.Concat(new string[]
                    {
                        text,
                        " E.eID IN (",
                        strEmpID,
                        ") AND E.eID NOT IN (Select EmployeeID From ",
                        this.tableTBLName,
                        " A Where EmployeeID is not Null)"
                    });
                }
                sqlDataAdapter = new SqlDataAdapter(text, StartUp.sqlConn());
                sqlDataAdapter.Fill(dataTable);
                //this.TabControl1.TabPages[0].Text = "Bảng chấm công (" + Conversions.ToString(this.SheetBangCong.RowCount) + ")";
                //this.SheetBangCong.ColumnCount = 59;
                //DataView defaultView = dataTable.DefaultView;
                //defaultView.Sort = " STTDeptID ,EmpID";
                //this.SheetBangCong.DataSource = defaultView;
                int num5 = 4;
                int arg_647_0;
                do
                {
                    flag = Information.IsDate(string.Concat(new string[]
                    {
                        Conversions.ToString(num5 - 3),
                        "/",
                        this.tuNgay.Value.Month.ToString(),
                        "/",
                        this.tuNgay.Value.Year.ToString()
                    }));
                    if (flag)
                    {
                        //this.SheetBangCong.Columns[num5].BackColor = Color.White;
                        flag = (Conversions.ToDate(string.Concat(new string[]
                        {
                            Conversions.ToString(num5 - 3),
                            "/",
                            this.tuNgay.Value.Month.ToString(),
                            "/",
                            this.tuNgay.Value.Year.ToString()
                        })).DayOfWeek == DayOfWeek.Sunday);
                        if (flag)
                        {
                            int arg_582_0 = 0;
                            int num6 = dataTable.Rows.Count - 1;
                            int num7 = arg_582_0;
                            while (true)
                            {
                                int arg_5C2_0 = num7;
                                num2 = num6;
                                if (arg_5C2_0 > num2)
                                {
                                    break;
                                }
                                bool flag2 = num7 % 2 == 0;
                                if (flag2)
                                {
                                    //this.SheetBangCong.Cells[num7, num5].BackColor = Color.LightSkyBlue;
                                }
                                num7++;
                            }
                            //this.SheetBangCong.Columns[num5].BackColor = Color.LightSkyBlue;
                        }
                        else
                        {
                            int arg_5F4_0 = 0;
                            int num8 = dataTable.Rows.Count - 1;
                            int num9 = arg_5F4_0;
                            while (true)
                            {
                                int arg_634_0 = num9;
                                num2 = num8;
                                if (arg_634_0 > num2)
                                {
                                    break;
                                }
                                bool flag2 = num9 % 2 == 0;
                                if (flag2)
                                {
                                    //this.SheetBangCong.Cells[num9, num5].BackColor = Color.AliceBlue;
                                }
                                num9++;
                            }
                        }
                    }
                    num5++;
                    arg_647_0 = num5;
                    num2 = 34;
                }
                while (arg_647_0 <= num2);
            }
        }
    }
}
