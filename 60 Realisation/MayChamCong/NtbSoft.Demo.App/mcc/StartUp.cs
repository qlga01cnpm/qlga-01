﻿using gcc;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Management;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App.mcc
{
    public sealed class StartUp
    {
        public static int login = 0;

        public static string sUsername = "";

        public static bool blnPrintRight = false;

        public static string strdoc = "";

        public static string strdoc1 = "";

        public static string strdoc2 = "";

        public static string strHardSNFromF = "";

        public static string strUSFromF = "";

        public static string strPWFromF = "";

        public static string strAutoLogon = "";

        public static int StatusLogin = 0;

        public static clsDal objDal = new clsDal("SQLSERVER");

        public static string server1;

        public static string dbName1;

        public static string user1;

        public static string pass1;

        public static string UserName;

        public static string sNgayChon;

        public static string sQuyen = "ADMIN";

        public static string CPT = "";//MyProject.Computer.Name.ToUpper();

        public static string selectNode = "";

        public static string qryEmployeesRP = "";

        public static string sSelectText = "";

        public static string iSelectNode = "";

        public static string sysUserName = "";

        public static string sMaCCSelected = "";

        public static string sEmpIDSelected = "";

        public static string sEIDSelected = "";

        public static string sEmpNameSelected = "";

        public static string sYear = DateAndTime.Now.Year.ToString();

        public static string qryEmployees;

        public static string NgayXem = "";

        public static string sysUserCur = "";

        public static string MMyyyy = DateAndTime.Now.ToString("MMyyyy");

        public static string sPrefixTblInOut = "TimeIn_TimeOutReal";

        public static string sPrefixTblQuetTheLuu = "TrangThaiQuetTheLuu";

        public static string sPrefixTblQuetThe = "TrangThaiQuetThe";

        public static string sLastTblTBL = "REAL";

        public static string sysTuNgay;

        public static string sysToiNgay;

        public static SqlConnection sqlConn()
        {
            SqlConnection sqlConnection = new SqlConnection();
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            dataSet = StartUp.fcnGetDatabaseInfo();
            StartUp.server1 = StartUp.fcnGetServerName();
            StartUp.dbName1 = StartUp.fcnGetDBName();
            StartUp.user1 = StartUp.fcnGetUSName();
            StartUp.pass1 = StartUp.fcnGetMK();
            string connectionString = string.Concat(new string[]
			{
				"Data Source = ",
				StartUp.server1,
				"; Initial Catalog = ",
				StartUp.dbName1,
				"; Persist Security Info=True; User ID=",
				StartUp.user1,
				"; PassWord= ",
				StartUp.pass1
			});
            sqlConnection.ConnectionString = connectionString;
            sqlConnection.Open();
            return sqlConnection;
        }

        public static string fcnGetServerName()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            string result = "";
            try
            {
                string str = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists(str + "\\DatabaseInfo.xml");
                if (flag)
                {
                    dataSet.ReadXml(str + "\\DatabaseInfo.xml");
                    result = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["ServerName"].ToString());
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception expr_83)
            {
                ProjectData.SetProjectError(expr_83);
                Exception ex = expr_83;
                clsCommon.uMsgboxInfo("Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
            return result;
        }

        public static string fcnGetDBName()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            string result = "";
            try
            {
                string str = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists(str + "\\DatabaseInfo.xml");
                if (flag)
                {
                    dataSet.ReadXml(str + "\\DatabaseInfo.xml");
                    result = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["DatabaseName"].ToString());
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception expr_83)
            {
                ProjectData.SetProjectError(expr_83);
                Exception ex = expr_83;
                clsCommon.uMsgboxInfo("Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
            return result;
        }

        public static string fcnGetUSName()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            string result = "";
            try
            {
                string str = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists(str + "\\DatabaseInfo.xml");
                if (flag)
                {
                    dataSet.ReadXml(str + "\\DatabaseInfo.xml");
                    result = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["UserName"].ToString());
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception expr_83)
            {
                ProjectData.SetProjectError(expr_83);
                Exception ex = expr_83;
                clsCommon.uMsgboxInfo("Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
            return result;
        }

        public static string fcnGetMK()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            string result = "";
            try
            {
                string str = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists(str + "\\DatabaseInfo.xml");
                if (flag)
                {
                    dataSet.ReadXml(str + "\\DatabaseInfo.xml");
                    result = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["Password"].ToString());
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception expr_83)
            {
                ProjectData.SetProjectError(expr_83);
                Exception ex = expr_83;
                clsCommon.uMsgboxInfo("Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
            return result;
        }

        public static DateTime fcnDateTimeFromServer()
        {
            DateTime result = DateAndTime.Now;
            clsDal clsDal = new clsDal("SQLSERVER");
            DataSet dataSet = new DataSet();
            string sSQL = "SELECT GETDATE() as vl";
            dataSet = clsDal.fcnExecQuery(sSQL, "tbl");
            bool flag = dataSet.Tables[0].Rows.Count > 0;
            if (flag)
            {
                result = Conversions.ToDate(dataSet.Tables[0].Rows[0]["vl"].ToString());
            }
            return result;
        }

        public static bool CanhBao(string strDMY)
        {
            bool flag = DateAndTime.DateDiff(DateInterval.Day, Conversions.ToDate("11/08/2018"), Conversions.ToDate(strDMY), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1) >= 0L;
            bool result;
            if (flag)
            {
                clsDal clsDal = new clsDal("SQLSERVER");
                string text = "if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='ChangeDepartment' and COLUMN_NAME='EmployeeID')\r\n";
                text += " begin";
                text += " EXEC sp_RENAME 'ChangeDepartment.EmployeeID' , 'EmployeeIDs', 'COLUMN'";
                text += " end ";
                flag = !clsDal.fcnExecNonQuery(text);
                if (flag)
                {
                    result = false;
                }
                else
                {
                    text = "if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Employees' and COLUMN_NAME='EmployeeID')\r\n";
                    text += " begin";
                    text += " EXEC sp_RENAME 'Employees.EmployeeID' , 'EmployeeIDs', 'COLUMN'";
                    text += " end ";
                    flag = !clsDal.fcnExecNonQuery(text);
                    result = !flag;
                }
            }
            else
            {
                clsDal clsDal2 = new clsDal("SQLSERVER");
                string text2 = "if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='ChangeDepartment' and COLUMN_NAME='EmployeeIDs')\r\n";
                text2 += " begin";
                text2 += " EXEC sp_RENAME 'ChangeDepartment.EmployeeIDs' , 'EmployeeID', 'COLUMN'";
                text2 += " end ";
                flag = !clsDal2.fcnExecNonQuery(text2);
                if (flag)
                {
                    result = false;
                }
                else
                {
                    text2 = "if exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Employees' and COLUMN_NAME='EmployeeIDs')\r\n";
                    text2 += " begin";
                    text2 += " EXEC sp_RENAME 'Employees.EmployeeIDs' , 'EmployeeID', 'COLUMN'";
                    text2 += " end ";
                    flag = !clsDal2.fcnExecNonQuery(text2);
                    result = (flag && false);
                }
            }
            return result;
        }

        public static string GetDiskSerialNumber()
        {
            object objectValue = RuntimeHelpers.GetObjectValue(Interaction.GetObject("WinMgmts:", null));
            try
            {
                IEnumerator enumerator = ((IEnumerable)NewLateBinding.LateGet(objectValue, null, "InstancesOf", new object[]
				{
					"Win32_PhysicalMedia"
				}, null, null, null)).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator.Current);
                    bool flag = !Information.IsDBNull(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(objectValue2, null, "SerialNumber", new object[0], null, null, null)));
                    if (flag)
                    {
                        string text = Strings.Trim(Conversions.ToString(NewLateBinding.LateGet(objectValue2, null, "SerialNumber", new object[0], null, null, null)));
                    }
                }
            }
            finally
            {
                IEnumerator enumerator = null;
                bool flag = enumerator is IDisposable;
                if (flag)
                {
                    (enumerator as IDisposable).Dispose();
                }
            }
            string result = "";
            return result;
        }

        public static void NhatKiSuDung(string ChucNang, string noidung, string CPT)
        {
            clsDal clsDal = new clsDal("SQLSERVER");
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string text = "INSERT INTO tblHistory(sCPTName,UserName,sFunction,sDate,sTime,sValue)";
                text = string.Concat(new string[]
				{
					text,
					" VALUES ('",
					CPT,
					"','",
					StartUp.sUsername,
					"',N'",
					ChucNang,
					"',Convert(DateTime,'",
					DateAndTime.Now.ToString("dd/MM/yyyy"),
					"',103),Convert(DateTime,'",
					DateAndTime.Now.ToString("HH:mm"),
					"',108),N'",
					noidung,
					"')"
				});
                clsDal.fcnExecNonQuery(text);
                Cursor.Current = Cursors.Default;
            }
            catch (Exception expr_E0)
            {
                ProjectData.SetProjectError(expr_E0);
                Exception ex = expr_E0;
                clsCommon.uMsgboxInfo("NhatKiSuDung.Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
            finally
            {
            }
        }

        //public static void Main()
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;
        //        bool flag = clsDal.IsCorrectConfiguration();
        //        bool flag2 = flag;
        //        Cursor.Current = Cursors.Default;
        //        flag = !flag2;
        //        if (flag)
        //        {
        //            HRM_CauHinh.frmMain frmMain = new HRM_CauHinh.frmMain();
        //            frmMain.ShowDialog();
        //            flag = (frmMain.DialogResult == DialogResult.Cancel);
        //            if (flag)
        //            {
        //                Application.Exit();
        //            }
        //            flag = clsDal.IsCorrectConfiguration();
        //            if (flag)
        //            {
        //                StartUp.prcShowOptionLogon_HardSN();
        //                flag = !StartUp.fcnCheckBanQuyen();
        //                if (flag)
        //                {
        //                    StartUp.blnPrintRight = false;
        //                }
        //                else
        //                {
        //                    StartUp.blnPrintRight = true;
        //                }
        //                MyProject.Forms.frmMain.ShowDialog();
        //            }
        //            else
        //            {
        //                clsCommon.uMsgboxInfo("Thông số kết nối cơ sở dữ liệu không đúng");
        //                Application.Exit();
        //            }
        //        }
        //        else
        //        {
        //            flag = !StartUp.fcnCheckBanQuyen();
        //            if (flag)
        //            {
        //                StartUp.blnPrintRight = false;
        //            }
        //            else
        //            {
        //                StartUp.blnPrintRight = true;
        //            }
        //            MyProject.Forms.frmMain.ShowDialog();
        //        }
        //        MyProject.Forms.frmMain.ShowDialog();
        //    }
        //    catch (Exception expr_F2)
        //    {
        //        ProjectData.SetProjectError(expr_F2);
        //        Exception ex = expr_F2;
        //        clsCommon.uMsgboxInfo("Error: " + ex.Message);
        //        Application.Exit();
        //        ProjectData.ClearProjectError();
        //    }
        //}

        private static DataSet fcnGetDatabaseInfo()
        {
            DataSet dataSet = new DataSet();
            DataSet result=null;
            try
            {
                string text = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists("C:\\WINDOWS\\system32\\Infohy.xml");
                if (flag)
                {
                    dataSet.ReadXml("C:\\WINDOWS\\system32\\Infohy.xml");
                    result = dataSet;
                }
            }
            catch (Exception expr_3B)
            {
                ProjectData.SetProjectError(expr_3B);
                ProjectData.ClearProjectError();
            }
            finally
            {
                dataSet.Dispose();
            }
            return result;
        }

        private static void prcShowDatabaseInfo()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            try
            {
                dataSet = StartUp.fcnGetDatabaseInfo();
                StartUp.strdoc = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["ServerName"].ToString());
                StartUp.strdoc1 = dataSet.Tables[0].Rows[0]["UserName"].ToString();
                StartUp.strdoc2 = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["UserName"].ToString());
            }
            catch (Exception expr_A3)
            {
                ProjectData.SetProjectError(expr_A3);
                ProjectData.ClearProjectError();
            }
            finally
            {
                dataSet.Dispose();
            }
        }

        private static DataSet fcnGetOptionLogon()
        {
            DataSet dataSet = new DataSet();
            DataSet result = null;
            try
            {
                string text = AppDomain.CurrentDomain.BaseDirectory.ToString();
                bool flag = File.Exists("F:\\GHOST\\PT\\Pro_TAS_TIENHUNG\\Main\\bin\\Options.xml");
                if (flag)
                {
                    dataSet.ReadXml("F:\\GHOST\\PT\\Pro_TAS_TIENHUNG\\Main\\bin\\Options.xml");
                    result = dataSet;
                }
            }
            catch (Exception expr_3B)
            {
                ProjectData.SetProjectError(expr_3B);
                ProjectData.ClearProjectError();
            }
            finally
            {
                dataSet.Dispose();
            }
            return result;
        }

        public static string fcnReadSN()
        {
            //clsReg clsReg = new clsReg();
            //bool flag = clsReg.GetMotherBoardID().Trim().Length > 0;
            //string result="";
            //if (flag)
            //{
            //    result = clsReg.GetMotherBoardID();
            //}
            //else
            //{
            //    result = clsReg.GetVolumeSerial("C");
            //}
            //return result;
            return "";
        }

        public static void prcShowOptionLogon_HardSN()
        {
            DataSet dataSet = new DataSet();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            try
            {
                dataSet = StartUp.fcnGetOptionLogon();
                StartUp.strHardSNFromF = Strings.Left(clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["PCN"].ToString()).Trim(), checked(clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["PCN"].ToString()).Trim().Length - 2));
                StartUp.strAutoLogon = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["AutoLog"].ToString()).Trim();
                StartUp.strUSFromF = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["UserName"].ToString()).Trim();
                StartUp.strPWFromF = clsAlgorithm.DecryptString(dataSet.Tables[0].Rows[0]["Password"].ToString()).Trim();
            }
            catch (Exception expr_12B)
            {
                ProjectData.SetProjectError(expr_12B);
                ProjectData.ClearProjectError();
            }
            finally
            {
                dataSet.Dispose();
            }
        }

        public static bool fcnCheckBanQuyen()
        {
            clsCommon clsCommon = new clsCommon();
            clsAlgorithm clsAlgorithm = new clsAlgorithm();
            bool result;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                StartUp.prcShowDatabaseInfo();
                string left = StartUp.strdoc;
                string text = SystemInformation.ComputerName.ToUpper();
                string text2 = Strings.UCase(StartUp.fcnGetServerName());
                string text3 = StartUp.Get_HardDisk_Serial();
                bool flag = Operators.CompareString(text3.ToString().Trim(), "", false) == 0;
                if (flag)
                {
                    text3 = "HIDATECH";
                }
                string right = clsCommon.Get_ActiveKey(text3);
                flag = (Operators.CompareString(left, right, false) == 0);
                if (flag)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception expr_AB)
            {
                ProjectData.SetProjectError(expr_AB);
                result = false;
                ProjectData.ClearProjectError();
            }
            finally
            {
            }
            return result;
        }

        public static bool CheckIsSupervisor(string UserName)
        {
            DataSet dataSet = new DataSet();
            clsDal clsDal = new clsDal("SQLSERVER");
            bool result;
            try
            {
                dataSet = clsDal.fcnExecQuery("Select * from tblHeThong_NguoiDung Where Ten='" + UserName + "' AND Project =1 AND IsSupervisor=1", "tbl");
                bool flag = dataSet.Tables[0].Rows.Count > 0;
                if (flag)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception expr_5C)
            {
                ProjectData.SetProjectError(expr_5C);
                Exception ex = expr_5C;
                clsCommon.uMsgboxInfo("Error CheckIsSupervisor: " + ex.Message);
                result = false;
                ProjectData.ClearProjectError();
            }
            finally
            {
            }
            return result;
        }

        public static void InitFormMain(bool ok)
        {
            checked
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int arg_2B_0 = 0;
                    int num = 0;// MyProject.Forms.frmMain.MenuStrip1.Items.Count - 1;
                    int num2 = arg_2B_0;
                    while (true)
                    {
                        int arg_12B_0 = num2;
                        int num3 = num;
                        if (arg_12B_0 > num3)
                        {
                            break;
                        }
                        ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();// (ToolStripMenuItem)MyProject.Forms.frmMain.MenuStrip1.Items[num2];
                        try
                        {
                            IEnumerator enumerator = toolStripMenuItem.DropDownItems.GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                ToolStripMenuItem toolStripMenuItem2 = (ToolStripMenuItem)enumerator.Current;
                                bool flag = toolStripMenuItem2.DropDownItems.Count > 0;
                                if (flag)
                                {
                                    try
                                    {
                                        IEnumerator enumerator2 = toolStripMenuItem2.DropDownItems.GetEnumerator();
                                        while (enumerator2.MoveNext())
                                        {
                                            ToolStripMenuItem toolStripMenuItem3 = (ToolStripMenuItem)enumerator2.Current;
                                            toolStripMenuItem3.Enabled = ok;
                                        }
                                    }
                                    finally
                                    {
                                        IEnumerator enumerator2 = null;
                                        flag = (enumerator2 is IDisposable);
                                        if (flag)
                                        {
                                            (enumerator2 as IDisposable).Dispose();
                                        }
                                    }
                                }
                                else
                                {
                                    toolStripMenuItem2.Enabled = ok;
                                }
                            }
                        }
                        finally
                        {
                            IEnumerator enumerator = null;
                            bool flag = enumerator is IDisposable;
                            if (flag)
                            {
                                (enumerator as IDisposable).Dispose();
                            }
                        }
                        num2++;
                    }
                    Cursor.Current = Cursors.Default;
                }
                catch (Exception expr_13D)
                {
                    ProjectData.SetProjectError(expr_13D);
                    Exception ex = expr_13D;
                    clsCommon.uMsgboxInfo("Error: " + ex.Message);
                    Application.Exit();
                    ProjectData.ClearProjectError();
                }
                finally
                {
                }
            }
        }

        public static void InitMenu(string username)
        {
            
        }

        public static string Get_HardDisk_Serial()
        {
            ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PhysicalMedia WHERE Tag ='\\\\\\\\.\\\\PHYSICALDRIVE0'");
            string result="";
            try
            {
                try
                {
                    ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        ManagementObject managementObject = (ManagementObject)enumerator.Current;
                        PropertyDataCollection.PropertyDataEnumerator enumerator2 = managementObject.Properties.GetEnumerator();
                        while (enumerator2.MoveNext())
                        {
                            PropertyData current = enumerator2.Current;
                            bool flag = Operators.CompareString(current.Name, "SerialNumber", false) == 0;
                            if (flag)
                            {
                                result = managementObject[current.Name].ToString().Trim();
                                return result;
                            }
                        }
                    }
                }
                finally
                {
                    ManagementObjectCollection.ManagementObjectEnumerator enumerator = null;
                    bool flag = enumerator != null;
                    if (flag)
                    {
                        ((IDisposable)enumerator).Dispose();
                    }
                }
            }
            catch (Exception expr_B3)
            {
                ProjectData.SetProjectError(expr_B3);
                result = "";
                ProjectData.ClearProjectError();
            }
            return result;
        }

        internal static string GetVolumeSerial(string strDriveLetter = "C")
        {
            ManagementObject managementObject = new ManagementObject(string.Format("win32_logicaldisk.deviceid=\"{0}:\"", strDriveLetter));
            managementObject.Get();
            return managementObject["VolumeSerialNumber"].ToString();
        }

        internal static string GetMotherBoardID()
        {
            string result = string.Empty;
            SelectQuery query = new SelectQuery("Win32_BaseBoard");
            ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(query);
            try
            {
                ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator();
                while (enumerator.MoveNext())
                {
                    ManagementObject managementObject = (ManagementObject)enumerator.Current;
                    result = managementObject["SerialNumber"].ToString();
                }
            }
            finally
            {
                ManagementObjectCollection.ManagementObjectEnumerator enumerator = null;
                bool flag = enumerator != null;
                if (flag)
                {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            return result;
        }

        public static void WriteFile(string UserName, string UserName1, bool PrintRight, string MenuName)
        {
            try
            {
                IResourceWriter resourceWriter = new ResourceWriter("myApp.resources");
                resourceWriter.AddResource("UserName", UserName);
                resourceWriter.AddResource("User_Name", UserName1);
                resourceWriter.AddResource("MenuName", MenuName);
                resourceWriter.AddResource("PrintRight", PrintRight);
                resourceWriter.Close();
            }
            catch (Exception expr_51)
            {
                ProjectData.SetProjectError(expr_51);
                Exception ex = expr_51;
                clsCommon.uMsgboxInfo("Error: " + ex.Message);
                ProjectData.ClearProjectError();
            }
        }
    }
}
