﻿namespace NtbSoft.Demo.App
{
    partial class frmTestModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btTaoGioAo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btTaoGioAo
            // 
            this.btTaoGioAo.Location = new System.Drawing.Point(12, 32);
            this.btTaoGioAo.Name = "btTaoGioAo";
            this.btTaoGioAo.Size = new System.Drawing.Size(75, 23);
            this.btTaoGioAo.TabIndex = 0;
            this.btTaoGioAo.Text = "Tao Gio ao";
            this.btTaoGioAo.UseVisualStyleBackColor = true;
            this.btTaoGioAo.Click += new System.EventHandler(this.btTaoGioAo_Click);
            // 
            // frmTestModels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 267);
            this.Controls.Add(this.btTaoGioAo);
            this.Name = "frmTestModels";
            this.Text = "frmTestModels";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btTaoGioAo;
    }
}