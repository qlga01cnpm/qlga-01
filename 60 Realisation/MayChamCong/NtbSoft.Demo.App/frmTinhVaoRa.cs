﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App
{
    public partial class frmTinhVaoRa : Form
    {
        public frmTinhVaoRa()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            //base.OnLoad(e);
            string MinGio = "", MaxGio = "", BreakOut = "", BreakIn = "";
            int _mSoPhut2LanCham = 30;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            mArrayGio.Add(new TimeSpan(7, 0, 0));
            //mArrayGio.Add(new TimeSpan(7, 10, 0));
            mArrayGio.Add(new TimeSpan(7, 30, 0));
            mArrayGio.Add(new TimeSpan(8, 00, 0));
            //mArrayGio.Add(new TimeSpan(8, 10, 0));

            mArrayGio.Add(new TimeSpan(17, 00, 0));

            List<clsDangKyRaVaoInfo> mArrayGioVaoRa = new List<clsDangKyRaVaoInfo>();

            TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
            while (mArrayGio.Count > 0)
            {
                //TimeSpan mGioRa = TimeSpan.Zero, mGioVao = TimeSpan.Zero;

                #region Vào
                //Lấy vào lần đầu tiên
                if (string.IsNullOrEmpty(MinGio))
                {//Vao
                    mTempGioVao = mArrayGio[0];
                    MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                    //Lấy giờ vào
                    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                    {
                        GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                        GioRa = "",GhiChu="Đi trễ"
                    });

                    mArrayGio.Remove(mTempGioVao);
                    continue;
                }
                else if (mTempGioVao == TimeSpan.Zero)
                {//Vao
                tt:
                    MaxGio = "";
                    if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
                    {
                        mArrayGio.Remove(mTempGioVao);
                        goto tt;
                    }
                    mTempGioVao = mArrayGio[0];
                    mArrayGio.Remove(mTempGioVao);
                }
                #endregion

                #region Ra
                if (mArrayGio.Count > 0)
                {
                    MaxGio = "";
                    //Kiểm tra giờ ra này có hợp lệ hay không
                    if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
                    {
                        mTempGioRa = mArrayGio[0];
                        mTempGioVao = TimeSpan.Zero;
                        MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                        
                        //Kiểm tra giờ ra nếu nhỏ hơn giờ cho phép sẽ tính về sớm
                    }
                    mArrayGio.Remove(mArrayGio[0]);
                }

                #endregion

                if (mTempGioRa != TimeSpan.Zero && mTempGioVao != TimeSpan.Zero)
                {
                    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                    {
                        GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                        GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour)
                    });
                }
                if (mArrayGio.Count == 0 && MaxGio != "")
                {//Kiểm tra về sớm
                    mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                    {
                        GioRa = MaxGio,
                        GhiChu = "Về sớm"
                    });
                }
            }
        }


        //protected override void OnLoad(EventArgs e)
        //{
        //    //base.OnLoad(e);
        //    string MinGio = "", MaxGio = "", BreakOut = "", BreakIn = "";
        //    int _mSoPhut2LanCham = 30;
        //    List<TimeSpan> mArrayGio = new List<TimeSpan>();
        //    mArrayGio.Add(new TimeSpan(7, 0, 0));
        //    mArrayGio.Add(new TimeSpan(7, 10, 0));
        //    mArrayGio.Add(new TimeSpan(7, 30, 0));
        //    mArrayGio.Add(new TimeSpan(8, 00, 0));
        //    mArrayGio.Add(new TimeSpan(8, 10, 0));

        //    List<clsDangKyRaVaoInfo> mArrayGioVaoRa = new List<clsDangKyRaVaoInfo>();


        //    TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
        //    while (mArrayGio.Count > 0)
        //    {
        //        TimeSpan mGioRa = TimeSpan.Zero, mGioVao = TimeSpan.Zero;
        //        #region Vào
        //        //Lấy vào lần đầu tiên
        //        if (string.IsNullOrEmpty(MinGio))
        //        {
        //            mTempGioVao = mArrayGio[0];
        //            MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
        //            mArrayGio.Remove(mTempGioVao);
        //        }
        //        else if (mTempGioVao == mTempGioRa)
        //        {
        //            if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
        //            {
        //                mArrayGio.Remove(mTempGioVao);
        //                continue;
        //            }
        //            mTempGioVao = mArrayGio[0];
        //            mArrayGio.Remove(mTempGioVao);
        //        }
        //        #endregion

        //        #region Ra
        //        if (mArrayGio.Count > 0)
        //        {
        //            MaxGio = "";
        //            //Kiểm tra giờ ra này có hợp lệ hay không
        //            if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
        //            {
        //                mTempGioRa = mArrayGio[0];
        //                MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);

        //                mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
        //                {
        //                    GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
        //                    GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour)
        //                });
        //                //Lấy cặp vào - ra
        //                mGioVao = mTempGioVao;
        //                mGioRa = mTempGioRa;
        //                mTempGioVao = mTempGioRa;
        //            }
        //            mArrayGio.Remove(mArrayGio[0]);
        //        }

        //        #endregion

        //        //Trường hợp không còn khoảng giờ nào và giờ vào khác giờ ra
        //        if (mArrayGio.Count == 0 && mTempGioVao != mTempGioRa)
        //        {
        //            mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
        //            {
        //                GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour)
        //                //, GioRa = mTempGioRa
        //            });
        //        }
        //    }
        //}
    }


    public class clsDangKyRaVaoInfo
    {
        public double ID { get; set; }
        //public TimeSpan GioVao { get; set; }
        //public TimeSpan GioRa { get; set; }
        public string MaNV { get; set; }
        public DateTime NgayDangKy { get; set; }
        public string GioVao { get; set; }
        public string GioRa { get; set; }
        public string ThoiGian { get; set; }
        public string GhiChu { get; set; }
    }
}
