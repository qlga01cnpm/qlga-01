﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NtbSoft.Demo.App
{
    public partial class frmTest2 : Form
    {
        public frmTest2()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            NtbSoft.ERP.Models.Interfaces.IBase<NtbSoft.ERP.Entity.Interfaces.ILyDoVangInfo> _ld =
                new ERP.Models.clsLyDoVang();
            
            _ld.GetAll((items, ex) =>
            {
                if (ex == null)
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.dataGridView1.DataSource = items;
                    });
                else
                    MessageBox.Show(ex.Message);

            });
        }
    }
}
