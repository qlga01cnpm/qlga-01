
/****** Object:  UserDefinedTableType [dbo].[TYPE_DIC_DANGKY_RAVAO]    Script Date: 09/12/2018 08:41:07 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TYPE_DIC_DANGKY_RAVAO' AND ss.name = N'dbo')
DROP TYPE [dbo].[TYPE_DIC_DANGKY_RAVAO]
GO


/****** Object:  UserDefinedTableType [dbo].[TYPE_DIC_DANGKY_RAVAO]    Script Date: 09/12/2018 08:41:07 ******/
CREATE TYPE [dbo].[TYPE_DIC_DANGKY_RAVAO] AS TABLE(
	[ID] [bigint] NULL,
	[MaNV] [nvarchar](50) NULL,
	[NgayDangKy] [datetime] NULL,
	[GioRa] [varchar](8) NULL,
	[GioVao] [varchar](8) NULL,
	[ThoiGian] [varchar](30) NULL,
	[GhiChu] [nvarchar](max) NULL,
	DiMuon	varchar(30) null,
	VeSom	varchar(30) null
)
GO


