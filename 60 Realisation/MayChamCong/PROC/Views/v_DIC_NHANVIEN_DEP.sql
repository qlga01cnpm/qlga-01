GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_DIC_NHANVIEN_DEP]'))
DROP VIEW [dbo].[v_DIC_NHANVIEN_DEP]
GO

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*** LẤY NHÂN VIÊN THUỘC BỘ PHẬN ***/
CREATE VIEW [dbo].[v_DIC_NHANVIEN_DEP] 
WITH ENCRYPTION
AS
	Select 
		nv.*
		,dep.DepID, dep.DepName
	from dbo.SYS_DEPARTMENTS dep
	inner join dbo.DIC_CHUYENBOPHAN depC 
	inner join dbo.DIC_NHANVIEN nv
	on depC.MaNV=nv.Id on dep.DepID=depC.DepID
	where depC.ChangedDate=(Select MAX(c.ChangedDate) 
							from dbo.DIC_CHUYENBOPHAN c
							where c.MaNV=nv.Id)
	and nv.IsVisible=0
			
GO


