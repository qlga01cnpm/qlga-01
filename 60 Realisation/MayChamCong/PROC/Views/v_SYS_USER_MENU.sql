GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_SYS_USER_MENU]'))
DROP VIEW [dbo].[v_SYS_USER_MENU]
GO

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_SYS_USER_MENU] 
WITH ENCRYPTION
AS
	select t1.MenuId,t1.MenuName,t1.MenuParent,t1.Sort,t1.Url,t1.IsFunc
	,case when t2.AllowView is null then 0 else t2.AllowView end as AllowView
	,case when t2.AllowAdd is null then 0 else t2.AllowAdd end as AllowAdd
	,case when t2.AllowEdit is null then 0 else t2.AllowEdit end as AllowEdit
	,case when t2.AlowDelete is null then 0 else t2.AlowDelete end as AlowDelete
	,t2.UserID
	From dbo.SYS_MENU_MASTER t1
	left join dbo.SYS_USER_MENU t2
	on t1.MenuId=t2.MenuID
			
GO


