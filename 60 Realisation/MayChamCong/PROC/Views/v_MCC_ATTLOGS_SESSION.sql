GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_MCC_ATTLOGS_SESSION]'))
DROP VIEW [dbo].[v_MCC_ATTLOGS_SESSION]
GO

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- View lấy dữ liệu chấm công để xử lý
CREATE VIEW [dbo].[v_MCC_ATTLOGS_SESSION] 
WITH ENCRYPTION
AS
	Select t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
	,t2.DepID,t2.DepName
	From dbo.MCC_ATTLOGS_SESSION t1
	--inner join dbo.DIC_NHANVIEN t2
	inner join dbo. v_DIC_NHANVIEN_DEP t2
	on t1.MaCC=t2.MaCC
GO


