DECLARE @DepId varchar(100) = 'SH.PX01'

;WITH temp(DepID, DepName,ParentID, Node)
        as (
                Select l1.DepID,l1.DepName,l1.ParentID, 0 as Node
                From dbo.SYS_DEPARTMENTS l1
                Where l1.ParentID is null
                --Where l1.DepID=@DepId
                Union All
                Select b.DepID,b.DepName,b.ParentID, a.Node + 1
                From temp as a, dbo.SYS_DEPARTMENTS as b
                Where a.DepID = b.ParentID
        )

Select * From temp
