 IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results

 declare @FromDate datetime,
		@ToDate datetime,
		@SearchTerm VARCHAR(100) = ''
      ,@PageIndex INT = 2
      ,@PageSize INT = 10
      ,@RecordCount INT,
      @DepId varchar(100)
      ,@MaNV varchar(50)
      
	set @FromDate='05/01/2018'
	set @ToDate='05/30/2018'
	set @DepId='DEP.ROOT'
	set @MaNV=''
	
   ----   SELECT ROW_NUMBER() OVER
   ----   (
   ----         ORDER BY t2.Id ASC
   ----   )AS RowNumber
   ----   ,t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			----,t2.DepID,t2.DepName
			----INTO #Results
			----From dbo.MCC_ATTLOGS_SESSION t1
			------From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			----inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			----on t1.MaCC=t2.MaCC
			----where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			----between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
   ----   SELECT @RecordCount = COUNT(*)
   ----   FROM #Results
          
   ----   SELECT * FROM #Results
   ----   WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
   
   
   SELECT ROW_NUMBER() OVER
      (
            ORDER BY t2.Id ASC
      )AS RowNumber
      ,t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			,t2.DepID,t2.DepName
			INTO #Results
			From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			--From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaCC=t2.MaCC
			where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
      SELECT @RecordCount = COUNT(*)
      FROM #Results
          
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
      
      
      --DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results