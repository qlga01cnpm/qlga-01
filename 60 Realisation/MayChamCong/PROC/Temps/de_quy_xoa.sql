DECLARE @MenuId varchar(100) = '8f9a312f-1b92-679d-a0bf-7c5b8d3e8df0'

;WITH temp(MenuId, MenuName,MenuParent, Node)
        as (
                Select l1.MenuId,l1.MenuName,l1.MenuParent, 0 as Node
                From dbo.tbl_MenuMaster l1
                Where l1.MenuId=@MenuId
                Union All
                Select b.MenuId,b.MenuName,b.MenuParent, a.Node + 1
                From temp as a, dbo.tbl_MenuMaster as b
                Where a.MenuId = b.MenuParent
        )

--Select * From temp


DELETE
FROM tbl_MenuMaster
WHERE MenuId IN (SELECT MenuId FROM temp)