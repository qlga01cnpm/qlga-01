declare @FromDate datetime,
@ToDate datetime,@TongSoNgay int,@day int
set @FromDate='07/07/2018' set @ToDate='07/07/2018'

select t2.MaNV,t2.TenNV,DepName
		,convert(varchar(10),t1.Workingday,103) as Workingday
		,case when ISNULL(t1.LyDoVang,'')='' then 'KCC' else t1.LyDoVang end LyDoVang
		From dbo.MCC_TIMEINOUT_SESSION_REAL t1 with(nolock)
		inner join dbo.fncNHANVIENDEP() t2 on t1.MaNV=t2.Id
		where CONVERT(date,Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		and(ISNULL(t1.TimeIn,'')='') and t1.Workingday<=GETDATE()
union
	select A.MaNV,A.TenNV,A.DepName
	,'' as Workingday
	,'kk' as LyDoVang
	from dbo.fncNHANVIENDEP() A
	where not exists(select A1.MaNV from dbo.MCC_TIMEINOUT_SESSION_REAL A1 where A1.MaNV=A.Id
	and CONVERT(date,A1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121))
	--and exists(select CAST(A2.MaNV as bigint) from dbo.DIC_DANGKY_CHEDO A2 where A2.MaNV=A.Id and A2.ToiNgay>=@FromDate)
	
	
--union
--	select A.MaNV,A.TenNV,A.DepName
--	,'' as Workingday
--	,'KCC' as LyDoVang
--	from dbo.fncNHANVIENDEP() A
--	where not exists(select A1.MaNV from dbo.MCC_TIMEINOUT_SESSION_REAL A1 where A1.MaNV=A.Id
--	and CONVERT(date,A1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121))