declare @s nvarchar(max)

while (
	select
		count(*)
	from
		sys.objects obj
		join sys.schemas s
			on (s.schema_id=obj.schema_id)
	where
		s.name='') > 0
begin
	select top 1
		@s = N'DROP SYNONYM [].[' + obj.name + ']'
	from
		sys.objects obj
		join sys.schemas s
			on (s.schema_id=obj.schema_id)
	where
		s.name=''
	print @s
	execute(@s)
end