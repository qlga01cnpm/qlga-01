
declare @ToDay date
set @ToDay=CONVERT(date,'2018-12-31',121)

--Nếu ngày năm vào làm bằng năm hiện tại thì lấy ngày vào trừ ngày cuối của năm, chia cho 30 ra số phép năm
--Nếu làm việc trong môi trường nguy hiểm thì số ngày phép là 14, ngoài ra là 12 ngày phép
--Nếu số năm làm việc là bội số của 5 thì được cộng thêm 1 ngày. Ví dụ đủ 5 năm thì được thêm 1 ngày phép, 10 năm thì đc thêm 2 ngày,15 năm thì 3 ngày,...

select 

DATEDIFF (day, A.BeginningDate,@ToDay)/365 as SoNam
,A.Id,A.MaNV,A.TenNV,A.BeginningDate
,cast((DATEDIFF (day, A.BeginningDate,@ToDay)/365)%5 as float) as ChiaPhanDu
,((DATEDIFF (day, A.BeginningDate,@ToDay)/365)/5 ) as ChiaPhanNguyen
,case 
	when YEAR(A.BeginningDate)=YEAR(@ToDay) then DATEDIFF (day, A.BeginningDate,@ToDay)/30
	
	when (DATEDIFF (day, A.BeginningDate,@ToDay)/365)%5=0 then ((DATEDIFF (day, A.BeginningDate,@ToDay)/365)/5 )+case when A.IsLamViecNguyHiem=1 then 14 else 12 end
	else        case when A.IsLamViecNguyHiem=1 then 14 else 12 end           end as SoNgayPhepNam

,case when (DATEDIFF (day, A.BeginningDate,@ToDay)/365)%5=0 then (DATEDIFF (day, A.BeginningDate,@ToDay)/365)/5 else 0 end as BoiSoCua5
From dbo.DIC_NHANVIEN A