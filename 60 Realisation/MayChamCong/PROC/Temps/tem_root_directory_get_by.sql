declare @id varchar(100)
set @id='ROOT.THIETBI'

;WITH temp(Id, Name,ParentId,HtmlTag,Sort, Node)
				as (
						Select l1.Id,l1.Name,l1.ParentId,l1.ContextMenuId,l1.Sort, 0 as Node
						From dbo.SYS_ROOT_DIRECTORY l1
						Where l1.Id=@id
						Union All
						Select b.Id,b.Name,b.ParentId,b.ContextMenuId,b.Sort, a.Node + 1
						From temp as a, dbo.SYS_ROOT_DIRECTORY as b
						Where a.Id = b.ParentId
				)
		select * From temp 
		order by Sort asc