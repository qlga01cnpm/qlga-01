-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER tg_SYS_MENU_MASTER
   ON   dbo.SYS_MENU_MASTER
   FOR INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	Insert into dbo.SYS_MENU_CONTEXT_MENU(MenuId,ContextMenuId)
	Select i.MenuId,t1.id
	from Inserted i
	inner join dbo.SYS_CONTEXT_MENU t1
	on t1.id=i.ContextMenuId
	where i.MenuId not in(
			select MenuId from dbo.SYS_MENU_CONTEXT_MENU t2
				inner join dbo.SYS_CONTEXT_MENU t3
				on t2.ContextMenuId=t3.id
				where t2.MenuId=i.MenuId and t2.ContextMenuId=i.ContextMenuId)

END
GO
