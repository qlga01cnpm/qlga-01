GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MenuMasterUpdateProperty]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MenuMasterUpdateProperty]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MenuMasterUpdateProperty] 
@MenuId varchar(100),
@MenuName nvarchar(255),
@MoTa nvarchar(200),
@ChuoiBatDau varchar(10),
@KyTuCach char(1),
@SoChuSo int,
@SoBatDau int,
@SoKetThuc int
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	/*** Update ***/
	update dbo.SYS_MENU_MASTER set	MenuName=@MenuName,
									MoTa=@MoTa,
									ChuoiBatDau=@ChuoiBatDau,
									KyTuCach=@KyTuCach,
									SoChuSo=@SoChuSo,
									SoBatDau=@SoBatDau,
									SoKetThuc=@SoKetThuc
	where MenuId=@MenuId
	
COMMIT

GO


