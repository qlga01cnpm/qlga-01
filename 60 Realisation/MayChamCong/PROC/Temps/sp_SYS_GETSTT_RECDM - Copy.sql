GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_GETSTT_RECDM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_GETSTT_RECDM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_GETSTT_RECDM] 
@codeF nvarchar(3),
@dataSource nvarchar(24),
@valRef NvarChar(11) OUTPUT
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	-- Set check input parameter
	-- Set check input parameter
	Set @codeF = UPPER(RTrim(@codeF))
	Set @dataSource = RTrim(@dataSource)
	-- End set check input parameter
		
	-- Declare value
	Declare @StrSql nvarchar(4000)
	Declare @Stt_rec_max  int,@id varchar(40)
	Declare @W_Id char(1),
		@Ma_File nvarchar(3),
		@StrFortmat Char(7), 
		@M_ID_DM char(11)
	Declare @Eror bit
	Create Table #MyTempTable1 (Eror Bit, Stt_rec Numeric(7))
	-- End declare

	-- Set default
	Set @Ma_File = 'XXX'
	Set @StrFortmat = '0000000'	
	-- End set default
		
	-- Process main	
	--Set @W_Id = (Select distinct Value From dbo.SYS_CEN_OPTIONS Where LTrim(RTrim(Name)) = 'M_Ws_Id')
	
	if @dataSource = ''
		begin
			Set @valRef = @W_Id + @StrFortmat + @Ma_File			
		end
	if(@codeF is null or @codeF='')
		select top 1 @Ma_File = codeF, @id=keys,@W_Id=value From dbo.SYS_DIRECTORY Where upper(LTrim(RTrim(Files))) =upper(@dataSource)
	else 
		set @Ma_File=@codeF
	
	if @Ma_File=''
		set @Ma_File = 'XXX'

	Set @Stt_rec_max = (Select Stt_recDm From dbo.SYS_DMSTT)
	Set @Stt_rec_max = @Stt_rec_max + 1
	Set @M_ID_DM = @W_Id + (Select SubString(@StrFortmat, 1, 7 - Len(@Stt_rec_max)) + Convert(nvarchar(7), @Stt_rec_max)) + @Ma_File
	
	
	Set @StrSql = N''
	Set @StrSql = N'If Exists (Select * From ' + @dataSource +' Where LTrim(RTrim('+@id+'))= ''' + @M_ID_DM  + '''' + ')'
	Set @StrSql = @StrSql + Char(13)
	Set @StrSql = @StrSql + N' Insert Into #MyTempTable1(Eror, Stt_rec) Select 1, 0 '
	
	
	--Exec sp_executesql @StrSql
	
	Set @Eror = (Select Eror From #MyTempTable1)
	If @Eror = 1
	Begin
		--Set @Stt_rec_max = @Stt_rec_max + 1
		Set @StrSql = N' Update #MyTempTable1 Set Stt_rec = (Select Max(ConVert(Numeric(7), SubString(Id, 2, 7))) + 1 From ' + @dataSource + ')'	
		Exec sp_executesql @StrSql
		Set @Stt_rec_max = (Select Stt_rec From #MyTempTable1)	
	End
	if(@Stt_rec_max is not null)
	Update dbo.SYS_DMSTT Set Stt_recDm = @Stt_rec_max
	
	Set @valRef = SubString(@StrFortmat, 1, 7 - Len(@Stt_rec_max)) + Convert(nvarchar(7), @Stt_rec_max)
	Set @valRef = @W_Id + RTrim(@valRef) + @Ma_File
	Drop table #MyTempTable1
	-- End process


	Select @valRef
COMMIT

GO


