

declare @card_id int
set @card_id=0

while @card_id<1000
Begin
	set @card_id=@card_id+1
	
	Declare @Stt_rec_max  int,@SoChuSo int
	Declare @StrFortmat Char(7) ,@ChuoiBatDau varchar(100),@valRef varchar(100)

	-- Set default
	Set @StrFortmat = '0000000000000000000000000000000000000000'	
	set @ChuoiBatDau='CARD'
	set @SoChuSo=10
	set @Stt_rec_max=1
	
	Set @valRef = UPPER(@ChuoiBatDau)+  SubString(@StrFortmat, 1, @SoChuSo - Len(@Stt_rec_max+1)) + Convert(nvarchar(7), @card_id)
	--select @valRef
	insert into dbo.RFI_CARD(CardId,CardLabel)
	values(@valRef,@valRef)
End