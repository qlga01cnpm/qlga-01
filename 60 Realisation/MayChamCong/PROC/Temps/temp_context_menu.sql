declare @ContextMenuId varchar(100)
set @ContextMenuId='ied.menu.mahang.folder'

;WITH temp(Id, Name,ParentId,HtmlTag,Sort, Node)
				as (
						Select l1.Id,l1.Name,l1.ParentId,l1.HtmlTag,l1.Sort, 0 as Node
						From dbo.SYS_CONTEXT_MENU l1
						Where l1.Id=@ContextMenuId
						Union All
						Select b.Id,b.Name,b.ParentId,b.HtmlTag,b.Sort, a.Node + 1
						From temp as a, dbo.SYS_CONTEXT_MENU as b
						Where a.Id = b.ParentId
				)
		select * From temp 
		where ParentId is not null and ParentId<>''
		order by Sort asc