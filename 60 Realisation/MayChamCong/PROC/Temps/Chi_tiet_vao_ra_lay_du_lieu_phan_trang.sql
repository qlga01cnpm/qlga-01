 IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results

 declare @FromDate datetime,
		@ToDate datetime,
		@SearchTerm VARCHAR(100) = ''
      ,@PageIndex INT = 2
      ,@PageSize INT = 10
      ,@RecordCount INT 
	set @FromDate='05/01/2018'
	set @ToDate='05/30/2018'
	
	
      SELECT ROW_NUMBER() OVER
      (
            ORDER BY f1.Id ASC
      )AS RowNumber
      ,f1.Id as IdNV,f1.MaNV,f1.MaCC,f1.TenNV
			,t1.Id
			,t1.Workingday
			,t1.TimeIn,t1.BreakOut,t1.BreakIn,t1.TimeOut
			,t1.TotalTime,t1.TotalTimeHM,t1.TotalHours
			,t1.LyDoVang
			,t1.LateTime,t1.EarlyTime
			,t1.Shift,t1.OTX,t1.CongK
			INTO #Results
			From dbo.fncNHANVIENBYDEP('DEP.ROOT','') f1
			inner join dbo.MCC_TIMEINOUT_SESSION_REAL t1
			on f1.Id=CONVERT(int,t1.MaNV) and CONVERT(date,t1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
			order by f1.MaNV, t1.Workingday asc
      SELECT @RecordCount = COUNT(*)
      FROM #Results
          
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
      
      --DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results