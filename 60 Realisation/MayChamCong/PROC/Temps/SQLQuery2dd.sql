alter FUNCTION fncNHANVIENBYDEP(@DepId varchar(100),@TenNV nvarchar(100))  
RETURNS TABLE  
AS  
RETURN  
    WITH temp(DepID, DepName,ParentID, Node)
        as (
                Select l1.DepID,l1.DepName,l1.ParentID, 0 as Node
                From dbo.SYS_DEPARTMENTS l1
                Where l1.DepID=@DepId
                Union All
                Select b.DepID,b.DepName,b.ParentID, a.Node + 1
                From temp as a, dbo.SYS_DEPARTMENTS as b
                Where a.DepID = b.ParentID
        )
Select nv.*
		,dep.DepID, dep.DepName
From temp dep
inner join dbo.DIC_CHUYENBOPHAN depC 
inner join dbo.DIC_NHANVIEN nv
on depC.MaNV=nv.MaNV on dep.DepID=depC.DepID
where depC.ChangedDate=(Select MAX(c.ChangedDate) 
						from dbo.DIC_CHUYENBOPHAN c
						where c.MaNV=nv.MaNV)
and nv.IsVisible=0 
and nv.TenNV like '%'+@TenNV+'%'