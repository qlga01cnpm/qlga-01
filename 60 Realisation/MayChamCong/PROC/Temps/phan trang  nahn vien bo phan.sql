--select * from fncNHANVIENBYDEP('SH.PX01','')

declare            
		 @SelectedPage int,
		 @PageSize int,
		 @DepID varchar(50),
		 @TenNV nvarchar(100),
		 @TotalPages int,
		 @TotalRecords int
		 

set @DepID='SH.PX01'
set @TenNV=''
 set @TotalPages=0
 set @SelectedPage=1
 set @PageSize=10
	 DECLARE @ReturnedRecords int
	 -- Finds total records
	 select @TotalRecords=COUNT(MaNV) from  dbo.fncNHANVIENBYDEP(@DepID,@TenNV)
	 --select @TotalRecords
	
	-- Finds number of pages
	 SET @ReturnedRecords = (@PageSize * @SelectedPage)
	  set @TotalPages = @TotalRecords / @PageSize
	 IF @TotalRecords % @PageSize > 0
	   BEGIN
		 SET @TotalPages = @TotalPages + 1
	   END

	;WITH LogEntries AS (
	 SELECT ROW_NUMBER() OVER (ORDER BY MaNV )
	  AS RowNumber, MaNV 
	 FROM dbo.fncNHANVIENBYDEP(@DepID,@TenNV))
	 
	SELECT *
	 FROM LogEntries
	  INNER JOIN dbo.fncNHANVIENBYDEP(@DepID,@TenNV) t ON LogEntries.MaNV = t.MaNV 
	 WHERE RowNumber between 
	CONVERT(nvarchar(10), (@SelectedPage - 1) * @PageSize + 1)  and CONVERT(nvarchar(10), @SelectedPage*@PageSize)
	ORDER BY LogEntries.MaNV asc
	
	select @TotalPages,@TotalRecords
	