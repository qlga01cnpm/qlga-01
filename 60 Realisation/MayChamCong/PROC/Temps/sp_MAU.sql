GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_BACTHO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_BACTHO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_BACTHO] 
@Action varchar(100),
@TYPE_DIC_BACTHO as dbo.TYPE_DIC_BACTHO readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		/*** Write Log ***/
		If(@Action='POST')
		Begin
			select 'b'
		End
		Else if(@Action='DELETE')
		Begin
			select 'a'
		End 
		
COMMIT

GO


