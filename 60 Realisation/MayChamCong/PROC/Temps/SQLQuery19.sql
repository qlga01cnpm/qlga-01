SELECT [name], [type], [type_desc]
  FROM sys.objects 
  WHERE [type] NOT IN ('IT','S') 
  AND is_ms_shipped = 1
  ORDER BY [name];