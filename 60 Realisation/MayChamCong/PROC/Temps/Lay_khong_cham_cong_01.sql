declare @FromDate datetime,
@ToDate datetime,@TongSoNgay int,@day int
declare @tblTemp01 as table(Workingday datetime,MaNV varchar(50))
declare @tblDate as table(Workingday datetime)
declare @tblNhanVienCheDo as table(MaNV varchar(50),LyDo nvarchar(40))
declare @tblNhanVienVang as table(MaNV varchar(50),LyDo nvarchar(40))
declare @tblNhanVienKo as table(MaNV varchar(50),TenNV nvarchar(100),DepName nvarchar(100),Workingday varchar(10),LyDo nvarchar(40))

set @FromDate='07/06/2018' set @ToDate='07/07/2018'

insert into @tblDate
select A.Workingday
from MCC_TIMEINOUT_SESSION_REAL A
where CONVERT(date,A.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
group by A.Workingday

--Lay nhan vien che do
insert into @tblNhanVienCheDo
select A.Id,case when B.MaCheDo=1 then N'Nuôi con nhỏ'
				 when B.MaCheDo=2 then N'Mang thai 7-9 tháng' 
				 when B.MaCheDo=3 then N'Vị thành niên'
				 else N'Thai sản' end
from dbo.fncNHANVIENDEP() A
inner join dbo.DIC_DANGKY_CHEDO B on A.Id=B.MaNV
where B.ToiNgay>=@ToDate

-- Lay nhan vien dang ky vang
insert into @tblNhanVienVang
select A.Id,case when B.MaCheDo=1 then N'Nuôi con nhỏ'
				 when B.MaCheDo=2 then N'Mang thai 7-9 tháng' 
				 when B.MaCheDo=3 then N'Vị thành niên'
				 else N'Thai sản' end
from dbo.fncNHANVIENDEP() A
inner join dbo.DIC_DANGKY_CHEDO B on A.Id=B.MaNV
where B.ToiNgay>=@ToDate

--select * from @tblNhanVienCheDo
while (select COUNT(*) from @tblDate)>0
Begin
	declare @WorkingDay date
	select top 1 @WorkingDay=Workingday from @tblDate
	
	insert into @tblNhanVienKo
	select '','','',CONVERT(varchar(10),@WorkingDay,103),'kcc'
	
	--select @WorkingDay
	delete from @tblDate where Workingday=@WorkingDay
End

select * from @tblNhanVienKo