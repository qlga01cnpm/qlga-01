--select * from v_ProductStyle ln

------declare @id int=5
------;WITH parent AS
------(
------    SELECT id, parentId  from tbl_SettingLineX WHERE id = @id
------    UNION ALL 
------    SELECT t.ID, t.ParentID FROM parent
------    INNER JOIN tbl_SettingLineX t ON t.id =  parent.parentid
------)
------SELECT TOP 1 id FROM  parent
------order by id asc
declare @max int
declare @id varchar(100)
set @id='A0000327MNU'
;WITH parent AS
(
    SELECT MenuId,MenuParent,IsFunc from SYS_MENU_MASTER WHERE MenuId = @id
    UNION ALL 
    SELECT t.MenuId,t.MenuParent,t.IsFunc FROM parent p
    INNER JOIN SYS_MENU_MASTER t ON t.MenuId = p.MenuParent
)
SELECT *
FROM  parent p
inner join SYS_USER_MENU t
on p.MenuId=t.MenuID 

--SELECT 
--@max= MAX(CONVERT(int,AllowAdd))
--FROM  parent p
--inner join SYS_USER_MENU t
--on p.MenuId=t.MenuID 
--select @max

--select *from SYS_MENU_MASTER where MenuId='09.01.01'
