with CTE
	as
	(
		select 0 as aLevel,M1.DepID,DepName,M1.ParentID,M1.Descriptions,M1.PhoneDep,M1.IsChain
		,M1.IsChainSew,M1.IsChainCut,M1.IsChainPackage,M1.IsWarehouse,M1.IsLocalDep,M1.Sort
		from dbo.SYS_DEPARTMENTS  M1 
		where ParentID is null or IsChain = 1 
		union all
		select C.alevel + 1 as aLevel,M.DepID,M.DepName,M.ParentID,M.Descriptions,M.PhoneDep,M.IsChain
		,M.IsChainSew,M.IsChainCut,M.IsChainPackage,M.IsWarehouse,M.IsLocalDep,M.Sort
		from dbo.SYS_DEPARTMENTS M 
			 join CTE C on M.ParentID = C.DepID 
	)
select distinct * from CTE 