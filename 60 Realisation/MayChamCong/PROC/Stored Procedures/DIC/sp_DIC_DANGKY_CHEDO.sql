GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_CHEDO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_CHEDO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký thai sản>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_CHEDO] 
@Action varchar(100),
@ID bigint,
@DepId varchar(100),
@MaNV nvarchar(50),
@TuNgay datetime=null,
@ToiNgay datetime=null,
@ShiftID nvarchar(50)=null,
@LyDo nvarchar(50)=null,
@SoNgay decimal(18,1),
@ThoiGian int,
@MaCheDo int
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
	Begin
		
		if(ISNULL(@MaNV,'')='')
			Select t1.ID, t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.LyDo,t1.ShiftID,t1.SoNgay,t1.ThoiGian,t1.ToiNgay,t1.TuNgay,t1.MaCheDo
			from dbo.DIC_DANGKY_CHEDO t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaNV=t2.Id
			order by t2.MaNV asc
		Else
			Select t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.LyDo,t1.ShiftID,t1.SoNgay,t1.ThoiGian,t1.ToiNgay,t1.TuNgay,t1.MaCheDo
			from dbo.DIC_DANGKY_CHEDO t1
			inner join dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) t2
			on t1.MaNV=t2.Id
			order by t2.MaNV asc
	End
	else if(@Action='PUT')
	Begin -- Cap nhat
		update dbo.DIC_DANGKY_CHEDO set TuNgay=@TuNgay,
										  ToiNgay=@ToiNgay,
										  ShiftID=@ShiftID,
										  LyDo=@LyDo,
										  SoNgay=@SoNgay,
										  ThoiGian=@ThoiGian,
										  MaCheDo=@MaCheDo
		where ID=@ID
	End
	Else if(@Action='POST')
	Begin
		declare @Tmp nvarchar(50),@eID varchar(50),@TenCheDo nvarchar(50)
		select @eID=Id from dbo.DIC_NHANVIEN where MaNV=@MaNV
		--Kiểm tra chế độ
		
		select top 1 @Tmp=A.MaNV,@TenCheDo=B.TenCheDo from dbo.DIC_DANGKY_CHEDO A 
		inner join DIC_CHEDO B on A.MaCheDo=B.MaCheDo
		where MaNV=@eID 
					and (CONVERT(date,@TuNgay,121) between CONVERT(date,A.TuNgay,121) and CONVERT(date,A.ToiNgay,121) or
						 CONVERT(date,@ToiNgay,121) between CONVERT(date,A.TuNgay,121) and CONVERT(date,A.ToiNgay,121))
		--select @ShiftTep
		if(@Tmp is not null)
		Begin -- Đã đăng ký chế độ
			--Kiểm tra ngày đăng ký mới có nằm trong khoảng đăng ký chế độ đẵ đăng ký trước hay không. 
			--Nếu mà nằm trong khoảng đó thì cảnh báo đã được đăng ký chế độ
			declare @error nvarchar(100)
			set @error=N'Mã nhân viên ['+@MaNV+N'] đang trong chế độ ['+@TenCheDo+']'
			raiserror(@error,16,1)
			return
		End
		Insert into dbo.DIC_DANGKY_CHEDO values(@eID,@TuNgay,@ToiNgay,@ShiftID,@LyDo,@SoNgay,@ThoiGian,@MaCheDo)
	End
	Else if(@Action='DELETE')
	Begin
		Delete from dbo.DIC_DANGKY_CHEDO where ID=@ID
	End
END

GO


