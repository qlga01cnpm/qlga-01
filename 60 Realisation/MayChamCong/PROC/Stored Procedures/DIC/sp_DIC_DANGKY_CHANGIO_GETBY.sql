GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_CHANGIO_GETBY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO_GETBY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký chăn giờ>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO_GETBY] 
@DepId varchar(100),
@MaNV varchar(50),
@Month int,
@Year int

WITH ENCRYPTION
AS
BEGIN
	--exec sp_DIC_DANGKY_CHANGIO_GETBY'NTB.KYT','',12,2018
	-- sp_DIC_DANGKY_CHANGIO_GETBY'NTB.KYT','00002',12,2017
	--exec sp_DIC_DANGKY_CHANGIO_GETBY'DEP.ROOT','',7,2018
	if(ISNULL(@MaNV,'')='')
	Begin
		Select A.Id as eID, A.MaNV,A.TenNV,GioVe,sMonth,sYear
			,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10, D11,D12,D13,D14,D15,D16,D17,D18,D19,D20, D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31
		from dbo.fncNHANVIENBYDEP(@DepId,'') A	
		left join dbo.MCC_DANGKY_CHANGIO B on A.Id=B.eID  and B.sMonth=@Month and B.sYear=@Year
	End
	Else
	Begin
		Select A.Id as eID,A.MaNV,A.TenNV ,GioVe,sMonth,sYear
			,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10, D11,D12,D13,D14,D15,D16,D17,D18,D19,D20, D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31
		from dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) A	
		left join dbo.MCC_DANGKY_CHANGIO B on A.Id=B.eID and B.sMonth=@Month and B.sYear=@Year
	End
	
END

GO


