GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên đăng ký đi muộn về sớm, theo mã nhaanvieen và mã phòng ban>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY] 
@DepId varchar(100),
@MaNV varchar(50),
@Month int,
@Year int

WITH ENCRYPTION
AS
BEGIN
	--exec sp_DIC_DANGKY_DIMUONVESOM_GETBY'NTB.KYT','',12,2017
	-- sp_DIC_DANGKY_DIMUONVESOM_GETBY'NTB.KYT','00002',12,2017
	--exec sp_DIC_DANGKY_DIMUONVESOM_GETBY'DEP.ROOT','00001',12,2017
	if(ISNULL(@MaNV,'')='')
	Begin
		Select t1.Id as eID, t1.MaNV,t1.TenNV,t1.SCHName,t1.ShiftID
			,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10, M11,M12,M13,M14,M15,M16,M17,M18,M19,M20, M21,M22,M23,M24,M25,M26,M27,M28,M29,M30,M31
			,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10, S11,S12,S13,S14,S15,S16,S17,S18,S19,S20, S21,S22,S23,S24,S25,S26,S27,S28,S29,S30,S31
		from dbo.fncNHANVIENBYDEP(@DepId,'') t1	
		left join dbo.DIC_DANGKY_DIMUONVESOM t2 on t1.Id=t2.eID  and t2.sMonth=@Month and t2.sYear=@Year
	End
	Else
	Begin
		Select t1.Id as eID,t1.MaNV,t1.TenNV,t1.SCHName,t1.ShiftID
			,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10, M11,M12,M13,M14,M15,M16,M17,M18,M19,M20, M21,M22,M23,M24,M25,M26,M27,M28,M29,M30,M31
			,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10, S11,S12,S13,S14,S15,S16,S17,S18,S19,S20, S21,S22,S23,S24,S25,S26,S27,S28,S29,S30,S31
		from dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) t1	
		left join dbo.DIC_DANGKY_DIMUONVESOM t2 on t1.Id=t2.eID and t2.sMonth=@Month and t2.sYear=@Year
	End
	
END

GO


