GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_LYDOVANG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_LYDOVANG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lý do vắng>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_LYDOVANG]
@Action varchar(100),
@Id bigint,
@Maso nvarchar(10),
@LyDo nvarchar(50)
WITH ENCRYPTION
AS
BEGIN
	
	declare @error nvarchar(150)
	if(@Action='GET')
	Begin
		select * from dbo.DIC_LYDOVANG
	End
	if(@Action='PUT')
	Begin
		Update dbo.DIC_LYDOVANG set LyDo=@LyDo where MaSo=@Maso
		--if(select COUNT(Id) from dbo.DIC_LYDOVANG where MaSo=@Maso)>0
		--Begin
			
		--End
		--Else 
		--	Insert into dbo.DIC_LYDOVANG(MaSo,LyDo) values(@Maso,@LyDo)
	End
	Else if(@Action='POST')
	Begin
		if(select COUNT(Id) from dbo.DIC_LYDOVANG where MaSo=@Maso)>0
		Begin
			set @error=N'Mã số đã tồn tại. Vui lòng nhập mã khác!'
			RAISERROR(@error, 16, 1)
			return
		End
		Insert into dbo.DIC_LYDOVANG(MaSo,LyDo) values(@Maso,@LyDo)
	End
	Else if(@Action='DELETE')
	Begin
		if(@Id=2)
		Begin
			
			set @error=N'Mặc định không thể xóa!'
			RAISERROR(@error, 16, 1)
			return;
		End
		Delete from dbo.DIC_LYDOVANG where Id=@Id
	End
END

GO


