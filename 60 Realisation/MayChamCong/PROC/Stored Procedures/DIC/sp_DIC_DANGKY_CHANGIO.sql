GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_CHANGIO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký chặn giờ>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO] 
@TYPE as dbo.TYPE_DANGKY_CHANGIO readonly

WITH ENCRYPTION
AS
BEGIN
	Merge into dbo.MCC_DANGKY_CHANGIO A
	using @TYPE B on A.sMonth=B.sMonth and A.sYear=B.sYear and A.eID=B.eID
	when not matched then
		insert (sMonth,sYear,eID,GioVe,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10    
						,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20  
						,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31)
		
		 values(B.sMonth,B.sYear,B.eID,B.GioVe,B.D1,B.D2,B.D3,B.D4,B.D5,B.D6,B.D7,B.D8,B.D9,B.D10    
						,B.D11,B.D12,B.D13,B.D14,B.D15,B.D16,B.D17,B.D18,B.D19,B.D20  
						,B.D21,B.D22,B.D23,B.D24,B.D25,B.D26,B.D27,B.D28,B.D29,B.D30,B.D31)
	when matched then
		update set A.GioVe=B.GioVe,
				   A.D1=B.D1,
				   A.D2=B.D2,
				   A.D3=B.D3,
				   A.D4=B.D4,
				   A.D5=B.D5,
				   A.D6=B.D6,
				   A.D7=B.D7,
				   A.D8=B.D8,
				   A.D9=B.D9,
				   A.D10=B.D10,
				   
				   A.D11=B.D11,
				   A.D12=B.D12,
				   A.D13=B.D13,
				   A.D14=B.D14,
				   A.D15=B.D15,
				   A.D16=B.D16,
				   A.D17=B.D17,
				   A.D18=B.D18,
				   A.D19=B.D19,
				   A.D20=B.D20,
				   
				   A.D21=B.D21,
				   A.D22=B.D22,
				   A.D23=B.D23,
				   A.D24=B.D24,
				   A.D25=B.D25,
				   A.D26=B.D26,
				   A.D27=B.D27,
				   A.D28=B.D28,
				   A.D29=B.D29,
				   A.D30=B.D30,
				   A.D31=B.D31
	;
	
	
END

GO


