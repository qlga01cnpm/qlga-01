﻿SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		dieu
-- Create date: 29/08/2018
-- Description:	cap nhat vao bang dang ky vao ra
-- =============================================
CREATE PROCEDURE sp_DIC_DANGKY_RAVAO_VIRTUAL_CAPNHATTUDONG
	@MaCC varchar(24) = null,
	@ngay varchar(24) = null
AS
BEGIN
	
DECLARE	
		@IdEmp bigint = (select id from DIC_NHANVIEN where MaCC = @MaCC and isnull(IsNghiViec, 0) <> 1),
		@sophut2lancham int = (select SoPhutGiuaHaiLan from MCC_QUYTAC),
		@dayofweek	int = (select DATEPART(dw, @ngay)),
		@solanchamcong int = (select COUNT(*) from MCC_ATTLOGS_SESSION where MaCC = @MaCC and  Convert(date, WorkingDay, 121)=Convert(date, @ngay,121) and IsHopLe = 1)

	select	MaCC ,
            WorkingDay ,
            Gio ,
            TrangThai ,
            Door ,
            DateTimeAtt ,
            CreateDateTime ,
            ModifyDateTime ,
            IsHopLe ,
            Id 
	into	#gioquetvantay
	from	MCC_ATTLOGS_SESSION 
	where	MaCC= @MaCC 
	and		Convert(date, WorkingDay, 121) = Convert(date, @ngay,121) 
	and		IsHopLe = 1 
	 		
	select	C.Ngay, A. MaCC, A.Id MaNV, D.*
	into	#CaLamViec
	From	dbo.DIC_NHANVIEN A
			inner join MCC_SCHEDULE B 
				on A.SCHName = B.SchName
			inner join dbo.MCC_SCHEDULE_SHIFT C 
				on B.Id=C.SchID
			inner join dbo.DIC_SHIFT D 
				on C.Shift1 = D.ShiftId
	where	A.Id = @IdEmp 
	and		C.Ngay = @dayofweek		
			
	SELECT	*,
			ROW_NUMBER() over (order by GioQuet) stt
	into	#laydulieu 
	FROM  	(
		select	DISTINCT
				ca.MaCC,
				ca.MaNV,
				CONVERT(date, workingday) workingday,
				case WHEN CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ gq.gio)) 
						  <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ca.TimeIn)) 
						then ca.TimeIn 
					 else gq.Gio
				end GioQuet, 
				TimeIn,
				BreakOut,
				BreakIn,
				TimeOut, ca.TimeOff
		from	#calamviec ca
				inner join #gioquetvantay gq
					on	gq.MaCC = ca.MaCC 
	)	main 
	 		
	declare @countrow int = (select COUNT(*) from #laydulieu)
		
	-- co gio ra va gio vao				
	select	a.MaCC,
			CONVERT(varchar(24), a.MaNV) MaNV,
			a.TimeOff,
			a.TimeIn,
			a.BreakOut,
			a.BreakIn,
			a.TimeOut,
			a.WorkingDay,
			a.GioQuet as GioVao,
			b.GioQuet as GioRa,
			a.stt,
			N'1' as Note -- 1 ra vao trong gio lam
	INTO	#OUTPUT
	from	#laydulieu a
			left join #laydulieu b
				on  a.macc = b.macc
				and a.manv = b.manv	
				AND	a.GioQuet <> b.GioQuet
	where	b.GioQuet is not null
	and		a.stt = (b.stt + 1)
	and		((a.stt % 2) <> 0)

	-- neu di ra ma khong di vao				
	INSERT INTO #OUTPUT
	select	a.MaCC,
			a.MaNV,
			a.TimeOff,
			a.TimeIn,
			a.BreakOut,
			a.BreakIn,
			a.TimeOut,
			a.WorkingDay,
			a.TimeOut GioVao,
			a.GioQuet GioRa,
			a.stt,
			N'2' as Note -- 2 là ve som
	from	#laydulieu a
			left join #laydulieu b
				on  a.macc = b.macc
				and a.manv = b.manv	
			LEFT JOIN #OUTPUT OUTP
				ON	A.MACC = OUTP.MACC
				AND a.manv = outp.manv
				and a.stt = outp.stt
	where	b.GioQuet is not null
	and		a.stt = @countrow
	and		b.stt = @countrow
	and		outp.macc is null
	and		CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.GioQuet)) > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.TimeIn))
	and		CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.GioQuet)) < CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.TimeOut))

	-- di tre hay khong
	INSERT INTO #OUTPUT
	select	a.MaCC,
			a.MaNV,
			a.TimeOff,
			a.TimeIn,
			a.BreakOut,
			a.BreakIn,
			a.TimeOut,
			a.WorkingDay,
			a.GioQuet GioVao,
			a.TimeIn GioRa,
			a.stt,
			N'3' as Note -- 3 la di tre
	from	#laydulieu a
			LEFT JOIN #OUTPUT OUTP
				ON	A.MACC = OUTP.MACC
				AND a.manv = outp.manv
				and a.stt = outp.stt
	where	a.stt = 1
	and		outp.macc is null
	and		CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.GioQuet)) > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ a.TimeIn))
	 
	-- xóa những dòng bị xóa ở dữ liệu thật
	delete	dk
	from	DIC_DANGKY_RAVAO_VIRTUAL dk
			LEFT join #OUTPUT ot
				on  dk.MaNV = ot.MaNV 
				and CONVERT(date, dk.NgayDangKy,121) = CONVERT(date, ot.WorkingDay, 121)
				and dk.GioRa = ot.GioRa
	where	ot.MaNV is null
	and		dk.MaNV = CONVERT(varchar(50), @IdEmp)
	and		CONVERT(date, dk.NgayDangKy,121) = CONVERT(date, @ngay, 121)
	
	-- cập nhật dòng đã có
	update	dk
	set		GioVao = case when ot.GioVao = ot.TimeOut then ''
					 else ot.GioVao end,
			ThoiGian = case  when (ot.giora IS not NULL and ot.giovao IS not NULL) 
							THEN CASE
							-- trường hợp là 0 khi: giờ ra, vào trong giờ nghỉ trưa, ra vào trước giờ làm việc, ra vào sau giờ làm việc
							WHEN -- giữa giờ	
								(CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
								>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))	
								AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.GioVao)) 
								<= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
								OR  
								(CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
								>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))	
								AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.GioVao)) 
								>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))

								THEN 0	
							ELSE CASE WHEN CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
											BETWEEN CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeIn))
											AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
										   AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
												>=  CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn))
												THEN DATEDIFF(MINUTE, CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)), 
													  case	when CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao)) 
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut)))
																 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
																 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))
													  end) - ot.TimeOff
									  ELSE
										DATEDIFF(MINUTE, CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)), 
													  case	when CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao)) 
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut)))
																 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
																 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
															when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
																 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
																then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))
													  end)
								 END  
						  END 
					end,
			GhiChu = case when ot.note = '1' then ''
						 when ot.note = '2' then N'Về sớm'
						 when ot.note = '3' then N'Đi trễ'
					end
	from	DIC_DANGKY_RAVAO_VIRTUAL dk
			inner join #OUTPUT ot
				on  dk.MaNV= ot.MaNV 
				and CONVERT(date, dk.NgayDangKy,121) = CONVERT(date, ot.WorkingDay, 121)
				and ISNULL(dk.GioRa, '') = ISNULL(ot.GioRa, '')
	
	-- thêm mới dòng chưa có
	insert into DIC_DANGKY_RAVAO_VIRTUAL (MaNV, NgayDangKy, GioVao, GioRa, ThoiGian, GhiChu)
	select	ot.MaNV,
			ot.WorkingDay,
			case when ot.GioVao = ot.TimeOut then ''
					 else ot.GioVao end GioVao,
			case when ot.GioRa = ot.TimeIn then ''
					else ot.GioRa
			end GioRa,
			case  when (ot.giora IS not NULL and ot.giovao IS not NULL) 
					THEN CASE
					-- trường hợp là 0 khi: giờ ra, vào trong giờ nghỉ trưa, ra vào trước giờ làm việc, ra vào sau giờ làm việc
					WHEN -- giữa giờ	
						(CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
						>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))	
						AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.GioVao)) 
						<= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
						OR  
						(CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
						>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))	
						AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.GioVao)) 
						>= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))

						THEN 0	
					ELSE CASE WHEN CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)) 
									BETWEEN CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeIn))
									AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
								   AND CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
										>=  CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn))
										THEN DATEDIFF(MINUTE, CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)), 
											  case	when CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao)) 
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut)))
														 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
														 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))
											  end) - ot.TimeOff
							  ELSE
								DATEDIFF(MINUTE, CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giora)), 
											  case	when CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao)) 
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut)))
														 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakOut))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.BreakIn)))
														 and (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 <= CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
													when (CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.giovao))
														 > CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut)))
														then CONVERT(datetime, CONVERT(varchar(24), @ngay +' '+ ot.TimeOut))
											  end)
						 END  
				  END 
			end  thoigian,
			case when ot.note = '1' then ''
				 when ot.note = '2' then N'Về sớm'
				 when ot.note = '3' then N'Đi trễ'
			end Note
	from	#OUTPUT ot
			left join DIC_DANGKY_RAVAO_VIRTUAL dk
				on  dk.MaNV= ot.MaNV 
				and CONVERT(date, dk.NgayDangKy,121) = CONVERT(date, ot.WorkingDay, 121)
				and ISNULL(dk.GioRa, '') = ISNULL(ot.GioRa, '')
	where	dk.ID is null 

	drop table #CaLamViec
	drop table #gioquetvantay
	drop table #OUTPUT
	drop table #laydulieu

END
GO


