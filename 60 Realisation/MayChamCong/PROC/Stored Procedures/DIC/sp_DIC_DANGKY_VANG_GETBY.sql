GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_VANG_GETBY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_VANG_GETBY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký vắng lấy danh sách>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_VANG_GETBY] 
@DepId varchar(100),
@MaNV varchar(50),
@Month int,
@Year int

WITH ENCRYPTION
AS
BEGIN
	--exec sp_DIC_DANGKY_VANG_GETBY'NTB.KYT','',12,2017
	-- sp_DIC_DANGKY_VANG_GETBY'NTB.KYT','00002',12,2017
	--exec sp_DIC_DANGKY_VANG_GETBY'DEP.ROOT','00001',12,2017
	if(ISNULL(@MaNV,'')='')
	Begin
		Select t1.Id as eID, t1.MaNV,t1.TenNV
			,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10, D11,D12,D13,D14,D15,D16,D17,D18,D19,D20, D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31
		from dbo.fncNHANVIENBYDEP(@DepId,'') t1	
		left join dbo.DIC_DANGKY_VANG t2 on t1.Id=t2.eID  and t2.sMonth=@Month and t2.sYear=@Year
	End
	Else
	Begin
		Select t1.Id as eID,t1.MaNV,t1.TenNV 
			,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10, D11,D12,D13,D14,D15,D16,D17,D18,D19,D20, D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31
		from dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) t1	
		left join dbo.DIC_DANGKY_VANG t2 on t1.Id=t2.eID and t2.sMonth=@Month and t2.sYear=@Year
	End
	
END

GO


