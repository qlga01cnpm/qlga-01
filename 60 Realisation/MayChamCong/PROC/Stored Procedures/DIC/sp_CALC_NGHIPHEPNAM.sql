GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_CALC_NGHIPHEPNAM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CALC_NGHIPHEPNAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Tính phép năm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CALC_NGHIPHEPNAM] 
@Year int
WITH ENCRYPTION
AS
BEGIN 
	-- sp_CALC_NGHIPHEPNAM 2018
	
	--Nếu ngày năm vào làm bằng năm hiện tại thì lấy ngày vào trừ ngày cuối của năm, chia cho 30 ra số phép năm
	--Nếu làm việc trong môi trường nguy hiểm thì số ngày phép là 14, ngoài ra là 12 ngày phép
	--Nếu số năm làm việc là bội số của 5 thì được cộng thêm 1 ngày. Ví dụ đủ 5 năm thì được thêm 1 ngày phép, 10 năm thì đc thêm 2 ngày,15 năm thì 3 ngày,...

	--Cách tính tàm thời, là lấy chức vụ. Nếu là công nhân thì là 14 ngày phép
	declare @LastDayOfYear date
	set @LastDayOfYear=CONVERT(date,cast(@Year as varchar(10))+'-12-31')
	declare @tblNhanVien as table(Id bigint,TenNV nvarchar(50),BeginningDate datetime, SoNgayPhep int)
	
	insert into @tblNhanVien 
	select 
		A.Id,A.TenNV,A.BeginningDate
		,case 
			when YEAR(A.BeginningDate)=YEAR(@LastDayOfYear) then DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/30
			
			when (DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)%5=0 then ((DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)/5 )+case when A.PositionName=N'Công nhân' then 14 when A.LoaiCongViec=2 then 16 else 12 end
			else        case when A.PositionName=N'Công nhân' then 14  else 12 end           end as SoNgayPhepNam
	From dbo.DIC_NHANVIEN A;


	----when (DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)%5=0 then ((DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)/5 )+case when A.LoaiCongViec=1 then 14 when A.LoaiCongViec=2 then 16 else 12 end
	----		else        case when A.LoaiCongViec=1 then 14 when A.LoaiCongViec=2 then 16 else 12 end           end as SoNgayPhepNam
			
	----when (DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)%5=0 then ((DATEDIFF (day, A.BeginningDate,@LastDayOfYear)/365)/5 )+case when A.IsLamViecNguyHiem=1 then 14 else 12 end
	----		else        case when A.IsLamViecNguyHiem=1 then 14 else 12 end           end as SoNgayPhepNam
	
	Merge into dbo.DIC_DANGKY_PHEPNAM A
			using @tblNhanVien B
			on A.eMaNV=B.Id and A.sYear=@Year
			when not matched then
				insert(sYear,eMaNV,SoNgayPhep,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12) values(@Year,B.Id,B.SoNgayPhep
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,1,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,2,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,3,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,4,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,5,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,6,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,7,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,8,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,9,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,10,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,11,@Year,'P'))
				,(select dbo.fncReturnSoNgayNghiWith(B.Id,12,@Year,'P')) )
			when matched then
				update set SoNgayPhep=B.SoNgayPhep
				,M1=(select dbo.fncReturnSoNgayNghiWith(B.Id,1,@Year,'P'))
				,M2=(select dbo.fncReturnSoNgayNghiWith(B.Id,2,@Year,'P'))
				,M3=(select dbo.fncReturnSoNgayNghiWith(B.Id,2,@Year,'P'))
				,M4=(select dbo.fncReturnSoNgayNghiWith(B.Id,4,@Year,'P'))
				,M5=(select dbo.fncReturnSoNgayNghiWith(B.Id,5,@Year,'P'))
				,M6=(select dbo.fncReturnSoNgayNghiWith(B.Id,6,@Year,'P'))
				,M7=(select dbo.fncReturnSoNgayNghiWith(B.Id,7,@Year,'P'))
				,M8=(select dbo.fncReturnSoNgayNghiWith(B.Id,8,@Year,'P'))
				,M9=(select dbo.fncReturnSoNgayNghiWith(B.Id,9,@Year,'P'))
				,M10=(select dbo.fncReturnSoNgayNghiWith(B.Id,10,@Year,'P'))
				,M11=(select dbo.fncReturnSoNgayNghiWith(B.Id,11,@Year,'P'))
				,M12=(select dbo.fncReturnSoNgayNghiWith(B.Id,12,@Year,'P'))
			
			;
	
END

GO


