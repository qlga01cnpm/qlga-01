﻿SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký>
-- =============================================
CREATE PROCEDURE sp_DIC_DANGKY_VAORA_VIRTUAL
@Action varchar(100),
@DepId varchar(100),
@MaNV nvarchar(50)=null,
@TuNgay datetime=null,
@DenNgay datetime=null,
@Type as dbo.TYPE_DIC_DANGKY_RAVAO readonly
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
	Begin
		if(ISNULL(@MaNV,'')='')
			Select B.MaNV,B.TenNV,B.DepName,
			A.ID,A.NgayDangKy,A.GioVao,A.GioRa,A.GhiChu,A.ThoiGian,A.DiMuon,A.VeSom
			from dbo.DIC_DANGKY_VAORA_VIRTUAL A
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') B
			on A.MaNV=B.Id
			where CONVERT(date,A.NgayDangKy,121) between CONVERT(date,@TuNgay,121) and CONVERT(date,@DenNgay,121)
			order by B.MaNV asc
		Else
			Select B.MaNV,B.TenNV,B.DepName,
			A.ID,A.NgayDangKy,A.GioVao,A.GioRa,A.GhiChu,A.ThoiGian,A.DiMuon,A.VeSom
			from dbo.DIC_DANGKY_VAORA_VIRTUAL A
			inner join dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) B
			on A.MaNV=B.Id
			where CONVERT(date,A.NgayDangKy,121) between CONVERT(date,@TuNgay,121) and CONVERT(date,@DenNgay,121)
			order by B.MaNV asc
			
	End
	Else if(@Action='POST')
	Begin
		--Thêm mới thì cần thêm cột ghi chú. Cập nhật thì không cập nhật cột Ghi chú
		Merge into dbo.DIC_DANGKY_VAORA_VIRTUAL A
		using @Type B on A.MaNV=B.MaNV and CONVERT(date,A.NgayDangKy,121)=CONVERT(date,B.NgayDangKy,121)
		and A.GioRa=B.GioRa
		when not matched then
			insert(MaNV,NgayDangKy,GioRa,GioVao,ThoiGian,GhiChu,DiMuon,VeSom)
			values(B.MaNV,B.NgayDangKy,B.GioRa,B.GioVao,B.ThoiGian,B.GhiChu,B.DiMuon,B.VeSom)
		when matched then
			update set GioVao=B.GioVao,ThoiGian=B.ThoiGian,DiMuon=B.DiMuon,VeSom=B.VeSom
		;
		
	End
	Else if(@Action='PUT')
	Begin	--Cập nhật lý do
		Merge into dbo.DIC_DANGKY_VAORA_VIRTUAL A
		using @Type B on A.ID=B.ID
		when matched then
			update set GhiChu=B.GhiChu,DiMuon=B.DiMuon,VeSom=B.VeSom
		when not matched then
			insert(MaNV,NgayDangKy,GioRa,GioVao,ThoiGian,GhiChu,DiMuon,VeSom)
			values(B.MaNV,B.NgayDangKy,B.GioRa,B.GioVao,B.ThoiGian,B.GhiChu,B.DiMuon,B.VeSom)
			
		;
	End
	Else if(@Action='DELETE')
	Begin
		Merge into dbo.DIC_DANGKY_VAORA_VIRTUAL A
		using @Type B on A.ID=B.ID
		when matched then
			delete
		;
	End
END

GO


