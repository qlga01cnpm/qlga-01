GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NGAYLE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NGAYLE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký lễ>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NGAYLE]
@Action varchar(100),
@Id int,
@Ngay datetime=null,
@Name nvarchar(50)=null
WITH ENCRYPTION
AS
BEGIN
	if(@Action='POST')
	Begin
		if(select COUNT(Id) from dbo.DIC_NGAYLE where CONVERT(date,Ngay,121)=CONVERT(date,@Ngay,121))>0
		Begin
			Update dbo.DIC_NGAYLE set Name=@Name where CONVERT(date,Ngay,121)=CONVERT(date,@Ngay,121)
		End
		Else 
			Insert into dbo.DIC_NGAYLE(Ngay,Name, Active) values(@Ngay,@Name, 1)
	End
	Else if(@Action='DELETE')
	Begin
		update	dbo.DIC_NGAYLE set Active = 0 where Id=@Id
	End
	Else if(@Action='ADDAUTO')
	Begin
		-- thêm những ngày chưa có trong bảng ngày lễ
		-- và trừ những ngày lễ bị xóa (Active = 0 bị xóa)
		-- danh sách ngày lễ (01/01, 30/04, 01/05, 02/09)
		IF OBJECT_ID('tempdb..#NgayTrongNam') IS NOT NULL DROP TABLE #NgayTrongNam
		create table #NgayTrongNam (Ngay datetime)
		insert into #NgayTrongNam
		values (CONVERT(varchar(10), Year(@Ngay)) + '-01-01')
		
		;with	NgayTrongNam as (
			select	Ngay,
					1 as [level]
			from	#NgayTrongNam
			
			union all
			
			select	DATEADD(day, 1,  a.Ngay) Ngay,
					a.[level] + 1 as [level]
			from	NgayTrongNam a
					inner join #NgayTrongNam b
						on 1 = 1
			where	a.Ngay <  CONVERT(varchar(10), Year(@Ngay)) +'-12-31'
		) 
		 select Ngay into #Day from NgayTrongNam option (maxrecursion 0)
		
		
		IF OBJECT_ID('tempdb..#NgayLe') IS NOT NULL DROP TABLE #NgayLe
		create table #NgayLe (Ngay datetime, Name nvarchar(50), Active int )
		insert into #NgayLe
		select	Ngay,
				case when ngay = CONVERT(varchar(10), Year(@Ngay)) + '-09-02'
						then N'Ngày quốc khánh'
					 when ngay = CONVERT(varchar(10), Year(@Ngay)) + '-01-01'	  
						then N'Tết dương lịch'
					 when ngay = CONVERT(varchar(10), Year(@Ngay)) + '-05-01'  
						then N'Quốc tế lao động'
					 when ngay = CONVERT(varchar(10), Year(@Ngay)) + '-04-30'	  
						then N'Giải phòng miền nam'
					 else NULL
				end Name,
				1
		from	#Day
		
		drop table #Day
		
		insert	into DIC_NGAYLE (Ngay, Name, Active)
		select	nl.Ngay,
				nl.Name,
				nl.Active 
		from	#NgayLe nl
				left join DIC_NGAYLE dnl
					on	dnl.Ngay = nl.Ngay
		where	nl.Name is Not null 
		and		dnl.Id is null
	End
END

GO


