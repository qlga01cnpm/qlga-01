GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY_MANV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY_MANV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên đăng ký đi muộn về sớm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM_GETBY_MANV] 
@MaNV varchar(50),
@Month int,
@Year int

WITH ENCRYPTION
AS
BEGIN
	-- exec sp_DIC_DANGKY_DIMUONVESOM_GETBY_MANV'00001',12,2017
	declare @eID varchar(30)
	select top 1 @eID=convert(varchar(30), Id) from dbo.DIC_NHANVIEN
	where MaNV=@MaNV
	if(@eID is null) return
	
		
	Select * from dbo.DIC_DANGKY_DIMUONVESOM  
	where sMonth=@Month and sYear=@Year and eID=@eID
END

GO


