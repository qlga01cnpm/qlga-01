GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_THAISAN_GETSHIFT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_THAISAN_GETSHIFT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy ca làm việc của nhân viên đăng ký thai sản>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_THAISAN_GETSHIFT] 
@MaNV nvarchar(50),
@WorkingDay datetime
WITH ENCRYPTION
AS
BEGIN
	declare @ShiftId varchar(50),@eID varchar(50)
	select @eID=Id from dbo.DIC_NHANVIEN with(nolock) where MaNV=@MaNV
	if(@eID is null) return
	select top 1 @ShiftId=ShiftID from dbo.DIC_DANGKY_THAISAN with(nolock) 
	where MaNV=@eID and CONVERT(date,@WorkingDay,121)>=CONVERT(date,TuNgay,121)
				    and CONVERT(date,@WorkingDay,121)<=CONVERT(date,ToiNgay,121)
	if(@ShiftId is null) return
	
	select top 1 * from dbo.DIC_SHIFT where ShiftId=@ShiftId
		
END

GO


