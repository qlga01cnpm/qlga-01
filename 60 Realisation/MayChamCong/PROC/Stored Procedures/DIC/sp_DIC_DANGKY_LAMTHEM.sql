GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_LAMTHEM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_LAMTHEM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký làm thêm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_LAMTHEM] 
@Action varchar(100),
@ID bigint,
@DepId varchar(100),
@MaNV nvarchar(50),
@TuNgay datetime=null,
@DenNgay datetime=null,
@TuGio varchar(8),
@DenGio varchar(8),
@SoGio decimal(18,1)
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
	Begin
		if(ISNULL(@MaNV,'')='')
			Select t1.ID, t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.NgayDangKy,t1.TuGio,t1.DenGio,t1.SoGio
			from dbo.DIC_DANGKY_LAMTHEM t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaNV=t2.Id
			where CONVERT(date,t1.NgayDangKy,121) between CONVERT(date,@TuNgay,121) and CONVERT(date,@DenNgay,121)
			order by t2.MaNV asc
		Else
			Select t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.NgayDangKy,t1.TuGio,t1.DenGio,t1.SoGio
			from dbo.DIC_DANGKY_LAMTHEM t1
			inner join dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) t2
			on t1.MaNV=t2.Id
			where CONVERT(date,t1.NgayDangKy,121) between CONVERT(date,@TuNgay,121) and CONVERT(date,@DenNgay,121)
			order by t2.MaNV asc
	End
	else if(@Action='PUT')
	Begin -- Cap nhat
		update dbo.DIC_DANGKY_LAMTHEM set NgayDangKy=@TuNgay,
										  TuGio=@TuGio,
										  DenGio=@DenGio,
										  SoGio=@SoGio
		where ID=@ID
	End
	Else if(@Action='POST')
	Begin
		declare @eID varchar(50)
		select @eID=Id from dbo.DIC_NHANVIEN where MaNV=@MaNV
		if(@eID is null)
		Begin
			declare @error nvarchar(100)
			set @error=N'Mã nhân viên ['+@MaNV+N'] không tồn tại'
			raiserror(@error,16,1)
			return
		End
		set @ID=null
		select top 1 @ID=ID from DIC_DANGKY_LAMTHEM where MaNV= @eID and CONVERT(date,NgayDangKy,121)=CONVERT(date,@TuNgay,121)
		if(@ID is null)
			Insert into dbo.DIC_DANGKY_LAMTHEM(MaNV,NgayDangKy,TuGio,DenGio,SoGio)
			values(@eID,@TuNgay,@TuGio,@DenGio,@SoGio)
		else
			update dbo.DIC_DANGKY_LAMTHEM set 
										  TuGio=@TuGio,
										  DenGio=@DenGio,
										  SoGio=@SoGio
		where ID=@ID 
	End
	Else if(@Action='DELETE')
	Begin
		Delete from dbo.DIC_DANGKY_LAMTHEM where ID=@ID
	End
END

GO


