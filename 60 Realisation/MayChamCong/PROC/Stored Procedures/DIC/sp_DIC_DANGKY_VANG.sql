GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_VANG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_VANG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký vắng>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_VANG] 
@TYPE_DANGKY_VANG as dbo.TYPE_DANGKY_VANG readonly

WITH ENCRYPTION
AS
BEGIN
	Merge into dbo.DIC_DANGKY_VANG t1
	using @TYPE_DANGKY_VANG t2 on t1.sMonth=t2.sMonth and t1.sYear=t2.sYear and t1.eID=t2.eID
	when not matched then
		insert values(sMonth,sYear,eID,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10    ,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20  ,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31)
	when matched then
		update set t1.D1=t2.D1,
				   t1.D2=t2.D2,
				   t1.D3=t2.D3,
				   t1.D4=t2.D4,
				   t1.D5=t2.D5,
				   t1.D6=t2.D6,
				   t1.D7=t2.D7,
				   t1.D8=t2.D8,
				   t1.D9=t2.D9,
				   t1.D10=t2.D10,
				   
				   t1.D11=t2.D11,
				   t1.D12=t2.D12,
				   t1.D13=t2.D13,
				   t1.D14=t2.D14,
				   t1.D15=t2.D15,
				   t1.D16=t2.D16,
				   t1.D17=t2.D17,
				   t1.D18=t2.D18,
				   t1.D19=t2.D19,
				   t1.D20=t2.D20,
				   
				   t1.D21=t2.D21,
				   t1.D22=t2.D22,
				   t1.D23=t2.D23,
				   t1.D24=t2.D24,
				   t1.D25=t2.D25,
				   t1.D26=t2.D26,
				   t1.D27=t2.D27,
				   t1.D28=t2.D28,
				   t1.D29=t2.D29,
				   t1.D30=t2.D30,
				   t1.D31=t2.D31
	;
	
	
END

GO


