GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_THEODOI_PHEPNAM_GETBY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_THEODOI_PHEPNAM_GETBY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách phép năm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_THEODOI_PHEPNAM_GETBY] 
@DepId varchar(100),
@MaNV varchar(50),
@Year int

WITH ENCRYPTION
AS
BEGIN
	
	--CÐ,DEP.ROOT
	-- sp_DIC_THEODOI_PHEPNAM_GETBY'CÐ','',2018
	
	
	
	if(ISNULL(@MaNV,'')='')
	Begin
		/*** Hàm ***/
		/*** Tính ngày phép ***/
		exec sp_CALC_NGHIPHEPNAM @Year
	
		/*** Lấy dữ liệu ***/
		Select A.Id as eID, A.MaNV,A.TenNV,A.DepName,CONVERT(varchar(10), A.BeginningDate,103) as NgayVao
			,B.SoNgayPhep ,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12
			,ISNULL(M1,0)+ISNULL(M2,0)+ISNULL(M3,0)+ISNULL(M4,0)+ISNULL(M5,0)+ISNULL(M6,0)+
			 ISNULL(M7,0)+ISNULL(M8,0)+ISNULL(M9,0)+ISNULL(M10,0)+ISNULL(M11,0)+ISNULL(M12,0) as SoNgayNghi
			--,0 as SoNgayNghi
			--,0 as SoNgayConLai
		from dbo.fncNHANVIENBYDEP(@DepId,'') A	
		left join dbo.DIC_DANGKY_PHEPNAM B on A.Id=B.eMaNV  and B.sYear=@Year
		order by A.Id asc
	End
	Else
	Begin
	
		/*** Lấy dữ liệu ***/
		Select A.Id as eID, A.MaNV,A.TenNV,A.DepName,CONVERT(varchar(10), A.BeginningDate,103) as NgayVao
			,B.SoNgayPhep ,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12
			,ISNULL(M1,0)+ISNULL(M2,0)+ISNULL(M3,0)+ISNULL(M4,0)+ISNULL(M5,0)+ISNULL(M6,0)+
			 ISNULL(M7,0)+ISNULL(M8,0)+ISNULL(M9,0)+ISNULL(M10,0)+ISNULL(M11,0)+ISNULL(M12,0) as SoNgayNghi
			--,0 as SoNgayConLai
		from dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) A	
		left join dbo.DIC_DANGKY_PHEPNAM B on A.Id=B.eMaNV and  B.sYear=@Year
	End
	
END

GO


