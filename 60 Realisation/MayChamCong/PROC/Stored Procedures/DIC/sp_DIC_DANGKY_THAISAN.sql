GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_THAISAN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_THAISAN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký thai sản>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_THAISAN] 
@Action varchar(100),
@ID bigint,
@DepId varchar(100),
@MaNV nvarchar(50),
@TuNgay datetime=null,
@ToiNgay datetime=null,
@ShiftID nvarchar(50)=null,
@LyDo nvarchar(50)=null,
@SoNgay decimal(18,1),
@ThoiGian int
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
	Begin
		if(ISNULL(@MaNV,'')='')
			Select t1.ID, t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.LyDo,t1.ShiftID,t1.SoNgay,t1.ThoiGian,t1.ToiNgay,t1.TuNgay
			from dbo.DIC_DANGKY_THAISAN t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaNV=t2.Id
			order by t2.MaNV asc
		Else
			Select t2.MaNV,t2.TenNV,t2.DepName,
			t1.ID,t1.LyDo,t1.ShiftID,t1.SoNgay,t1.ThoiGian,t1.ToiNgay,t1.TuNgay
			from dbo.DIC_DANGKY_THAISAN t1
			inner join dbo.fncNHANVIENBYDEPMANV(@DepId,@MaNV) t2
			on t1.MaNV=t2.Id
			order by t2.MaNV asc
	End
	else if(@Action='PUT')
	Begin -- Cap nhat
		update dbo.DIC_DANGKY_THAISAN set TuNgay=@TuNgay,
										  ToiNgay=@ToiNgay,
										  ShiftID=@ShiftID,
										  LyDo=@LyDo,
										  SoNgay=@SoNgay,
										  ThoiGian=@ThoiGian
		where ID=@ID
	End
	Else if(@Action='POST')
	Begin
		declare @ShiftTep varchar(50),@eID varchar(50)
		select @eID=Id from dbo.DIC_NHANVIEN where MaNV=@MaNV
		select @ShiftTep=ShiftID from dbo.DIC_DANGKY_THAISAN where MaNV=@eID
		select @ShiftTep
		if(@ShiftTep is not null)
		Begin
			declare @error nvarchar(100)
			set @error=N'Mã nhân viên ['+@MaNV+N'] đã đăng ký ca ['+@ShiftID+']'
			raiserror(@error,16,1)
			return
		End
		Insert into dbo.DIC_DANGKY_THAISAN values(@eID,@TuNgay,@ToiNgay,@ShiftID,@LyDo,@SoNgay,@ThoiGian)
	End
	Else if(@Action='DELETE')
	Begin
		Delete from dbo.DIC_DANGKY_THAISAN where ID=@ID
	End
END

GO


