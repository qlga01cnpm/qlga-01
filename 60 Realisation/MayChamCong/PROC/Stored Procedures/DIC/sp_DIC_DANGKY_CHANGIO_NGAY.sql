GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_CHANGIO_NGAY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO_NGAY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Đăng ký chặn giờ>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_CHANGIO_NGAY] 
@TYPE as dbo.TYPE_DANGKY_CHANGIO_NGAY readonly

WITH ENCRYPTION
AS
BEGIN
	Merge into dbo.MCC_DANGKY_CHANGIO_NGAY A
	using @TYPE B on Convert(date,A.NgayChan,121)=Convert(date,B.NgayChan,121) and A.eID=B.eID
	when not matched then
		insert (NgayChan,eID,GioVe,SoPhut)
		
		 values(B.NgayChan,B.eID,B.GioVe,B.SoPhut)
	when matched then
		update set A.GioVe=B.GioVe,
				   A.SoPhut=B.SoPhut
	;
	
END

GO


