GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_DANGKY_DIMUONVESOM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_DANGKY_DIMUONVESOM]
@Action varchar(20),
@TYPE_DANGKY_DIMUONVESOM as dbo.TYPE_DANGKY_DIMUONVESOM readonly
AS
BEGIN
	
	if(@Action ='GET')
	begin
		select sMonth,sYear,eID,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12,M13,M14,M15,M16
		,M17,M18,M19,M20,M21,M22,M23,M24,M25,M26,M27,M28,M29,M30,M31,S1,S2,S3,S4,S5,S6
		,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17,S18,S19,S20,S21,S22,S23,S24,S25,S26,S27
		,S28,S29,S30,S31
		from dbo.DIC_DANGKY_DIMUONVESOM
	end
	else
	if(@Action = 'POST')
		begin
			Merge into dbo.DIC_DANGKY_DIMUONVESOM t1
			using @TYPE_DANGKY_DIMUONVESOM t2 on t1.sMonth=t2.sMonth and t1.sYear=t2.sYear and t1.eID=t2.eID
			when matched then
				update set 
					M1=	t2.M1,	M2=	t2.M2,	M3=	t2.M3,	M4=	t2.M4,	M5=	t2.M5,	M6 =t2.M6,	M7=	t2.M7,	M8=t2.M8,	M9=t2.M9,	M10=t2.M10,
					M11=t2.M11,	M12=t2.M12,	M13=t2.M13,	M14=t2.M14,	M15=t2.M15,	M16=t2.M16,	M17=t2.M17,	M18=t2.M18,	M19=t2.M19,	M20=t2.M20,
					M21=t2.M21,	M22=t2.M22,	M23=t2.M23,	M24=t2.M24,	M25=t2.M25,	M26=t2.M26,	M27=t2.M27,	M28=t2.M28,	M29=t2.M29,	M30=t2.M30,
					M31=t2.M31,
					S1= t2.S1,	S2= t2.S2,	S3= t2.S3,	S4= t2.S4,	S5= t2.S5,	S6= t2.S6,	S7= t2.S7,	S8= t2.S8,	S9= t2.S9,	S10=t2.S10,
					S11=t2.S11,	S12=t2.S12, S13=t2.S13,	S14=t2.S14,	S15=t2.S15,	S16=t2.S16,	S17=t2.S17,	S18=t2.S18,	S19=t2.S19,	S20=t2.S20,
					S21=t2.S21,	S22=t2.S22,	S23=t2.S23,	S24=t2.S24,	S25=t2.S25,	S26=t2.S16,	S27=t2.S27,	S28=t2.S28,	S29=t2.S29,	S30=t2.S30,
					S31 = t2.S31
			when not matched then
			insert (sMonth,sYear,eID,
			M1,	M2,	M3,	M4,	M5,	M6,	M7,	M8,	M9,	M10,M11,M12,M13,M14,M15,
			M16,M17,M18,M19,M20,M21,M22,M23,M24,M25,M26,M27,M28,M29,M30,M31,
			S1,	S2,	S3,	S4,	S5,	S6,	S7,	S8,	S9,	S10,S11,S12,S13,S14,S15,S16,S17,
			S18,S19,S20,S21,S22,S23,S24,S25,S26,S27,S28,S29,S30,S31)
			values( t2.sMonth,t2.sYear,t2.eID,
			t2.M1,	t2.M2,	t2.M3,	t2.M4,	t2.M5,	t2.M6,	t2.M7,	t2.M8,	t2.M9,	t2.M10,
			t2.M11,	t2.M12,	t2.M13,	t2.M14,	t2.M15,	t2.M16,	t2.M17,	t2.M18,	t2.M19,	t2.M20,
			t2.M21,	t2.M22,	t2.M23,	t2.M24,	t2.M25,	t2.M26,	t2.M27,	t2.M28,	t2.M29,	t2.M30,
			t2.M31,
			t2.S1,	t2.S2,	t2.S3,	t2.S4,	t2.S5,	t2.S6,	t2.S7,	t2.S8,	t2.S9,	t2.S10,
			t2.S11,	t2.S12,	t2.S13,	t2.S14,	t2.S15,	t2.S16,	t2.S17,	t2.S18,	t2.S19,	t2.S20,
			t2.S21,	t2.S22,	t2.S23,	t2.S24,	t2.S25,	t2.S26,	t2.S27,	t2.S28,	t2.S29,	t2.S30,
			t2.S31)
			;
		
		--if(@Action = 'DELETE')
		--begin
		--	print 'Delete'
		--	delete from dbo.DIC_DANGKY_DIMUONVESOM where sMonth = @sMonth and sYear = @sYear and eID =@eID
		--end
	end
    
END

