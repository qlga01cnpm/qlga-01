GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_CHONGHI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_CHONGHI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên đăng ký nghỉ, Cho nghỉ, cho làm lại>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_CHONGHI] 
@Action varchar(100),
@FromDate datetime,
@ToDate datetime,
@ValueSearch nvarchar(max),

@MaNV varchar(50),
@StopWorkingday datetime,
@LyDoNghi nvarchar(max)

WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
	Begin
		if(ISNULL(@ValueSearch,'')='')
			select * from dbo.DIC_NHANVIEN 
			where IsNghiViec=1 and CONVERT(date, StopWorkingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		else
			select * from dbo.DIC_NHANVIEN where IsNghiViec=1 and( MaNV like '%'+@ValueSearch+'%'
											or	TenNV like '%'+@ValueSearch+'%')
		
	ENd
	Else If(@Action='PUT')
	Begin --Khôi phục
		update dbo.DIC_NHANVIEN set IsNghiViec=0,LyDoNghiViec='',StopWorkingday=NULL
		where MaNV=@MaNV
	End
	Else If(@Action='POST')
	Begin --Check cho nghi
		update dbo.DIC_NHANVIEN set IsNghiViec=1,LyDoNghiViec=@LyDoNghi,StopWorkingday=@StopWorkingday
		where MaNV=@MaNV
	End
	
END

GO


