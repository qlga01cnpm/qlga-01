GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN] 
@Action varchar(100),
@Id bigint,
@MaNV varchar(50),
@TenNV nvarchar(100),
@MaCC int,

@CardID varchar(25),
@BirthDate datetime,
@BirthPlace nvarchar(100),
@Sex varchar(3),
@Address nvarchar(100)=null,
@NguyenQuan nvarchar(100)=null,
@CMND varchar(25)=null,
@NoiCap nvarchar(50)=null,

@BeginningDate datetime,
@StopWorkingday datetime,
@SCHName nvarchar(50)=null,
@ShiftID nvarchar(30)=null,
@DepID varchar(100)=null,
@Password nvarchar(100)=null,
@UserName nvarchar(30)=null
WITH ENCRYPTION
AS
BEGIN
	
	declare @chkMaCC int,@chkCardID int,@chkMaNV int
	if(@Action='POST')
	Begin
		select @chkMaNV=COUNT(MaNV) from dbo.DIC_NHANVIEN where MaNV=@MaNV
		select @chkMaCC=COUNT(MaCC) from dbo.DIC_NHANVIEN where MaCC=@MaCC
		select @chkCardID=COUNT(CardID) from dbo.DIC_NHANVIEN where ISNULL(CardID,'')<>'' and CardID=@CardID
		if(@chkMaNV>0)
		Begin
			raiserror(N'Mã nhân viên đã tồn tại vui lòng nhập mã khác...',16,1)
			return
		End
		if(@chkMaCC>0)
		Begin
			raiserror(N'Mã chấm công đã tồn tại vui lòng nhập mã khác...',16,1)
			return
		End
		if(@chkCardID>0)
		Begin
			raiserror(N'Mã thẻ đã tồn tại vui lòng nhập mã khác...',16,1)
			return
		End
		
		insert into dbo.DIC_NHANVIEN(MaNV,TenNV,MaCC,CardID,BirthDate,Sex,Address,BeginningDate,SCHName,Password,NguyenQuan,CMND,NoiCap,UserName)
		values(@MaNV,@TenNV,@MaCC,@CardID,@BirthDate,@Sex,@Address,@BeginningDate,@SCHName,@Password,@NguyenQuan,@CMND,@NoiCap,@UserName)
		
		select @Id=Id from dbo.DIC_NHANVIEN where MaNV=@MaNV
		if(ISNULL(@DepID,'')<>'' and @Id is not null)
			insert into dbo.DIC_CHUYENBOPHAN(MaNV,DepID,ChangedDate)
			values(@Id,@DepID,GETDATE())
		
	ENd
	Else if(@Action='PUT')
	Begin
		
		--Kiểm tra mã nhân viên và mã chấm công, có bị trùng hay không
		select @chkMaNV=COUNT(MaNV) from dbo.DIC_NHANVIEN where Id<>@Id and MaNV=@MaNV
		select @chkMaCC=COUNT(MaCC) from dbo.DIC_NHANVIEN where Id<>@Id and MaCC=@MaCC
		--select @chkCardID=COUNT(CardID) from dbo.DIC_NHANVIEN where Id<>@Id and ISNULL(CardID,'')<>'' and CardID=@CardID
		if(@chkMaNV>0)
		Begin
			raiserror(N'Mã nhân viên đã tồn tại vui lòng nhập mã khác...',16,1)
			return
		End
		if(@chkMaCC>0)
		Begin
			raiserror(N'Mã chấm công đã tồn tại vui lòng nhập mã khác...',16,1)
			return
		End
		--if(@chkCardID>0)
		--Begin
		--	raiserror(N'Cập nhật mã thẻ đã tồn vui lòng nhập mã khác...',16,1)
		--	return
		--End
		
		Update dbo.DIC_NHANVIEN 
				set MaNV=@MaNV, MaCC=@MaCC,TenNV=@TenNV,CardID=@CardID,BirthDate=@BirthDate,BirthPlace=@BirthPlace,
					Sex=@Sex,Address=@Address,BeginningDate=@BeginningDate,StopWorkingday=@StopWorkingday,
					SCHName=@SCHName,Password=@Password,NguyenQuan=@NguyenQuan,CMND=@CMND,NoiCap=@NoiCap,
					UserName=@UserName
		where Id=@Id
		
	End 
	Else if(@Action='DELETE')
	Begin
		--Đánh đấu trạng thái xóa tạm thời
		update 	dbo.DIC_NHANVIEN set IsVisible=1
		where Id=@Id
		
		/*** Xóa bộ phận ***/
		--delete from dbo.DIC_CHUYENBOPHAN where MaNV=@Id
	End

		
END

GO


