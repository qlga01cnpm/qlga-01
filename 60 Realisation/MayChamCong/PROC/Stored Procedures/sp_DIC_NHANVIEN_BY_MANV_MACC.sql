GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_BY_MANV_MACC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_BY_MANV_MACC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy nhân viên theo mã nhân viên hoặc mã chấm công>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_BY_MANV_MACC] 
@MaNV varchar(100)=null,
@MaCC int=null
WITH ENCRYPTION
AS
BEGIN
	Select * from dbo.DIC_NHANVIEN with(nolock) 
	where MaNV=@MaNV or MaCC=@MaCC

END

GO


