GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_MENU_GET_BY_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_MENU_GET_BY_USER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_MENU_GET_BY_USER] 
@Action varchar(100),
@UserId varchar(50)
WITH ENCRYPTION
AS
BEGIN
	-- sp_SYS_MENU_GET_BY_USER'GET','quanly'
	If(@Action='GET')
	Begin
		select * From SYS_MENU_MASTER order by Sort asc
		select * from dbo.SYS_USER_MENU t1 where UserID=@UserId
		--;WITH temp(MenuId, MenuName,MenuParent, Node,
		--Url,PageName,Sort,IsVisible,iconCls,ContextMenuId,ContextMenuIdFile,
		--DefaultUrl,MoTa,ChuoiBatDau,KyTuCach,SoChuSo,SoBatDau,SoKetThuc,
		--AllowView,AllowAdd,AllowEdit
		--)
  --      as (
  --              Select l1.MenuId,l1.MenuName,l1.MenuParent, 0 as Node,
  --              Url,PageName,Sort,IsVisible,iconCls,ContextMenuId,ContextMenuIdFile,
  --              DefaultUrl,MoTa,ChuoiBatDau,KyTuCach,SoChuSo,SoBatDau,SoKetThuc,
  --              AllowView,AllowAdd,AllowEdit
  --              From dbo.SYS_MENU_MASTER l1
  --              inner join SYS_USER_MENU l2
  --              on l1.MenuId=l2.MenuID and l2.UserID=@UserId and l1.MenuParent is null
  --              Union All
  --              Select b.MenuId,b.MenuName,b.MenuParent, a.Node + 1,
  --              b.Url,b.PageName,b.Sort,b.IsVisible,b.iconCls,b.ContextMenuId,b.ContextMenuIdFile,
  --              b.DefaultUrl,b.MoTa,b.ChuoiBatDau,b.KyTuCach,b.SoChuSo,b.SoBatDau,b.SoKetThuc,
  --              AllowView,AllowAdd,AllowEdit
  --              From temp as a, dbo.SYS_MENU_MASTER as b
  --              Where a.MenuId = b.MenuParent and (AllowView=1 or AllowEdit=1)
  --      )
		--select distinct * from temp mn
	End 
	
	
END

GO


