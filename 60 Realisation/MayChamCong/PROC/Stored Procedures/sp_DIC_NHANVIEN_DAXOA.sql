GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_DAXOA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_DAXOA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Nhân viên đã xóa tạm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_DAXOA] 
@Action varchar(100),
@MaNV varchar(50)
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
		select * from dbo.DIC_NHANVIEN where IsVisible=1
	Else If(@Action='PUT')
	Begin --Khôi phục
		update dbo.DIC_NHANVIEN set IsVisible=0 where MaNV=@MaNV
	End
	Else If(@Action='DELETE')
	Begin --Xóa Vĩnh viễn
		declare @eId varchar(50)
		select top 1 @eId=convert(varchar(30),Id) from dbo.DIC_NHANVIEN where MaNV=@MaNV
		if(@eId is null)return
		
		delete from dbo.DIC_CHUYENBOPHAN where MaNV=@eId
		delete from dbo.DIC_NHANVIEN where Id=CONVERT(bigint,@eId)
	End
END

GO


