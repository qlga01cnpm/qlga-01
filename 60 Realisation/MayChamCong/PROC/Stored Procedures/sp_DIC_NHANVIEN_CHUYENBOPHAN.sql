GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_CHUYENBOPHAN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_CHUYENBOPHAN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Chuyển bộ phận cho nhân viên>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_CHUYENBOPHAN] 
@MaNV varchar(50),
@DepId varchar(100),
@ChangedDate datetime
WITH ENCRYPTION
AS
BEGIN
	declare @eId varchar(50)
	select top 1 @eId=CONVERT(varchar(50),Id) from dbo.DIC_NHANVIEN where MaNV=@MaNV
	if(@eId is null) return
	
	--Kiểm tra ngày chuyển có trùng nhau không. Nếu trùng ngày thì cập nhật lại bộ phận
	--Nếu khác ngày thì thêm mới
	if(select COUNT(*) from dbo.DIC_CHUYENBOPHAN 
		where MaNV=@eId and 
		CONVERT(date,ChangedDate,121)=CONVERT(date,@ChangedDate,121))>0
	Begin -- Cap nhat
		update dbo.DIC_CHUYENBOPHAN set DepID=@DepId
		where MaNV=@eId and CONVERT(date,ChangedDate,121)=CONVERT(date,@ChangedDate,121)
	End
	Else
	Begin
		Insert into dbo.DIC_CHUYENBOPHAN(MaNV,DepID,ChangedDate,ToDate)
		values(@eId,@DepId,@ChangedDate,GETDATE())
	End
END

GO


