GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_GROUP_DEL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_GROUP_DEL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_GROUP_DEL] 
@GroupID varchar(50),
@TypeSysLog as dbo.TypeSysLog readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		/*** Write Log ***/
		exec sp_EventLogWrite @TypeSysLog
	
		if(@GroupID='Administrator' or @GroupID='SYSTEM')
		Begin
			raiserror(N'Nhóm mặc định, không thể xóa!',16,1)
			commit
			return
		End
		
		delete from dbo.SYS_USER_GROUP where GroupID=@GroupID
		delete from dbo.SYS_GROUP where GroupID=@GroupID
COMMIT

GO


