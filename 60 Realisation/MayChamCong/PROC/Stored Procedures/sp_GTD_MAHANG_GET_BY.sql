GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_GTD_MAHANG_GET_BY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GTD_MAHANG_GET_BY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GTD_MAHANG_GET_BY] 
@Action varchar(100),
@MenuId varchar(100)
WITH ENCRYPTION
AS
BEGIN 
		-- exec sp_GTD_MAHANG_GET_BY 'get','A0000386MNU'
		If(@Action='GET')
		Begin /***  ***/
			declare @Id_Dm varchar(20)
			Select top 1 @Id_Dm=Id_Dm From dbo.GTD_MAHANG where MenuId=@MenuId
			
			Select * from dbo.GTD_MAHANG where Id_Dm=@Id_Dm
			/*** Lấy thao tác theo Danh mục công đoạn ***/
			Select t1.*,t2.*
			,case when t3.HeSo is null then 0 else t3.HeSo end as HeSo
			From dbo.GTD_THAOTACMAHANG t1
			inner join dbo.IED_CONGDOAN t2
			on t1.Id_DmCD=t2.Id_Dm
			left join dbo.DIC_BACTHO t3 on t2.MaBacTho=t3.MaBacTho
			where t1.Id_DmMaHang=@Id_Dm
		End
		
END

GO


