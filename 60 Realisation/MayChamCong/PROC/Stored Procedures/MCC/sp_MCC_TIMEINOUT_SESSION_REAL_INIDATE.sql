GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE]
@Month int,
@MaxDay int,
@Year int,
@MaCC varchar(50)

WITH ENCRYPTION
AS
BEGIN
		-- sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE 12,31,2017,'1'
		declare @eID varchar(30),@MinDay int
		select @eID=Id from dbo.DIC_NHANVIEN where MaCC=@MaCC
		if(@eID is null) return
		/*** Nếu số dòng trong tháng đó đã đủ thì không thêm gì nữa ***/
		if(select COUNT(Id) from dbo.MCC_TIMEINOUT_SESSION_REAL where MONTH(Workingday)=@Month and MaNV=@eID)=@MaxDay
			return
		
		set @MinDay=1
		while(@MinDay<=@MaxDay)
		Begin
			declare @date varchar(10)
			set @date=CAST(@Year as varchar(5))+ '-'+CAST(@Month as varchar(2))+'-'+cast(@MinDay as varchar(2))
			if(select COUNT(Id) from dbo.MCC_TIMEINOUT_SESSION_REAL where CONVERT(date,Workingday,121)=@date and MaNV=@eID)=0
			Begin
				insert into dbo.MCC_TIMEINOUT_SESSION_REAL(Workingday,MaNV)
				values(@date,@eID)
			End
		
			set @MinDay=@MinDay+1
		End
		
END

GO


