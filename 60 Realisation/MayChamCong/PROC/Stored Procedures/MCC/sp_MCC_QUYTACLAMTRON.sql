GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_QUYTACLAMTRON]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_QUYTACLAMTRON]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_QUYTACLAMTRON] 
@Action varchar(100),
@Type as dbo.TYPE_MCC_QUYTACLAMTRON readonly

WITH ENCRYPTION
AS
BEGIN
		If(@Action='GET')
		Begin
			select * from dbo.MCC_QUYTACLAMTRON
		End
		If(@Action='POST') --Thêm, sửa
		Begin
				Merge into dbo.MCC_QUYTACLAMTRON t1
				using @Type t2 on t1.Id=t2.Id
				WHEN NOT MATCHED
					then insert(TuPhut,DenPhut,LamTron) 
					values(t2.TuPhut,t2.DenPhut,t2.LamTron)
					
				when matched
					then update set t1.TuPhut=t2.TuPhut,
								    t1.DenPhut=t2.DenPhut,
									t1.LamTron=t2.LamTron;
		End
		If(@Action='DELETE') --Xóa
		Begin
			Merge into dbo.MCC_QUYTACLAMTRON t1
				using @Type t2 on t1.Id=t2.Id
			when matched 
				then delete; 
		End
		
END

GO


