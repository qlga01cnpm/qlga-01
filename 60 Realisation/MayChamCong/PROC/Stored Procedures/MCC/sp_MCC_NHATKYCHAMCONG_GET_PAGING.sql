GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_NHATKYCHAMCONG_GET_PAGING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_NHATKYCHAMCONG_GET_PAGING]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_NHATKYCHAMCONG_GET_PAGING] 
@FromDate datetime,
@ToDate datetime,
@SearchTerm VARCHAR(100) = ''

,@PageIndex INT = 1
,@PageSize INT = 10

,@TotalPages int=1 OUTPUT
,@RecordCount INT OUTPUT
WITH ENCRYPTION
AS
BEGIN

		--declare @RecordCount1 int,@TTPages int
		--exec sp_MCC_NHATKYCHAMCONG_GET_PAGING '05/23/2018','05/23/2018','',1,30,@TTPages  out,@RecordCount1 out
		--select @TTPages , @RecordCount1
		
		--DROP TABLE #Results
		IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
		
		
			SELECT ROW_NUMBER() OVER
				  (
						ORDER BY t1.Id ASC
				  )AS RowNumber
				  ,t1.*
						INTO #Results
						From dbo.v_MCC_NHATKYCHAMCONG t1
						where  CONVERT(date,WorkingDay,121)
						between CONVERT(date,@FromDate,121) 
						and CONVERT(date,@ToDate,121) order by MaCC asc,WorkingDay asc
				 
				 /*** Đếm tổng số dòng ***/
				  SELECT @RecordCount = COUNT(*)
				  FROM #Results
			          
				  SELECT * FROM #Results
				  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
      
	
	/*** Tính tổng số trang ***/
	 set @TotalPages = @RecordCount / @PageSize
	 if(@RecordCount%@PageSize>0)
		set @TotalPages=@TotalPages+1
	
	--DROP TABLE #Results
    IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
	 
END

GO


