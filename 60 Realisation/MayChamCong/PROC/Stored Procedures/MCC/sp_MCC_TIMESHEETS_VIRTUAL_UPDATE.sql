GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMESHEETS_VIRTUAL_UPDATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMESHEETS_VIRTUAL_UPDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMESHEETS_VIRTUAL_UPDATE] 
@WorkingDay datetime,
@MaNV varchar(50),
@DepId varchar(100)=null,
@TimeIn varchar(8)=null,
@BreakOut varchar(8)=null,
@TimeOut varchar(8)=null,
@BreakIn varchar(8)=null,
@TotalTimeHM nvarchar(50)=null,
@TotalHours decimal(18,1),
@LyDoVang nvarchar(50)=null

WITH ENCRYPTION
AS
BEGIN
		declare @month int,@day int,@year int,@eId varchar(50),@TenNV nvarchar(50)
		set @month=MONTH(@WorkingDay)
		set @day=DAY(@WorkingDay)
		set @year=YEAR(@WorkingDay)
		select @eId=CONVERT(varchar(50), Id),@TenNV=TenNV from dbo.DIC_NHANVIEN where MaNV=@MaNV
		if(@eId is null)return
		/*** Kiểm tra nếu không có thì thêm mới ***/
		if(select COUNT(*) from dbo.MCC_TIMESHEETS_VIRTUAL where MaNV=@eId and Month=@month and Year=@year)=0
			insert into dbo.MCC_TIMESHEETS_VIRTUAL(Month,Year,MaNV)values(@month,@year,@eId)
		
		if(@LyDoVang is null) set @LyDoVang=''
		
		/*** Cập nhật ***/
		declare @sql nvarchar(max)
		set @sql = 'update dbo.MCC_TIMESHEETS_VIRTUAL set ' 
				--+ 'TenNV='+quotename(@TenNV,'''')+','
				+ quotename('I'+CONVERT(varchar(3),@day))+'='+quotename(@TimeIn,'''')+','
				+ quotename('O'+CONVERT(varchar(3),@day))+'='+quotename(@TimeOut,'''')+','
				+ quotename('T'+CONVERT(varchar(3),@day))+'='+quotename(@TotalHours,'''')+','
				+ quotename('C'+CONVERT(varchar(3),@day))+'='+quotename(@LyDoVang,'''')
				
				
				+ ' where MaNV='+QUOTENAME(@eId,'''')
					   +' and Month='+CONVERT(varchar(3),@month)
					   +' and Year='+CONVERT(varchar(5),@year)
		exec sp_executesql @sql
	
END

GO


