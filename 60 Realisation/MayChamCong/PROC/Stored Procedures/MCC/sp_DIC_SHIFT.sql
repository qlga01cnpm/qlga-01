GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_SHIFT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_SHIFT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_SHIFT] 
@Action varchar(100),
@Id int,
@TYPE_SHIFT as dbo.TYPE_DIC_SHIFT readonly
WITH ENCRYPTION
AS
BEGIN
		if(@Action='GET')
		Begin
			Select * from dbo.DIC_SHIFT
		End
		If(@Action='POST') --Them moi
		Begin
			declare @ShiftId nvarchar(50)
			select top 1 @Id=Id,@ShiftId=ShiftId from @TYPE_SHIFT
			if(@Id is not null)
			Begin
				if(select COUNT(Id) from dbo.DIC_SHIFT where Id=@Id)>0
				Begin --Kiem tra Cap nhat
					if(select COUNT(Id) from dbo.DIC_SHIFT where Id<>@Id and ISNULL(ShiftId,'')<>'' and ShiftId=@ShiftId)>0
					Begin
						raiserror(N'Mã ca làm việc đã tồn vui lòng nhập mã khác...',16,1)
						return
					End
				End
				Else
				Begin --Kiem tra them moi
					if(select COUNT(Id) from dbo.DIC_SHIFT where ISNULL(ShiftId,'')='' or ShiftId=@ShiftId)>0
					Begin
						raiserror(N'Them moi. Mã ca làm việc đã tồn vui lòng nhập mã khác...',16,1)
						return
					End
				End
			End
			
			
			;Merge into dbo.DIC_SHIFT t1
			using @TYPE_SHIFT t2
			on t1.Id=t2.Id
			when not matched then 
			insert(TuNgay,ShiftId,ShiftName
								,TimeIn
								,BreakOut
								,BreakIn
								,TimeOut
								,StartIn
								,EndIn
								,StartOut
								,EndOut
								,Total
								,TimeOff
								,CheckIn
								,CheckOut
								,TotalTime
								,IsTinhLamThem
								
								,IsSoPhutTruocCa
								,SoPhutTruocCa
								,IsSoPhutSauCa
								,SoPhutSauCa
								
								,IsShiftNight
								,IsHanhChinh
								,IsCachQuyCong
								,Status
								,IsTinhAnToi
								,BatDauAnToi
								,KetThucAnToi
								,ThoiGianAnToi
								,BatDauTinhMuon
								,KetThucTinhMuon
								,BatDauTinhSom
								,KetThucTinhSom
								,FromMinute
								,GioHienThi
								,SoGioDuCong
								,LoaiCa
								,IsHienThiMaCa
								,IsBoQuaChuNhat
								
								,IsThaiSan
								,IsCaConBu
								,IsCaThai7Thang
								,IsCaCuoiTuan
								,CaCuoiTuan
								,IsCaNgayLe
								,CaNgayLe
								,ChuyenNgay
								,IsTongSoPhutLam
								,TongSoPhutLam
								,MaxBeforeOTX
								,MaxAfterOTX
								)
			values(t2.TuNgay,
								t2.ShiftId,
								t2.ShiftName,
								t2.TimeIn,
								t2.BreakOut,
								t2.BreakIn,
								t2.TimeOut,
								t2.StartIn,
								t2.EndIn,
								t2.StartOut,
								t2.EndOut,
								t2.Total,
								t2.TimeOff,
								t2.CheckIn,
								t2.CheckOut,
								t2.TotalTime,
								
								t2.IsTinhLamThem,
								t2.IsSoPhutTruocCa,
								t2.SoPhutTruocCa,
								t2.IsSoPhutSauCa,
								t2.SoPhutSauCa,
								
								t2.IsShiftNight,
								t2.IsHanhChinh,
								t2.IsCachQuyCong,
								t2.Status,
								t2.IsTinhAnToi,
								t2.BatDauAnToi,
								t2.KetThucAnToi,
								t2.ThoiGianAnToi,
								t2.BatDauTinhMuon,
								t2.KetThucTinhMuon,
								t2.BatDauTinhSom,
								t2.KetThucTinhSom,
								t2.FromMinute,
								t2.GioHienThi,
								t2.SoGioDuCong,
								t2.LoaiCa,
								t2.IsHienThiMaCa,
								t2.IsBoQuaChuNhat
								
								,t2.IsThaiSan
								,t2.IsCaConBu
								,t2.IsCaThai7Thang
								,t2.IsCaCuoiTuan
								,t2.CaCuoiTuan
								,t2.IsCaNgayLe
								,t2.CaNgayLe
								,t2.ChuyenNgay
								,t2.IsTongSoPhutLam
								,t2.TongSoPhutLam
								,t2.MaxBeforeOTX
								,t2.MaxAfterOTX
								)
				when matched 
				then update 
				set 
					t1.TuNgay=t2.TuNgay,
					t1.ShiftId=t2.ShiftId,
					t1.ShiftName=t2.ShiftName,
					t1.TimeIn=t2.TimeIn,
					t1.BreakOut=t2.BreakOut,
					t1.BreakIn=t2.BreakIn,
					t1.TimeOut=t2.TimeOut,
					t1.StartIn=t2.StartIn,
					t1.EndIn=t2.EndIn,
					t1.StartOut=t2.StartOut,
					t1.EndOut=t2.EndOut,
					t1.Total=t2.Total,
					t1.TimeOff=t2.TimeOff,
					t1.CheckIn=t2.CheckIn,
					t1.CheckOut=t2.CheckOut,
					t1.TotalTime=t2.TotalTime,
					t1.SoGioDuCong=t2.SoGioDuCong,
					t1.BatDauTinhMuon=t2.BatDauTinhMuon,
					t1.KetThucTinhMuon=t2.KetThucTinhMuon,
					t1.BatDauTinhSom=t2.BatDauTinhSom,
					t1.KetThucTinhSom=t2.KetThucTinhSom,
					
					t1.IsTinhLamThem=t2.IsTinhLamThem,
					t1.IsSoPhutTruocCa=t2.IsSoPhutTruocCa,
					t1.SoPhutTruocCa=t2.SoPhutTruocCa,
					t1.IsSoPhutSauCa=t2.IsSoPhutSauCa,
					t1.SoPhutSauCa=t2.SoPhutSauCa,
					
					t1.IsThaiSan=t2.IsThaiSan,
					t1.IsCaConBu=t2.IsCaConBu,
					t1.IsCaThai7Thang=t2.IsCaThai7Thang,
					t1.IsCaCuoiTuan=t2.IsCaCuoiTuan,
					t1.CaCuoiTuan=t2.CaCuoiTuan,
					t1.IsCaNgayLe=t2.IsCaNgayLe,
					t1.CaNgayLe=t2.CaNgayLe,
					t1.ChuyenNgay=t2.ChuyenNgay,
					t1.IsTongSoPhutLam=t2.IsTongSoPhutLam,
					t1.TongSoPhutLam=t2.TongSoPhutLam,
					t1.MaxBeforeOTX=t2.MaxBeforeOTX,
					t1.MaxAfterOTX=t2.MaxAfterOTX
					;
		End
		Else if(@Action='DELETE')
		Begin
			Delete from dbo.DIC_SHIFT where Id=@Id
		End
		
END

GO


