GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_BC_LAODONG_CHEDO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_BC_LAODONG_CHEDO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên chế độ>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_BC_LAODONG_CHEDO] 
@FromDate datetime,
@ToDate datetime,
@DepID varchar(100),
@MaCheDo int
WITH ENCRYPTION
AS
BEGIN
	--sp_MCC_BC_LAODONG_CHEDO'05/01/2018','05/20/2018','DEP.ROOT',0
	if(@MaCheDo=0)
	Begin
		select t2.MaNV,t2.TenNV,DepName,t2.MaCC
		,t1.MaCheDo,t3.TenCheDo
		,CONVERT(varchar(10), t1.TuNgay,103) as TuNgay
		,CONVERT(varchar(10),t1.ToiNgay,103) as ToiNgay
		From dbo.DIC_DANGKY_CHEDO t1 with(nolock)
		inner join dbo.fncNHANVIENBYDEP(@DepID,'') t2 on cast(t1.MaNV as bigint)=t2.Id
		inner join dbo.DIC_CHEDO t3 on t3.MaCheDo=t1.MaCheDo
		--where CONVERT(date,t1.NgayDangKy,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		
		group by t2.MaNV,t2.TenNV,DepName,t2.MaCC,t1.TuNgay,t1.ToiNgay,t1.MaCheDo,t3.TenCheDo
		order by t2.MaCC desc
	End
	Else
	Begin
		select t2.MaNV,t2.TenNV,DepName,t2.MaCC
		,t1.MaCheDo,t3.TenCheDo
		,CONVERT(varchar(10), t1.TuNgay,103) as TuNgay
		,CONVERT(varchar(10),t1.ToiNgay,103) as ToiNgay
		From dbo.DIC_DANGKY_CHEDO t1 with(nolock)
		inner join dbo.fncNHANVIENBYDEP(@DepID,'') t2 on cast(t1.MaNV as bigint)=t2.Id
		inner join dbo.DIC_CHEDO t3 on t3.MaCheDo=t1.MaCheDo
		where t1.MaCheDo=@MaCheDo
		
		group by t2.MaNV,t2.TenNV,DepName,t2.MaCC,t1.TuNgay,t1.ToiNgay,t1.MaCheDo,t3.TenCheDo
		order by t2.MaCC desc
	End
END

GO


