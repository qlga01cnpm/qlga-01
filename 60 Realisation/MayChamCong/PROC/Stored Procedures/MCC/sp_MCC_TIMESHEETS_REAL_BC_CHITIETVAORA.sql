GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMESHEETS_REAL_BC_CHITIETVAORA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMESHEETS_REAL_BC_CHITIETVAORA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMESHEETS_REAL_BC_CHITIETVAORA] 
@Month int,
@Year int,
@ArrayEmId nvarchar(max)
WITH ENCRYPTION
AS
BEGIN
		/*** Cập nhật ***/
		declare @sql nvarchar(max)
		set @sql = 'select  dbo.MCC_TIMESHEETS_REAL set ' 
				+ quotename('I'+CONVERT(varchar(3),@day))+'='+quotename(@TimeIn,'''')+','
				+ quotename('O'+CONVERT(varchar(3),@day))+'='+quotename(@TimeOut,'''')+','
				+ quotename('T'+CONVERT(varchar(3),@day))+'='+quotename(@TotalTimeHM,'''')+','
				+ quotename('C'+CONVERT(varchar(3),@day))+'='+quotename(@LyDoVang,'''')
				
				
				+ ' where MaNV='+QUOTENAME(@eId,'''')
					   +' and Month='+CONVERT(varchar(3),@month)
					   +' and Year='+CONVERT(varchar(5),@year)
		exec sp_executesql @sql
	
END

GO


