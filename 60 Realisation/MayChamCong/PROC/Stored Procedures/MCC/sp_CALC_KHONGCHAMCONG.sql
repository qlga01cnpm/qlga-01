GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_CALC_KHONGCHAMCONG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CALC_KHONGCHAMCONG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Tính phép năm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CALC_KHONGCHAMCONG] 
@FromDate datetime,
@ToDate datetime
WITH ENCRYPTION
AS
BEGIN 
	-- sp_CALC_KHONGCHAMCONG '08/2/2018','08/15/2018'
	
	declare @tblDate as table(Workingday datetime)
	declare @tblTemp01 as table(Workingday datetime,MaNV varchar(50))
	declare @tblNhanVien as table(MaNV varchar(50))
	declare @tblNhanVienCheDo as table(MaNV varchar(50),MaCheDo int)

	--set @FromDate='07/07/2018' set @ToDate='07/07/2018'
	/*** Tính tổng số ngày ***/
	--set @TongSoNgay=DATEDIFF (day, @FromDate,@ToDate)
	/*** Lấy những nhân viên không chấm công ***/
	insert into @tblNhanVien
	select A.Id 
	from dbo.fncNHANVIENDEP() A
	where exists(select CAST(A1.MaNV as bigint) from dbo.MCC_TIMEINOUT_SESSION_REAL A1 where A1.MaNV=A.Id
	and CONVERT(date,A1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
	and ISNULL(A1.TimeIn,'')=''
	--and not exists(select CAST(MaNV as bigint) from dbo.DIC_DANGKY_CHEDO A2 where A2.ToiNgay>=@FromDate)
	) 
	group by A.Id

	--Lay nhung ngay lam viec
	insert into @tblDate
	select A.Workingday
	from MCC_TIMEINOUT_SESSION_REAL A
	where CONVERT(date,A.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
	group by A.Workingday

	--Lay nhan vien che do
	insert into @tblNhanVienCheDo
	select MaNV,A.MaCheDo
	from DIC_DANGKY_CHEDO A
	where CONVERT(date,@FromDate,121) >= CONVERT(date,A.TuNgay,121) 
	and CONVERT(date,@ToDate,121)<= CONVERT(date,A.ToiNgay,121)
	group by MaNV,MaCheDo
	
	--select * from @tblNhanVienCheDo
	--return;
	while (select COUNT(*) from @tblNhanVien)>0 and (select COUNT(*) from @tblDate)>0
	Begin
		declare @WorkingDay date
		select top 1 @WorkingDay=Workingday from @tblDate
	
		insert into @tblTemp01(MaNV,Workingday)
			select MaNV,@WorkingDay
			from @tblNhanVien
			
		delete from @tblDate where Workingday=@WorkingDay
	End
	;
	

	--select * from @tblTemp01;
	Merge into dbo.MCC_TIMEINOUT_SESSION_REAL A
				using @tblTemp01 B
				on A.MaNV=B.MaNV and convert(date, A.Workingday,121)=convert(date,B.Workingday,121)
				when not matched then
					insert(Workingday,MaNV ) values(B.Workingday,B.MaNV)
				
				;

	--Kiểm tra chế độ
	
	Merge into dbo.MCC_TIMEINOUT_SESSION_REAL A
		using @tblNhanVienCheDo B 
		on A.MaNV=B.MaNV and convert(date, A.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		
		when matched then
			update set LyDoVang=case when B.MaCheDo=1 then N'Nuôi con nhỏ'
											 when B.MaCheDo=2 then N'Mang thai 7-9 tháng' 
											 when B.MaCheDo=3 then N'Vị thành niên'
											 when B.MaCheDo=4 then N'Thai sản' 
											 else '' end
	;
	
	--Kiểm tra đăng ký vắng
	--Merge into dbo.MCC_TIMEINOUT_SESSION_REAL A
	--	using dbo.DIC_DANGKY_CHEDO B 
	--	on A.MaNV=B.MaNV
	--	when matched then
	--		update set LyDoVang=N'Chế độ' 
	--;
END

GO


