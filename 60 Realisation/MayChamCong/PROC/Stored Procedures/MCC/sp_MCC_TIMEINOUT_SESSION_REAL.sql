GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_REAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_REAL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_REAL]
@Workingday datetime,
@DepID varchar(100),
@MaNV varchar(50),
@TimeIn varchar(8),
@BreakOut varchar(8),
@BreakIn varchar(8),
@TimeOut varchar(8),
@DateTimeIn datetime,
@DateBreakOut datetime,
@DateBreakIn datetime,
@DateTimeOut datetime

WITH ENCRYPTION
AS
BEGIN
		declare @id bigint
		select @id=ID from dbo.MCC_TIMEINOUT_SESSION_REAL 
		where MaNV = @MaNV and CONVERT(date,Workingday,121)=CONVERT(date,@Workingday,121)
		
		if(@id is null or @id=0)--Thêm mới
		Begin
			insert into dbo.MCC_TIMEINOUT_SESSION_REAL(Workingday,MaNV,TimeIn,BreakOut,BreakIn,TimeOut,    DateTimeIn,DateBreakOut,DateBreakIn,DateTimeOut)
											values(@Workingday,@MaNV,@TimeIn,@BreakOut,@BreakIn,@TimeOut,   @DateTimeIn,@DateBreakOut,@DateBreakIn,@DateTimeOut)
		End
		Else --Cập nhật
		Begin 
			update dbo.MCC_TIMEINOUT_SESSION_REAL set TimeIn=@TimeIn,
													BreakOut=@BreakOut,
													BreakIn=@BreakIn,
													TimeOut=@TimeOut,
													
													DateTimeIn=@DateTimeIn,
													DateBreakOut=@DateBreakOut,
													DateBreakIn=@DateBreakIn,
													DateTimeOut=@DateTimeOut
			where ID=@id
		End
END

GO


