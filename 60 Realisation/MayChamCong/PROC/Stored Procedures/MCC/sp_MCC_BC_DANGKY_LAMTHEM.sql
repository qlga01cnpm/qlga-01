GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_BC_DANGKY_LAMTHEM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_BC_DANGKY_LAMTHEM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên đăng ký làm thêm>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_BC_DANGKY_LAMTHEM] 
@FromDate datetime,
@ToDate datetime,
@DepID varchar(100)
WITH ENCRYPTION
AS
BEGIN
	--sp_MCC_BC_DANGKY_LAMTHEM'05/01/2018','05/20/2018','DEP.ROOT'
		select t2.MaNV,t2.TenNV,DepName,t2.MaCC
		From dbo.DIC_DANGKY_LAMTHEM t1 with(nolock)
		inner join dbo.fncNHANVIENBYDEP(@DepID,'') t2 on cast(t1.MaNV as bigint)=t2.Id
		where CONVERT(date,t1.NgayDangKy,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		
		group by t2.MaNV,t2.TenNV,DepName,t2.MaCC
		order by t2.MaCC desc
END

GO


