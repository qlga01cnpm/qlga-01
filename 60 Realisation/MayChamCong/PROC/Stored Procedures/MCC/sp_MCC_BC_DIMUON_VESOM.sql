GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_BC_DIMUON_VESOM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_BC_DIMUON_VESOM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_BC_DIMUON_VESOM] 
@FromDate datetime,
@ToDate datetime
WITH ENCRYPTION
AS
BEGIN
	-- sp_MCC_BC_DIMUON_VESOM'07/01/2018','07/20/2018'
	
	--Lấy cài dữ liệu ảo
	declare @IsDataVirtual bit
	select Top 1 @IsDataVirtual=IsVirtualData from dbo.SYS_APP with(nolock)
	if(@IsDataVirtual is null) set @IsDataVirtual=0
	if(@IsDataVirtual=0) --Lấy dữ liệu thật
	Begin
		select t2.MaNV,t2.TenNV,DepName
		,convert(varchar(10),t1.Workingday,103) as Workingday
		,case when t1.LateTime<=0 then '' else t1.TimeIn end TimeIn
		,case when t1.EarlyTime<=0 then '' else t1.TimeOut end TimeOut,
		t1.LateTime,t1.EarlyTime
		From dbo.MCC_TIMEINOUT_SESSION_REAL t1 with(nolock)
		inner join dbo.fncNHANVIENDEP() t2 on t1.MaNV=t2.Id
		where CONVERT(date,Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		and(LateTime>0 or EarlyTime>0)
		order by t2.MaCC, t1.Workingday desc
	End
	Else
	Begin	--Lấy dữ liệu ảo
		select t2.MaNV,t2.TenNV,DepName
		,convert(varchar(10),t1.Workingday,103) as Workingday
		,case when t1.LateTime<=0 then '' else t1.TimeIn end TimeIn
		,case when t1.EarlyTime<=0 then '' else t1.TimeOut end TimeOut,
		t1.LateTime,t1.EarlyTime
		From dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1 with(nolock)
		inner join dbo.fncNHANVIENDEP() t2 on t1.MaNV=t2.Id and ISNULL(t2.HideVirtual, 0) = 0
		where CONVERT(date,Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		and(LateTime>0 or EarlyTime>0)
		order by t2.MaCC, t1.Workingday desc
	End
END

GO


