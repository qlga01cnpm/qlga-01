GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_BC_KHONGCHAM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_BC_KHONGCHAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách nhân viên không chấm công>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_BC_KHONGCHAM] 
@FromDate datetime,
@ToDate datetime
WITH ENCRYPTION
AS
BEGIN
	-- sp_MCC_BC_KHONGCHAM'08/29/2018','08/29/2018'
	--Lấy cài dữ liệu ảo
	declare @IsDataVirtual bit
	select Top 1 @IsDataVirtual=IsVirtualData from dbo.SYS_APP with(nolock)
	if(@IsDataVirtual is null) set @IsDataVirtual=0
	--select @IsDataVirtual
	if(@IsDataVirtual=0) --Lấy dữ liệu thật
	Begin
		/*** Tính những công nhân không chấm công ***/
		-- sp_CALC_KHONGCHAMCONG '08/29/2018','08/29/2018'
		exec sp_CALC_KHONGCHAMCONG @FromDate,@ToDate
		
		select b.id, B.MaNV,B.TenNV,DepName,IsNghiViec
		,convert(varchar(10),A.Workingday,103) as Workingday
		,case when dbo.fncReturnVang(A.MaNV,A.Workingday) <> '' 
					then dbo.fncReturnVang(A.MaNV,A.Workingday)
			  when ISNULL(A.LyDoVang,'') = '' then 'KCC' 
			  when cd.ID is null then 'KCC'
			  else A.LyDoVang end LyDoVang
		From dbo.MCC_TIMEINOUT_SESSION_REAL A with(nolock)
		inner join dbo.fncNHANVIENDEP() B on A.MaNV=B.Id
		left join DIC_DANGKY_CHEDO cd
			on	cd.MaNV = B.Id
			and CONVERT(date, Workingday) between CONVERT(date,cd.TuNgay) and CONVERT(date,cd.ToiNgay)
		where CONVERT(date,Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		and(ISNULL(A.TimeIn,'')='') and A.Workingday <= GETDATE()
		and ISNULL(B.IsNghiViec,'')=0
		order by B.MaCC, A.Workingday desc
		
		--select MaNV,TenNV,DepName,'KCC' as LyDoVang
		--from dbo.fncNHANVIENDEP() A
		--where A.Id not in( 
		--	select B.MaNV from MCC_TIMEINOUT_SESSION_REAL B 
		--	where ISNULL(B.TimeIn,'')<>'' and CONVERT(date,B.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		--	)
		--group by MaNV,TenNV,DepName,A.MaCC
		--order by A.MaCC asc
	End
	Else
	Begin	--Lấy dữ liệu ảo
		select t2.MaNV,t2.TenNV,DepName
		,convert(varchar(10),t1.Workingday,103) as Workingday
		,case when ISNULL(t1.LyDoVang,'')='' then 'KCC' else t1.LyDoVang end LyDoVang
		From dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1 with(nolock)
		inner join dbo.fncNHANVIENDEP() t2 on t1.MaNV = t2.Id and ISNULL(t2.HideVirtual, 0) = 0
		where CONVERT(date,Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
		and(ISNULL(t1.TimeIn,'')='') and t1.Workingday<=GETDATE()
		and ISNULL(t2.IsNghiViec,'')=0
		order by t2.MaCC, t1.Workingday desc
	End
END

GO


