GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách chi tiết nhân viên chấm công ảo>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING]
@FromDate datetime,
@ToDate datetime,
@DepID varchar(100),
@MaNV varchar(50)

,@PageIndex INT = 1
,@PageSize INT = 10
,@RecordCount INT OUTPUT
WITH ENCRYPTION
AS
BEGIN
		--declare @pp int
		--exec sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING '05/01/2018','05/31/2018','DEP.ROOT','',1,20,@pp out
		--select @pp
		-- sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING '11/01/2017','11/30/2017','DEP.ROOT','NV0026'
		--MCC_TIMEINOUT_SESSION_VIRTUAL
		
		--DROP TABLE #Results
		IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
		--DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results1') IS NOT NULL 
			DROP TABLE #Results1
		declare @id bigint
		if(ISNULL(@MaNV,'')='')
		Begin 
			
			SELECT ROW_NUMBER() OVER
						  (
								ORDER BY f1.Id ASC
						  )AS RowNumber
						  ,f1.Id as IdNV,f1.MaNV,f1.MaCC,f1.TenNV
								,t1.Id
								,t1.Workingday
								,t1.TimeIn,t1.BreakOut,t1.BreakIn,t1.TimeOut
								,t1.TotalTime,t1.TotalTimeHM,t1.TotalHours
								,t1.LyDoVang
								,t1.LateTime,t1.EarlyTime
								,t1.Shift,t1.OTX,t1.CongK
								INTO #Results
								From dbo.fncNHANVIENBYDEP(@DepID,@MaNV) f1
								inner join dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1
								on f1.Id=CONVERT(int,t1.MaNV) and CONVERT(date,t1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
								and t1.Workingday<=GETDATE()
								order by f1.MaNV, t1.Workingday asc
						  SELECT @RecordCount = COUNT(*)
						  FROM #Results
					          
						  SELECT * FROM #Results
						  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
		End
		Else
		Begin
			
			SELECT ROW_NUMBER() OVER
				  (
						ORDER BY f1.Id ASC
				  )AS RowNumber
				  ,f1.Id as IdNV,f1.MaNV,f1.MaCC,f1.TenNV
						,t1.Id
						,t1.Workingday
						,t1.TimeIn,t1.BreakOut,t1.BreakIn,t1.TimeOut
						,t1.TotalTime,t1.TotalTimeHM,t1.TotalHours
						,t1.LyDoVang
						,t1.LateTime,t1.EarlyTime
						,t1.Shift,t1.OTX,t1.CongK
						INTO #Results1
						from dbo.fncNHANVIENBYDEPMANV(@DepID,@MaNV) f1
						inner join dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1
						on f1.Id=CONVERT(int,t1.MaNV) and CONVERT(date,t1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
						and t1.Workingday<=GETDATE()
						order by f1.MaNV, t1.Workingday asc
				  SELECT @RecordCount = COUNT(*)
				  FROM #Results1
			          
				  SELECT * FROM #Results1
				  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
		End
		
		
		--DROP TABLE #Results
		IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
		--DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results1') IS NOT NULL 
			DROP TABLE #Results1
END

GO


