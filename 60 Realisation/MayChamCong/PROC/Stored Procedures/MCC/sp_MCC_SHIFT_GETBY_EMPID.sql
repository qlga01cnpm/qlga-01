GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_SHIFT_GETBY_EMPID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_SHIFT_GETBY_EMPID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_SHIFT_GETBY_EMPID] 
@EmpID bigint,
@Ngay int

WITH ENCRYPTION
AS
BEGIN
		
	select top 1 C.Ngay, D.* From dbo.DIC_NHANVIEN A
	inner join MCC_SCHEDULE B on A.SCHName=B.SchName
	inner join dbo.MCC_SCHEDULE_SHIFT C on B.Id=C.SchID
	inner join dbo.DIC_SHIFT D on C.Shift1=D.ShiftId
	where A.Id=@EmpID and C.Ngay=@Ngay
		
END

GO


