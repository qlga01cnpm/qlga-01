GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_VIRTUAL_DEL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIRTUAL_DEL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIRTUAL_DEL] 
@Month int,
@Year int,
@MaNV varchar(50)
WITH ENCRYPTION
AS
BEGIN
		declare @MaCC varchar(50),@eID varchar(50)
		select top 1 @MaCC=MaCC,@eID=Id from dbo.DIC_NHANVIEN with(nolock) where MaNV=@MaNV
		if(@MaCC is null) return
		
		Delete from dbo.MCC_ATTLOGS_SESSION_VIRTUAL where MaCC=@MaCC and MONTH(WorkingDay)=@Month and YEAR(WorkingDay)=@Year 
		if(@eID is not null)
		Begin
			Delete from dbo.MCC_TIMEINOUT_SESSION_VIRTUAL where MaNV=@eID and MONTH(WorkingDay)=@Month and YEAR(WorkingDay)=@Year 
			delete from dbo.MCC_TIMESHEETS_VIRTUAL where MaNV=@eID and Month=@Month and Year=@Year 
		End
END

GO


