﻿GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_SESSION_VIRTUAL_REMOVE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_MCC_ATTLOGS_SESSION_VIRTUAL_REMOVE
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].sp_MCC_ATTLOGS_SESSION_VIRTUAL_REMOVE
@Action varchar(100),
@Id bigint
AS
BEGIN
	if(@Action='DELETE')
	Begin
	
		declare @MaNV varchar(50),@MaCC varchar(50), @workingday date, @gio varchar(8)
		select	@MaCC = MaCC,
				@workingday = CONVERT(date, WorkingDay),
				@gio = Gio
		FROM	DBO.MCC_ATTLOGS_SESSION_VIRTUAL 
		WHERE	ID = @ID			
		
		select @MaNV = id from DIC_NHANVIEN where MaCC = @MaCC
		
		update	MCC_TIMEINOUT_SESSION_VIRTUAL
		set		TimeIn = ''
		where	CONVERT(date, Workingday) = @workingday
		and		MaNV = @MaNV
		and		(@gio = TimeIn)
		
		update	MCC_TIMEINOUT_SESSION_VIRTUAL
		set		[TimeOut] = ''
		where	CONVERT(date, Workingday) = @workingday
		and		MaNV = @MaNV
		and		(@gio = [TimeOut])
		
		-- xóa trong bảng đăng ký vào ra
		delete from DIC_DANGKY_RAVAO
		where	CONVERT(date, NgayDangKy) = @workingday
		and		MaNV = @MaNV
		and		(@gio = GioRa)
		
		Delete from dbo.MCC_ATTLOGS_SESSION_VIRTUAL 
		WHERE	Id = @Id		
	End
END

GO


