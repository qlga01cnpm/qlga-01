﻿GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	lấy chấm công tháng ảo, và nhân viên không bị ẩn công ảo
-- =============================================
CREATE PROCEDURE [dbo].sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG 
@Year int = null,
@Month int = null,
@MaNV varchar(50) = '',
@DepId varchar(100)=null

WITH ENCRYPTION
AS
BEGIN
	SELECT	TS.MaNV,
			ts.K1,
			ts.K2,
			ts.K3,
			ts.K4,
			ts.K5,
			ts.K6,
			ts.K7,
			ts.K8,
			ts.K9,
			ts.K10,
			ts.K11,
			ts.K12,
			ts.K13,
			ts.K14,
			ts.K15,
			ts.K16,
			ts.K17,
			ts.K18,
			ts.K19,
			ts.K20,
			ts.K21,
			ts.K22,
			ts.K23,
			ts.K24,
			ts.K25,
			ts.K26,
			ts.K27,
			ts.K28,
			ts.K29,
			ts.K30,
			ts.K31,
			NV.TenNV,
			CONVERT(VARCHAR(10), NV.BeginningDate, 103) BeginningDate
	FROM	MCC_TIMESHEETS_VIRTUAL TS
			INNER JOIN dbo.fncNHANVIENDEP() NV
				ON	 TS.MaNV = NV.Id
				and  ISNULL(IsNghiViec, 0) = 0
				and	 ISNULL(HideVirtual, 0) = 0
	WHERE	(@DepId = 'DEP.ROOT' OR @DepId = '' OR @DepId = NV.DepID)
	AND		(@MaNV = '' OR @MaNV IS NULL OR @MaNV = NV.MaNV)
	AND		(@Year = TS.Year AND @Month = TS.Month)
	order by NV.Id
END

GO


