GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy Log chấm công lọc theo bộ phận, mã nhân viên, từ ngày nào đến ngày nào.
--				 Lấy dữ liệu để xử lý chấm công, và tính lương>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING] 
@DepId varchar(100),
@MaNV varchar(50),
@FromDate datetime,
@ToDate datetime,
@SearchTerm VARCHAR(100) = ''

,@PageIndex INT = 1
,@PageSize INT = 10
,@RecordCount INT OUTPUT
WITH ENCRYPTION
AS
BEGIN

		--declare @pp int
		--exec sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING 'DEP.ROOT','','08/20/2018','08/20/2018','5807',1,500,@pp out
		--select @pp
		
		 IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
		--DROP TABLE #Results
		IF OBJECT_ID('tempdb..#Results1') IS NOT NULL 
			DROP TABLE #Results1
			
		--Lấy cài dữ liệu ảo
		declare @IsDataVirtual bit
		select Top 1 @IsDataVirtual=IsVirtualData from dbo.SYS_APP with(nolock)
		if(@IsDataVirtual is null) set @IsDataVirtual=0
		--set @IsDataVirtual=0
		if(@IsDataVirtual=0)
		Begin /*** Trường hợp dữ liệu thật ***/
			--Select t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			--,t2.DepID,t2.DepName
			--From dbo.MCC_ATTLOGS_SESSION t1
			----From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			--inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			--on t1.MaCC=t2.MaCC
			--where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			--between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
			SELECT
			 ROW_NUMBER() OVER
				  (
						ORDER BY t2.Id ASC
				  )AS RowNumber,
				  t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
						,t2.DepID,t2.DepName
						INTO #Results
						From dbo.MCC_ATTLOGS_SESSION t1
						--From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
						inner join dbo.fncNHANVIENBYDEP(@DepId,@SearchTerm) t2
						on t1.MaCC=t2.MaCC
						where  CONVERT(date,WorkingDay,121)
						--t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and
						between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) 
						order by IDNV,WorkingDay asc
				  SELECT @RecordCount = COUNT(*)
				  FROM #Results
			      --select COUNT(*) from #Results
			      
				  SELECT * FROM #Results
				  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
      
		End
		Else
		Begin /*** Trường hợp dữ liệu ảo ***/
			----Select t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			----,t2.DepID,t2.DepName
			----From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			----inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			----on t1.MaCC=t2.MaCC
			----where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			----between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
			SELECT ROW_NUMBER() OVER
				  (
						ORDER BY t2.Id ASC
				  )AS RowNumber
				  ,t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
						,t2.DepID,t2.DepName
						INTO #Results1
						From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
						--From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
						inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
						ON T1.MACC=T2.MACC
						AND ISNULL(T2.HIDEVIRTUAL, 0) = 0
						where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
						between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
				  SELECT @RecordCount = COUNT(*)
				  FROM #Results1
			          
				  SELECT * FROM #Results1
				  WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
		End
		
		--DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results') IS NOT NULL 
			DROP TABLE #Results
	  --DROP TABLE #Results
      IF OBJECT_ID('tempdb..#Results1') IS NOT NULL 
			DROP TABLE #Results1
END

GO


