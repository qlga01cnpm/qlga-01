GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy Log chấm công lọc theo bộ phận, mã nhân viên, từ ngày nào đến ngày nào.
--				 Lấy dữ liệu để xử lý chấm công, và tính lương>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIEW_BYDAY] 
@DepId varchar(100),
@MaNV varchar(50),
@FromDate datetime,
@ToDate datetime
WITH ENCRYPTION
AS
BEGIN
		--Lấy cài dữ liệu ảo
		declare @IsDataVirtual bit
		select Top 1 @IsDataVirtual=IsVirtualData from dbo.SYS_APP with(nolock)
		if(@IsDataVirtual is null) set @IsDataVirtual=0
		
		if(@IsDataVirtual=0)
		Begin /*** Trường hợp dữ liệu thật ***/
			Select t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			,t2.DepID,t2.DepName
			From dbo.MCC_ATTLOGS_SESSION t1
			--From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaCC=t2.MaCC
			where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
		End
		Else
		Begin /*** Trường hợp dữ liệu ảo ***/
			Select t1.*,t2.MaNV,t2.TenNV,t2.Id as IDNV
			,t2.DepID,t2.DepName
			From dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			inner join dbo.fncNHANVIENBYDEP(@DepId,'') t2
			on t1.MaCC=t2.MaCC
			where t2.MaNV like '%'+LTRIM(rtrim(@MaNV))+'%' and CONVERT(date,WorkingDay,121)
			between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121) order by IDNV,WorkingDay asc
		End
END

GO


