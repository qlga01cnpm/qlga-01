GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_SCHEDULE_SHIFT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_SCHEDULE_SHIFT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_SCHEDULE_SHIFT] 
@Action varchar(100),
@SchID int,
@TYPE_SCHEDULE_SHIFT as dbo.TYPE_SCHEDULE_SHIFT readonly
WITH ENCRYPTION
AS
BEGIN
		--declare @TT dbo.TYPE_SCHEDULE_SHIFT
		--exec sp_MCC_SCHEDULE_SHIFT 'get','1',@TT
		if(@Action='GET')
		Begin
			Select * from dbo.MCC_SCHEDULE_SHIFT where SchID=@SchID
			order by Ngay asc
		End
		If(@Action='POST') --Them moi
		Begin
			delete from dbo.MCC_SCHEDULE_SHIFT where SchID=@SchID;
			
			Merge into MCC_SCHEDULE_SHIFT t1
			using @TYPE_SCHEDULE_SHIFT t2 on t1.Ngay=t2.Ngay and t1.SchID=t2.SchID
			When not matched
				then insert(Ngay,SchID,SchName,Shift1,Shift2,Shift3,Shift4,Shift5,Shift6,Shift7,Shift8,Shift9)
					values(t2.Ngay,t2.SchID,t2.SchName
					,t2.Shift1,t2.Shift2,t2.Shift3,t2.Shift4,t2.Shift5,t2.Shift6,t2.Shift7,t2.Shift8,t2.Shift9);
		End
		Else if(@Action='DELETE')
		Begin
			select 'a'
		End
		
		
		
END

GO


