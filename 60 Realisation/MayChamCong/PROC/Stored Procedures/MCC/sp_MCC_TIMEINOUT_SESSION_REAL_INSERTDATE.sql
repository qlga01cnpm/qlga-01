﻿GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE
@Month int,
@MaxDay int,
@Year int

WITH ENCRYPTION
AS
BEGIN
		declare @MinDay int = 1

		/*** Nếu số dòng trong tháng đó đã đủ thì không thêm gì nữa ***/
		if (select COUNT(*) from dbo.MCC_TIMEINOUT_SESSION_REAL 
			where MONTH(Workingday)= @Month and YEAR(Workingday)=@Year) > 0
			return

		while(@MinDay<=@MaxDay)
		Begin
			declare @date varchar(10)
			set @date = CAST(@Year as varchar(5))+ '-' +CAST(@Month as varchar(2))+ '-' +cast(@MinDay as varchar(2))
			
			insert into dbo.MCC_TIMEINOUT_SESSION_REAL(Workingday,MaNV)
			select	@date, Id
			from	DIC_NHANVIEN
			where	ISNULL(IsNghiViec, 0) = 0		

			set @MinDay=@MinDay+1
		End
		
END

GO


