GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy thời gian ra(thời gian nghỉ sáng-nghỉ trưa), và thời gian vào (thời gian sau khi nghỉ trưa vào)>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET]
@Workingday datetime,
@MaCC nvarchar(50),
@MinGioCham varchar(8),
@MaxGioCham varchar(8),
@BreakOut varchar(8) out /**Giờ ra(Giờ ra nghỉ trưa)**/,
@BreakIn varchar(8) out /**Giờ vào (sau khi nghỉ trưa) ***/
WITH ENCRYPTION
AS
BEGIN
		/*** Kiểm tra định dạng chuỗi có đúng là định dạng giờ không ***/
		If(ISNUMERIC(REPLACE(@MinGioCham,':',''))=0)
		Begin
			raiserror(N'Định dạng giờ chấm vào "@MinGioCham" không hợp lệ',16,1)
			return
		End
		/*** Kiểm tra định dạng chuỗi có đúng là định dạng giờ không ***/
		If(ISNUMERIC(REPLACE(@MaxGioCham,':',''))=0)
		Begin
			raiserror(N'Định dạng giờ chấm ra về "@MaxGioCham" không hợp lệ',16,1)
			return
		End
		/** Lấy giờ ra (giờ ra nghỉ sau khi vào làm việc) **/
		SELECT @BreakOut= Min(Gio) FROM dbo.MCC_ATTLOGS_SESSION
		where MaCC=@MaCC and CONVERT(date,WorkingDay,121)=CONVERT(date,@Workingday,121) and
			  CONVERT(time(7),Gio)>CONVERT(time(7),@MinGioCham) and
			  CONVERT(time(7),Gio)<CONVERT(time(7),@MaxGioCham) and IsHopLe=1
			  
		if(@BreakOut is not null)
		Begin
			/** Lấy giờ vào (là giờ sau khi ra nghỉ trưa và bắt đầu vào làm lại) **/
			Select @BreakIn=MAX(Gio) from dbo.MCC_ATTLOGS_SESSION
			Where MaCC=@MaCC and CONVERT(date,WorkingDay,121)=CONVERT(date,@Workingday,121) and
				  CONVERT(time(7),Gio)<CONVERT(time(7),@MaxGioCham) and
				  CONVERT(time(7),Gio)>CONVERT(time(7),@BreakOut) and IsHopLe=1
		End
	
END

GO


