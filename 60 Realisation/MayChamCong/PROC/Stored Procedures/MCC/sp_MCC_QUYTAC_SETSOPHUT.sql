GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_QUYTAC_SETSOPHUT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_QUYTAC_SETSOPHUT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_QUYTAC_SETSOPHUT] 
@SoPhutGiuaHaiLan int
WITH ENCRYPTION
AS
BEGIN
	   update dbo.MCC_QUYTAC set SoPhutGiuaHaiLan=@SoPhutGiuaHaiLan
END

GO


