GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_USER_SET_PRIVILEGE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_USER_SET_PRIVILEGE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Cài đặt vai trò của người dùng trên máy chấm công>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_USER_SET_PRIVILEGE] 
@ArrayMaCC varchar(max),
@Privilege int	--0: common user, 1: enroller(ghi danh), 2: administrator, 3: super administrator
WITH ENCRYPTION
AS
BEGIN
		update dbo.DIC_NHANVIEN set Privilege=@Privilege
		where MaCC in(@ArrayMaCC)
END

GO


