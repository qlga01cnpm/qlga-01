GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS] 
@Action varchar(100),
@TYPE_ATTLOGS as dbo.TYPE_ATTLOGS readonly
WITH ENCRYPTION
AS
BEGIN
		If(@Action='POST') --Them moi
		Begin
			/*** (1) Thêm vào bảng chính không chỉnh sửa gì trên bảng này ***/
			Merge into dbo.MCC_ATTLOGS t1
			using @TYPE_ATTLOGS t2
			on t1.MaCC=t2.MaCC and t1.WorkingDay=t2.WorkingDay
			when not matched then 
			insert values(t2.MaCC,t2.WorkingDay,t2.Gio,t2.TrangThai,t2.Door,t2.DateTimeAtt,GetDate(),NULL);
			
			/*** (2) Thêm vào bảng phụ (bản sao) mọi xử lý đều thao tác trên mã này (Tab Quẹt thẻ) ***/
			-- Khi nhấn nhút "Xử lý" thì sẽ lấy dữ liệu từ bảng này đi xử lý. Khi dữ liệu được xử lý sẽ lưu vào bảng "MCC_TINHCONG_REAL" để tính công
			Merge into dbo.MCC_ATTLOGS_SESSION t1
			using @TYPE_ATTLOGS t2
			on t1.MaCC=t2.MaCC and t1.WorkingDay=t2.WorkingDay
			when not matched then
			insert values(t2.MaCC,t2.WorkingDay,t2.Gio,t2.TrangThai,t2.Door,t2.DateTimeAtt,GetDate(),NULL,t2.IsHopLe);
			
			/*** (3) Thêm vào bảng Nhật ký chấm công ***/
			Merge into dbo.MCC_NHATKYCHAMCONG t1
			using @TYPE_ATTLOGS t2 
			on t1.MaCC=t2.MaCC and t1.WorkingDay=t2.WorkingDay
			when not matched then
			insert values(t2.MaCC,t2.WorkingDay,t2.Gio,t2.TrangThai,t2.Door,t2.DateTimeAtt,t2.IsHand,t2.IsHopLe,t2.IsUsed,Getdate(),null);
			
			/*** (4) Cập nhật số lần Update cuối cùng, Giá trị thời gian và ngày giờ người cuối cùng chấm ***/
			-- Lưu về bảng MCC_TERMINAL. Khi tải dữ liệu về so sách nếu lần Update là mới nhất thì không tại nữa
			declare @tblTemp table(TerminalID int)
			insert into @tblTemp
			select CAST(t.Door as int) from @TYPE_ATTLOGS t
			group by t.Door;
			
			Merge into dbo.MCC_TERMINAL t1
			using @tblTemp t2 on t1.TerminalID=t2.TerminalID
			When MATCHED then
			Update set t1.LastRecordDate=dbo.fncReturnLastTimeDownload(t1.TerminalID),
					   t1.LastTimeGetData=getdate();
			
			  
			 
			 --delete from dbo.MCC_NHATKYCHAMCONG
			 --delete from dbo.MCC_ATTLOGS_SESSION
			 --delete from dbo.MCC_ATTLOGS
			 --delete from dbo.MCC_TIMEINOUT_SESSION_REAL
			 --update dbo.MCC_TERMINAL set LastRecordDate='2017/01/01'
			 
			 
		End
		
		
		
END

GO


