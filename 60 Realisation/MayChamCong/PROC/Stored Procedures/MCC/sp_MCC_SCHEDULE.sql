GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_SCHEDULE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_SCHEDULE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_SCHEDULE] 
@Action varchar(100),
@Id int,
@SchName nvarchar(50),
@Shift nvarchar(100),
@Chuky int,
@Descriptions nvarchar(200),
@NumberShift int,
@Shift1 varchar(10),
@Shift2 varchar(10),
@Shift3 varchar(10),
@TimeInS1 varchar(10),
@TimeInS2 varchar(10),
@TimeInS3 varchar(10)

WITH ENCRYPTION
AS
BEGIN
		if(@Action='GET')
		Begin
			Select * from dbo.MCC_SCHEDULE
		End
		If(@Action='POST') --Them moi
		Begin
			If(select COUNT(SchName) from dbo.MCC_SCHEDULE where SchName=@SchName)>0
			Begin
				raiserror(N'Mã lịch trình đã tồn tại. Vui lòng nhập mã khác...',16,1)
				return
			End
			Insert into dbo.MCC_SCHEDULE values(@SchName,@Shift,@Chuky,@Descriptions,
												@NumberShift,@Shift1,@Shift2,@Shift3,
												@TimeInS1,@TimeInS2,@TimeInS3)
		End
		Else if(@Action='PUT')
		Begin
			if(select COUNT(Id) from dbo.MCC_SCHEDULE where Id<>@Id and SchName=@SchName)>0
			Begin
				raiserror(N'Mã lịch trình đã tồn. Vui lòng nhập mã khác...',16,1)
				return
			End
					
			update dbo.MCC_SCHEDULE set SchName=@SchName,
										Shift=@Shift,
										Chuky=@Chuky,
										Descriptions=@Descriptions,
										NumberShift=@NumberShift,
										Shift1=@Shift1,
										Shift2=@Shift2,
										Shift3=@Shift3,
										TimeInS1=@TimeInS1,
										TimeInS2=@TimeInS2,
										TimeInS3=@TimeInS3
			where Id=@Id
		End
		Else if(@Action='DELETE')
		Begin
			delete from dbo.MCC_SCHEDULE_SHIFT where SchID=@Id
			Delete from dbo.MCC_SCHEDULE where Id=@Id
		End
		
		
		
END

GO


