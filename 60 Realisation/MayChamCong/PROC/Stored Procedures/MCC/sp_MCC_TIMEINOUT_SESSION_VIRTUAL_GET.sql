GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy danh sách chi tiết nhân viên chấm công ảo>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET]
@FromDate datetime,
@ToDate datetime,
@DepID varchar(100),
@MaNV varchar(50)

WITH ENCRYPTION
AS
BEGIN
		-- sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET '05/02/2018','05/02/2018','DEP.ROOT',''
		-- sp_MCC_TIMEINOUT_SESSION_REAL_GET '11/01/2017','11/30/2017','DEP.ROOT','NV0026'
		declare @id bigint
		if(ISNULL(@MaNV,'')='')
		Begin 
			
			Select f1.Id as IdNV,f1.MaNV,f1.MaCC,f1.TenNV
			,t1.Id
			,t1.Workingday
			,t1.TimeIn,t1.BreakOut,t1.BreakIn,t1.TimeOut
			,t1.TotalTime,t1.TotalTimeHM,t1.TotalHours
			,t1.LyDoVang
			,t1.LateTime,t1.EarlyTime
			,t1.Shift,t1.OTX
			From dbo.fncNHANVIENBYDEP(@DepID,'') f1
			inner join dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1
			on f1.Id=CONVERT(int,t1.MaNV) and CONVERT(date,t1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
			and ISNULL(f1.HideVirtual, 0) = 0
			order by f1.MaNV, t1.Workingday asc
		End
		Else
		Begin
			
			Select f1.Id as IdNV,f1.MaNV,f1.MaCC,f1.TenNV
			,t1.Id
			,t1.Workingday
			,t1.TimeIn,t1.BreakOut,t1.BreakIn,t1.TimeOut
			,t1.TotalTime,t1.TotalTimeHM,t1.TotalHours
			,t1.LyDoVang
			,t1.LateTime,t1.EarlyTime
			,t1.Shift,t1.OTX
			from dbo.fncNHANVIENBYDEPMANV(@DepID,@MaNV) f1
			inner join dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1
			on f1.Id=CONVERT(int,t1.MaNV) and CONVERT(date,t1.Workingday,121) between CONVERT(date,@FromDate,121) and CONVERT(date,@ToDate,121)
			and ISNULL(f1.HideVirtual, 0) = 0
			order by f1.MaNV, t1.Workingday asc
		End
END

GO


