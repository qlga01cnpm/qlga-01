GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_VIRTUAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIRTUAL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS_VIRTUAL] 
@Action varchar(100),
@TYPE_ATTLOGS as dbo.TYPE_ATTLOGS readonly
WITH ENCRYPTION
AS
BEGIN
		If(@Action='POST') --Them moi
		Begin
			/*** (1) Thêm vào bảng phụ (bản sao) mọi xử lý đều thao tác trên mã này (Tab Quẹt thẻ) ***/
			-- Khi nhấn nhút "Xử lý" thì sẽ lấy dữ liệu từ bảng này đi xử lý. Khi dữ liệu được xử lý sẽ lưu vào bảng "MCC_TINHCONG_REAL" để tính công
			Merge into dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			using @TYPE_ATTLOGS t2
			on t1.MaCC=t2.MaCC 
			and t1.DateTimeAtt=t2.DateTimeAtt
			when not matched then
			insert values(t2.MaCC,t2.WorkingDay,t2.Gio,t2.TrangThai,t2.Door,t2.DateTimeAtt,GetDate(),NULL,t2.IsHopLe);
		End
		Else if(@Action='PUT')
		Begin
			Merge into dbo.MCC_ATTLOGS_SESSION_VIRTUAL t1
			using @TYPE_ATTLOGS t2
			on t1.MaCC=t2.MaCC 
			and t1.DateTimeAtt=t2.DateTimeAtt
			when not matched then
				insert values(t2.MaCC,t2.WorkingDay,t2.Gio,t2.TrangThai,t2.Door,t2.DateTimeAtt,GetDate(),NULL,t2.IsHopLe)
			when matched then
				update set Gio=t2.Gio
			
			;
		End
END

GO


