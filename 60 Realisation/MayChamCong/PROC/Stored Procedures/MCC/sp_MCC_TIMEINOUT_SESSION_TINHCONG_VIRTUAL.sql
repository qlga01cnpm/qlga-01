GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TIMEINOUT_SESSION_TINHCONG_VIRTUAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_TINHCONG_VIRTUAL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Cập nhật sau khi tính công ảo>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TIMEINOUT_SESSION_TINHCONG_VIRTUAL] 
@Action varchar(100),
@TYPE as dbo.TYPE_TIMEINOUT_SESSION readonly
WITH ENCRYPTION
AS
BEGIN
		If(@Action='PUT') --Cập nhật
		Begin
			Merge into dbo.MCC_TIMEINOUT_SESSION_VIRTUAL t1
			using @TYPE t2 on t1.Id=t2.Id
			when matched
				then update set TotalTime=t2.TotalTime
								,TotalTimeHM=t2.TotalTimeHM
								,TotalHours=t2.TotalHours
								,LyDoVang=t2.LyDoVang
								,Shift=t2.Shift
								,LateTime=t2.LateTime
								,EarlyTime=t2.EarlyTime
								,OTX=t2.OTX
			
			;
			 
		End
		
		
		
END

GO


