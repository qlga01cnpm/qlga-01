GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_SCHEDULE_SHIFT_BY_SCHNAME]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_SCHEDULE_SHIFT_BY_SCHNAME]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_SCHEDULE_SHIFT_BY_SCHNAME] 
@SchName nvarchar(100)
WITH ENCRYPTION
AS
BEGIN
		--exec sp_MCC_SCHEDULE_SHIFT_BY_SCHNAME'LT.CA.HC'
	Select * From dbo.MCC_SCHEDULE_SHIFT 
	where SchID in(select Id from dbo.MCC_SCHEDULE where SchName=@SchName)
END

GO


