GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_ATTLOGS_EDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_ATTLOGS_EDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_ATTLOGS_EDIT] 
@Action varchar(100),
@MaCC varchar(50),
@WorkingDay datetime,
@DateTimeAtt datetime,
@Gio varchar(8),
@Id bigint=null
WITH ENCRYPTION
AS
BEGIN

	if(@Action='PUT')
	Begin	/*** Cập nhật ***/
		update dbo.MCC_ATTLOGS_SESSION set Gio=@Gio,
		WorkingDay=@WorkingDay,DateTimeAtt=@DateTimeAtt,
		CreateDateTime=@DateTimeAtt
		where Id=@id
				--MaCC=@MaCC 
				--and CONVERT(date,WorkingDay,121)=CONVERT(date,@WorkingDay,121)
				-- and Gio=@Gio
	End
	Else if(@Action='POST') 
	Begin /*** Thêm mới ***/
		If(select COUNT(MaCC) from dbo.MCC_ATTLOGS_SESSION where MaCC=@MaCC 
				and CONVERT(date,WorkingDay,121)=CONVERT(date,@WorkingDay,121) and Gio=@Gio)>0
		Begin
			raiserror(N'Giờ không hợp lệ. Vui lòng nhập giờ khác.',16,1)
			return
		End
		
		Insert into dbo.MCC_ATTLOGS_SESSION(MaCC,WorkingDay,Gio,DateTimeAtt,CreateDateTime,IsHopLe)
		values(@MaCC,CONVERT(varchar(10),@WorkingDay,121)+' '+@Gio,@Gio,@DateTimeAtt,GETDATE(),1)
	End
END

GO


