GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MCC_TERMINAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MCC_TERMINAL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MCC_TERMINAL] 
@Action varchar(100),
@TerminalID int,
@Name nvarchar(50),
@Connection nvarchar(15),
@BaudRate int,
@IpAddress nvarchar(20),
@Port int,
@CommType int,
@Status bit,

@ActiveKey nvarchar(150)=null,
@SerialNumber nvarchar(50)=null

WITH ENCRYPTION
AS
BEGIN
		If(@Action='POST') --Them moi
		Begin
			If(Select count(TerminalID) from dbo.MCC_TERMINAL where TerminalID=@TerminalID)>0
			Begin
				raiserror(N'Mã số đã tồn tại vui lòng nhập mã số khác...',16,1)
				return
			End
			/*** THÊM MỚI ***/
			Insert into dbo.MCC_TERMINAL(TerminalID,Name,Connection,BaudRate,IpAddress,Port,CommType,Status)
			values(@TerminalID,@Name,@Connection,@BaudRate,@IpAddress,@Port,@CommType,@Status)
		End
		Else if(@Action='PUT')
		Begin
				/*** CẬP NHẬT TRONG TRƯỜNG HỢP MÃ ĐÃ CÓ ***/
			Update dbo.MCC_TERMINAL set Name=@Name,Connection=@Connection,BaudRate=@BaudRate,
										IpAddress=@IpAddress,Port=@Port,CommType=@CommType,
										Status=@Status,SerialNumber=@SerialNumber,ActiveKey=@ActiveKey
			where TerminalID=@TerminalID
		End
		Else if(@Action='DELETE')
		Begin
			/*** XÓA ***/
			delete del from dbo.MCC_TERMINAL del
			where del.TerminalID=@TerminalID
		End 
		
END

GO


