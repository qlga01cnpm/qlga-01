GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_CAPNHAT_VT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_CAPNHAT_VT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Cập nhật vân tay>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_CAPNHAT_VT] 
@MaCC int,
@TenNV nvarchar(50),
@CardID varchar(25),
@sTmpData0 text=null,
@sTmpData1 text=null,
@sTmpData2 text=null,
@sTmpData3 text=null,
@sTmpData4 text=null,
@sTmpData5 text=null,
@sTmpData6 text=null,
@sTmpData7 text=null,
@sTmpData8 text=null,
@sTmpData9 text=null,
@Privilege int,
@Enabled bit,
@Password nvarchar(100)=null,
@BackupNumber int

WITH ENCRYPTION
AS
BEGIN
	
	update dbo.DIC_NHANVIEN set 
								CardID=@CardID,
								Privilege=@Privilege,
								UserName=@TenNV,
								Enabled=@Enabled,
								Password=@Password,
								BackupNumber=@BackupNumber,
								sTmpData0=@sTmpData0,
								sTmpData1=@sTmpData1,
								sTmpData2=@sTmpData2,
								sTmpData3=@sTmpData3,
								sTmpData4=@sTmpData4,
								sTmpData5=@sTmpData5,
								sTmpData6=@sTmpData6,
								sTmpData7=@sTmpData7,
								sTmpData8=@sTmpData8,
								sTmpData9=@sTmpData9
  where MaCC=@MaCC
		
END

GO


