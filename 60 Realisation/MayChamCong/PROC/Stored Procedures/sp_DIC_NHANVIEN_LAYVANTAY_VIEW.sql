GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_LAYVANTAY_VIEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_LAYVANTAY_VIEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy nhân viên theo phòng ban và mã nhân viên>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_LAYVANTAY_VIEW] 
@SelectedPage int,
@PageSize int,
@DepID varchar(50),
@TenNV nvarchar(100),
@MaCC nvarchar(30),
@TotalPages int output,
@TotalRecords int output
WITH ENCRYPTION
AS
BEGIN
	
	--declare @t1 int,@t2 int
	--exec sp_DIC_NHANVIEN_LAYVANTAY_VIEW 1,10000,'DEP.ROOT','Hạnh','0',@t1 out,@t2 out
	--select @t1,@t2
	if(@MaCC='0' or @MaCC='')set @MaCC='%%'
		
	set @TotalPages=0
	set @TotalRecords=0
	DECLARE @ReturnedRecords int
	-- Finds total records
	select @TotalRecords=COUNT(UserId) from  dbo.fncNHANVIEN_VANTAY(@DepID,@TenNV) 
	where cast(UserId as varchar(30)) like @MaCC
		
	-- Finds number of pages
	SET @ReturnedRecords = (@PageSize * @SelectedPage)
	 set @TotalPages = @TotalRecords / @PageSize
	IF @TotalRecords % @PageSize > 0
	BEGIN
		 SET @TotalPages = @TotalPages + 1
	END

	;WITH LogEntries AS (
	SELECT ROW_NUMBER() OVER (ORDER BY UserId )
	AS RowNumber, UserId 
	FROM dbo.fncNHANVIEN_VANTAY(@DepID,@TenNV)
	WHERE cast(UserId as varchar(30)) like @MaCC
	)
		 
	SELECT LogEntries.RowNumber,t.*
	FROM LogEntries
	INNER JOIN dbo.fncNHANVIEN_VANTAY(@DepID,@TenNV) t ON LogEntries.UserId = t.UserId 
	WHERE RowNumber between 
	CONVERT(nvarchar(10), (@SelectedPage - 1) * @PageSize + 1)  and CONVERT(nvarchar(10), @SelectedPage*@PageSize)
	and cast(t.UserId as varchar(30)) like @MaCC
	ORDER BY LogEntries.UserId asc
		
END

GO


