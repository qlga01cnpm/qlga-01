GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_ROOT_DIRECTORY_GET_BY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_ROOT_DIRECTORY_GET_BY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_ROOT_DIRECTORY_GET_BY] 
@Action varchar(100),
@id varchar(50)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	-- sp_SYS_ROOT_DIRECTORY_GET_BY 'get','ROOT.THIETBI'
	If(@Action='GET')
	Begin
		;WITH temp(Id, Name,ParentId,ContextMenuId,Sort,url,iconCls,IsVisible, Node)
				as (
						Select l1.Id,l1.Name,l1.ParentId,l1.ContextMenuId,l1.Sort,l1.url,l1.iconCls,l1.IsVisible, 0 as Node
						From dbo.SYS_ROOT_DIRECTORY l1
						Where l1.Id=@id
						Union All
						Select b.Id,b.Name,b.ParentId,b.ContextMenuId,b.Sort,b.url,b.iconCls,b.IsVisible, a.Node + 1
						From temp as a, dbo.SYS_ROOT_DIRECTORY as b
						Where a.Id = b.ParentId
				)
		select * From temp 
		order by Sort asc

	End /***End action GET ***/
	
COMMIT

GO


