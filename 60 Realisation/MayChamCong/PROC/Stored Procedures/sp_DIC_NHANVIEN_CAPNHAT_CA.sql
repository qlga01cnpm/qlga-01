GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_CAPNHAT_CA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_CAPNHAT_CA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Cập nhật ca làm việc, lịch trình, cho nhân viên>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_CAPNHAT_CA] 
@TYPE_NHANVIEN_SCHEDULE as dbo.TYPE_NHANVIEN_SCHEDULE readonly
WITH ENCRYPTION
AS
BEGIN
	
	Merge into dbo.DIC_NHANVIEN t1
	using @TYPE_NHANVIEN_SCHEDULE t2 on t1.Id=t2.Id
	when matched 
		then update set t1.SCHName=t2.SCHName,t1.ShiftID=t2.ShiftID;
		
END

GO


