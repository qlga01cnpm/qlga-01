GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_EXCEPTION]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_EXCEPTION]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Nhân viên không lên khi chấm công ảo>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_EXCEPTION] 
@Action varchar(100),
@MaNV varchar(50)
WITH ENCRYPTION
AS
BEGIN
	if(@Action='GET')
		select * from dbo.DIC_NHANVIEN where HideVirtual=1
	Else If(@Action='PUT')
	Begin --Khôi phục
		update dbo.DIC_NHANVIEN set HideVirtual=0 where MaNV=@MaNV
	End
	Else If(@Action='POST')
	Begin --Check ngoại lệ
		update dbo.DIC_NHANVIEN set HideVirtual=1 where MaNV=@MaNV
	End
	
END

GO


