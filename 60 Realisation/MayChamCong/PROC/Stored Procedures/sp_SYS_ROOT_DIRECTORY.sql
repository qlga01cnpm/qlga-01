GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_ROOT_DIRECTORY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_ROOT_DIRECTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_ROOT_DIRECTORY] 
@Action varchar(100),
@Id varchar(100),
@Name nvarchar(255),
@ParentId varchar(100),
@RootType varchar(2),
@ContextMenuId varchar(100),
@Url nvarchar(max),
@Sort int,
@OutNewName nvarchar(150) output
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	declare @Count int
	set @OutNewName=''
	If(@Action='POST')
	Begin
		select @Count=COUNT(Id) from dbo.SYS_ROOT_DIRECTORY where Id=@Id
		if(@Count=0)
		Begin
			select @Sort=MAX(Sort) from dbo.SYS_ROOT_DIRECTORY where ParentId=@ParentId
			if(@Sort is null)
			Begin
				set @Sort=0
			End
			Else if(@Sort>0)
				set @Name=@Name+' '+CONVERT(varchar(10),@Sort)+''
			/*** Insert ***/
			insert into dbo.SYS_ROOT_DIRECTORY(Id,Name,ParentId,ContextMenuId,Url,Sort)
			values(@Id,@Name,@ParentId,@ContextMenuId,@Url,@Sort+1)
			
			
			/*** Cài đặt ContextMenu  ***/
			;WITH temp(Id, text,ParentId,iconCls,Sort,href,onclick, Node)
						as (
								Select l1.Id,l1.text,l1.ParentId,l1.iconCls,l1.Sort,l1.href,l1.onclick, 0 as Node
								From dbo.SYS_CONTEXT_MENU l1
								Where l1.Id=@ContextMenuId
								Union All
								Select b.Id,b.text,b.ParentId,b.iconCls,b.Sort,b.href,b.onclick, a.Node + 1
								From temp as a, dbo.SYS_CONTEXT_MENU as b
								Where a.Id = b.ParentId
			)
			insert into dbo.SYS_MENU_CONTEXT_MENU(MenuId,ContextMenuId)
			select @Id,t.Id from temp t
			where t.Id not in(
				select t1.ContextMenuId from dbo.SYS_MENU_CONTEXT_MENU t1
				where t1.MenuId=@Id and t.Id=t1.ContextMenuId)
			
			/*** Cập nhật lại rl ***/
			update SYS_ROOT_DIRECTORY set url=null
			where Id=@ParentId
			
			set @OutNewName=@Name
		End
		Else
		Begin
			/*** Update ***/
			update dbo.SYS_ROOT_DIRECTORY set Name=@Name
			
			where Id=@Id
											
		End
	End /***End action POST ***/
	Else if(@Action='DELETE')
	Begin
		;WITH temp(Id, Name,ParentId, Node)
				as (
						Select l1.Id,l1.Name,l1.ParentId, 0 as Node
						From dbo.SYS_ROOT_DIRECTORY l1
						Where l1.Id=@Id
						Union All
						Select b.Id,b.Name,b.ParentId, a.Node + 1
						From temp as a, dbo.SYS_ROOT_DIRECTORY as b
						Where a.Id = b.ParentId
				)
		DELETE FROM SYS_ROOT_DIRECTORY
		WHERE Id IN (SELECT Id FROM temp)
		
		delete from dbo.SYS_MENU_CONTEXT_MENU where MenuId=@Id
	End /*** End Action DELETE***/
	
COMMIT

GO


