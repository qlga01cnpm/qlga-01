
GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_EventLogWrite]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EventLogWrite]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vi-Hoang>
-- Create date: <18-07-2014>
-- Description:	<Logs>
-- =============================================
CREATE PROCEDURE [dbo].[sp_EventLogWrite] 
@TypeSysLog as dbo.TypeSysLog readonly
with encryption
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	if(select COUNT(*) from @TypeSysLog)>0
	Begin
		Insert into tbl_Sys_Logs(MChine,AccountWin,UserID,UserName,Created,Module,Action,Action_Name,IPLan,IPWan,Mac,DeviceName,Description,JsonObject)
		select MChine,AccountWin,UserID,UserName,GETDATE(),Module,Action,Action_Name,IPLan,IPWan,Mac,DeviceName,Description,JsonObject
		from @TypeSysLog newValue
		where newValue.SYS_ID not in
			(select SYS_ID from tbl_Sys_Logs lg where lg.SYS_ID=newValue.SYS_ID)
	End
		
END

GO


