GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_CONTEXT_MENU]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_CONTEXT_MENU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_CONTEXT_MENU] 
@Action varchar(100),
@MenuId varchar(100),
@ContextMenuId varchar(100),

@UserId varchar(50),
@MainMenuId varchar(100)
WITH ENCRYPTION
AS
BEGIN
	------SET NOCOUNT ON;
	------SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	-- sp_SYS_CONTEXT_MENU'GET','A0000327MNU','ied.menu.mahang.file','quanly','A0000327MNU'
	-- sp_SYS_CONTEXT_MENU'GET','SH.PKH','sys.menu.dep.folder','admin','05.01.01'
	-- sp_SYS_CONTEXT_MENU'GET','09.01.01','ied.menu.mahang.folder','quanly','09.01.01'
	-- sp_SYS_CONTEXT_MENU'GET','fd9724d4-d26a-d0b4-0ac0-eb988d8494a9','ied.menu.mahang.file','quanly','fd9724d4-d26a-d0b4-0ac0-eb988d8494a9'
	If(@Action='GET')
	Begin
		declare @AllowAdd bit,@AllowEdit bit,@AlowDelete bit
		/*** Cách 01 ***/
		--select @AllowAdd=AllowAdd^1,@AllowEdit=AllowEdit^1,@AlowDelete=AlowDelete^1
		--From SYS_USER_MENU with(nolock) where UserID=@UserId and MenuID=@MainMenuId
		
		/*** Cách 2 Lấy quyền của cao nhất được cấp cho danh mục ***/
		;WITH parent AS
		(
			SELECT MenuId,MenuParent,IsFunc from SYS_MENU_MASTER WHERE MenuId = @MainMenuId
			UNION ALL 
			SELECT t.MenuId,t.MenuParent,t.IsFunc FROM parent p
			INNER JOIN SYS_MENU_MASTER t ON t.MenuId = p.MenuParent
		)
		SELECT @AllowAdd= MAX(CONVERT(int,AllowAdd))^1,@AllowEdit= MAX(CONVERT(int,AllowEdit))^1,@AlowDelete= MAX(CONVERT(int,AlowDelete))^1
		FROM  parent p inner join SYS_USER_MENU t on p.MenuId=t.MenuID 
		
		/*** NẾU TRƯỜNG HỢP USER ĐÓ KHÔNG CÓ QUYỀN NÀO THÌ KHÓA***/
		if(@AllowAdd is null)set @AllowAdd=1	--Truog hop null thì khóa hết
		if(@AllowEdit is null)set @AllowEdit=1	--Trường hợp null thì khóa hết
		if(@AlowDelete is null) set @AlowDelete=1	--Trường hợp null thì khóa hết
		/*** NẾU USER ĐÓ LÀ 'admin' hoặc 'user' CHO PHÉP TOÀN QUYỀN ***/
		if(@UserId='admin' or @UserId='user')
		Begin
			set @AllowAdd=0 set @AllowEdit=0 set @AlowDelete=0
		End
		
		;WITH temp(Id, text,ParentId,iconCls,Sort,href,onclick, Node)
						as (
								Select l1.Id,l1.text,l1.ParentId,l1.iconCls,l1.Sort,l1.href,l1.onclick, 0 as Node
								From dbo.SYS_CONTEXT_MENU l1 with(nolock)
								Where l1.Id=@ContextMenuId
								Union All
								Select b.Id,b.text,b.ParentId,b.iconCls,b.Sort,b.href,b.onclick, a.Node + 1
								From temp as a, dbo.SYS_CONTEXT_MENU as b with(nolock)
								Where a.Id = b.ParentId
						)
		select t1.*,case when t2.disabled =1 then t2.disabled 
			  when t2.disabled=0 and (CHARINDEX('.new',t1.Id)>0 or CHARINDEX('.create',t1.Id)>0
										--or CHARINDEX('.rename',t1.Id)>0
										 ) 
				    then @AllowAdd
			  
			  when t2.disabled=0 and (CHARINDEX('.edit',t1.Id)>0 or CHARINDEX('.rename',t1.Id)>0) then @AllowEdit
			  when t2.disabled=0 and CHARINDEX('.delete',t1.Id)>0 then @AlowDelete
			  else t2.disabled end as disabled
		From temp t1
		inner join dbo.SYS_MENU_CONTEXT_MENU t2 with(nolock)
		on t1.Id=t2.ContextMenuId
		where ParentId is not null and ParentId<>'' and t2.MenuId=@MenuId
		order by Sort asc
	End /***End action POST ***/
	
	
END

GO


