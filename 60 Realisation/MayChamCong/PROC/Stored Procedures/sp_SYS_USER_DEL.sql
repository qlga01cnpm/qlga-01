GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_USER_DEL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_USER_DEL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_USER_DEL] 
@UserId varchar(50),
@TypeSysLog as dbo.TypeSysLog readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		/*** Write Log ***/
		exec sp_EventLogWrite @TypeSysLog
	
		if(@UserId='admin' or @UserId='user')
		Begin
			raiserror(N'Tài khoản admin mặc định, không thể xóa!',16,1)
			commit
			return
		End
		
		delete from dbo.SYS_USER_MENU where UserID=@UserId
		delete from dbo.SYS_USER_MENU where UserID=@UserId
		delete from dbo.SYS_USERS where UserID=@UserId
		
		
COMMIT

GO


