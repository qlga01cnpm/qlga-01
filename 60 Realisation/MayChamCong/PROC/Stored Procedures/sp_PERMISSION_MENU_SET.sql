GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_PERMISSION_MENU_SET]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PERMISSION_MENU_SET]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PERMISSION_MENU_SET] 
@UserId varchar(50),
@LastName nvarchar(30),
@FirstName nvarchar(15),
@IsAdmin bit,
@IsNewRow bit,
@TYPE_SYS_USER_MENU as dbo.TYPE_SYS_USER_MENU readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	/*** CẬP NHẬT LẠI THÔNG TIN NGƯỜI DÙNG ***/
		declare @Count as int
		
		if(@IsNewRow=1)
		Begin
			select @Count=COUNT(UserID) from dbo.SYS_USERS where UserID=@UserId
			if(@Count>0)
			Begin
				ROLLBACK
				--commit
				raiserror(N'Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác!',16,1)
				return
			End
		End
		
		if(@UserId='' or @UserId='admin' or @UserId='user' )
		Begin
			commit
			return
		End
		
		select @Count=COUNT(UserID) from dbo.SYS_USERS where UserID=@UserId
		if(@Count>0)
			update dbo.SYS_USERS set LastName=@LastName,FirstName=@FirstName,IsAdmin=@IsAdmin
			where UserID=@UserId
		else
			insert into dbo.SYS_USERS(UserID,LastName,FirstName,IsAdmin)
			values(@UserId,@LastName,@FirstName,@IsAdmin)
		
		/*** XÓA QUYỀN ***/
		delete from dbo.SYS_USER_MENU where UserID=@UserId
		/*** CẤP LẠI QUYỀN ***/
		insert into dbo.SYS_USER_MENU(UserID,MenuID,AllowView,AllowAdd,AllowEdit,AlowDelete)
		select @UserId,n.MenuID,n.AllowView,n.AllowAdd,n.AllowEdit,n.AlowDelete
		from @TYPE_SYS_USER_MENU n
COMMIT

GO


