GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_APP_UPDATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_APP_UPDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_APP_UPDATE] 
@Id int,
@Name nvarchar(500)=null,
@Address nvarchar(500)=null,
@Tel varchar(50)=null,
@Fax varchar(50)=null,
@Website nvarchar(50)=null,
@Email varchar(50)=null,
@MaSoThue varchar(20)=null,
@Logo image=null,
@IsAutoBackup bit=null,
@Url nvarchar(max)=null,
@IsVirtualData bit=null,
@IsRunMCC bit=null

WITH ENCRYPTION
AS
BEGIN TRANSACTION
	 
	Update dbo.SYS_APP set Name=@Name,
						   Address=@Address,
						   Tel=@Tel,
						   Fax=@Fax,
						   Website=@Website,
						   Email=@Email,
						   MaSoThue=@MaSoThue,
						   Logo=@Logo,
						   IsAutoBackup=@IsAutoBackup,
						   Url=@Url,
						   IsVirtualData=@IsVirtualData,
						   IsRunMCC=@IsRunMCC
	where Id=@Id
		
COMMIT

GO


