GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_PERMISSION_MENU_BY_USER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PERMISSION_MENU_BY_USER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PERMISSION_MENU_BY_USER] 
@MenuId varchar(100),
@UserId varchar(50)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	------SET NOCOUNT ON;
	------SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
		-- sp_PERMISSION_MENU_BY_USER '05.01.02','quanly'
		-- sp_PERMISSION_MENU_BY_USER 'A0000327MNU','quanly'
		declare @ParentId varchar(100),@Url nvarchar(max)
		select @ParentId=MenuParent,@Url=Url From  dbo.SYS_MENU_MASTER
		where MenuId=@MenuId
		if(@Url='/IED/Library' or @Url='/IED/Step' or @Url='/IED/Analysis')
		Begin
			/*** Lấy quyền của cao nhất được cấp cho danh mục ***/
			;WITH parent AS
			(
				SELECT MenuId,MenuParent,IsFunc from SYS_MENU_MASTER WHERE MenuId = @MenuId
				UNION ALL 
				SELECT t.MenuId,t.MenuParent,t.IsFunc FROM parent p
				INNER JOIN SYS_MENU_MASTER t ON t.MenuId = p.MenuParent
			)
			SELECT @UserId as UserID,@MenuId as MenuID,MAX(CONVERT(int,AllowView)) as AllowView,
			MAX(CONVERT(int,AllowAdd)) as AllowAdd, MAX(CONVERT(int,AllowEdit)) as AllowEdit,MAX(CONVERT(int,AlowDelete)) as AlowDelete
			FROM  parent p inner join SYS_USER_MENU t on p.MenuId=t.MenuID 
			
			--select * from SYS_USER_MENU t1
			--where t1.UserID=@UserId and t1.MenuID=@ParentId
		End
		Else
		Begin
			--select '1'
			select * from dbo.SYS_USER_MENU 
			where UserID=@UserId and MenuID=@MenuId
		End	

COMMIT

GO


