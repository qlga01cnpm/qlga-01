GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_PERMISSION_GROUP_USER_MENU]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PERMISSION_GROUP_USER_MENU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PERMISSION_GROUP_USER_MENU] 
@GroupID varchar(20),
@GroupName nvarchar(250),
@Descriptions nvarchar(500),
@TYPE_SYS_USER_GROUP as dbo.TYPE_SYS_USER_GROUP readonly,
@TYPE_SYS_USER_MENU as dbo.TYPE_SYS_USER_MENU readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	/*** CẬP NHẬT LẠI THÔNG TIN NHÓM ***/
		if(@GroupID='')
		Begin
			commit
			return
		End
		
		if(@GroupID<>'Administrator' and @GroupID<>'SYSTEM' and @GroupID<>'USERS')
		Begin
			declare @Count as int
			select @Count=COUNT(GroupID) from dbo.SYS_GROUP where GroupID=@GroupID
			if(@Count>0)
				update dbo.SYS_GROUP set GroupName=@GroupName,Descriptions=@Descriptions
				where GroupID=@GroupID
			else 
				insert into dbo.SYS_GROUP(GroupID,GroupName,Descriptions)
				values(UPPER(@GroupID),@GroupName,@Descriptions)
		End
		
		/*** XÓA THÊM PHÂN NHÓM NGƯỜI DÙNG ***/
		delete from dbo.SYS_USER_GROUP where GroupID=@GroupID
		insert into dbo.SYS_USER_GROUP(GroupID,UserID)
		select @GroupID,UserID from @TYPE_SYS_USER_GROUP
		
		/*** CẤP LẠI QUYỀN ***/
		--insert into dbo.SYS_USER_MENU(UserID,MenuID,AllowView,AllowAdd,AllowEdit,AlowDelete)
		--select @UserId,n.MenuID,n.AllowView,n.AllowAdd,n.AllowEdit,n.AlowDelete
		--from @TYPE_SYS_USER_MENU n
COMMIT

GO


