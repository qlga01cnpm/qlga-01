GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MenuMasterRename]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MenuMasterRename]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MenuMasterRename] 
@Action varchar(100),
@MenuId varchar(100),
@MenuName nvarchar(255),
@Url nvarchar(max)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	declare @Count int
	If(@Action='PUT')
	Begin
		/*** Update ***/
		update dbo.SYS_MENU_MASTER set	MenuName=@MenuName
		where MenuId=@MenuId
		
		If(@Url='/IED/Analysis')
		Begin
			set @Action=''
		End
		Else If(@Url='/IED/Step')
		Begin
			set @Action=''
		End
		Else If(@Url='/IED/Library')
		Begin
			select @Count=COUNT(MenuId) from dbo.DIC_THUVIEN where MenuId=@MenuId
			if(@Count=0)
				Insert into dbo.DIC_THUVIEN(MaThuVien,MoTa,MenuId)
					values(@MenuName,'',@MenuId)
			else
				Update dbo.DIC_THUVIEN set MaThuVien=@MenuName
				where MenuId=@MenuId
		End
		
	End /***End action POST ***/
	Else if(@Action='DELETE__')
	Begin
		select '0'
	End /*** End Action DELETE***/
	
COMMIT

GO


