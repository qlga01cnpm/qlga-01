GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_CONGDOAN_GET_BY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_CONGDOAN_GET_BY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_CONGDOAN_GET_BY] 
@Action varchar(100),
@MenuId varchar(100)
WITH ENCRYPTION
AS
BEGIN 
		--exec sp_DIC_CONGDOAN_GET_BY 'get','A0000326MNU'
		If(@Action='GET')
		Begin /***  ***/
			declare @Id_Dm varchar(20)
			--Lấy Id_Dm công đoạn
			Select top 1 @Id_Dm=Id_Dm From dbo.IED_CONGDOAN where MenuId=@MenuId
			
			Select * from dbo.IED_CONGDOAN where Id_Dm=@Id_Dm
			/*** Lấy thao tác theo Danh mục công đoạn ***/
			Select t1.*
			,case when t2.ChieuDaiDuongMay is null then 0 else t2.ChieuDaiDuongMay end ChieuDaiDuongMay
			From dbo.IED_THAOTACCONGDOAN t1
			left join dbo.DIC_THUVIEN t2 on t1.MaSo=t2.MaThuVien
			where Id_DmCd=@Id_Dm
			
			/*** Lấy thiết bị ***/
			Select top 1 t2.* from dbo.IED_CONGDOAN t1
			inner join dbo.DIC_THIETBI t2 on t1.MaThietBi=t2.MaThietBi
			where Id_Dm=@Id_Dm
			
			/*** Lấy Thao thao tác sản phẩm***/
			Select top 1 t2.* from dbo.IED_CONGDOAN t1
			inner join dbo.DIC_THAOTACSANPHAM t2 on t1.MaThaoTacSp=t2.MaThaoTacSP
			where Id_Dm=@Id_Dm
		End
		--Else if(@Action='DELETE')
		--Begin
		--	/*** XÓA ***/
		--End 
		
END

GO


