GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_USER_CHANGE_PASSWORD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_USER_CHANGE_PASSWORD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_USER_CHANGE_PASSWORD] 
@UserId varchar(50),
@Password varchar(40),
@NewPassword varchar(40),
@RePassword varchar(40)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	
		if(@UserId='' or @UserId='admin')
		Begin
			commit
			return
		End
		if(select COUNT(UserID) from dbo.SYS_USERS where UserID=@UserId and Password=@Password)=0
		Begin
			raiserror(N'Mật khẩu hiện tại không đúng. Vui lòng nhập lại',16,1)
			commit
			return
		End
		
		if(@NewPassword<>@RePassword)
		Begin
			raiserror(N'Nhập lại mật khẩu không trùng khớp',16,1)
			commit
			return
		End
		
		
		update dbo.SYS_USERS set Password=@NewPassword
			where UserID=@UserId
COMMIT

GO


