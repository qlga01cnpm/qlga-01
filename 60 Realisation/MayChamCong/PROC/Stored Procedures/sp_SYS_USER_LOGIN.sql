GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_USER_LOGIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_USER_LOGIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_USER_LOGIN] 
@UserId varchar(50)
AS
BEGIN 
--TRANSACTION
	--SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	update dbo.SYS_USERS set LastView=GETDATE(),
							 CountView=CountView+1
	where UserID=@UserId
	
	select * From SYS_USERS with(nolock) where UserID=@UserId
	
--COMMIT
END
GO


