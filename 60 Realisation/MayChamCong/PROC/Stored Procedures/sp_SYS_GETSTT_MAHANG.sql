GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_GETSTT_MAHANG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_GETSTT_MAHANG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_GETSTT_MAHANG] 
@ChuoiBatDau varchar(10),
@Separator char(1),
@valRef varchar(20) OUTPUT
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	Declare @Stt_rec_max  int,@SoChuSo int
	Declare @StrFortmat Char(7) 

	-- Set default
	Set @StrFortmat = '0000000000000000000000000000000000000000'	
	
	--set @ChuoiBatDau='JKSD'
	set @Separator='-'
	set @SoChuSo=4
	set @Stt_rec_max=1
	
	Set @valRef = UPPER(@ChuoiBatDau)+ @Separator+ SubString(@StrFortmat, 1, @SoChuSo - Len(@Stt_rec_max+1)) + Convert(nvarchar(7), @Stt_rec_max+1)
	Set @valRef = RTrim(@valRef)
	-- End process

	Select @valRef
COMMIT

GO


