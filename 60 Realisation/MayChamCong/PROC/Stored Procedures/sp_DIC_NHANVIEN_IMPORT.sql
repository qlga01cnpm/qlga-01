GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_IMPORT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_IMPORT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Store Import Nhân viên từ file excel>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_IMPORT] 
@TYPE_NHANVIEN as dbo.TYPE_DIC_NHANVIEN readonly
WITH ENCRYPTION
AS
BEGIN
	Merge into dbo.DIC_NHANVIEN t1 
	using @TYPE_NHANVIEN t2 on t1.MaNV=t2.MaNV
	when not matched
		then insert(MaNV,TenNV,MaCC,CardID,BirthDate,BirthPlace,NguyenQuan,Sex,Address,BeginningDate,NgayKyHopDong,
				CMND,NoiCap,PositionName,NgayQuayLai,TheBHXH,IsVisible)
		values(t2.MaNV,t2.TenNV,t2.MaCC,t2.CardID,t2.BirthDate,t2.BirthPlace,t2.NguyenQuan,
			   t2.Sex,t2.Address,t2.BeginningDate,t2.NgayKyHopDong,
			   t2.CMND,t2.NoiCap,t2.PositionName,t2.NgayQuayLai,t2.TheBHXH,0)
	when matched
		then update set TenNV=t2.TenNV
					   ,MaCC=t2.MaCC
					   ,CardID=t2.CardID
					   ,BirthDate=t2.BirthDate
					   ,BirthPlace=t2.BirthPlace
					   ,NguyenQuan=t2.NguyenQuan
					   ,Sex=t2.Sex
					   ,Address=t2.Address
					   ,BeginningDate=t2.BeginningDate
					   ,NgayKyHopDong=t2.NgayKyHopDong
					   ,CMND=t2.CMND
					   ,NoiCap=t2.NoiCap
					   ,PositionName=t2.PositionName
					   ,NgayQuayLai=t2.NgayQuayLai
					   ,TheBHXH=t2.TheBHXH
	;
	
	/*** Cập nhật phòng ban ***/
	insert into dbo.DIC_CHUYENBOPHAN(MaNV,DepID,ChangedDate)
	select t1.Id,'DEP.NEW',GETDATE()
	from dbo.DIC_NHANVIEN t1 inner join @TYPE_NHANVIEN t2
	on t1.MaNV=t2.MaNV
	where t1.Id not in(select CONVERT(bigint, t3.MaNV) from dbo.DIC_CHUYENBOPHAN t3)
	
END

GO


