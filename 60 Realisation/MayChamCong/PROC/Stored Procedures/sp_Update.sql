GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update]
@SetValues nvarchar(max),
@TableName nvarchar(100),
@WhereClause nvarchar(max)
WITH ENCRYPTION
AS
BEGIN
	
	if(ISNULL(@WhereClause,'')<>'')
	Begin
		--Kiem tra trong truong hop WhereClause='Where Id=1;Delete from tbl_Name' (hack)
		if(CHARINDEX('Delete',@WhereClause)>0 or CHARINDEX('drop',@WhereClause)>0 or
		   CHARINDEX('Alter',@WhereClause)>0 or CHARINDEX('Deny',@WhereClause)>0 or
		   CHARINDEX('Revoke',@WhereClause)>0 or CHARINDEX('Grant',@WhereClause)>0 or
		   CHARINDEX('Insert',@WhereClause)>0)
		Begin
			raiserror(N'Where Clause is error...',16,1)
			Return
		End
	End
	
	declare @query nvarchar(max)
	set @query=N'Update '+@TableName+' set '+@SetValues+' '+@WhereClause
	
	exec sp_executesql @query
END

