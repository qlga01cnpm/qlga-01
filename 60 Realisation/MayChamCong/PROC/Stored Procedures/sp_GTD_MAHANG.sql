GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_GTD_MAHANG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GTD_MAHANG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GTD_MAHANG] 
@Action varchar(100),

@Id_Dm varchar(20),
@MenuId varchar(100),
@MaSo varchar(25),
@KhachHang nvarchar(150),
@TenMaHang nvarchar(150),
@SoPhienBan bigint,
@TrangThai int,
@DangKhoa bit,
@MoTa nvarchar(500),
@MaTienTe char(10), --(10)

@TiGia float,
@ChiPhiKhauCat float,
@ChiPhiThietBi float,
@ChiPhiKiemTra float,
@ChiPhiKhauUi float,
@ChiPhiKhauDongGoi float,
@ChiPhiGiaCongNgoai float,
@DonGiaGiaCongNgoai float,
@ChiPhiSanXuatTrucTiep float,
@ChiPhiQuanLyTrucTiep float, --(20)

@ChiPhiVai float,
@ChiPhiPhuLieu float,
@ChiPhiChi float,
@ChiPhiPhuKien float,
@ChiPhiBaoBi float,
@ChiPhiGoc float,
@ChiPhiBanHang float,
@ChiPhiHanNgach float,
@MaChuyenTo float, 
@DinhMucNangSuat float, --(30)

@TongSoCongNhan float,
@ChiPhiMaHang float,
@TongThoiGian float,
@NguoiTao nvarchar(50),
@NguoiCapNhat nvarchar(50),	--(35)

@TypeThaoTac as dbo.TYPE_GTD_THAOTACMAHANG readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		
		If(@Action='POST')
		Begin /***  ***/
			declare @Count int
			if(select COUNT(*) from dbo.GTD_MAHANG where Id_Dm=@Id_Dm)=0
			Begin /*** THEM MOI ***/
				/*** Tạo Id_Dm mới ***/
				exec sp_SYS_GETSTT_RECDM '','GTD_MAHANG',@Id_Dm out
				
				Insert into dbo.GTD_MAHANG(Id_Dm,MenuId,MaSo,KhachHang,TenMaHang,SoPhienBan,TrangThai,DangKhoa,MoTa,MaTienTe,
							TiGia,ChiPhiKhauCat,ChiPhiThietBi,ChiPhiKiemTra,ChiPhiKhauUi,ChiPhiKhauDongGoi,ChiPhiGiaCongNgoai,DonGiaGiaCongNgoai,ChiPhiSanXuatTrucTiep,ChiPhiQuanLyTrucTiep,
							ChiPhiVai,ChiPhiPhuLieu,ChiPhiChi,ChiPhiPhuKien,ChiPhiBaoBi,ChiPhiGoc,ChiPhiBanHang,ChiPhiHanNgach,MaChuyenTo,DinhMucNangSuat,
							TongSoCongNhan,ChiPhiMaHang,TongThoiGian,NguoiTao,NguoiCapNhat,
							NgayTao,NgayCapNhat)
				values(@Id_Dm,@MenuId,@MaSo,@KhachHang,@TenMaHang,@SoPhienBan,@TrangThai,@DangKhoa,@MoTa,@MaTienTe,
				@TiGia,@ChiPhiKhauCat,@ChiPhiThietBi,@ChiPhiKiemTra,@ChiPhiKhauUi,@ChiPhiKhauDongGoi,@ChiPhiGiaCongNgoai,@DonGiaGiaCongNgoai,@ChiPhiSanXuatTrucTiep,@ChiPhiQuanLyTrucTiep,
				@ChiPhiVai,@ChiPhiPhuLieu,@ChiPhiChi,@ChiPhiPhuKien,@ChiPhiBaoBi,@ChiPhiGoc,@ChiPhiBanHang,@ChiPhiHanNgach,@MaChuyenTo,@DinhMucNangSuat,
				@TongSoCongNhan,@ChiPhiMaHang,@TongThoiGian,@NguoiTao,@NguoiCapNhat,
				GETDATE(),GETDATE())
			End 
			Else 
			Begin /*** CAP NHAT ***/
				update GTD_MAHANG set MaSo=@MaSo,KhachHang=@KhachHang,TenMaHang=@TenMaHang,MoTa=@MoTa,MaTienTe=@MaTienTe,
				TiGia=@TiGia,ChiPhiKhauCat=@ChiPhiKhauCat,ChiPhiThietBi=@ChiPhiThietBi,ChiPhiKiemTra=@ChiPhiKiemTra,ChiPhiKhauUi=@ChiPhiKhauUi,
				ChiPhiKhauDongGoi=@ChiPhiKhauDongGoi,ChiPhiGiaCongNgoai=@ChiPhiGiaCongNgoai,DonGiaGiaCongNgoai=@DonGiaGiaCongNgoai,ChiPhiSanXuatTrucTiep=@ChiPhiSanXuatTrucTiep,ChiPhiQuanLyTrucTiep=@ChiPhiQuanLyTrucTiep
				
				,ChiPhiVai=@ChiPhiVai,ChiPhiPhuLieu=@ChiPhiPhuLieu,ChiPhiChi=@ChiPhiChi,ChiPhiPhuKien=@ChiPhiPhuKien,ChiPhiBaoBi=@ChiPhiBaoBi
				,ChiPhiBanHang=@ChiPhiBanHang,ChiPhiHanNgach=@ChiPhiHanNgach
				
				where Id_Dm=@Id_Dm
			End
			
			/*** CẬP NHẬT BẢNG THAO TÁC ***/
			Delete from dbo.GTD_THAOTACMAHANG where Id_DmMaHang=@Id_Dm
			
			Insert into dbo.GTD_THAOTACMAHANG(Id_DmMaHang,Id_DmCD,ThuTu,SoLanXuatHien)
			select @Id_Dm,Id_DmCD,ThuTu,SoLanXuatHien
			from @TypeThaoTac t1
			
		End
		Else if(@Action='DELETE')
		Begin
			/*** XÓA ***/
			Delete from dbo.GTD_THAOTACMAHANG where Id_DmMaHang=@Id_Dm
			Delete from dbo.GTD_MAHANG where Id_Dm=@Id_Dm
		End 
		
COMMIT

GO


