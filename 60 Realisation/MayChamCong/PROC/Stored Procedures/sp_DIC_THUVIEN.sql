GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_THUVIEN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_THUVIEN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_THUVIEN] 
@Action varchar(100),
@MaThuVien varchar(15),
@MoTa nvarchar(150),
@GhiChu nvarchar(200),
@KemThietBi bit,
@ChieuDaiDuongMay float,
@TMU float,
@DuocTaoBoiHeThong bit,
@MenuId varchar(100)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		
		If(@Action='PUT')
		Begin /*** Cap nhat ***/
			/*** CẬP NHẬT TRONG TRƯỜNG HỢP MÃ ĐÃ CÓ ***/
			Update dbo.DIC_THUVIEN set MoTa=@MoTa,GhiChu=@GhiChu,KemThietBi=@KemThietBi,
									   ChieuDaiDuongMay=@ChieuDaiDuongMay,TMU=@TMU,
									   DuocTaoBoiHeThong=@DuocTaoBoiHeThong
			where MaThuVien=@MaThuVien and MenuId=@MenuId
		End
		Else
		If(@Action='POST')
		Begin /*** Tao moi ***/
			declare @Count int
			if(select COUNT(*) from dbo.DIC_THUVIEN where MaThuVien=@MaThuVien)>0
			Begin
				raiserror(N'Mã thư viện đã tồn tại. Vui lòng nhập mã khác!',16,1)
				return
			End
			
			Insert into dbo.DIC_THUVIEN(MaThuVien,MoTa,GhiChu,KemThietBi,ChieuDaiDuongMay,TMU,DuocTaoBoiHeThong,MenuId)
			values(@MaThuVien,@MoTa,@GhiChu,@KemThietBi,@ChieuDaiDuongMay,@TMU,@DuocTaoBoiHeThong,@MenuId)
			
		End
		Else if(@Action='DELETE')
		Begin
			/*** XÓA ***/
			select 'dd'
		End 
		
COMMIT

GO


