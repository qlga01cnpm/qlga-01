GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_DEPARTMENT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_DEPARTMENT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_DEPARTMENT] 
@Action varchar(100),
@DepID varchar(100),
@DepName nvarchar(255),
@ParentID varchar(100),
@ContextMenuId varchar(100),
@ContextMenuIdFile varchar(100),
@IsChain bit,
@IsChainSew bit,
@IsChainCut bit,
@IsChainPackage bit,
@IsWarehouse bit,
@Sort int
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	declare @Count int
	
	If(@Action='POST')
	Begin
		select @Count=COUNT(DepID) from dbo.SYS_DEPARTMENTS where DepID=@DepID
		if(@Count=0)
		Begin
			select @Sort=MAX(Sort) from dbo.SYS_MENU_MASTER where MenuParent=@ParentID
			if(@Sort is null)
			Begin
				set @Sort=0
			End
			
			/*** Insert ***/
			insert into dbo.SYS_DEPARTMENTS(DepID,DepName,ParentID,ContextMenuId,ContextMenuIdFile,IsChain,IsChainSew,IsChainCut,IsChainPackage,IsWarehouse,Sort)
			values(@DepID,@DepName,@ParentID,@ContextMenuId,@ContextMenuIdFile,@IsChain,@IsChainSew,@IsChainCut,@IsChainPackage,@IsWarehouse, @Sort+1)
			
			/***  ***/
			;WITH temp(Id, text,ParentId,iconCls,Sort,href,onclick, Node)
						as (
								Select l1.Id,l1.text,l1.ParentId,l1.iconCls,l1.Sort,l1.href,l1.onclick, 0 as Node
								From dbo.SYS_CONTEXT_MENU l1
								Where l1.Id=@ContextMenuId
								Union All
								Select b.Id,b.text,b.ParentId,b.iconCls,b.Sort,b.href,b.onclick, a.Node + 1
								From temp as a, dbo.SYS_CONTEXT_MENU as b
								Where a.Id = b.ParentId
			)
			insert into dbo.SYS_MENU_CONTEXT_MENU(MenuId,ContextMenuId)
			select @DepID,t.Id from temp t
			where t.Id not in(
				select t1.ContextMenuId from dbo.SYS_MENU_CONTEXT_MENU t1
				where t1.MenuId=@DepID and t.Id=t1.ContextMenuId)
			
		End
		Else
		Begin
			/*** Update ***/
			update dbo.SYS_DEPARTMENTS set	DepName=@DepName,IsChain=@IsChain,
											IsChainSew=@IsChainSew,IsChainCut=@IsChainCut,
											IsChainPackage=@IsChainPackage,IsWarehouse=@IsWarehouse
											,Sort=@Sort
			where DepID=@DepID
											
		End
	End /***End action POST ***/
	Else if(@Action='DELETE')
	Begin
		;WITH temp(DepID,DepName,ParentID, Node)
				as (
						Select l1.DepID,l1.DepName,l1.ParentID, 0 as Node
						From dbo.SYS_DEPARTMENTS l1
						Where l1.DepID=@DepID
						Union All
						Select b.DepID,b.DepName,b.ParentID, a.Node + 1
						From temp as a, dbo.SYS_DEPARTMENTS as b
						Where a.DepID = b.ParentID
				)
		DELETE FROM SYS_DEPARTMENTS
		--UPDATE dbo.SYS_MENU_MASTER set IsVisible=1
		WHERE DepID IN (SELECT DepID FROM temp)
		
		delete from dbo.SYS_MENU_CONTEXT_MENU where MenuId=@DepID
	End /*** End Action DELETE***/
	
COMMIT

GO


