GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_CONGDOAN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_CONGDOAN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_CONGDOAN] 
@Action varchar(100),
@Id_Dm varchar(20),
@MenuId varchar(100),
@MaSo nvarchar(150),
@TenCongDoan nvarchar(150),
@SoPhienBan bigint,
@TrangThai int,
@DangKhoa bit,
@MoTa nvarchar(500),
@MaBoPhan varchar(100), --(10)

@LucTacDung int,
@MaBacTho varchar(10),
@MaThietBi varchar(10),
@MaThaoTacSp varchar(10),
@HaoPhiDacBiet float,
@MoTaNoiLamViec nvarchar(500),
@ChiPhiCongDoan float,
@TongThoiGian float,
@NguoiTao varchar(50),
@NguoiCapNhat varchar(50), --(20)

@ThoiGianChuan float,

@TypeThaoTac as dbo.TYPE_IED_THAOTACCONGDOAN readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		
		If(@Action='POST')
		Begin /***  ***/
			declare @Count int
			if(select COUNT(*) from dbo.IED_CONGDOAN where Id_Dm=@Id_Dm)=0
			Begin /*** THEM MOI ***/
				/*** Tạo Id_Dm mới ***/
				exec sp_SYS_GETSTT_RECDM '','IED_CONGDOAN',@Id_Dm out
				
				Insert into dbo.IED_CONGDOAN(Id_Dm,MenuId,MaSo,TenCongDoan,
								MoTa,MaBoPhan,LucTacDung,MaBacTho,MaThietBi,MaThaoTacSp,HaoPhiDacBiet,ChiPhiCongDoan,
								TongThoiGian,NguoiTao,NgayTao,NguoiCapNhat,NgayCapNhat,ThoiGianChuan)
				values(@Id_Dm,@MenuId,@MaSo,@TenCongDoan,@MoTa,@MaBoPhan,@LucTacDung,@MaBacTho,@MaThietBi,@MaThaoTacSp,@HaoPhiDacBiet,@ChiPhiCongDoan,
						@TongThoiGian,@NguoiTao,GETDATE(),@NguoiCapNhat,GETDATE(),@ThoiGianChuan)
			End 
			Else 
			Begin /*** CAP NHAT ***/
				update IED_CONGDOAN set MaSo=@MaSo,TenCongDoan=@TenCongDoan,MoTa=@MoTa,MaBoPhan=@MaBoPhan,LucTacDung=@LucTacDung,
										MaBacTho=@MaBacTho,MaThietBi=@MaThietBi,MaThaoTacSp=@MaThaoTacSp,HaoPhiDacBiet=@HaoPhiDacBiet,
										ChiPhiCongDoan=@ChiPhiCongDoan,TongThoiGian=@TongThoiGian,NguoiCapNhat=@NguoiCapNhat,
										NgayCapNhat=GETDATE(),ThoiGianChuan=@ThoiGianChuan
				where Id_Dm=@Id_Dm
			End
			
			/*** CẬP NHẬT BẢNG THAO TÁC ***/
			Delete from IED_THAOTACCONGDOAN where Id_DmCd=@Id_Dm
			
			Insert into dbo.IED_THAOTACCONGDOAN(Id_DmCd,MaSo,ThuTu,SoLanXuatHien,MoTa,LoaiMaSo,TMUThaoTac,TMUThietBi)
			select @Id_Dm,MaSo,ThuTu,SoLanXuatHien,MoTa,LoaiMaSo,TMUThaoTac,TMUThietBi
			from @TypeThaoTac t1
			--Where t1.ThaoTacCongDoanID
			--Not In (Select t2.ThaoTacCongDoanID FROM dbo.IED_THAOTACCONGDOAN t2 
			--		where t2.ThaoTacCongDoanID=t1.ThaoTacCongDoanID)
			
		End
		--Else if(@Action='DELETE')
		--Begin
		--	/*** XÓA ***/
		--End 
		
COMMIT

GO


