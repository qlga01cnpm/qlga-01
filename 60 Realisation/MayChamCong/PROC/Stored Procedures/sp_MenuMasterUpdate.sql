GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_MenuMasterUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MenuMasterUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MenuMasterUpdate] 
@Action varchar(100),
@MenuId varchar(100),
@MenuName nvarchar(255),
@MenuParent varchar(100),
@ContextMenuId varchar(100),
@ContextMenuIdFile varchar(100),
@Url nvarchar(max),
@DefaultUrl nvarchar(max),
@PageName varchar(50),
@Sort int,
@MenuType varchar(10),
@OutNewName nvarchar(150) output
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	--declare @out nvarchar(150)
	--exec dbo.sp_MenuMasterUpdate 'POST','A0000282MNU','New File','A0000269MNU','ied.menu.mahang.file','','/IED/Analysis','','',0,@out out
	declare @Count int
	set @OutNewName=''
	If(@Action='POST')
	Begin
		select @Count=COUNT(MenuId) from dbo.SYS_MENU_MASTER where MenuId=@MenuId
		declare @Stt_rec_max int
		if(@Count=0)
		Begin
			declare @ChuoiBatDau varchar(10),@KyTuCach char(1),@StrFortmat Char(7),@SoChuSo int
			set @StrFortmat='0000000000000000000000000'
			select @Sort=MAX(Sort) from dbo.SYS_MENU_MASTER where MenuParent=@MenuParent
			if(@Sort is null)set @Sort=0
			/*** Kiểm tra nếu là danh mục phân tích công đoạn ***/
			if(@Url is not null and @Url<>'' and @Url='/IED/Step')
			Begin
				if(@SoChuSo=0)set @SoChuSo=3
				select @ChuoiBatDau=ChuoiBatDau,@KyTuCach=KyTuCach,@Stt_rec_max=Stt_RecDm,@SoChuSo=SoChuSo from dbo.SYS_MENU_MASTER where MenuId=@MenuParent
				set @MenuName=@ChuoiBatDau+@KyTuCach+SubString(@StrFortmat, 1, @SoChuSo - Len(@Stt_rec_max+1)) + Convert(nvarchar(5), @Stt_rec_max+1)
			End
			Else If(@Url is not null and @Url<>'' and @Url='/IED/Analysis')
			Begin /*** Phân tích mã hàng ***/
				if(@SoChuSo=0)set @SoChuSo=3
				select @ChuoiBatDau=ChuoiBatDau,@KyTuCach=KyTuCach,@Stt_rec_max=Stt_RecDm,@SoChuSo=SoChuSo from dbo.SYS_MENU_MASTER where MenuId=@MenuParent
				if(@KyTuCach is not null)
					set @MenuName=@ChuoiBatDau+@KyTuCach+SubString(@StrFortmat, 1, @SoChuSo - Len(@Stt_rec_max+1)) + Convert(nvarchar(5), @Stt_rec_max+1)
				else
					set @MenuName=@ChuoiBatDau+SubString(@StrFortmat, 1, @SoChuSo - Len(@Stt_rec_max+1)) + Convert(nvarchar(5), @Stt_rec_max+1)
				select @MenuName,@MenuParent
			End
			Else If(@Sort is null)
			Begin
				set @Sort=0
			End
			Else if(@Sort>0)
				set @MenuName=@MenuName+' '+CONVERT(varchar(10),@Sort)+''
			if(@Stt_rec_max is null) set @Stt_rec_max=0
			
			if((@Url<>'' and @Url<>'/IED/Library') and( @ChuoiBatDau is null or @ChuoiBatDau=''))
			Begin
				raiserror(N'#"Chuỗi bắt đầu" chưa được tạo. Nhấn chuột phải vào thư mục chọn "Thuộc tính" và cài đặt chuỗi bắt đầu!',16,1)
				commit
				return
			End
			if(@Url='/IED/Library')
				exec sp_SYS_GETSTT_RECDM '','DIC_THUVIEN',@MenuName out
			if(@SoChuSo is null or @SoChuSo=0)set @SoChuSo=3
			/*** Insert ***/
			insert into dbo.SYS_MENU_MASTER(MenuId,MenuName,MenuParent,ContextMenuId,ContextMenuIdFile,Url,DefaultUrl,PageName,Sort,MenuType,SoChuSo)
			values(@MenuId,@MenuName,@MenuParent,@ContextMenuId,@ContextMenuIdFile,@Url,@DefaultUrl,@PageName,@Sort+1,@MenuType,@SoChuSo)
			
			/***  ***/
			;WITH temp(Id, text,ParentId,iconCls,Sort,href,onclick, Node)
						as (
								Select l1.Id,l1.text,l1.ParentId,l1.iconCls,l1.Sort,l1.href,l1.onclick, 0 as Node
								From dbo.SYS_CONTEXT_MENU l1
								Where l1.Id=@ContextMenuId
								Union All
								Select b.Id,b.text,b.ParentId,b.iconCls,b.Sort,b.href,b.onclick, a.Node + 1
								From temp as a, dbo.SYS_CONTEXT_MENU as b
								Where a.Id = b.ParentId
			)
			insert into dbo.SYS_MENU_CONTEXT_MENU(MenuId,ContextMenuId)
			select @MenuId,t.Id from temp t
			where t.Id not in(
				select t1.ContextMenuId from dbo.SYS_MENU_CONTEXT_MENU t1
				where t1.MenuId=@MenuId and t.Id=t1.ContextMenuId)
			
				set @OutNewName=@MenuName
			/*** Cap nhat lai so thu tu tu tang ***/
			if(@Url is not null and @Url<>'' and (@Url='/IED/Step'))
				Update dbo.SYS_MENU_MASTER set Stt_RecDm=@Stt_rec_max+1 where MenuId=@MenuParent
			Else If(@Url is not null and @Url<>'' and @Url='/IED/Analysis')
				Update dbo.SYS_MENU_MASTER set Stt_RecDm=@Stt_rec_max+1 where MenuId=@MenuParent
				
			
		End
		Else
		Begin
			/*** Update ***/
			update dbo.SYS_MENU_MASTER set	MenuName=@MenuName
			where MenuId=@MenuId
			--set @Action=''
			
		End
		If(@Url='/IED/Analysis')
		Begin
			set @Action=''
		End
		Else If(@Url='/IED/Step')
		Begin
			/*** Bỏ ***/
			--if(select COUNT(MenuId) from dbo.IED_CONGDOAN where MenuId=@MenuId)=0
			--Begin
			--	Insert into dbo.IED_CONGDOAN(MenuId,MaSo) values(@MenuId,@MenuName)
			--End
			set @Action=''
		End
		Else If(@Url='/IED/Library')
		Begin
			select @Count=COUNT(MenuId) from dbo.DIC_THUVIEN where MenuId=@MenuId
			if(@Count=0)
				Insert into dbo.DIC_THUVIEN(MaThuVien,MoTa,MenuId)
					values(@MenuName,'',@MenuId)
			else
				Update dbo.DIC_THUVIEN set MaThuVien=@MenuName
				where MenuId=@MenuId
		End
		
	End /***End action POST ***/
	Else if(@Action='DELETE')
	Begin
		/*** Xóa bảng thư viện ***/
		delete from dbo.DIC_THUVIEN where MenuId=@MenuId
		/*** Xóa bảng công đoạn ***/
		delete d from dbo.IED_THAOTACCONGDOAN d
		where d.Id_DmCd in (select t1.Id_Dm from dbo.IED_CONGDOAN t1 where t1.MenuId=@MenuId)
		delete from dbo.IED_CONGDOAN where MenuId=@MenuId
		/*** Xóa bảng mã hàng ***/
		delete d from dbo.GTD_THAOTACMAHANG d
		where d.Id_DmMaHang in (select t1.Id_Dm from dbo.GTD_MAHANG t1 where t1.MenuId=@MenuId)
		delete from dbo.GTD_MAHANG where MenuId=@MenuId
		
		;WITH temp(MenuId, MenuName,MenuParent, Node)
				as (
						Select l1.MenuId,l1.MenuName,l1.MenuParent, 0 as Node
						From dbo.SYS_MENU_MASTER l1
						Where l1.MenuId=@MenuId
						Union All
						Select b.MenuId,b.MenuName,b.MenuParent, a.Node + 1
						From temp as a, dbo.SYS_MENU_MASTER as b
						Where a.MenuId = b.MenuParent
				)
		DELETE FROM SYS_MENU_MASTER
		--UPDATE dbo.SYS_MENU_MASTER set IsVisible=1
		WHERE MenuId IN (SELECT MenuId FROM temp)
		
		delete from dbo.SYS_MENU_CONTEXT_MENU where MenuId=@MenuId
	End /*** End Action DELETE***/
	
COMMIT

GO


