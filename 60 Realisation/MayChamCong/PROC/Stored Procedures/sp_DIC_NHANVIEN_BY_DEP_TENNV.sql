GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_NHANVIEN_BY_DEP_TENNV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_NHANVIEN_BY_DEP_TENNV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<Lấy nhân viên theo phòng ban và theo tên nhân viên>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_NHANVIEN_BY_DEP_TENNV] 
@SelectedPage int,
@PageSize int,
@DepID varchar(50),
@TenNV nvarchar(100),
@TotalPages int output,
@TotalRecords int output
WITH ENCRYPTION
AS
BEGIN
	
	--declare @t1 int,@t2 int
	--exec sp_DIC_NHANVIEN_BY_DEP_TENNV 1,10000,'DEP.ROOT','',@t1 out,@t2 out
	--select @t1,@t2
	
	--set @DepID='SH.PX01'
	--set @TenNV=''
	-- set @SelectedPage=1
	-- set @PageSize=10
	
	set @TotalPages=0
	set @TotalRecords=0
	DECLARE @ReturnedRecords int
	-- Finds total records
	select @TotalRecords=COUNT(MaNV) from  dbo.fncNHANVIENBYDEP(@DepID,@TenNV)
		
	-- Finds number of pages
	SET @ReturnedRecords = (@PageSize * @SelectedPage)
	 set @TotalPages = @TotalRecords / @PageSize
	IF @TotalRecords % @PageSize > 0
	BEGIN
		 SET @TotalPages = @TotalPages + 1
	END

	;WITH LogEntries AS (
	SELECT ROW_NUMBER() OVER (ORDER BY MaCC )
	AS RowNumber, MaCC 
	FROM dbo.fncNHANVIENBYDEP(@DepID,@TenNV))
		 
	SELECT *
	FROM LogEntries
	INNER JOIN dbo.fncNHANVIENBYDEP(@DepID,@TenNV) t ON LogEntries.MaCC = t.MaCC 
	WHERE RowNumber between 
	CONVERT(nvarchar(10), (@SelectedPage - 1) * @PageSize + 1)  and CONVERT(nvarchar(10), @SelectedPage*@PageSize)
	ORDER BY CAST(LogEntries.MaCC as int) asc
	
	--select @TotalPages,@TotalRecords

		
END

GO


