GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_MENU_GET_NODE_BY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_MENU_GET_NODE_BY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_MENU_GET_NODE_BY] 
@Action varchar(100),
@Id varchar(50),
@HasLastNode bit --False lấy tất cả node trừ node cuối cùng, True: lấy tất các node
WITH ENCRYPTION
AS
BEGIN
	--ContextMenuIdFile,DefaultUrl,MoTa,ChuoiBatDau,KyTuCach,SoChuSo,SoBatDau,SoKetThuc
	--exec sp_SYS_MENU_GET_NODE_BY 'get','09.02.00','true'
	If(@Action='GET')
	Begin
		if(@HasLastNode=1)
		Begin
			;WITH temp(Id, Name,ParentId,Sort,Node,url,iconCls,ContextMenuId,IsFunc)
				as (
						Select l1.MenuId,l1.MenuName,l1.MenuParent,l1.Sort, 0 as Node,l1.Url,l1.iconCls,l1.ContextMenuId,l1.IsFunc
						From dbo.SYS_MENU_MASTER l1
						Where l1.MenuId=@Id
						Union All
						Select b.MenuId,b.MenuName,b.MenuParent,b.Sort, a.Node + 1,b.Url,b.iconCls,b.ContextMenuId,b.IsFunc
						From temp as a, dbo.SYS_MENU_MASTER as b
						Where a.Id = b.MenuParent 
				)
			select * From temp 
			order by Sort asc
		End
		Else
		Begin
			;WITH temp(Id, Name,ParentId,Sort,Node,url,iconCls,ContextMenuId,IsFunc)
				as (
						Select l1.MenuId,l1.MenuName,l1.MenuParent,l1.Sort, 0 as Node,l1.Url,l1.iconCls,l1.ContextMenuId,l1.IsFunc
						From dbo.SYS_MENU_MASTER l1
						Where l1.MenuId=@Id
						Union All
						Select b.MenuId,b.MenuName,b.MenuParent,b.Sort, a.Node + 1,b.Url,b.iconCls,b.ContextMenuId,b.IsFunc
						From temp as a, dbo.SYS_MENU_MASTER as b
						Where a.Id = b.MenuParent and IsNull(b.Url, '') = ''
				)
			select * From temp 
			order by Sort asc
		End
	End 
	
END

GO


