GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_THAOTACSANPHAM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_THAOTACSANPHAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_THAOTACSANPHAM] 
@Action varchar(100),
@TYPE as dbo.TYPE_DIC_THAOTACSANPHAM readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		
		If(@Action='POST')
		Begin
			/*** CẬP NHẬT TRONG TRƯỜNG HỢP MÃ ĐÃ CÓ ***/
			Update dbo.DIC_THAOTACSANPHAM set MoTa=t2.MoTa,SoLuong=t2.SoLuong,ThoiGianThaoTac=t2.ThoiGianThaoTac
			from dbo.DIC_THAOTACSANPHAM t1
			inner join @TYPE t2
			on t1.MaThaoTacSP=t2.MaThaoTacSP
			
			/*** THÊM MỚI ***/
			Insert into dbo.DIC_THAOTACSANPHAM(MaThaoTacSP,MoTa,SoLuong,ThoiGianThaoTac,RootId)
			select UPPER(MaThaoTacSP),MoTa,SoLuong,ThoiGianThaoTac,RootId
			from @TYPE t1
			Where t1.MaThaoTacSP
			Not In (Select t2.MaThaoTacSP FROM dbo.DIC_THAOTACSANPHAM t2 
					where t2.MaThaoTacSP=t1.MaThaoTacSP)
		End
		Else if(@Action='DELETE')
		Begin
			/*** XÓA ***/
			delete pl from dbo.DIC_THAOTACSANPHAM pl
			where pl.MaThaoTacSP in
				(select p1.MaThaoTacSP from @TYPE p1)
		End 
		
COMMIT

GO


