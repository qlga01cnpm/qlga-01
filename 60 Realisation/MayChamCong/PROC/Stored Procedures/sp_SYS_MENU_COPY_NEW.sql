GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_MENU_COPY_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_MENU_COPY_NEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_MENU_COPY_NEW] 
@MenuId varchar(100),
@Url nvarchar(max)
--,@NewMenuId nvarchar(max)
WITH ENCRYPTION
AS
BEGIN
	-- sp_SYS_MENU_COPY_NEW 'A0000516MNU',''
	If(OBJECT_ID('tempdb..#menu') Is Not Null) Drop Table #menu
	/*** Lấy Item tạm ***/
	select * into #menu from  dbo.SYS_MENU_MASTER where MenuId=@MenuId
	if(select COUNT(MenuId) from #menu)>0
	Begin
		declare @NewMenuId varchar(20)
		select * from #menu
		If(@Url='/IED/Analysis')
		Begin --Copy phân tích mã hàng
			exec sp_SYS_GETSTT_RECDM '','SYS_MENU_MASTER',@NewMenuId out
			
			
		End 
		Else If(@Url='/IED/Step')
		Begin --Copy phân tích công đoạn
			exec sp_SYS_GETSTT_RECDM '','SYS_MENU_MASTER',@NewMenuId out
			
			
			
		End
	End
	
	If(OBJECT_ID('tempdb..#menu') Is Not Null) Drop Table #menu
	
END

GO


