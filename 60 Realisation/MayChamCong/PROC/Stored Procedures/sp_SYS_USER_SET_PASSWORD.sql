GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_SYS_USER_SET_PASSWORD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SYS_USER_SET_PASSWORD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_SYS_USER_SET_PASSWORD] 
@UserId varchar(50),
@Password varchar(40),
@RePassword varchar(40)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	/*** CẬP NHẬT LẠI THÔNG TIN NGƯỜI DÙNG ***/
		if(@UserId='' or @UserId='admin')
		Begin
			commit
			return
		End
		if(@Password<>@RePassword)
		Begin
			raiserror(N'Nhập lại mật khẩu không trùng khớp',16,1)
			commit
			return
		End
		
		update dbo.SYS_USERS set Password=@Password
			where UserID=@UserId
COMMIT

GO


