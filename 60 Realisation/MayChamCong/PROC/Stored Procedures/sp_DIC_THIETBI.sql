GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_DIC_THIETBI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DIC_THIETBI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DIC_THIETBI] 
@Action varchar(100),
@TYPE as dbo.TYPE_DIC_THIETBI readonly
WITH ENCRYPTION
AS
BEGIN TRANSACTION
		
		If(@Action='POST')
		Begin
			/*** CẬP NHẬT TRONG TRƯỜNG HỢP MÃ ĐÃ CÓ ***/
			Update dbo.DIC_THIETBI set MoTa=t2.MoTa,SoMuiCM=t2.SoMuiCM,TocDo=t2.TocDo, 
									   HaoPhiThietBi=t2.HaoPhiThietBi,HaoPhiTayChan=t2.HaoPhiTayChan
			from dbo.DIC_THIETBI t1
			inner join @TYPE t2
			on t1.MaThietBi=t2.MaThietBi
			
			/*** THÊM MỚI ***/
			Insert into dbo.DIC_THIETBI(MaThietBi,MoTa,SoMuiCM,TocDo,HaoPhiThietBi,HaoPhiTayChan,RootId)
			select UPPER(MaThietBi),MoTa,SoMuiCM,TocDo,HaoPhiThietBi,HaoPhiTayChan,RootId
			from @TYPE t1
			Where t1.MaThietBi
			Not In (Select t2.MaThietBi FROM dbo.DIC_THIETBI t2 
					where t2.MaThietBi=t1.MaThietBi)
		End
		Else if(@Action='DELETE')
		Begin
			/*** XÓA ***/
			delete pl from dbo.DIC_THIETBI pl
			where pl.MaThietBi in
				(select p1.MaThietBi from @TYPE p1)
		End 
		
COMMIT

GO


