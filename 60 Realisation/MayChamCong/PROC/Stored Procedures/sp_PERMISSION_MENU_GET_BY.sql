GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[sp_PERMISSION_MENU_GET_BY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PERMISSION_MENU_GET_BY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PERMISSION_MENU_GET_BY] 
@Action varchar(100),
@UserId varchar(50)
WITH ENCRYPTION
AS
BEGIN TRANSACTION
	--exec sp_PERMISSION_MENU_GET_BY 'get','quanly'
	declare @Count int
	If(@Action='GET')
	Begin
		select t1.MenuId,t1.MenuName,t1.MenuParent,t1.Sort,t1.Url,t1.IsFunc
		,case when j1.AllowView is null then 0 else j1.AllowView end as AllowView
		,case when j1.AllowAdd is null then 0 else j1.AllowAdd end as AllowAdd
		,case when j1.AllowEdit is null then 0 else j1.AllowEdit end as AllowEdit
		,case when j1.AlowDelete is null then 0 else j1.AlowDelete end as AlowDelete
		,case when j1.AllowImport is null then 0 else j1.AllowImport end as AllowImport
		,case when j1.AllowExport is null then 0 else j1.AllowExport end as AllowExport
		,case when j1.AllowDownload is null then 0 else j1.AllowDownload end as AllowDownload
		,j1.UserID
		From dbo.SYS_MENU_MASTER t1 with(nolock)
		left join (select t3.UserID,t4.MenuID,t4.AllowView,t4.AllowAdd,t4.AllowEdit,t4.AlowDelete,
						  t4.AllowImport,t4.AllowExport,t4.AllowPrint,t4.AllowDownload
					from  dbo.SYS_USERS t3 with(nolock)
					left join dbo.SYS_USER_MENU t4 with(nolock)
					on t3.UserID=t4.UserID and t3.UserID=@UserId ) j1
			
		on t1.MenuId=j1.MenuID
		where t1.IsVisible=0
		--where Url<>'/IED/Library' and Url<>'/IED/Step'
		order by t1.Sort asc
	End /***End action POST ***/
	Else if(@Action='DELETE')
	Begin
		select ''
	End /*** End Action DELETE***/
	
COMMIT

GO


