GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fncReturnLastTimeDownload]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncReturnLastTimeDownload]
GO


GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fncReturnLastTimeDownload](@Door nvarchar(50))
Returns datetime
With encryption
AS
Begin
	-- select dbo.fncReturnLastTimeDownload(1) as DT
	declare @DateTime datetime
	Select @DateTime=MAX(WorkingDay) from dbo.MCC_ATTLOGS
	Return @DateTime
End

GO


