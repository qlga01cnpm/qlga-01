GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fncReturnSoNgayNghi]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncReturnSoNgayNghi]
GO


GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fncReturnSoNgayNghi](@eID bigint, @Month int,@Year int)
Returns varchar(10)
With encryption
AS
Begin
	-- select dbo.fncReturnSoNgayNghi(11,7,2018) as SoNgayNghi
	declare @SoNgayNghi int
	select  @SoNgayNghi=
		 case when ISNULL(D1,'')<>'' then 1 else 0 end +
		 case when ISNULL(D2,'')<>'' then 1 else 0 end +
		 case when ISNULL(D3,'')<>'' then 1 else 0 end +
		 case when ISNULL(D4,'')<>'' then 1 else 0 end +
		 case when ISNULL(D5,'')<>'' then 1 else 0 end +
		 case when ISNULL(D6,'')<>'' then 1 else 0 end +
		 case when ISNULL(D7,'')<>'' then 1 else 0 end +
		 case when ISNULL(D8,'')<>'' then 1 else 0 end +
		 case when ISNULL(D9,'')<>'' then 1 else 0 end +
		 case when ISNULL(D10,'')<>'' then 1 else 0 end +
		 
		 
		 case when ISNULL(D11,'')<>'' then 1 else 0 end +
		 case when ISNULL(D12,'')<>'' then 1 else 0 end +
		 case when ISNULL(D13,'')<>'' then 1 else 0 end +
		 case when ISNULL(D14,'')<>'' then 1 else 0 end +
		 case when ISNULL(D15,'')<>'' then 1 else 0 end +
		 case when ISNULL(D16,'')<>'' then 1 else 0 end +
		 case when ISNULL(D17,'')<>'' then 1 else 0 end +
		 case when ISNULL(D18,'')<>'' then 1 else 0 end +
		 case when ISNULL(D19,'')<>'' then 1 else 0 end +
		 case when ISNULL(D20,'')<>'' then 1 else 0 end +
		 
		 case when ISNULL(D21,'')<>'' then 1 else 0 end +
		 case when ISNULL(D22,'')<>'' then 1 else 0 end +
		 case when ISNULL(D23,'')<>'' then 1 else 0 end +
		 case when ISNULL(D24,'')<>'' then 1 else 0 end +
		 case when ISNULL(D25,'')<>'' then 1 else 0 end +
		 case when ISNULL(D26,'')<>'' then 1 else 0 end +
		 case when ISNULL(D27,'')<>'' then 1 else 0 end +
		 case when ISNULL(D28,'')<>'' then 1 else 0 end +
		 case when ISNULL(D29,'')<>'' then 1 else 0 end +
		 case when ISNULL(D30,'')<>'' then 1 else 0 end +
		 case when ISNULL(D31,'')<>'' then 1 else 0 end
		 
		from dbo.DIC_DANGKY_VANG A with(nolock) where A.sYear=@Year and A.sMonth=@Month and A.eID=@eID
		
	if(@SoNgayNghi is null or @SoNgayNghi=0) Return ''
		
	Return cast(@SoNgayNghi as varchar(10))
End

GO


