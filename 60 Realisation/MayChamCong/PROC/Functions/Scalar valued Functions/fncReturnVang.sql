GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fncReturnVang]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncReturnVang]
GO


GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fncReturnVang](@eID bigint,@Workingday datetime)
Returns nvarchar(50)
With encryption
AS
Begin
	-- select dbo.fncReturnVang(1097,'2018-07-10') as Vang
	declare @LyDoVang nvarchar(50),@Day int
	set @Day=DAY(@Workingday)
	
	if(@Day=1)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D1= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=2)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D2= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=3)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D3= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=4)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D4= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=5)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D5= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=6)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D6= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=7)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D7= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=8)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D8= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=9)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D9= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=10)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D10= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
			
	else if(@Day=11)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D11= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=12)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D12= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=13)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D13= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=14)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D14= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=15)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D15= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=16)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D16= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=17)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D17= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=18)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D18= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=19)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D19= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
			
			
	else if(@Day=20)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D20= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=21)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D21= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=22)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D22= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=23)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D23= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=24)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D24= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=25)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D25= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=26)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D26= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=27)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D27= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=28)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D28= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=29)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D29= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	else if(@Day=30)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D30= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
			
			
	else if(@Day=31)
		select  @LyDoVang=B.LyDo
			from dbo.DIC_DANGKY_VANG A with(nolock) 
			inner join dbo.DIC_LYDOVANG B with(nolock) on A.D31= B.MaSo
			where A.sYear=YEAR(@Workingday) and A.sMonth=MONTH(@Workingday) and A.eID=@eID
	
			
	if(@LyDoVang is null) return ''
	Return @LyDoVang
End

GO


