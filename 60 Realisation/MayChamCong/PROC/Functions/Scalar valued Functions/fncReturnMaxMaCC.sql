GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fncReturnMaxMaCC]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncReturnMaxMaCC]
GO


GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fncReturnMaxMaCC]()
Returns int
With encryption
AS
Begin
	--select dbo.fncReturnMaxMaCC() as MaCC
	declare @MaxMaCC int
	Select @MaxMaCC= max(MaCC) from dbo.DIC_NHANVIEN
	if(@MaxMaCC is null)
		set @MaxMaCC=0
	Return @MaxMaCC
End

GO


