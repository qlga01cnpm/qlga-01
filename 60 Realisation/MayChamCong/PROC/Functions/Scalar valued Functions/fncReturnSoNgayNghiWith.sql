GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fncReturnSoNgayNghiWith]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncReturnSoNgayNghiWith]
GO


GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fncReturnSoNgayNghiWith](@eID bigint, @Month int,@Year int,@MaSoLyDo nvarchar(10))
Returns varchar(10)
With encryption
AS
Begin
	-- select dbo.fncReturnSoNgayNghiWith(773,7,2018,'F') as SoNgayNghi
	declare @SoNgayNghi int
	select  @SoNgayNghi=
		 case when ISNULL(D1,'')<>'' and D1=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D2,'')<>'' and D2=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D3,'')<>'' and D3=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D4,'')<>'' and D4=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D5,'')<>'' and D5=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D6,'')<>'' and D6=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D7,'')<>'' and D7=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D8,'')<>'' and D8=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D9,'')<>'' and D9=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D10,'')<>'' and D10=@MaSoLyDo then 1 else 0 end +
		 
		 
		 case when ISNULL(D11,'')<>'' and D11=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D12,'')<>'' and D12=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D13,'')<>'' and D13=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D14,'')<>'' and D14=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D15,'')<>'' and D15=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D16,'')<>'' and D16=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D17,'')<>'' and D17=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D18,'')<>'' and D18=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D19,'')<>'' and D19=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D20,'')<>'' and D10=@MaSoLyDo then 1 else 0 end +
		 
		 case when ISNULL(D21,'')<>'' and D21=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D22,'')<>'' and D22=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D23,'')<>'' and D23=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D24,'')<>'' and D24=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D25,'')<>'' and D25=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D26,'')<>'' and D26=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D27,'')<>'' and D27=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D28,'')<>'' and D28=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D29,'')<>'' and D29=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D30,'')<>'' and D30=@MaSoLyDo then 1 else 0 end +
		 case when ISNULL(D31,'')<>'' and D31=@MaSoLyDo then 1 else 0 end
		 
		from dbo.DIC_DANGKY_VANG A with(nolock) where A.sYear=@Year and A.sMonth=@Month and A.eID=@eID
		
	if(@SoNgayNghi is null or @SoNgayNghi=0) Return ''
		
	Return cast(@SoNgayNghi as varchar(10))
End

GO


