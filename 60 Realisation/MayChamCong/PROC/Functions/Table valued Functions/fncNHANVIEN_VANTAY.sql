GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fncNHANVIEN_VANTAY]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncNHANVIEN_VANTAY]
GO

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*** LẤY THÔNG TIN VÂN TAY CỦA NHÂN VIÊN THEO PHÒNG BAN VÀ TÊN NHÂN VIÊN ***/
CREATE FUNCTION [dbo].[fncNHANVIEN_VANTAY](@DepId varchar(100),@TenNV nvarchar(100))  
RETURNS TABLE  
WITH ENCRYPTION
AS  
RETURN  
	-- SELECT * FROM fncNHANVIEN_VANTAY('DEP.ROOT','cúc') where cast(UserId as varchar(10)) like '10'
    WITH temp(DepID, DepName,ParentID, Node)
        as (
                Select l1.DepID,l1.DepName,l1.ParentID, 0 as Node
                From dbo.SYS_DEPARTMENTS l1 with(nolock)
                Where l1.DepID=@DepId
                Union All
                Select b.DepID,b.DepName,b.ParentID, a.Node + 1
                From temp as a, dbo.SYS_DEPARTMENTS as b with(nolock)
                Where a.DepID = b.ParentID
        )
Select nv.MaCC as UserId,nv.TenNV, nv.UserName,nv.CardID,nv.Privilege,nv.Enabled,nv.BackupNumber
	,case when ISNULL(nv.Password,'') LIKE '' then 0 else 1 end as  IsPassword
	,case when ISNULL(nv.sTmpData0,'') LIKE '' then  0 else 1 end as FingerIndex0
	,case when ISNULL(nv.sTmpData1,'') LIKE '' then  0 else 1 end as FingerIndex1
	,case when ISNULL(nv.sTmpData2,'') LIKE '' then  0 else 1 end as FingerIndex2
	,case when ISNULL(nv.sTmpData3,'') LIKE '' then  0 else 1 end as FingerIndex3
	,case when ISNULL(nv.sTmpData4,'') LIKE '' then  0 else 1 end as FingerIndex4
	,case when ISNULL(nv.sTmpData5,'') LIKE '' then  0 else 1 end as FingerIndex5
	,case when ISNULL(nv.sTmpData6,'') LIKE '' then  0 else 1 end as FingerIndex6
	,case when ISNULL(nv.sTmpData7,'') LIKE '' then  0 else 1 end as FingerIndex7
	,case when ISNULL(nv.sTmpData8,'') LIKE '' then  0 else 1 end as FingerIndex8
	,case when ISNULL(nv.sTmpData9,'') LIKE '' then  0 else 1 end as FingerIndex9

From temp dep
inner join dbo.DIC_CHUYENBOPHAN depC with(nolock)
inner join dbo.DIC_NHANVIEN nv with(nolock)
on depC.MaNV=nv.Id on dep.DepID=depC.DepID
where depC.ChangedDate=(Select MAX(c.ChangedDate) 
						from dbo.DIC_CHUYENBOPHAN c
						where c.MaNV=nv.Id)
and nv.IsVisible=0  and isnull(nv.IsNghiViec,'')=0
and nv.TenNV like '%'+rtrim(@TenNV)+'%'
GO


