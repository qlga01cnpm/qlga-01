GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fncNHANVIENBYDEPMANV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fncNHANVIENBYDEPMANV]
GO

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*** Lấy nhân viên đệ quy theo phòng ban và theo mã nhân viên ***/
CREATE FUNCTION [dbo].[fncNHANVIENBYDEPMANV](@DepId varchar(100),@MaNV varchar(100))  
RETURNS TABLE  
WITH ENCRYPTION
AS  
RETURN  
    WITH temp(DepID, DepName,ParentID, Node)
        as (
                Select l1.DepID,l1.DepName,l1.ParentID, 0 as Node
                From dbo.SYS_DEPARTMENTS l1 with(nolock)
                Where l1.DepID=@DepId
                Union All
                Select b.DepID,b.DepName,b.ParentID, a.Node + 1
                From temp as a, dbo.SYS_DEPARTMENTS as b with(nolock)
                Where a.DepID = b.ParentID
        )
Select nv.*
		,dep.DepID, dep.DepName
From temp dep
inner join dbo.DIC_CHUYENBOPHAN depC with(nolock)
inner join dbo.DIC_NHANVIEN nv with(nolock)
on depC.MaNV=nv.Id on dep.DepID=depC.DepID
where depC.ChangedDate=(Select MAX(c.ChangedDate) 
						from dbo.DIC_CHUYENBOPHAN c
						where c.MaNV=nv.Id)
and (nv.IsVisible=0 or HideVirtual=0 or isnull(nv.IsNghiViec,'')=0)
and nv.MaNV=@MaNV
GO


