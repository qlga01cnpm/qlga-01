
GO

IF  EXISTS (SELECT * FROM sys.triggers 
WHERE object_id = OBJECT_ID(N'[dbo].[tg_OnUSER_GROUP_DOES_NOT_DEL]'))
DROP TRIGGER [dbo].[tg_OnUSER_GROUP_DOES_NOT_DEL]
GO

-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <22-08-2017>
-- Description:	<Không cho xóa 2 tài khoản admin và user>
-- =============================================
CREATE TRIGGER tg_OnUSER_GROUP_DOES_NOT_DEL
   ON  dbo.SYS_USER_GROUP
WITH ENCRYPTION
   FOR DELETE
AS 
BEGIN
	SET NOCOUNT ON;

    IF EXISTS (
        SELECT *
        FROM deleted
        WHERE UserID = 'admin' or UserID='user'
    )
    BEGIN
        ROLLBACK;
        raiserror(N'Không thể xóa tài khoản này!',16,1)
    END

END
GO
