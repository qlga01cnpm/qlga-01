
GO

IF  EXISTS (SELECT * FROM sys.triggers 
WHERE object_id = OBJECT_ID(N'[dbo].[tg_OnGROUP_DOES_NOT_DEL]'))
DROP TRIGGER [dbo].[tg_OnGROUP_DOES_NOT_DEL]
GO

-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <23-08-2017>
-- Description:	<Không cho xóa nhóm >
-- =============================================
CREATE TRIGGER tg_OnGROUP_DOES_NOT_DEL
   ON  dbo.SYS_GROUP
WITH ENCRYPTION
   FOR DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
    IF EXISTS (
        SELECT *
        FROM deleted
        WHERE GroupID = 'Administrator' or GroupID='SYSTEM' or GroupID='USERS'
    )
    BEGIN
        ROLLBACK;
        raiserror(N'Nhóm này không thể chỉnh sửa hoặc xóa!',16,1)
    END

END
GO
