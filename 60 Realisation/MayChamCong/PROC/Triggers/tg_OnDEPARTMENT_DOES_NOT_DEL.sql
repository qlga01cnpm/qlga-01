
GO

IF  EXISTS (SELECT * FROM sys.triggers 
WHERE object_id = OBJECT_ID(N'[dbo].[tg_OnDEPARTMENT_DOES_NOT_DEL]'))
DROP TRIGGER [dbo].[tg_OnDEPARTMENT_DOES_NOT_DEL]
GO

-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <22-08-2017>
-- Description:	<Không cho xóa 2 phòng ban>
-- =============================================
CREATE TRIGGER tg_OnDEPARTMENT_DOES_NOT_DEL
   ON  dbo.SYS_DEPARTMENTS
WITH ENCRYPTION
   FOR DELETE
AS 
BEGIN
	SET NOCOUNT ON;
    IF EXISTS (
        SELECT *
        FROM deleted
        WHERE DepID = 'DEP.NEW' or DepID='DEP.ROOT'
    )
    BEGIN
        ROLLBACK;
        raiserror(N'Không thể xóa 2 phòng ban này!',16,1)
    END
END
GO
