
GO

IF  EXISTS (SELECT * FROM sys.triggers 
WHERE object_id = OBJECT_ID(N'[dbo].[tg_OnDEPARTMENT_DOES_NOT_UPDATE]'))
DROP TRIGGER [dbo].[tg_OnDEPARTMENT_DOES_NOT_UPDATE]
GO

-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<NamThanhBinhCo>
-- Create date: <22-08-2017>
-- Description:	<Không cho xóa 2 phòng ban>
-- =============================================
CREATE TRIGGER tg_OnDEPARTMENT_DOES_NOT_UPDATE
   ON  dbo.SYS_DEPARTMENTS
WITH ENCRYPTION
   FOR UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF update(DepID)
	Begin
		IF  EXISTS (
        SELECT *
        FROM Updated
        WHERE DepID = 'DEP.NEW' or DepID='DEP.ROOT'
    )
		Begin
			ROLLBACK TRANSACTION;
			raiserror(N'Không thể chỉnh sửa cột này!',16,1)
		End
	End
END
GO
