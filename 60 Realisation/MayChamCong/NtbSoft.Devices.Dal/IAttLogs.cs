﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.Dal
{
    public delegate void AttLogsEventHandler(object sender, AttLogsEventArgs e);
    public delegate void AttReadingEventHandler(object sender,AttReadingEventArgs e);
    public delegate void AttClearGLogEventHandler(object sender,AttClearGLogEventArgs e);

    public class AttLogsEventArgs : EventArgs
    {
        public List<Dal.IUserInfo> IUserCollection { get; set; }

        public Exception Ex { get; set; }
    }

    public class AttReadingEventArgs : EventArgs
    {
        public int RowIndex { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public Exception Ex { get; set; }
    }

    public class AttClearGLogEventArgs : EventArgs
    {
        //public bool IsDeleted { get; set; }
        public Exception Ex { get; set; }
    }

    public interface IAttLogs
    {
        event AttLogsEventHandler OnDownloadCompleted;
        event AttReadingEventHandler OnDownloading;
        event AttClearGLogEventHandler OnClearGLogCompleted;
        void DownloadData();
        void DownloadAttLogs();
        
        //void GetRecordCount();
        void ClearGLog();
    }
}
