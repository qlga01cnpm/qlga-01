﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.Dal
{
    public interface IConnect
    {
        void Connect(string COM, int BaudRate, Action<zkemkeeper.IZKEM, Exception> Complete);
    }
}
