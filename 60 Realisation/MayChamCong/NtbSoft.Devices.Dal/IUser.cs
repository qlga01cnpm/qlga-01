﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.Dal
{
    public delegate void UserUploadEventHandler(object sender,UserUploadEventArgs e);
    public class UserUploadEventArgs : EventArgs
    {
        public int RowIndex { get; set; }
        public int FingerIndex { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public Exception Ex { get; set; }
    }

    public interface IUserInfo
    {
        string UserId { get; set; }
        string UserName { get; set; }
        int FingerIndex { get; set; }
        string TmpData { get; set; }
        int Privilege { get; set; }
        string Password { get; set; }
        bool Enabled { get; set; }
        int Flag { get; set; }
        DateTime DateInOut { get; set; }
        int MachineNumber { get; set; }
        string StrDate { get; }
        string StrTime { get; }
        
    }

    public interface IUser
    {
        event AttLogsEventHandler OnDownloadCompleted;
        event UserUploadEventHandler OnDownloading;

        void DownloadFPTmp();
        void UploadFPTmp(List<IUserInfo> Items);
    }
}
