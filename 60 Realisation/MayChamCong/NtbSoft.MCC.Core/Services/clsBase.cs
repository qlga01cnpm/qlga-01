﻿using DapperUtil;
using Dev.Sdk.Generic;
using Dev.Sdk.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.MCC.Core.Services
{
    public abstract class clsBase<T> : GenericRepository<T> where T : class
    {
        protected clsBase()
            : base(
                new ConnectionFactory().GetOpenConnection(ConnectionType.Sql,
                    ConfigurationManager.ConnectionStrings["strCnn_ln3"].ConnectionString))
        {
        }
         
        public virtual IEnumerable<T> GetAll(Func<T, bool> predicate = null)
        {
            if (predicate != null)
            {
                return base.GetAll().Where(predicate);
            }

            return base.GetAll();
        }

        public override IResult Insert(T item)
        {
            var result = ValidateInsert(item);
            if (result.Success)
            {
                return base.Insert(item);
            }

            return result;
        }

        public override IResult Update(T item)
        {
            var result = ValidateUpdate(item);
            if (result.Success)
            {
                return base.Update(item);
            }

            return result;
        }

        public override IResult Delete(T item)
        {
            var result = ValidateDelete(item);
            if (result.Success)
            {
                return base.Delete(item);
            }

            return result;
        }

        public IResult DeleteByKey(object id)
        {
            var item = Get(id);
            var result = ValidateDelete(item);
            if (result.Success)
            {
                return base.Delete(item);
            }

            return result;
        }

        protected virtual IResult ValidateInsert(T item)
        {
            return new BaseResult();
        }

        protected virtual IResult ValidateUpdate(T item)
        {
            return new BaseResult();
        }

        protected virtual IResult ValidateDelete(T item)
        {
            return new BaseResult();
        }
    }
}
