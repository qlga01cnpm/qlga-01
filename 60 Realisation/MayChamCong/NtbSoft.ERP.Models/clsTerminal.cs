﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{

    public class clsTerminal:Interfaces.IBase<Entity.Interfaces.ITerminalInfo>
    {
        public void BatchDelete(IEnumerable<Entity.Interfaces.ITerminalInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            
        }

        public void BatchInsert(IEnumerable<Entity.Interfaces.ITerminalInfo> Items,Action<Interfaces.IResultOk> Complete)
        {
        }

        public void BatchUpdate(IEnumerable<Entity.Interfaces.ITerminalInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TERMINAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@TerminalID", Id);
                    _cmd.Parameters.AddWithValue("@Name", "");

                    _cmd.Parameters.AddWithValue("@Connection", "");
                    _cmd.Parameters.AddWithValue("@BaudRate", 0);
                    _cmd.Parameters.AddWithValue("@IpAddress", "");
                    _cmd.Parameters.AddWithValue("@Port", 0);
                    _cmd.Parameters.AddWithValue("@CommType", 0);
                    _cmd.Parameters.AddWithValue("@Status", 0);

                    _cmd.Parameters.AddWithValue("@ActiveKey", "");
                    _cmd.Parameters.AddWithValue("@SerialNumber", "");

                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add("ERR_01", ex.Message);
            }
            if (Complete != null) Complete(result);
        }

        public void GetBy(object Id, Action<Entity.Interfaces.ITerminalInfo,Exception> Complete)
        {
            Entity.clsTerminalInfo Item = new Entity.clsTerminalInfo();
            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_TERMINAL");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where TerminalID={0}", Id));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var items = Libs.ConvertTableToList.ConvertToList<Entity.clsTerminalInfo>(tbl);
                        Item = items[0];
                       
                    }
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Item, _ex);
        }

        public void GetAll(Action<IEnumerable<Entity.Interfaces.ITerminalInfo>, Exception> Complete)
        {
            List<Entity.clsTerminalInfo> Items = new List<Entity.clsTerminalInfo>();
            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_TERMINAL");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsTerminalInfo>(tbl);
                    
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }

        public void Insert(Entity.Interfaces.ITerminalInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                if (item.TerminalID > 0)
                    using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                    {
                        System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TERMINAL", _cnn);
                        _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        _cmd.Parameters.AddWithValue("@Action", "POST");
                        _cmd.Parameters.AddWithValue("@TerminalID", item.TerminalID);
                        _cmd.Parameters.AddWithValue("@Name", item.Name);

                        _cmd.Parameters.AddWithValue("@Connection", item.Connection);
                        _cmd.Parameters.AddWithValue("@BaudRate", item.BaudRate);
                        _cmd.Parameters.AddWithValue("@IpAddress", string.IsNullOrEmpty(item.IpAddress) ? "" : item.IpAddress);
                        _cmd.Parameters.AddWithValue("@Port", item.Port);
                        _cmd.Parameters.AddWithValue("@CommType", item.CommType);
                        _cmd.Parameters.AddWithValue("@Status", item.Status);

                        _cmd.Parameters.AddWithValue("@ActiveKey", item.ActiveKey);
                        _cmd.Parameters.AddWithValue("@SerialNumber", item.SerialNumber);

                        _cmd.ExecuteNonQuery();
                    }
                else
                    _result.Errors.Add("ex", "Chưa nhập số máy");
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex1", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }

        public void Update(Entity.Interfaces.ITerminalInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TERMINAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@TerminalID", item.TerminalID);
                    _cmd.Parameters.AddWithValue("@Name", item.Name);

                    _cmd.Parameters.AddWithValue("@Connection", item.Connection);
                    _cmd.Parameters.AddWithValue("@BaudRate", item.BaudRate);
                    _cmd.Parameters.AddWithValue("@IpAddress", item.IpAddress);
                    _cmd.Parameters.AddWithValue("@Port", item.Port);
                    _cmd.Parameters.AddWithValue("@CommType", item.CommType);
                    _cmd.Parameters.AddWithValue("@Status", item.Status);

                    _cmd.Parameters.AddWithValue("@ActiveKey", item.ActiveKey);
                    _cmd.Parameters.AddWithValue("@SerialNumber", item.SerialNumber);

                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }

        public Interfaces.IResultOk ValidateInsert(Entity.Interfaces.ITerminalInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateUpdate(Entity.Interfaces.ITerminalInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateDelete(Entity.Interfaces.ITerminalInfo Item)
        {
            return new BaseResultOk();
        }


        public void GetPaging(int selectedPage, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.ITerminalInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
        }


        public void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.ITerminalInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
        }
    }


    //public class clsTerminal : Interfaces.ITerminal
    //{
    //    public event Interfaces.TerminalDataEventHandler LoadCompleted;

    //    public void GetAll()
    //    {
    //        List<Entity.Interfaces.ITerminalInfo> Items = new List<Entity.Interfaces.ITerminalInfo>();
    //        Exception _ex = null;
    //        try
    //        {
    //            using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
    //            {
    //                System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
    //                _cmd.CommandType = System.Data.CommandType.StoredProcedure;

    //                _cmd.Parameters.AddWithValue("@Columns", "*");
    //                _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_TERMINAL");
    //                _cmd.Parameters.AddWithValue("@WhereClause", "");

    //                System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
    //                System.Data.DataTable tbl = new System.Data.DataTable();
    //                _adapter.Fill(tbl);
    //                if (tbl != null && tbl.Rows.Count > 0)
    //                {
    //                    foreach (System.Data.DataRow row in tbl.Rows)
    //                    {
    //                        Entity.Interfaces.ITerminalInfo Info = new Entity.clsTerminalInfo();
    //                        Info.TerminalID = Convert.ToInt32(row["TerminalID"]);
    //                        Info.Name = row["Name"].ToString();
    //                        Info.Connection = row["Connection"].ToString();
    //                        Info.BaudRate = Convert.ToInt32(row["BaudRate"]);
    //                        Info.Port = Convert.ToInt32(row["Port"]);
    //                        if (!row.IsNull("LastTimeGetData"))
    //                            Info.LastTimeGetData = Convert.ToDateTime(row["LastTimeGetData"]);
    //                        Info.RecordReaded = Convert.ToInt32(row["RecordReaded"]);
    //                        Info.IpAddress = Convert.ToString(row["IpAddress"]);
    //                        Items.Add(Info);
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            _ex = new Exception(ex.Message);
    //        }

    //        if (LoadCompleted != null)
    //            LoadCompleted(this, new Interfaces.TerminalDataEventArgs() { TerminalCollection = Items, Ex = _ex });
            
    //    }

    //    public Entity.Interfaces.ITerminalInfo GetBy(int Id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public bool Update(Entity.Interfaces.ITerminalInfo Item)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public bool Delete(int Id)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
