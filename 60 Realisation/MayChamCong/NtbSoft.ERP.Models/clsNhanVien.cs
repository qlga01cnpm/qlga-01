﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.ERP.Entity.Interfaces;
using NtbSoft.ERP.Models.Interfaces;

namespace NtbSoft.ERP.Models
{
    public class clsNhanVienType
    {
        public System.Data.DataTable TYPE_DIC_NHANVIEN
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_NHANVIEN");
                _tbl.Columns.Add("Id", typeof(System.Double));

                _tbl.Columns.Add("MaNV", typeof(System.String));
                _tbl.Columns.Add("TenNV", typeof(System.String));
                _tbl.Columns.Add("MaCC", typeof(System.Int32));
                _tbl.Columns.Add("CardID", typeof(System.String));
                _tbl.Columns.Add("BirthDate", typeof(System.DateTime));
                _tbl.Columns.Add("BirthPlace", typeof(System.String));
                _tbl.Columns.Add("NguyenQuan", typeof(System.String));
                _tbl.Columns.Add("Sex", typeof(System.String));
                _tbl.Columns.Add("Address", typeof(System.String));
                _tbl.Columns.Add("CMND", typeof(System.String));

                _tbl.Columns.Add("NoiCap", typeof(System.String));
                _tbl.Columns.Add("PositionID", typeof(System.Int32));
                _tbl.Columns.Add("PositionName", typeof(System.String));
                _tbl.Columns.Add("Telephone", typeof(System.String));
                _tbl.Columns.Add("BeginningDate", typeof(System.DateTime));
                _tbl.Columns.Add("NgayKyHopDong", typeof(System.DateTime));
                _tbl.Columns.Add("StopWorkingday", typeof(System.DateTime));
                _tbl.Columns.Add("NgayDu18Tuoi", typeof(System.DateTime));
                _tbl.Columns.Add("IsVisible", typeof(System.Boolean));
                _tbl.Columns.Add("IsNghiViec", typeof(System.Boolean));

                _tbl.Columns.Add("LyDoNghiViec", typeof(System.String));
                _tbl.Columns.Add("NgayQuayLai", typeof(System.DateTime));
                _tbl.Columns.Add("SCHName", typeof(System.String));
                _tbl.Columns.Add("ShiftID", typeof(System.String));
                _tbl.Columns.Add("STT", typeof(System.Int32));
                _tbl.Columns.Add("IsShift", typeof(System.Int32));
                _tbl.Columns.Add("Privilege", typeof(System.Int32));
                _tbl.Columns.Add("Enabled", typeof(System.Boolean));
                _tbl.Columns.Add("Password", typeof(System.String));
                _tbl.Columns.Add("BackupNumber", typeof(System.Int32));
                _tbl.Columns.Add("TheBHXH", typeof(System.String));

                return _tbl;
            }
        }

    }


    public class clsNhanVienChangeDept : Interfaces.INhanVienChangeDept
    {
        public void Change(string MaNV, string DepId, DateTime ChangeDate, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_CHUYENBOPHAN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@ChangedDate", ChangeDate);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Change", ex.Message);
            }

            if (Complete != null) Complete(ok);
        }
    }
    public class clsNhanVienImport : Interfaces.INhanVienImport
    {
        /// <summary>
        /// Kiem tra ma nhan vien
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="Complete"></param>
        public void CheckMaNV(string MaNV, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool exits = false;
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Count(Id)");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN with(nolock)");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where MaNV='{0}'", MaNV));

                    int value = (int)_cmd.ExecuteScalar();
                    if (value > 0)
                        exits = true;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("CheckMaNV", ex.Message);
            }

            if (Complete != null) Complete(exits, ok);
        }

        public void CheckMaCC(string MaCC, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool exits = false;
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Count(Id)");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN with(nolock)");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where MaCC={0}", MaCC));

                    int value = (int)_cmd.ExecuteScalar();
                    if (value > 0)
                        exits = true;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("CheckMaCC", ex.Message);
            }

            if (Complete != null) Complete(exits, ok);
        }
        public void Import(System.Data.DataTable TypeNhanVien, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_IMPORT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@TYPE_NHANVIEN", TypeNhanVien);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Import", ex.Message);
            }

            if (Complete != null) Complete(ok);
        }

       
    }


    public class clsNhanVien:Interfaces.IBase<Entity.Interfaces.INhanVienInfo>
    {
        public void BatchDelete(IEnumerable<Entity.Interfaces.INhanVienInfo> Items, Action<Interfaces.IResultOk> Complete)
        {

        }

        public void BatchInsert(IEnumerable<Entity.Interfaces.INhanVienInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void BatchUpdate(IEnumerable<Entity.Interfaces.INhanVienInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            
        }

        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@Id", Id);
                    _cmd.Parameters.AddWithValue("@MaNV", "DD");
                    _cmd.Parameters.AddWithValue("@TenNV", "");
                    _cmd.Parameters.AddWithValue("@MaCC", 0);

                    _cmd.Parameters.AddWithValue("@CardID", "");
                    _cmd.Parameters.AddWithValue("@BirthDate",DateTime.Now);
                    _cmd.Parameters.AddWithValue("@BirthPlace", "");
                    _cmd.Parameters.AddWithValue("@Sex", "0");
                    _cmd.Parameters.AddWithValue("@Address", "");
                    _cmd.Parameters.AddWithValue("@NguyenQuan", "");
                    _cmd.Parameters.AddWithValue("@CMND", "");
                    _cmd.Parameters.AddWithValue("@NoiCap", "");

                    _cmd.Parameters.AddWithValue("@BeginningDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@StopWorkingday", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@SCHName", "");
                    _cmd.Parameters.AddWithValue("@ShiftID", 1);
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }

            if (Complete != null) Complete(_result);
        }

        public void GetBy(object Id, Action<Entity.Interfaces.INhanVienInfo, Exception> Complete)
        {
            
        }

        public void GetAll(Action<IEnumerable<Entity.Interfaces.INhanVienInfo>, Exception> Complete)
        {
            
        }

        public void GetByDepTenNV(int SelectedPage, int pageSize, string DepId, string TenNV, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhanVienInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            List<Entity.clsNhanVienInfo> Items = new List<Entity.clsNhanVienInfo>();
            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_BY_DEP_TENNV", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SelectedPage", SelectedPage);
                    _cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@TenNV", TenNV);
                    _cmd.Parameters.Add("@TotalPages", SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@TotalRecords", SqlDbType.Int);
                    _cmd.Parameters["@TotalRecords"].Direction = ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);

                    if (_cmd.Parameters["@TotalPages"].Value == null)
                        totalPages = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalPages"].Value.ToString(), out totalPages);
                    if (_cmd.Parameters["@TotalRecords"].Value == null)
                        totalRecords = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalRecords"].Value.ToString(), out totalRecords);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }

        public void GetByDepMaNV(int SelectedPage, int pageSize, string DepId, string MaNV, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhanVienInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            List<Entity.clsNhanVienInfo> Items = new List<Entity.clsNhanVienInfo>();
            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_BY_DEP_MANV", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SelectedPage", SelectedPage);
                    _cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.Add("@TotalPages", SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@TotalRecords", SqlDbType.Int);
                    _cmd.Parameters["@TotalRecords"].Direction = ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);

                    if (_cmd.Parameters["@TotalPages"].Value == null)
                        totalPages = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalPages"].Value.ToString(), out totalPages);
                    if (_cmd.Parameters["@TotalRecords"].Value == null)
                        totalRecords = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalRecords"].Value.ToString(), out totalRecords);
                    _adapter.Dispose();
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }

        public void Insert(Entity.Interfaces.INhanVienInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@Id", item.Id);
                    _cmd.Parameters.AddWithValue("@MaNV", item.MaNV);
                    _cmd.Parameters.AddWithValue("@TenNV", item.TenNV == null ? "" : item.TenNV);
                    _cmd.Parameters.AddWithValue("@MaCC", item.MaCC);

                    _cmd.Parameters.AddWithValue("@CardID", item.CardID == null ? "" : item.CardID);
                    _cmd.Parameters.AddWithValue("@BirthDate", item.BirthDate);
                    _cmd.Parameters.AddWithValue("@BirthPlace", item.BirthPlace == null ? "" : item.BirthPlace);
                    _cmd.Parameters.AddWithValue("@Sex", item.Sex);
                    _cmd.Parameters.AddWithValue("@Address", item.Address == null ? "" : item.Address);
                    _cmd.Parameters.AddWithValue("@NguyenQuan", item.NguyenQuan);
                    _cmd.Parameters.AddWithValue("@CMND", item.CMND);
                    _cmd.Parameters.AddWithValue("@NoiCap", item.NoiCap);

                    _cmd.Parameters.AddWithValue("@BeginningDate", Libs.clsCommon.CheckDateTime(item.BeginningDate));
                    _cmd.Parameters.AddWithValue("@StopWorkingday", Libs.clsCommon.CheckDateTime(item.StopWorkingday));
                    _cmd.Parameters.AddWithValue("@SCHName", item.SCHName == null ? "" : item.SCHName);
                    _cmd.Parameters.AddWithValue("@ShiftID", item.ShiftID);
                    _cmd.Parameters.AddWithValue("@DepID", item.DepID == null ? "" : item.DepID);
                   


                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }

        public void Update(Entity.Interfaces.INhanVienInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@Id", item.Id);
                    _cmd.Parameters.AddWithValue("@MaNV", item.MaNV);
                    _cmd.Parameters.AddWithValue("@TenNV", item.TenNV == null ? "" : item.TenNV);
                    _cmd.Parameters.AddWithValue("@MaCC", item.MaCC);

                    _cmd.Parameters.AddWithValue("@CardID", item.CardID == null ? "" : item.CardID);
                    _cmd.Parameters.AddWithValue("@BirthDate", item.BirthDate);
                    _cmd.Parameters.AddWithValue("@BirthPlace", item.BirthPlace == null ? "" : item.BirthPlace);
                    _cmd.Parameters.AddWithValue("@Sex", item.Sex);
                    _cmd.Parameters.AddWithValue("@Address", item.Address == null ? "" : item.Address);
                    _cmd.Parameters.AddWithValue("@NguyenQuan", item.NguyenQuan);
                    _cmd.Parameters.AddWithValue("@CMND", item.CMND);
                    _cmd.Parameters.AddWithValue("@NoiCap", item.NoiCap);
                    _cmd.Parameters.AddWithValue("@BeginningDate", Libs.clsCommon.CheckDateTime(item.BeginningDate));
                    _cmd.Parameters.AddWithValue("@StopWorkingday", Libs.clsCommon.CheckDateTime(item.StopWorkingday));
                    _cmd.Parameters.AddWithValue("@SCHName", item.SCHName == null ? "" : item.SCHName);
                    _cmd.Parameters.AddWithValue("@ShiftID", item.ShiftID);
                    _cmd.Parameters.AddWithValue("@DepID", item.DepID == null ? "" : item.DepID);
                    _cmd.Parameters.AddWithValue("@Password", item.Password == null ? "" : item.Password);
                    _cmd.Parameters.AddWithValue("@UserName", item.UserName == null ? "" : item.UserName);

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }

  
        public Interfaces.IResultOk ValidateInsert(Entity.Interfaces.INhanVienInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateUpdate(Entity.Interfaces.INhanVienInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateDelete(Entity.Interfaces.INhanVienInfo Item)
        {
            return new BaseResultOk();
        }


        public void GetPaging(int selectedPage, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhanVienInfo>, Exception> Complete)
        {
            throw new NotImplementedException();
        }

        public void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhanVienInfo>, Exception> Complete)
        {
            throw new NotImplementedException();
        }
    }


    public class clsNhanVienDaXoa : Models.Interfaces.INhanVienDaXoa
    {
        public void GetAll(Action<List<Entity.Interfaces.INhanVienInfo>, Interfaces.IResultOk> Complete)
        {
            List<Entity.Interfaces.INhanVienInfo> Items = new List<Entity.Interfaces.INhanVienInfo>();
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_DAXOA", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@MaNV", "0");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var temps = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);
                        Items.AddRange(temps);
                    }
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, ok);
        }

        public void Modifiy(Libs.ApiAction action, string MaNV, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_DAXOA", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", action.ToString());
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Modifiy", ex.Message);
            }
            if (Complete != null) Complete( ok);
            
        }
    }

    public class clsNhanVienException : Models.Interfaces.INhanVienException
    {
        public void GetAll(Action<List<Entity.Interfaces.INhanVienInfo>, Interfaces.IResultOk> Complete)
        {
            List<Entity.Interfaces.INhanVienInfo> Items = new List<Entity.Interfaces.INhanVienInfo>();
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_EXCEPTION", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@MaNV", "0");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var temps = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);
                        Items.AddRange(temps);
                    }
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, ok);
        }

        public void Modifiy(Libs.ApiAction action, string MaNV, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_EXCEPTION", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", action.ToString());
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Modifiy", ex.Message);
            }
            if (Complete != null) Complete(ok);

        }
    }

    public class clsNhanVienNghiViec : Models.Interfaces.INhanVienNghiViec
    {
        public void GetAll(string ValueSearch, DateTime FromDate, DateTime ToDate, Action<List<Entity.Interfaces.INhanVienInfo>, Interfaces.IResultOk> Complete)
        {
            List<Entity.Interfaces.INhanVienInfo> Items = new List<Entity.Interfaces.INhanVienInfo>();
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_CHONGHI", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@FromDate", FromDate);
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate);
                    _cmd.Parameters.AddWithValue("@ValueSearch", ValueSearch ?? "");
                    _cmd.Parameters.AddWithValue("@MaNV", "");
                    _cmd.Parameters.AddWithValue("@StopWorkingday", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@LyDoNghi", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var temps = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);
                        Items.AddRange(temps);
                    }
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, ok);
        }

        public void ChoNghiViec(Libs.ApiAction action, string MaNV, string LyDoNghi, DateTime StopWorkingday, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_CHONGHI", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@FromDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ToDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ValueSearch", "");
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@StopWorkingday", StopWorkingday);
                    _cmd.Parameters.AddWithValue("@LyDoNghi", LyDoNghi);

                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("ChoNghiViec", ex.Message);
            }
            if (Complete != null) Complete( ok);
        }

        public void PhucHoi(Libs.ApiAction action, string MaNV, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NHANVIEN_CHONGHI", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@FromDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ToDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ValueSearch", "");
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@StopWorkingday", DateTime.Now.AddDays(18250));
                    _cmd.Parameters.AddWithValue("@LyDoNghi", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("PhucHoi", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
