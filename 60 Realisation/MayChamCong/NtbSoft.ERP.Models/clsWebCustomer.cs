﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsWebCustomer : Entity.Interfaces.IWebCustomer
    {
        Entity.Interfaces.IDatabase _db;
        public clsWebCustomer(Entity.Interfaces.IDatabase pDatabase)
        {
            this._db = pDatabase;
        }
        public List<Entity.Interfaces.IWebCustomerInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Save(List<Entity.Interfaces.IWebCustomerInfo> Items)
        {
            if (Items == null) throw new ArgumentNullException("Items");
            if (this._db == null) throw new Exception("IDatabase is null");
            _db.ExecuteSqlCommand();
            return true;
        }

        public bool Delete(string Id)
        {
            throw new NotImplementedException();
        }


        public Entity.Interfaces.IWebCustomerInfo GetBy(string Id)
        {
            throw new NotImplementedException();
        }
    }
}
