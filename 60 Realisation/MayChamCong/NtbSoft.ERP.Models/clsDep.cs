﻿using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    /// <summary>
    /// Phòng ban
    /// </summary>
    public class clsDep
    {
        public Entity.clsTreeDepInfo GetTreeNodeBy(string Id)
        {
            if (string.IsNullOrEmpty(Id)) return null;
            //System.Data.SqlClient.SqlDataReader _reader = null;
            //System.Data.SqlClient.SqlConnection _cnn = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.SYS_DEPARTMENTS");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where DepID='{0}'", Id));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        foreach (System.Data.DataRow row in tbl.Rows)
                        {
                            Entity.clsTreeDepInfo treeNode = new Entity.clsTreeDepInfo();
                            treeNode.id = row["DepId"].ToString().ToUpper();
                            treeNode.Name = row["DepName"].ToString();
                            treeNode.parentId = row["ParentId"].ToString();
                            treeNode.text = row["DepName"].ToString();
                            treeNode.IsChain = Convert.ToBoolean(row["IsChain"]);

                            treeNode.IsChain = Convert.ToBoolean(row["IsChain"]);
                            treeNode.IsChainSew = Convert.ToBoolean(row["IsChainSew"]);
                            treeNode.IsChainCut = Convert.ToBoolean(row["IsChainCut"]);
                            treeNode.IsChainPackage = Convert.ToBoolean(row["IsChainPackage"]);
                            treeNode.IsWarehouse = Convert.ToBoolean(row["IsWarehouse"]);

                            treeNode.ContextMenuId = row["ContextMenuId"].ToString();
                            treeNode.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();
                            return treeNode;
                        }
                    }

                    return null;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsDep.GetTreeNodes: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsTreeDepCollection GetTreeNodes()
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsTreeDepCollection _objCollection = new Entity.clsTreeDepCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select",_cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.SYS_DEPARTMENTS with(nolock)");
                    _cmd.Parameters.AddWithValue("@WhereClause", "order by Sort asc ");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    DataTable tbl = new DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        DataRow[] Parents = tbl.Select("[ParentID] is null");

                        foreach (DataRow row in Parents)
                        {
                            Entity.clsTreeDepInfo treeNode = new Entity.clsTreeDepInfo();
                            treeNode.id = row["DepId"].ToString().ToUpper();
                            treeNode.Name = row["DepName"].ToString();
                            treeNode.parentId = row["ParentId"].ToString();
                            treeNode.text = row["DepName"].ToString();
                            treeNode.IsChain = Convert.ToBoolean(row["IsChain"]);
                            
                            treeNode.IsChain = Convert.ToBoolean(row["IsChain"]);
                            treeNode.IsChainSew = Convert.ToBoolean(row["IsChainSew"]);
                            treeNode.IsChainCut = Convert.ToBoolean(row["IsChainCut"]);
                            treeNode.IsChainPackage = Convert.ToBoolean(row["IsChainPackage"]);
                            treeNode.IsWarehouse = Convert.ToBoolean(row["IsWarehouse"]);

                            treeNode.Sort = Convert.ToInt32(row["Sort"]);
                            treeNode.ContextMenuId = row["ContextMenuId"].ToString();
                            treeNode.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();
                            addChildNode(ref treeNode, tbl);

                            _objCollection.Add(treeNode);
                        }
                    }
                    _adapter.Dispose();
                    _adapter = null;
                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsDep.GetTreeNodes: " + ex.Message);
            }
            finally
            {
            }
        }

        private void addChildNode(ref Entity.clsTreeDepInfo node, System.Data.DataTable tbl)
        {
            foreach (System.Data.DataRow row in tbl.Rows)
            {
                if (node.id.Equals(row["ParentId"].ToString()))
                {
                    Entity.clsTreeDepInfo tn = new Entity.clsTreeDepInfo();
                    tn.id = row["DepId"].ToString().ToUpper();
                    tn.Name = row["DepName"].ToString();
                    tn.parentId = row["ParentId"].ToString();
                    tn.text = row["DepName"].ToString();
                    tn.IsChain = Convert.ToBoolean(row["IsChain"]);

                    tn.IsChain = Convert.ToBoolean(row["IsChain"]);
                    tn.IsChainSew = Convert.ToBoolean(row["IsChainSew"]);
                    tn.IsChainCut = Convert.ToBoolean(row["IsChainCut"]);
                    tn.IsChainPackage = Convert.ToBoolean(row["IsChainPackage"]);
                    tn.IsWarehouse = Convert.ToBoolean(row["IsWarehouse"]);

                    tn.Sort = Convert.ToInt32(row["Sort"]);
                    tn.ContextMenuId = row["ContextMenuId"].ToString();
                    tn.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();

                    if (node.children == null)
                        node.children = new List<Entity.clsTreeDepInfo>();
                    node.children.Add(tn);

                    //Goi de quy
                    addChildNode(ref tn, tbl);
                }
            }
        }

        public bool Update(ApiAction action, string DepID, string DepName, string ParentID,
           string ContextMenuId, string ContextMenuIdFile, 
            bool IsChain, bool IsChainSew,bool IsChainCut,bool IsChainPackage,bool IsWarehouse, int Sort)
        {
            if (string.IsNullOrWhiteSpace(DepID))
                throw new Exception("Chưa nhập mã bộ phận!");
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_SYS_DEPARTMENT", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", action.ToString());
                    cmd.Parameters.AddWithValue("@DepID", DepID.ToUpper());
                    cmd.Parameters.AddWithValue("@DepName", DepName == null ? "" : DepName.Trim());
                    cmd.Parameters.AddWithValue("@ParentID", ParentID);
                    cmd.Parameters.AddWithValue("@ContextMenuId", ContextMenuId == null ? "" : ContextMenuId);
                    cmd.Parameters.AddWithValue("@ContextMenuIdFile", ContextMenuIdFile == null ? "" : ContextMenuIdFile);
                    cmd.Parameters.AddWithValue("@IsChain", IsChain);
                    cmd.Parameters.AddWithValue("@IsChainSew", IsChainSew);
                    cmd.Parameters.AddWithValue("@IsChainCut", IsChainCut);
                    cmd.Parameters.AddWithValue("@IsChainPackage", IsChainPackage);
                    cmd.Parameters.AddWithValue("@IsWarehouse", IsWarehouse);

                    cmd.Parameters.AddWithValue("@Sort", Sort);
                    /*** ***/
                    //cmd.Parameters.Add("@OutNewName", System.Data.SqlDbType.NVarChar, 150);
                    //cmd.Parameters["@OutNewName"].Direction = System.Data.ParameterDirection.Output;

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsDep.Update: " + ex.Message);
            }
        }
    }
}
