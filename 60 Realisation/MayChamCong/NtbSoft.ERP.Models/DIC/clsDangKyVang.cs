﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsDangKyVangType
    {
        public System.Data.DataTable TYPE_DANGKY_VANG
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("DIC_DANGKY_VANG");
                _tbl.Columns.Add("sMonth", typeof(System.Int32));
                _tbl.Columns.Add("sYear", typeof(System.Int32));

                _tbl.Columns.Add("eID", typeof(System.String));
                //_tbl.Columns.Add("TenNV", typeof(System.String));

                _tbl.Columns.Add("D1", typeof(System.String));
                _tbl.Columns.Add("D2", typeof(System.String));
                _tbl.Columns.Add("D3", typeof(System.String));
                _tbl.Columns.Add("D4", typeof(System.String));
                _tbl.Columns.Add("D5", typeof(System.String));
                _tbl.Columns.Add("D6", typeof(System.String));
                _tbl.Columns.Add("D7", typeof(System.String));
                _tbl.Columns.Add("D8", typeof(System.String));
                _tbl.Columns.Add("D9", typeof(System.String));
                _tbl.Columns.Add("D10", typeof(System.String));

                _tbl.Columns.Add("D11", typeof(System.String));
                _tbl.Columns.Add("D12", typeof(System.String));
                _tbl.Columns.Add("D13", typeof(System.String));
                _tbl.Columns.Add("D14", typeof(System.String));
                _tbl.Columns.Add("D15", typeof(System.String));
                _tbl.Columns.Add("D16", typeof(System.String));
                _tbl.Columns.Add("D17", typeof(System.String));
                _tbl.Columns.Add("D18", typeof(System.String));
                _tbl.Columns.Add("D19", typeof(System.String));
                _tbl.Columns.Add("D20", typeof(System.String));

                _tbl.Columns.Add("D21", typeof(System.String));
                _tbl.Columns.Add("D22", typeof(System.String));
                _tbl.Columns.Add("D23", typeof(System.String));
                _tbl.Columns.Add("D24", typeof(System.String));
                _tbl.Columns.Add("D25", typeof(System.String));
                _tbl.Columns.Add("D26", typeof(System.String));
                _tbl.Columns.Add("D27", typeof(System.String));
                _tbl.Columns.Add("D28", typeof(System.String));
                _tbl.Columns.Add("D29", typeof(System.String));
                _tbl.Columns.Add("D30", typeof(System.String));

                _tbl.Columns.Add("D31", typeof(System.String));

                return _tbl;
            }
        }

        public void SetValue(int Month, int Year, Entity.Interfaces.IDangKyVangInfo Item, ref System.Data.DataTable TYPE_DANGKY_VANG)
        {
            System.Data.DataRow newRow = TYPE_DANGKY_VANG.NewRow();
            newRow["sMonth"] = Month;
            newRow["sYear"] = Year;
            newRow["eID"] = Item.eID;

            newRow["D1"] = Item.D1 == null ? "" : Item.D1;
            newRow["D2"] = Item.D2 == null ? "" : Item.D2;
            newRow["D3"] = Item.D3 == null ? "" : Item.D3;
            newRow["D4"] = Item.D4 == null ? "" : Item.D4;
            newRow["D5"] = Item.D5 == null ? "" : Item.D5;
            newRow["D6"] = Item.D6 == null ? "" : Item.D6;
            newRow["D7"] = Item.D7 == null ? "" : Item.D7;
            newRow["D8"] = Item.D8 == null ? "" : Item.D8;
            newRow["D9"] = Item.D9 == null ? "" : Item.D9;
            newRow["D10"] = Item.D10 == null ? "" : Item.D10;

            newRow["D11"] = Item.D11 == null ? "" : Item.D11;
            newRow["D12"] = Item.D12 == null ? "" : Item.D12;
            newRow["D13"] = Item.D13 == null ? "" : Item.D13;
            newRow["D14"] = Item.D14 == null ? "" : Item.D14;
            newRow["D15"] = Item.D15 == null ? "" : Item.D15;
            newRow["D16"] = Item.D16 == null ? "" : Item.D16;
            newRow["D17"] = Item.D17 == null ? "" : Item.D17;
            newRow["D18"] = Item.D18 == null ? "" : Item.D18;
            newRow["D19"] = Item.D19 == null ? "" : Item.D19;
            newRow["D20"] = Item.D20 == null ? "" : Item.D20;

            newRow["D21"] = Item.D21 == null ? "" : Item.D21;
            newRow["D22"] = Item.D22 == null ? "" : Item.D22;
            newRow["D23"] = Item.D23 == null ? "" : Item.D23;
            newRow["D24"] = Item.D24 == null ? "" : Item.D24;
            newRow["D25"] = Item.D25 == null ? "" : Item.D25;
            newRow["D26"] = Item.D26 == null ? "" : Item.D26;
            newRow["D27"] = Item.D27 == null ? "" : Item.D27;
            newRow["D28"] = Item.D28 == null ? "" : Item.D28;
            newRow["D29"] = Item.D29 == null ? "" : Item.D29;
            newRow["D30"] = Item.D30 == null ? "" : Item.D30;
            newRow["D31"] = Item.D31 == null ? "" : Item.D31;

            TYPE_DANGKY_VANG.Rows.Add(newRow);
        }
    }
    public class clsDangKyVang:Interfaces.IDangKyVang
    {
        public void DangKyVang(int Month, int Year, List<Entity.Interfaces.IDangKyVangInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                clsDangKyVangType clsType = new clsDangKyVangType();
                System.Data.DataTable type = clsType.TYPE_DANGKY_VANG;
                foreach (var item in Items)
                    clsType.SetValue(Month,Year, item, ref type);
                
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_VANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@Flag", 0);
                    _cmd.Parameters.AddWithValue("@TYPE_DANGKY_VANG", type);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("DangKyVang", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
        public void DangKyVang(List<Entity.Interfaces.IDangKyVangInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                clsDangKyVangType clsType = new clsDangKyVangType();
                System.Data.DataTable type = clsType.TYPE_DANGKY_VANG;
                foreach (var item in Items)
                    clsType.SetValue(item.sMonth, item.sYear, item, ref type);

                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_VANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@Flag", 1);
                    _cmd.Parameters.AddWithValue("@TYPE_DANGKY_VANG", type);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("DangKyVang", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        public void GetBy(int Month,int Year, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyVangInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyVangInfo> temps = new List<Entity.Interfaces.IDangKyVangInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_VANG_GETBY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyVangInfo>(tbl);
                        temps.AddRange(Items);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void GetBy(int Month, int Year, string MaNV, Action<Entity.Interfaces.IDangKyVangInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.Interfaces.IDangKyVangInfo temp = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_VANG_GETBY_MANV", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyVangInfo>(tbl);
                        temp = Items[0];
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temp, ok);
        }
    }
}
