﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsDangKyCheDo : Interfaces.IDangKyCheDo
    {
        public void GetAll(string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyCheDoInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyCheDoInfo> temps = new List<Entity.Interfaces.IDangKyCheDoInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_CHEDO", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@ID", 1);
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@TuNgay", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ToiNgay", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ShiftID", "");
                    _cmd.Parameters.AddWithValue("@LyDo", "");
                    _cmd.Parameters.AddWithValue("@SoNgay", 0);
                    _cmd.Parameters.AddWithValue("@ThoiGian", 0);
                    _cmd.Parameters.AddWithValue("@MaCheDo", 0);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyCheDoInfo>(tbl);
                        temps.AddRange(Items);
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void Update(Libs.ApiAction action, Entity.Interfaces.IDangKyCheDoInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_CHEDO", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", action.ToString());
                    _cmd.Parameters.AddWithValue("@ID", Item.ID);
                    _cmd.Parameters.AddWithValue("@DepId", "");
                    _cmd.Parameters.AddWithValue("@MaNV", Item.MaNV);
                    _cmd.Parameters.AddWithValue("@TuNgay", Item.TuNgay);
                    _cmd.Parameters.AddWithValue("@ToiNgay", Item.ToiNgay);
                    _cmd.Parameters.AddWithValue("@ShiftID", Item.ShiftID);
                    _cmd.Parameters.AddWithValue("@LyDo", Item.LyDo);
                    _cmd.Parameters.AddWithValue("@SoNgay", Item.SoNgay);
                    _cmd.Parameters.AddWithValue("@ThoiGian", Item.ThoiGian);
                    _cmd.Parameters.AddWithValue("@MaCheDo", Item.MaCheDo);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add(action.ToString() + ".", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }


        public void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.clsShiftInfo shiftInfo = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_CHEDO_GETSHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@WorkingDay", WorkingDay);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(tbl);
                        shiftInfo = Items[0];
                        shiftInfo.IsThaiSan = true;
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetShift", ex.Message);
            }
            if (Complete != null) Complete(shiftInfo, ok);
        }

        public void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo, string, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.clsShiftInfo shiftInfo = null;
            string kyHieu = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_CHEDO_GETSHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@WorkingDay", WorkingDay);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataSet ds = new System.Data.DataSet();
                    _adapter.Fill(ds);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(ds.Tables[0]);
                        shiftInfo = Items[0];
                        shiftInfo.IsThaiSan = true;
                    }
                    if (ds != null && ds.Tables[1].Rows.Count > 0)
                        kyHieu = ds.Tables[1].Rows[0]["KyHieu"].ToString();

                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetShift", ex.Message);
            }
            if (Complete != null) Complete(shiftInfo, kyHieu, ok);
        }

        public bool CheckIsRegisLeave(string dateYMD, int eID)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    // 1. Kiểm tra đăng ký phép
                    string cmdText = string.Format(@"DECLARE	@Date	DATETIME = '{0}',
		                                                        @MaNv	INT = {1}

                                                        DECLARE	@colVang NVARCHAR(max) = '',
		                                                        @sql NVARCHAR(MAX)

                                                        SELECT	@colVang = @colVang + QUOTENAME('D'+ CONVERT(VARCHAR(2), NUM)) +','
                                                        FROM	dbo.FTableInt(31)

                                                        SET @colVang = LEFT(@colVang,LEN(@colVang) - 1) 
	
                                                        IF OBJECT_ID('TEMPDB.DBO.##UNPIVOTVANG') IS NOT NULL
	                                                        BEGIN
	                                                            SET	@SQL = N'DROP TABLE ##UNPIVOTVANG'
	                                                            EXEC (@SQL);
	                                                        END
	
                                                        SET	@SQL = N'SELECT MANV, '''+CAST(YEAR(@Date) AS VARCHAR(4))+''' + ''-'+CAST(MONTH(@Date) AS VARCHAR(2))+'-'' + SUBSTRING(DATE, 2, LEN(DATE)) DATE, LOAIPHEP
			                                                            INTO	##UNPIVOTVANG
			                                                            FROM   (SELECT EID MANV, '+@colVang+'  
					                                                            FROM	DIC_DANGKY_VANG 
					                                                            WHERE	SYEAR = '''+CAST(Year(@Date) AS VARCHAR(4))+'''
					                                                            AND	SMONTH = '''+CAST(MONTH(@Date) AS VARCHAR(4))+'''
					                                                            AND	EID = '+CAST(@MaNv AS VARCHAR(24))+') P  
			                                                            UNPIVOT  
			                                                            (LOAIPHEP FOR DATE IN ('+@colVang+')  
			                                                            )AS UNPVT;'
                                                        EXEC (@SQL)
 
                                                        SELECT	* FROM ##UNPIVOTVANG 
                                                        WHERE	DATE = @DATE
                                                        AND		ISNULL(LOAIPHEP, '') <> ''
                                                        AND		LOAIPHEP NOT LIKE '%/2%'", dateYMD, eID);
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;

                    System.Data.DataTable tbl = new System.Data.DataTable();
                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        return true;
                    }

                    // 2. kiểm tra đăng ký chế độ
                    cmdText = string.Format(@"SELECT	* 
                                                FROM	dbo.DIC_DANGKY_CHEDO
                                                WHERE	MaNV = {1}
                                                AND		'{0}' BETWEEN TuNgay AND ToiNgay
                                                AND     MaCheDo = 4", dateYMD, eID);
                    _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;

                    tbl = new System.Data.DataTable();
                    _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        return true;
                    }

                    // 3. kiểm tra ngày nghỉ lễ
                    cmdText = string.Format(@"SELECT * FROM dbo.DIC_NGAYLE WHERE
                                                Ngay = '{0}' and Active = 1", dateYMD);
                    _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;

                    tbl = new System.Data.DataTable();
                    _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckIsLeaveTS(string dateYMD, int eID)
        {
            using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
            {
                string cmdText = string.Format(@"SELECT	* 
                                                FROM	dbo.DIC_DANGKY_CHEDO
                                                WHERE	MaNV = {1}
                                                AND		'{0}' BETWEEN TuNgay AND ToiNgay
                                                AND     MaCheDo = 4", dateYMD, eID);
                System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                _cmd.CommandType = System.Data.CommandType.Text;

                System.Data.DataTable tbl = new System.Data.DataTable();
                System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                _adapter.Fill(tbl);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
