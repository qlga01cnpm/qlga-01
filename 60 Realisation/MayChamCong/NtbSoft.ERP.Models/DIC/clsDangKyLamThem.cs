﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsDangKyLamThem : Interfaces.IDangKyLamThem
    {
        public void GetList(DateTime TuNgay, DateTime DenNgay, string eID,
            Action<List<Entity.Interfaces.IDangKyLamThemInfo>,
            Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyLamThemInfo> temps = new List<Entity.Interfaces.IDangKyLamThemInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    string mSql = string.Format(@"select * from dbo.DIC_DANGKY_LAMTHEM A
                                                where CONVERT(date,A.NgayDangKy,121) between '{0}' and '{1}'
                                                and MaNV = '{2}'",
                                                TuNgay.ToString("yyyy/MM/dd"),
                                                DenNgay.ToString("yyyy/MM/dd"), eID);
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(mSql, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyLamThemInfo>(tbl);
                        temps.AddRange(Items);
                    }
                    _cmd.Dispose();
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetList", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void GetAll(DateTime TuNgay, DateTime DenNgay, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyLamThemInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyLamThemInfo> temps = new List<Entity.Interfaces.IDangKyLamThemInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_LAMTHEM", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@ID", 1);
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@TuNgay", TuNgay.ToString(Libs.clsConsts.FormatYMD));
                    _cmd.Parameters.AddWithValue("@DenNgay", DenNgay.ToString(Libs.clsConsts.FormatYMD));
                    _cmd.Parameters.AddWithValue("@TuGio", "");
                    _cmd.Parameters.AddWithValue("@DenGio", "");
                    _cmd.Parameters.AddWithValue("@SoGio", 0);

                    System.Data.SqlClient.SqlDataAdapter _adapter
                        = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyLamThemInfo>(tbl);
                        temps.AddRange(Items);
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void Update(Libs.ApiAction action, Entity.Interfaces.IDangKyLamThemInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_LAMTHEM", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", action.ToString());
                    _cmd.Parameters.AddWithValue("@ID", Item.ID);
                    _cmd.Parameters.AddWithValue("@DepId", "");
                    _cmd.Parameters.AddWithValue("@MaNV", Item.MaNV ?? "");
                    _cmd.Parameters.AddWithValue("@TuNgay", Item.NgayDangKy.ToString(Libs.clsConsts.FormatYMD));
                    _cmd.Parameters.AddWithValue("@DenNgay", Item.DenNgay);
                    _cmd.Parameters.AddWithValue("@TuGio", Item.TuGio ?? "");
                    _cmd.Parameters.AddWithValue("@DenGio", Item.DenGio ?? "");
                    _cmd.Parameters.AddWithValue("@SoGio", Item.SoGio);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add(action.ToString() + ".", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
