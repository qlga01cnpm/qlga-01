﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{

    public class clsDangKyChanGioNgay : Interfaces.IDangKyChanGio
    {
        public void DangKy(System.Data.DataTable TYPE_DANGKY_CHANGIO, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_CHANGIO_NGAY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@TYPE", TYPE_DANGKY_CHANGIO);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("DangKyChanGio", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        //private Task<int> SumAsync(int a, int b)
        //{
        //    //return Task.Factory.StartNew(() => Sum(a, b));
        //}

        private int Sum(DateTime FromDate,DateTime ToDate)
        {
            //var calculatingTime = _rnd.Next(3000);
            //Thread.Sleep(1000000);

            return 0;
        }


        public void GetBy(int Month, int Year, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyChanGioInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyChanGioInfo> temps = new List<Entity.Interfaces.IDangKyChanGioInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    DateTime mFirstDay = new DateTime(Year, Month, 1);
                    DateTime mLasDayOfMonth = Libs.clsCommon.LastDayOfMonth(Month, Year);
                    string mColumns = "";
                    while (mFirstDay<=mLasDayOfMonth)
                    {
                        mColumns += string.Format(@",(
                                                        select top 1 A1.GioVe from dbo.MCC_DANGKY_CHANGIO_NGAY A1 with(nolock)
                                                        where A1.eID=A.Id and DAY(A1.NgayChan)={0} and MONTH(A1.NgayChan)={1} and YEAR(A1.NgayChan)={2}  
                                                    ) as D{0}", mFirstDay.Day,mFirstDay.Month,mFirstDay.Year);

                        mFirstDay = mFirstDay.AddDays(1); 
                    }
                    string mSql = string.Format(@"select  A.Id as eID, A.MaNV,A.TenNV
                                                {0}

                                                from dbo.fncNHANVIENBYDEP('{1}','') A ", mColumns, DepId);
                    if (!string.IsNullOrEmpty(MaNV))
                    {
                        mSql = string.Format(@"select  A.Id as eID, A.MaNV,A.TenNV
                                                {0}

                                                from dbo.fncNHANVIENBYDEPMANV('{1}','{2}') A ", mColumns, DepId,MaNV);
                    }
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(mSql, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;
                    //_cmd.Parameters.AddWithValue("@DepId", DepId);
                    //_cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    //_cmd.Parameters.AddWithValue("@Month", Month);
                    //_cmd.Parameters.AddWithValue("@Year", Year);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyChanGioInfo>(tbl);
                        temps.AddRange(Items);
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void GetBy(int Month, int Year, string MaNV, Action<Entity.Interfaces.IDangKyChanGioInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.Interfaces.IDangKyChanGioInfo temp = null;
            try
            {
                ////using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                ////{
                ////    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_VANG_GETBY_MANV", _cnn);
                ////    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                ////    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                ////    _cmd.Parameters.AddWithValue("@Month", Month);
                ////    _cmd.Parameters.AddWithValue("@Year", Year);

                ////    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                ////    System.Data.DataTable tbl = new System.Data.DataTable();
                ////    _adapter.Fill(tbl);
                ////    if (tbl != null && tbl.Rows.Count > 0)
                ////    {
                ////        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyVangInfo>(tbl);
                ////        temp = Items[0];
                ////    }

                ////}
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temp, ok);
        }
    }

    public class clsDangKyChanGioNgayType
    {
        public System.Data.DataTable TYPE_DANGKY_CHANGIO
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DANGKY_CHANGIO_NGAY");
                _tbl.Columns.Add("AutoID", typeof(System.Double));
                _tbl.Columns.Add("NgayChan", typeof(System.DateTime));
                _tbl.Columns.Add("eID", typeof(System.String));

                _tbl.Columns.Add("GioVe", typeof(System.String));
                _tbl.Columns.Add("SoPhut", typeof(System.String));

                return _tbl;
            }
        }

    }
}
