﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsLyDoVang:Interfaces.ILyDoVang
    {
        public void GetLyDo(Action<JData.DataComboBoxInfo[], Models.Interfaces.IResultOk> Complete)
        {
            JData.DataComboBoxInfo[] temps = new JData.DataComboBoxInfo[1] { new JData.DataComboBoxInfo() { id = "", name = "--" } };
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            System.Data.SqlClient.SqlDataReader reader = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_LYDOVANG");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");
                    reader = _cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Array.Resize(ref temps, (temps.Length + 1));
                        temps[temps.Length - 1] = new JData.DataComboBoxInfo() { id = reader["MaSo"].ToString(), name = reader["LyDo"].ToString() };
                    }
                    

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetLyDo", ex.Message);
            }
            finally { if (reader != null) { reader.Dispose(); reader = null; } }
            if (Complete != null) Complete(temps, ok);
        }

        public void GetBy(Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Models.Interfaces.IResultOk> Complete)
        {
            //sp_DIC_LYDOVANG
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items = new List<Entity.Interfaces.ILyDoVangInfo>();

            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_LYDOVANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@Maso", "");
                    _cmd.Parameters.AddWithValue("@LyDo", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsLyDoVangInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(Items, _ok);
        }


        public void Modify(Libs.ApiAction Action, Entity.Interfaces.ILyDoVangInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items = new List<Entity.Interfaces.ILyDoVangInfo>();

            if (Action == Libs.ApiAction.GET) return;

            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_LYDOVANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Action.ToString());
                    _cmd.Parameters.AddWithValue("@Id", Item.Id);
                    _cmd.Parameters.AddWithValue("@Maso", Item.MaSo);
                    _cmd.Parameters.AddWithValue("@LyDo", Item.LyDo);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }
    }
}
