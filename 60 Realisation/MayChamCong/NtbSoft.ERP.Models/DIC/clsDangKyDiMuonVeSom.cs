﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsDangKyDiMuonVeSomType
    {
        public System.Data.DataTable TYPE_DANGKY_DIMUONVESOM
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DANGKY_DIMUONVESOM");
                _tbl.Columns.Add("sMonth", typeof(System.Int32));
                _tbl.Columns.Add("sYear", typeof(System.Int32));

                _tbl.Columns.Add("eID", typeof(System.String));
                
                #region M
                _tbl.Columns.Add("M1", typeof(System.String));
                _tbl.Columns.Add("M2", typeof(System.String));
                _tbl.Columns.Add("M3", typeof(System.String));
                _tbl.Columns.Add("M4", typeof(System.String));
                _tbl.Columns.Add("M5", typeof(System.String));
                _tbl.Columns.Add("M6", typeof(System.String));
                _tbl.Columns.Add("M7", typeof(System.String));
                _tbl.Columns.Add("M8", typeof(System.String));
                _tbl.Columns.Add("M9", typeof(System.String));
                _tbl.Columns.Add("M10", typeof(System.String));

                _tbl.Columns.Add("M11", typeof(System.String));
                _tbl.Columns.Add("M12", typeof(System.String));
                _tbl.Columns.Add("M13", typeof(System.String));
                _tbl.Columns.Add("M14", typeof(System.String));
                _tbl.Columns.Add("M15", typeof(System.String));
                _tbl.Columns.Add("M16", typeof(System.String));
                _tbl.Columns.Add("M17", typeof(System.String));
                _tbl.Columns.Add("M18", typeof(System.String));
                _tbl.Columns.Add("M19", typeof(System.String));
                _tbl.Columns.Add("M20", typeof(System.String));

                _tbl.Columns.Add("M21", typeof(System.String));
                _tbl.Columns.Add("M22", typeof(System.String));
                _tbl.Columns.Add("M23", typeof(System.String));
                _tbl.Columns.Add("M24", typeof(System.String));
                _tbl.Columns.Add("M25", typeof(System.String));
                _tbl.Columns.Add("M26", typeof(System.String));
                _tbl.Columns.Add("M27", typeof(System.String));
                _tbl.Columns.Add("M28", typeof(System.String));
                _tbl.Columns.Add("M29", typeof(System.String));
                _tbl.Columns.Add("M30", typeof(System.String));
                _tbl.Columns.Add("M31", typeof(System.String));
                #endregion

                #region S
                _tbl.Columns.Add("S1", typeof(System.String));
                _tbl.Columns.Add("S2", typeof(System.String));
                _tbl.Columns.Add("S3", typeof(System.String));
                _tbl.Columns.Add("S4", typeof(System.String));
                _tbl.Columns.Add("S5", typeof(System.String));
                _tbl.Columns.Add("S6", typeof(System.String));
                _tbl.Columns.Add("S7", typeof(System.String));
                _tbl.Columns.Add("S8", typeof(System.String));
                _tbl.Columns.Add("S9", typeof(System.String));
                _tbl.Columns.Add("S10", typeof(System.String));

                _tbl.Columns.Add("S11", typeof(System.String));
                _tbl.Columns.Add("S12", typeof(System.String));
                _tbl.Columns.Add("S13", typeof(System.String));
                _tbl.Columns.Add("S14", typeof(System.String));
                _tbl.Columns.Add("S15", typeof(System.String));
                _tbl.Columns.Add("S16", typeof(System.String));
                _tbl.Columns.Add("S17", typeof(System.String));
                _tbl.Columns.Add("S18", typeof(System.String));
                _tbl.Columns.Add("S19", typeof(System.String));
                _tbl.Columns.Add("S20", typeof(System.String));

                _tbl.Columns.Add("S21", typeof(System.String));
                _tbl.Columns.Add("S22", typeof(System.String));
                _tbl.Columns.Add("S23", typeof(System.String));
                _tbl.Columns.Add("S24", typeof(System.String));
                _tbl.Columns.Add("S25", typeof(System.String));
                _tbl.Columns.Add("S26", typeof(System.String));
                _tbl.Columns.Add("S27", typeof(System.String));
                _tbl.Columns.Add("S28", typeof(System.String));
                _tbl.Columns.Add("S29", typeof(System.String));
                _tbl.Columns.Add("S30", typeof(System.String));
                _tbl.Columns.Add("S31", typeof(System.String));
                #endregion

                return _tbl;
            }
        }

        public void SetValue(int Month, int Year, Entity.Interfaces.IDangKyDiMuonVeSomInfo Item, ref System.Data.DataTable TYPE_DANGKY_DIMUONVESOM)
        {
            System.Data.DataRow newRow = TYPE_DANGKY_DIMUONVESOM.NewRow();
            newRow["sMonth"] = Month;
            newRow["sYear"] = Year;
            newRow["eID"] = Item.eID;

            #region M
            newRow["M1"] = Item.M1 == null ? "" : Item.M1;
            newRow["M2"] = Item.M2 == null ? "" : Item.M2;
            newRow["M3"] = Item.M3 == null ? "" : Item.M3;
            newRow["M4"] = Item.M4 == null ? "" : Item.M4;
            newRow["M5"] = Item.M5 == null ? "" : Item.M5;
            newRow["M6"] = Item.M6 == null ? "" : Item.M6;
            newRow["M7"] = Item.M7 == null ? "" : Item.M7;
            newRow["M8"] = Item.M8 == null ? "" : Item.M8;
            newRow["M9"] = Item.M9 == null ? "" : Item.M9;
            newRow["M10"] = Item.M10 == null ? "" : Item.M10;

            newRow["M11"] = Item.M11 == null ? "" : Item.M11;
            newRow["M12"] = Item.M12 == null ? "" : Item.M12;
            newRow["M13"] = Item.M13 == null ? "" : Item.M13;
            newRow["M14"] = Item.M14 == null ? "" : Item.M14;
            newRow["M15"] = Item.M15 == null ? "" : Item.M15;
            newRow["M16"] = Item.M16 == null ? "" : Item.M16;
            newRow["M17"] = Item.M17 == null ? "" : Item.M17;
            newRow["M18"] = Item.M18 == null ? "" : Item.M18;
            newRow["M19"] = Item.M19 == null ? "" : Item.M19;
            newRow["M20"] = Item.M20 == null ? "" : Item.M20;

            newRow["M21"] = Item.M21 == null ? "" : Item.M21;
            newRow["M22"] = Item.M22 == null ? "" : Item.M22;
            newRow["M23"] = Item.M23 == null ? "" : Item.M23;
            newRow["M24"] = Item.M24 == null ? "" : Item.M24;
            newRow["M25"] = Item.M25 == null ? "" : Item.M25;
            newRow["M26"] = Item.M26 == null ? "" : Item.M26;
            newRow["M27"] = Item.M27 == null ? "" : Item.M27;
            newRow["M28"] = Item.M28 == null ? "" : Item.M28;
            newRow["M29"] = Item.M29 == null ? "" : Item.M29;
            newRow["M30"] = Item.M30 == null ? "" : Item.M30;
            newRow["M31"] = Item.M31 == null ? "" : Item.M31;
            #endregion

            #region S
            newRow["S1"] = Item.S1 == null ? "" : Item.S1;
            newRow["S2"] = Item.S2 == null ? "" : Item.S2;
            newRow["S3"] = Item.S3 == null ? "" : Item.S3;
            newRow["S4"] = Item.S4 == null ? "" : Item.S4;
            newRow["S5"] = Item.S5 == null ? "" : Item.S5;
            newRow["S6"] = Item.S6 == null ? "" : Item.S6;
            newRow["S7"] = Item.S7 == null ? "" : Item.S7;
            newRow["S8"] = Item.S8 == null ? "" : Item.S8;
            newRow["S9"] = Item.S9 == null ? "" : Item.S9;
            newRow["S10"] = Item.S10 == null ? "" : Item.S10;

            newRow["S11"] = Item.S11 == null ? "" : Item.S11;
            newRow["S12"] = Item.S12 == null ? "" : Item.S12;
            newRow["S13"] = Item.S13 == null ? "" : Item.S13;
            newRow["S14"] = Item.S14 == null ? "" : Item.S14;
            newRow["S15"] = Item.S15 == null ? "" : Item.S15;
            newRow["S16"] = Item.S16 == null ? "" : Item.S16;
            newRow["S17"] = Item.S17 == null ? "" : Item.S17;
            newRow["S18"] = Item.S18 == null ? "" : Item.S18;
            newRow["S19"] = Item.S19 == null ? "" : Item.S19;
            newRow["S20"] = Item.S20 == null ? "" : Item.S20;

            newRow["S21"] = Item.S21 == null ? "" : Item.S21;
            newRow["S22"] = Item.S22 == null ? "" : Item.S22;
            newRow["S23"] = Item.S23 == null ? "" : Item.S23;
            newRow["S24"] = Item.S24 == null ? "" : Item.S24;
            newRow["S25"] = Item.S25 == null ? "" : Item.S25;
            newRow["S26"] = Item.S26 == null ? "" : Item.S26;
            newRow["S27"] = Item.S27 == null ? "" : Item.S27;
            newRow["S28"] = Item.S28 == null ? "" : Item.S28;
            newRow["S29"] = Item.S29 == null ? "" : Item.S29;
            newRow["S30"] = Item.S30 == null ? "" : Item.S30;
            newRow["S31"] = Item.S31 == null ? "" : Item.S31;
            #endregion

            TYPE_DANGKY_DIMUONVESOM.Rows.Add(newRow);
        }
    }
    public class clsDangKyDiMuonVeSom:Interfaces.IDangKyDiMuonVeSom
    {
        public void GetBy(int Month, int Year, string MaNV, Action<Entity.Interfaces.IDangKyDiMuonVeSomInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.Interfaces.IDangKyDiMuonVeSomInfo temp = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_DIMUONVESOM_GETBY_MANV", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyDiMuonVeSomInfo>(tbl);
                        temp = Items[0];
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temp, ok);
        }
        public void GetBy(int Month, int Year, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyDiMuonVeSomInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyDiMuonVeSomInfo> temps = new List<Entity.Interfaces.IDangKyDiMuonVeSomInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_DIMUONVESOM_GETBY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyDiMuonVeSomInfo>(tbl);
                        temps.AddRange(Items);
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }
        public void DangKy(int Month, int Year, List<Entity.Interfaces.IDangKyDiMuonVeSomInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                clsDangKyDiMuonVeSomType clsType = new clsDangKyDiMuonVeSomType();
                System.Data.DataTable type = clsType.TYPE_DANGKY_DIMUONVESOM;
                foreach (var item in Items)
                    clsType.SetValue(Month, Year, item, ref type);

                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_DIMUONVESOM", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@TYPE_DANGKY_DIMUONVESOM", type);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("DangKy", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
