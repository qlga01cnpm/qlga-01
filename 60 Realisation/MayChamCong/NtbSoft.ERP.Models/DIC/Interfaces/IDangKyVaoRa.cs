﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface IDangKyVaoRa
    {
        void GetAll(DateTime TuNgay, DateTime DenNgay, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyRaVaoInfo>, Models.Interfaces.IResultOk> Complete);
        void Update(Libs.ApiAction action, List<Entity.Interfaces.IDangKyRaVaoInfo> Item, Action<Models.Interfaces.IResultOk> Complete);

        void Update(Libs.ApiAction action, string maCC, string ngay, Action<Models.Interfaces.IResultOk> Complete);
    }
}
