﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface IDangKyLamThem
    {
        void GetAll(DateTime TuNgay,DateTime DenNgay, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyLamThemInfo>, Models.Interfaces.IResultOk> Complete);

        /// <summary>
        /// Lấy danh sách các ngày đăng ký làm thêm của một nhân viên
        /// </summary>
        /// <param name="TuNgay"></param>
        /// <param name="DenNgay"></param>
        /// <param name="eID"></param>
        /// <param name="Complete"></param>
        void GetList(DateTime TuNgay, DateTime DenNgay, string eID, Action<List<Entity.Interfaces.IDangKyLamThemInfo>, Models.Interfaces.IResultOk> Complete);
        void Update(Libs.ApiAction action, Entity.Interfaces.IDangKyLamThemInfo Item, Action<Models.Interfaces.IResultOk> Complete);
    }
}
