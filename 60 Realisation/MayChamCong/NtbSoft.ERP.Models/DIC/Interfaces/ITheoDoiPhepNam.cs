﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface ITheoDoiPhepNam
    {
        void GetBy(int Month,int Year,string MaNV,Action<Entity.Interfaces.IDangKyVangInfo,Models.Interfaces.IResultOk> Complete);
        void GetBy(int Month, int Year, string DepId, string MaNV, Action<List<Entity.Interfaces.ITheoDoiPhepNamInfo>, Models.Interfaces.IResultOk> Complete);
        void DangKyVang(int Month, int Year, List<Entity.Interfaces.IDangKyVangInfo> Items, Action<Models.Interfaces.IResultOk> Complete);
        
    }
}
