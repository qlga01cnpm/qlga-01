﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    /// <summary>
    /// Ngày lễ
    /// </summary>
    public interface INgayLe
    {
        void GetAll(Action<List<Entity.Interfaces.INgayLeInfo>, Models.Interfaces.IResultOk> Complete);
        void Save(DateTime Ngay, string Name, Action<Models.Interfaces.IResultOk> Complete);

        void Delete(int Id, Action<Models.Interfaces.IResultOk> Complete);

        void AddAuto(Action<Models.Interfaces.IResultOk> Complete);

        bool CheckIsHoliday(string date);
    }
}
