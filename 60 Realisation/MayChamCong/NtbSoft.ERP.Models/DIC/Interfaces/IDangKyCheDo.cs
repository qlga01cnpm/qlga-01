﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface IDangKyCheDo
    {
        void GetAll(string DepId,string MaNV, Action<List<Entity.Interfaces.IDangKyCheDoInfo>, Models.Interfaces.IResultOk> Complete);
        void Update(Libs.ApiAction action, Entity.Interfaces.IDangKyCheDoInfo Item, Action<Models.Interfaces.IResultOk> Complete);
        
        /// <summary>
        /// Lấy ca làm việc
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="WorkingDay"></param>
        /// <param name="Complete"></param>
        void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo, Models.Interfaces.IResultOk> Complete);

        /// <summary>
        /// Lấy ca làm việc
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="WorkingDay"></param>
        /// <param name="Complete"></param>
        void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo,string, Models.Interfaces.IResultOk> Complete);
    }
}
