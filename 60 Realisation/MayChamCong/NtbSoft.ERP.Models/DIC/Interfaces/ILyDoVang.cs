﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface ILyDoVang
    {
        void GetLyDo(Action<JData.DataComboBoxInfo[], Models.Interfaces.IResultOk> Complete);

        void GetBy(Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Models.Interfaces.IResultOk> Complete);

        void Modify(Libs.ApiAction Action, Entity.Interfaces.ILyDoVangInfo Item, Action<Models.Interfaces.IResultOk> Complete);
    }
}
