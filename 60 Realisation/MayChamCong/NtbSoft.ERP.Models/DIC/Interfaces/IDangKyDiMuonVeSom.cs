﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC.Interfaces
{
    public interface IDangKyDiMuonVeSom
    {
        void GetBy(int Month, int Year, string MaNV, Action<Entity.Interfaces.IDangKyDiMuonVeSomInfo, Models.Interfaces.IResultOk> Complete);
        void GetBy(int Month, int Year, string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyDiMuonVeSomInfo>, Models.Interfaces.IResultOk> Complete);
        void DangKy(int Month, int Year, List<Entity.Interfaces.IDangKyDiMuonVeSomInfo> Items, Action<Models.Interfaces.IResultOk> Complete);
    }
}
