﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsNgayLe:Interfaces.INgayLe
    {
        public void GetAll(Action<List<Entity.Interfaces.INgayLeInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            List<Entity.Interfaces.INgayLeInfo> Items = new List<Entity.Interfaces.INgayLeInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NGAYLE");
                    _cmd.Parameters.AddWithValue("@WhereClause", "where Active = 1");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = new List<Entity.Interfaces.INgayLeInfo>(Libs.ConvertTableToList.ConvertToList<Entity.clsNgayLeInfo>(tbl));
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, _ok);
        }
         
        public void Save(DateTime Ngay, string Name, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NGAYLE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@Ngay", Ngay);
                    _cmd.Parameters.AddWithValue("@Name", Name);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("Save", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }


        public void Delete(int Id, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NGAYLE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@Id", Id);
                    _cmd.Parameters.AddWithValue("@Ngay", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@Name", "");

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("Delete", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }

        public void AddAuto(Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_NGAYLE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "ADDAUTO");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@Ngay", DateTime.Now);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("AddAuto", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }

        public bool CheckIsHoliday(string date)
        {
            using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
            {
                // 3. kiểm tra ngày nghỉ lễ
                string cmdText = string.Format(@"SELECT * FROM dbo.DIC_NGAYLE WHERE
                                                Ngay = '{0}' and Active = 1", date);
                System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                _cmd.CommandType = System.Data.CommandType.Text;

                System.Data.DataTable tbl = new System.Data.DataTable();
                System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                _adapter.Fill(tbl);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }
    }
}
