﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.DIC
{
    public class clsDangKyThaiSan:Interfaces.IDangKyCheDo
    {
        public void GetAll(string DepId, string MaNV, Action<List<Entity.Interfaces.IDangKyCheDoInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            List<Entity.Interfaces.IDangKyCheDoInfo> temps = new List<Entity.Interfaces.IDangKyCheDoInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_THAISAN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@ID", 1);
                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@TuNgay", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ToiNgay", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@ShiftID", "");
                    _cmd.Parameters.AddWithValue("@LyDo", "");
                    _cmd.Parameters.AddWithValue("@SoNgay", 0);
                    _cmd.Parameters.AddWithValue("@ThoiGian", 0);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsDangKyThaiSanInfo>(tbl);
                        temps.AddRange(Items);
                    }

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(temps, ok);
        }

        public void Update(Libs.ApiAction action, Entity.Interfaces.IDangKyCheDoInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_THAISAN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", action.ToString());
                    _cmd.Parameters.AddWithValue("@ID", Item.ID);
                    _cmd.Parameters.AddWithValue("@DepId", "");
                    _cmd.Parameters.AddWithValue("@MaNV", Item.MaNV);
                    _cmd.Parameters.AddWithValue("@TuNgay", Item.TuNgay);
                    _cmd.Parameters.AddWithValue("@ToiNgay", Item.ToiNgay);
                    _cmd.Parameters.AddWithValue("@ShiftID", Item.ShiftID);
                    _cmd.Parameters.AddWithValue("@LyDo", Item.LyDo);
                    _cmd.Parameters.AddWithValue("@SoNgay", Item.SoNgay);
                    _cmd.Parameters.AddWithValue("@ThoiGian", Item.ThoiGian);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add(action.ToString() + ".", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }


        public void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.clsShiftInfo shiftInfo = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_DANGKY_THAISAN_GETSHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@WorkingDay", WorkingDay);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(tbl);
                        shiftInfo = Items[0];
                        shiftInfo.IsThaiSan = true;
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetShift", ex.Message);
            }
            if (Complete != null) Complete(shiftInfo, ok);
        }


        public void GetShift(string MaNV, DateTime WorkingDay, Action<Entity.clsShiftInfo, string, Models.Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }
    }
}
