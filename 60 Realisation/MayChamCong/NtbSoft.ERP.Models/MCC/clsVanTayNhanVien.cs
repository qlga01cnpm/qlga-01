﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsVanTayNhanVien
    {
        /// <summary>
        /// Chỉ lấy vị trí dấu vân tay của các ngon tay. Ngón nào có dấu thì "FingerIndex=1"
        /// </summary>
        /// <param name="DepId"></param>
        /// <param name="TenNV"></param>
        /// <param name="Complete"></param>
        public void GetInfomation(string DepId, string TenNV,string MaCC, Action<List<Entity.clsVanTayNhanVienInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<Entity.clsVanTayNhanVienInfo> Items = new List<Entity.clsVanTayNhanVienInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_LAYVANTAY_VIEW", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SelectedPage", 1);
                    _cmd.Parameters.AddWithValue("@PageSize", 10000);
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@TenNV", TenNV);
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC ?? "");

                    _cmd.Parameters.Add("@TotalPages", SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@TotalRecords", SqlDbType.Int);
                    _cmd.Parameters["@TotalRecords"].Direction = ParameterDirection.Output;
                    
                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsVanTayNhanVienInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetInfomation", ex.Message);
            }
            if (Complete != null) Complete(Items, error);

        }

        /// <summary>
        /// Lấy Data dấu vân tay để tải lên máy chấm công
        /// </summary>
        /// <param name="ArrayMaCC"></param>
        /// <param name="Complete"></param>
        public void GetDataVanTay(string ArrayMaCC, Action<List<Entities.Interfaces.MCC.IUserFingerprintInfo>, Models.Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk _ok = new BaseResultOk();
            List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items = new List<Entities.Interfaces.MCC.IUserFingerprintInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MaCC as UserId,TenNV, UserName,CardID,Privilege,Enabled,Password,BackupNumber,sTmpData0,sTmpData1,sTmpData2,sTmpData3,sTmpData4,sTmpData5,sTmpData6,sTmpData7,sTmpData8,sTmpData9");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where MaCC in({0})", ArrayMaCC));


                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        List<Entity.clsVanTayNhanVienDataInfo> temps = Libs.ConvertTableToList.ConvertToList<Entity.clsVanTayNhanVienDataInfo>(tbl);
                        Items.AddRange(temps);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetData", ex.Message);
            }
            if (Complete != null) Complete(Items, _ok);
        }

        public void UpdateVanTay(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    using (System.Data.SqlClient.SqlTransaction tran=_cnn.BeginTransaction())
                    {
                        foreach (var item in Items)
                        {
                            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_CAPNHAT_VT", _cnn, tran);
                            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            _cmd.Parameters.AddWithValue("@MaCC", item.UserId);
                            _cmd.Parameters.AddWithValue("@TenNV", item.UserName);
                            _cmd.Parameters.AddWithValue("@CardID", item.CardID == null ? "" : item.CardID);
                            _cmd.Parameters.AddWithValue("@sTmpData0", item.sTmpData0);
                            _cmd.Parameters.AddWithValue("@sTmpData1", item.sTmpData1);
                            _cmd.Parameters.AddWithValue("@sTmpData2", item.sTmpData2);
                            _cmd.Parameters.AddWithValue("@sTmpData3", item.sTmpData3);
                            _cmd.Parameters.AddWithValue("@sTmpData4", item.sTmpData4);
                            _cmd.Parameters.AddWithValue("@sTmpData5", item.sTmpData5);
                            _cmd.Parameters.AddWithValue("@sTmpData6", item.sTmpData6);
                            _cmd.Parameters.AddWithValue("@sTmpData7", item.sTmpData7);
                            _cmd.Parameters.AddWithValue("@sTmpData8", item.sTmpData8);
                            _cmd.Parameters.AddWithValue("@sTmpData9", item.sTmpData8);
                            _cmd.Parameters.AddWithValue("@Privilege", item.Privilege);
                            _cmd.Parameters.AddWithValue("@Enabled", item.Enabled);
                            _cmd.Parameters.AddWithValue("@Password", item.Password);
                            _cmd.Parameters.AddWithValue("@BackupNumber", item.BackupNumber);
                            
                            _cmd.ExecuteNonQuery();
                        }

                        tran.Commit();
                    }
                   
                   
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("UpdateVanTay", ex.Message);
            }
            if (Complete != null) Complete(error);
        }

        /// <summary>
        /// Cập nhật quyền cho người sử dụng máy chấm công
        /// </summary>
        /// <param name="ArrayMacc"></param>
        /// <param name="Privilege"></param>
        /// <param name="Complete"></param>
        public void UpdatePrivilege(string ArrayMaCC, int Privilege, Action<Models.Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk _ok = new BaseResultOk();
            List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items = new List<Entities.Interfaces.MCC.IUserFingerprintInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Update", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SetValues", "Privilege=" + Privilege.ToString());
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where MaCC in({0})", ArrayMaCC));

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("UpdatePrivilege", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }
    }
}
