﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsScheduleShift:Interfaces.IScheduleSchift
    {
        public System.Data.DataTable TYPE_SCHEDULE_SHIFT
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_SCHEDULE_SHIFT");
                _tbl.Columns.Add("Ngay", typeof(System.Int32));
                _tbl.Columns.Add("SchID", typeof(System.Int32));
                _tbl.Columns.Add("SchName", typeof(System.String));
                
                _tbl.Columns.Add("Shift1", typeof(System.String));
                _tbl.Columns.Add("Shift2", typeof(System.String));
                _tbl.Columns.Add("Shift3", typeof(System.String));
                _tbl.Columns.Add("Shift4", typeof(System.String));
                _tbl.Columns.Add("Shift5", typeof(System.String));
                _tbl.Columns.Add("Shift6", typeof(System.String));
                _tbl.Columns.Add("Shift7", typeof(System.String));
                _tbl.Columns.Add("Shift8", typeof(System.String));
                _tbl.Columns.Add("Shift9", typeof(System.String));

                _tbl.Columns.Add("NumberShift", typeof(System.Int32));
                _tbl.Columns.Add("Shift", typeof(System.String));
                _tbl.Columns.Add("StrShift", typeof(System.String));
                _tbl.Columns.Add("IsUse", typeof(System.Boolean));
                return _tbl;
            }
        }

        private void SetType(int SchID, List<Entity.clsScheduleShiftInfo> Items, ref System.Data.DataTable Type_Schedule_Shift)
        {
            if (Items == null) return;
            foreach (var item in Items)
            {
                System.Data.DataRow newRow = Type_Schedule_Shift.NewRow();
                newRow["Ngay"]=item.Ngay;
                newRow["SchID"]=SchID;
                newRow["SchName"] = item.SchName == null ? "" : item.SchName;

                newRow["Shift1"] = item.Shift1 == null ? "" : item.Shift1;
                newRow["Shift2"] = item.Shift2 == null ? "" : item.Shift2;
                newRow["Shift3"] = item.Shift3 == null ? "" : item.Shift3;
                newRow["Shift4"] = item.Shift4 == null ? "" : item.Shift4;
                newRow["Shift5"] = item.Shift5 == null ? "" : item.Shift5;
                newRow["Shift6"] = item.Shift6 == null ? "" : item.Shift6;
                newRow["Shift7"] = item.Shift4 == null ? "" : item.Shift7;
                newRow["Shift8"] = item.Shift5 == null ? "" : item.Shift8;
                newRow["Shift9"] = item.Shift6 == null ? "" : item.Shift9;

                newRow["NumberShift"] = item.NumberShift;
                newRow["Shift"] = item.Shift == null ? "" : item.Shift;
                newRow["StrShift"] = item.StrShift == null ? "" : item.StrShift;
                newRow["IsUse"] = item.IsUse;

                Type_Schedule_Shift.Rows.Add(newRow);
            }
        }

        private List<Entity.clsScheduleShiftInfo> GetDefault()
        {
            return new List<Entity.clsScheduleShiftInfo>()
            {
                 new Entity.clsScheduleShiftInfo(1),
                 new Entity.clsScheduleShiftInfo(2),
                 new Entity.clsScheduleShiftInfo(3),
                 new Entity.clsScheduleShiftInfo(4),
                 new Entity.clsScheduleShiftInfo(5),
                 new Entity.clsScheduleShiftInfo(6),
                 new Entity.clsScheduleShiftInfo(7),
                 new Entity.clsScheduleShiftInfo(8)
            };
        }

        public void GetBy(int SchID, Action<List<Entity.clsScheduleShiftInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<Entity.clsScheduleShiftInfo> Items = new List<Entity.clsScheduleShiftInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@SchID", SchID);
                    _cmd.Parameters.AddWithValue("@TYPE_SCHEDULE_SHIFT", this.TYPE_SCHEDULE_SHIFT);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsScheduleShiftInfo>(tbl);
                    else
                        Items = this.GetDefault();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy_Id", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }

        public void GetBy(string SchName, Action<List<Entity.clsScheduleShiftInfo>, NtbSoft.ERP.Models.Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<Entity.clsScheduleShiftInfo> Items = new List<Entity.clsScheduleShiftInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE_SHIFT_BY_SCHNAME", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SchName", SchName == null ? "" : SchName);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsScheduleShiftInfo>(tbl);
                    else
                        Items = this.GetDefault();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy_Name", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }

        public void GetBy(string SchName, DateTime WorkingDay, Action<Entity.clsScheduleShiftInfo, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            Entity.clsScheduleShiftInfo Item = new Entity.clsScheduleShiftInfo();
            List<Entity.clsScheduleShiftInfo> scheduleSfhits = null;
            this.GetBy(SchName, (result, ex) => { scheduleSfhits = result; ok = ex; });
            if (!ok.Success) goto exit;

            string ShiftId = "";
            foreach (var item in scheduleSfhits)
            {
                #region Ngày lễ
                if (item.Ngay == 8)
                {//Ngày lễ (Tết)

                    break;
                }
                #endregion

                #region Chủ nhật
                if (WorkingDay.DayOfWeek == DayOfWeek.Sunday && item.Ngay == 1)
                {
                    break;
                }
                #endregion

                #region Thứ 2
                if (WorkingDay.DayOfWeek == DayOfWeek.Monday && item.Ngay == 2)
                {//Thứ 2

                    break;
                }
                #endregion

                #region Thứ 3
                if (WorkingDay.DayOfWeek == DayOfWeek.Tuesday && item.Ngay == 3)
                {//Thứ 3
                    break;
                }
                #endregion

                #region Thứ 4
                if (WorkingDay.DayOfWeek == DayOfWeek.Wednesday && item.Ngay == 4)
                {//Thứ 4
                    break;
                }
                #endregion

                #region Thứ 5
                if (WorkingDay.DayOfWeek == DayOfWeek.Thursday && item.Ngay == 4)
                {//Thứ 5
                    break;
                }
                #endregion

                #region Thứ 6
                if (WorkingDay.DayOfWeek == DayOfWeek.Friday && item.Ngay == 6)
                {//Thứ 6

                    var kk = 1;
                    var dd = kk;
                    var ss = dd;
                    break;
                }
                #endregion

                #region Thứ 7
                if (WorkingDay.DayOfWeek == DayOfWeek.Saturday && item.Ngay == 4)
                {//Thứ 7
                    break;
                }
                #endregion
            }

            exit:
            if (Complete != null) Complete(Item, ok);
        }


        //public void GetByEmID(double EmpID, Action<List<Entity.clsScheduleShiftInfo>, NtbSoft.ERP.Models.Interfaces.IResultOk> Complete)
        //{
        //    Interfaces.IResultOk error = new BaseResultOk();
        //    List<Entity.clsScheduleShiftInfo> Items = new List<Entity.clsScheduleShiftInfo>();
        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE_SHIFT", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Action", "GET");
        //            _cmd.Parameters.AddWithValue("@SchID", SchID);
        //            _cmd.Parameters.AddWithValue("@TYPE_SCHEDULE_SHIFT", this.TYPE_SCHEDULE_SHIFT);

        //            System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
        //            System.Data.DataTable tbl = new System.Data.DataTable();
        //            _adapter.Fill(tbl);
        //            if (tbl != null && tbl.Rows.Count > 0)
        //                Items = Libs.ConvertTableToList.ConvertToList<Entity.clsScheduleShiftInfo>(tbl);
        //            else
        //                Items = this.GetDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        error.Errors.Add("GetBy_Id", ex.Message);
        //    }
        //    if (Complete != null) Complete(Items, error);
        //}

        public void BatchUpdate(int SchID, List<Entity.clsScheduleShiftInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                System.Data.DataTable Type_ScheduleShift = this.TYPE_SCHEDULE_SHIFT;
                this.SetType(SchID, Items, ref Type_ScheduleShift);
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@SchID", SchID);
                    _cmd.Parameters.AddWithValue("@TYPE_SCHEDULE_SHIFT", Type_ScheduleShift);
                    
                    error.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(error);
        }
    }
}
