﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsTimeSheetVirtual : Interfaces.ITimeSheet
    {
        public void GetBy(int Month, int Year, string ArrayEmpID, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    string tblJoin = string.Format("dbo.MCC_TIMESHEETS_VIRTUAL t1 inner join dbo.DIC_NHANVIEN t2 on t1.MaNV=t2.Id and isnull(t2.HideVirtual, 0) = 0");
                    string WhereClause = string.Format("where t1.Month={0} and t1.Year={1} and t2.Id in({2})", Month, Year, ArrayEmpID);
                    _cmd.Parameters.AddWithValue("@Columns", "t2.MaNV as MaNV2,t2.TenNV,t1.* ");
                    _cmd.Parameters.AddWithValue("@TableName", tblJoin);
                    _cmd.Parameters.AddWithValue("@WhereClause", WhereClause);

                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);

                    adapter.Fill(tbl);
                    adapter.Dispose();
                    adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(tbl, ok);
        }


        public void ExportMonthly(int Month, int Year, string DepId, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            //double id = 0;
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    if (Year > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Year", Year);
                    }
                    if (Month > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Month", Month);
                    }
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("ExportMonthly", ex.Message);
            }
            if (Complete != null) Complete(tbl, ok);
        }

        public void ReportMonthly(int Month, int Year, string DepId, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            //double id = 0;
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMESHEETS_VIRTUAL_BaoCao_THANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    if (Year > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Year", Year);
                    }
                    if (Month > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Month", Month);
                    }
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("ExportMonthly", ex.Message);
            }
            if (Complete != null) Complete(tbl, ok);
        }

        public void ReportYear(int Month, int Year, string DepId, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            //double id = 0;
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMESHEETS_VIRTUAL_BaoCao_TongHop", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    if (Year > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Year", Year);
                    }
                    if (Month > 0)
                    {
                        _cmd.Parameters.AddWithValue("@MonthWithout", Month);
                    }
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("ReportYear", ex.Message);
            }
            if (Complete != null) Complete(tbl, ok);
        }
    }
}
