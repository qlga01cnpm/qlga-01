﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsQuyTac : Interfaces.IQuyTac
    {
        Interfaces.IQuyTacLamTron _QuyTacLamTron;
        public clsQuyTac(Interfaces.IQuyTacLamTron QuyTacLamTron)
        {
            this._QuyTacLamTron = QuyTacLamTron;
        }
    

        public void SoPhutToiThieuGiuaHaiLanCham(Action<int, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk _ok = new BaseResultOk();
            int _sophut = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "SoPhutGiuaHaiLan");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_QUYTAC");
                    _cmd.Parameters.AddWithValue("@WhereClause", " ");

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        _sophut = Convert.ToInt32(_reader["SoPhutGiuaHaiLan"]);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("SoPhutToiThieuGiuaHaiLanCham", ex.Message);
            }
            if (Complete != null) Complete(_sophut, _ok);
        }

        public void checkInRaMay(Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void GetFirst(Action<Entity.Interfaces.IQuyTacInfo, Interfaces.IResultOk> Complete)
        {
            Entity.Interfaces.IQuyTacInfo Item = new Entity.clsQuyTacInfo();
            Interfaces.IResultOk result = new BaseResultOk();
            this._QuyTacLamTron.GetAll((Items, complete) =>
            {
                if (complete.Success)
                    Item.QuyTacLamTron = Items.ToList();
            });

            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "TOP 1*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_QUYTAC");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataReader reader = _cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Item.SoPhutGiuaHaiLan = Convert.ToInt32(reader["SoPhutGiuaHaiLan"]);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add("GetFirst", ex.Message);
            }
            if (Complete != null) Complete(Item, result);
        }

        public void SetSoPhut(int SoPhutGiuaHaiLan, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk result = new BaseResultOk();
            try
            {

                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_QUYTAC_SETSOPHUT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SoPhutGiuaHaiLan", SoPhutGiuaHaiLan);

                    result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add("SetSoPhut", ex.Message);
            }
        }
    }
}
