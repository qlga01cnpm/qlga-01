﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    /// <summary>
    /// Xử lý dữ liệu tải Ảo
    /// </summary>
    public class clsXuLyDuLieuTaiAo : Interfaces.IXuLyDuLieuTai
    {
        internal Interfaces.IQuyTac _QuyTac;
        private string _MaCC = "";
        //Ca lam viec
        private Models.Interfaces.IShift _mShift;
        public clsXuLyDuLieuTaiAo(Models.Interfaces.IShift Shift,Interfaces.IQuyTac QuyTac)
        {
            _QuyTac = QuyTac;
        }

        /// <summary>
        /// Hàm kiểm tra chuỗi giờ chấm có phải là định dạng thời gian hay không
        /// </summary>
        /// <param name="GioCham"></param>
        /// <returns></returns>
        public bool IsTime(string GioCham)
        {
            TimeSpan tmp;
            return TimeSpan.TryParse(GioCham,out tmp);
        }

        public bool CheckHopLe(string MaCC, DateTime Ngay, string GioCham)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BreakInOfShift(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void CheckExistInOut(string MaNV, string WorkingDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void NumberShift(string SchID, Action<int, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void ReturnEmpID(string MaCC, Action<double, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            double id = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 Id ");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' ", MaCC));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        id = Convert.ToDouble(_reader["id"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("ReturnEmpID", ex.Message);
            }
            if (Complete != null) Complete(id, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<int, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            int SoLan = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Count(MaCC) as SoLan");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION_VIRTUAL");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        SoLan = Convert.ToInt32(_reader["SoLan"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(SoLan, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<List<TimeSpan>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<TimeSpan> mTime = new List<TimeSpan>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        TimeSpan mTemp;
                        if (TimeSpan.TryParse(Convert.ToString(_reader["Gio"]), out mTemp))
                            mTime.Add(mTemp);
                        //SoLan = Convert.ToInt32(_reader["SoLan"]);
                        //break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("SoLanCham", ex.Message);
            }
            if (Complete != null) Complete(mTime, error);
        }
        public void MinGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MIN(Gio) as Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION_VIRTUAL");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        public void MaxGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MAX(Gio) as Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION_VIRTUAL");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        /// <summary>
        /// Giờ ra (giờ ra nghỉ trưa)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakOutGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            //////bool flag = Information.IsDate(MinGioQuet) & Information.IsDate(MaxGioQuet);
            //////if (flag)
            //////{
            //////    string text = string.Concat(new string[]
            //////    {
            //////        "SELECT Min(GioQuet) as Gio FROM ",
            //////        TenBang,
            //////        " WHERE WorkingDay=Convert(DateTime,'",
            //////        Ngay,
            //////        "',103) AND MaCC='",
            //////        CardID,
            //////        "'\r\n"
            //////    });
            //////    text = string.Concat(new string[]
            //////    {
            //////        text,
            //////        " AND GioQuet >'",
            //////        MinGioQuet,
            //////        "' AND GioQuet < '",
            //////        MaxGioQuet,
            //////        "' AND chkUsed=0"
            //////    });
            //////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            //////    flag = (dataSet.Tables[0].Rows.Count > 0);
            //////    if (flag)
            //////    {
            //////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            //////    }
            //////}
        }

        /// <summary>
        /// Giờ vào (giờ sau khi nghỉ trưa vào)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="BreakOutGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, string BreakOutGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            ////bool flag = Information.IsDate(MaxGioQuet) & Information.IsDate(BreakOutGioQuet);
            ////if (flag)
            ////{
            ////    string text = string.Concat(new string[]
            ////    {
            ////        "SELECT Max(GioQuet) as Gio FROM ",
            ////        TenBang,
            ////        " WHERE WorkingDay=Convert(DateTime,'",
            ////        Ngay,
            ////        "',103) AND MaCC='",
            ////        CardID,
            ////        "'\r\n"
            ////    });
            ////    text = string.Concat(new string[]
            ////    {
            ////        text,
            ////        " AND GioQuet < '",
            ////        MaxGioQuet,
            ////        "'  AND GioQuet > '",
            ////        BreakOutGioQuet,
            ////        "' AND chkUsed=0"
            ////    });
            ////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            ////    flag = (dataSet.Tables[0].Rows.Count > 0);
            ////    if (flag)
            ////    {
            ////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            ////    }
            ////}
        }


        private void GetBreakOutInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, out string BreakOutGioCham, out string BreakInGioCham, Action<Interfaces.IResultOk> Complete)
        {
            BreakOutGioCham = ""; BreakInGioCham = "";
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                //using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                //{
                //    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET", _cnn);
                //    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                //    _cmd.Parameters.AddWithValue("@Workingday", Ngay);
                //    _cmd.Parameters.AddWithValue("@MaCC", MaCC);
                //    _cmd.Parameters.AddWithValue("@MinGioCham", MinGioCham);
                //    _cmd.Parameters.AddWithValue("@MaxGioCham", MaxGioCham);

                //    _cmd.Parameters.Add("@BreakOut", SqlDbType.VarChar,8);
                //    _cmd.Parameters["@BreakOut"].Direction = ParameterDirection.Output;
                //    _cmd.Parameters.Add("@BreakIn", SqlDbType.VarChar,8);
                //    _cmd.Parameters["@BreakIn"].Direction = ParameterDirection.Output;

                //    ok.RowsAffected = _cmd.ExecuteNonQuery();

                //    if (_cmd.Parameters["@BreakOut"].Value != null)
                //        BreakOutGioCham = _cmd.Parameters["@BreakOut"].Value.ToString();
                //    if (_cmd.Parameters["@BreakIn"].Value != null)
                //        BreakInGioCham = _cmd.Parameters["@BreakIn"].Value.ToString();
                //}
            }
            catch (Exception ex)
            {
                ok.Errors.Add("BreakOutInGioCham", ex.Message);
            }
        }

        public void XuLyDuLieuChamCong(string MaCC, DateTime CurDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool _flagHopLe = false;
            int SoLan = 0,EmpId =0;
            this.ReturnEmpID(MaCC, (result, complete) => { EmpId = (int)result; });
            if (EmpId == 0) return;

            string MinGio = "", MaxGio = "",BreakOut="",BreakIn="";
            this.SoLanCham(MaCC, CurDay, (result, complete) => { SoLan = result; });
            this.MinGioCham(MaCC, CurDay, (result, complete) => { MinGio = result; });
            this.MaxGioCham(MaCC, CurDay, (result, complete) => { MaxGio = result; });
            
            
            /** Trường hợp 1 lần chấm ***/
            _flagHopLe = (SoLan == 1);
            if (_flagHopLe)
            {
                MaxGio = "";
            }

            /** Trường hợp 2 lần chấm ***/
            _flagHopLe = (SoLan == 2);
            if (_flagHopLe)
            {


            }
            /** Trường hợp 3 lần chấm (Lúc này sẽ kiểm tra giờ ra vào trước và sau khi nghỉ trưa) ***/
            _flagHopLe = (SoLan == 3);
            if (_flagHopLe)
            {
                this.GetBreakOutInGioCham(MaCC, CurDay, MinGio, MaxGio, out BreakOut, out BreakIn, ex => { });
                //CheckOverNight
            }

            string DateIn,DateOut;
            DateIn = DateOut = CurDay.ToString(Libs.clsConsts.FormatMDY);
            if (this.IsTime(MinGio))
                DateIn = DateIn + " " + MinGio;
            if (this.IsTime(MaxGio))
                DateOut = DateOut + " " + MaxGio;
                
            Interfaces.IResultOk error = new BaseResultOk();
            bool bl=false;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_VIRTUAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", CurDay.ToString(Libs.clsConsts.FormatMDY));
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.Parameters.AddWithValue("@MaNV", EmpId);
                    _cmd.Parameters.AddWithValue("@TimeIn", MinGio);
                    _cmd.Parameters.AddWithValue("@BreakOut", BreakOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", BreakIn);
                    
                    _cmd.Parameters.AddWithValue("@TimeOut", MaxGio);
                    _cmd.Parameters.AddWithValue("@DateTimeIn", DateIn);
                    _cmd.Parameters.AddWithValue("@DateBreakOut", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateBreakIn", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateTimeOut", DateOut);

                   bl = _cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("XuLyDuLieuChamCong", ex.Message);
            }
            if (this._MaCC != MaCC)
            {
                this._MaCC = MaCC;
                /*** Lấy ngày cuối cùng của 1 tháng ***/
                DateTime tmp = Libs.clsCommon.LastDayOfMonth(CurDay.Month, CurDay.Year);
                /*** Thiết lập các ngày trong 1 tháng ***/
                this.InitializeDate(CurDay.Month, tmp.Day, CurDay.Year, MaCC, ex => { });
            }
            if (Complete != null) Complete(bl, error);

        }

        public void CheckTonTaiBang(string TableName, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }


        public void InitializeDate(int Month, int MaxDay, int Year, string MaCC, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                //using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                //{
                //    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", _cnn);
                //    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                //    _cmd.Parameters.AddWithValue("@Month", Month);
                //    _cmd.Parameters.AddWithValue("@MaxDay",MaxDay);
                //    _cmd.Parameters.AddWithValue("@Year", Year);
                //    _cmd.Parameters.AddWithValue("@MaCC", MaCC);

                //    ok.RowsAffected = _cmd.ExecuteNonQuery();
                //}
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        public void InitializeDate(int Month, int MaxDay, int Year, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@MaxDay", MaxDay);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
