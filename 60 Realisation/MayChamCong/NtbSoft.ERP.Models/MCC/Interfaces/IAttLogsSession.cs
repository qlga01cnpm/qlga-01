﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface IAttLogsSession
    {
        void Modify(Libs.ApiAction Action, Entity.Interfaces.IXuLyChamCongInfo Item, Action<Models.Interfaces.IResultOk> Complete);
    }
}
