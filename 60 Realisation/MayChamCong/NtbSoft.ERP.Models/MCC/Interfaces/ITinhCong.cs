﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface ITinhCong
    {
        /// <summary>
        /// Tinh công cho nhân viên
        /// </summary>
        /// <param name="SessionInfo"></param>
        /// <param name="ShifInfo"></param>
        /// <param name="Complete"></param>
        void TinhCong(Entity.Interfaces.ITimeInOutSessionInfo SessionInfo, Action<Entity.Interfaces.ITimeInOutSessionInfo, Models.Interfaces.IResultOk> Complete);
        void TinhCong(IEnumerable<Entity.Interfaces.INhanVienInfo> DsNhanVien, Action<Models.Interfaces.IResultOk> Complete);
    }
}
