﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface IBcDangKyCheDo
    {
        void GetBy(DateTime fromDate, DateTime toDate, string DepID, int MaCheDo, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete);
    }
}
