﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface IAttLogsVirtual
    {
        void GenerateVirtualTime(Action<Models.Interfaces.IResultOk> Complete);

        void GenerateVirtualTime(bool AllowUpdated,int Month,int Year,int[] ArrayCN,List<Entity.Interfaces.INhanVienInfo> Items, Action<Models.Interfaces.IResultOk> Complete);

        void Delete(int Month, int Year,string MaNV, Action<Models.Interfaces.IResultOk> Complete);
    }
}
