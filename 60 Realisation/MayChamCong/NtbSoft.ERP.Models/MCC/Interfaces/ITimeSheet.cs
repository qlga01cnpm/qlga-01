﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface ITimeSheet
    {
        void GetBy(int Month, int Year, string ArrayEmpID,Action<System.Data.DataTable,Models.Interfaces.IResultOk> Complete);

        void ExportMonthly(int Month, int Year, string DepID, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete);

        void ReportMonthly(int Month, int Year, string DepID, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete);

        void ReportYear(int Month, int Year, string DepID, string MaNV, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete);
    }
}
