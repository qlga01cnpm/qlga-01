﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC.Interfaces
{
    public interface IBcChiTietNgay
    {
        void GetBy(DateTime fromDate, DateTime toDate, string ArrayEmpID, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete);
    }
}
