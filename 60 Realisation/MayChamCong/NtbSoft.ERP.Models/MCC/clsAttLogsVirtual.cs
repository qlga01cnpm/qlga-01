﻿using NtbSoft.Entities.Interfaces.MCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsAttLogsVirtual:MCC.Interfaces.IAttLogsVirtual
    {
        internal Models.DIC.Interfaces.IDangKyVang _DangKyVang;
        internal Models.DIC.Interfaces.IDangKyDiMuonVeSom _DangKyDiMuonVeSom;
        internal Models.Interfaces.IScheduleSchift _ScheduleShift;
        
        internal Models.Interfaces.IShift _clsShift;
        internal List<Entity.clsShiftInfo> _ShiftCollection = null;
        public clsAttLogsVirtual(Models.DIC.Interfaces.IDangKyVang DangKyVang,
            Models.DIC.Interfaces.IDangKyDiMuonVeSom DangKyDiMuonVeSom,
            Models.Interfaces.IScheduleSchift ScheduleShift,
            Models.Interfaces.IShift clsShift)
        {
            _DangKyVang = DangKyVang;
            this._DangKyDiMuonVeSom = DangKyDiMuonVeSom;
            this._ScheduleShift = ScheduleShift;
            this._clsShift = clsShift;
        }
        public void GenerateVirtualTime(Action<Models.Interfaces.IResultOk> Complete)
        {
            //Lấy danh sách 

        }

        public void GenerateVirtualTime(bool AllowUpdated, int Month, int Year, int[] ArrayCN, List<Entity.Interfaces.INhanVienInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        {
            if (Items == null)
                Items = new List<Entity.Interfaces.INhanVienInfo>();
            if (ArrayCN == null || ArrayCN.Length < 6) ArrayCN = new int[] { 0, 0, 0, 0, 0, 0 };
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            //1. Lấy ca làm việc và lấy giờ bắt đầu ca
            //2. Lấy giờ kết thúc 
            Models.clsAttLogsType type = new clsAttLogsType();
            System.Data.DataTable tbl = type.TYPE_ATTLOGS;
            var dayEnd = DateTime.Now.Day;
            if (Month != DateTime.Now.Month) { dayEnd = Libs.clsCommon.LastDayOfMonth(Month, Year).Day; }
            Random randomIn = new Random();
            Random randomOut = new Random();

            while (Items.Count > 0)
            {
                var MaCC = Items[0].MaCC;
                List<Entity.clsScheduleShiftInfo> scheduleShiftCollection = null;
                this._ScheduleShift.GetBy(Items[0].SCHName, (result, ex) => { scheduleShiftCollection = result; });
                if (scheduleShiftCollection == null || scheduleShiftCollection.Count == 0) continue;

                int countCN = 0;
                for (int i = 1; i <= dayEnd; i++)
                {
                    DateTime dTemp = new DateTime(Year, Month, i);
                    var shift = GetCaLamViec(dTemp, Items[0].SCHName);
                    if (shift == null) continue;

                    TimeSpan startIn, startOut;
                    if (!TimeSpan.TryParse(shift.TimeIn, out startIn)) startIn = TimeSpan.FromHours(7);
                    if (!TimeSpan.TryParse(shift.TimeOut, out startOut)) startOut = new TimeSpan(17, 0, 0);

                    TimeSpan endIn = startIn.Add(TimeSpan.FromMinutes(-11));
                    int maxMinuteIn = (int)(startIn - endIn).TotalMinutes;

                    TimeSpan endOut = startOut.Add(TimeSpan.FromMinutes(11));
                    int maxMinuteOut = (int)(endOut - startOut).TotalMinutes;

                    string strTimeIn = "", strTimeOut = "";
                    /*** 1. Bỏ qua ngày chủ nhật ***/
                    if (dTemp.DayOfWeek == DayOfWeek.Sunday)
                    {
                        countCN++;
                        if (ArrayCN[0] == 0)
                            goto tt;
                        else
                        {
                            if ((countCN == 1 && ArrayCN[1] == 0) ||
                                countCN == 2 && ArrayCN[2] == 0 ||
                                countCN == 3 && ArrayCN[3] == 0 ||
                                countCN == 4 && ArrayCN[4] == 0 ||
                                countCN == 5 && ArrayCN[5] == 0)
                                goto tt;
                        }
                    }
                    /** 2. Kiểm tra ngày lễ. Bỏ qua ngày lễ***/

                    /*** 3. Kiểm tra nghỉ. Bỏ qua nghỉ ***/
                    Entity.Interfaces.IDangKyVangInfo DangKyVangInfo = null;
                    _DangKyVang.GetBy(Month, Year, Items[0].MaNV, (result, ex) => { DangKyVangInfo = result; });
                    if (DangKyVangInfo != null && !string.IsNullOrWhiteSpace(DangKyVangInfo.GetValue(i))) goto tt;
                    Entity.Interfaces.IDangKyDiMuonVeSomInfo DiMuonVeSomInfo = null;
                    this._DangKyDiMuonVeSom.GetBy(Month, Year, Items[0].MaNV, (result, ex) => { DiMuonVeSomInfo = result; if (!ok.Success)ok.Errors.Add("Get_DangKy_DiMuon_VeSom", ex.Message); });

                    int minutes = randomIn.Next(0, maxMinuteIn);
                    bool hasVeSom = false;
                    double vesom = 0;
                    if (DiMuonVeSomInfo != null)
                    {
                        var valueM = DiMuonVeSomInfo.GetMValue(dTemp.Day);
                        var valueS = DiMuonVeSomInfo.GetSValue(dTemp.Day);
                        double dimuon = 0;
                        if (double.TryParse(valueM, out dimuon))
                        {
                            minutes = (int)dimuon;
                            endIn = startIn;
                        }
                        if (double.TryParse(valueS, out vesom)) { hasVeSom = true; }

                    }
                    strTimeIn = endIn.Add(TimeSpan.FromMinutes(minutes)).ToString(Libs.cFormat.Hour);
                    minutes = randomOut.Next(0, maxMinuteOut);
                    if (hasVeSom) minutes = -(int)vesom;
                    strTimeOut = startOut.Add(TimeSpan.FromMinutes(minutes)).ToString(Libs.cFormat.Hour);

                tt:
                    //Phân biệt giờ vào ngày hôm đó ở số giây (s) 1 giây là chấm vào, 2 giây là chấm ra
                    type.SetValue(MaCC, dTemp.AddSeconds(1), strTimeIn, 1, true, ref tbl);

                    /*** Nếu giờ hiện tại nhỏ hơn giờ quy định chấm ra, thì bỏ qua ***/
                    type.SetValue(MaCC, dTemp.AddSeconds(2), strTimeOut, 1, true, ref tbl);
                }//End For

                Items.Remove(Items[0]);
            }

            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIRTUAL", _cnn);
                    //System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    if (AllowUpdated) _cmd.Parameters.AddWithValue("@Action", "PUT");
                    else _cmd.Parameters.AddWithValue("@Action", "POST");

                    _cmd.Parameters.AddWithValue("@TYPE_ATTLOGS", tbl);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GenerateVirtualTime", ex.Message);
            }
            if (Complete != null) Complete(ok);

        }

        private Entity.clsShiftInfo GetCaLamViec(DateTime WorkingDay, string SchName)
        {
            Entity.clsShiftInfo Info = null;
            List<Entity.clsScheduleShiftInfo> scheduleShiftCollection = null;
            Models.Interfaces.IResultOk ok = null;
            this._ScheduleShift.GetBy(SchName, (result, ex) => { scheduleShiftCollection = result; ok = ex; });
            if (scheduleShiftCollection == null || scheduleShiftCollection.Count == 0) return Info;
            foreach (var item in scheduleShiftCollection)
            {
                Info = GetCaLamViec1(WorkingDay, item.Shift1);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift2);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift3);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift4);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift5);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift6);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift7);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift8);
                if (Info != null) return Info;
                Info = GetCaLamViec1(WorkingDay, item.Shift9);
                if (Info != null) return Info;
            }
            return Info;
        }

        private Entity.clsShiftInfo GetCaLamViec1(DateTime WorkingDay, string ShiftId)
        {
            if (this._ShiftCollection == null || this._ShiftCollection.Count == 0)
                /*** Lấy danh sách Ca làm việc ***/
                this._clsShift.GetAll((result, ex) => { if (ex.Success) this._ShiftCollection = result; });
            var fItem = this._ShiftCollection.Find(f => f.ShiftId.Equals(ShiftId));
            if (fItem != null)
            {
                //Kiểm tra ngày áp dụng ca
                //WorkingDay: Ngày chấm công; TuNgay: là ngày ca được áp dụng
                //Ngày chấm công phải lơn hơn hoặc bằng ngày áp dụng thì mới tính ca
                if (DateTime.Compare(WorkingDay, fItem.TuNgay) == -1)
                    return null;
                return fItem;

            }

            return null;
        }

        //public void GenerateVirtualTime(int Month, int Year, List<Entity.Interfaces.INhanVienInfo> Items, Action<Models.Interfaces.IResultOk> Complete)
        //{
        //    if (Items == null)
        //        Items = new List<Entity.Interfaces.INhanVienInfo>();
        //    Models.Interfaces.IResultOk ok = new BaseResultOk();
        //    //1. Lấy ca làm việc và lấy giờ bắt đầu ca
        //    //2. Lấy giờ kết thúc 
        //    Models.clsAttLogsType type = new clsAttLogsType();
        //    System.Data.DataTable tbl = type.TYPE_ATTLOGS;
        //    List<string> ArrayMaCC = new List<string>();
        //    Random randomIn = new Random();
        //    Random randomOut = new Random();
        //    List<Tuple<string, string>> t = new List<Tuple<string, string>>();
            
        //    ArrayMaCC.Add("1");
        //    ArrayMaCC.Add("2");
        //    while (ArrayMaCC.Count>0)
        //    {
        //        var MaCC = ArrayMaCC[0];
        //        TimeSpan startIn = TimeSpan.FromHours(7);
        //        TimeSpan endIn = startIn.Add(TimeSpan.FromMinutes(-11));
        //        int maxMinuteIn = (int)(startIn - endIn).TotalMinutes;

        //        TimeSpan startOut = new TimeSpan(17, 0, 0),
        //                 endOut = startOut.Add(TimeSpan.FromMinutes(11));
        //        int maxMinuteOut = (int)(endOut - startOut).TotalMinutes;
        //        for (int i = 1; i <= 9; i++)
        //        {
        //            DateTime dTemp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i);
                    
        //            /*** 1. Bỏ qua ngày chủ nhật ***/
        //            if (dTemp.DayOfWeek == DayOfWeek.Sunday) continue;
        //            /** 2. Kiểm tra ngày lễ. Bỏ qua ngày lễ***/

        //            /*** 3. Kiểm tra nghỉ. Bỏ qua nghỉ ***/
                    
        //            int minutes = randomIn.Next(0, maxMinuteIn);
        //            string strTimeIn = endIn.Add(TimeSpan.FromMinutes(minutes)).ToString(Libs.cFormat.Hour);
        //            minutes = randomOut.Next(0, maxMinuteOut);
        //            string strTimeOut = startOut.Add(TimeSpan.FromMinutes(minutes)).ToString(Libs.cFormat.Hour);
        //            t.Add(new Tuple<string, string>(MaCC, strTimeIn));
        //            //Phân biệt giờ vào ngày hôm đó ở số giây (s) 1 giây là chấm vào, 2 giây là chấm ra
        //            type.SetValue(MaCC, dTemp.AddSeconds(1), strTimeIn, 1, true, ref tbl);

        //            /*** Nếu giờ hiện tại nhỏ hơn giờ quy định chấm ra, thì bỏ qua ***/
        //            type.SetValue(MaCC, dTemp.AddSeconds(2), strTimeOut, 1, true, ref tbl);
        //        }
        //        ArrayMaCC.Remove(MaCC);
        //    }


        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIRTUAL", _cnn);
        //            //System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Action", "POST");
        //            _cmd.Parameters.AddWithValue("@TYPE_ATTLOGS", tbl);
        //            ok.RowsAffected = _cmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ok.Errors.Add("GenerateVirtualTime", ex.Message);
        //    }
        //    if (Complete != null) Complete(ok);

        //}


        public void Delete(int Month, int Year,string MaNV, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_ATTLOGS_VIRTUAL_DEL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@Year", Year);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Delete", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
