﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsXuLyChamCong
    {
        public void GetBy(string DepId, string MaNV, DateTime FromDate, DateTime ToDate, Action<IEnumerable<Entity.Interfaces.IXuLyChamCongInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            List<Entity.clsXuLyChamCongInfo> Items = new List<Entity.clsXuLyChamCongInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIEW_BYDAY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@FromDate", FromDate);
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsXuLyChamCongInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(Items, ok);
        }

        public void GetPaging(int PageIndex, int PageSize, string DepId, string MaNV, DateTime FromDate, DateTime ToDate, Action<IEnumerable<Entity.Interfaces.IXuLyChamCongInfo>, Interfaces.IResultOk,int> Complete)
        {
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            List<Entity.clsXuLyChamCongInfo> Items = new List<Entity.clsXuLyChamCongInfo>();
            int RecordCount = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@DepId", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@FromDate", FromDate);
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate);

                    _cmd.Parameters.AddWithValue("@SearchTerm", MaNV);
                    _cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
                    _cmd.Parameters.AddWithValue("@PageSize", PageSize);
                    _cmd.Parameters.Add("@RecordCount", System.Data.SqlDbType.Int);
                    _cmd.Parameters["@RecordCount"].Direction = System.Data.ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (_cmd.Parameters["@RecordCount"].Value != null)
                        int.TryParse(_cmd.Parameters["@RecordCount"].Value.ToString(), out RecordCount);

                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsXuLyChamCongInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetPaging", ex.Message);
            }
            if (Complete != null) Complete(Items, ok,RecordCount);
        }

        #region Bo Qua
        //public void BatchDelete(IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void BatchInsert(IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void BatchUpdate(IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void GetBy(object Id, Action<Entity.Interfaces.IXuLyChamCongInfo, Exception> Complete)
        //{
        //}

        //public void GetAll(Action<IEnumerable<Entity.Interfaces.IXuLyChamCongInfo>, Exception> Complete)
        //{
        //}

        //public void Insert(Entity.Interfaces.IXuLyChamCongInfo item, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void Update(Entity.Interfaces.IXuLyChamCongInfo item, Action<Interfaces.IResultOk> Complete)
        //{
        //}

        //public void GetPaging(int pageIndex, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.IXuLyChamCongInfo>, Exception> Complete)
        //{
        //    totalPages = 0; totalRecords = 0;
        //    Exception _ex = null;
        //    List<Entity.clsXuLyChamCongInfo> Items = new List<Entity.clsXuLyChamCongInfo>();
        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            //string whereClause = string.Format("Where CONVERT(date,WorkingDay,121) between '{0}' and '{1}' order by IDNV,WorkingDay asc",
        //            //    FromDate.ToString(Libs.clsConsts.FormatYMD), ToDate.ToString(Libs.clsConsts.FormatYMD));
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIEW_BYDAY", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Columns", "*");
        //            _cmd.Parameters.AddWithValue("@TableName", "dbo.sp_MCC_ATTLOGS_VIEW_BYDAY");
        //            _cmd.Parameters.AddWithValue("@WhereClause", WhereClause);

        //            System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
        //            System.Data.DataTable tbl = new System.Data.DataTable();
        //            _adapter.Fill(tbl);
        //            if (tbl != null && tbl.Rows.Count > 0)
        //                Items = Libs.ConvertTableToList.ConvertToList<Entity.clsXuLyChamCongInfo>(tbl);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _ex = new Exception(ex.Message);
        //    }
        //    if (Complete != null) Complete(Items, _ex);
        //}
        //public void GetPaging(DateTime FromDate, DateTime ToDate, int pageIndex, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.IXuLyChamCongInfo>, Exception> Complete)
        //{
        //    totalPages = 0; totalRecords = 0;
        //    Exception _ex = null;
        //    List<Entity.clsXuLyChamCongInfo> Items = new List<Entity.clsXuLyChamCongInfo>();
        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            string whereClause = string.Format("Where CONVERT(date,WorkingDay,121) between '{0}' and '{1}' order by IDNV,WorkingDay asc",
        //                FromDate.ToString(Libs.clsConsts.FormatYMD), ToDate.ToString(Libs.clsConsts.FormatYMD));
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Columns", "*");
        //            _cmd.Parameters.AddWithValue("@TableName", "dbo.v_MCC_ATTLOGS_SESSION");
        //            _cmd.Parameters.AddWithValue("@WhereClause", whereClause);

        //            System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
        //            System.Data.DataTable tbl = new System.Data.DataTable();
        //            _adapter.Fill(tbl);
        //            if (tbl != null && tbl.Rows.Count > 0)
        //                Items = Libs.ConvertTableToList.ConvertToList<Entity.clsXuLyChamCongInfo>(tbl);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _ex = new Exception(ex.Message);
        //    }
        //    if (Complete != null) Complete(Items, _ex);

        //}

        //public Interfaces.IResultOk ValidateInsert(Entity.Interfaces.IXuLyChamCongInfo Item)
        //{
        //    throw new NotImplementedException();
        //}

        //public Interfaces.IResultOk ValidateUpdate(Entity.Interfaces.IXuLyChamCongInfo Item)
        //{
        //    throw new NotImplementedException();
        //}

        //public Interfaces.IResultOk ValidateDelete(Entity.Interfaces.IXuLyChamCongInfo Item)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

    }
}
