﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsQuyTacLamTron:Interfaces.IQuyTacLamTron
    {
        public System.Data.DataTable TYPE_MCC_QUYTACLAMTRON
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_MCC_QUYTACLAMTRON");
                _tbl.Columns.Add("Id", typeof(System.Int32));
                _tbl.Columns.Add("TuPhut", typeof(System.Int32));
                _tbl.Columns.Add("DenPhut", typeof(System.Int32));
                _tbl.Columns.Add("LamTron", typeof(System.Int32));
                return _tbl;
            }
        }
        private System.Data.DataTable SetType(IEnumerable<Entity.Interfaces.IQuyTacLamTronInfo> Items)
        {
            System.Data.DataTable tbl = this.TYPE_MCC_QUYTACLAMTRON;
            foreach (var item in Items)
            {
                System.Data.DataRow newRow = tbl.NewRow();
                newRow["Id"] = item.Id;
                newRow["TuPhut"] = item.TuPhut;
                newRow["DenPhut"] = item.DenPhut;
                newRow["LamTron"] = item.LamTron;
                tbl.Rows.Add(newRow);
            }
            return tbl;
        }
        public void GetAll(Action<List<Entity.clsQuyTacLamTronInfo>, Interfaces.IResultOk> Complete)
        {
            List<Entity.clsQuyTacLamTronInfo> Items = new List<Entity.clsQuyTacLamTronInfo>();
            Interfaces.IResultOk result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_QUYTACLAMTRON", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@Type", this.TYPE_MCC_QUYTACLAMTRON);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsQuyTacLamTronInfo>(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, result);
        }

        public void Update(int Id, int TuPhut, int DenPhut, int LamTron, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void Delete(int Id, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk result = new BaseResultOk();
            try
            {
                System.Data.DataTable tbl = this.TYPE_MCC_QUYTACLAMTRON;
                System.Data.DataRow newRow = tbl.NewRow();
                newRow["Id"] = Id;
                tbl.Rows.Add(newRow);
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_QUYTACLAMTRON", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.DELETE.ToString());
                    _cmd.Parameters.AddWithValue("@Type", tbl);

                    result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add("BatchUpdate", ex.Message);
            }
        }


        public void BatchUpdate(IEnumerable<Entity.Interfaces.IQuyTacLamTronInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_QUYTACLAMTRON", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@Type", this.SetType(Items));

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("BatchUpdate", ex.Message);
            }
        }


        public void LamTronPhut(int SoPhut, Action<int, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            int lamtron = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 LamTron ");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_QUYTACLAMTRON");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where {0}>=TuPhut AND {0}<=DenPhut ", SoPhut));
                    var reader= _cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        lamtron = (int)reader["LamTron"];
                        break;
                    }
                    reader.Close();
                    reader.Dispose();
                    reader = null;

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("LamTronPhut", ex.Message);
            }

            if (Complete != null) Complete(lamtron, ok);
        }
    }
}
