﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.ERP.Models
{
    public class clsAttLogsType
    {
        public System.Data.DataTable TYPE_ATTLOGS
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_ATTLOGS");
                _tbl.Columns.Add("MaCC", typeof(System.String));
                _tbl.Columns.Add("WorkingDay", typeof(System.DateTime));
                _tbl.Columns.Add("Gio", typeof(System.String));
                _tbl.Columns.Add("TrangThai", typeof(System.Int32));
                _tbl.Columns.Add("Door", typeof(System.String));
                _tbl.Columns.Add("DateTimeAtt", typeof(DateTime));
                _tbl.Columns.Add("CreateDateTime", typeof(System.DateTime));
                _tbl.Columns.Add("ModifyDateTime", typeof(System.DateTime));
                _tbl.Columns.Add("IsHand", typeof(System.Boolean));
                _tbl.Columns.Add("IsHopLe", typeof(System.Boolean));
                _tbl.Columns.Add("IsUsed", typeof(System.Boolean));
                _tbl.Columns.Add("LastRecordDate", typeof(System.DateTime));
                _tbl.Columns.Add("RecordReaded", typeof(System.Int32));

                return _tbl;
            }
        }

        public void SetValue(string MaCC, DateTime WorkingDay, string StrTime, int MachineNumber, bool IsHopLe, ref System.Data.DataTable tbl)
        {
            System.Data.DataRow newRow = tbl.NewRow();
            newRow["MaCC"] = MaCC;
            newRow["WorkingDay"] = WorkingDay.ToString("yyyy/MM/dd HH:mm:ss");
            newRow["Gio"] = StrTime;
            newRow["TrangThai"] = 0;
            newRow["Door"] = MachineNumber;
            newRow["DateTimeAtt"] = Libs.clsCommon.ConvertMMddyyy(DateTime.Now);
            newRow["IsHand"] = 0;
            newRow["IsHopLe"] = IsHopLe;
            newRow["IsUsed"] = 0;

            tbl.Rows.Add(newRow);
        }
    }
    public class clsAttLogs : Interfaces.IBase<ICardUserInfo>
    {
        //Ca lam viec
        private Models.Interfaces.IShift _Shift;
        //Lịch trình ca
        private Models.Interfaces.IScheduleSchift _ScheduleShift;

        private Models.Interfaces.IQuyTac _QuyTacChamCong;
        private Models.DIC.Interfaces.IDangKyCheDo _DangKyCheDo;
        private Models.Interfaces.INhanVien _NhanVienCC;
        public clsAttLogs(Models.Interfaces.INhanVien NhanVien, Models.Interfaces.IQuyTac QuyTacChamCong, Models.DIC.Interfaces.IDangKyCheDo DangKyCheDo,
            Models.Interfaces.IShift Shift,Models.Interfaces.IScheduleSchift LichTrinh)
        {
            this._QuyTacChamCong = QuyTacChamCong;
            this._DangKyCheDo = DangKyCheDo;
            this._NhanVienCC = NhanVien;
            this._ScheduleShift = LichTrinh;
            this._Shift = Shift;
        }
       
        public void BatchDelete(IEnumerable<ICardUserInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            
        }

        public void BatchInsert(IEnumerable<ICardUserInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                /*** Lấy danh sách Ca làm việc ***/
                this._Shift.GetAll((result, ex) => { if (ex.Success) _ShiftCollection = result; });

                clsAttLogsType type = new clsAttLogsType();
                int soPhutGiuaHaiLanCham = 0;
                
                this._QuyTacChamCong.SoPhutToiThieuGiuaHaiLanCham((result, ex) => {
                    error = ex;
                    soPhutGiuaHaiLanCham = result;
                });
                if (!error.Success || Items==null || Items.ToList().Count==0)
                    goto exit;
                
                System.Data.DataTable tbl = type.TYPE_ATTLOGS;
                System.Data.DataTable tblVirtual = type.TYPE_ATTLOGS;
                //Entity.clsShiftInfo shiftInfo = null;
                int k = 0;
                foreach (var item in Items)
                {
                    bool IsHopLe = true;
                    if (tbl.Rows.Count > 0)
                    {
                        var rows = tbl.AsEnumerable().Where(row => Convert.ToString(row["MaCC"]) == item.UserId &&
                            Convert.ToDateTime(row["WorkingDay"]).ToString(Libs.cFormat.YMD) == item.DateInOut.ToString(Libs.cFormat.YMD));
                                                  //.Max(row => row["Gio"]).ToString();
                        if (rows != null && rows.ToList().Count>0)
                        {
                            var max = rows.Max(row => row["Gio"]).ToString();
                            if (!string.IsNullOrWhiteSpace(max))
                            {
                                TimeSpan Time1, Time2;
                                if (TimeSpan.TryParse(max, out Time1) &&
                                TimeSpan.TryParse(item.StrTime, out Time2))
                                {
                                    var totalMinutes = Time2.TotalMinutes - Time1.TotalMinutes;
                                    if (totalMinutes < soPhutGiuaHaiLanCham)
                                        IsHopLe = false;
                                }
                            }
                        }
                    }

                    #region DỮ LIỆU CHẤM CÔNG THẬT
                    System.Data.DataRow newRow = tbl.NewRow();
                    newRow["MaCC"] = item.UserId;
                    newRow["WorkingDay"] = item.DateInOut.ToString("yyyy/MM/dd HH:mm:ss");
                    newRow["Gio"] = item.StrTime;
                    newRow["TrangThai"] = item.InOutMode;
                    newRow["Door"] = item.MachineNumber;
                    newRow["DateTimeAtt"] = Libs.clsCommon.ConvertMMddyyy(DateTime.Now);
                    newRow["IsHand"] = 0;
                    newRow["IsHopLe"] = IsHopLe;
                    newRow["IsUsed"] = 0;
                    
                    tbl.Rows.Add(newRow);
                    #endregion

                    #region XỬ LÝ DỮ LIỆU CHO PHẦN CHẤM CÔNG ẢO
                    newRow = tblVirtual.NewRow();
                    newRow["DateTimeAtt"] = item.DateInOut.ToString("yyyy/MM/dd HH:mm:ss");

                    #region Chưa cần chặn giờ

                    ///*** Lấy thông tin nhân viên để lấy lịch trình của nhân viên ***/
                    //Entity.Interfaces.INhanVienInfo NhanVienInfo = null;
                    //this._NhanVienCC.GeByMaCC(item.UserId, (result, ex) => { if (ex.Success)NhanVienInfo = result; });

                    //if (NhanVienInfo != null)
                    //{
                    //    /*** Kiểm tra đăng ký chế độ của nhân viên ***/
                    //    Random randomOut = new Random();
                    //    string _kyhieu = "";
                    //    DateTime dtNow = new DateTime(item.DateInOut.Year, item.DateInOut.Month, item.DateInOut.Day);
                    //    //Lấy ca làm việc của nhân viên có đăng ký chế độ
                    //    _DangKyCheDo.GetShift(NhanVienInfo.MaNV, item.DateInOut, (result, kyhieu, ex) =>
                    //    {
                    //        shiftInfo = result; _kyhieu = kyhieu;
                    //    });
                    //    //Nhân viên có đăng ký chế độ
                    //    //Nếu là nhân viên chế độ thì kiểm tra thời gian làm việc lọc bỏ đầu và bỏ cuối
                    //    if (shiftInfo != null)
                    //    {

                    //    }
                    //    else //Nếu không phải là nhân viên chế độ
                    //    {
                    //        /*** Lấy lịch trình ca làm việc của nhân viên ***/
                    //        List<Entity.clsScheduleShiftInfo> scheduleShiftCollection = null;
                    //        this._ScheduleShift.GetBy(NhanVienInfo.SCHName, (result, ex) =>
                    //        {
                    //            scheduleShiftCollection = result;
                    //        });

                    //        foreach (var itemShedule in scheduleShiftCollection)
                    //        {
                    //            shiftInfo = this.GetCaLamViec(item.DateInOut, itemShedule);
                    //            //Nếu lịch trình ngày hôm đó không có ca. Thì tiếp tục duyệt 
                    //            if (shiftInfo == null) continue;

                    //            TimeSpan tsOut, tsStrTime;
                    //            TimeSpan.TryParse(shiftInfo.TimeOut, out tsOut);
                    //            if (TimeSpan.TryParse(item.StrTime, out tsStrTime))
                    //            {
                    //                //Kiểm tra giờ chấm ra, trong khoảng chấm cho phép là 10 phút
                    //                //Ví dụ giờ cho cài đặt chấm ra là 16h: thì giờ cho phép chấm công từ 16h -> 16h10
                    //                //Nếu ngoài giờ đó thì chặn lại
                    //                if ((tsStrTime - tsOut).TotalMinutes > 10)
                    //                {
                    //                    //Random trong khoang 10 phut
                    //                    int minutes = randomOut.Next(0, 10);
                    //                    DateTime dtNew = new DateTime(item.DateInOut.Year, item.DateInOut.Month, item.DateInOut.Day, tsOut.Hours, minutes, 0);
                    //                    item.DateInOut = dtNew;
                    //                }

                    //            }

                    //            #region Ngày lễ
                    //            if (itemShedule.Ngay == 8)
                    //            {//Ngày lễ (Tết)

                    //                break;
                    //            }
                    //            #endregion

                    //            #region Chủ nhật
                    //            if (dtNow.DayOfWeek == DayOfWeek.Sunday && itemShedule.Ngay == 1)
                    //            {
                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 2
                    //            if (dtNow.DayOfWeek == DayOfWeek.Monday && itemShedule.Ngay == 2)
                    //            {//Thứ 2

                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 3
                    //            if (dtNow.DayOfWeek == DayOfWeek.Tuesday && itemShedule.Ngay == 3)
                    //            {//Thứ 3

                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 4
                    //            if (dtNow.DayOfWeek == DayOfWeek.Wednesday && itemShedule.Ngay == 4)
                    //            {//Thứ 4

                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 5
                    //            if (dtNow.DayOfWeek == DayOfWeek.Thursday && itemShedule.Ngay == 4)
                    //            {//Thứ 5
                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 6
                    //            if (dtNow.DayOfWeek == DayOfWeek.Friday && itemShedule.Ngay == 6)
                    //            {//Thứ 6

                    //                break;
                    //            }
                    //            #endregion

                    //            #region Thứ 7
                    //            if (dtNow.DayOfWeek == DayOfWeek.Saturday && itemShedule.Ngay == 4)
                    //            {//Thứ 7
                    //                break;
                    //            }
                    //            #endregion

                    //            break;
                    //        }
                    //    }
                    //}

                    #endregion

                    newRow["MaCC"] = item.UserId;
                    newRow["WorkingDay"] = item.DateInOut.ToString("yyyy/MM/dd HH:mm:ss");
                    newRow["Gio"] = item.StrTime;
                    newRow["TrangThai"] = item.InOutMode;
                    newRow["Door"] = item.MachineNumber;
                    newRow["IsHand"] = 0;
                    newRow["IsHopLe"] = IsHopLe;
                    newRow["IsUsed"] = 0;

                    tblVirtual.Rows.Add(newRow);
                    #endregion

                    k++;
                }

                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlTransaction _tran = _cnn.BeginTransaction();

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS", _cnn, _tran);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //CẬP NHẬT VÀO BẢNG CHẤM CÔNG THẬT
                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@TYPE_ATTLOGS", tbl);
                    error.RowsAffected = _cmd.ExecuteNonQuery();

                    //CẬP NHẬT VÀO BẢNG CHẤM CÔNG ẢO
                    _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_ATTLOGS_VIRTUAL", _cnn,_tran);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@TYPE_ATTLOGS", tblVirtual);
                    error.RowsAffected=_cmd.ExecuteNonQuery();

                    _tran.Commit();

                }
            }
            catch (Exception ex)
            {
                error.Errors.Add(Guid.NewGuid().ToString(), ex.Message);
            }
            exit:
            if (Complete != null) Complete(error);
        }

        List<Entity.clsShiftInfo> _ShiftCollection = null;
        private Entity.clsShiftInfo GetCaLamViec(DateTime WorkingDay, string ShiftId)
        {
            //Khai báo giờ bắt đầu và kết thức để hiểu ca
            TimeSpan tsBegin, tsEnd, tsIn;
            if (!string.IsNullOrWhiteSpace(ShiftId))
            {
                var fItem = this._ShiftCollection.Find(f => f.ShiftId.Equals(ShiftId));
                if (fItem != null)
                {
                    //Kiểm tra ngày áp dụng ca
                    //WorkingDay: Ngày chấm công; TuNgay: là ngày ca được áp dụng
                    //Ngày chấm công phải lơn hơn hoặc bằng ngày áp dụng thì mới tính ca
                    if (DateTime.Compare(WorkingDay, fItem.TuNgay) == -1)
                        return null;
                    return fItem;
                    
                }
            }

            return null;
        }
        private Entity.clsShiftInfo GetCaLamViec(DateTime WorkingDay, Entity.clsScheduleShiftInfo scheduleShiftInfo)
        {
            // Cần thêm giờ để hiểu ca
            Entity.clsShiftInfo Info = null;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift1);
            if (Info != null) return Info;

            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift2);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift3);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift4);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift5);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift6);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift7);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift8);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, scheduleShiftInfo.Shift9);
            if (Info != null) return Info;

            return Info;
        }
        /// <summary>
        /// Xử lý cho ra dữ liệu ảo
        /// </summary>
        /// <param name="Items"></param>
        /// <param name="ItemsVirtual"></param>
        private void XuLyDuLieuVirtual(List<ICardUserInfo> Items, out List<ICardUserInfo> ItemsVirtual)
        {
            ItemsVirtual = new List<ICardUserInfo>();
            if (Items == null) return;
            NtbSoft.ERP.Models.DIC.Interfaces.IDangKyCheDo _chedo = new ERP.Models.DIC.clsDangKyCheDo();
            Entity.clsShiftInfo shiftInfo = null;
            foreach (var item in Items)
            {

                string _kyhieu = "";
                _chedo.GetShift(item.UserId, item.DateInOut, (result, kyhieu, ex) =>
                {
                    shiftInfo = result; _kyhieu = kyhieu;
                });
            }
        }
        public void BatchUpdate(IEnumerable<ICardUserInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void GetBy(object Id, Action<ICardUserInfo, Exception> Complete)
        {
        }

        public void GetAll(Action<IEnumerable<ICardUserInfo>, Exception> Complete)
        {
        }

        public void Insert(ICardUserInfo item, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void Update(ICardUserInfo item, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void GetPaging(int pageIndex, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<ICardUserInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
        }

        public void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<ICardUserInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
        }

        public Interfaces.IResultOk ValidateInsert(ICardUserInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateUpdate(ICardUserInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateDelete(ICardUserInfo Item)
        {
            return new BaseResultOk();
        }


    }
}
