﻿using NtbSoft.ERP.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    /// <summary>
    /// Xử lý dữ liệu tải thật
    /// </summary>
    public class clsXuLyDuLieuTaiThat :Interfaces.IXuLyDuLieuTai
    {
        //Ca lam viec
        private Models.Interfaces.IShift _mShift;
        internal Interfaces.IQuyTac _QuyTac;
        private string _MaCC = "";
        private int _mSoPhut2LanCham = 0;
        public clsXuLyDuLieuTaiThat(Models.Interfaces.IShift Shift, Interfaces.IQuyTac QuyTac)
        {
            _mShift = Shift;
            _QuyTac = QuyTac;
        }

        /// <summary>
        /// Hàm kiểm tra chuỗi giờ chấm có phải là định dạng thời gian hay không
        /// </summary>
        /// <param name="GioCham"></param>
        /// <returns></returns>
        public bool IsTime(string GioCham)
        {
            TimeSpan tmp;
            return TimeSpan.TryParse(GioCham,out tmp);
        }

        public bool IsTime(string GioCham,out TimeSpan OutGioCham)
        {
            return TimeSpan.TryParse(GioCham, out OutGioCham);
        }

        public bool CheckHopLe(string MaCC, DateTime Ngay, string GioCham)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BreakInOfShift(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void CheckExistInOut(string MaNV, string WorkingDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void NumberShift(string SchID, Action<int, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void ReturnEmpID(string MaCC, Action<double, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            double id = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 Id ");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' ", MaCC));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        id = Convert.ToDouble(_reader["id"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("ReturnEmpID", ex.Message);
            }
            if (Complete != null) Complete(id, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<int, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            int SoLan = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Count(MaCC) as SoLan");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        SoLan = Convert.ToInt32(_reader["SoLan"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("SoLanCham", ex.Message);
            }
            if (Complete != null) Complete(SoLan, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<List<TimeSpan>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<TimeSpan> mTime = new List<TimeSpan>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        TimeSpan mTemp;
                        if (TimeSpan.TryParse(Convert.ToString(_reader["Gio"]), out mTemp))
                            mTime.Add(mTemp);
                        //SoLan = Convert.ToInt32(_reader["SoLan"]);
                        //break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("SoLanCham", ex.Message);
            }
            if (Complete != null) Complete(mTime, error);
        }

        public void MinGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MIN(Gio) as Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        public void MaxGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MAX(Gio) as Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        /// <summary>
        /// Giờ ra (giờ ra nghỉ trưa)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakOutGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            //////bool flag = Information.IsDate(MinGioQuet) & Information.IsDate(MaxGioQuet);
            //////if (flag)
            //////{
            //////    string text = string.Concat(new string[]
            //////    {
            //////        "SELECT Min(GioQuet) as Gio FROM ",
            //////        TenBang,
            //////        " WHERE WorkingDay=Convert(DateTime,'",
            //////        Ngay,
            //////        "',103) AND MaCC='",
            //////        CardID,
            //////        "'\r\n"
            //////    });
            //////    text = string.Concat(new string[]
            //////    {
            //////        text,
            //////        " AND GioQuet >'",
            //////        MinGioQuet,
            //////        "' AND GioQuet < '",
            //////        MaxGioQuet,
            //////        "' AND chkUsed=0"
            //////    });
            //////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            //////    flag = (dataSet.Tables[0].Rows.Count > 0);
            //////    if (flag)
            //////    {
            //////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            //////    }
            //////}
        }

        /// <summary>
        /// Giờ vào (giờ sau khi nghỉ trưa vào)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="BreakOutGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, string BreakOutGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            ////bool flag = Information.IsDate(MaxGioQuet) & Information.IsDate(BreakOutGioQuet);
            ////if (flag)
            ////{
            ////    string text = string.Concat(new string[]
            ////    {
            ////        "SELECT Max(GioQuet) as Gio FROM ",
            ////        TenBang,
            ////        " WHERE WorkingDay=Convert(DateTime,'",
            ////        Ngay,
            ////        "',103) AND MaCC='",
            ////        CardID,
            ////        "'\r\n"
            ////    });
            ////    text = string.Concat(new string[]
            ////    {
            ////        text,
            ////        " AND GioQuet < '",
            ////        MaxGioQuet,
            ////        "'  AND GioQuet > '",
            ////        BreakOutGioQuet,
            ////        "' AND chkUsed=0"
            ////    });
            ////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            ////    flag = (dataSet.Tables[0].Rows.Count > 0);
            ////    if (flag)
            ////    {
            ////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            ////    }
            ////}
        }


        private void GetBreakOutInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, out string BreakOutGioCham, out string BreakInGioCham, Action<Interfaces.IResultOk> Complete)
        {
            BreakOutGioCham = ""; BreakInGioCham = "";
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", Ngay);
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);
                    _cmd.Parameters.AddWithValue("@MinGioCham", MinGioCham);
                    _cmd.Parameters.AddWithValue("@MaxGioCham", MaxGioCham);

                    _cmd.Parameters.Add("@BreakOut", SqlDbType.VarChar,8);
                    _cmd.Parameters["@BreakOut"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@BreakIn", SqlDbType.VarChar,8);
                    _cmd.Parameters["@BreakIn"].Direction = ParameterDirection.Output;

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                    if (_cmd.Parameters["@BreakOut"].Value != null)
                        BreakOutGioCham = _cmd.Parameters["@BreakOut"].Value.ToString();
                    if (_cmd.Parameters["@BreakIn"].Value != null)
                        BreakInGioCham = _cmd.Parameters["@BreakIn"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("BreakOutInGioCham", ex.Message);
            }
        }

        public void XuLyDuLieuChamCong(string MaCC, DateTime CurDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool _flagHopLe = false;
            int SoLan = 0,EmpId =0;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            this.ReturnEmpID(MaCC, (result, complete) => { EmpId = (int)result; });
            if (EmpId == 0) return;

            /*** Lấy ca làm việc ***/
            Entity.clsShiftInfo mShiftInfo = null;
            int Ngay = 1;
            if (CurDay.DayOfWeek == DayOfWeek.Sunday) Ngay = 1;
            else if (CurDay.DayOfWeek == DayOfWeek.Monday) Ngay = 2;
            else if (CurDay.DayOfWeek == DayOfWeek.Tuesday) Ngay = 3;
            else if (CurDay.DayOfWeek == DayOfWeek.Wednesday) Ngay = 4;
            else if (CurDay.DayOfWeek == DayOfWeek.Thursday) Ngay = 5;
            else if (CurDay.DayOfWeek == DayOfWeek.Friday) Ngay = 6;
            else if (CurDay.DayOfWeek == DayOfWeek.Saturday) Ngay = 7;

            List<Entity.Interfaces.IDangKyRaVaoInfo> mArrayGioVaoRa = new List<Entity.Interfaces.IDangKyRaVaoInfo>();

            string MinGio = "", MaxGio = "",BreakOut="",BreakIn="";
            //this.SoLanCham(MaCC, CurDay, (result, complete) => { SoLan = result; });
            //Lấy số lần chấm công của nhân viên 
            this.SoLanCham(MaCC, CurDay, (result, complete) => { mArrayGio = result; });
            mArrayGio.Sort();
            SoLan = mArrayGio.Count;
            
            //this.MinGioCham(MaCC, CurDay, (result, complete) => { MinGio = result; });
            //this.MaxGioCham(MaCC, CurDay, (result, complete) => { MaxGio = result; });

            if (_mSoPhut2LanCham == 0)
                _QuyTac.SoPhutToiThieuGiuaHaiLanCham((result, complete) => { _mSoPhut2LanCham = result; });
            if (mArrayGio.Count > 0)
            {
                /*** Lấy ca làm việc ***/
                _mShift.GetBy(EmpId, Ngay, (result, complete) => { mShiftInfo = result; });
                if (string.IsNullOrEmpty(mShiftInfo.TimeIn)) mShiftInfo.TimeIn = "00:00";

                TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
                //string mGioVao = "", mGioRa = "";
                while (mArrayGio.Count > 0)
                {
                    #region Vào
                    //Lấy vào lần đầu tiên
                    if (string.IsNullOrEmpty(MinGio))
                    {
                        mTempGioVao = mArrayGio[0];
                        MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                        //Kiểm tra đi muộn 
                        if (TimeSpan.Parse(mShiftInfo.TimeIn)!=TimeSpan.Zero)
                            if (mTempGioVao > TimeSpan.Parse(mShiftInfo.TimeIn))
                            {
                                string mSoPhut = (mTempGioVao - TimeSpan.Parse(mShiftInfo.TimeIn)).TotalMinutes.ToString();
                                //Đang ký đi muộn
                                mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo() { MaNV = EmpId.ToString(), NgayDangKy = CurDay, GioVao = MinGio, GioRa = "", ThoiGian = mSoPhut, GhiChu = "Đi muôn" });
                            }

                        mArrayGio.Remove(mTempGioVao);
                        //continue;
                    }
                    else if (mTempGioVao == TimeSpan.Zero)
                    {
                    tt:
                        MaxGio = "";
                        if (mArrayGio.Count > 0)
                        {
                            if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
                            {
                                mArrayGio.Remove(mTempGioVao);
                                goto tt;
                            }
                            mTempGioVao = mArrayGio[0];
                            mArrayGio.Remove(mTempGioVao);
                        }

                        if (TimeSpan.Parse(mShiftInfo.TimeIn) != TimeSpan.Zero)
                            if (mTempGioRa != TimeSpan.Zero && mTempGioVao != TimeSpan.Zero)
                            {
                                mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                                {
                                    GioRa = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                                    GioVao = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour),
                                    ThoiGian = (mTempGioVao - mTempGioRa).TotalMinutes.ToString(),
                                    MaNV = EmpId.ToString(),
                                    NgayDangKy = CurDay
                                });
                            }
                    }
                    #endregion

                    #region Ra
                    if (mArrayGio.Count > 0)
                    {
                        MaxGio = "";
                        //Kiểm tra giờ ra kế có hợp lệ hay không
                        if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
                        { //Hợp lệ
                            mTempGioRa = mArrayGio[0];//Gán giờ ra
                            MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                            mTempGioVao = TimeSpan.Zero;
                        }
                        mArrayGio.Remove(mArrayGio[0]);//Gán giờ vào
                    }

                    #endregion

                    //Trường hợp không còn khoảng giờ nào và giờ vào khác giờ ra
                    if (mArrayGio.Count == 0 && MaxGio != "" &&  TimeSpan.Parse(mShiftInfo.TimeIn)!=TimeSpan.Zero)
                    {//Kiểm tra về sớm
                        if (!string.IsNullOrEmpty(mShiftInfo.BatDauTinhSom) &&
                            !string.IsNullOrEmpty(mShiftInfo.KetThucTinhSom) && !string.IsNullOrEmpty(mShiftInfo.TimeOut)
                            && TimeSpan.Parse(MaxGio)<TimeSpan.Parse(mShiftInfo.TimeOut))
                        {//Giờ chấm ra nhỏ hơn giờ làm việc
                            string mGhiChu = "";
                            if (TimeSpan.Parse(MaxGio) >= TimeSpan.Parse(mShiftInfo.BatDauTinhSom) &&
                                TimeSpan.Parse(MaxGio) < TimeSpan.Parse(mShiftInfo.KetThucTinhSom))
                                mGhiChu = "Về sớm";
                           
                            mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                            {
                                GioRa = MaxGio,
                                GhiChu = mGhiChu,
                                MaNV = EmpId.ToString(),
                                NgayDangKy = CurDay
                            });
                        }
                    }

                }//End while==============================================================================================

            }

            #region TESSSSSSSSSSSS
            /** Trường hợp 1 lần chấm ***/
            ////_flagHopLe = (SoLan == 1);
            ////if (_flagHopLe)
            ////{
            ////    MaxGio = "";
            ////}
            /////*** Kiểm tra thời gian giữa 2 lần chấm ***/
            ////if (!string.IsNullOrEmpty(MaxGio))
            ////{//Kiểm tra giữa 2 lần chấm nếu nếu số phút nhỏ hơn n phút, 
            ////    TimeSpan tIn, tOut;
            ////    if (IsTime(MinGio, out tIn) && IsTime(MaxGio, out tOut))
            ////    {
            ////        if ((tOut - tIn).TotalMinutes < _mSoPhut2LanCham)
            ////            MaxGio = "";
            ////    }
            ////}

            ///** Trường hợp 2 lần chấm ***/
            //_flagHopLe = (SoLan == 2);
            //if (_flagHopLe)
            //{

            //}
            ///** Trường hợp 3 lần chấm (Lúc này sẽ kiểm tra giờ ra vào trước và sau khi nghỉ trưa) ***/
            //_flagHopLe = (SoLan == 3);
            //if (_flagHopLe)
            //{
            //    this.GetBreakOutInGioCham(MaCC, CurDay, MinGio, MaxGio, out BreakOut, out BreakIn, ex => { });
            //    //CheckOverNight
            //}

            ///*** Tính toán để đưa về bảng đăng ký vào ra ***/
            //if (SoLan > 1)
            //{

            //}
            //if (MaCC == "369")
            //{

            //}
           
            //Kiểm tra nếu thời gian lần đầu chấm nhỏ hơn thời gian làm việc thì lưu vào bảng đăng ký vào ra
            //if (mArrayGio.Count > 0)
            //{
            //    if (mArrayGio.Count == 1)
            //    {
            //        //Chỉ chấm vào. Kiểm tra giờ vào có chấm sau giờ làm việc hay không
            //        //Nếu giờ chấm công sau giờ làm việc thì đăng ký cho đi muộn
            //    }
            //    else if (mArrayGio.Count == 2)
            //    {
            //        //Nếu là 2 lần chấm thì kiểm tra lần đầu và lần cuối.
            //        //Nếu lần chấm đẩu chấm sau giờ làm việc thì cho vào bảng vào ra đi muộn
            //        //Nếu lần chấm cuối chấm trước giờ về thì cho vào bảng vào ra về sớm
                    
            //    }
            //    else
            //    {
            //        //Kiểm tra các lần chấm

            //    }
            //}
            #endregion

            string DateIn,DateOut;
            DateIn = DateOut = CurDay.ToString(Libs.clsConsts.FormatMDY);
            //if (this.IsTime(MinGio))
                DateIn = DateIn + " " + MinGio;
            //if (this.IsTime(MaxGio))
                DateOut = DateOut + " " + MaxGio;
                
            Interfaces.IResultOk error = new BaseResultOk();
            bool bl=false;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", CurDay.ToString(Libs.clsConsts.FormatMDY));
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.Parameters.AddWithValue("@MaNV", EmpId);
                    _cmd.Parameters.AddWithValue("@TimeIn", MinGio);
                    _cmd.Parameters.AddWithValue("@BreakOut", BreakOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", BreakIn);
                    
                    _cmd.Parameters.AddWithValue("@TimeOut", MaxGio);
                    _cmd.Parameters.AddWithValue("@DateTimeIn", DateIn);
                    _cmd.Parameters.AddWithValue("@DateBreakOut", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateBreakIn", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateTimeOut", DateOut);

                   bl = _cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("XuLyDuLieuChamCong", ex.Message);
            }
            if (this._MaCC != MaCC)
            {
                this._MaCC = MaCC;
                /*** Lấy ngày cuối cùng của 1 tháng ***/
                DateTime tmp = Libs.clsCommon.LastDayOfMonth(CurDay.Month, CurDay.Year);
                /*** Thiết lập các ngày trong 1 tháng ***/
                this.InitializeDate(CurDay.Month, tmp.Day, CurDay.Year, MaCC, ex => { });

                /*** Cập nhật về bảng đăng ký vào ra (Tự động chạy về bảng đăng ký) ***/
                //- Giờ chấm vào sau giờ làm việc
                //- Số lần chấm vào ra lớn hơn 2 lần
                Models.DIC.Interfaces.IDangKyVaoRa mObjVaoRa = new DIC.clsDangKyVaoRa();
                mObjVaoRa.Update(Libs.ApiAction.POST, mArrayGioVaoRa, (complete) => { });
            }
            if (Complete != null) Complete(bl, error);

        }


        public void XuLyDuLieuChamCong_OK1(string MaCC, DateTime CurDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool _flagHopLe = false;
            int SoLan = 0, EmpId = 0;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            this.ReturnEmpID(MaCC, (result, complete) => { EmpId = (int)result; });
            if (EmpId == 0) return;

            /*** Lấy ca làm việc ***/
            Entity.clsShiftInfo mShiftInfo = null;
            int Ngay = 1;
            if (CurDay.DayOfWeek == DayOfWeek.Sunday) Ngay = 1;
            else if (CurDay.DayOfWeek == DayOfWeek.Monday) Ngay = 2;
            else if (CurDay.DayOfWeek == DayOfWeek.Tuesday) Ngay = 3;
            else if (CurDay.DayOfWeek == DayOfWeek.Wednesday) Ngay = 4;
            else if (CurDay.DayOfWeek == DayOfWeek.Thursday) Ngay = 5;
            else if (CurDay.DayOfWeek == DayOfWeek.Friday) Ngay = 6;
            else if (CurDay.DayOfWeek == DayOfWeek.Saturday) Ngay = 7;

            List<Entity.Interfaces.IDangKyRaVaoInfo> mArrayGioVaoRa = new List<Entity.Interfaces.IDangKyRaVaoInfo>();

            string MinGio = "", MaxGio = "", BreakOut = "", BreakIn = "";
            //this.SoLanCham(MaCC, CurDay, (result, complete) => { SoLan = result; });
            //Lấy số lần chấm công của nhân viên 
            this.SoLanCham(MaCC, CurDay, (result, complete) => { mArrayGio = result; });
            mArrayGio.Sort();
            SoLan = mArrayGio.Count;

            //this.MinGioCham(MaCC, CurDay, (result, complete) => { MinGio = result; });
            //this.MaxGioCham(MaCC, CurDay, (result, complete) => { MaxGio = result; });

            if (_mSoPhut2LanCham == 0)
                _QuyTac.SoPhutToiThieuGiuaHaiLanCham((result, complete) => { _mSoPhut2LanCham = result; });
            if (mArrayGio.Count > 0)
            {
                /*** Lấy ca làm việc ***/
                _mShift.GetBy(EmpId, Ngay, (result, complete) => { mShiftInfo = result; });
                if (string.IsNullOrEmpty(mShiftInfo.TimeIn)) mShiftInfo.TimeIn = "00:00";

                TimeSpan mTempGioRa = TimeSpan.Zero, mTempGioVao = TimeSpan.Zero;
                string mGioVao = "", mGioRa = "";
                while (mArrayGio.Count > 0)
                {
                    //TimeSpan mGioRa = TimeSpan.Zero, mGioVao = TimeSpan.Zero;
                    string mSoPhut = "", mGhiChu = "";
                    #region Vào
                    //Lấy vào lần đầu tiên
                    if (string.IsNullOrEmpty(MinGio))
                    {
                        mTempGioVao = mArrayGio[0];
                        MinGio = mTempGioVao.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                        //Kiểm tra đi muộn 
                        if (mTempGioVao > TimeSpan.Parse(mShiftInfo.TimeIn))
                        {
                            mSoPhut = (mTempGioVao - TimeSpan.Parse(mShiftInfo.TimeIn)).TotalMinutes.ToString();
                            mGhiChu = "Đi muộn";
                            mGioVao = MinGio;
                        }
                        mArrayGio.Remove(mTempGioVao);
                    }
                    else if (mTempGioVao == mTempGioRa)
                    {
                        mGioVao = "";
                        if ((mArrayGio[0] - mTempGioRa).TotalMinutes < _mSoPhut2LanCham)
                        {
                            mArrayGio.Remove(mTempGioVao);
                            continue;
                        }
                        mTempGioVao = mArrayGio[0];
                        mGioVao = mTempGioVao.ToString(Libs.clsConsts.FormatHour);

                        mArrayGio.Remove(mTempGioVao);
                    }
                    #endregion

                    #region Ra
                    mGioRa = "";
                    if (mArrayGio.Count > 0)
                    {
                        MaxGio = "";
                        //Kiểm tra giờ ra kế có hợp lệ hay không
                        if ((mArrayGio[0] - mTempGioVao).TotalMinutes >= _mSoPhut2LanCham)
                        { //Hợp lệ
                            mTempGioRa = mArrayGio[0];
                            MaxGio = mTempGioRa.ToString(NtbSoft.ERP.Libs.cFormat.Hour);
                            //Kiểm tra về sớm 
                            //if (mTempGioRa > TimeSpan.Parse(mShiftInfo.BatDauTinhSom) && mTempGioRa < TimeSpan.Parse(mShiftInfo.KetThucTinhSom))
                            mGioRa = mTempGioRa.ToString(Libs.clsConsts.FormatHour);
                            //Nếu giờ chấm vào sau giờ làm việc thì add vào
                            if (mTempGioVao > TimeSpan.Parse(mShiftInfo.TimeIn))
                                mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                                {
                                    MaNV = EmpId.ToString(),
                                    NgayDangKy = CurDay,
                                    GioVao = mGioVao,
                                    GioRa = mGioRa,
                                    ThoiGian = mSoPhut,
                                    GhiChu = mGhiChu
                                });
                            //Lấy cặp vào - ra
                            //mGioVao = mTempGioVao;
                            //mGioRa = mTempGioRa;
                            mTempGioVao = mTempGioRa;
                        }
                        mArrayGio.Remove(mArrayGio[0]);
                    }

                    #endregion

                    //Trường hợp không còn khoảng giờ nào và giờ vào khác giờ ra
                    if (mArrayGio.Count == 0 && mTempGioVao != mTempGioRa)
                    {
                        if (mTempGioVao > TimeSpan.Parse(mShiftInfo.TimeIn))
                            mArrayGioVaoRa.Add(new clsDangKyRaVaoInfo()
                            {
                                MaNV = EmpId.ToString(),
                                NgayDangKy = CurDay,
                                GioVao = mGioVao,
                                GioRa = mGioRa
                            });
                    }

                }//End while==============================================================================================

            }

            #region TESSSSSSSSSSSS
            /** Trường hợp 1 lần chấm ***/
            ////_flagHopLe = (SoLan == 1);
            ////if (_flagHopLe)
            ////{
            ////    MaxGio = "";
            ////}
            /////*** Kiểm tra thời gian giữa 2 lần chấm ***/
            ////if (!string.IsNullOrEmpty(MaxGio))
            ////{//Kiểm tra giữa 2 lần chấm nếu nếu số phút nhỏ hơn n phút, 
            ////    TimeSpan tIn, tOut;
            ////    if (IsTime(MinGio, out tIn) && IsTime(MaxGio, out tOut))
            ////    {
            ////        if ((tOut - tIn).TotalMinutes < _mSoPhut2LanCham)
            ////            MaxGio = "";
            ////    }
            ////}

            ///** Trường hợp 2 lần chấm ***/
            //_flagHopLe = (SoLan == 2);
            //if (_flagHopLe)
            //{

            //}
            ///** Trường hợp 3 lần chấm (Lúc này sẽ kiểm tra giờ ra vào trước và sau khi nghỉ trưa) ***/
            //_flagHopLe = (SoLan == 3);
            //if (_flagHopLe)
            //{
            //    this.GetBreakOutInGioCham(MaCC, CurDay, MinGio, MaxGio, out BreakOut, out BreakIn, ex => { });
            //    //CheckOverNight
            //}

            ///*** Tính toán để đưa về bảng đăng ký vào ra ***/
            //if (SoLan > 1)
            //{

            //}
            //if (MaCC == "369")
            //{

            //}

            //Kiểm tra nếu thời gian lần đầu chấm nhỏ hơn thời gian làm việc thì lưu vào bảng đăng ký vào ra
            //if (mArrayGio.Count > 0)
            //{
            //    if (mArrayGio.Count == 1)
            //    {
            //        //Chỉ chấm vào. Kiểm tra giờ vào có chấm sau giờ làm việc hay không
            //        //Nếu giờ chấm công sau giờ làm việc thì đăng ký cho đi muộn
            //    }
            //    else if (mArrayGio.Count == 2)
            //    {
            //        //Nếu là 2 lần chấm thì kiểm tra lần đầu và lần cuối.
            //        //Nếu lần chấm đẩu chấm sau giờ làm việc thì cho vào bảng vào ra đi muộn
            //        //Nếu lần chấm cuối chấm trước giờ về thì cho vào bảng vào ra về sớm

            //    }
            //    else
            //    {
            //        //Kiểm tra các lần chấm

            //    }
            //}
            #endregion

            string DateIn, DateOut;
            DateIn = DateOut = CurDay.ToString(Libs.clsConsts.FormatMDY);
            //if (this.IsTime(MinGio))
            DateIn = DateIn + " " + MinGio;
            //if (this.IsTime(MaxGio))
            DateOut = DateOut + " " + MaxGio;

            Interfaces.IResultOk error = new BaseResultOk();
            bool bl = false;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", CurDay.ToString(Libs.clsConsts.FormatMDY));
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.Parameters.AddWithValue("@MaNV", EmpId);
                    _cmd.Parameters.AddWithValue("@TimeIn", MinGio);
                    _cmd.Parameters.AddWithValue("@BreakOut", BreakOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", BreakIn);

                    _cmd.Parameters.AddWithValue("@TimeOut", MaxGio);
                    _cmd.Parameters.AddWithValue("@DateTimeIn", DateIn);
                    _cmd.Parameters.AddWithValue("@DateBreakOut", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateBreakIn", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateTimeOut", DateOut);

                    bl = _cmd.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("XuLyDuLieuChamCong", ex.Message);
            }
            if (this._MaCC != MaCC)
            {
                this._MaCC = MaCC;
                /*** Lấy ngày cuối cùng của 1 tháng ***/
                DateTime tmp = Libs.clsCommon.LastDayOfMonth(CurDay.Month, CurDay.Year);
                /*** Thiết lập các ngày trong 1 tháng ***/
                this.InitializeDate(CurDay.Month, tmp.Day, CurDay.Year, MaCC, ex => { });

                /*** Cập nhật về bảng đăng ký vào ra (Tự động chạy về bảng đăng ký) ***/
                //- Giờ chấm vào sau giờ làm việc
                //- Số lần chấm vào ra lớn hơn 2 lần
                Models.DIC.Interfaces.IDangKyVaoRa mObjVaoRa = new DIC.clsDangKyVaoRa();
                mObjVaoRa.Update(Libs.ApiAction.POST, mArrayGioVaoRa, (complete) => { });
            }
            if (Complete != null) Complete(bl, error);

        }

        public void CheckTonTaiBang(string TableName, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }


        public void InitializeDate(int Month, int MaxDay, int Year, string MaCC, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@MaxDay",MaxDay);
                    _cmd.Parameters.AddWithValue("@Year", Year);
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        public void InitializeDate(int Month, int MaxDay, int Year, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@MaxDay", MaxDay);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

       // public void CapNhatRaVao()

    }
}
