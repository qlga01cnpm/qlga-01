﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    /// <summary>
    /// Nhật ký chấm công
    /// </summary>
    public class clsNhatKyChamCong:Interfaces.IBase<Entity.Interfaces.INhatKyChamCongInfo>
    {

        public void BatchDelete(IEnumerable<Entity.Interfaces.INhatKyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void BatchInsert(IEnumerable<Entity.Interfaces.INhatKyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void BatchUpdate(IEnumerable<Entity.Interfaces.INhatKyChamCongInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void GetBy(object Id, Action<Entity.Interfaces.INhatKyChamCongInfo, Exception> Complete)
        {

        }

        public void GetPaging(int pageIndex, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhatKyChamCongInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
        }
       
        public void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.INhatKyChamCongInfo>, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            Exception _ex = null;
            List<Entity.clsNhatKyChamCongInfo> Items = new List<Entity.clsNhatKyChamCongInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    //
                    ////string whereClause = string.Format("Where CONVERT(date,WorkingDay,121) between '{0}' and '{1}' order by IDNV,WorkingDay asc",
                    ////    FromDate.ToString(Libs.clsConsts.FormatYMD), ToDate.ToString(Libs.clsConsts.FormatYMD));
                    ////System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    ////_cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    ////_cmd.Parameters.AddWithValue("@Columns", "*");
                    ////_cmd.Parameters.AddWithValue("@TableName", "dbo.v_MCC_NHATKYCHAMCONG");
                    ////_cmd.Parameters.AddWithValue("@WhereClause", whereClause);

                   
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_NHATKYCHAMCONG_GET_PAGING", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@FromDate", FromDate);
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate);
                    _cmd.Parameters.AddWithValue("@SearchTerm", "");

                    _cmd.Parameters.AddWithValue("@PageIndex", selectedPage);
                    _cmd.Parameters.AddWithValue("@PageSize", pageSize);

                    _cmd.Parameters.Add("@TotalPages", System.Data.SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = System.Data.ParameterDirection.Output;
                    _cmd.Parameters.Add("@RecordCount", System.Data.SqlDbType.Int);
                    _cmd.Parameters["@RecordCount"].Direction = System.Data.ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);

                    if (_cmd.Parameters["@TotalPages"].Value != null)
                        int.TryParse(_cmd.Parameters["@TotalPages"].Value.ToString(), out totalRecords);
                    if (_cmd.Parameters["@RecordCount"].Value != null)
                        int.TryParse(_cmd.Parameters["@RecordCount"].Value.ToString(), out totalRecords);

                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsNhatKyChamCongInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }

        public void GetAll(Action<IEnumerable<Entity.Interfaces.INhatKyChamCongInfo>, Exception> Complete)
        {
        }

        public void Insert(Entity.Interfaces.INhatKyChamCongInfo item, Action<Interfaces.IResultOk> Complete)
        {
        }

        public void Update(Entity.Interfaces.INhatKyChamCongInfo item, Action<Interfaces.IResultOk> Complete)
        {
        }

      

        public Interfaces.IResultOk ValidateInsert(Entity.Interfaces.INhatKyChamCongInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateUpdate(Entity.Interfaces.INhatKyChamCongInfo Item)
        {
            return new BaseResultOk();
        }

        public Interfaces.IResultOk ValidateDelete(Entity.Interfaces.INhatKyChamCongInfo Item)
        {
            return new BaseResultOk();
        }








      
    }
}
