﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsBcChiTietNgay : Interfaces.IBcChiTietNgay
    {
        /// <summary>
        /// MaCheDo=0: Lấy tất cả các chế độ
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="DepID"></param>
        /// <param name="MaCheDo"></param>
        /// <param name="Complete"></param>
        public void GetBy(DateTime fromDate, DateTime toDate, string ArrayEmpID, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    string tblJoin = string.Format("dbo.MCC_TIMEINOUT_SESSION_REAL A inner join dbo.fncNHANVIENDEP() B on A.MaNV=B.Id");
                    string WhereClause = string.Format("where convert(date,A.Workingday,121) between '{0}' and '{1}' and B.Id in({2})", 
                                                                                                    fromDate.ToString("yyyy/MM/dd"), 
                                                                                                    toDate.ToString("yyyy/MM/dd"), 
                                                                                                    ArrayEmpID);
                    _cmd.Parameters.AddWithValue("@Columns", "B.MaNV as MaNV2,B.TenNV,B.DepName,CONVERT(varchar(10),A.Workingday,103) as WorkingdayDMY,A.* ");
                    _cmd.Parameters.AddWithValue("@TableName", tblJoin);
                    _cmd.Parameters.AddWithValue("@WhereClause", WhereClause);

                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);

                    adapter.Fill(tbl);
                    adapter.Dispose();
                    adapter = null;
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(tbl, _ok);
        }
    }

    public class clsBcChiTietNgayVirtual : Interfaces.IBcChiTietNgay
    {
        /// <summary>
        /// MaCheDo=0: Lấy tất cả các chế độ
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="DepID"></param>
        /// <param name="MaCheDo"></param>
        /// <param name="Complete"></param>
        public void GetBy(DateTime fromDate, DateTime toDate, string ArrayEmpID, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                //using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                //{
                //    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_BC_LAODONG_CHEDO", _cnn);
                //    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                //    _cmd.Parameters.AddWithValue("@FromDate", fromDate);
                //    _cmd.Parameters.AddWithValue("@ToDate", toDate);
                //    _cmd.Parameters.AddWithValue("@DepID", ArrayEmpID);
                //    //_cmd.Parameters.AddWithValue("@MaCheDo", MaCheDo);

                //    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                //    _adapter.Fill(tbl);
                //    if (tbl == null) tbl = new System.Data.DataTable();
                //}

                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    string tblJoin = string.Format("dbo.MCC_TIMEINOUT_SESSION_VIRTUAL A inner join dbo.fncNHANVIENDEP() B on A.MaNV=B.Id and isnull(b.HideVirtual, 0) = 0");
                    string WhereClause = string.Format("where convert(date,A.Workingday,121) between '{0}' and '{1}' and B.Id in({2})",
                                                                                                    fromDate.ToString("yyyy/MM/dd"),
                                                                                                    toDate.ToString("yyyy/MM/dd"),
                                                                                                    ArrayEmpID);
                    _cmd.Parameters.AddWithValue("@Columns", "B.MaNV as MaNV2,B.TenNV,B.DepName,CONVERT(varchar(10),A.Workingday,103) as WorkingdayDMY,A.* ");
                    _cmd.Parameters.AddWithValue("@TableName", tblJoin);
                    _cmd.Parameters.AddWithValue("@WhereClause", WhereClause);

                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);

                    adapter.Fill(tbl);
                    adapter.Dispose();
                    adapter = null;
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(tbl, _ok);
        }
    }
}
