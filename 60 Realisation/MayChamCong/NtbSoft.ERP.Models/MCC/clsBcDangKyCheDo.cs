﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsBcDangKyCheDo : Interfaces.IBcDangKyCheDo
    {
        /// <summary>
        /// MaCheDo=0: Lấy tất cả các chế độ
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="DepID"></param>
        /// <param name="MaCheDo"></param>
        /// <param name="Complete"></param>
        public void GetBy(DateTime fromDate, DateTime toDate,string DepID,int MaCheDo, Action<System.Data.DataTable, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_BC_LAODONG_CHEDO", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@FromDate", fromDate);
                    _cmd.Parameters.AddWithValue("@ToDate", toDate);
                    _cmd.Parameters.AddWithValue("@DepID", DepID);
                    _cmd.Parameters.AddWithValue("@MaCheDo", MaCheDo);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                    if (tbl == null) tbl = new System.Data.DataTable();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(tbl, _ok);
        }
    }
}
