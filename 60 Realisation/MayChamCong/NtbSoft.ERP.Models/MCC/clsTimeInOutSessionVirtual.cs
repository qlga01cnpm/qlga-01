﻿using NtbSoft.ERP.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsTimeInOutSessionVirtual : Interfaces.ITimeInOutSession
    {
        public void GetBy(DateTime FromDate, DateTime ToDate, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo> Items = new List<Entity.Interfaces.ITimeInOutSessionInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@FromDate", FromDate.ToString(Libs.cFormat.MDY));
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate.ToString(Libs.cFormat.MDY));
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsTimeInOutSessionInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }
        public void GetPaging(int PageIndex, int PageSize, DateTime FromDate, DateTime ToDate, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo>, IResultOk, int> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo> Items = new List<Entity.Interfaces.ITimeInOutSessionInfo>();
            int RecordCount = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@FromDate", FromDate.ToString(Libs.cFormat.MDY));
                    _cmd.Parameters.AddWithValue("@ToDate", ToDate.ToString(Libs.cFormat.MDY));
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    _cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
                    _cmd.Parameters.AddWithValue("@PageSize", PageSize);
                    _cmd.Parameters.Add("@RecordCount", SqlDbType.Int);
                    _cmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (_cmd.Parameters["@RecordCount"].Value != null)
                        int.TryParse(_cmd.Parameters["@RecordCount"].Value.ToString(), out RecordCount);

                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsTimeInOutSessionInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetPaging", ex.Message);
            }
            if (Complete != null) Complete(Items, error, RecordCount);

        }

        public void Update(Libs.ApiAction Action, double Id, DateTime WorkingDay, int MaCC, string Gio, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                TimeSpan GioTemp;
                if (!TimeSpan.TryParse(Gio, out GioTemp))
                    goto exit;
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_ATTLOGS_SESSION_VIRTUAL_EDIT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Action.ToString());
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);
                    _cmd.Parameters.AddWithValue("@WorkingDay", WorkingDay.ToString("yyyy/MM/dd"));
                    _cmd.Parameters.AddWithValue("@DateTimeAtt",
                        string.Format("{0} {1}", WorkingDay.ToString("yyyy/MM/dd"), Gio));
                    _cmd.Parameters.AddWithValue("@Gio", Gio);
                    _cmd.Parameters.AddWithValue("@Id", Id);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Update", ex.Message);
            }
        exit:
            if (Complete != null) Complete(ok);
        }
    }
}
