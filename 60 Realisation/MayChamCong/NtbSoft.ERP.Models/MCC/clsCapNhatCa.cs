﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsCapNhatCa:Interfaces.ICapNhatCa
    {
        public System.Data.DataTable TYPE_NHANVIEN_SCHEDULE
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_NHANVIEN_SCHEDULE");
                _tbl.Columns.Add("Id", typeof(System.Int32));
                _tbl.Columns.Add("MaNV", typeof(System.String));
                _tbl.Columns.Add("TenNV", typeof(System.String));

                _tbl.Columns.Add("MaCC", typeof(System.Int32));
                _tbl.Columns.Add("CardID", typeof(System.String));
                _tbl.Columns.Add("SCHName", typeof(System.String));
                _tbl.Columns.Add("ShiftID", typeof(System.String));
                return _tbl;
            }
        }

        private void SetType(string ShiftId, string SchID, List<Entity.Interfaces.INhanVienInfo> Items, ref System.Data.DataTable TYPE_NHANVIEN_SCHEDULE)
        {
            if (Items == null) return;
            foreach (var item in Items)
            {
                System.Data.DataRow newRow = TYPE_NHANVIEN_SCHEDULE.NewRow();
                newRow["Id"] = item.Id;
                newRow["MaNV"] = SchID;
                newRow["TenNV"] = item.TenNV == null ? "" : item.TenNV;

                newRow["MaCC"] = item.MaCC;
                newRow["CardID"] = item.CardID == null ? "" : item.CardID;
                newRow["SCHName"] = SchID;
                newRow["ShiftID"] = ShiftId;

                TYPE_NHANVIEN_SCHEDULE.Rows.Add(newRow);
            }
        }
        /// <summary>
        /// Cập nhật ca làm việc, và lịch trình
        /// </summary>
        /// <param name="ShiftId"></param>
        /// <param name="SchID"></param>
        /// <param name="Items"></param>
        /// <param name="Complete"></param>
        public void CapNhatCaLichTrinh(string ShiftId, string SchID, List<Entity.Interfaces.INhanVienInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk _ok = new BaseResultOk();
            try
            {
                System.Data.DataTable tbl = this.TYPE_NHANVIEN_SCHEDULE;
                SetType(ShiftId, SchID, Items, ref tbl);
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_CAPNHAT_CA", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@TYPE_NHANVIEN_SCHEDULE", tbl);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("CapNhatCaLichTrinh", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }
    }
}
