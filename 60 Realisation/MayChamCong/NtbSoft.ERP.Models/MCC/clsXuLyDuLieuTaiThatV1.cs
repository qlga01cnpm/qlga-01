﻿using NtbSoft.ERP.Entity;
using NtbSoft.ERP.Models.DIC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    /// <summary>
    /// Xử lý dữ liệu tải thật
    /// </summary>
    public class clsXuLyDuLieuTaiThatV1 : Interfaces.IXuLyDuLieuTai
    {
        //Ca lam viec
        private Models.Interfaces.IShift _mShift;
        internal Interfaces.IQuyTac _QuyTac;
        private string _MaCC = "";
        private int _mSoPhut2LanCham = 0;
        List<Entity.Interfaces.IDangKyChanGioInfo> _DangKyChanGioCollection = null;
        public clsXuLyDuLieuTaiThatV1(Models.Interfaces.IShift Shift, Interfaces.IQuyTac QuyTac, List<Entity.Interfaces.IDangKyChanGioInfo> DangKyChanGioCollection)
        {
            _mShift = Shift;
            _QuyTac = QuyTac;
            _DangKyChanGioCollection = DangKyChanGioCollection;
        }

        /// <summary>
        /// Hàm kiểm tra chuỗi giờ chấm có phải là định dạng thời gian hay không
        /// </summary>
        /// <param name="GioCham"></param>
        /// <returns></returns>
        public bool IsTime(string GioCham)
        {
            TimeSpan tmp;
            return TimeSpan.TryParse(GioCham, out tmp);
        }

        public bool IsTime(string GioCham, out TimeSpan OutGioCham)
        {
            return TimeSpan.TryParse(GioCham, out OutGioCham);
        }

        public bool CheckHopLe(string MaCC, DateTime Ngay, string GioCham)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiMuon2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom1(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatDauTinhDiSom2(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BreakInOfShift(string ShiftID, Action<string, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void CheckExistInOut(string MaNV, string WorkingDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void NumberShift(string SchID, Action<int, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void ReturnEmpID(string MaCC, Action<double, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            double id = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Id ");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_NHANVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and isnull(IsNghiViec, 0) <> 1", MaCC));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        id = Convert.ToDouble(_reader["id"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("ReturnEmpID", ex.Message);
            }
            if (Complete != null) Complete(id, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<int, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            int SoLan = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Count(MaCC) as SoLan");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        SoLan = Convert.ToInt32(_reader["SoLan"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("SoLanCham", ex.Message);
            }
            if (Complete != null) Complete(SoLan, error);
        }

        public void SoLanCham(string MaCC, DateTime Ngay, Action<List<TimeSpan>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<TimeSpan> mTime = new List<TimeSpan>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "Gio");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe = 1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        TimeSpan mTemp;
                        if (TimeSpan.TryParse(Convert.ToString(_reader["Gio"]), out mTemp))
                            mTime.Add(mTemp);
                        //SoLan = Convert.ToInt32(_reader["SoLan"]);
                        //break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("SoLanCham", ex.Message);
            }
            if (Complete != null) Complete(mTime, error);
        }

        public void MinGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "IsVirtualData");
                    _cmd.Parameters.AddWithValue("@TableName", "SYS_APP");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["IsVirtualData"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;

                    _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@Columns", "MIN(Gio) as Gio");
                    if (!value.Equals("True"))
                    {
                        _cmd.Parameters.AddWithValue("@TableName", "MCC_ATTLOGS_SESSION");
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@TableName", "MCC_ATTLOGS_SESSION_VIRTUAL");
                    }
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date, WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));


                    _reader = _cmd.ExecuteReader();
                    value = "";
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        public void MaxGioCham(string MaCC, DateTime Ngay, Action<string, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            string value = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "MAX(Gio) as Gio");
                    if (!IsVirtualData())
                    {
                        _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION");
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@TableName", "dbo.MCC_ATTLOGS_SESSION_VIRTUAL");
                    }
                    _cmd.Parameters.AddWithValue("@WhereClause",
                        string.Format("where MaCC='{0}' and  Convert(date,WorkingDay,121)=Convert(date,'{1}',121) and IsHopLe=1", MaCC, Ngay.ToString(Libs.clsConsts.FormatYMD)));

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["Gio"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("MinGioCham", ex.Message);
            }
            if (Complete != null) Complete(value, error);
        }

        /// <summary>
        /// Giờ ra (giờ ra nghỉ trưa)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakOutGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            //////bool flag = Information.IsDate(MinGioQuet) & Information.IsDate(MaxGioQuet);
            //////if (flag)
            //////{
            //////    string text = string.Concat(new string[]
            //////    {
            //////        "SELECT Min(GioQuet) as Gio FROM ",
            //////        TenBang,
            //////        " WHERE WorkingDay=Convert(DateTime,'",
            //////        Ngay,
            //////        "',103) AND MaCC='",
            //////        CardID,
            //////        "'\r\n"
            //////    });
            //////    text = string.Concat(new string[]
            //////    {
            //////        text,
            //////        " AND GioQuet >'",
            //////        MinGioQuet,
            //////        "' AND GioQuet < '",
            //////        MaxGioQuet,
            //////        "' AND chkUsed=0"
            //////    });
            //////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            //////    flag = (dataSet.Tables[0].Rows.Count > 0);
            //////    if (flag)
            //////    {
            //////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            //////    }
            //////}
        }

        /// <summary>
        /// Giờ vào (giờ sau khi nghỉ trưa vào)
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="MinGioCham"></param>
        /// <param name="MaxGioCham"></param>
        /// <param name="BreakOutGioCham"></param>
        /// <param name="Complete"></param>
        public void BreakInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, string BreakOutGioCham, Action<string, Interfaces.IResultOk> Complete)
        {
            ////bool flag = Information.IsDate(MaxGioQuet) & Information.IsDate(BreakOutGioQuet);
            ////if (flag)
            ////{
            ////    string text = string.Concat(new string[]
            ////    {
            ////        "SELECT Max(GioQuet) as Gio FROM ",
            ////        TenBang,
            ////        " WHERE WorkingDay=Convert(DateTime,'",
            ////        Ngay,
            ////        "',103) AND MaCC='",
            ////        CardID,
            ////        "'\r\n"
            ////    });
            ////    text = string.Concat(new string[]
            ////    {
            ////        text,
            ////        " AND GioQuet < '",
            ////        MaxGioQuet,
            ////        "'  AND GioQuet > '",
            ////        BreakOutGioQuet,
            ////        "' AND chkUsed=0"
            ////    });
            ////    dataSet = clsDal.fcnExecQuery(text, "tbl");
            ////    flag = (dataSet.Tables[0].Rows.Count > 0);
            ////    if (flag)
            ////    {
            ////        result = dataSet.Tables[0].Rows[0]["Gio"].ToString();
            ////    }
            ////}
        }


        private void GetBreakOutInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, out string BreakOutGioCham, out string BreakInGioCham, Action<Interfaces.IResultOk> Complete)
        {
            BreakOutGioCham = ""; BreakInGioCham = "";
            Interfaces.IResultOk ok = new Models.BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_TIMEINOUT_BREAK_OUT_IN_GET", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", Ngay);
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);
                    _cmd.Parameters.AddWithValue("@MinGioCham", MinGioCham);
                    _cmd.Parameters.AddWithValue("@MaxGioCham", MaxGioCham);

                    _cmd.Parameters.Add("@BreakOut", SqlDbType.VarChar, 8);
                    _cmd.Parameters["@BreakOut"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@BreakIn", SqlDbType.VarChar, 8);
                    _cmd.Parameters["@BreakIn"].Direction = ParameterDirection.Output;

                    ok.RowsAffected = _cmd.ExecuteNonQuery();

                    if (_cmd.Parameters["@BreakOut"].Value != null)
                        BreakOutGioCham = _cmd.Parameters["@BreakOut"].Value.ToString();
                    if (_cmd.Parameters["@BreakIn"].Value != null)
                        BreakInGioCham = _cmd.Parameters["@BreakIn"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("BreakOutInGioCham", ex.Message);
            }
        }

        public void XuLyDuLieuChamCong(string MaCC, DateTime CurDay, Action<bool, Interfaces.IResultOk> Complete)
        {
            int EmpId = 0;
            List<TimeSpan> mArrayGio = new List<TimeSpan>();
            this.ReturnEmpID(MaCC, (result, complete) => { EmpId = (int)result; });
            if (EmpId == 0) return;
            string MinGio = "", MaxGio = "", BreakOut = "", BreakIn = "";

            string DateIn, DateOut;

            this.MinGioCham(MaCC, CurDay, (result, complete) => { MinGio = result; });
            this.MaxGioCham(MaCC, CurDay, (result, complete) => { MaxGio = result; });

            DateIn = DateOut = CurDay.ToString(Libs.clsConsts.FormatMDY);

            //Kiểm tra giờ ra kế có hợp lệ hay không

            if (_mSoPhut2LanCham == 0)
                _QuyTac.SoPhutToiThieuGiuaHaiLanCham((result, complete) => { _mSoPhut2LanCham = result; });

            TimeSpan mingio, maxgio;
            TimeSpan.TryParse(MinGio, out mingio);
            TimeSpan.TryParse(MaxGio, out maxgio);
            if ((maxgio - mingio).TotalMinutes < _mSoPhut2LanCham)
            {
                MaxGio = "";
            }

            DateIn = DateIn + " " + MinGio;
            DateOut = DateOut + " " + MaxGio;

            #region XỬ LÝ DỮ LIỆU ẢO

            int Ngay = 1;
            if (CurDay.DayOfWeek == DayOfWeek.Sunday) Ngay = 1;
            else if (CurDay.DayOfWeek == DayOfWeek.Monday) Ngay = 2;
            else if (CurDay.DayOfWeek == DayOfWeek.Tuesday) Ngay = 3;
            else if (CurDay.DayOfWeek == DayOfWeek.Wednesday) Ngay = 4;
            else if (CurDay.DayOfWeek == DayOfWeek.Thursday) Ngay = 5;
            else if (CurDay.DayOfWeek == DayOfWeek.Friday) Ngay = 6;
            else if (CurDay.DayOfWeek == DayOfWeek.Saturday) Ngay = 7;

            string mMaxGioAo = "";
            string mMinGioAo = "";
            mMinGioAo = MinGio;
            // NẾU KHÔNG CÓ GIỜ RA VỀ (TRƯỚC NGÀY HÔM NAY) THÌ THÊM VÀO BẢNG CHẤM CÔNG ẢO GIỜ VỀ RANDOM GIỜ VỀ CA LÀM VIỆC

            clsDangKyCheDo clsDangKyCheDo = new clsDangKyCheDo();
            bool isLeave = clsDangKyCheDo.CheckIsRegisLeave(CurDay.ToString(Libs.clsConsts.FormatYMD), EmpId);
            if (!isLeave && Ngay != 1) // nếu mà không đăng ký nghỉ phép hoặc chế độ
            {
                Entity.clsShiftInfo shiftInfo = this.GetShiftByMaNV(EmpId, Ngay);
                if (shiftInfo.TimeOut == null)
                {
                    return;
                }
                TimeSpan mGioRa = TimeSpan.Parse(shiftInfo.TimeOut);
                TimeSpan mGioVao = TimeSpan.Parse(shiftInfo.TimeIn);

                TimeSpan GioBDChanSang = NtbSoft.ERP.Libs.clsConsts.GioBDChanSang;
                TimeSpan GioKTChanSang = NtbSoft.ERP.Libs.clsConsts.GioKTChanSang;

                // nếu không có giờ vào
                if ((string.IsNullOrEmpty(MinGio)
                    || (mingio != null && (mingio >= mGioRa || mingio < GioBDChanSang)))
                    && DateTime.Compare(CurDay, DateTime.Now) < 0)
                {
                    if (shiftInfo != null)
                    {
                        mMinGioAo = this.SetValue(CurDay, mGioVao.Hours,
                            GioBDChanSang.Minutes, GioKTChanSang.Minutes);
                    }
                }

                // lấy giờ đăng ký làm thêm
                List<Entity.Interfaces.IDangKyLamThemInfo> mListGioLamThem = new List<Entity.Interfaces.IDangKyLamThemInfo>();
                DIC.Interfaces.IDangKyLamThem _DangKyLamThem = new Models.DIC.clsDangKyLamThem();
                _DangKyLamThem.GetList(CurDay, CurDay, EmpId.ToString(),
                    (result, completed) => { mListGioLamThem = result; });

                // nếu không có giờ ra
                if ((string.IsNullOrEmpty(MaxGio)) || (mListGioLamThem.Count > 0 && TimeSpan.Parse(mListGioLamThem[0].DenGio) > maxgio)
                    && DateTime.Compare(CurDay, DateTime.Now) < 0)
                {
                    if (shiftInfo != null)
                    {
                        if (mListGioLamThem.Count > 0 && TimeSpan.Parse(mListGioLamThem[0].DenGio) > maxgio)
                        {
                            mMaxGioAo = this.SetValue(CurDay, TimeSpan.Parse(mListGioLamThem[0].DenGio).Hours,
                                   TimeSpan.Parse(mListGioLamThem[0].DenGio).Minutes,
                                   TimeSpan.Parse(mListGioLamThem[0].DenGio).Minutes + 10);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(MaxGio) || maxgio > mGioRa)
                            {
                                mMaxGioAo = this.SetValue(CurDay, mGioRa.Hours, mGioRa.Minutes, mGioRa.Minutes + 10);
                            }
                        }
                    }
                }
                else
                {
                    mMaxGioAo = MaxGio;
                }
            }

            if (!string.IsNullOrEmpty(mMaxGioAo) && Ngay != 1)
            {
                //Giờ chặn (chấm công ảo)
                TimeSpan mGioChan = this.GetGioChan(EmpId, CurDay);
                TimeSpan GioKTChanChieu = NtbSoft.ERP.Libs.clsConsts.GioKTChanChieu;

                // nếu có giờ chặn
                if (mGioChan != TimeSpan.Zero)
                {
                    TimeSpan mMaxGio = TimeSpan.Parse(mMaxGioAo);
                    //Nếu giờ chấm ra
                    if (mMaxGio > mGioChan)
                    {
                        //Giờ chấm ra lớn hơn giờ chặn, sẽ lấy giờ chặn và phát sinh số phút trong khoảng từ 0->10 phút
                        //Ví dụ giờ chấm ra là 18h, giờ chặn chấm ra là 16h. Lúc này sẽ sinh ra giờ ảo là từ 16h -> 16h10
                        if ((mMaxGio - mGioChan).TotalMinutes > GioKTChanChieu.Minutes)
                        {
                            mMaxGioAo = this.SetValue(CurDay, mGioChan.Hours, mGioChan.Minutes, mGioChan.Minutes + 10);
                        }
                    }
                }
            }

            // nếu thai sản
            bool isTS = clsDangKyCheDo.CheckIsLeaveTS(CurDay.ToString(Libs.clsConsts.FormatYMD), EmpId);

            // kiểm tra nếu là ngày lễ thì 0 đi làm 
            NtbSoft.ERP.Models.DIC.Interfaces.INgayLe ngayLe = new DIC.clsNgayLe();
            bool isHoliday = ngayLe.CheckIsHoliday(CurDay.ToString(Libs.clsConsts.FormatYMD));

            // nếu là chủ nhật 
            if (isTS || isHoliday || Ngay == 1)
            {
                mMinGioAo = "";
                mMaxGioAo = "";
            }

            #endregion

            Interfaces.IResultOk error = new BaseResultOk();
            System.Data.SqlClient.SqlTransaction mTran = null;
            bool mIsCapNhat = false, mIsCapNhatAo = false;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    mTran = _cnn.BeginTransaction();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL", _cnn, mTran);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", CurDay.ToString(Libs.clsConsts.FormatMDY));
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.Parameters.AddWithValue("@MaNV", EmpId);
                    _cmd.Parameters.AddWithValue("@TimeIn", MinGio);
                    _cmd.Parameters.AddWithValue("@BreakOut", BreakOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", BreakIn);

                    _cmd.Parameters.AddWithValue("@TimeOut", MaxGio);
                    _cmd.Parameters.AddWithValue("@DateTimeIn", DateIn);
                    _cmd.Parameters.AddWithValue("@DateBreakOut", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateBreakIn", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateTimeOut", DateOut);

                    mIsCapNhat = _cmd.ExecuteNonQuery() > 0;

                    /*** Cập nhật về bảng dữ liệu ảo ***/
                    _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_VIRTUAL", _cnn, mTran);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Workingday", CurDay.ToString(Libs.clsConsts.FormatMDY));
                    _cmd.Parameters.AddWithValue("@DepID", "");
                    _cmd.Parameters.AddWithValue("@MaNV", EmpId);
                    _cmd.Parameters.AddWithValue("@TimeIn", mMinGioAo);
                    _cmd.Parameters.AddWithValue("@BreakOut", BreakOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", BreakIn);

                    _cmd.Parameters.AddWithValue("@TimeOut", mMaxGioAo);
                    _cmd.Parameters.AddWithValue("@DateTimeIn", DateIn);
                    _cmd.Parameters.AddWithValue("@DateBreakOut", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateBreakIn", "1900/01/01");
                    _cmd.Parameters.AddWithValue("@DateTimeOut", DateOut);

                    mIsCapNhatAo = _cmd.ExecuteNonQuery() > 0;
                    if (mIsCapNhat && mIsCapNhatAo)
                        mTran.Commit();
                    else
                        mTran.Rollback();
                }
            }
            catch (Exception ex)
            {
                if (mTran != null)
                    mTran.Rollback();

                error.Errors.Add("XuLyDuLieuChamCong", ex.Message);
            }
            finally
            {
                if (mTran != null) mTran.Dispose();
            }

            #region ĐĂNG KÝ VÀO RA MỚI

            if (this._MaCC != MaCC)
            {
                //if (MaCC == "5")
                //{

                //}
                this._MaCC = MaCC;
                Models.DIC.Interfaces.IDangKyVaoRa mObjVaoRa = new DIC.clsDangKyVaoRa();
                mObjVaoRa.Update(Libs.ApiAction.POST, MaCC, CurDay.ToString(Libs.clsConsts.FormatYMD), (complete) => { });
            }

            #endregion

            if (Complete != null) Complete(mIsCapNhat, error);
        }

        public Entity.clsShiftInfo GetShiftByMaNV(double maNV, int ngay)
        {
            Entity.clsShiftInfo shiftInfo = null;
            clsShift shiftService = new clsShift();
            shiftService.GetBy(maNV, ngay, (result, ex) =>
            {
                shiftInfo = result;
            });
            return shiftInfo;
        }

        public void CheckTonTaiBang(string TableName, Action<bool, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }


        public void InitializeDate(int Month, int MaxDay, int Year, string MaCC, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@MaxDay", MaxDay);
                    _cmd.Parameters.AddWithValue("@Year", Year);
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INIDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        public void InitializeDate(int Month, int MaxDay, int Year, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Month);
                    _cmd.Parameters.AddWithValue("@MaxDay", MaxDay);
                    _cmd.Parameters.AddWithValue("@Year", Year);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("InitializeDate.sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        private TimeSpan GetGioChan(int eID, DateTime WorkingDay)
        {
            if (this._DangKyChanGioCollection == null) return TimeSpan.Zero;

            var mResult = this._DangKyChanGioCollection.FirstOrDefault(f => f.eID == eID.ToString() && f.sMonth == WorkingDay.Month && f.sYear == WorkingDay.Year);
            if (mResult == null) return TimeSpan.Zero;
            TimeSpan mTemp;
            if (!TimeSpan.TryParse(mResult.GetValue(WorkingDay.Day), out mTemp))
                return TimeSpan.Zero;
            return mTemp;

        }

        private bool IsVirtualData()
        {
            try
            {
                string value = "";
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "IsVirtualData");
                    _cmd.Parameters.AddWithValue("@TableName", "SYS_APP");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataReader _reader = _cmd.ExecuteReader();
                    while (_reader.Read())
                    {
                        if (_reader.IsDBNull(0)) break;
                        value = Convert.ToString(_reader["IsVirtualData"]);
                        break;
                    }
                    _reader.Dispose();
                    _reader = null;
                    return value.ToLower() == "true";
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string SetValue(DateTime date, int hours, int fromMinutes, int toMinutes)
        {
            string gio = string.Empty;
            Random randomOut = new Random();
            //Random trong khoang phut
            int minutes = randomOut.Next(fromMinutes, toMinutes);
            DateTime dtNew = new DateTime(date.Year, date.Month, date.Day, hours, minutes, 0);
            gio = dtNew.ToString(Libs.clsConsts.FormatHm);
            return gio;
        }

    }
}
