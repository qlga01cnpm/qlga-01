﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsTinhCongVirtual : Interfaces.ITinhCong
    {
        //1. Chuyển nhân viên vào bảng công
        //2. Nhận ca (lấy ca làm việc)
        //3. Lấy quy tăng làm tron

        //Quy tac lam tron
        internal Models.Interfaces.IQuyTacLamTron _QuyTacLamTron;
        //Ca lam viec
        private Models.Interfaces.IShift _clsShift;
        //Lịch trình ca
        private Models.Interfaces.IScheduleSchift _ScheduleShift;
        private Models.Interfaces.INhanVien _NhanVienCC;
        private List<Entity.clsShiftInfo> _ShiftCollection = null;
        private Models.Interfaces.ITimeSheets _TimeSheets = null;
        private DIC.Interfaces.IDangKyVang _DangKyVang = null;
        private DIC.Interfaces.IDangKyCheDo _DangKyCheDo = null;
        private DIC.Interfaces.IDangKyLamThem _DangKyLamThem = null;

        public clsTinhCongVirtual(Models.Interfaces.IQuyTacLamTron QuyTacLamTron, Models.Interfaces.IShift Shift,
            Models.Interfaces.IScheduleSchift ScheduleShift, Models.Interfaces.INhanVien NhanVienCC,
            Models.Interfaces.ITimeSheets TimeSheets, DIC.Interfaces.IDangKyVang DangKyVang,
            Models.DIC.Interfaces.IDangKyCheDo DangKyCheDo,
             DIC.Interfaces.IDangKyLamThem DangKyLamThem)
        {
            this._QuyTacLamTron = QuyTacLamTron;
            this._clsShift = Shift;
            this._ScheduleShift = ScheduleShift;
            this._NhanVienCC = NhanVienCC;
            this._ShiftCollection = new List<Entity.clsShiftInfo>();
            this._TimeSheets = TimeSheets;
            this._DangKyVang = DangKyVang;
            this._DangKyCheDo = DangKyCheDo;
            this._DangKyLamThem = DangKyLamThem;
        }

        public void TinhCong(Entity.Interfaces.ITimeInOutSessionInfo SessionInfo, Action<Entity.Interfaces.ITimeInOutSessionInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            Entity.clsShiftInfo shiftInfo = null;
            bool IsNhanVienCheDo = false;

            /*** 1. Lấy thông tin nhân viên để lấy lịch trình của nhân viên ***/

            Entity.Interfaces.INhanVienInfo NhanVienInfo = null;
            this._NhanVienCC.GeBy(SessionInfo.MaNV, (result, ex) => { if (ex.Success)NhanVienInfo = result; });


            /**** 5. Lấy số giờ ra vào trong ngày*/
            float soGioRaVao = 0;
            this._NhanVienCC.GetVaoRaByDate(NhanVienInfo.Id.ToString(),
                SessionInfo.Workingday.ToString(Libs.clsConsts.FormatYMD), (result, ex) =>
            {
                soGioRaVao = result;
                if (!ex.Success) ok.Errors.Add("GetVaoRaByDate: tinh cong ao", ex.Message);
            });

            /*** Kiểm tra đăng ký chế độ của nhân viên ***/
            string _kyhieu = "";

            _DangKyCheDo.GetShift(NhanVienInfo.MaNV, SessionInfo.Workingday,
                (result, kyhieu, ex) =>
                {
                    if (!ex.Success)
                        ok.Errors.Add("Dang_ky_che_do", ex.Message);
                    shiftInfo = result;
                    _kyhieu = kyhieu;
                });

            //Nhân viên có đăng ký chế độ
            if (shiftInfo != null)
            {
                IsNhanVienCheDo = true;
                SessionInfo.Shift = shiftInfo.ShiftId;
                //SessionInfo.OTX = 0;
                goto xuly;
            }

            /*** 2. Lấy lịch trình ca làm việc của nhân viên ***/
            List<Entity.clsScheduleShiftInfo> scheduleShiftCollection = null;
            this._ScheduleShift.GetBy(NhanVienInfo.SCHName, (result, ex) => { scheduleShiftCollection = result; if (!ex.Success)ok.Errors.Add("Get_Schedule_Shift_By_SchName", ex.Message); });
            /*** 3.Lấy danh sách ngày lễ ***/
            //List<Entity.Interfaces.INgayLeInfo> NgayLe
            /*** 4. Lấy danh sách Ca làm việc ***/
            this._clsShift.GetAll((result, ex) =>
            {
                if (ex.Success) this._ShiftCollection = result;
            });

            //Duyệt cua từng ngày trong lịch trình( Kiểm tra ngày lễ và sau đó kiểm tra từ chủ nhật đến thứ 7)
            DateTime dtNow = new DateTime(SessionInfo.Workingday.Year,
                SessionInfo.Workingday.Month,
                SessionInfo.Workingday.Day);

            SessionInfo.Shift = "--";

            #region LẤY NGÀY LÀM VIỆC

            foreach (var item in scheduleShiftCollection)
            {
                shiftInfo = this.GetCaLamViec(SessionInfo.Workingday, SessionInfo.TimeIn, item);
                //var kk = shiftInfo;
                //var dd = kk;
                //var ss = dd;
                //Nếu lịch trình ngày hôm đó không có ca. Thì tiếp tục duyệt 
                if (shiftInfo == null) continue;
                SessionInfo.Shift = shiftInfo.ShiftId;

                #region Ngày lễ
                if (item.Ngay == 8)
                {//Ngày lễ (Tết)

                    break;
                }
                #endregion

                #region Chủ nhật
                if (dtNow.DayOfWeek == DayOfWeek.Sunday && item.Ngay == 1)
                {
                    break;
                }
                #endregion

                #region Thứ 2
                if (dtNow.DayOfWeek == DayOfWeek.Monday && item.Ngay == 2)
                {//Thứ 2

                    break;
                }
                #endregion

                #region Thứ 3
                if (dtNow.DayOfWeek == DayOfWeek.Tuesday && item.Ngay == 3)
                {//Thứ 3

                    break;
                }
                #endregion

                #region Thứ 4
                if (dtNow.DayOfWeek == DayOfWeek.Wednesday && item.Ngay == 4)
                {//Thứ 4

                    break;
                }
                #endregion

                #region Thứ 5
                if (dtNow.DayOfWeek == DayOfWeek.Thursday && item.Ngay == 4)
                {//Thứ 5
                    break;
                }
                #endregion

                #region Thứ 6
                if (dtNow.DayOfWeek == DayOfWeek.Friday && item.Ngay == 6)
                {//Thứ 6

                    break;
                }
                #endregion

                #region Thứ 7
                if (dtNow.DayOfWeek == DayOfWeek.Saturday && item.Ngay == 4)
                {//Thứ 7
                    break;
                }
                #endregion

                break;
            }

            #endregion

        xuly:
            //int LamTron = 0;
            //_QuyTacLamTron.LamTronPhut(17, (result, exx) => { ok.Errors.Add("QuyTacLamTron.LamTronPhut", exx.Message); LamTron = result; });
            //var kkk1 = LamTron;
            //var ddd = kkk1;
            //var cc = ddd;

            #region Kiểm tra Vắng
            SessionInfo.LyDoVang = null;
            if (!IsNhanVienCheDo)
            {
                Entity.Interfaces.IDangKyVangInfo vangInfo = null;
                this._DangKyVang.GetBy(SessionInfo.Workingday.Month, SessionInfo.Workingday.Year,
                    SessionInfo.MaNV, (result, ex) => { vangInfo = result; ok = ex; });
                if (vangInfo != null)
                    SessionInfo.LyDoVang = vangInfo.GetValue(SessionInfo.Workingday.Day);
            }
            #endregion

            #region Kiểm tra tính đi muộn về sớm
            SessionInfo.LateTime = (float)TinhDiMuon(SessionInfo.TimeIn, SessionInfo.TimeOut, shiftInfo, SessionInfo.LyDoVang);
            SessionInfo.EarlyTime = (float)TinhVeSom(SessionInfo.TimeIn, SessionInfo.TimeOut, shiftInfo, SessionInfo.LyDoVang);

            //Nếu là thai sản thì cho phép về trước 1 tiếng
            if (IsNhanVienCheDo)
            {
                if ((SessionInfo.LateTime + SessionInfo.EarlyTime) <= 60)
                {
                    SessionInfo.LateTime = 0;
                    SessionInfo.EarlyTime = 0;
                }
                else
                {
                    SessionInfo.LateTime = ((SessionInfo.LateTime + SessionInfo.EarlyTime) - 60f) / 2f;
                    SessionInfo.EarlyTime = ((SessionInfo.LateTime + SessionInfo.EarlyTime) - 60f) / 2f;
                }
            }
            #endregion

            // kiểm tra 1 nhân viên
            //if (NhanVienInfo.MaNV == "0003")
            //{
            //    if (SessionInfo.Workingday.ToString("yyyy-MM-dd") == "2018-11-19")
            //    {

            //    }
            //}

            #region Tính tổng thời gian làm việc
            SessionInfo.TotalHours = (float)TongTG(SessionInfo.TimeIn, SessionInfo.TimeOut, soGioRaVao, shiftInfo);
            #endregion

            #region Tính công làm việc
            //Nếu tổng số thời gian làm việc của lao động <= 4.5 thì tính nửa công
            //Ngược lại tính 1 công
            if (SessionInfo.TotalHours < 1)
            {
                SessionInfo.CongK = 0;
            }
            else if (SessionInfo.TotalHours < 5)
                SessionInfo.CongK = 0.5f;
            else
            {
                // chưa trừ được công người ta đi muộn ... 
                // nếu đi muộn thì trừ số giờ theo quy định của công ty
                SessionInfo.CongK = 1;
            }
            #endregion

            #region Tính giờ tăng ca
            //TinhGioTangCa(shiftInfo, ref SessionInfo);
            //Nếu không phải là thai sản thì mới tính tăng ca
            if (!IsNhanVienCheDo)
            {
                List<Entity.Interfaces.IDangKyLamThemInfo> mListGioLamThem = new List<Entity.Interfaces.IDangKyLamThemInfo>();
                _DangKyLamThem.GetList(SessionInfo.Workingday, SessionInfo.Workingday, NhanVienInfo.Id.ToString(), (result, completed) => { mListGioLamThem = result; });
                if (mListGioLamThem.Count > 0)
                    TinhGioTangCa(shiftInfo, ref SessionInfo, mListGioLamThem[0]);
                else
                    TinhGioTangCa(shiftInfo, ref SessionInfo, null);
            }
            else
            {
                SessionInfo.OTX = 0;
            }
            #endregion

            #region Kiểm Tra In Out
            if (string.IsNullOrWhiteSpace(SessionInfo.TimeIn) &&
                string.IsNullOrWhiteSpace(SessionInfo.TimeOut))
            {
                if (!IsNhanVienCheDo)
                {
                    SessionInfo.TotalTimeHM = "";//Off
                    SessionInfo.Shift = "***";
                }
                else
                {
                    SessionInfo.Shift = shiftInfo.ShiftId;
                }
            }
            else if (!string.IsNullOrWhiteSpace(SessionInfo.TimeIn)
                && !string.IsNullOrWhiteSpace(SessionInfo.TimeOut)
                && SessionInfo.TotalHours != 0)
                SessionInfo.TotalTimeHM = "K";
            else
            {
                SessionInfo.TotalTimeHM = "";
            }
            #endregion

            /*** Trường hợp là nhân viên chế độ ***/
            if (!string.IsNullOrEmpty(_kyhieu))
            {
                SessionInfo.TotalTimeHM = _kyhieu;
                // SessionInfo.LyDoVang = _kyhieu;
            }

            /*** Trường hợp rơi vào ngày lễ ***/
            // Ký hiệu là L
            NtbSoft.ERP.Models.DIC.Interfaces.INgayLe ngayLe = new DIC.clsNgayLe();
            bool isHoliday = ngayLe.CheckIsHoliday(SessionInfo.Workingday.ToString("yyyy-MM-dd"));
            if (isHoliday)
            {
                _kyhieu = "L";
                SessionInfo.TotalTimeHM = _kyhieu;
                SessionInfo.LyDoVang = _kyhieu;
            }

            /*** 1. Cập nhật nội dung sau khi tính công ***/
            UpdateSession(SessionInfo, ex => { if (!ex.Success) ok.Errors.Add("UpdateSession_", ex.Message); });

            /*** 2. Cập nhật về bảng tính công tháng ***/
            this._TimeSheets.UpdateTimeSheet(SessionInfo, ex => { });

            if (Complete != null) Complete(SessionInfo, ok);
        }

        public void TinhCong(IEnumerable<Entity.Interfaces.INhanVienInfo> DsNhanVien, Action<Models.Interfaces.IResultOk> Complete)
        {

        }

        private Entity.clsShiftInfo GetCaLamViec(DateTime WorkingDay, string TimeIn, string ShiftId)
        {
            //Khai báo giờ bắt đầu và kết thức để hiểu ca
            TimeSpan tsBegin, tsEnd, tsIn;
            if (!string.IsNullOrWhiteSpace(ShiftId))
            {
                var fItem = this._ShiftCollection.Find(f => f.ShiftId.Equals(ShiftId));
                if (fItem != null)
                {
                    //Kiểm tra ngày áp dụng ca
                    //WorkingDay: Ngày chấm công; TuNgay: là ngày ca được áp dụng
                    //Ngày chấm công phải lơn hơn hoặc bằng ngày áp dụng thì mới tính ca
                    if (DateTime.Compare(WorkingDay, fItem.TuNgay) == -1)
                        return null;
                    return fItem;
                    /*** KIỂM TRA NHẬN DIỆN CA ***/
                    //string StrBegin = "7:00", StrEnd = "7:48";
                    //StrBegin = fItem.StartIn; StrEnd = fItem.EndIn;
                    //if(TimeSpan.TryParse(TimeIn,out tsIn))
                    //    if (TimeSpan.TryParse(StrBegin, out tsBegin))
                    //        if (TimeSpan.TryParse(StrEnd, out tsEnd))
                    //        {
                    //            //Giờ chấm vào lớn hơn hoặc bằng quy định giờ quy định nhận diện
                    //            //Và giờ chấm vào nhỏ ơn hoặc bằng giờ kết thúc chấm vào.
                    //            //Ý nghĩa: Giờ chấm vào nằm trong khoảng giờ nhận diện ca thì sẽ lấy ca đó để tính công
                    //            //Ví dụ: Ca HC 8h làm. Giờ bắt đầu để hiểu ca là 7h và giờ kết thúc hiểu ca là 9h. Nếu giờ chấm mà nằm trong khoảng từ 7h->9h thì giờ chấm đó sẽ hiểu là ca hành chính

                    //            if (TimeSpan.Compare(tsIn, tsBegin) != -1 && TimeSpan.Compare(tsIn, tsEnd) < 1)
                    //                return fItem;

                    //            #region Tests
                    //            //var kq = TimeSpan.Compare(tsIn, tsBegin);
                    //            //var k = kq;
                    //            //var c = k;
                    //            //var kq2 = TimeSpan.Compare(tsIn, tsEnd);
                    //            //var d1 = kq2;
                    //            //var d2 = d1;
                    //            //var d5 = d2;
                    //            #endregion
                    //        }
                }
            }

            return null;
        }
        private Entity.clsShiftInfo GetCaLamViec(DateTime WorkingDay, string TimeIn, Entity.clsScheduleShiftInfo scheduleShiftInfo)
        {
            // Cần thêm giờ để hiểu ca
            Entity.clsShiftInfo Info = null;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift1);
            if (Info != null) return Info;

            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift2);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift3);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift4);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift5);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift6);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift7);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift8);
            if (Info != null) return Info;
            Info = GetCaLamViec(WorkingDay, TimeIn, scheduleShiftInfo.Shift9);
            if (Info != null) return Info;

            return Info;
        }

        private double TinhDiMuon(string TimeIn, string TimeOut, Entity.clsShiftInfo shift, string lyDoVang)
        {
            if (shift == null) return 0;
            try
            {
                double totalMinutes = 0;
                TimeSpan tsBegin, tsEnd, tsIn, tsTemp, tsOut = TimeSpan.Parse(TimeOut);
                if (TimeSpan.TryParse(shift.TimeIn, out tsTemp))
                    if (TimeSpan.TryParse(TimeIn, out tsIn))
                        if (TimeSpan.TryParse(shift.BatDauTinhMuon, out tsBegin))
                            if (TimeSpan.TryParse(shift.TimeOut, out tsEnd))
                            {
                                //Thời gian chấm vào trong khoảng bắt đầu và kết thúc thì tính đi muộn
                                //Thời gian chấm vào lơn hơn hoặc bằng thời gian bắt đầu quy định. 
                                //Và nhỏ hơn hoặc bằng thời gian kết thúc theo quy định thì tính đi muộn

                                /*
                                 * nếu có lý do vắng mà nhân viên đi làm trễ thì xem đi làm trễ trong khoảng tgian nào
                                 * 
                                 */
                                if (lyDoVang != null && lyDoVang.ToString().Trim().Length > 0)
                                {
                                    // nếu đi làm rơi vào buổi sáng
                                    if (tsIn > tsTemp && tsIn < TimeSpan.Parse(shift.BreakIn)
                                        && tsOut <= TimeSpan.Parse(shift.BreakIn) && tsOut > tsTemp)
                                    {
                                        if (tsIn <= TimeSpan.Parse(shift.BreakOut))
                                        {
                                            totalMinutes = (tsIn - tsTemp).TotalMinutes;
                                        }
                                        else
                                        {
                                            totalMinutes = (TimeSpan.Parse(shift.BreakOut) - tsTemp).TotalMinutes;
                                        }
                                    }

                                    // nếu đi làm rơi vào buổi chiều
                                    if (tsIn > TimeSpan.Parse(shift.BreakIn) && tsIn < tsEnd
                                        && tsOut >= tsIn)
                                    {
                                        if (tsIn >= tsEnd)
                                        {
                                            totalMinutes = (tsEnd - TimeSpan.Parse(shift.BreakIn)).TotalMinutes;
                                        }
                                        else
                                        {
                                            totalMinutes = (tsIn - TimeSpan.Parse(shift.BreakIn)).TotalMinutes;
                                        }
                                    }

                                    return totalMinutes;
                                }

                                if (TimeSpan.Compare(tsIn, tsBegin) != -1 && TimeSpan.Compare(tsIn, tsEnd) < 1)
                                {
                                    if (tsIn >= TimeSpan.Parse(shift.BreakOut) && tsIn <= TimeSpan.Parse(shift.BreakIn))
                                    {
                                        return (TimeSpan.Parse(shift.BreakOut) - tsTemp).TotalMinutes;
                                    }
                                    else
                                    {
                                        return (tsIn - tsTemp).TotalMinutes;
                                    }
                                }
                            }

                return totalMinutes;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private double TinhVeSom(string TimeIn, string TimeOut, Entity.clsShiftInfo shift, string lyDoVang)
        {
            if (shift == null) return 0;
            try
            {
                TimeSpan tsBegin, tsEnd, tsOut, tsTemp, tsIn = TimeSpan.Parse(TimeIn);
                double totalMinutes = 0;
                if (TimeSpan.TryParse(TimeOut, out tsOut))
                    if (TimeSpan.TryParse(shift.TimeOut, out tsTemp))
                        if (TimeSpan.TryParse(shift.BatDauTinhMuon, out tsBegin))
                            if (TimeSpan.TryParse(shift.KetThucTinhSom, out tsEnd))
                            {
                                /*
                                * nếu có lý do vắng mà nhân viên về sớm thì xem về sớm trong khoảng tgian nào
                                * 
                                */
                                if (lyDoVang != null && lyDoVang.ToString().Trim().Length > 0)
                                {
                                    // nếu đi làm rơi vào buổi sáng
                                    if (tsIn < TimeSpan.Parse(shift.BreakOut)
                                        && tsOut < TimeSpan.Parse(shift.BreakOut) && tsOut > tsIn)
                                    {
                                        totalMinutes = (TimeSpan.Parse(shift.BreakOut) - tsOut).TotalMinutes;
                                    }

                                    // nếu đi làm rơi vào buổi chiều
                                    if (tsOut > TimeSpan.Parse(shift.BreakIn) && tsOut > tsIn
                                        && tsOut < tsTemp)
                                    {
                                        totalMinutes = (tsTemp - tsOut).TotalMinutes;
                                    }

                                    return totalMinutes;
                                }
                                if (TimeSpan.Compare(tsOut, tsBegin) != -1 && TimeSpan.Compare(tsOut, tsTemp) < 1)
                                    return (tsTemp - tsOut).TotalMinutes;
                            }
                return totalMinutes;

            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private double TongTG(string TimeIn, string TimeOut, float soGioRaVao, Entity.Interfaces.IShiftInfo shiftInfo)
        {
            try
            {
                if (shiftInfo == null) return 0;
                double totalHours = 0;
                TimeSpan tsIn, tsOut, breakIn, breakOut, mGioRa, mGioVao;
                tsIn = TimeSpan.Parse(TimeIn);
                tsOut = TimeSpan.Parse(TimeOut);
                breakIn = TimeSpan.Parse(shiftInfo.BreakIn);
                breakOut = TimeSpan.Parse(shiftInfo.BreakOut);
                mGioRa = TimeSpan.Parse(shiftInfo.TimeOut);
                mGioVao = TimeSpan.Parse(shiftInfo.TimeIn);
                if (tsOut > mGioRa)
                {
                    tsOut = mGioRa;
                }

                if (tsIn < mGioVao)
                {
                    tsIn = mGioVao;
                }
                else if (tsIn > breakOut && tsIn < breakIn)
                {
                    tsIn = breakIn;
                }
                TimeSpan mTimeOut;
                //Nếu số giờ chấm ra mà lớn hơn số kết thúc làm việc thì kiểm tra số phút để làm tròn
                if (TimeSpan.TryParse(shiftInfo.TimeOut, out mTimeOut) && tsOut > mTimeOut && tsOut != tsIn)
                {
                    int LamTron = 0;
                    _QuyTacLamTron.LamTronPhut(tsOut.Minutes, (result, exx) => { LamTron = result; });
                    tsOut = new TimeSpan(tsOut.Hours, LamTron, 0);
                }

                //Nếu số giờ chấm công nhỏ hơn số giờ làm việc thì lấy số giờ làm việc
                if (tsIn < TimeSpan.Parse(shiftInfo.TimeIn) && tsOut != tsIn)
                    tsIn = TimeSpan.Parse(shiftInfo.TimeIn);

                // nếu giờ ra lớn hơn giờ 
                if (tsOut > breakIn && tsOut != tsIn && tsIn < breakOut)
                {
                    if ((tsOut == null || tsOut == TimeSpan.Zero || string.IsNullOrEmpty(tsOut.ToString()))
                        || ((tsIn == null || tsIn == TimeSpan.Zero || string.IsNullOrEmpty(tsIn.ToString())))) // nếu không quẹt vân tay vào hoặc ra mới tính số giờ vào ra
                    {
                        totalHours = (tsOut - tsIn).TotalHours - (shiftInfo.TimeOff / 60f) - soGioRaVao;
                    }
                    else
                    {
                        totalHours = (tsOut - tsIn).TotalHours - (shiftInfo.TimeOff / 60f);
                    }
                }
                else // nếu nv chỉ đi làm 1 buổi trong ngày thì không trừ tgian nghỉ trưa
                {
                    if ((tsOut == null || tsOut == TimeSpan.Zero || string.IsNullOrEmpty(tsOut.ToString()))
                        || ((tsIn == null || tsIn == TimeSpan.Zero || string.IsNullOrEmpty(tsIn.ToString())))) // nếu không quẹt vân tay vào hoặc ra mới tính số giờ vào ra
                    {
                        totalHours = (tsOut - tsIn).TotalHours - soGioRaVao;
                    }
                    else
                    {
                        totalHours = (tsOut - tsIn).TotalHours;
                    }
                }

                // công ảo được nhiêu thể hiện nhiêu (giữ nguyên phần lẻ)
                return Math.Round(totalHours, 1);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private void TinhGioTangCa(Entity.Interfaces.IShiftInfo shift,
            ref Entity.Interfaces.ITimeInOutSessionInfo sessionInfo,
            Entity.Interfaces.IDangKyLamThemInfo ItemDangKyLamThem)
        {
            sessionInfo.OTX = 0;
            if (shift == null) return;
            /*** Nếu ca đó không cho phép tăng ca thì không tính ***/
            //if (!shift.IsTinhLamThem) return;

            TimeSpan tsIn, tsOut, tsShiftIn, tsShiftOut;
            if (!TimeSpan.TryParse(sessionInfo.TimeIn, out tsIn)) return;
            if (!TimeSpan.TryParse(sessionInfo.TimeOut, out tsOut)) return;
            if (!TimeSpan.TryParse(shift.TimeIn, out tsShiftIn)) return;
            if (!TimeSpan.TryParse(shift.TimeOut, out tsShiftOut)) return;

            #region QUY TẮC LÀM TRÒN
            int LamTron = 0;
            _QuyTacLamTron.LamTronPhut(tsOut.Minutes, (result, exx) => { LamTron = result; });
            tsOut = new TimeSpan(tsOut.Hours, LamTron, 0);
            #endregion

            //- Giờ tăng ca được tính dựa vào số phút quy định
            //- Ví dụ: 8h làm việc, quy định tính tăng ca trước 30phut
            // => Nếu giờ chấm vào từ 7h30-8h sẽ không được tính tăng ca
            // => Nếu giờ chấm vào trước 7h30  (7h29,7h28,..) thì sẽ bắt đầu tình giờ tăng ca 
            double tangCaTruoc = 0, tangCaSau = 0;
            #region KIỂM TRA CHO PHÉP TĂNG CA TRƯỚC GIỜ LÀM VIỆC HAY KHÔNG
            /*** Kiểm tra có cho phép tăng ca trước giờ làm hay không ***/
            if (shift.IsSoPhutTruocCa)
            {
                int mSoPhutTruocCa = (int)(tsIn - tsShiftIn).TotalMinutes;
                /*** Nếu số phút mà lớn hơn số phút cho phép tăng ca thì mới tính tăng ca ***/
                if (mSoPhutTruocCa > shift.SoPhutTruocCa)
                {
                    /*** Nếu số phút tăng ca mà nhỏ hơn giới hạn cho phép tăng ca trước giờ làm (MaxBeforeOTX) thì sẽ lấy max ***/
                    if (mSoPhutTruocCa > shift.MaxBeforeOTX)
                        tangCaTruoc = shift.MaxBeforeOTX;

                    tangCaTruoc = (mSoPhutTruocCa / 30) * 0.5;// -shift.SoPhutTruocCa;
                }
            }
            #endregion

            #region KIỂM TRA CHO PHÉP TĂNG CA SAU GIỜ LÀM VIỆC HAY KHÔNG
            /*** Nếu cho phép tăng ca sau giờ làm việc ***/
            if (shift.IsSoPhutSauCa)
            {
                /*** Tính số phút tăng ca ***/
                int mSoPhutTangCaSau = (int)(tsOut - tsShiftOut).TotalMinutes;
                /*** Nếu số phút lơn hơn hoặc bằng số phút quy định sẽ tính tăng ca ***/
                if (mSoPhutTangCaSau >= shift.SoPhutSauCa)
                {
                    /*** Nếu số phút tăng lớn hơn giới hạn cho phép tăng ca sau làm việc (MaxAfterOTX) thì sẽ lấy giới hạn tăng ***/
                    if (mSoPhutTangCaSau > shift.MaxAfterOTX)
                    {
                        /*** -Kiểm tra xem nhân viên này có trong danh mục đăng ký làm thêm hay không ***/
                        /*** -Nếu những lao động có tên trong danh sách đăng ký làm thêm thì mới được tính làm thêm giờ ngoài mức giới hạn ***/
                        TimeSpan mGioDangKy;
                        if (ItemDangKyLamThem != null && TimeSpan.TryParse(ItemDangKyLamThem.DenGio, out mGioDangKy))
                        {
                            //Nếu giờ chấm ra mà lớn hơn giờ đăng ký tăng ca
                            if (tsOut > mGioDangKy)
                                mSoPhutTangCaSau = (int)(mGioDangKy - tsShiftOut).TotalMinutes;
                            else//Nếu giờ chấm ra nhỏ hơn giờ làm việc (Giờ ra - Giờ kết thúc ca làm việc)
                                mSoPhutTangCaSau = (int)(tsOut - tsShiftOut).TotalMinutes;
                        }
                        else
                            mSoPhutTangCaSau = shift.MaxAfterOTX;
                    }

                    tangCaSau = (mSoPhutTangCaSau / 30) * 0.5;
                }
            }

            #endregion

            sessionInfo.OTX = (float)tangCaTruoc + (float)tangCaSau;
        }

        public System.Data.DataTable TYPE_TIMEINOUT_SESSION
        {
            get
            {
                #region
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_TIMEINOUT_SESSION");
                _tbl.Columns.Add("Id", typeof(System.Double));
                _tbl.Columns.Add("Workingday", typeof(System.DateTime));
                _tbl.Columns.Add("DepID", typeof(System.String));
                _tbl.Columns.Add("MaNV", typeof(System.String));
                _tbl.Columns.Add("TenNV", typeof(System.String));
                _tbl.Columns.Add("TimeIn", typeof(System.String));
                _tbl.Columns.Add("BreakOut", typeof(System.String));
                _tbl.Columns.Add("BreakIn", typeof(System.String));
                _tbl.Columns.Add("TimeOut", typeof(System.String));
                _tbl.Columns.Add("OverTime", typeof(float)); //(10)

                _tbl.Columns.Add("TotalTime", typeof(float));
                _tbl.Columns.Add("TotalTimeHM", typeof(System.String));
                _tbl.Columns.Add("TotalHours", typeof(float));
                _tbl.Columns.Add("LyDoVang", typeof(System.String));
                _tbl.Columns.Add("IsChuanHoa", typeof(System.Boolean));
                _tbl.Columns.Add("SchName", typeof(System.String));
                _tbl.Columns.Add("StrShiftID", typeof(System.String));
                _tbl.Columns.Add("Shift", typeof(System.String));
                _tbl.Columns.Add("StrTimeIn", typeof(System.String));
                _tbl.Columns.Add("StrBreakOut", typeof(System.String)); //(10)

                _tbl.Columns.Add("StrBreakIn", typeof(System.String));
                _tbl.Columns.Add("StrTimeOut", typeof(System.String));
                _tbl.Columns.Add("TotalTimeMinute", typeof(float));
                _tbl.Columns.Add("LateTime", typeof(float));
                _tbl.Columns.Add("EarlyTime", typeof(float));
                _tbl.Columns.Add("IsLate]", typeof(System.Boolean));
                _tbl.Columns.Add("IsEarly", typeof(System.Boolean));
                _tbl.Columns.Add("DateTimeIn", typeof(System.DateTime));
                _tbl.Columns.Add("DateBreakOut", typeof(System.DateTime));
                _tbl.Columns.Add("DateBreakIn", typeof(System.DateTime)); //(10)

                _tbl.Columns.Add("DateTimeOut", typeof(System.DateTime));
                _tbl.Columns.Add("CongK", typeof(float));
                _tbl.Columns.Add("CongX", typeof(float));
                _tbl.Columns.Add("Cong30P", typeof(float));
                _tbl.Columns.Add("CongAnTrua", typeof(float));
                _tbl.Columns.Add("CongAnChieu", typeof(float));
                _tbl.Columns.Add("CongPhep", typeof(float));
                _tbl.Columns.Add("CongThieu", typeof(float));
                _tbl.Columns.Add("IsTS", typeof(System.Boolean));
                _tbl.Columns.Add("GioConBu", typeof(float)); //(10)

                _tbl.Columns.Add("GioThai7Thang", typeof(float));
                _tbl.Columns.Add("ModifyDate", typeof(System.DateTime));
                _tbl.Columns.Add("OTX", typeof(float));
                return _tbl;
                #endregion
            }
        }

        public void AddRow(Entity.Interfaces.ITimeInOutSessionInfo Item, ref System.Data.DataTable TYPE_TIMEINOUT_SESSION)
        {
            #region
            System.Data.DataRow newRow = TYPE_TIMEINOUT_SESSION.NewRow();
            newRow["Id"] = Item.Id;
            newRow["Workingday"] = Item.Workingday;
            newRow["DepID"] = Item.DepID == null ? "" : Item.DepID;
            newRow["MaNV"] = Item.MaNV;
            newRow["TenNV"] = Item.TenNV;
            newRow["TimeIn"] = Item.TimeIn == null ? "" : Item.TimeIn;
            newRow["BreakOut"] = Item.BreakOut == null ? "" : Item.BreakOut;
            newRow["BreakIn"] = Item.BreakIn == null ? "" : Item.BreakIn;
            newRow["TimeOut"] = Item.TimeOut == null ? "" : Item.TimeOut;
            newRow["OverTime"] = Item.OverTime; //(10)

            newRow["TotalTime"] = Item.TotalTime;
            newRow["TotalTimeHM"] = Item.TotalTimeHM == null ? "" : Item.TotalTimeHM;
            newRow["TotalHours"] = Item.TotalHours;
            newRow["LyDoVang"] = Item.LyDoVang == null ? "" : Item.LyDoVang;
            newRow["IsChuanHoa"] = Item.IsChuanHoa;
            newRow["SchName"] = Item.SchName == null ? "" : Item.SchName;
            newRow["StrShiftID"] = Item.StrShiftID == null ? "" : Item.StrShiftID;
            newRow["Shift"] = Item.Shift == null ? "" : Item.Shift;
            newRow["StrTimeIn"] = Item.StrTimeIn == null ? "" : Item.StrTimeIn;
            newRow["StrBreakOut"] = Item.StrBreakOut == null ? "" : Item.StrBreakOut; //(10)

            newRow["StrBreakIn"] = Item.StrBreakIn == null ? "" : Item.StrBreakIn;
            newRow["StrTimeOut"] = Item.StrTimeOut == null ? "" : Item.StrTimeOut;
            newRow["TotalTimeMinute"] = Item.TotalTimeMinute;
            newRow["LateTime"] = Item.LateTime;
            newRow["EarlyTime"] = Item.EarlyTime;
            newRow["IsLate]"] = Item.IsLate;
            newRow["IsEarly"] = Item.IsEarly;
            newRow["DateTimeIn"] = DateTime.Now;
            newRow["DateBreakOut"] = DateTime.Now;
            newRow["DateBreakIn"] = DateTime.Now; //(10)

            newRow["DateTimeOut"] = DateTime.Now;
            newRow["CongK"] = Item.CongK;
            newRow["CongX"] = Item.CongX;
            newRow["Cong30P"] = Item.Cong30P;
            newRow["CongAnTrua"] = Item.CongAnTrua;
            newRow["CongAnChieu"] = Item.CongAnChieu;
            newRow["CongPhep"] = Item.CongPhep;
            newRow["CongThieu"] = Item.CongThieu;
            newRow["IsTS"] = Item.IsTS;
            newRow["GioConBu"] = Item.GioConBu; //(10)

            newRow["GioThai7Thang"] = Item.GioThai7Thang;
            newRow["ModifyDate"] = DateTime.Now;
            newRow["OTX"] = Item.OTX;

            TYPE_TIMEINOUT_SESSION.Rows.Add(newRow);
            #endregion
        }

        private void UpdateSession(Entity.Interfaces.ITimeInOutSessionInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                System.Data.DataTable tbl = this.TYPE_TIMEINOUT_SESSION;
                this.AddRow(Item, ref tbl);
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMEINOUT_SESSION_TINHCONG_VIRTUAL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@TYPE", tbl);

                    error.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("UpdateSession", ex.Message);
            }
            if (Complete != null) Complete(error);
        }

        /// <summary>
        /// Lấy chế độ của nhân viên
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="Ngay"></param>
        /// <returns></returns>
        private string CheDo(string MaNV, string Ngay)
        {
            return "";
        }

        /// <summary>
        /// Đếm số lần đi muộn
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="Month"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        private int SoLanDiMuon(string MaNV, int Month, int Year)
        {
            return 0;
        }

        /// <summary>
        /// Đếm số lần về sớm
        /// </summary>
        /// <param name="MaNv"></param>
        /// <param name="Mont"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        private int SoLanVeSom(string MaNv, int Mont, int Year)
        {
            return 0;
        }

        /// <summary>
        /// Kiểm tra ngày đó có phải là ngày lễ - tết hay không?
        /// </summary>
        /// <param name="Ngay"></param>
        /// <returns></returns>
        private bool CheckNgayLeTet(DateTime Ngay)
        {

            return false;
        }
    }
}
