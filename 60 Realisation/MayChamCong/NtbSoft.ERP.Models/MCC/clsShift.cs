﻿using NtbSoft.ERP.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsShift:Interfaces.IShift
    {
        public System.Data.DataTable TYPE_DIC_SHIFT
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_SHIFT");
                _tbl.Columns.Add("Id", typeof(System.Int32));

                _tbl.Columns.Add("TuNgay", typeof(System.DateTime));
                _tbl.Columns.Add("ShiftId", typeof(System.String));
                _tbl.Columns.Add("ShiftName", typeof(System.String));
                _tbl.Columns.Add("TimeIn", typeof(System.String));
                _tbl.Columns.Add("BreakOut", typeof(System.String));
                _tbl.Columns.Add("BreakIn", typeof(System.String));
                _tbl.Columns.Add("TimeOut", typeof(System.String));
                _tbl.Columns.Add("StartIn", typeof(System.String));
                _tbl.Columns.Add("EndIn", typeof(System.String));
                _tbl.Columns.Add("StartOut", typeof(System.String));
                _tbl.Columns.Add("EndOut", typeof(System.String));
                _tbl.Columns.Add("Total", typeof(System.String));
                _tbl.Columns.Add("TimeOff", typeof(float));
                _tbl.Columns.Add("CheckIn", typeof(System.Boolean));
                _tbl.Columns.Add("CheckOut", typeof(System.Boolean));
                _tbl.Columns.Add("TotalTime", typeof(System.Int32));

                _tbl.Columns.Add("IsTinhLamThem", typeof(System.Boolean));
                _tbl.Columns.Add("IsSoPhutTruocCa", typeof(System.Boolean));
                _tbl.Columns.Add("SoPhutTruocCa", typeof(System.Int32));
                _tbl.Columns.Add("IsSoPhutSauCa", typeof(System.Boolean));
                _tbl.Columns.Add("SoPhutSauCa", typeof(System.Int32));

                _tbl.Columns.Add("IsShiftNight", typeof(System.Boolean));
                _tbl.Columns.Add("IsHanhChinh", typeof(System.Boolean));
                _tbl.Columns.Add("IsCachQuyCong", typeof(System.Boolean));
                _tbl.Columns.Add("Status", typeof(System.Int32));
                _tbl.Columns.Add("IsTinhAnToi", typeof(System.Boolean));
                _tbl.Columns.Add("BatDauAnToi", typeof(System.String));
                _tbl.Columns.Add("KetThucAnToi", typeof(System.String));
                _tbl.Columns.Add("ThoiGianAnToi", typeof(System.Int32));
                _tbl.Columns.Add("BatDauTinhMuon", typeof(System.String));
                _tbl.Columns.Add("KetThucTinhMuon", typeof(System.String));
                _tbl.Columns.Add("BatDauTinhSom", typeof(System.String));
                _tbl.Columns.Add("KetThucTinhSom", typeof(System.String));
                _tbl.Columns.Add("FromMinute", typeof(System.Int32));
                _tbl.Columns.Add("GioHienThi", typeof(float));
                _tbl.Columns.Add("SoGioDuCong", typeof(float));
                _tbl.Columns.Add("LoaiCa", typeof(System.Int32));
                _tbl.Columns.Add("IsHienThiMaCa", typeof(System.Boolean));
                _tbl.Columns.Add("IsBoQuaChuNhat", typeof(System.Boolean));


                _tbl.Columns.Add("IsThaiSan", typeof(System.Boolean));
                _tbl.Columns.Add("IsCaConBu", typeof(System.Boolean));
                _tbl.Columns.Add("IsCaThai7Thang", typeof(System.Boolean));
                _tbl.Columns.Add("IsCaCuoiTuan", typeof(System.Boolean));
                _tbl.Columns.Add("CaCuoiTuan", typeof(float));
                _tbl.Columns.Add("IsCaNgayLe", typeof(System.Boolean));
                _tbl.Columns.Add("CaNgayLe", typeof(float));
                _tbl.Columns.Add("ChuyenNgay", typeof(System.Int32));

                _tbl.Columns.Add("IsTongSoPhutLam", typeof(System.Boolean));
                _tbl.Columns.Add("TongSoPhutLam", typeof(int));

                _tbl.Columns.Add("MaxBeforeOTX", typeof(int));
                _tbl.Columns.Add("MaxAfterOTX", typeof(int));
                return _tbl;
            }
         
        }

        private string GetTimeTemp(string time)
        {
            if (string.IsNullOrWhiteSpace(time)||time.Equals("00:00")) return "";
            return time;
        }
        public void AddRow(Entity.Interfaces.IShiftInfo Item, ref System.Data.DataTable TYPE_SHIFT)
        {
            System.Data.DataRow newRow = TYPE_SHIFT.NewRow();
            newRow["Id"] = Item.Id;
            newRow["TuNgay"] = Item.TuNgay;
            newRow["ShiftId"] = Item.ShiftId.ToUpper();
            newRow["ShiftName"] = Item.ShiftName;
            newRow["TimeIn"] = GetTimeTemp(Item.TimeIn);
            newRow["BreakOut"] = GetTimeTemp(Item.BreakOut);
            newRow["BreakIn"] = GetTimeTemp(Item.BreakIn);
            newRow["TimeOut"] = GetTimeTemp(Item.TimeOut);
            newRow["StartIn"] = GetTimeTemp(Item.StartIn);
            newRow["EndIn"] = GetTimeTemp(Item.EndIn);
            newRow["StartOut"] = GetTimeTemp(Item.StartOut);
            newRow["EndOut"] = GetTimeTemp(Item.EndOut);
            newRow["Total"] = Item.Total;
            newRow["TimeOff"] = Item.TimeOff;
            newRow["CheckIn"] = Item.CheckIn;
            newRow["CheckOut"] = Item.CheckOut;
            newRow["TotalTime"] = Item.TotalTime;

            newRow["IsTinhLamThem"] = Item.IsTinhLamThem;
            newRow["IsSoPhutTruocCa"] = Item.IsSoPhutTruocCa;
            newRow["SoPhutTruocCa"] = Item.SoPhutTruocCa;
            newRow["IsSoPhutSauCa"] = Item.IsSoPhutSauCa;
            newRow["SoPhutSauCa"] = Item.SoPhutSauCa;

            newRow["IsShiftNight"] = Item.IsShiftNight;
            newRow["IsHanhChinh"] = Item.IsHanhChinh;
            newRow["IsCachQuyCong"] = Item.IsCachQuyCong;
            newRow["Status"] = Item.Status;
            newRow["IsTinhAnToi"] = Item.IsTinhAnToi;
            newRow["BatDauAnToi"] = Item.BatDauAnToi;
            newRow["KetThucAnToi"] = Item.KetThucAnToi;
            newRow["ThoiGianAnToi"] = Item.ThoiGianAnToi;
            newRow["BatDauTinhMuon"] = Item.BatDauTinhMuon;
            newRow["KetThucTinhMuon"] = Item.KetThucTinhMuon;
            newRow["BatDauTinhSom"] = Item.BatDauTinhSom;
            newRow["KetThucTinhSom"] = Item.KetThucTinhSom;
            newRow["FromMinute"] = Item.FromMinute;
            newRow["GioHienThi"] = Item.GioHienThi;


            /*** Tinh số giờ đủ công ***/
            TimeSpan tsIn, tsOut;
            if (TimeSpan.TryParse(Item.TimeIn, out tsIn))
            {
                if (TimeSpan.TryParse(Item.TimeOut, out tsOut))
                    Item.SoGioDuCong = (float)Math.Round((tsOut - tsIn).TotalMinutes - Item.TimeOff, 1);
            }

            newRow["SoGioDuCong"] = Item.SoGioDuCong;
            newRow["LoaiCa"] = Item.IsLoaiCa;
            newRow["IsHienThiMaCa"] = Item.IsHienThiMaCa;
            newRow["IsBoQuaChuNhat"] = Item.IsBoQuaChuNhat;


            newRow["IsThaiSan"] = Item.IsThaiSan;
            newRow["IsCaConBu"] = Item.IsCaConBu;
            newRow["IsCaThai7Thang"] = Item.IsCaThai7Thang;
            newRow["IsCaCuoiTuan"] = Item.IsCaCuoiTuan;
            newRow["CaCuoiTuan"] = Item.CaCuoiTuan;
            newRow["IsCaNgayLe"] = Item.IsCaNgayLe;
            newRow["CaNgayLe"] = Item.CaNgayLe;
            newRow["ChuyenNgay"] = Item.ChuyenNgay;

            newRow["IsTongSoPhutLam"] = Item.IsTongSoPhutLam;
            newRow["TongSoPhutLam"] = Item.TongSoPhutLam;

            newRow["MaxBeforeOTX"] = Item.MaxBeforeOTX;
            newRow["MaxAfterOTX"] = Item.MaxAfterOTX;

            TYPE_SHIFT.Rows.Add(newRow);
        }

        public void GetBy(string ShiftId, Action<Entity.clsShiftInfo, IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            Entity.clsShiftInfo Item = new Entity.clsShiftInfo();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_SHIFT");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where ShiftId='{0}'", ShiftId));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(tbl);
                        Item = Items[0];
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(Item, error);
        }

        /// <summary>
        /// 1: Chủ nhật
        /// 2: Thứ 2
        /// 3: Thứ 3
        /// 4: Thứ 4
        /// 5: Thứ 5
        /// 6: Thứ 6
        /// 7: Thứ 7
        /// 8: Ngày lễ
        /// </summary>
        /// <param name="EmpID"></param>
        /// <param name="Ngay"></param>
        /// <param name="Complete"></param>
        public void GetBy(double EmpID, int Ngay, Action<Entity.clsShiftInfo, IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            Entity.clsShiftInfo Item = new Entity.clsShiftInfo();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_MCC_SHIFT_GETBY_EMPID", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@EmpID", EmpID);
                    _cmd.Parameters.AddWithValue("@Ngay", Ngay);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(tbl);
                        Item = Items[0];
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetBy", ex.Message);
            }
            if (Complete != null) Complete(Item, error);

        }

        public void GetAll(Action<List<Entity.clsShiftInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<Entity.clsShiftInfo> Items = new List<Entity.clsShiftInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@TYPE_SHIFT", this.TYPE_DIC_SHIFT);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsShiftInfo>(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }

        public void GetAll(Action<JData.DataComboBoxInfo[], Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            JData.DataComboBoxInfo[] Items = new JData.DataComboBoxInfo[] { };
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "GET");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@TYPE_SHIFT", this.TYPE_DIC_SHIFT);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        Items = new JData.DataComboBoxInfo[tbl.Rows.Count + 1];
                        Items[0] = new JData.DataComboBoxInfo("", "None");
                        for (int i = 0; i < tbl.Rows.Count; i++)
                            Items[i+1]=new JData.DataComboBoxInfo(Convert.ToString(tbl.Rows[i]["ShiftId"]),
                                Convert.ToString(tbl.Rows[i]["ShiftName"]));
                    }
                    _adapter.Dispose();
                    _adapter = null;
                        
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }
        public void Update(Entity.Interfaces.IShiftInfo Item, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                System.Data.DataTable tbl = this.TYPE_DIC_SHIFT;
                this.AddRow(Item, ref tbl);
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@Id", 0);
                    _cmd.Parameters.AddWithValue("@TYPE_SHIFT", tbl);

                    error.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("Update", ex.Message);
            }
            if (Complete != null) Complete(error);
        }

        public void Delete(int Id, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_SHIFT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@Id", Id);
                    _cmd.Parameters.AddWithValue("@TYPE_SHIFT", this.TYPE_DIC_SHIFT);

                    error.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("Update", ex.Message);
            }
            if (Complete != null) Complete(error);
        }
    }
}
