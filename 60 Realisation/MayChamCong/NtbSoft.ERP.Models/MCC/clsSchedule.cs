﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsSchedule : Interfaces.ISchedule
    {
        private System.Data.SqlClient.SqlParameter[] GetParamaters(Libs.ApiAction action, Entity.Interfaces.IScheduleInfo Item)
        {
            System.Data.SqlClient.SqlParameter[] paramaters
                ={
                     new System.Data.SqlClient.SqlParameter("@Action",action.ToString()),
                     new System.Data.SqlClient.SqlParameter("@Id",Item.Id),
                     new System.Data.SqlClient.SqlParameter("@SchName",Item.SchName==null?"":Item.SchName),
                     new System.Data.SqlClient.SqlParameter("@Shift",Item.Shift==null?"":Item.Shift),
                     new System.Data.SqlClient.SqlParameter("@Chuky",Item.Chuky),
                     new System.Data.SqlClient.SqlParameter("@Descriptions",Item.Descriptions==null?"":Item.Descriptions),
                     new System.Data.SqlClient.SqlParameter("@NumberShift",Item.NumberShift),
                     new System.Data.SqlClient.SqlParameter("@Shift1",Item.Shift1==null?"":Item.Shift1),
                     new System.Data.SqlClient.SqlParameter("@Shift2",Item.Shift2==null?"":Item.Shift2),
                     new System.Data.SqlClient.SqlParameter("@Shift3",Item.Shift3==null?"":Item.Shift3),
                     new System.Data.SqlClient.SqlParameter("@TimeInS1",Item.TimeInS1==null?"":Item.TimeInS1),
                     new System.Data.SqlClient.SqlParameter("@TimeInS2",Item.TimeInS2==null?"":Item.TimeInS2),
                     new System.Data.SqlClient.SqlParameter("@TimeInS3",Item.TimeInS3==null?"":Item.TimeInS3)
                 };

            return paramaters;
        }
        public void GetAll(Action<List<Entity.clsScheduleInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            List<Entity.clsScheduleInfo> Items = new List<Entity.clsScheduleInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddRange(this.GetParamaters(Libs.ApiAction.GET, new Entity.clsScheduleInfo()));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsScheduleInfo>(tbl);
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }

        public void GetAll(Action<JData.DataComboBoxInfo[],Interfaces.IResultOk> Complete){
            Interfaces.IResultOk error = new BaseResultOk();
            JData.DataComboBoxInfo[] Items = new JData.DataComboBoxInfo[] { };
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddRange(this.GetParamaters(Libs.ApiAction.GET, new Entity.clsScheduleInfo()));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        Items = new JData.DataComboBoxInfo[tbl.Rows.Count + 1];
                        Items[0] = new JData.DataComboBoxInfo("", "None");
                        for (int i = 0; i < tbl.Rows.Count; i++)
                            Items[i + 1] = new JData.DataComboBoxInfo(Convert.ToString(tbl.Rows[i]["SchName"]),
                                Convert.ToString(tbl.Rows[i]["Shift"]));
                    }
                    _adapter.Dispose();
                    _adapter = null;
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GetAll", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }
        public void Update(Libs.ApiAction Action,Entity.Interfaces.IScheduleInfo Item, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            if(Item==null || string.IsNullOrWhiteSpace(Item.SchName))
            {
                error.Errors.Add("Update", "Mã lịch trình không được để trống");
                goto exit;
            }
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddRange(this.GetParamaters(Action, Item));

                    error.RowsAffected = _cmd.ExecuteNonQuery();
                 
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("Action_" + Action.ToString(), ex.Message);
            }
            exit:
            if (Complete != null) Complete(error);
        }

        public void Delete(int Id, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_SCHEDULE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    Entity.clsScheduleInfo item = new Entity.clsScheduleInfo();
                    item.Id = Id;
                    _cmd.Parameters.AddRange(this.GetParamaters(Libs.ApiAction.DELETE, item));

                    error.RowsAffected = _cmd.ExecuteNonQuery();
                    if (error.RowsAffected <= 0)
                        error.Errors.Add("Delete", "Không xóa được.");
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("Delete", ex.Message);
            }
        }


       
    }
}
