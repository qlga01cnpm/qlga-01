﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.MCC
{
    public class clsAttLogsSession : Interfaces.IAttLogsSession
    {
        public void Modify(Libs.ApiAction Action, Entity.Interfaces.IXuLyChamCongInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items = new List<Entity.Interfaces.IXuLyChamCongInfo>();

            if (Action == Libs.ApiAction.GET) return;

            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_ATTLOGS_SESSION_REAL_REMOVE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Action.ToString());
                    _cmd.Parameters.AddWithValue("@Id", Item.Id);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("Remove", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }
    }


    public class clsAttLogsSessionVỉtual : Interfaces.IAttLogsSession
    {
        public void Modify(Libs.ApiAction Action, Entity.Interfaces.IXuLyChamCongInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk _ok = new BaseResultOk();
            IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items = new List<Entity.Interfaces.IXuLyChamCongInfo>();

            if (Action == Libs.ApiAction.GET) return;

            System.Data.DataTable tbl = new System.Data.DataTable();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_ATTLOGS_SESSION_VIRTUAL_REMOVE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Action.ToString());
                    _cmd.Parameters.AddWithValue("@Id", Item.Id);

                    _ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _ok.Errors.Add("Remove", ex.Message);
            }
            if (Complete != null) Complete(_ok);
        }
    }
}
