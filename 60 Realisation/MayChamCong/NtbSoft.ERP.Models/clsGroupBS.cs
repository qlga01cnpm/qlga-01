using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using NtbSoft.ERP.Entity;

namespace NtbSoft.ERP.Models
{
    public class clsGroupBS:MarshalByRefObject
    {               
        public enum GroupAction { Insert, Update, Delete, MultilangUI, MultilangData }
       
        /// <summary>
        /// RoleCollection
        /// </summary>
        public clsGroupCollection GetListOfGroup()
        {
            clsGroupCollection groupCollection = new clsGroupCollection();
            using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
            {
                SqlCommand _cmd = new SqlCommand("sp_SYS_GROUP_SELECTALL", _cnn);
                _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader reader = _cmd.ExecuteReader();
                while (reader.Read())
                {
                    clsGroupInfo objGroup = new clsGroupInfo();

                    objGroup.GroupID = reader["GroupID"].ToString();
                    objGroup.GroupName = reader["GroupName"].ToString();
                    objGroup.Descriptions = reader["Descriptions"].ToString();
                    objGroup.IsAdmin = (bool)reader["IsAdmin"];

                    groupCollection.Add(objGroup);
                }
                //Call Close when done reading.
                reader.Close();
            }

            return groupCollection;                      
        }

        
    }
}
