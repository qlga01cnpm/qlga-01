﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    public class clsThuVien
    {
        public Entity.clsThuVienInfo GetBy(string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 *");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THUVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where MenuId='{0}'", MenuId));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl.Rows.Count == 0) return new Entity.clsThuVienInfo();

                    List<Entity.clsThuVienInfo> arrayTemp = new List<Entity.clsThuVienInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThuVienInfo()
                        {
                            MaThuVien = m.Field<string>("MaThuVien"),
                            MoTa = m.Field<string>("MoTa"),
                            GhiChu = m.Field<string>("GhiChu"),
                            KemThietBi = m.Field<bool>("KemThietBi"),
                            ChieuDaiDuongMay = float.Parse(m["ChieuDaiDuongMay"].ToString()),
                            TMU = float.Parse(m["TMU"].ToString()),
                            DuocTaoBoiHeThong = m.Field<bool>("DuocTaoBoiHeThong"),
                            MenuId = m.Field<string>("MenuId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThuVien.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsThuVienInfo GetByMaSo(string MaSo)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 t1.*,t2.MenuType");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THUVIEN t1 inner join dbo.SYS_MENU_MASTER t2 on t1.MenuId=t2.MenuId");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where t1.MaThuVien like '{0}'", MaSo));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl.Rows.Count == 0) return new Entity.clsThuVienInfo();

                    List<Entity.clsThuVienInfo> arrayTemp = new List<Entity.clsThuVienInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThuVienInfo()
                        {
                            MaThuVien = m.Field<string>("MaThuVien"),
                            MoTa = m.Field<string>("MoTa"),
                            GhiChu = m.Field<string>("GhiChu"),
                            KemThietBi = m.Field<bool>("KemThietBi"),
                            ChieuDaiDuongMay = float.Parse(m["ChieuDaiDuongMay"].ToString()),
                            TMU = float.Parse(m["TMU"].ToString()),
                            DuocTaoBoiHeThong = m.Field<bool>("DuocTaoBoiHeThong"),
                            MenuId = m.Field<string>("MenuId"),
                            MenuType=m.Field<string>("MenuType")
                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThuVien.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsThuVienCollection FindMatch(string MaSo)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", " *");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THUVIEN");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where MaThuVien like'%{0}%'", MaSo));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl.Rows.Count == 0) return new Entity.clsThuVienCollection();

                    List<Entity.clsThuVienInfo> arrayTemp = new List<Entity.clsThuVienInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThuVienInfo()
                        {
                            MaThuVien = m.Field<string>("MaThuVien"),
                            MoTa = m.Field<string>("MoTa"),
                            GhiChu = m.Field<string>("GhiChu"),
                            KemThietBi = m.Field<bool>("KemThietBi"),
                            ChieuDaiDuongMay = float.Parse(m["ChieuDaiDuongMay"].ToString()),
                            TMU = float.Parse(m["TMU"].ToString()),
                            DuocTaoBoiHeThong = m.Field<bool>("DuocTaoBoiHeThong"),
                            MenuId = m.Field<string>("MenuId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return new Entity.clsThuVienCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThietBi.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsThuVienInfo Item)
        {
            try
            { 
                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_THUVIEN", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.PUT.ToString());
                    cmd.Parameters.AddWithValue("@MaThuVien", Item.MaThuVien );
                    cmd.Parameters.AddWithValue("@MoTa", Item.MoTa == null ? "" : Item.MoTa);
                    cmd.Parameters.AddWithValue("@GhiChu", Item.GhiChu == null ? "" : Item.GhiChu);

                    cmd.Parameters.AddWithValue("@KemThietBi", Item.KemThietBi);
                    cmd.Parameters.AddWithValue("@ChieuDaiDuongMay", Item.ChieuDaiDuongMay);
                    cmd.Parameters.AddWithValue("@TMU", Item.TMU);
                    cmd.Parameters.AddWithValue("@DuocTaoBoiHeThong", Item.DuocTaoBoiHeThong);
                    cmd.Parameters.AddWithValue("@MenuId", Item.MenuId);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsThuVien.Update: " + ex.Message);
            }
        }
    }
}
