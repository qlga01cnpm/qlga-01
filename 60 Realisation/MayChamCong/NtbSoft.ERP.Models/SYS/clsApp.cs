﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.SYS
{
    public class clsApp:Interfaces.IApp
    {
        public void GetInfo(Action<Entity.Interfaces.IAppInfo, Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            Entity.Interfaces.IAppInfo info = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "top 1 *");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.SYS_APP");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsAppInfo>(tbl);
                        info = Items[0];
                    }
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("GetInfo", ex.Message);
            }
            if (Complete != null) Complete(info, ok);
        }


        public void Update(Entity.Interfaces.IAppInfo Item, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_SYS_APP_UPDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Id", Item.Id);
                    _cmd.Parameters.AddWithValue("@Name", Item.Name);
                    _cmd.Parameters.AddWithValue("@Address", Item.Address);
                    _cmd.Parameters.AddWithValue("@Tel", Item.Tel);
                    _cmd.Parameters.AddWithValue("@Fax", Item.Fax);
                    _cmd.Parameters.AddWithValue("@Website", Item.Website);
                    _cmd.Parameters.AddWithValue("@Email", Item.Email);
                    _cmd.Parameters.AddWithValue("@MaSoThue", Item.MaSoThue);
                    _cmd.Parameters.AddWithValue("@Logo", Item.Logo);
                    _cmd.Parameters.AddWithValue("@IsAutoBackup", Item.IsAutoBackup);
                    _cmd.Parameters.AddWithValue("@Url", Item.Url);
                    _cmd.Parameters.AddWithValue("@IsVirtualData", Item.IsVirtualData);
                    _cmd.Parameters.AddWithValue("@IsRunMCC", Item.IsRunMCC);
                    
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                    
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Update", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }
    }
}
