﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.ERP.Entity.Interfaces;
using NtbSoft.ERP.Models.Interfaces;

namespace NtbSoft.ERP.Models
{
    public interface ISinhVienImport
    {
        void CheckStudentID(string StudentID, Action<bool, Interfaces.IResultOk> Complete);
        void ImportSinhVien(System.Data.DataTable TypeSinhVien, Action<Interfaces.IResultOk> Complete);
    }

    public interface ISinhVien
    {
        void GetThongTinSinhVien(Action<IEnumerable<Entity.Interfaces.ISinhVienInfo>, Exception> Complete);
        void Update(Entity.Interfaces.ISinhVienInfo item, Action<Interfaces.IResultOk> Complete);
        void Insert(Entity.Interfaces.ISinhVienInfo item, Action<Interfaces.IResultOk> Complete);
        void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete);
    }

    public class clsSinhVien : ISinhVien
    {
        public void GetThongTinSinhVien(Action<IEnumerable<Entity.Interfaces.ISinhVienInfo>, Exception> Complete)
        {
            List<Entity.clsSinhVienInfo> Items = new List<Entity.clsSinhVienInfo>();
            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_SINHVIEN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsSinhVienInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }
        public void Update(Entity.Interfaces.ISinhVienInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_SINHVIEN_POST_PUT_DEL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@StudentID", item.StudentID);
                    _cmd.Parameters.AddWithValue("@Name", item.Name);
                    _cmd.Parameters.AddWithValue("@Age", item.Age);
                    _cmd.Parameters.AddWithValue("@Address", item.Address);
                    _cmd.Parameters.AddWithValue("@PhoneNumber", item.PhoneNumber);
                    _cmd.Parameters.AddWithValue("@Email", item.Email);

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void Insert(Entity.Interfaces.ISinhVienInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_DIC_SINHVIEN_POST_PUT_DEL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@StudentID", item.StudentID);
                    _cmd.Parameters.AddWithValue("@Name", item.Name);
                    _cmd.Parameters.AddWithValue("@Age", item.Age);
                    _cmd.Parameters.AddWithValue("@Address", item.Address);
                    _cmd.Parameters.AddWithValue("@PhoneNumber", item.PhoneNumber);
                    _cmd.Parameters.AddWithValue("@Email", item.Email);

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_SINHVIEN_POST_PUT_DEL", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@StudentID", Id);
                    _cmd.Parameters.AddWithValue("@Name", "");
                    _cmd.Parameters.AddWithValue("@Age", 0);
                    _cmd.Parameters.AddWithValue("@Address", "");
                    _cmd.Parameters.AddWithValue("@PhoneNumber", "");
                    _cmd.Parameters.AddWithValue("@Email", "");
                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }

            if (Complete != null) Complete(_result);
        }
    }


    public class clsSinhVienImport : ISinhVienImport
    {
        /// <summary>
        /// Kiem tra ma nhan vien
        /// </summary>
        /// <param name="MaNV"></param>
        /// <param name="Complete"></param>
        public void CheckStudentID(string StudentID, Action<bool, Interfaces.IResultOk> Complete)
        {
            bool exits = false;
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(
                        string.Format("select count(StudentID) from TEST_THONGTINCANHAN where StudentID = '{0}'", StudentID), _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    //System.Data.DataTable tbl = new System.Data.DataTable();
                    //_adapter.Fill(tbl);
                    //tbl.Rows[0][0].ToString();

                    int value = (int)_cmd.ExecuteScalar();
                    if (value > 0)
                        exits = true;
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("CheckMaNV", ex.Message);
            }

            if (Complete != null) Complete(exits, ok);
        }

        public void ImportSinhVien(System.Data.DataTable TypeSinhVien, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_SINHVIEN_IMPORT", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@TYPE_SinhVien", TypeSinhVien);
                    ok.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("Import", ex.Message);
            }

            if (Complete != null) Complete(ok);
        }


    }

    public class clsSinhVienType
    {
        public System.Data.DataTable TYPE_DIC_SINHVIEN
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_SINHVIEN");
                _tbl.Columns.Add("StudentID", typeof(String));
                _tbl.Columns.Add("Name", typeof(String));
                _tbl.Columns.Add("Age", typeof(Int32));
                _tbl.Columns.Add("Address", typeof(String));
                _tbl.Columns.Add("PhoneNumber", typeof(String));
                _tbl.Columns.Add("Email", typeof(String));
                return _tbl;
            }
        }

    }
}
