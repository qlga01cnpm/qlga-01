﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public interface IKhachHang
    {
        void GetThongTinKhachHang(int SelectedPage, int pageSize, string KhachHangId, string TenKhachHang, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void Update(Entity.Interfaces.IQLGRKhachHangInfo item, Action<Interfaces.IResultOk> Complete);
        void Insert(Entity.Interfaces.IQLGRKhachHangInfo item, Action<Interfaces.IResultOk> Complete);
        void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete);
        void InsertHoSoXe(Entity.Interfaces.IQLGRHoSoXeInfo item, Action<Interfaces.IResultOk> Complete);
        void GetHoSoXe(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void GetDetailPSC(int SelectedPage, int pageSize, string DepId, int Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void GetHieuXe(Action<DataTable, Exception> Complete);
        void GetPhuTung(Action<DataTable, Exception> Complete);
        void GetTienCong(Action<DataTable, Exception> Complete);
        void InsertPhieuSuaChua(Entity.Interfaces.IPhieuSuaChuaInfo item, Action<Interfaces.IResultOk> Complete);
        void GetTTKH(int SelectedPage, int pageSize, string DepId, int Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void GetHoSoSuaChua(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void InsertPhieuThuTien(Entity.Interfaces.IQLGRPhieuThuTienInfo item, Action<Interfaces.IResultOk> Complete);
        void GetThongTinTraCuu(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);
        void GetBaoCaoDoanhSo(int SelectedPage, int pageSize, string DepId, string Month, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete);



    }
    public class clsQLGRKhachHang : IKhachHang
    {
        public void GetThongTinKhachHang(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_KhachHang", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SelectedPage", SelectedPage);
                    _cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@Keysearch", Keysearch);

                    _cmd.Parameters.Add("@TotalPages", SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@TotalRecords", SqlDbType.Int);
                    _cmd.Parameters["@TotalRecords"].Direction = ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);

                    if (_cmd.Parameters["@TotalPages"].Value == null)
                        totalPages = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalPages"].Value.ToString(), out totalPages);
                    if (_cmd.Parameters["@TotalRecords"].Value == null)
                        totalRecords = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalRecords"].Value.ToString(), out totalRecords);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void Update(Entity.Interfaces.IQLGRKhachHangInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_KhachHangXuLy", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "PUT");
                    _cmd.Parameters.AddWithValue("@KhachHangId", item.KhachHangId);
                    _cmd.Parameters.AddWithValue("@TenKhachHang", item.TenKhachHang);
                    _cmd.Parameters.AddWithValue("@SoDienThoai", item.SoDienThoai);
                    _cmd.Parameters.AddWithValue("@DiaChi", item.DiaChi);
                    _cmd.Parameters.AddWithValue("@Email", item.Email);
                    _cmd.Parameters.AddWithValue("@TienNo", item.TienNo);

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void Insert(Entity.Interfaces.IQLGRKhachHangInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_KhachHangXuLy", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@KhachHangId", 0);
                    _cmd.Parameters.AddWithValue("@TenKhachHang", item.TenKhachHang);
                    _cmd.Parameters.AddWithValue("@SoDienThoai", item.SoDienThoai);
                    _cmd.Parameters.AddWithValue("@DiaChi", item.DiaChi);
                    _cmd.Parameters.AddWithValue("@Email", item.Email);
                    _cmd.Parameters.AddWithValue("@TienNo", item.TienNo);

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_KhachHangXuLy", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@KhachHangId", Id);
                    _cmd.Parameters.AddWithValue("@TenKhachHang", "");
                    _cmd.Parameters.AddWithValue("@SoDienThoai", "");
                    _cmd.Parameters.AddWithValue("@DiaChi", "");
                    _cmd.Parameters.AddWithValue("@Email", "");
                    _cmd.Parameters.AddWithValue("@TienNo", 0);
                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }

            if (Complete != null) Complete(_result);
        }
        public void InsertHoSoXe(Entity.Interfaces.IQLGRHoSoXeInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_SaveHoSoXe", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@BienSo", item.BienSo);
                    _cmd.Parameters.AddWithValue("@MaHieuXe", item.MaHieuXe);
                    _cmd.Parameters.AddWithValue("@KhachHangId", item.KhachHangId);
                    _cmd.Parameters.AddWithValue("@NgayTiepNhan", item.NgayTiepNhan);
                    _cmd.Parameters.AddWithValue("@TienDo", "Mới tiếp nhận");

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void GetHoSoXe(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_HoSoXe", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@SelectedPage", SelectedPage);
                    _cmd.Parameters.AddWithValue("@PageSize", pageSize);
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@Keysearch", Keysearch);

                    _cmd.Parameters.Add("@TotalPages", SqlDbType.Int);
                    _cmd.Parameters["@TotalPages"].Direction = ParameterDirection.Output;
                    _cmd.Parameters.Add("@TotalRecords", SqlDbType.Int);
                    _cmd.Parameters["@TotalRecords"].Direction = ParameterDirection.Output;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);

                    if (_cmd.Parameters["@TotalPages"].Value == null)
                        totalPages = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalPages"].Value.ToString(), out totalPages);
                    if (_cmd.Parameters["@TotalRecords"].Value == null)
                        totalRecords = 0;
                    else
                        int.TryParse(_cmd.Parameters["@TotalRecords"].Value.ToString(), out totalRecords);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void GetDetailPSC(int SelectedPage, int pageSize, string DepId, int Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_GetPhieuSuaChua", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Keysearch", Keysearch);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void GetHieuXe(Action<DataTable, Exception> Complete)
        {
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_HieuXe", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void InsertPhieuSuaChua(Entity.Interfaces.IPhieuSuaChuaInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_PhieuSuaChua", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@HoSoId", item.HoSoId);
                    _cmd.Parameters.AddWithValue("@NgayLapPhieu", item.NgaySuaChua);
                    _cmd.Parameters.AddWithValue("@PhuTungId", item.PhuTungId);
                    _cmd.Parameters.AddWithValue("@TienCongId", item.TienCongId);
                    _cmd.Parameters.AddWithValue("@SoLuong", item.SoLuong);
                    _cmd.Parameters.AddWithValue("@NoiDung", item.NoiDung);
                    _cmd.Parameters.AddWithValue("@TinhTrang", "Đã lập phiếu sữa chữa");

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void GetTTKH(int SelectedPage, int pageSize, string DepId, int Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_GetPhieuThuTien", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Keysearch", Keysearch);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void GetHoSoSuaChua(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_HoSoSuaChua", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void GetPhuTung(Action<DataTable, Exception> Complete)
        {
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_PhuTung", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void GetTienCong(Action<DataTable, Exception> Complete)
        {
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_TienCong", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);
                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }
        public void InsertPhieuThuTien(Entity.Interfaces.IQLGRPhieuThuTienInfo item, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_PhieuThuTien", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "POST");
                    _cmd.Parameters.AddWithValue("@HoSoId", item.HoSoId);
                    _cmd.Parameters.AddWithValue("@NgayThuTien", item.NgayThuTien);
                    _cmd.Parameters.AddWithValue("@SoTienThu", item.SoTienThu);
                    _cmd.Parameters.AddWithValue("@TongTien", item.TongTien);


                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }
            if (Complete != null) Complete(_result);
        }
        public void DeletePSC(object Id, Action<Interfaces.IResultOk> Complete)
        {
            BaseResultOk _result = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_PhieuSuaChua", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", "DELETE");
                    _cmd.Parameters.AddWithValue("@HoSoId", "");
                    _cmd.Parameters.AddWithValue("@NgayLapPhieu", "");
                    _cmd.Parameters.AddWithValue("@PhuTungId", Id);
                    _cmd.Parameters.AddWithValue("@TienCongId", "");
                    _cmd.Parameters.AddWithValue("@SoLuong", "");
                    _cmd.Parameters.AddWithValue("@NoiDung", "");
                    _cmd.Parameters.AddWithValue("@TinhTrang", "");

                    _result.RowsAffected = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _result.Errors.Add("ex", ex.Message);
            }

            if (Complete != null) Complete(_result);
        }
        public void GetThongTinTraCuu(int SelectedPage, int pageSize, string DepId, string Keysearch, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_TraCuuThongTin", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Keysearch", Keysearch);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);

                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }

        public void GetBaoCaoDoanhSo(int SelectedPage, int pageSize, string DepId, string Month, out int totalPages, out int totalRecords, Action<DataTable, Exception> Complete)
        {
            totalPages = 0; totalRecords = 0;
            DataTable tbl = new DataTable();

            Exception _ex = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_QLGR_BaoCaoDoanhSo", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Month", Convert.ToInt32(Month));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    _adapter.Fill(tbl);

                }
            }
            catch (Exception ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(tbl, _ex);
        }


    }
}
