﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsTimeSheets : Interfaces.ITimeSheets
    {

        public void UpdateTimeSheet(Entity.Interfaces.ITimeInOutSessionInfo Item, Action<Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk ok = new BaseResultOk();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMESHEETS_REAL_UPDATE", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@WorkingDay", Item.Workingday);
                    _cmd.Parameters.AddWithValue("@MaNV", Item.MaNV);
                    _cmd.Parameters.AddWithValue("@DepId", Item.DepID);
                    _cmd.Parameters.AddWithValue("@TimeIn", Item.TimeIn == null ? "" : Item.TimeIn);
                    _cmd.Parameters.AddWithValue("@BreakOut", Item.BreakOut == null ? "" : Item.BreakOut);
                    _cmd.Parameters.AddWithValue("@TimeOut", Item.TimeOut == null ? "" : Item.TimeOut);
                    _cmd.Parameters.AddWithValue("@BreakIn", Item.BreakIn == null ? "" : Item.BreakIn);
                    _cmd.Parameters.AddWithValue("@TotalTimeHM", Item.TotalTimeHM == null ? "" : Item.TotalTimeHM);
                    _cmd.Parameters.AddWithValue("@TotalHours", Item.TotalHours);
                    _cmd.Parameters.AddWithValue("@LyDoVang", Item.LyDoVang);
                    if (!string.IsNullOrEmpty(Item.LyDoVang))
                    {

                    }
                    _cmd.Parameters.AddWithValue("@OTX", Item.OTX);
                    _cmd.Parameters.AddWithValue("@CongX", Item.CongK);

                    ok.RowsAffected = _cmd.ExecuteNonQuery();


                }
            }
            catch (Exception ex)
            {
                ok.Errors.Add("UpdateSession", ex.Message);
            }
            if (Complete != null) Complete(ok);
        }

        public void ReportMonthly(int year, int month, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeSheetInfo>, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            IEnumerable<Entity.Interfaces.ITimeSheetInfo> Items = new List<Entity.Interfaces.ITimeSheetInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("dbo.sp_MCC_TIMESHEETS_REAL_BC_THANG", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    if (year > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Year", year);
                    }
                    if (month > 0)
                    {
                        _cmd.Parameters.AddWithValue("@Month", month);
                    }
                    _cmd.Parameters.AddWithValue("@DepID", DepId);
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsTimeSheetInfo>(tbl);
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("ReportMonthly", ex.Message);
            }
            if (Complete != null) Complete(Items, error);
        }
    }
}
