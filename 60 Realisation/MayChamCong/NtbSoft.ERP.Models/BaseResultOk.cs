﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class BaseResultOk:Interfaces.IResultOk
    {
        public BaseResultOk() { Errors = new Dictionary<string, string>(); }

        public Dictionary<string, string> Errors { get; set; }

        public string Message
        {
            get
            {
                if (Errors == null || Errors.Count == 0) return "";

                return string.Join(Environment.NewLine, Errors.Select(x => x.Value).ToArray());
            }
        }

        /// <summary>
        /// Get or set 
        /// </summary>
        public int RowsAffected { get; set; }

        /// <summary>
        /// True: Lưu thành công, False: Lỗi không lưu đc
        /// </summary>
        public bool Success
        {
            get { if (this.Errors == null || this.Errors.Count == 0) return true; return false; }
        }
    }
}
