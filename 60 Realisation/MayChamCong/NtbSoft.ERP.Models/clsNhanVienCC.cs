﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsNhanVienCC:Interfaces.INhanVien
    {
        public void GeBy(int Id, Action<Entity.Interfaces.INhanVienInfo, Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void GeBy(string MaNV, Action<Entity.Interfaces.INhanVienInfo, Interfaces.IResultOk> Complete)
        {
            Interfaces.IResultOk error = new BaseResultOk();
            Entity.clsNhanVienInfo Item = new Entity.clsNhanVienInfo();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_BY_MANV_MACC", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@MaNV", MaNV);
                    _cmd.Parameters.AddWithValue("@MaCC", -1);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);
                        Item = Items[0];
                    }
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GeByMaCC", ex.Message);
            }
            if (Complete != null) Complete(Item, error);
        }

        public void GeByMaCC(string MaCC, Action<Entity.Interfaces.INhanVienInfo, Interfaces.IResultOk> Complete)
        {
            //
            Interfaces.IResultOk error = new BaseResultOk();
            Entity.clsNhanVienInfo Item = new Entity.clsNhanVienInfo();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_NHANVIEN_BY_MANV_MACC", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@MaNV", "");
                    _cmd.Parameters.AddWithValue("@MaCC", MaCC);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        var Items = Libs.ConvertTableToList.ConvertToList<Entity.clsNhanVienInfo>(tbl);
                        Item = Items[0];
                    }
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GeByMaCC", ex.Message);
            }
            if (Complete != null) Complete(Item, error);
        }

        public void GetVaoRaByDate(string maNv, string date, Action<float, Interfaces.IResultOk> Complete)
        {
            //
            Interfaces.IResultOk error = new BaseResultOk();
            float soGio = 0;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    #region COMMAND TEXT LẤY THỜI GIAN VÀO RA
                    string cmdText = string.Format(@"DECLARE	@NgayDangKy	VARCHAR(10) = '{1}',
		                                                        @MaNV NVARCHAR(50) = '{0}'
	
                                                    -- nếu giờ ra và giờ vào trong khoảng nghỉ thì không tính	
                                                    SELECT	rv.MaNV,
                                                            rv.NgayDangKy,
		                                                    ROUND(SUM(CONVERT(INT, rv.ThoiGian)) / 60.0, 1)   ThoiGian  
                                                    FROM	dbo.DIC_DANGKY_RAVAO  rv
		                                                    LEFT JOIN (
			                                                    SELECT	rv.MaNV,
					                                                    rv.NgayDangKy,
					                                                    rv.GioVao,
					                                                    rv.GioRa,
					                                                    rv.ThoiGian,
					                                                    s.TimeIn,
					                                                    s.BreakOut,
					                                                    s.BreakIn,
					                                                    s.TimeOut
			                                                    FROM	dbo.DIC_DANGKY_RAVAO rv
					                                                    LEFT JOIN DIC_DANGKY_CHEDO  cd
						                                                    ON	cd.MaNV = rv.MaNV
						                                                    AND @NgayDangKy BETWEEN TuNgay and ToiNgay
					                                                    LEFT JOIN DIC_SHIFT s
						                                                    ON	s.ShiftId = cd.ShiftID
			                                                    WHERE	rv.MaNV = @MaNV
			                                                    AND		NgayDangKy = @NgayDangKy
			                                                    AND		rv.GioRa BETWEEN s.BreakOut AND s.BreakIn
			                                                    AND		rv.GioVao BETWEEN s.BreakOut AND s.BreakIn
		                                                    ) tr
			                                                    ON	tr.MaNV = rv.MaNV
			                                                    AND tr.GioRa = rv.GioRa
			                                                    AND tr.GioVao = rv.GioVao
			                                                    AND tr.NgayDangKy = rv.NgayDangKy
                                                    WHERE	@NgayDangKy = rv.NgayDangKy
                                                    AND		@MaNV = rv.MaNV
                                                    AND		tr.MaNV IS NULL
                                                    GROUP	BY rv.MaNV,
                                                            rv.NgayDangKy
                                                     ", maNv, date);
                    #endregion

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand(cmdText, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        soGio = Convert.ToSingle(tbl.Rows[0]["ThoiGian"]);
                    }
                }
            }
            catch (Exception ex)
            {
                error.Errors.Add("GeByMaCC", ex.Message);
            }
            if (Complete != null) Complete(soGio, error);
        }
    }
}
