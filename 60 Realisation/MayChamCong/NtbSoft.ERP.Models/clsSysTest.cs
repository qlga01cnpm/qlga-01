﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Dapper;
using Dev.Sdk.Generic;
using Dev.Sdk.Interfaces.Repositories;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace NtbSoft.ERP.Models
{
    public class clsSysTest : clsBaseBus<Entity.clsSysTestInfo>
        
    {
        private const string strSqlGetAll =
       @"select * 
            from dbo.SYS_TEST";

        public override IResult BatchDelete(IEnumerable<Entity.clsSysTestInfo> items)
        {
            return base.BatchDelete(items);
        }
        public override IEnumerable<Entity.clsSysTestInfo> GetAll()
        {
            var result = Connection.Query<Entity.clsSysTestInfo>(strSqlGetAll);


            return result;
        }
        protected override IResult ValidateInsert(Entity.clsSysTestInfo item)
        {
            var result = new BaseResult();
            var Id = Get(item.Id);
            if (Id != null)
                result.Errors.Add(Guid.NewGuid().ToString(), "Id đã tồn tại");

            return result;
            //return base.ValidateInsert(item);
        }

        
    }
}
