using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using NtbSoft.ERP.Entity;

namespace NtbSoft.ERP.Models
{
    public class clsGroupUserBS:MarshalByRefObject
    {        
        /// <summary>
        /// GetListGroupIdByUserAndLanguageID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<string> GetListGroupIdByUserID(string sLoginID)
        {
            //sLoginID = sLoginID.ToLower();
            List<string> lstGroupID = new List<string>();

            try
            {
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    SqlCommand _cmd = new SqlCommand("sp_SYS_GroupUser_SelectByLoginID", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@LoginID", sLoginID);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            lstGroupID.Add(reader["GroupID"].ToString());
                        }

                        //Call Close when done reading.
                        //reader.Close();
                        //reader.Dispose();
                    }
                    return lstGroupID;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsGroupUserBS. " + ex.Message);
            }
        }

        /// <summary>
        /// Get Group Collection By User ID
        /// </summary>
        /// <param name="sUserName">User Name</param>
        /// <returns>Group Collection</returns>
        public clsGroupCollection GetGroupCollectionByUserID(string sLoginID)
        {
            clsGroupCollection groupCollection = new clsGroupCollection();
            SqlDataReader reader = null;
            //SqlConnection _cnn = null;
            try
            {
                //sLoginID = sLoginID.ToLower();
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    SqlCommand _cmd = new SqlCommand("sp_SYS_GroupUser_SelectByLoginID", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@LoginID", sLoginID);
                    reader = _cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        clsGroupInfo objGroup = new clsGroupInfo();

                        objGroup.GroupID = reader["GroupID"].ToString();
                        objGroup.GroupName = reader["GroupName"].ToString();
                        objGroup.Descriptions = reader["Descriptions"].ToString();
                        objGroup.IsAdmin = (bool)reader["IsAdmin"];

                        groupCollection.Add(objGroup);
                    }

                    //Call Close when done reading.
                    reader.Close();
                }
            }
            catch { }
            finally
            {
                if (reader != null) { reader.Close(); reader.Dispose(); }
            }
            return groupCollection;
        }

    }
}
