﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsWebServerCustomer:NtbSoft.ERP.Entity.Interfaces.IWebServerCustomer
    {
        private Entity.Interfaces.IWebCustomerInfo _webCustomerInfo;
        public clsWebServerCustomer(Entity.Interfaces.IWebCustomerInfo pWebCustomerInfo)
        {
            this._webCustomerInfo=pWebCustomerInfo;
        }

        public List<Entity.Interfaces.IWebServerCustomerInfo> GetByCustumer()
        {
            List<Entity.Interfaces.IWebServerCustomerInfo> Items =
                new List<Entity.Interfaces.IWebServerCustomerInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.WEB_SERVER_CUSTOMER");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("where CusID='{0}'", this._webCustomerInfo.CusID));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        foreach (System.Data.DataRow row in tbl.Rows)
                        {
                            Entity.Interfaces.IWebServerCustomerInfo Info = new Entity.clsWebServerCustomerInfo();
                            Info.ServerID = row["ServerID"].ToString();
                            Info.Name = row["Name"].ToString();
                            Info.MChine = row["MChine"].ToString();
                            Info.IPLan = row["IPLan"].ToString();
                            Info.IPWan = row["IPWan"].ToString();
                            Info.Mac = row["Mac"].ToString();
                            Info.Port = Convert.ToInt32(row["Port"]);
                            Info.Url = Convert.ToString(row["Url"]);
                            Info.PageName = row["PageName"].ToString();

                            Info.TypeService = row["TypeService"].ToString();
                            Info.WebCustomerInfo = _webCustomerInfo;
                            Items.Add(Info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return Items;
        }

        public IEnumerable<Entity.Interfaces.IWebServerCustomerInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Save(List<Entity.Interfaces.IWebServerCustomerInfo> Items)
        {
            if (Items == null) throw new ArgumentNullException("Items");

            return true;
        }

        public bool Delete(string Id)
        {
            throw new NotImplementedException();
        }


       
    }
}
