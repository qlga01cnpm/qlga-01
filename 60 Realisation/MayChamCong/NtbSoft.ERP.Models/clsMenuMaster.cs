﻿using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    public class clsMenuMaster
    {
        public System.Data.DataTable TYPE_SYS_USER_MENU
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_SYS_USER_MENU");
                _tbl.Columns.Add("UserID", typeof(System.String));
                _tbl.Columns.Add("MenuID", typeof(System.String));

                _tbl.Columns.Add("AllowView", typeof(System.Boolean));
                _tbl.Columns.Add("AllowAdd", typeof(System.Boolean));
                _tbl.Columns.Add("AllowEdit", typeof(System.Boolean));
                _tbl.Columns.Add("AlowDelete", typeof(System.Boolean));

                _tbl.PrimaryKey = new System.Data.DataColumn[] { _tbl.Columns["UserID"], _tbl.Columns["MenuID"] };
                return _tbl;
            }
        }

        public System.Data.DataTable TYPE_SYS_USER_GROUP
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_SYS_USER_GROUP");
                _tbl.Columns.Add("UserID", typeof(System.String));
                _tbl.Columns.Add("GroupID", typeof(System.String));

                _tbl.Columns.Add("IsLeader", typeof(System.Boolean));
                _tbl.Columns.Add("IsView", typeof(System.Boolean));
                _tbl.Columns.Add("IsEdit", typeof(System.Boolean));

                _tbl.PrimaryKey = new System.Data.DataColumn[] { _tbl.Columns["UserID"], _tbl.Columns["GroupID"] };
                return _tbl;
            }
        }

        public Entity.clsMenuMasterCollection GetAll()
        {
            System.Data.SqlClient.SqlDataReader _reader = null;
            //System.Data.SqlClient.SqlConnection _cnn = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsMenuMasterCollection _objCollection = new Entity.clsMenuMasterCollection();
                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[3];

                    sp[0] = new System.Data.SqlClient.SqlParameter("@Columns", "*");
                    sp[1] = new System.Data.SqlClient.SqlParameter("@TableName", "dbo.SYS_MENU_MASTER");
                    sp[2] = new System.Data.SqlClient.SqlParameter("@WhereClause", "Where IsVisible=0 order by Sort asc ");
                    _reader = Libs.SqlHelper.ExecuteReader(_cnn, System.Data.CommandType.StoredProcedure, "sp_Select", sp);
                    while (_reader.Read())
                    {
                        if (!_reader.HasRows)
                            break;
                        Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo();
                        _item.id = _reader["MenuId"].ToString().Trim();
                        _item.MenuParent = _reader["MenuParent"].ToString();
                        _item.text = _reader["MenuName"].ToString().Trim();
                        _item.url = _reader["Url"].ToString();
                        //if (!string.IsNullOrWhiteSpace(_item.url))
                        //    _item.url = string.Format("{0}?id={1}", _item.url, _item.id);
                        _item.PageName = _reader["PageName"].ToString().Trim();
                        _item.Sort = (int)_reader["Sort"];
                        _item.IsVisible = (bool)_reader["IsVisible"];
                        _item.iconCls = _reader["iconCls"].ToString().Trim();
                        _item.ContextMenuId = _reader["ContextMenuId"].ToString();
                        _item.ContextMenuIdFile = _reader["ContextMenuIdFile"].ToString();
                        _item.DefaultUrl = _reader["DefaultUrl"].ToString();

                        _item.MoTa = _reader["MoTa"].ToString();

                        _item.ChuoiBatDau = _reader["ChuoiBatDau"].ToString();
                        _item.KyTuCach = _reader["KyTuCach"].ToString();
                        _item.SoChuSo = (int)_reader["SoChuSo"];
                        _item.SoBatDau = (int)_reader["SoBatDau"];
                        _item.SoKetThuc = (int)_reader["SoKetThuc"];
                        _item.MenuType = _reader["MenuType"].ToString();
                        _objCollection.Add(_item);
                    }

                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetAll: " + ex.Message);
            }
            finally
            {
                if (_reader != null) { _reader.Close(); _reader.Dispose(); }
               // if (_cnn != null) { _cnn.Close(); _cnn.Dispose(); }
            }
        }

        public Entity.clsTreeDataCollection GetTreeData(string MenuId,bool HasLastNode)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsTreeDataCollection _objCollection = new Entity.clsTreeDataCollection();
                    SqlCommand _cmd = new SqlCommand("sp_SYS_MENU_GET_NODE_BY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.GET.ToString());
                    _cmd.Parameters.AddWithValue("@Id", MenuId);
                    _cmd.Parameters.AddWithValue("@HasLastNode", HasLastNode);
                    SqlDataAdapter _adapter = new SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    System.Data.DataRow[] Rows = tbl.Select("Id='" + MenuId + "'");
                    if (Rows == null || Rows.Length == 0) return _objCollection;
                    Entity.clsTreeDataInfo newItem = new Entity.clsTreeDataInfo(Rows[0]);
                    addChildNodeTreeData(ref newItem, tbl);
                    _objCollection.Add(newItem);

                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetBy: " + ex.Message);
            }
        }

        private void addChildNodeTreeData(ref NtbSoft.ERP.Entity.clsTreeDataInfo node, System.Data.DataTable tbl)
        {
            foreach (System.Data.DataRow row in tbl.Rows)
            {
                if (node.id.Equals(row["ParentId"].ToString()))
                {
                    Entity.clsTreeDataInfo tn = new Entity.clsTreeDataInfo();
                    tn.id = row["Id"].ToString().ToUpper();
                    tn.Name = row["Name"].ToString();
                    tn.parentId = row["ParentId"].ToString();
                    tn.text = row["Name"].ToString();
                    tn.url = row.Field<string>("url");
                    tn.Level = row.Field<int>("Node");
                    if (node.children == null)
                        node.children = new List<Entity.clsTreeDataInfo>();
                    node.children.Add(tn);

                    //Goi de quy
                    addChildNodeTreeData(ref tn, tbl);
                }
            }
        }

        public Entity.clsMenuMasterCollection GetByUsers(string UserId)
        {
            //GetByUserId(UserId);
            System.Data.SqlClient.SqlDataReader _reader = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsMenuMasterCollection _objCollection = new Entity.clsMenuMasterCollection();
                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[2];

                    //string _tblJoin = "dbo.SYS_MENU_MASTER mn inner join dbo.SYS_USER_MENU t1 on mn.MenuId=t1.MenuID";

                    //sp[0] = new System.Data.SqlClient.SqlParameter("@Columns", "mn.*");
                    //sp[1] = new System.Data.SqlClient.SqlParameter("@TableName", _tblJoin);
                    //sp[2] = new System.Data.SqlClient.SqlParameter("@WhereClause", string.Format("Where t1.UserID='{0}' and mn.IsVisible=0 order by Sort asc ", UserId));


                    sp[0] = new System.Data.SqlClient.SqlParameter("@Action", Libs.ApiAction.GET.ToString());
                    sp[1] = new System.Data.SqlClient.SqlParameter("@UserId", UserId);

                    _reader = Libs.SqlHelper.ExecuteReader(_cnn, System.Data.CommandType.StoredProcedure, "sp_SYS_MENU_GET_BY_USER", sp);
                    while (_reader.Read())
                    {
                        if (!_reader.HasRows)
                            break;
                        Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo();
                        _item.id = _reader["MenuId"].ToString().Trim();
                        _item.MenuParent = _reader["MenuParent"].ToString();
                        _item.text = _reader["MenuName"].ToString().Trim();
                        _item.url = _reader["Url"].ToString();
                        //if (!string.IsNullOrWhiteSpace(_item.url))
                        //    _item.url = string.Format("{0}?id={1}", _item.url, _item.id);

                        _item.PageName = _reader["PageName"].ToString().Trim();
                        _item.Sort = (int)_reader["Sort"];
                        _item.IsVisible = (bool)_reader["IsVisible"];
                        _item.iconCls = _reader["iconCls"].ToString().Trim();
                        _item.ContextMenuId = _reader["ContextMenuId"].ToString();
                        _item.ContextMenuIdFile = _reader["ContextMenuIdFile"].ToString();
                        _item.DefaultUrl = _reader["DefaultUrl"].ToString();

                        _item.MoTa = _reader["MoTa"].ToString();

                        _item.ChuoiBatDau = _reader["ChuoiBatDau"].ToString();
                        _item.KyTuCach = _reader["KyTuCach"].ToString();
                        _item.SoChuSo = (int)_reader["SoChuSo"];
                        _item.SoBatDau = (int)_reader["SoBatDau"];
                        _item.SoKetThuc = (int)_reader["SoKetThuc"];
                        _item.MenuType = _reader["MenuType"].ToString();
                        _item.IsFunc = Convert.ToBoolean(_reader["IsFunc"]);
                        _objCollection.Add(_item);
                    }

                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetAll: " + ex.Message);
            }
            finally
            {
                if (_reader != null) { _reader.Close(); _reader.Dispose(); }
                // if (_cnn != null) { _cnn.Close(); _cnn.Dispose(); }
            }
        }


        /// <summary>
        /// Lay menu + quyền truy cập
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public Entity.clsMenuMasterCollection GetByUserId(string UserId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsMenuMasterCollection _objCollection = new Entity.clsMenuMasterCollection();
                    SqlCommand _cmd = new SqlCommand("sp_SYS_MENU_GET_BY_USER", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.GET.ToString());
                    _cmd.Parameters.AddWithValue("@UserId", @UserId);
                    SqlDataAdapter _adapter = new SqlDataAdapter(_cmd);
                    System.Data.DataSet ds = new System.Data.DataSet();
                    _adapter.Fill(ds);
                    System.Data.DataRow[] Rows = ds.Tables[0].Select("MenuParent is null");
                    System.Data.DataRow[] ChildRows = ds.Tables[0].Select("MenuParent is not null");
                    foreach (System.Data.DataRow row in Rows)
                    {
                        System.Data.DataRow[] rowPermmion = ds.Tables[1].Select("MenuID='" + row["MenuId"].ToString() + "'");
                        if (rowPermmion == null || rowPermmion.Length == 0) continue;
                        var parentAllow = false;
                        if (Convert.ToBoolean(rowPermmion[0]["AllowView"]) || Convert.ToBoolean(rowPermmion[0]["AllowAdd"]) ||
                            Convert.ToBoolean(rowPermmion[0]["AllowEdit"]) || Convert.ToBoolean(rowPermmion[0]["AlowDelete"]))
                            parentAllow = true;

                        
                        if (parentAllow || checkPermisionChildNode(row["MenuId"].ToString(),ds.Tables[1], ChildRows))
                        {
                            Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo(row);
                            _objCollection.Add(_item);
                            addChildNode(parentAllow,ds.Tables[1], _item.id, ChildRows, ref _objCollection);
                        }
                        
                        //
                        //Nếu Check MenuParent và kiểm tra có menu con thì thêm
                        //if (parentAllow || checkPermisionChildNode(row["MenuId"].ToString(), ChildRows))
                        //{
                        //    Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo(row);
                        //    _objCollection.Add(_item);
                        //    addChildNode(parentAllow,_item.id, ChildRows, ref _objCollection);
                        //}

                    }

                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetAll: " + ex.Message);
            }
            finally
            {
            }
        }
        private void addChildNode(bool ParentAllow, System.Data.DataTable tbl_MenuPermmision, string MenuId, System.Data.DataRow[] ChildRows, ref Entity.clsMenuMasterCollection Menus)
        {
            List<System.Data.DataRow> Rows = ChildRows.Where(w => w["MenuParent"].ToString() == MenuId).ToList();
            if (Rows == null || Rows.Count == 0) return;
            foreach (System.Data.DataRow row in Rows)
            {
                bool hasChild = checkPermisionChildNode(row["MenuId"].ToString(), tbl_MenuPermmision, ChildRows);
                var nodeAllow = false;

                System.Data.DataRow[] rowPermmion = tbl_MenuPermmision.Select("MenuID='" + row["MenuId"].ToString() + "'");
                if (rowPermmion != null && rowPermmion.Length > 0 && ParentAllow)
                    if (Convert.ToBoolean(rowPermmion[0]["AllowView"]) || Convert.ToBoolean(rowPermmion[0]["AllowAdd"]) ||
                        Convert.ToBoolean(rowPermmion[0]["AllowEdit"]) || Convert.ToBoolean(rowPermmion[0]["AlowDelete"]))
                    nodeAllow = true;

                //if (ParentAllow) {nodeAllow = true;}
               // if (!ParentAllow && nodeAllow) ParentAllow = true;

                if (row["MenuId"].ToString() == "05.22.00")
                {

                }
                if (nodeAllow || hasChild)
                {
                     Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo(row);
                     if (!Menus.Exists(b => b.id == _item.id))
                     {
                         Menus.Add(_item);
                         addChildNode(nodeAllow, tbl_MenuPermmision, _item.id, ChildRows, ref Menus);
                     }
                }
                else if(!hasChild)
                {//Trường hợp không có Child Node thì add node cuối
                    //var fRows = ChildRows.Where(w => w["MenuId"].ToString() == row["MenuParent"].ToString()).ToList();
                    //if (fRows != null && fRows.Count>0)
                    //{
                    //    if ((!fRows[0].IsNull("AllowView") && Convert.ToBoolean(fRows[0]["AllowView"])))
                    //    {
                    //        Entity.clsMenuMasterInfo _item = new Entity.clsMenuMasterInfo(row);
                    //        if (!Menus.Exists(b => b.id == _item.id))
                    //            Menus.Add(_item);
                    //        //Menus.Add(_item);
                    //    }
                    //}
                }
            }
        }

        /// <summary>
        /// Kiểm tra "MenuId" có menu con được check "AllowView","AllowAdd","AllowEdit","AlowDelete"
        /// True: có, False: không
        /// </summary>
        /// <param name="ItemTreeMenu"></param>
        /// <returns></returns>
        private bool checkPermisionChildNode(string MenuId,System.Data.DataTable tbl_MenuPermission, System.Data.DataRow[] ChildRows)
        {
            if (ChildRows == null) return false;
            List<System.Data.DataRow> Rows = ChildRows.Where(w => w["MenuParent"].ToString() == MenuId).ToList();
            if (Rows == null || Rows.Count == 0) return false;
            foreach (var item in Rows)
            {
                System.Data.DataRow[] rowPermmion = tbl_MenuPermission.Select("MenuID='" + item["MenuId"].ToString() + "'");
                if (rowPermmion != null && rowPermmion.Length > 0)
                    if (Convert.ToBoolean(rowPermmion[0]["AllowView"]) || Convert.ToBoolean(rowPermmion[0]["AllowAdd"]) ||
                        Convert.ToBoolean(rowPermmion[0]["AllowEdit"]) || Convert.ToBoolean(rowPermmion[0]["AlowDelete"]))
                        return true;
                //Gọi đệ quy
                if (checkPermisionChildNode(item["MenuId"].ToString(), tbl_MenuPermission, ChildRows))
                    return true;
            }

            return false;
        }

        public System.Data.DataTable GetContextMenu(string MenuId, string ContextMenuId, string UserId, string MainMenuId)
        {
            System.Data.SqlClient.SqlConnection _cnn = null;
            try
            {
                _cnn = SqlHelper.GetConnection();
                using (SqlCommand cmd = new SqlCommand("sp_SYS_CONTEXT_MENU", _cnn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Action", ApiAction.GET.ToString());
                    cmd.Parameters.AddWithValue("@MenuId", MenuId);
                    cmd.Parameters.AddWithValue("@ContextMenuId", ContextMenuId);

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@MainMenuId", MainMenuId);

                    SqlDataAdapter Adapter = new SqlDataAdapter(cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    Adapter.Fill(tbl);

                    return tbl;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetHtmlContextMenu: " + ex.Message);
            }
            finally
            {
                //if (_cnn != null) { _cnn.Close(); _cnn.Dispose(); }
            }
        }

        public bool Rename(string MenuId, string Name)
        {
            //System.Data.SqlClient.SqlConnection sqlcnn = null;
            try
            {

                using (System.Data.SqlClient.SqlConnection sqlcnn = SqlHelper.GetConnection())
                {

                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[3];
                    sp[0] = new System.Data.SqlClient.SqlParameter("@TableName", "dbo.SYS_MENU_MASTER");
                    sp[1] = new System.Data.SqlClient.SqlParameter("@SetValues", string.Format("MenuName=N'{0}'", Name == null ? "" : Name.Trim()));
                    sp[2] = new System.Data.SqlClient.SqlParameter("@WhereClause", string.Format("Where MenuId='{0}'", MenuId));

                    int result = (int)SqlHelper.ExecuteNonQuery(sqlcnn, System.Data.CommandType.StoredProcedure, "sp_Update", sp);

                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsMenuMaster.Rename: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Rename(string MenuId, string Name,string Url)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection sqlcnn = SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[4];
                    sp[0] = new System.Data.SqlClient.SqlParameter("@Action", Libs.ApiAction.PUT.ToString());
                    sp[1] = new System.Data.SqlClient.SqlParameter("@MenuId", MenuId);
                    sp[2] = new System.Data.SqlClient.SqlParameter("@MenuName", Name);
                    sp[3] = new System.Data.SqlClient.SqlParameter("@Url", Url);

                    int result = (int)SqlHelper.ExecuteNonQuery(sqlcnn, System.Data.CommandType.StoredProcedure, "sp_MenuMasterRename", sp);
                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsMenuMaster.Rename: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(ApiAction action, string MenuId, string MenuName, string MenuParent,
            string ContextMenuId, string ContextMenuIdFile, string Url, string DefaultUrl, int Sort, string MenuType, out string NewMenuName)
        {
            //System.Data.SqlClient.SqlConnection _cnn = null;
            NewMenuName = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_MenuMasterUpdate", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", action.ToString());
                    cmd.Parameters.AddWithValue("@MenuId", MenuId);
                    cmd.Parameters.AddWithValue("@MenuName", MenuName == null ? "" : MenuName.Trim());
                    cmd.Parameters.AddWithValue("@MenuParent", MenuParent);
                    cmd.Parameters.AddWithValue("@ContextMenuId", ContextMenuId == null ? "" : ContextMenuId);
                    cmd.Parameters.AddWithValue("@ContextMenuIdFile", ContextMenuIdFile == null ? "" : ContextMenuIdFile);
                    cmd.Parameters.AddWithValue("@Url", Url == null ? "" : Url);
                    cmd.Parameters.AddWithValue("@DefaultUrl", DefaultUrl == null ? "" : DefaultUrl);
                    cmd.Parameters.AddWithValue("@PageName", "");
                    cmd.Parameters.AddWithValue("@Sort", Sort);
                    cmd.Parameters.AddWithValue("@MenuType", MenuType == null ? "" : MenuType);
                    /*** ***/
                    cmd.Parameters.Add("@OutNewName", System.Data.SqlDbType.NVarChar, 150);
                    cmd.Parameters["@OutNewName"].Direction = System.Data.ParameterDirection.Output;

                    int kq = cmd.ExecuteNonQuery();

                    NewMenuName = cmd.Parameters["@OutNewName"].Value.ToString();

                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsMenuMaster.Update: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(string MenuId, string MenuName, string MoTa, string ChuoiBatDau, string KyTuCach,
            int SoChuSo, int SoBatDau, int SoKetThuc,string MenuType)
        {
            //System.Data.SqlClient.SqlConnection _cnn = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_MenuMasterUpdateProperty", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@MenuId", MenuId);
                    cmd.Parameters.AddWithValue("@MenuName", MenuName == null ? "" : MenuName.Trim());
                    cmd.Parameters.AddWithValue("@MoTa", MoTa == null ? "" : MoTa);
                    cmd.Parameters.AddWithValue("@ChuoiBatDau", ChuoiBatDau == null ? "" : ChuoiBatDau);
                    cmd.Parameters.AddWithValue("@KyTuCach", KyTuCach == null ? "" : KyTuCach);
                    cmd.Parameters.AddWithValue("@SoChuSo", SoChuSo);
                    cmd.Parameters.AddWithValue("@SoBatDau", SoBatDau);
                    cmd.Parameters.AddWithValue("@SoKetThuc", SoKetThuc);
                    cmd.Parameters.AddWithValue("@MenuType", MenuType == null ? "" : MenuType);

                    int kq = cmd.ExecuteNonQuery();

                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsMenuMaster.Update: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsTreeMenuCollection GetTreeNodes(string UserId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsTreeMenuCollection _objCollection = new Entity.clsTreeMenuCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_PERMISSION_MENU_GET_BY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", ApiAction.GET.ToString());
                    _cmd.Parameters.AddWithValue("@UserId", UserId);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        System.Data.DataRow[] Parents = tbl.Select("[MenuParent] is null");

                        foreach (System.Data.DataRow row in Parents)
                        {
                            if (!Convert.ToBoolean(row["IsFunc"])) continue;
                            Entity.clsTreeMenuInfo treeNode = new Entity.clsTreeMenuInfo();
                            treeNode.id = row["MenuId"].ToString().ToUpper();
                            treeNode.Name = row["MenuName"].ToString();
                            treeNode.parentId = row["MenuParent"].ToString();
                            treeNode.text = row["MenuName"].ToString();
                            treeNode.url = row["Url"].ToString();

                            treeNode.AllowView = Convert.ToBoolean(row["AllowView"]);
                            treeNode.AllowAdd = Convert.ToBoolean(row["AllowAdd"]);
                            treeNode.AllowEdit = Convert.ToBoolean(row["AllowEdit"]);
                            treeNode.AlowDelete = Convert.ToBoolean(row["AlowDelete"]);

                           
                            //treeNode.ContextMenuId = row["ContextMenuId"].ToString();
                            //treeNode.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();
                            addChildNode(ref treeNode, tbl);

                          
                            _objCollection.Add(treeNode);
                        }
                    }

                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetTreeNodes: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsTreeMenuInfo GetPermissionMenu(string UserId, string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsTreeMenuCollection _objCollection = new Entity.clsTreeMenuCollection();
                    //

                    //System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    //_cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //_cmd.Parameters.AddWithValue("@Columns","*");
                    //_cmd.Parameters.AddWithValue("@TableName","dbo.SYS_USER_MENU");
                    //_cmd.Parameters.AddWithValue("@WhereClause", string.Format("where UserID='{0}' and MenuID='{1}'", UserId, MenuId));


                    //sp[0] = new System.Data.SqlClient.SqlParameter("@Columns", "*");
                    //sp[1] = new System.Data.SqlClient.SqlParameter("@TableName", "dbo.SYS_MENU_MASTER");
                    //sp[2] = new System.Data.SqlClient.SqlParameter("@WhereClause", "Where IsVisible=0 order by Sort asc ");

                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_PERMISSION_MENU_BY_USER", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@MenuId", MenuId);
                    _cmd.Parameters.AddWithValue("@UserId", UserId);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        foreach (System.Data.DataRow row in tbl.Rows)
                        {
                            Entity.clsTreeMenuInfo treeNode = new Entity.clsTreeMenuInfo();
                            treeNode.id = row["MenuId"].ToString().ToUpper();

                            treeNode.AllowView = Convert.ToBoolean(row["AllowView"]);
                            treeNode.AllowAdd = Convert.ToBoolean(row["AllowAdd"]);
                            treeNode.AllowEdit = Convert.ToBoolean(row["AllowEdit"]);
                            treeNode.AlowDelete = Convert.ToBoolean(row["AlowDelete"]);

                            return treeNode;

                        }
                    }

                    return null;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsMenuMaster.GetTreeNodes: " + ex.Message);
            }
            finally
            {
            }
        }

        private void addChildNode(ref Entity.clsTreeMenuInfo node, System.Data.DataTable tbl)
        {
            foreach (System.Data.DataRow row in tbl.Rows)
            {
                if (node.id.Equals(row["MenuParent"].ToString()))
                {
                    Entity.clsTreeMenuInfo tn = new Entity.clsTreeMenuInfo();
                    tn.id = row["MenuId"].ToString().ToUpper();
                    tn.Name = row["MenuName"].ToString();
                    tn.parentId = row["MenuParent"].ToString();
                    tn.text = row["MenuName"].ToString();
                    tn.url = row["Url"].ToString();

                    tn.AllowView = Convert.ToBoolean(row["AllowView"]);
                    tn.AllowAdd = Convert.ToBoolean(row["AllowAdd"]);
                    tn.AllowEdit = Convert.ToBoolean(row["AllowEdit"]);
                    tn.AlowDelete = Convert.ToBoolean(row["AlowDelete"]);

                    //tn.ContextMenuId = row["ContextMenuId"].ToString();
                    //tn.ContextMenuIdFile = row["ContextMenuIdFile"].ToString();
                    if (tn.url == "/IED/Library" || tn.url == "/IED/Step" || tn.url == "/IED/Analysis" || !Convert.ToBoolean(row["IsFunc"])) continue;
                    if (node.children == null)
                        node.children = new List<Entity.clsTreeMenuInfo>();
                    node.children.Add(tn);

                    //Goi de quy
                    addChildNode(ref tn, tbl);
                }
            }
        }

        public bool SetPermisionUser(Entity.clsUserInfo UserInfo, System.Data.DataTable TYPE_SYS_USER_MENU)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_PERMISSION_MENU_SET", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", UserInfo.UserID);
                    cmd.Parameters.AddWithValue("@LastName", UserInfo.LastName == null ? "" : UserInfo.LastName.Trim());
                    cmd.Parameters.AddWithValue("@FirstName", UserInfo.FirstName == null ? "" : UserInfo.FirstName);
                    cmd.Parameters.AddWithValue("@IsAdmin", UserInfo.IsAdmin);
                    cmd.Parameters.AddWithValue("@IsNewRow", UserInfo.IsNewRow);
                    cmd.Parameters.AddWithValue("@TYPE_SYS_USER_MENU", TYPE_SYS_USER_MENU);

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsMenuMaster.SetPermisionUser: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool SetPermisionGroupUser(Entity.clsGroupInfo GroupInfo, 
            System.Data.DataTable TYPE_SYS_USER_GROUP,
            System.Data.DataTable TYPE_SYS_USER_MENU)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_PERMISSION_GROUP_USER_MENU", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@GroupID", GroupInfo.GroupID);
                    cmd.Parameters.AddWithValue("@GroupName", GroupInfo.GroupName == null ? "" : GroupInfo.GroupName.Trim());
                    cmd.Parameters.AddWithValue("@Descriptions", GroupInfo.Descriptions == null ? "" : GroupInfo.Descriptions);

                    cmd.Parameters.AddWithValue("@TYPE_SYS_USER_GROUP", TYPE_SYS_USER_GROUP);
                    cmd.Parameters.AddWithValue("@TYPE_SYS_USER_MENU", TYPE_SYS_USER_MENU);

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR.SetPermisionGroupUser: " + ex.Message);
            }
            finally
            {
            }
        }

    }
}

