﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsThaoTacSp
    {
        public clsThaoTacSp() { }

        public System.Data.DataTable TYPE_DIC_THAOTACSANPHAM
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_THAOTACSANPHAM");
                _tbl.Columns.Add("MaThaoTacSP", typeof(System.String));
                _tbl.Columns.Add("MoTa", typeof(System.String));
                _tbl.Columns.Add("ThoiGianThaoTac", typeof(float));
                _tbl.Columns.Add("SoLuong", typeof(int));
                _tbl.Columns.Add("RootId", typeof(System.String));


                _tbl.PrimaryKey = new System.Data.DataColumn[] { _tbl.Columns["MaThaoTacSP"] };
                return _tbl;
            }
        }

       

        /// <summary>
        /// Lấy danh sách bậc thợ theo Id thư mục gốc
        /// </summary>
        /// <param name="RootId"></param>
        /// <returns></returns>
        public Entity.clsThaoTacSpCollection GetBy(string RootId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThaoTacSpCollection _objCollection = new Entity.clsThaoTacSpCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THAOTACSANPHAM");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where RootId='{0}'", RootId));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    List<Entity.clsThaoTacSpInfo> arrayTemp = new List<Entity.clsThaoTacSpInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThaoTacSpInfo()
                        {
                            MaThaoTacSP = m.Field<string>("MaThaoTacSP"),
                            MoTa = m.Field<string>("MoTa"),
                            ThoiGianThaoTac = float.Parse(m["ThoiGianThaoTac"].ToString()),
                            SoLuong = m.Field<int>("SoLuong"),
                            RootId = m.Field<string>("RootId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return new Entity.clsThaoTacSpCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThaoTacSp.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsThaoTacSpInfo GetThaoTac(string MaThaoTacSP)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThaoTacSpCollection _objCollection = new Entity.clsThaoTacSpCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THAOTACSANPHAM");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where MaThaoTacSP='{0}'", MaThaoTacSP));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl == null || tbl.Rows.Count == 0) return new Entity.clsThaoTacSpInfo();

                    List<Entity.clsThaoTacSpInfo> arrayTemp = new List<Entity.clsThaoTacSpInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThaoTacSpInfo()
                        {
                            MaThaoTacSP = m.Field<string>("MaThaoTacSP"),
                            MoTa = m.Field<string>("MoTa"),
                            ThoiGianThaoTac = float.Parse(m["ThoiGianThaoTac"].ToString()),
                            SoLuong = m.Field<int>("SoLuong"),
                            RootId = m.Field<string>("RootId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThaoTacSp.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsThaoTacSpCollection Items)
        {
            try
            {
                System.Data.DataTable _TYPE = this.TYPE_DIC_THAOTACSANPHAM;
                foreach (var item in Items)
                {
                    if (item.MaThaoTacSP.Length > 10)
                        throw new Exception("Mã thao tác vượt quá 10 ký tự");
                    System.Data.DataRow newRow = _TYPE.NewRow();
                    newRow["MaThaoTacSP"] = item.MaThaoTacSP;
                    newRow["MoTa"] = item.MoTa == null ? "" : item.MoTa;
                    newRow["ThoiGianThaoTac"] = item.ThoiGianThaoTac;
                    newRow["SoLuong"] = item.SoLuong;
                    newRow["RootId"] = item.RootId;
                    _TYPE.Rows.Add(newRow);
                }

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_THAOTACSANPHAM", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.POST.ToString());
                    cmd.Parameters.AddWithValue("@TYPE", _TYPE);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsThaoTacSp.Update: " + ex.Message);
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                DataTable _Type = this.TYPE_DIC_THAOTACSANPHAM;
                DataRow newRow = _Type.NewRow();
                newRow["MaThaoTacSP"] = Id;
                newRow["MoTa"] = "";
                newRow["ThoiGianThaoTac"] = 0;
                newRow["SoLuong"] = 0;
                newRow["RootId"] = "00";
                _Type.Rows.Add(newRow);

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_THAOTACSANPHAM", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.DELETE.ToString());
                    cmd.Parameters.AddWithValue("@TYPE", _Type);
                    //cmd.Parameters.Add("", SqlDbType.Image)

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsThaoTacSp.Delete: " + ex.Message);
            }
        }

    }
}
