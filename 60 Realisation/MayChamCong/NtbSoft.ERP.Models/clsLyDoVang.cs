﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{

    public class clsLyDoVang :Interfaces.IBase<Entity.Interfaces.ILyDoVangInfo>
        //: BaseBus<Entity.Interfaces.ILyDoVangInfo>
    {
       
        //public override void GetAll(Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Exception> Complete)
        //{
        //    Exception _ex = null;
        //    IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items = null;
        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Columns", "*");
        //            _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_LYDOVANG");
        //            _cmd.Parameters.AddWithValue("@WhereClause", string.Format(""));

        //            System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
        //            System.Data.DataTable tbl = new System.Data.DataTable();
        //            _adapter.Fill(tbl);
        //            if (tbl != null && tbl.Rows.Count > 0)
        //                Items = Libs.ConvertTableToList.ConvertToList<Entity.clsLyDoVangInfo>(tbl);
        //        }
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        _ex = new Exception(ex.Message);
        //    }
        //    if (Complete != null) Complete(Items, _ex);
        //}

        //public override void Delete(object Id, Action<Exception> Complete)
        //{
        //    Exception _ex = null;
        //    try
        //    {
        //        using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
        //        {
        //            System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
        //            _cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            _cmd.Parameters.AddWithValue("@Columns", "*");
        //            _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_LYDOVANG");
        //            _cmd.Parameters.AddWithValue("@WhereClause", string.Format(""));
        //        }
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        _ex = new Exception(ex.Message);
        //    }
        //    if (Complete != null) Complete(_ex);
        //}


        public void BatchDelete(IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatchInsert(IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void BatchUpdate(IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void DeleteByKey(object Id, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public void GetBy(object Id, Action<Entity.Interfaces.ILyDoVangInfo, Exception> Complete)
        {
            throw new NotImplementedException();
        }

        public void GetAll(Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Exception> Complete)
        {
            Exception _ex = null;
            IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items = null;
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_LYDOVANG");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format(""));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl != null && tbl.Rows.Count > 0)
                        Items = Libs.ConvertTableToList.ConvertToList<Entity.clsLyDoVangInfo>(tbl);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                _ex = new Exception(ex.Message);
            }
            if (Complete != null) Complete(Items, _ex);
        }

        public void Insert(Entity.Interfaces.ILyDoVangInfo item, Action<Interfaces.IResultOk> Complete)
        {
            
        }

        public void Update(Entity.Interfaces.ILyDoVangInfo item, Action<Interfaces.IResultOk> Complete)
        {
            throw new NotImplementedException();
        }

        public Interfaces.IResultOk ValidateInsert(Entity.Interfaces.ILyDoVangInfo Item)
        {
            throw new NotImplementedException();
        }

        public Interfaces.IResultOk ValidateUpdate(Entity.Interfaces.ILyDoVangInfo Item)
        {
            throw new NotImplementedException();
        }

        public Interfaces.IResultOk ValidateDelete(Entity.Interfaces.ILyDoVangInfo Item)
        {
            throw new NotImplementedException();
        }






        public void GetPaging(int selectedPage, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Exception> Complete)
        {
            throw new NotImplementedException();
        }

        public void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<Entity.Interfaces.ILyDoVangInfo>, Exception> Complete)
        {
            throw new NotImplementedException();
        }
    }
}
