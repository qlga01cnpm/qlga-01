﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsServer
    {
        public static bool RegisterIpWan(string ServerID,string IpLan, string IpWan,string Mac, int Port)
        {
            try
            {
                if (string.IsNullOrEmpty(IpWan)) return false;

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    //string _query = string.Format("Update WEB_SERVER_CUSTOMER set IPLan='{0}',IPWan='{1}',Mac='{2}',Port={3} Where ServerID='{4}'",
                    //    IpLan == null ? "" : IpLan, IpWan, Mac, Port, ServerID);

                    string _query = string.Format("Update WEB_SERVER_CUSTOMER set IPLan='{0}',IPWan='{1}',Port={2},LastUpdated='{3}' Where ServerID='{4}' and Mac='{5}'",
                        IpLan == null ? "" : IpLan, IpWan, Port, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"), ServerID, Mac);

                    SqlCommand cmd = new SqlCommand(_query, _cnn);
                    cmd.CommandType = System.Data.CommandType.Text;

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsServer.RegisterIpWan: " + ex.Message);
            }
        }

        public static string GetUrl(string ServerID)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThietBiCollection _objCollection = new Entity.clsThietBiCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.WEB_SERVER_CUSTOMER");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where ServerID='{0}'", ServerID));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl == null || tbl.Rows.Count == 0) return "";

                    _adapter.Dispose();
                    tbl.Dispose();
                    //http://116.108.153.137:91/d3f7c630-3789-499d-bd3f-3430d9074914.asmx
                    return string.Format("http://{0}:{1}/{2}", tbl.Rows[0]["IPWan"], tbl.Rows[0]["Port"], tbl.Rows[0]["PageName"]);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsServer.GetUrl: " + ex.Message);
            }
            finally
            {
            }
        }

        public static string GetUrlByMenuId(string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThietBiCollection _objCollection = new Entity.clsThietBiCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "t1.*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.WEB_SERVER_CUSTOMER t1 inner join dbo.WEB_SERVER_CUSTOMER_MENU t2 on t1.ServerID=t2.ServerID ");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where MenuId='{0}'", MenuId));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl == null || tbl.Rows.Count == 0) return "";

                    _adapter.Dispose();
                    tbl.Dispose();
                    //http://116.108.153.137:91/d3f7c630-3789-499d-bd3f-3430d9074914.asmx
                    return string.Format("http://{0}:{1}/{2}", tbl.Rows[0]["IPWan"], tbl.Rows[0]["Port"], tbl.Rows[0]["PageName"]);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsServer.GetUrl: " + ex.Message);
            }
            finally
            {
            }
        }



        public static bool RunScript(string SqlCommand)
        {
            try
            {
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    ServerConnection svrConnection = new ServerConnection(_cnn);
                    Server server = new Server(svrConnection);
                    //int _Result = server.ConnectionContext.ExecuteNonQuery(SqlCommand);

                    //var kq = _Result;
                    //if (kq > 0)
                    //{
                    //    return true;
                    //}
                    try
                    {
                        server.ConnectionContext.ExecuteNonQuery(SqlCommand, ExecutionTypes.ContinueOnError);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //handling and logging for the errors are done here
                        return false;
                    }
                }
                return false;
            }
            catch (Microsoft.SqlServer.Management.Common.ExecutionFailureException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
        }
         
    }
}
