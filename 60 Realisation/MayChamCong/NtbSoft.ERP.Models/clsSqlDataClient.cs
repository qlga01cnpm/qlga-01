﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsSqlDataClient : Entity.Interfaces.IDatabase
    {
        private string _strConnection = "";
        public clsSqlDataClient()
        {
            this._strConnection = Libs.SqlHelper.StrConnectionString;
        }
        public string ConnectionString
        {
            get { return this._strConnection; }
        }

        public int OpenConnection()
        {
            throw new NotImplementedException();
        }

        public int CloseConnection()
        {
            throw new NotImplementedException();
        }

        public int ExecuteSqlCommand()
        {
            throw new NotImplementedException();
        }
    }
}
