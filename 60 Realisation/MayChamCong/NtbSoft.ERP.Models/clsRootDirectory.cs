﻿using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsRootDirectory
    {
        public Entity.clsRootDirectoryCollection GetRootDirectory(string RootId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsRootDirectoryCollection _objCollection = new Entity.clsRootDirectoryCollection();
                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[3];

                    sp[0] = new System.Data.SqlClient.SqlParameter("@Action", Libs.ApiAction.GET.ToString());
                    sp[1] = new System.Data.SqlClient.SqlParameter("@id", RootId);//"
                    using (System.Data.SqlClient.SqlDataReader _reader = Libs.SqlHelper.ExecuteReader(_cnn, System.Data.CommandType.StoredProcedure, "sp_SYS_ROOT_DIRECTORY_GET_BY", sp))
                    {
                        while (_reader.Read())
                        {
                            if (!_reader.HasRows)
                                break;
                            Entity.clsRootDirectoryInfo _item = new Entity.clsRootDirectoryInfo();
                            _item.id = _reader["Id"].ToString().Trim();
                            _item.MenuParent = _reader["ParentId"].ToString();
                            _item.text = _reader["Name"].ToString().Trim();
                            _item.url = _reader["Url"].ToString();
                            _item.Sort = (int)_reader["Sort"];
                            _item.IsVisible = (bool)_reader["IsVisible"];
                            _item.iconCls = _reader["iconCls"].ToString().Trim();
                            _item.ContextMenuId = _reader["ContextMenuId"].ToString();

                            _objCollection.Add(_item);
                        }
                    }
                    return _objCollection;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThietBi.GetRootDirectory: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(ApiAction action, string Id, string Name, string ParentId,
            string ContextMenuId, string Url, int Sort, out string NewMenuName)
        {
            NewMenuName = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_SYS_ROOT_DIRECTORY", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", action.ToString());
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Name", Name == null ? "" : Name.Trim());
                    cmd.Parameters.AddWithValue("@ParentId", ParentId);
                    cmd.Parameters.AddWithValue("@RootType", "");
                    cmd.Parameters.AddWithValue("@ContextMenuId", ContextMenuId == null ? "" : ContextMenuId);
                    cmd.Parameters.AddWithValue("@Url", Url == null ? "" : Url);
                    cmd.Parameters.AddWithValue("@Sort", Sort);
                    /*** ***/
                    cmd.Parameters.Add("@OutNewName", System.Data.SqlDbType.NVarChar, 150);
                    cmd.Parameters["@OutNewName"].Direction = System.Data.ParameterDirection.Output;

                    int kq = cmd.ExecuteNonQuery();

                    NewMenuName = cmd.Parameters["@OutNewName"].Value.ToString();

                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsRootDirectory.Update: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Rename(string Id, string Name)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection sqlcnn = SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlParameter[] sp = new System.Data.SqlClient.SqlParameter[3];
                    sp[0] = new System.Data.SqlClient.SqlParameter("@TableName", "dbo.SYS_ROOT_DIRECTORY");
                    sp[1] = new System.Data.SqlClient.SqlParameter("@SetValues", string.Format("Name=N'{0}'", Name == null ? "" : Name.Trim()));
                    sp[2] = new System.Data.SqlClient.SqlParameter("@WhereClause", string.Format("Where Id='{0}'", Id));

                    int result = (int)SqlHelper.ExecuteNonQuery(sqlcnn, System.Data.CommandType.StoredProcedure, "sp_Update", sp);

                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsRootDirectory.Rename: " + ex.Message);
            }
            finally
            {
            }
        }

    }
}
