﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    public class clsThietBi 
    {
        public System.Data.DataTable TYPE_DIC_THIETBI
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_THIETBI");
                _tbl.Columns.Add("MaThietBi", typeof(System.String));
                _tbl.Columns.Add("MoTa", typeof(System.String));
                _tbl.Columns.Add("SoMuiCM", typeof(float));
                _tbl.Columns.Add("TocDo", typeof(int));
                _tbl.Columns.Add("HaoPhiThietBi", typeof(float));
                _tbl.Columns.Add("HaoPhiTayChan", typeof(float));
                //_tbl.Columns.Add("AnhThietBi", typeof(byte[]));
                _tbl.Columns.Add("RootId", typeof(System.String));


                _tbl.PrimaryKey = new System.Data.DataColumn[] { _tbl.Columns["MaThietBi"] };
                return _tbl;
            }
        }


        /// <summary>
        /// Lấy danh sách bậc thợ theo Id thư mục gốc
        /// </summary>
        /// <param name="RootId"></param>
        /// <returns></returns>
        public Entity.clsThietBiCollection GetBy(string RootId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThietBiCollection _objCollection = new Entity.clsThietBiCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THIETBI");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where RootId='{0}'", RootId));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    List<Entity.clsThietBiInfo> arrayTemp = new List<Entity.clsThietBiInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThietBiInfo()
                        {
                            MaThietBi = m.Field<string>("MaThietBi"),
                            MoTa = m.Field<string>("MoTa"),
                            SoMuiCM = float.Parse(m["SoMuiCM"].ToString()),
                            TocDo = m.Field<int>("TocDo"),
                            HaoPhiThietBi = float.Parse(m["HaoPhiThietBi"].ToString()),
                            HaoPhiTayChan = float.Parse(m["HaoPhiTayChan"].ToString()),
                            RootId = m.Field<string>("RootId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return new Entity.clsThietBiCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThietBi.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public Entity.clsThietBiInfo GetThietBi(string MaThietBi)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsThietBiCollection _objCollection = new Entity.clsThietBiCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_THIETBI");
                    _cmd.Parameters.AddWithValue("@WhereClause", string.Format("Where MaThietBi='{0}'", MaThietBi.Trim()));

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    if (tbl == null || tbl.Rows.Count == 0) return new Entity.clsThietBiInfo();

                    List<Entity.clsThietBiInfo> arrayTemp = new List<Entity.clsThietBiInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsThietBiInfo()
                        {
                            MaThietBi = m.Field<string>("MaThietBi"),
                            MoTa = m.Field<string>("MoTa"),
                            SoMuiCM = float.Parse(m["SoMuiCM"].ToString()),
                            TocDo = m.Field<int>("TocDo"),
                            HaoPhiThietBi = float.Parse(m["HaoPhiThietBi"].ToString()),
                            HaoPhiTayChan = float.Parse(m["HaoPhiTayChan"].ToString()),
                            RootId = m.Field<string>("RootId")

                        }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsThietBi.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsThietBiCollection Items)
        {
            try
            {
                System.Data.DataTable _TYPE = this.TYPE_DIC_THIETBI;
                foreach (var item in Items)
                {
                    if (item.MaThietBi.Length > 10)
                        throw new Exception("Mã bậc thợ vượt quá 10 ký tự");
                    System.Data.DataRow newRow = _TYPE.NewRow();
                    newRow["MaThietBi"] = item.MaThietBi;
                    newRow["MoTa"] = item.MoTa == null ? "" : item.MoTa;
                    newRow["SoMuiCM"] = item.SoMuiCM;
                    newRow["TocDo"] = item.TocDo;
                    newRow["HaoPhiThietBi"] = item.HaoPhiThietBi;
                    newRow["HaoPhiTayChan"] = item.HaoPhiTayChan;
                    newRow["RootId"] = item.RootId;
                    _TYPE.Rows.Add(newRow);
                }

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_THIETBI", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.POST.ToString());
                    cmd.Parameters.AddWithValue("@TYPE", _TYPE);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsBacTho.Update: " + ex.Message);
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                DataTable _Type = this.TYPE_DIC_THIETBI;
                DataRow newRow = _Type.NewRow();
                newRow["MaThietBi"] = Id;
                newRow["MoTa"] = "";
                newRow["SoMuiCM"] = 0;
                newRow["TocDo"] = 0;
                newRow["HaoPhiThietBi"] = 0;
                newRow["HaoPhiTayChan"] = 0;
                newRow["RootId"] = "";
                _Type.Rows.Add(newRow);

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_THIETBI", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.DELETE.ToString());
                    cmd.Parameters.AddWithValue("@TYPE", _Type);
                    //cmd.Parameters.Add("", SqlDbType.Image)

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsThietBi.Delete: " + ex.Message);
            }
        }

        public double Lay_TMU_Duong_may(string pMaThietBi, double pChieu_dai,double pSoLanXuatHien, Libs.DIEM_DUNG pDIEM_DUNG)
        {
            if (string.IsNullOrWhiteSpace(pMaThietBi))
                return 0.0;
            Entity.clsThietBiInfo Item = this.GetThietBi(pMaThietBi);
            if (Item == null || string.IsNullOrWhiteSpace(Item.MaThietBi)) return 0.0;

            return (this.Lay_BST(Item) * pChieu_dai + (double)pDIEM_DUNG + 18.0) * pSoLanXuatHien;
        }

        protected double Lay_BST(Entity.clsThietBiInfo Item)
        {
            if (Item == null || string.IsNullOrWhiteSpace(Item.MaThietBi))
                return 0.0;

            return Item.SoMuiCM / ((double)Item.TocDo * Libs.clsDBConstant.TMU_PHUT);
        }
    }
}
