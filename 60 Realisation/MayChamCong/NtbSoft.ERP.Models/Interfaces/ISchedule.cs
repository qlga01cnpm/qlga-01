﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    /// <summary>
    /// Lịch trình ca làm việc
    /// </summary>
    public interface ISchedule
    {
        void GetAll(Action<List<Entity.clsScheduleInfo>, IResultOk> Complete);
        //void BatchUpdate(List<Entity.Interfaces.IScheduleInfo> Items);

        void Update(Libs.ApiAction Action, Entity.Interfaces.IScheduleInfo Item, Action<IResultOk> Complete);

        void Delete(int Id, Action<IResultOk> Complete);
    }
}
