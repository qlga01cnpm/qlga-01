﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IResultOk
    {
        Dictionary<string, string> Errors { get; set; }
        /// <summary>
        /// Get
        /// </summary>
        string Message { get; }
        int RowsAffected { get; set; }
        
        /// <summary>
        /// True: kết quả thành công. False: có lỗi xảy ra
        /// </summary>
        bool Success { get; }
    }
}
