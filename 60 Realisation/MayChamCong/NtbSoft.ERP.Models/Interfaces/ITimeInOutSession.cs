﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface ITimeInOutSession
    {
        void GetBy(DateTime FromDate, DateTime ToDate, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo>, IResultOk> Complete);
        void GetPaging(int PageIndex,int PageSize, DateTime FromDate, DateTime ToDate, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo>, IResultOk,int> Complete);

        void Update(Libs.ApiAction Action,double Id, DateTime WorkingDay, int MaCC, string Gio, Action<IResultOk> Complete);
    }
}
