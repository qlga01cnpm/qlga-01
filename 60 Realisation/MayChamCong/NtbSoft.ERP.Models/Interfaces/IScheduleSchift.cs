﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IScheduleSchift
    {
        void GetBy(int SchID, Action<List<Entity.clsScheduleShiftInfo>, IResultOk> Complete);
        void GetBy(string SchName, Action<List<Entity.clsScheduleShiftInfo>, IResultOk> Complete);
        //void GetByEmID(double EmpID, Action<List<Entity.clsScheduleShiftInfo>, IResultOk> Complete);
        void BatchUpdate(int SchID, List<Entity.clsScheduleShiftInfo> Items, Action<IResultOk> Complete);

        /// <summary>
        /// Lấy ca làm việc. Dựa vào Lịch trình ca, và ngày làm việc
        /// </summary>
        /// <param name="SchName"></param>
        /// <param name="WorkingDay"></param>
        /// <param name="Complete"></param>
        //void GetBy(string SchName, DateTime WorkingDay, Action<Entity.clsScheduleShiftInfo, IResultOk> Complete);
    }
}
