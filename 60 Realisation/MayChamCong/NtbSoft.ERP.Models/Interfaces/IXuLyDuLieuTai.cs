﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    /// <summary>
    /// Xử lý dữ liệu tải
    /// </summary>
    public interface IXuLyDuLieuTai
    {
        void CheckTonTaiBang(string TableName, Action<bool, IResultOk> Complete);
        /// <summary>
        /// Kiểm tra giờ đó có hợp lệ hay không
        /// </summary>
        /// <param name="GioCham"></param>
        /// <returns></returns>
        bool IsTime(string GioCham);
        bool CheckHopLe(string MaCC,DateTime Ngay, string GioCham);

        void BatDauTinhDiMuon1(string ShiftID, Action<string, IResultOk> Complete);
        void BatDauTinhDiMuon2(string ShiftID, Action<string, IResultOk> Complete);
        void BatDauTinhDiSom1(string ShiftID, Action<string, IResultOk> Complete);
        void BatDauTinhDiSom2(string ShiftID, Action<string, IResultOk> Complete);

        void BreakInOfShift(string ShiftID, Action<string, IResultOk> Complete);

        void CheckExistInOut(string MaNV, string WorkingDay, Action<bool, IResultOk> Complete);

        /// <summary>
        /// SELECT NumberShift FROM Schedule WHERE SCHName=
        /// </summary>
        /// <param name="SchID"></param>
        /// <param name="Complete"></param>
        void NumberShift(string SchID, Action<int, IResultOk> Complete);

        /// <summary>
        /// SELECT eID FROM Employees WHERE MaCC='
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Complete"></param>
        void ReturnEmpID(string MaCC, Action<double, IResultOk> Complete);

        /// <summary>
        /// SELECT COUNT(WorkingDay) as SL FROM
        /// </summary>
        /// <param name="MaCC"></param>
        /// <param name="Ngay"></param>
        /// <param name="Complete"></param>
        void SoLanCham(string MaCC, DateTime Ngay, Action<int, IResultOk> Complete);

        void SoLanCham(string MaCC, DateTime Ngay, Action<List<TimeSpan>, IResultOk> Complete);

        void MinGioCham(string MaCC, DateTime Ngay, Action<string, IResultOk> Complete);
        void MaxGioCham(string MaCC, DateTime Ngay, Action<string,IResultOk> Complete);

        void BreakInGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, string BreakOutGioCham, Action<string,IResultOk> Complete);

        void BreakOutGioCham(string MaCC, DateTime Ngay, string MinGioCham, string MaxGioCham, Action<string,IResultOk> Complete);

        void XuLyDuLieuChamCong(string MaCC, DateTime CurDay, Action<bool, IResultOk> Complete);

        void InitializeDate(int Month, int MaxDay, int Year, string MaCC, Action<IResultOk> Complete);

        void InitializeDate(int Month, int MaxDay, int Year, Action<IResultOk> Complete);
    }
}
