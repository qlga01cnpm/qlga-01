﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public delegate void TerminalDataEventHandler(object sender,TerminalDataEventArgs e);
    public class TerminalDataEventArgs : EventArgs
    {
        public List<Entity.Interfaces.ITerminalInfo> TerminalCollection { get; set; }
        public Exception Ex { get; set; }
    }

    public interface ITerminal
    {
        event TerminalDataEventHandler LoadCompleted;
        //List<Entity.Interfaces.ITerminalInfo> GetAll();
        void GetAll();
        Entity.Interfaces.ITerminalInfo GetBy(int Id);
        bool Update(Entity.Interfaces.ITerminalInfo Item);
        bool Delete(int Id);
    }
}
