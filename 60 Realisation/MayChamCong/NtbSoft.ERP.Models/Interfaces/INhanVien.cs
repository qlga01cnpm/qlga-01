﻿using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface INhanVien
    {
        void GeBy(int Id, Action<Entity.Interfaces.INhanVienInfo, IResultOk> Complete);
        void GeBy(string MaNV, Action<Entity.Interfaces.INhanVienInfo, IResultOk> Complete);
        void GeByMaCC(string MaCC, Action<Entity.Interfaces.INhanVienInfo, IResultOk> Complete);

        void GetVaoRaByDate(string maNv, string date, Action<float, Interfaces.IResultOk> Complete);
    }

    public interface INhanVienChangeDept
    {
        void Change(string MaNV, string DepId, DateTime ChangeDate, Action<IResultOk> Complete);
    }

    public interface INhanVienDaXoa
    {
        void GetAll(Action<List<Entity.Interfaces.INhanVienInfo>, IResultOk> Complete);
        void Modifiy(ApiAction action, string MaNV, Action<IResultOk> Complete);
    }

    public interface INhanVienException
    {
        void GetAll(Action<List<Entity.Interfaces.INhanVienInfo>, IResultOk> Complete);
        void Modifiy(ApiAction action, string MaNV, Action<IResultOk> Complete);
    }

    public interface INhanVienNghiViec
    {
        void GetAll(string ValueSearch, DateTime FromDate, DateTime ToDate, Action<List<Entity.Interfaces.INhanVienInfo>, IResultOk> Complete);
        void ChoNghiViec(ApiAction action, string MaNV, string LyDoNghi, DateTime StopWorkingday, Action<IResultOk> Complete);
        void PhucHoi(ApiAction action, string MaNV, Action<IResultOk> Complete);
    }
}
