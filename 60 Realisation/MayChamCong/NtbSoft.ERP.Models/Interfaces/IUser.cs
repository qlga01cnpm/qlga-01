﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    /// <summary>
    /// Quản lý người dùng
    /// </summary>
    public interface IUser
    {
        List<Entity.Interfaces.IUserInfo> GetAll();
        Entity.Interfaces.IUserInfo GetBy(string UserId);
        bool Update(Entity.Interfaces.IUserInfo Item);
        bool Delete(string UserId);
        
    }
}
