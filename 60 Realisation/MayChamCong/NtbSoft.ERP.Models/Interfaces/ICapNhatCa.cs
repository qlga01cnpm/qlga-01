﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    /// <summary>
    /// Cập nhật ca làm việc, và lịch trình ca
    /// </summary>
    public interface ICapNhatCa
    {
        void CapNhatCaLichTrinh(string ShiftId, string SchID, List<Entity.Interfaces.INhanVienInfo> Items, Action<IResultOk> Complete);
    }
}
