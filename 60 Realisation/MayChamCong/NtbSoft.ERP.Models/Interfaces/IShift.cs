﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IShift
    {
        void GetBy(string ShiftId, Action<Entity.clsShiftInfo, IResultOk> Complete);
        /// <summary>
        /// 1: Chủ nhật
        /// 2: Thứ 2
        /// 3: Thứ 3
        /// 4: Thứ 4
        /// 5: Thứ 5
        /// 6: Thứ 6
        /// 7: Thứ 7
        /// 8: Ngày lễ
        /// </summary>
        /// <param name="EmpID"></param>
        /// <param name="Ngay"></param>
        /// <param name="Complete"></param>
        void GetBy(double EmpID,int Ngay, Action<Entity.clsShiftInfo, IResultOk> Complete);
        void GetAll(Action<List<Entity.clsShiftInfo>, IResultOk> Complete);
        void GetAll(Action<JData.DataComboBoxInfo[], IResultOk> Complete);
        void Update(Entity.Interfaces.IShiftInfo Item, Action<IResultOk> Complete);
        void Delete(int Id, Action<IResultOk> Complete);
    }
}
