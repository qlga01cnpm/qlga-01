﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface ITimeSheets
    {
        //void GetBy(int Month,int Year,)
        void UpdateTimeSheet(Entity.Interfaces.ITimeInOutSessionInfo Item, Action<IResultOk> Complete);

        void ReportMonthly(int year, int month, string DepId, string MaNV, Action<IEnumerable<Entity.Interfaces.ITimeSheetInfo>, IResultOk> Complete);
    }
}
