﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IQuyTac
    {
        void GetFirst(Action<Entity.Interfaces.IQuyTacInfo, IResultOk> Complete);
        void SetSoPhut(int SoPhutGiuaHaiLan, Action<IResultOk> Complete);

        /// <summary>
        /// SELECT SoPhutGiuaHaiLan FROM tblQuyTac
        /// </summary>
        /// <param name="Complete"></param>
        void SoPhutToiThieuGiuaHaiLanCham(Action<int, IResultOk> Complete);
        void checkInRaMay(Action<bool, IResultOk> Complete);

    }
}
