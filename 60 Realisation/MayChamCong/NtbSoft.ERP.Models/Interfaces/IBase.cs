﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IBase<T> 
    {
        void BatchDelete(IEnumerable<T> Items, Action<IResultOk> Complete);
        void BatchInsert(IEnumerable<T> Items, Action<IResultOk> Complete);
        void BatchUpdate(IEnumerable<T> Items, Action<IResultOk> Complete);
        void DeleteByKey(object Id, Action<IResultOk> Complete);
        void GetBy(object Id, Action<T, Exception> Complete);
        void GetAll(Action<IEnumerable<T>,Exception> Complete);
        void Insert(T item, Action<IResultOk> Complete);
        void Update(T item, Action<IResultOk> Complete);
        void GetPaging(int selectedPage, int pageSize, string WhereClause, out int totalPages, out int totalRecords, Action<IEnumerable<T>, Exception> Complete);
        void GetPaging(DateTime FromDate, DateTime ToDate, int selectedPage, int pageSize, out int totalPages, out int totalRecords, Action<IEnumerable<T>, Exception> Complete);

        Interfaces.IResultOk ValidateInsert(T Item);
        Interfaces.IResultOk ValidateUpdate(T Item);
        Interfaces.IResultOk ValidateDelete(T Item);
    }

     
}
