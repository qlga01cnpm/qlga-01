﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IWorkSheet
    {
        void LoadDataMonth(int Month, int Year, string DepId, string MaNV, Action<System.Data.DataTable, IResultOk> Complete);

    }
}
