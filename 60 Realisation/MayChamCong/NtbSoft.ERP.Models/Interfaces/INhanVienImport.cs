﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface INhanVienImport
    {
        void CheckMaNV(string MaNV, Action<bool, IResultOk> Complete);
        void CheckMaCC(string MaCC, Action<bool, IResultOk> Complete);
        void Import(System.Data.DataTable TypeNhanVien, Action<IResultOk> Complete);
    }
}
