﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    public interface IQuyTacLamTron
    {
        void GetAll(Action<List<Entity.clsQuyTacLamTronInfo>, IResultOk> Complete);
        void BatchUpdate(IEnumerable<Entity.Interfaces.IQuyTacLamTronInfo> Items, Action<IResultOk> Complete);
        void Update(int Id, int TuPhut, int DenPhut, int LamTron, Action<IResultOk> Complete);
        void Delete(int Id, Action<IResultOk> Complete);
        void LamTronPhut(int SoPhut, Action<int, Interfaces.IResultOk> Complete);
    }
}
