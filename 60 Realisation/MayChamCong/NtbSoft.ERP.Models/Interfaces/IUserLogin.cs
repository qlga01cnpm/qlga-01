﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models.Interfaces
{
    /// <summary>
    /// Quản lý người dùng đăng nhập
    /// </summary>
    public interface IUserLogin
    {
        Entity.Interfaces.IUserInfo UserInfo { get; set; }
        void Login(string UserName, string Password, Action<Exception> complete);
        void LogOut(string UserName);
        bool ChangePassword(string UserName, string OldPassword, string NewPassword, string ConformPassword, List<Exception> complete);
        bool IsLogin { get; }
    }
}
