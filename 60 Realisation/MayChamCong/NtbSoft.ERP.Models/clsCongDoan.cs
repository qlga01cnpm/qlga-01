﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Text.RegularExpressions;

namespace NtbSoft.ERP.Models
{
    public class clsCongDoan
    {
        public System.Data.DataTable TYPE_IED_THAOTACCONGDOAN
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_IED_THAOTACCONGDOAN");
                _tbl.Columns.Add("ThaoTacCongDoanID", typeof(double));
                _tbl.Columns.Add("Id_DmCd", typeof(System.String));
                _tbl.Columns.Add("MaSo", typeof(System.String));
                _tbl.Columns.Add("ThuTu", typeof(int));
                _tbl.Columns.Add("SoLanXuatHien", typeof(float));
                _tbl.Columns.Add("MoTa", typeof(System.String));
                _tbl.Columns.Add("LoaiMaSo", typeof(System.String));
                _tbl.Columns.Add("TMUThaoTac", typeof(float));
                _tbl.Columns.Add("TMUThietBi", typeof(float));

                return _tbl;
            }
        }

        /// <summary>
        /// Kiểm tra mã số có phải là mã số đặc biệt hay không?
        /// </summary>
        /// <param name="MaSo"></param>
        /// <returns></returns>
        public bool LaThaoTacDacBiet(string MaSo)
        {
            MaSo = MaSo.ToUpper();
            string pattern = "SP\\d{1,3}";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(MaSo);
        }

        /// <summary>
        /// Kiểm tra mã số có phải là Đường May không
        /// </summary>
        /// <param name="MaSo"></param>
        /// <returns></returns>
        public bool LaDuongMay(string MaSo)
        {
            MaSo = MaSo.ToUpper();
            string pattern = "SE(\\d{1,3}\\.[0-9]|\\d{1,3})[ABC]";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(MaSo);
        }

        public bool LaDuongMay(string pMA_SO, Entity.clsTreeDepInfo pBoPhan)
        {
            return LaDuongMay(pMA_SO) && pBoPhan != null && pBoPhan.IsChainSew;
        }

        /// <summary>
        /// Kiểm tra mã số có phải là Cắt hay không
        /// </summary>
        /// <param name="MaSo"></param>
        /// <returns></returns>
        public bool LaCat(string MaSo)
        {
            MaSo = MaSo.ToUpper();
            string pattern = "C(\\d{1,3}\\.[0-9]|\\d{1,3})[CHS][ABC]";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(MaSo);
        }
        public bool LaCat(string pMA_SO, Entity.clsTreeDepInfo pBoPhan)
        {
            return LaCat(pMA_SO) && pBoPhan != null && pBoPhan.IsChainCut;
        }
        protected double Lay_BST(byte pLucTacDung)
        {
            if (pLucTacDung == 0)
                return 0.0;
            return Math.Sqrt(0.15 / (double)pLucTacDung) * 27.799999237060547;
        }
        public double Lay_TMU_Cat(byte pLucTacDung, double pDoKho, double pChieuDai, Libs.DIEM_DUNG pDIEM_DUNG)
        {
            //var kq = this.Lay_BST(pLucTacDung) * pDoKho * pChieuDai + (double)pDIEM_DUNG;
            return this.Lay_BST(pLucTacDung) * pDoKho * pChieuDai + (double)pDIEM_DUNG;
        }

        public Entity.clsCongDoanCollection GetAll()
        {
            List<Entity.clsCongDoanInfo> arrayTemp = new List<Entity.clsCongDoanInfo>();
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "IED_CONGDOAN");
                    _cmd.Parameters.AddWithValue("@WhereClause", "");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataSet ds = new System.Data.DataSet();
                    _adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0) return new Entity.clsCongDoanCollection();


                    
                    arrayTemp = ds.Tables[0].AsEnumerable().Select(m => new Entity.clsCongDoanInfo()
                    {
                        Id_Dm = m.Field<string>("Id_Dm"),
                        MenuId = m.Field<string>("MenuId"),
                        MaSo = m.Field<string>("MaSo"),
                        TenCongDoan = m.Field<string>("TenCongDoan"),
                        SoPhienBan = Convert.ToDouble(m["SoPhienBan"]),
                        TrangThai = m.Field<int>("TrangThai"),
                        DangKhoa = m.Field<bool>("DangKhoa"),

                        MoTa = m.Field<string>("MoTa"),
                        MaBoPhan = m.Field<string>("MaBoPhan"),
                        LucTacDung = m.Field<int>("LucTacDung"),
                        MaBacTho = m.Field<string>("MaBacTho"),
                        MaThietBi = m.Field<string>("MaThietBi"),
                        MaThaoTacSp = m.Field<string>("MaThaoTacSp"),
                        HaoPhiDacBiet = float.Parse(m["HaoPhiDacBiet"].ToString()),
                        MoTaNoiLamViec = m.Field<string>("MoTaNoiLamViec"),
                        ChiPhiCongDoan = float.Parse(m["ChiPhiCongDoan"].ToString()),
                        TongThoiGian = float.Parse(m["TongThoiGian"].ToString()),
                        NguoiTao = m.Field<string>("NguoiTao"),
                        NgayTao = Convert.ToDateTime(m["NgayTao"]),
                        NguoiCapNhat = m.Field<string>("NguoiCapNhat"),
                        NgayCapNhat = Convert.ToDateTime(m["NgayCapNhat"]),

                    }).ToList();

                    _adapter.Dispose();
                    ds.Dispose();

                    return new Entity.clsCongDoanCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsCongDoan.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lấy một công đoạn theo Menu chính
        /// </summary>
        /// <param name="MenuId"></param>
        /// <returns></returns>
        public Entity.clsCongDoanInfo GetByMenuFirst(string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_CONGDOAN_GET_BY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.GET.ToString());
                    _cmd.Parameters.AddWithValue("@MenuId", MenuId);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataSet ds = new System.Data.DataSet();
                    _adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0) return new Entity.clsCongDoanInfo();

                    #region Lấy Thiết bị
                    Entity.clsThietBiInfo mThietBiInfo = null;
                    if (ds.Tables[2].Rows.Count > 0)
                        mThietBiInfo = ds.Tables[2].AsEnumerable().Select(n => new Entity.clsThietBiInfo()
                        {
                            MaThietBi = n["MaThietBi"].ToString(),
                            MoTa = n["MaThietBi"].ToString(),
                            SoMuiCM = Convert.ToSingle(n["SoMuiCM"]),
                            TocDo = Convert.ToInt32(n["TocDo"]),
                            HaoPhiThietBi = Convert.ToSingle(n["HaoPhiThietBi"]),
                            HaoPhiTayChan = Convert.ToSingle(n["HaoPhiTayChan"])
                        }).FirstOrDefault();
                    #endregion

                    #region Lấy Thao tác sản phẩm
                    Entity.clsThaoTacSpInfo mThaoTacSpInfo = null;
                    if (ds.Tables[3].Rows.Count > 0)
                        mThaoTacSpInfo = ds.Tables[3].AsEnumerable().Select(n => new Entity.clsThaoTacSpInfo()
                        {
                            MaThaoTacSP = n["MaThaoTacSP"].ToString(),
                            MoTa = n["MoTa"].ToString(),
                            ThoiGianThaoTac = Convert.ToSingle(n["ThoiGianThaoTac"]),
                            SoLuong = Convert.ToInt32(n["SoLuong"])
                        }).FirstOrDefault();
                    #endregion

                    #region Hàm trả về danh sách các thao tác công đoạn
                    Func<string, Entity.clsThaoTacCongDoanCollection> GetThaoTac = delegate(string Id_Dm)
                    {
                        if (ds.Tables[1].Rows.Count == 0) return new Entity.clsThaoTacCongDoanCollection(new Entity.clsThaoTacCongDoanInfo());

                        string _Expression = string.Format("Id_DmCd='{0}'", Id_Dm);

                        var items = ds.Tables[1].Select(_Expression).AsEnumerable().Select(n => new Entity.clsThaoTacCongDoanInfo()
                        {
                            ThaoTacCongDoanID = Convert.ToDouble(n["ThaoTacCongDoanID"]),
                            Id_DmCd = n["Id_DmCd"].ToString(),
                            MaSo=n["MaSo"].ToString(),
                            ThuTu = n.Field<int>("ThuTu"),
                            SoLanXuatHien = (float)Convert.ToDouble(n["SoLanXuatHien"]),
                            MoTa=n["MoTa"].ToString(),
                            TMUThaoTac = (float)Convert.ToDouble(n["TMUThaoTac"]),
                            LoaiMaSo = n["LoaiMaSo"].ToString(),
                            TMUThietBi = (float)Convert.ToDouble(n["TMUThietBi"]),
                            ChieuDaiDuongMay = Convert.ToSingle(n["ChieuDaiDuongMay"])
                        }).ToList();

                        if (items == null) return new Entity.clsThaoTacCongDoanCollection(new Entity.clsThaoTacCongDoanInfo());

                        return new Entity.clsThaoTacCongDoanCollection(items);
                    };
                    #endregion


                    List<Entity.clsCongDoanInfo> arrayTemp = new List<Entity.clsCongDoanInfo>();
                    
                    arrayTemp = ds.Tables[0].AsEnumerable().Select(m => new Entity.clsCongDoanInfo()
                    {
                        Id_Dm = m.Field<string>("Id_Dm"),
                        MenuId = m.Field<string>("MenuId"),
                        MaSo = m.Field<string>("MaSo"),
                        TenCongDoan = m.Field<string>("TenCongDoan"),
                        SoPhienBan = Convert.ToDouble(m["SoPhienBan"]),
                        TrangThai = m.Field<int>("TrangThai"),
                        DangKhoa = m.Field<bool>("DangKhoa"),

                        MoTa = m.Field<string>("MoTa"),
                        MaBoPhan = m.Field<string>("MaBoPhan"),
                        LucTacDung = m.Field<int>("LucTacDung"),
                        MaBacTho = m.Field<string>("MaBacTho"),
                        MaThietBi = m.Field<string>("MaThietBi"),
                        MaThaoTacSp = m.Field<string>("MaThaoTacSp"),
                        HaoPhiDacBiet = float.Parse(m["HaoPhiDacBiet"].ToString()),
                        MoTaNoiLamViec = m.Field<string>("MoTaNoiLamViec"),
                        ChiPhiCongDoan = float.Parse(m["ChiPhiCongDoan"].ToString()),
                        TongThoiGian = float.Parse(m["TongThoiGian"].ToString()),
                        NguoiTao = m.Field<string>("NguoiTao"),
                        NgayTao = Convert.ToDateTime(m["NgayTao"]),
                        NguoiCapNhat = m.Field<string>("NguoiCapNhat"),
                        NgayCapNhat = Convert.ToDateTime(m["NgayCapNhat"]),
                        ThaoTacCongDoan = GetThaoTac(m.Field<string>("Id_Dm"))

                    }).ToList();

                    _adapter.Dispose();
                    ds.Dispose();
                    arrayTemp[0].SetThietBi_ThaoTac(mThietBiInfo, mThaoTacSpInfo);
                    arrayTemp[0].THOI_GIAN_THAO_TAC_HAO_PHI_PHUT(mThietBiInfo);
                    arrayTemp[0].THOI_GIAN_THIET_BI_HAO_PHI_PHUT(mThietBiInfo);
                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsCongDoan.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lấy tất cả các công đoạn they menu Menu chính
        /// </summary>
        /// <param name="MenuId"></param>
        /// <returns></returns>
        public Entity.clsCongDoanCollection GetByMenu(string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    string tblJoin = string.Format("dbo.SYS_MENU_MASTER t1 inner join dbo.IED_CONGDOAN t2 on t1.MenuId=t2.MenuId and t1.MenuParent='{0}'", MenuId);
                    _cmd.Parameters.AddWithValue("@Columns", "t2.*");
                    _cmd.Parameters.AddWithValue("@TableName", tblJoin);
                    _cmd.Parameters.AddWithValue("@WhereClause", "");


                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new DataTable();
                    _adapter.Fill(tbl);
                    if (tbl.Rows.Count == 0) return new Entity.clsCongDoanCollection();
                    List<Entity.clsCongDoanInfo> arrayTemp = new List<Entity.clsCongDoanInfo>();
                    arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsCongDoanInfo()
                    {
                        Id_Dm = m.Field<string>("Id_Dm"),
                        MenuId = m.Field<string>("MenuId"),
                        MaSo = m.Field<string>("MaSo"),
                        TenCongDoan = m.Field<string>("TenCongDoan"),
                        SoPhienBan = Convert.ToDouble(m["SoPhienBan"]),
                        TrangThai = m.Field<int>("TrangThai"),
                        DangKhoa = m.Field<bool>("DangKhoa"),

                        MoTa = m.Field<string>("MoTa"),
                        MaBoPhan = m.Field<string>("MaBoPhan"),
                        LucTacDung = m.Field<int>("LucTacDung"),
                        MaBacTho = m.Field<string>("MaBacTho"),
                        MaThietBi = m.Field<string>("MaThietBi"),
                        MaThaoTacSp = m.Field<string>("MaThaoTacSp"),
                        HaoPhiDacBiet = float.Parse(m["HaoPhiDacBiet"].ToString()),
                        MoTaNoiLamViec = m.Field<string>("MoTaNoiLamViec"),
                        ChiPhiCongDoan = float.Parse(m["ChiPhiCongDoan"].ToString()),
                        TongThoiGian = float.Parse(m["TongThoiGian"].ToString()),
                        NguoiTao = m.Field<string>("NguoiTao"),
                        NgayTao = Convert.ToDateTime(m["NgayTao"]),
                        NguoiCapNhat = m.Field<string>("NguoiCapNhat"),
                        NgayCapNhat = Convert.ToDateTime(m["NgayCapNhat"]),

                        ThoiGianChuan = float.Parse(m["ThoiGianChuan"].ToString()),
                    }).ToList();

                    _adapter.Dispose();
                    tbl.Dispose();

                    return new Entity.clsCongDoanCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsCongDoan.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsCongDoanInfo Item)
        {
            if (Item == null) return false;
            try
            {
                System.Data.DataTable TypeThaoTac = this.TYPE_IED_THAOTACCONGDOAN;
                if(Item.ThaoTacCongDoan!=null)
                    foreach (var itemThaoTac in Item.ThaoTacCongDoan)
                    {
                        if (string.IsNullOrWhiteSpace(itemThaoTac.MaSo)) continue;
                        System.Data.DataRow newRow = TypeThaoTac.NewRow();
                        newRow["ThaoTacCongDoanID"] = itemThaoTac.ThaoTacCongDoanID;
                        newRow["Id_DmCd"] = itemThaoTac.Id_DmCd == null ? "" : itemThaoTac.Id_DmCd;
                        newRow["MaSo"] = itemThaoTac.MaSo;
                        newRow["ThuTu"] = itemThaoTac.ThuTu;
                        newRow["SoLanXuatHien"] = itemThaoTac.SoLanXuatHien;
                        newRow["MoTa"] = itemThaoTac.MoTa;
                        newRow["LoaiMaSo"] = itemThaoTac.LoaiMaSo == null ? "" : itemThaoTac.LoaiMaSo;
                        newRow["TMUThaoTac"] = itemThaoTac.TMUThaoTac;
                        newRow["TMUThietBi"] = itemThaoTac.TMUThietBi;
                        TypeThaoTac.Rows.Add(newRow);
                    }

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_CONGDOAN", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.POST.ToString());
                    cmd.Parameters.AddWithValue("@Id_Dm", Item.Id_Dm == null ? "00" : Item.Id_Dm);
                    cmd.Parameters.AddWithValue("@MenuId", Item.MenuId);
                    cmd.Parameters.AddWithValue("@MaSo", Item.MaSo);
                    cmd.Parameters.AddWithValue("@TenCongDoan", Item.TenCongDoan == null ? "" : Item.TenCongDoan);
                    cmd.Parameters.AddWithValue("@SoPhienBan", Item.SoPhienBan);
                    cmd.Parameters.AddWithValue("@TrangThai", Item.TrangThai);
                    cmd.Parameters.AddWithValue("@DangKhoa", Item.DangKhoa);
                    cmd.Parameters.AddWithValue("@MoTa", Item.MoTa == null ? "" : Item.MoTa);
                    cmd.Parameters.AddWithValue("@MaBoPhan", Item.MaBoPhan);

                    cmd.Parameters.AddWithValue("@LucTacDung", Item.LucTacDung);
                    cmd.Parameters.AddWithValue("@MaBacTho", Item.MaBacTho);
                    cmd.Parameters.AddWithValue("@MaThietBi", Item.MaThietBi);
                    cmd.Parameters.AddWithValue("@MaThaoTacSp", Item.MaThaoTacSp);
                    cmd.Parameters.AddWithValue("@HaoPhiDacBiet", Item.HaoPhiDacBiet);
                    cmd.Parameters.AddWithValue("@MoTaNoiLamViec", Item.MoTaNoiLamViec == null ? "" : Item.MoTaNoiLamViec);
                    cmd.Parameters.AddWithValue("@ChiPhiCongDoan", Item.ChiPhiCongDoan);
                    cmd.Parameters.AddWithValue("@TongThoiGian", Item.TongThoiGian);
                    cmd.Parameters.AddWithValue("@NguoiTao", Item.NguoiTao);
                    cmd.Parameters.AddWithValue("@NguoiCapNhat", Item.NguoiCapNhat);

                    cmd.Parameters.AddWithValue("@ThoiGianChuan", Math.Round(Item.TongThoiGianGiay + Item.ThoiGianChuanBi, 5));

                    cmd.Parameters.AddWithValue("@TypeThaoTac", TypeThaoTac);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsCongDoan.Update: " + ex.Message);
            }
        }
    }
}
