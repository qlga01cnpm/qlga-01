﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    public class clsBacTho
    {
        public System.Data.DataTable TYPE_SYS_USER_MENU
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_DIC_BACTHO");
                _tbl.Columns.Add("MaBacTho", typeof(System.String));
                _tbl.Columns.Add("MoTa", typeof(System.String));
                _tbl.Columns.Add("HeSo", typeof(float));

                _tbl.PrimaryKey = new System.Data.DataColumn[] { _tbl.Columns["MaBacTho"] };
                return _tbl;
            }
        }

        public Entity.clsBacThoCollection GetAll()
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    Entity.clsTreeDepCollection _objCollection = new Entity.clsTreeDepCollection();
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_Select", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Columns", "*");
                    _cmd.Parameters.AddWithValue("@TableName", "dbo.DIC_BACTHO");
                    _cmd.Parameters.AddWithValue("@WhereClause", " ");

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    _adapter.Fill(tbl);
                    List<Entity.clsBacThoInfo> arrayTemp = new List<Entity.clsBacThoInfo>();
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        arrayTemp = tbl.AsEnumerable().Select(m => new Entity.clsBacThoInfo()
                            {
                                MaBacTho = m.Field<string>("MaBacTho"),
                                MoTa = m.Field<string>("MoTa"),
                                HeSo = float.Parse(m["HeSo"].ToString()),
                            }).ToList();
                    }
                    _adapter.Dispose();
                    tbl.Dispose();

                    return new Entity.clsBacThoCollection(arrayTemp);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsBacTho.GetAll: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsBacThoCollection Items)
        {
            try
            {
                System.Data.DataTable _TYPE = this.TYPE_SYS_USER_MENU;
                foreach (var item in Items)
                {
                    if (item.MaBacTho.Length > 10)
                        throw new Exception("Mã bậc thợ vượt quá 10 ký tự");
                    System.Data.DataRow newRow = _TYPE.NewRow();
                    newRow["MaBacTho"] = item.MaBacTho;
                    newRow["MoTa"] = item.MoTa;
                    newRow["HeSo"] = item.HeSo;
                    _TYPE.Rows.Add(newRow);
                }
                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_BACTHO", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action",Libs.ApiAction.POST.ToString());
                    cmd.Parameters.AddWithValue("@TYPE_DIC_BACTHO", _TYPE);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsBacTho.Update: " + ex.Message);
            }
        }

        public bool Delete(string MaBacTho)
        {
            try
            {
                System.Data.DataTable _Type = this.TYPE_SYS_USER_MENU;
                DataRow row = _Type.NewRow();
                row["MaBacTho"] = MaBacTho;
                row["MoTa"] = "";
                row["HeSo"] = 0;
                _Type.Rows.Add(row);

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_DIC_BACTHO", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.DELETE.ToString());
                    cmd.Parameters.AddWithValue("@TYPE_DIC_BACTHO", _Type);

                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsBacTho.Update: " + ex.Message);
            }
        }
    }
}
