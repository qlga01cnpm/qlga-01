﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public abstract class BaseBus<T>
    {
        public virtual void BatchDelete(Action<IEnumerable<T>, Exception> Complete) { }
        public virtual void BatchInsert(Action<IEnumerable<T>, Exception> Complete) { }
        public virtual void BatchUpdate(Action<IEnumerable<T>, Exception> Complete) { }
        public virtual void Delete(object Id, Action<Exception> Complete) { }
        public virtual void GetBy(object id, Action<IEnumerable<T>,Exception> Complete) { }
        public virtual void GetAll(Action<IEnumerable<T>,Exception> Complete) { }
        public virtual void Insert(T item, Action<Exception> Complete) { }
        public virtual void Update(T item, Action<Exception> Complete) { }

        protected virtual void GetPaging(int selectedPage, int pageSize, out int totalPage, out int totalRecords, Action<Exception> Complete)
        {
            totalPage = 0; totalRecords = 0;
        }

        protected virtual Interfaces.IResultOk ValidateInsert(T Item)
        {
            return new BaseResultOk();
        }
        protected virtual Interfaces.IResultOk ValidateUpdate(T Item)
        {
            return new BaseResultOk();
        }
        protected virtual Interfaces.IResultOk ValidateDelete(T Item)
        {
            return new BaseResultOk();
        }
    }
}
