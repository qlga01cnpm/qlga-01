﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using NtbSoft.ERP.Entity;
using NtbSoft.ERP.Libs;

namespace NtbSoft.ERP.Models
{
    
    public class clsUserBS:MarshalByRefObject
    {
        /// <summary>
        /// Check Login user
        /// </summary>
        /// <param name="sUserName">User Name</param>
        /// <param name="sPassword">Password</param>
        /// <param name="status">Status of this User</param>
        /// <returns></returns>
        public clsUserInfo CheckUser(string sLoginID, string sPassword, ref UserStatus status)
        {
            // DateTime dtmNow = clsCommonBS.GetServerTime();
            try
            {
                DateTime dtmNow = DateTime.Now;
            clsUserInfo objUser = LoginUserByID(sLoginID);
            if (objUser != null)
            {
                if (Libs.MD5Password.verifyMd5Hash(sPassword, objUser.Password) || 
                    Libs.MD5Password.CompareMd5(sPassword, objUser.Password))
                {
                    //Check this user belongs to Admin Group ?
                    if (IsAdminUser(sLoginID))
                        status = UserStatus.OK;
                    else
                    {
                        status = UserStatus.OK;
                        /*** 
                        if (!objUser.LockedUser)
                        {
                            if (objUser.DeadlineOfUsing.Date >= dtmNow.Date)
                                status = UserStatus.OK;
                            else
                                status = UserStatus.ExpiredDate;
                        }
                        else
                            status = UserStatus.Locked;
                         ***/
                    }
                }
                else
                    status = UserStatus.NotExists;
            }
            else
                status = UserStatus.NotExists;

            return objUser;
            }
            catch (Exception ex)
            {
                Libs.clsCommon.WriteLogAspNet("Error CheckUser.", ex.Message);
                return new clsUserInfo();
            }
        }


        /// <summary>
        /// Check login user belongs to Admin Group ?
        /// </summary>
        /// <param name="sUserName">User Name</param>
        /// <returns>Admin Group or not</returns>
        private bool IsAdminUser(string sLoginID)
        {
            clsGroupCollection groupCollection = new clsGroupUserBS().GetGroupCollectionByUserID(sLoginID);
            foreach (clsGroupInfo objGroup in groupCollection)
            {
                if (objGroup.IsAdmin)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// GetListUser
        /// </summary>
        /// <returns></returns>
        public clsUserCollection GetListUser()
        {
            clsUserCollection userCollection = new clsUserCollection();
            System.Data.SqlClient.SqlDataReader reader = null;
            try
            {
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    if (_cnn.State == System.Data.ConnectionState.Closed)
                        _cnn.Open();
                    SqlCommand _cmd = new SqlCommand("select * from SYS_USERS", _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;
                    reader = _cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        clsUserInfo objUser = new clsUserInfo();
                        objUser.UserID = reader["UserID"].ToString();
                        objUser.FullName = reader["LastName"].ToString() + " " + reader["FirstName"].ToString();
                        objUser.LastName = reader["LastName"].ToString();
                        objUser.FirstName = reader["FirstName"].ToString();

                        objUser.Password = reader["Password"].ToString();
                        objUser.Email = reader["Email"].ToString();
                        objUser.IsAdmin = (bool)reader["IsAdmin"];
                        objUser.IsLock = (bool)reader["IsLock"];
                        objUser.Active = (bool)reader["Active"];
                        if (!Convert.IsDBNull(reader["LastView"]))
                            objUser.LastView = (DateTime)reader["LastView"];
                        if (!Convert.IsDBNull(reader["LastAccess"]))
                            objUser.LastAccess = (DateTime)reader["LastAccess"];
                        objUser.CountView = (int)reader["CountView"];

                        userCollection.Add(objUser);
                    }

                    //Call Close when reading done.
                    reader.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally { if (reader != null)reader.Dispose(); }

            return userCollection;
        }


        /// <summary>
        /// Get: Người dùng ngoài nhóm
        /// </summary>
        /// <returns></returns>
        public clsUserCollection GetOuterGroup(string GroupID)
        {
            clsUserCollection userCollection = new clsUserCollection();
            System.Data.SqlClient.SqlDataReader reader = null;
            try
            {
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    if (_cnn.State == System.Data.ConnectionState.Closed)
                        _cnn.Open();
                    string _Query = string.Format("select t1.* From dbo.SYS_USERS t1 where t1.UserID not in(select t2.UserID from dbo.SYS_USER_GROUP t2 where t2.GroupID='{0}')", GroupID);
                    SqlCommand _cmd = new SqlCommand(_Query, _cnn);
                    _cmd.CommandType = System.Data.CommandType.Text;
                    reader = _cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        clsUserInfo objUser = new clsUserInfo();
                        objUser.UserID = reader["UserID"].ToString();
                        objUser.FullName = reader["LastName"].ToString() + " " + reader["FirstName"].ToString();
                        objUser.LastName = reader["LastName"].ToString();
                        objUser.FirstName = reader["FirstName"].ToString();

                        userCollection.Add(objUser);
                    }

                    //Call Close when reading done.
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
            }
            finally { if (reader != null)reader.Dispose(); }

            return userCollection;
        }

        /// <summary>
        /// Get: Người dùng thuộc nhóm
        /// </summary>
        /// <returns></returns>
        public clsUserCollection GetInnerGroup(string GroupID)
        {
            clsUserCollection userCollection = new clsUserCollection();
            System.Data.SqlClient.SqlDataReader reader = null;
            try
            {
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
            {
                if (_cnn.State == System.Data.ConnectionState.Closed)
                    _cnn.Open();
                string _Query = string.Format("select t1.* from SYS_USERS t1 inner join dbo.SYS_USER_GROUP t2 on t1.UserID=t2.UserID and t2.GroupID='{0}'", GroupID);
                SqlCommand _cmd = new SqlCommand(_Query, _cnn);
                _cmd.CommandType = System.Data.CommandType.Text;
                reader = _cmd.ExecuteReader();

                while (reader.Read())
                {
                    clsUserInfo objUser = new clsUserInfo();
                    objUser.UserID = reader["UserID"].ToString();
                    objUser.FullName = reader["LastName"].ToString() + " " + reader["FirstName"].ToString();
                    objUser.LastName = reader["LastName"].ToString();
                    objUser.FirstName = reader["FirstName"].ToString();

                    userCollection.Add(objUser);
                }

                //Call Close when reading done.
                reader.Close();
            }
            }
            catch (Exception ex)
            {
            }
            finally { if (reader != null)reader.Dispose(); }

            return userCollection;
        }

        // <summary>
        /// GetUserByID
        /// </summary>
        /// <param name="sUserName"></param>
        /// <returns></returns>
        public clsUserInfo LoginUserByID(string sLoginID)
        {
            try
            {
                clsUserInfo objUser = new clsUserInfo();
                using (SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    SqlCommand _cmd = new SqlCommand("sp_SYS_USER_LOGIN", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    _cmd.Parameters.AddWithValue("@UserId", sLoginID);
                    _cmd.CommandTimeout = 2000;

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        lock (this)
                        {
                            if (reader.Read())
                            {
                                objUser.UserID = reader["UserID"].ToString();
                                objUser.FullName = reader["LastName"].ToString() + " " + reader["FirstName"].ToString();
                                objUser.Password = reader["Password"].ToString();

                                objUser.Email = reader["Email"].ToString();
                                objUser.IsAdmin = (bool)reader["IsAdmin"];
                                objUser.IsLock = (bool)reader["IsLock"];
                                objUser.Active = (bool)reader["Active"];
                                if (!Convert.IsDBNull(reader["LastView"]))
                                    objUser.LastView = (DateTime)reader["LastView"];
                                if (!Convert.IsDBNull(reader["LastAccess"]))
                                    objUser.LastAccess = (DateTime)reader["LastAccess"];
                                objUser.CountView = (int)reader["CountView"];

                            }
                        }
                    }
                    //Call Close when reading done.
                }
                return objUser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool SetPassword(string UserID,string Password,string RePassword)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_SYS_USER_SET_PASSWORD", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", UserID);
                    cmd.Parameters.AddWithValue("@Password", Libs.MD5Password.getMd5Hash(Password));
                    cmd.Parameters.AddWithValue("@RePassword", Libs.MD5Password.getMd5Hash(RePassword));

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsUserBS.SetPassword: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool ChangePassword(string UserID, string Password,string newPassword, string RePassword)
        {
            try
            {
                if (UserID == "admin") return true;
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("dbo.sp_SYS_USER_CHANGE_PASSWORD", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", UserID);
                    cmd.Parameters.AddWithValue("@Password", Libs.MD5Password.getMd5Hash(Password));
                    cmd.Parameters.AddWithValue("@NewPassword", Libs.MD5Password.getMd5Hash(newPassword));
                    cmd.Parameters.AddWithValue("@RePassword", Libs.MD5Password.getMd5Hash(RePassword));

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsUserBS.ChangePassword: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Delete(string UserID, System.Data.DataTable TypeSysLog)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_SYS_USER_DEL", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", UserID);
                    cmd.Parameters.AddWithValue("@TypeSysLog", TypeSysLog);

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Delete User: " + ex.Message);
            }
            finally
            {
            }
        }


        public bool DeleteGroup(string GroupID, System.Data.DataTable TypeSysLog)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_SYS_GROUP_DEL", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@GroupID", GroupID);
                    cmd.Parameters.AddWithValue("@TypeSysLog", TypeSysLog);

                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Delete Group: " + ex.Message);
            }
            finally
            {
            }
        }
    }
}
