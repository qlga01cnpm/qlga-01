﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsSysLog
    {
        /// <summary>
        /// Get Structure
        /// </summary>
        private static System.Data.DataTable TypeSysLogs
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TypeSysLog");
                _tbl.Columns.Add("SYS_ID", typeof(System.Double));
                _tbl.Columns.Add("MChine", typeof(System.String));
                _tbl.Columns.Add("AccountWin", typeof(System.String));
                _tbl.Columns.Add("UserID", typeof(System.String));
                _tbl.Columns.Add("UserName", typeof(System.String));
                _tbl.Columns.Add("Created", typeof(DateTime));
                _tbl.Columns.Add("Module", typeof(System.String));
                _tbl.Columns.Add("Action", typeof(System.Int32));
                _tbl.Columns.Add("Action_Name", typeof(System.String));
                _tbl.Columns.Add("Reference", typeof(System.String));

                _tbl.Columns.Add("IPLan", typeof(System.String));
                _tbl.Columns.Add("IPWan", typeof(System.String));
                _tbl.Columns.Add("Mac", typeof(System.String));
                _tbl.Columns.Add("DeviceName", typeof(System.String));
                _tbl.Columns.Add("Description", typeof(System.String));
                _tbl.Columns.Add("JsonObject", typeof(System.String));
                _tbl.Columns.Add("Active", typeof(System.Boolean));

                _tbl.PrimaryKey = new System.Data.DataColumn[] { 
                    _tbl.Columns["LineX"],_tbl.Columns["SYS_ID"]
                };
                return _tbl;
            }
        }

        /// <summary>
        /// Get log Table TypeSysLog
        /// </summary>
        /// <param name="MChine"></param>
        /// <param name="UserName"></param>
        /// <param name="IPWan"></param>
        /// <param name="DeviceName"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
        public static System.Data.DataTable LogSystem(string MChine, string UserName, string IPWan, string DeviceName, string Description, string JsonObject)
        {
            System.Data.DataTable _tbl_SysLog = TypeSysLogs;

            System.Data.DataRow _NewRow = _tbl_SysLog.NewRow();
            _NewRow["SYS_ID"] = 0;
            _NewRow["MChine"] = MChine;
            _NewRow["UserName"] = UserName;
            _NewRow["IPLan"] = Libs.clsCommon.LocalIPAddress();
            _NewRow["IPWan"] = IPWan;
            _NewRow["Mac"] = Libs.clsGenerateUniqueID.macId();
            _NewRow["DeviceName"] = DeviceName;
            _NewRow["Description"] = Description;
            _NewRow["JsonObject"] = JsonObject;

            _tbl_SysLog.Rows.Add(_NewRow);
            return _tbl_SysLog;
        }


        public static System.Data.DataTable LogSystem(string MChine, string UserName, string Action_Name, string IPWan, string DeviceName, string Description, string JsonObject)
        {
            System.Data.DataTable _tbl_SysLog = TypeSysLogs;

            System.Data.DataRow _NewRow = _tbl_SysLog.NewRow();
            _NewRow["SYS_ID"] = 0;
            _NewRow["MChine"] = MChine;
            _NewRow["UserName"] = UserName;
            _NewRow["IPLan"] = Libs.clsCommon.LocalIPAddress();
            _NewRow["IPWan"] = IPWan;
            _NewRow["Mac"] = Libs.clsGenerateUniqueID.macId();
            _NewRow["Action_Name"] = Action_Name;
            _NewRow["DeviceName"] = DeviceName;
            _NewRow["Description"] = Description;
            _NewRow["JsonObject"] = JsonObject;

            _tbl_SysLog.Rows.Add(_NewRow);
            return _tbl_SysLog;
        }

        /// <summary>
        /// Get from Database
        /// </summary>
        /// <param name="Machine"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static System.Data.DataTable GetEventLog(string Machine, DateTime dt)
        {
            SqlConnection _cnn = null;
            try
            {
                string whereClause = "";
                whereClause = string.Format("Where MChine='{0}' and convert(date,Created,121)='{1}' order by Created desc",
                    Machine, dt.ToString(Libs.clsConsts.FormatYMD));

                _cnn = Libs.SqlHelper.GetConnection();
                SqlCommand cmd = new SqlCommand("sp_Select", _cnn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;


                cmd.Parameters.AddWithValue("@Columns", "*,CONVERT(VARCHAR(10), Created, 103) + ' ' + CONVERT(VARCHAR(8), Created, 108) as StrCreated");
                cmd.Parameters.AddWithValue("@TableName", "tbl_Sys_Logs");
                cmd.Parameters.AddWithValue("@WhereClause", whereClause);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                System.Data.DataTable tbl = new System.Data.DataTable();
                adapter.Fill(tbl);

                return tbl;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("GetEventLog Error: " + ex.Message);
            }
            finally
            {
                if (_cnn != null) { _cnn.Close(); _cnn.Dispose(); }
            }
        }

        public static bool WriteEventLog(string MChine, string UserName, string IPWan, string DeviceName, string Description, string JsonObject)
        {
            try
            {
                System.Data.DataTable tbl_Log = Models.clsSysLog.LogSystem(MChine, UserName, IPWan, DeviceName, Description, JsonObject);
                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    SqlCommand cmd = new SqlCommand("sp_EventLogWrite", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TypeSysLog", tbl_Log);
                    
                    /*** ***/
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsSysLog.WriteEventLog: " + ex.Message);
            }
        }
    }
}
