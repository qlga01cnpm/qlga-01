﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NtbSoft.ERP.Models
{
    public class clsGTDMaHang
    {
        public System.Data.DataTable TYPE_GTD_THAOTACMAHANG
        {
            get
            {
                System.Data.DataTable _tbl = new System.Data.DataTable("TYPE_GTD_THAOTACMAHANG");
                _tbl.Columns.Add("ThaoTacMaHangID", typeof(double));
                _tbl.Columns.Add("Id_DmMaHang", typeof(System.String));
                _tbl.Columns.Add("Id_DmCD", typeof(System.String));
                _tbl.Columns.Add("NhomCongDoanID", typeof(double));
                _tbl.Columns.Add("ThuTu", typeof(int));
                _tbl.Columns.Add("SoLanXuatHien", typeof(float));

                return _tbl;
            }
        }

        public Entity.clsGTDMaHangInfo GetBy(string MenuId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_GTD_MAHANG_GET_BY", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.GET.ToString());
                    _cmd.Parameters.AddWithValue("@MenuId", MenuId);

                    System.Data.SqlClient.SqlDataAdapter _adapter = new System.Data.SqlClient.SqlDataAdapter(_cmd);
                    System.Data.DataSet ds = new System.Data.DataSet();
                    _adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0) return new Entity.clsGTDMaHangInfo();

                    #region Hàm trả về danh sách các thao tác
                    Func<string, Entity.clsGTDThaoTacMaHangCollection> GetThaoTac = delegate(string Id_Dm)
                    {
                        if (ds.Tables[1].Rows.Count == 0) return new Entity.clsGTDThaoTacMaHangCollection();
                        string _Expression = string.Format("Id_DmMaHang='{0}'", Id_Dm);
                        var items = ds.Tables[1].Select(_Expression).AsEnumerable().Select(n => new Entity.clsGTDThaoTacMaHangInfo()
                        {
                            ThaoTacMaHangID = Convert.ToDouble(n["ThaoTacMaHangID"]),
                            Id_DmMaHang = n["Id_DmMaHang"].ToString(),
                            Id_Dm = n["Id_DmCD"].ToString(),
                            ThuTu = Convert.ToInt32(n["ThuTu"]),
                            SoLanXuatHien = (float)Convert.ToDouble(n["SoLanXuatHien"]),
                            NhomCongDoanID = n.IsNull("NhomCongDoanID") ? 0 : Convert.ToDouble(n["NhomCongDoanID"]),

                            MaSo = n.Field<string>("MaSo"),
                            TenCongDoan = n.Field<string>("TenCongDoan"),
                            ChiPhiCongDoan = (float)Convert.ToDouble(n["ChiPhiCongDoan"]),
                            TongThoiGian = (float)Convert.ToDouble(n["TongThoiGian"]),

                            MaThietBi = n.Field<string>("MaThietBi"),
                            MaBacTho = n.Field<string>("MaBacTho"),
                            HeSo = Convert.ToSingle(n["HeSo"]),
                            ThoiGianChuan = Convert.ToSingle(n["ThoiGianChuan"])
                        }).ToList();

                        if (items == null) return new Entity.clsGTDThaoTacMaHangCollection();

                        return new Entity.clsGTDThaoTacMaHangCollection(items);
                    };
                    #endregion

                    List<Entity.clsGTDMaHangInfo> arrayTemp = new List<Entity.clsGTDMaHangInfo>();
                    arrayTemp = ds.Tables[0].AsEnumerable().Select(m => new Entity.clsGTDMaHangInfo()
                    {
                        Id_Dm = m.Field<string>("Id_Dm"),
                        MenuId = m.Field<string>("MenuId"),
                        MaSo = m.Field<string>("MaSo"),
                        KhachHang = m.Field<string>("KhachHang"),
                        TenMaHang = m.Field<string>("TenMaHang"),
                        SoPhienBan = Convert.ToDouble(m["SoPhienBan"]),
                        TrangThai = m.Field<int>("TrangThai"),
                        DangKhoa = m.Field<bool>("DangKhoa"),
                        MoTa = m.Field<string>("MoTa"),
                        TiGia = (float)Convert.ToDouble(m["TiGia"]),

                        ChiPhiKhauCat = (float)Convert.ToDouble(m["ChiPhiKhauCat"]),
                        ChiPhiThietBi = (float)Convert.ToDouble(m["ChiPhiThietBi"]),
                        ChiPhiKiemTra = (float)Convert.ToDouble(m["ChiPhiKiemTra"]),
                        ChiPhiKhauUi = (float)Convert.ToDouble(m["ChiPhiKhauUi"]),
                        ChiPhiKhauDongGoi = (float)Convert.ToDouble(m["ChiPhiKhauDongGoi"]),
                        ChiPhiGiaCongNgoai = (float)Convert.ToDouble(m["ChiPhiGiaCongNgoai"]),
                        DonGiaGiaCongNgoai = (float)Convert.ToDouble(m["DonGiaGiaCongNgoai"]),
                        ChiPhiSanXuatTrucTiep = (float)Convert.ToDouble(m["ChiPhiSanXuatTrucTiep"]),
                        ChiPhiQuanLyTrucTiep = (float)Convert.ToDouble(m["ChiPhiQuanLyTrucTiep"]),
                        ChiPhiVai = (float)Convert.ToDouble(m["ChiPhiVai"]),
                        ChiPhiPhuLieu = (float)Convert.ToDouble(m["ChiPhiPhuLieu"]),
                        ChiPhiChi = (float)Convert.ToDouble(m["ChiPhiChi"]),
                        ChiPhiPhuKien = (float)Convert.ToDouble(m["ChiPhiPhuKien"]),
                        ChiPhiBaoBi = (float)Convert.ToDouble(m["ChiPhiBaoBi"]),
                        ChiPhiGoc = (float)Convert.ToDouble(m["ChiPhiGoc"]),
                        ChiPhiBanHang = (float)Convert.ToDouble(m["ChiPhiBanHang"]),

                        ChiPhiHanNgach = (float)Convert.ToDouble(m["ChiPhiHanNgach"]),
                        MaChuyenTo = m.Field<string>("MaChuyenTo"),
                        DinhMucNangSuat = (float)Convert.ToDouble(m["DinhMucNangSuat"]),
                        TongSoCongNhan = m.Field<int>("TongSoCongNhan"),

                        ChiPhiMaHang = (float)Convert.ToDouble(m["ChiPhiMaHang"]),
                        TongThoiGian = (float)Convert.ToDouble(m["TongThoiGian"]),
                        NguoiTao = m.Field<string>("NguoiTao"),
                        NguoiCapNhat = m.Field<string>("NguoiCapNhat"),
                        NgayCapNhat = m.Field<DateTime>("NgayCapNhat"),
                        ThaoTacMaHangCollection = GetThaoTac(m.Field<string>("Id_Dm"))

                       

                    }).ToList();

                    _adapter.Dispose();
                    ds.Dispose();

                    return arrayTemp[0];
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsGTDMaHang.GetBy: " + ex.Message);
            }
            finally
            {
            }
        }

        public bool Update(Entity.clsGTDMaHangInfo Item)
        {
            if (Item == null) return false;
            try
            {
                System.Data.DataTable TypeThaoTac = this.TYPE_GTD_THAOTACMAHANG;
                if (Item.ThaoTacMaHangCollection != null)
                    foreach (var itemThaoTac in Item.ThaoTacMaHangCollection)
                    {
                        System.Data.DataRow newRow = TypeThaoTac.NewRow();
                        newRow["ThaoTacMaHangID"] = itemThaoTac.ThaoTacMaHangID;
                        newRow["Id_DmMaHang"] = Item.Id_Dm == null ? "" : Item.Id_Dm;
                        newRow["Id_DmCD"] = itemThaoTac.Id_Dm == null ? "" : itemThaoTac.Id_Dm;
                        newRow["NhomCongDoanID"] = itemThaoTac.NhomCongDoanID;
                        newRow["ThuTu"] = itemThaoTac.ThuTu;
                        newRow["SoLanXuatHien"] = itemThaoTac.SoLanXuatHien;
                        
                        TypeThaoTac.Rows.Add(newRow);
                    }

                using (System.Data.SqlClient.SqlConnection _cnn = NtbSoft.ERP.Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_GTD_MAHANG", _cnn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Action", Libs.ApiAction.POST.ToString());

                    cmd.Parameters.AddWithValue("@Id_Dm", Item.Id_Dm == null ? "00" : Item.Id_Dm);
                    cmd.Parameters.AddWithValue("@MenuId", Item.MenuId);
                    cmd.Parameters.AddWithValue("@MaSo", Item.MaSo == null ? "" : Item.MaSo);
                    cmd.Parameters.AddWithValue("@KhachHang", Item.KhachHang == null ? "" : Item.KhachHang);
                    cmd.Parameters.AddWithValue("@TenMaHang", Item.TenMaHang==null?"":Item.TenMaHang);
                    cmd.Parameters.AddWithValue("@SoPhienBan", Item.SoPhienBan);
                    cmd.Parameters.AddWithValue("@TrangThai", Item.TrangThai);
                    cmd.Parameters.AddWithValue("@DangKhoa", Item.DangKhoa);
                    cmd.Parameters.AddWithValue("@MoTa", Item.MoTa == null ? "" : Item.MoTa);
                    cmd.Parameters.AddWithValue("@MaTienTe", Item.MaTienTe == null ? "" : Item.MaTienTe);

                    cmd.Parameters.AddWithValue("@TiGia", Item.TiGia);
                    cmd.Parameters.AddWithValue("@ChiPhiKhauCat", Item.ChiPhiKhauCat);
                    cmd.Parameters.AddWithValue("@ChiPhiThietBi", Item.ChiPhiThietBi);
                    cmd.Parameters.AddWithValue("@ChiPhiKiemTra", Item.ChiPhiKiemTra);
                    cmd.Parameters.AddWithValue("@ChiPhiKhauUi", Item.ChiPhiKhauUi);
                    cmd.Parameters.AddWithValue("@ChiPhiKhauDongGoi", Item.ChiPhiKhauDongGoi);
                    cmd.Parameters.AddWithValue("@ChiPhiGiaCongNgoai", Item.ChiPhiGiaCongNgoai);
                    cmd.Parameters.AddWithValue("@DonGiaGiaCongNgoai", Item.DonGiaGiaCongNgoai);
                    cmd.Parameters.AddWithValue("@ChiPhiSanXuatTrucTiep", Item.ChiPhiSanXuatTrucTiep);
                    cmd.Parameters.AddWithValue("@ChiPhiQuanLyTrucTiep", Item.ChiPhiQuanLyTrucTiep);

                    cmd.Parameters.AddWithValue("@ChiPhiVai", Item.ChiPhiVai);
                    cmd.Parameters.AddWithValue("@ChiPhiPhuLieu", Item.ChiPhiPhuLieu);
                    cmd.Parameters.AddWithValue("@ChiPhiChi", Item.ChiPhiChi);
                    cmd.Parameters.AddWithValue("@ChiPhiPhuKien", Item.ChiPhiPhuKien);
                    cmd.Parameters.AddWithValue("@ChiPhiBaoBi", Item.ChiPhiBaoBi);
                    cmd.Parameters.AddWithValue("@ChiPhiGoc", Item.ChiPhiGoc);
                    cmd.Parameters.AddWithValue("@ChiPhiBanHang", Item.ChiPhiBanHang);
                    cmd.Parameters.AddWithValue("@ChiPhiHanNgach", Item.ChiPhiHanNgach);
                    cmd.Parameters.AddWithValue("@MaChuyenTo", Item.MaChuyenTo == null ? "" : Item.MaChuyenTo);
                    cmd.Parameters.AddWithValue("@DinhMucNangSuat", Item.DinhMucNangSuat);

                    cmd.Parameters.AddWithValue("@TongSoCongNhan", Item.TongSoCongNhan);
                    cmd.Parameters.AddWithValue("@ChiPhiMaHang", Item.ChiPhiMaHang);
                    cmd.Parameters.AddWithValue("@TongThoiGian", Item.TongThoiGian);
                    cmd.Parameters.AddWithValue("@NguoiTao", Item.NguoiTao);
                    cmd.Parameters.AddWithValue("@NguoiCapNhat", Item.NguoiCapNhat);

                    cmd.Parameters.AddWithValue("@TypeThaoTac", TypeThaoTac);
                    int kq = cmd.ExecuteNonQuery();
                    return kq > 0;

                    //return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("clsGTDMaHang.Update: " + ex.Message);
            }
        }
    }
}
