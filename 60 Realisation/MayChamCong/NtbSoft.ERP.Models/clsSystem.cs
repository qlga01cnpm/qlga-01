﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Models
{
    public class clsSystem
    {
        /// <summary>
        /// codeF: ma code max 3 ký tự
        /// dataSource: tên table
        /// </summary>
        /// <param name="codeF"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static string GetIdDm(string codeF, string dataSource)
        {
            string id_dm = "";
            try
            {
                using (System.Data.SqlClient.SqlConnection _cnn = Libs.SqlHelper.GetConnection())
                {
                    System.Data.SqlClient.SqlCommand _cmd = new System.Data.SqlClient.SqlCommand("sp_SYS_GETSTT_RECDM", _cnn);
                    _cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    _cmd.Parameters.AddWithValue("@codeF", codeF);
                    _cmd.Parameters.AddWithValue("@dataSource", dataSource);

                    _cmd.Parameters.Add("@valRef", System.Data.SqlDbType.NVarChar, 150);
                    _cmd.Parameters["@valRef"].Direction = System.Data.ParameterDirection.Output;
                    _cmd.ExecuteNonQuery();
                    if (_cmd.Parameters["@valRef"].Value != null)
                        id_dm = _cmd.Parameters["@valRef"].Value.ToString();

                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new Exception("clsSystem.GetIdDm: " + ex.Message);
            }
            finally
            {
            }

            return id_dm;
        }
    }
}
