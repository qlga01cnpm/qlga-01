﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC
{
    public class ConnectCOM:Dal.IConnect
    {
        internal zkemkeeper.IZKEM _axCZKEM1;
        private int _MachineNumber = 0;
        public ConnectCOM(int MachineNumber)
        {
            this._axCZKEM1 = new zkemkeeper.CZKEMClass();
            this._MachineNumber = MachineNumber;
        }
        public void Connect(string COM, int BaudRate, Action<zkemkeeper.IZKEM, Exception> Complete)
        {
            int idwErrorCode = 0;
            Exception _ex = null;
            int iPort;
            //accept serialport number from string like "COMi"
            string sPort = COM.Trim();
            for (iPort = 1; iPort < 10; iPort++)
                if (sPort.IndexOf(iPort.ToString()) > -1)
                    break;
            if (this._axCZKEM1.Connect_Com(iPort, this._MachineNumber, BaudRate))
                this._axCZKEM1.RegEvent(this._MachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            else
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString() + ".Error");
            }
            if (Complete != null) Complete(this._axCZKEM1,_ex);
        }
    }
}
