﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.Devices.MCC.Models
{
    public class clsDeviceInfo:IDeviceInfo
    {
        public string FirmwareVersion { get; set; }

        public string MAC { get; set; }

        public string SerialNumber { get; set; }

        public string ProductCode { get; set; }

        public string Platform { get; set; }

        public string DeviceIP { get; set; }

        public string SDKVersion { get; set; }

        public string Vendor { get; set; }

        public int DataCount { get; set; }

        public int CountAdmin { get; set; }
        public int CountUser { get; set; }
        public int SoBanGhi { get; set; }
    }
}
