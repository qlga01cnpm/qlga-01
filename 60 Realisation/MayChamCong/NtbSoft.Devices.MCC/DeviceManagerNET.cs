﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC
{
    public class DeviceManagerNET : Dal.IDeviceManager
    {
        internal zkemkeeper.IZKEM _axCZKEM1;
        private int _MachineNumber = 0;
        public DeviceManagerNET(int MachineNumber)
            
        {
            this._axCZKEM1 = new zkemkeeper.CZKEMClass();
            this._MachineNumber = MachineNumber;
        }

        public void ConnectNet(string IPAdd, int Port, Action<bool, Exception> Complete)
        {
            int idwErrorCode = 0;
            bool Connected = false;
            Exception _ex = null;
            Connected = this._axCZKEM1.Connect_Net(IPAdd, Port);
            if (Connected)
                this._axCZKEM1.RegEvent(this._MachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            else
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString() + ".Error");
            }
            if (Complete != null) Complete(Connected, _ex);
        }

        public virtual void GetFirmwareVersion(Action<int, Exception> Complete)
        {
            //_axCZKEM1.Connect_Com(ComPort,MachineNumber,BaudRate)
            string sVersion = "";
            int idwErrorCode = 0;
            Exception _ex = null;
            if (!this._axCZKEM1.GetFirmwareVersion(_MachineNumber, ref sVersion))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(idwErrorCode, _ex);
        }

        public void GetDeviceMAC(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sMAC = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetDeviceMAC(this._MachineNumber, ref sMAC))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sMAC, _ex);
        }

        public void GetSerialNumber(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sdwSerialNumber = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetSerialNumber(this._MachineNumber, out sdwSerialNumber))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sdwSerialNumber, _ex);
        }

        public void GetProductCode(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sdwSerialNumber = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetProductCode(this._MachineNumber, out sdwSerialNumber))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sdwSerialNumber, _ex);
        }

        public void GetPlatform(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sPlatform = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetPlatform(this._MachineNumber, ref sPlatform))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sPlatform, _ex);
        }

        public void GetDeviceIP(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sIP = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetDeviceIP(this._MachineNumber, ref sIP))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sIP, _ex);
            //this._axCZKEM1.Connect_Com()

        }

        public void GetSDKVersion(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sVersion = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetSDKVersion(ref sVersion))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sVersion, _ex);
        }


        public void GetVendor(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sVendor = "";
            Exception _ex = null;
            if (!_axCZKEM1.GetVendor(ref sVendor))
            {
                _axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sVendor, _ex);
        }


        public void SetDeviceIP(string IPAddr, Action<Exception> Complete)
        {

        }

        public void GetDeviceInfo(Action<Dal.IDeviceInfo, Exception> Complete)
        {
            string sdwSerialNumber = "", sIP = "";
            Exception _ex = null;
            clsDeviceInfo Item = new clsDeviceInfo();
            if (this._axCZKEM1.GetSerialNumber(this._MachineNumber, out sdwSerialNumber))
                Item.SerialNumber = sdwSerialNumber;

            if (this._axCZKEM1.GetDeviceIP(this._MachineNumber, ref sIP))
                Item.DeviceIP = sIP;

            if (Complete != null) Complete(Item, _ex);
        }



        public void Connect(string IPAdd, int Port, Action<bool, Exception> Complete)
        {
            throw new NotImplementedException();
        }
    }
}
