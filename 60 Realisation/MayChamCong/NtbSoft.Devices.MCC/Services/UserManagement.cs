﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services
{
    /// <summary>
    /// Quản lý người dùng trên máy chấm công
    /// </summary>
    public class UserManagement:Interfaces.IUserManagement
    {
        Services.Interfaces.IBaseConnect _BaseConnect;
        private int _MachineNumber = 0;
        private zkemkeeper.IZKEM _axCZKEM1;
        private bool _IsConnected = false;
        internal bool _IsDownload = false;
        internal bool _IsUpload = false;
        public UserManagement(int MachineNumber, Services.Interfaces.IBaseConnect BaseConnect)
        {
            this._MachineNumber = MachineNumber;
            this._BaseConnect = BaseConnect;
        }

        public void Connect(Action<bool, Exception> Complete)
        {
            Exception _ex = null;
            _BaseConnect.Connect(this._MachineNumber, (result, connected, ex) =>
            {
                _IsConnected = connected;
                _ex = ex;
                if (ex == null)
                    this._axCZKEM1 = result;
            });
            if (Complete != null) Complete(_IsConnected, _ex);
        }

        /// <summary>
        /// Tải người dùng về máy tính
        /// </summary>
        /// <param name="Complete"></param>
        public void DownloadFPTmp(Action<List<Entities.Interfaces.MCC.IUserFingerprintInfo>, Exception> Complete)
        {
            if (this._IsDownload) return;
            this._IsDownload = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(null, new Exception("Please connect the device first"));
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => Download(Complete)).Wait();
            }
        }

        private void Download(Action<List<Entities.Interfaces.MCC.IUserFingerprintInfo>, Exception> Complete)
        {
            Exception _ex = null;
            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;

            int idwFingerIndex;
            string sTmpData = "";
            int iTmpLength = 0;
            int iFlag = 0;
            List<Entities.Interfaces.MCC.IUserFingerprintInfo> IUserCollection = new List<Entities.Interfaces.MCC.IUserFingerprintInfo>();
            
            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            this._axCZKEM1.ReadAllUserID(this._MachineNumber);//read all the user information to the memory
            this._axCZKEM1.ReadAllTemplate(this._MachineNumber);//read all the users' fingerprint templates to the memory
            while (this._axCZKEM1.SSR_GetAllUserInfo(this._MachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                string sCardnumber = "";
                Models.clsUserFingerDataInfo Item = new Models.clsUserFingerDataInfo();
                Item.UserId = sdwEnrollNumber;
                Item.UserName = sName;
                Item.Privilege = iPrivilege;
                Item.Password = sPassword;
                Item.Enabled = bEnabled;
                
                /*** Lấy mã thẻ ***/
                if (this._axCZKEM1.GetStrCardNumber(out sCardnumber))//get the card number from the memory
                    Item.CardID = sCardnumber;

                //Item.FingerIndex = idwFingerIndex;
                //Duyệt qua dấu vân tay của 10 ngón
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (this._axCZKEM1.GetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        if (idwFingerIndex == 0) Item.sTmpData0 = sTmpData;
                        else if (idwFingerIndex == 1) Item.sTmpData1 = sTmpData;
                        else if (idwFingerIndex == 2) Item.sTmpData2 = sTmpData;
                        else if (idwFingerIndex == 3) Item.sTmpData3 = sTmpData;
                        else if (idwFingerIndex == 4) Item.sTmpData4 = sTmpData;
                        else if (idwFingerIndex == 5) Item.sTmpData5 = sTmpData;
                        else if (idwFingerIndex == 6) Item.sTmpData6 = sTmpData;
                        else if (idwFingerIndex == 7) Item.sTmpData7 = sTmpData;
                        else if (idwFingerIndex == 8) Item.sTmpData8 = sTmpData;
                        else if (idwFingerIndex == 9) Item.sTmpData9 = sTmpData;
                    }
                }
                IUserCollection.Add(Item);
            }

            this._axCZKEM1.EnableDevice(this._MachineNumber, true);

            if (Complete != null) Complete(IUserCollection, _ex);
            this._IsDownload = false;
        }


        public void UploadFPTmp(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Exception> Complete)
        {
            if (this._IsUpload) return;
            this._IsUpload = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(new Exception("Please connect the device first"));
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => Upload(Items,Complete)).Wait();
            }
        }

        private void Upload(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Exception> Complete)
        {
            int idwErrorCode = 0;
            Exception _ex = null;
            string sdwEnrollNumber = "";
            string sName = "";
            //int idwFingerIndex = 0;
            string sTmpData = "";
            int iPrivilege = 0;
            string sPassword = "";
            int iFlag = 0;
            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            //Các bước Upload dữ liệu lên máy châm công
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].UserId == null) continue;
                sdwEnrollNumber = Items[i].UserId.Trim();
                sName = Items[i].UserName == null ? "" : Items[i].UserName.Trim();
                sTmpData = Items[i].TmpData == null ? "" : Items[i].TmpData;
                iPrivilege = Items[i].Privilege;
                sPassword = Items[i].Password;
                Items[i].Enabled = true;

                iFlag = Items[i].Flag;
                /*** Cài đặt mã thẻ (CardID) ***/
                this._axCZKEM1.SetStrCardNumber(Items[i].CardID);//Before you using function SetUserInfo,set the card number to make sure you can upload it to the device

                //User privilege. 0: common user, 1: enroller(ghi danh), 2: administrator, 3: super administrator
                /*** Cài đặt thông tin  ***/
                if (this._axCZKEM1.SSR_SetUserInfo(this._MachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, Items[i].Enabled))//upload user information to the device
                {
                    //*** Ngón thứ 0 ***/
                    if (Items[i].sTmpData0 == null) Items[i].sTmpData0 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 0, iFlag, Items[i].sTmpData0);//upload templates information to the device
                    //*** Ngón thứ 1 ***/
                    if (Items[i].sTmpData1 == null) Items[i].sTmpData1 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 1, iFlag, Items[i].sTmpData1);
                    //*** Ngón thứ 2 ***/
                    if (Items[i].sTmpData2 == null) Items[i].sTmpData2 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 2, iFlag, Items[i].sTmpData2);
                    //*** Ngón thứ 3 ***/
                    if (Items[i].sTmpData3 == null) Items[i].sTmpData3 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 3, iFlag, Items[i].sTmpData3);
                    //*** Ngón thứ 4 ***/
                    if (Items[i].sTmpData4 == null) Items[i].sTmpData4 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 4, iFlag, Items[i].sTmpData4);
                    //*** Ngón thứ 5 ***/
                    if (Items[i].sTmpData5 == null) Items[i].sTmpData5 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 5, iFlag, Items[i].sTmpData5);
                    //*** Ngón thứ 6 ***/
                    if (Items[i].sTmpData6 == null) Items[i].sTmpData6 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 6, iFlag, Items[i].sTmpData6);
                    //*** Ngón thứ 7 ***/
                    if (Items[i].sTmpData7 == null) Items[i].sTmpData7 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 7, iFlag, Items[i].sTmpData7);
                    //*** Ngón thứ 8 ***/
                    if (Items[i].sTmpData8 == null) Items[i].sTmpData8 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 8, iFlag, Items[i].sTmpData8);
                    //*** Ngón thứ 9 ***/
                    if (Items[i].sTmpData9 == null) Items[i].sTmpData9 = "";
                    this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 9, iFlag, Items[i].sTmpData9);
                }
                else
                {
                    this._axCZKEM1.GetLastError(ref idwErrorCode);
                    _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
                    this._axCZKEM1.EnableDevice(this._MachineNumber, true);
                    break;
                }

                /*** khong can su du Cài đặt quyền quyền cho người dùng ***/
                //+++++++++++++this._axCZKEM1.SetUserInfo(this._MachineNumber,Convert.ToInt32(Items[i].UserId), Items[i].UserName, Items[i].Password, Items[i].Privilege, Items[i].Enabled);
            }

            this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            this._axCZKEM1.EnableDevice(this._MachineNumber, true);
            if (Complete != null) Complete(_ex);
            this._IsUpload = false;
        }

        #region Temps
        //private void UploadALL(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Exception> Complete)
        //{
        //    int idwErrorCode = 0;
        //    Exception _ex = null;
        //    string sdwEnrollNumber = "";
        //    string sName = "";
        //    //int idwFingerIndex = 0;
        //    string sTmpData = "";
        //    int iPrivilege = 0;
        //    string sPassword = "";
        //    int iFlag = 0;
        //    int iUpdateFlag = 1;
        //    this._axCZKEM1.EnableDevice(this._MachineNumber, false);
        //    //Các bước Upload dữ liệu lên máy châm công
        //    //1. Cho phép tải lên bộ nhớ tạm của máy chấm công
        //    if (this._axCZKEM1.BeginBatchUpdate(this._MachineNumber, iUpdateFlag))//create memory space for batching data
        //    {
        //        for (int i = 0; i < Items.Count; i++)
        //        {
        //            if (Items[i].UserId == null) continue;
        //            sdwEnrollNumber = Items[i].UserId.Trim();
        //            sName = Items[i].UserName.Trim();
        //            //idwFingerIndex = Items[i].FingerIndex;
        //            sTmpData = Items[i].TmpData == null ? "" : Items[i].TmpData;
        //            iPrivilege = Items[i].Privilege;
        //            sPassword = Items[i].Password;
        //            Items[i].Enabled = true;

        //            iFlag = Items[i].Flag;
        //            //iFlag = 1;
        //            if (this._axCZKEM1.SSR_SetUserInfo(this._MachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, Items[i].Enabled))//upload user information to the memory
        //            {
        //                //*** Ngón thứ 0 ***/
        //                if (Items[i].sTmpData0 == null) Items[i].sTmpData0 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 0, iFlag, Items[i].sTmpData0);//upload templates information to the memory
        //                //*** Ngón thứ 1 ***/
        //                if (Items[i].sTmpData1 == null) Items[i].sTmpData1 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 1, iFlag, Items[i].sTmpData1);
        //                //*** Ngón thứ 2 ***/
        //                if (Items[i].sTmpData2 == null) Items[i].sTmpData2 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 2, iFlag, Items[i].sTmpData2);
        //                //*** Ngón thứ 3 ***/
        //                if (Items[i].sTmpData3 == null) Items[i].sTmpData3 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 3, iFlag, Items[i].sTmpData3);
        //                //*** Ngón thứ 4 ***/
        //                if (Items[i].sTmpData4 == null) Items[i].sTmpData4 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 4, iFlag, Items[i].sTmpData4);
        //                //*** Ngón thứ 5 ***/
        //                if (Items[i].sTmpData5 == null) Items[i].sTmpData5 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 5, iFlag, Items[i].sTmpData5);
        //                //*** Ngón thứ 6 ***/
        //                if (Items[i].sTmpData6 == null) Items[i].sTmpData6 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 6, iFlag, Items[i].sTmpData6);
        //                //*** Ngón thứ 7 ***/
        //                if (Items[i].sTmpData7 == null) Items[i].sTmpData7 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 7, iFlag, Items[i].sTmpData7);
        //                //*** Ngón thứ 8 ***/
        //                if (Items[i].sTmpData8 == null) Items[i].sTmpData8 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 8, iFlag, Items[i].sTmpData8);
        //                //*** Ngón thứ 9 ***/
        //                if (Items[i].sTmpData9 == null) Items[i].sTmpData9 = "";
        //                this._axCZKEM1.SetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, 9, iFlag, Items[i].sTmpData9);

                       
        //            }
        //            else
        //            {
        //                this._axCZKEM1.GetLastError(ref idwErrorCode);
        //                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
        //                this._axCZKEM1.EnableDevice(this._MachineNumber, true);
        //                return;
        //            }
        //        }
        //    }
        //    //2. Bắt đầu upload dữ liệu từ bộ nhớ tạm và lưu vào máy chấm công
        //    this._axCZKEM1.BatchUpdate(this._MachineNumber);//upload all the information in the memory
        //    this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
        //    this._axCZKEM1.EnableDevice(this._MachineNumber, true);
        //    if (Complete != null) Complete(_ex);
        //    this._IsUpload = false;
        //}

        #endregion
    }
}
