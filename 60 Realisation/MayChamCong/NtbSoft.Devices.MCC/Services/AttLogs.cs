﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.Entities.Interfaces;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.Devices.MCC.Services
{
    //public class AttLogEventArgs : EventArgs
    //{
    //    public int RowIndex { get; set; }
    //    public string UserId { get; set; }
    //    public string UserName { get; set; }
    //    public Exception Ex { get; set; }
    //}

    public class AttLogs:Services.Interfaces.IAttLogs
    {
        Services.Interfaces.IBaseConnect _BaseConnect;
        private int _MachineNumber = 0;
        private zkemkeeper.IZKEM _axCZKEM1;
        private bool _IsConnected = false;
        internal bool _IsDownload = false;
        private DateTime _LastTimeGetData;
        public event AttLogsEventHandler OnDownloading;
        public AttLogs(int MachineNumber, DateTime LastTimeGetData, Services.Interfaces.IBaseConnect BaseConnect)
        {
            this._MachineNumber = MachineNumber;
            this._BaseConnect = BaseConnect;
            this._LastTimeGetData = LastTimeGetData;
        }

        public void Connect( Action<bool, Exception> Complete)
        {
            Exception _ex = null;
            _BaseConnect.Connect(this._MachineNumber, (result, connected, ex) =>
            {
                _IsConnected = connected;
                _ex = ex;
                if (ex == null)
                    this._axCZKEM1 = result;
            });
            if (Complete != null) Complete(_IsConnected, _ex);
        }

        public void Connect(Action<bool, string, Exception> Complete)
        {
            Exception _ex = null;
            string SerialNumber = "";
            _BaseConnect.Connect(this._MachineNumber, (result, connected, ex) =>
            {
                _IsConnected = connected;
                _ex = ex;
                if (ex == null)
                    this._axCZKEM1 = result;
            });
            if (_IsConnected)
                GetSerialNumber((result, ex) => { SerialNumber = result; });
            if (Complete != null) Complete(_IsConnected, SerialNumber, _ex);
        }

        public void DownloadData()
        {
            
        }

        public void DownloadAttLogs(Action<List<ICardUserInfo>, Exception> Complete)
        {
            if (this._IsDownload) return;
            this._IsDownload = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(null, new Exception("Please connect the device first"));
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => Download(Complete)).Wait();
            }
        }

        private void Download(Action<List<ICardUserInfo>, Exception> Complete)
        {
            int idwErrorCode = 0;
            Exception _ex = null;
            List<ICardUserInfo> UserCollection = null;
            this._axCZKEM1.EnableDevice(this._MachineNumber, false);//disable the device
            if (_axCZKEM1.ReadGeneralLogData(this._MachineNumber))//read all the attendance records to the memory
            {
                string sdwEnrollNumber = "";
                int idwVerifyMode = 0;
                int idwInOutMode = 0;
                int idwYear = 0;
                int idwMonth = 0;
                int idwDay = 0;
                int idwHour = 0;
                int idwMinute = 0;
                int idwSecond = 0;
                int idwWorkcode = 0;

                int iGLCount = 0;
                int iIndex = 0;
                UserCollection = new List<ICardUserInfo>();
                
                var flag = this._axCZKEM1.IsTFTMachine(this._MachineNumber);
                var dd = flag;
                while (_axCZKEM1.SSR_GetGeneralLogData(_MachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                           out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    iGLCount++;

                    Models.clsUserInfo item = new Models.clsUserInfo();
                    item.UserId = sdwEnrollNumber;
                    item.DateInOut = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond);
                    item.InOutMode = idwInOutMode;
                    item.MachineNumber = this._MachineNumber;
                    DateTime dt1 = DateTime.Now;
                    DateTime dt2 = DateTime.Now;//.AddMinutes(1);
                    
                    /*** Lấy thông tin người dùng ***/
                    string sName = "", sPassword = "";
                    int sPrivilege = 0; bool sEnabled = false;
                    if (this._axCZKEM1.SSR_GetUserInfo(this._MachineNumber, sdwEnrollNumber, out sName, out sPassword, out sPrivilege, out sEnabled))
                        item.UserName = sName;

                    if (DateTime.Compare(item.DateInOut, this._LastTimeGetData) > 0)
                        UserCollection.Add(item);

                    if (OnDownloading != null) OnDownloading(this, new AttLogsEventArgs() { UserId = item.UserId, UserName = item.UserName, RowIndex = iGLCount });
                    //System.Threading.Thread.Sleep(100);
                    iIndex++;
                }
                
            }
            else
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                if (idwErrorCode != 0)
                    _ex = new Exception("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString());
                else
                    _ex = new Exception("No data from terminal returns!");
            }
            if (Complete != null) Complete(UserCollection, _ex);
            this._IsDownload = false;
            this._axCZKEM1.EnableDevice(this._MachineNumber, true);//enable the device
        }

        public void ClearGLog(Action<Exception> Complete)
        {
            Exception _ex = null;
            if (!this._IsConnected)
                _ex = new Exception("Please connect the device first");
            else
            {
                int idwErrorCode = 0;
                this._axCZKEM1.EnableDevice(this._MachineNumber, false);//disable the device
                if (this._axCZKEM1.ClearGLog(this._MachineNumber))
                    this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
                else
                {
                    this._axCZKEM1.GetLastError(ref idwErrorCode);
                    _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
                }
                this._axCZKEM1.EnableDevice(this._MachineNumber, true);//enable the device
            }
            if (Complete != null) Complete(_ex);

        }


        private void GetSerialNumber(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sdwSerialNumber = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetSerialNumber(this._MachineNumber, out sdwSerialNumber))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sdwSerialNumber, _ex);
        }

        public void SynzTimeMachine(Action<bool, Exception> Complete)
        {
            bool success = true;
            Exception _ex = null;
            if (!this._axCZKEM1.SetDeviceTime(this._MachineNumber))
            {
                success = false;
                _ex = new Exception("Not synz datetime to machine " + this._MachineNumber);
            }
            if (Complete != null) Complete(success, _ex);
        }
    }
}
