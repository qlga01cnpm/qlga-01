﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services.Interfaces
{
    /// <summary>
    /// Quản lý người dùng
    /// </summary>
    public interface IUserManagement
    {
        void Connect(Action<bool, Exception> Complete);
        /// <summary>
        /// Tải vân tay
        /// </summary>
        /// <param name="Complete"></param>
        void DownloadFPTmp(Action<List<Entities.Interfaces.MCC.IUserFingerprintInfo>, Exception> Complete);
        
        void UploadFPTmp(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Exception> Complete);
    }
}
