﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.Devices.MCC.Services.Interfaces
{
    public interface IDeviceManager
    {
        void Connect( Action<bool, Exception> Complete);
        void GetFirmwareVersion(Action<int, Exception> Complete);
        void GetDeviceMAC(Action<string, Exception> Complete);
        void GetSerialNumber(Action<string, Exception> Complete);
        void GetProductCode(Action<string, Exception> Complete);
        void GetPlatform(Action<string, Exception> Complete);
        void GetDeviceIP(Action<string, Exception> Complete);
        void GetSDKVersion(Action<string, Exception> Complete);
        void GetVendor(Action<string, Exception> Complete);
        void SetDeviceIP(string IPAddr, Action<bool> Complete);
        void GetDeviceInfo(Action<IDeviceInfo, Exception> Complete);
        void SetTimeFormat(int dwValue, Action<bool> Complete);
        void SetDeviceTime2(int dwYear, int dwMonth, int dwDay, int dwHour, int dwMinute, int dwSecond, Action<bool> Complete);
        void ClearAdministrators(Action<bool> Complete);

    }
}
