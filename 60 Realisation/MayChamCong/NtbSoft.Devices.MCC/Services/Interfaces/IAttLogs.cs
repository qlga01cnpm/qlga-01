﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.Devices.MCC.Services.Interfaces
{
 
    public interface IAttLogs
    {
        event AttLogsEventHandler OnDownloading;
        void Connect(Action<bool, Exception> Complete);
        void Connect(Action<bool, string, Exception> Complete);
        void DownloadData();
        void DownloadAttLogs(Action<List<ICardUserInfo>, Exception> Complete);

        //void GetRecordCount();
        void ClearGLog(Action<Exception> Complete);

        void SynzTimeMachine(Action<bool, Exception> Complete);
    }
}
