﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services
{
    public class ConnectNet : Services.Interfaces.IBaseConnect
    {
        private string _IPAdd;
        private int _Port;
        public ConnectNet(string IPAdd,int Port) {
            this._IPAdd = IPAdd;
            this._Port = Port;
        }
        public void Connect(int MachineNumber, Action<zkemkeeper.IZKEM, bool, Exception> Complete)
        {
            zkemkeeper.IZKEM _axCZKEM1 = new zkemkeeper.CZKEMClass();
            int idwErrorCode = 0;
            bool Connected = false;
            Exception _ex = null;
            //_axCZKEM1.SetOptionCommPwd(9, "123");
            Connected = _axCZKEM1.Connect_Net(_IPAdd, _Port);
            if (Connected)
                _axCZKEM1.RegEvent(MachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            else
            {
                _axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString() + ".Error");
            }
            if (Complete != null) Complete(_axCZKEM1, Connected, _ex);
        }
    }
}
