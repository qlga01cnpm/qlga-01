﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.Devices.MCC.Services
{
    public class clsDeviceManager : Services.Interfaces.IDeviceManager
    {
        Services.Interfaces.IBaseConnect _BaseConnect;
        private int _MachineNumber = 0;
        private zkemkeeper.IZKEM _axCZKEM1;
        public clsDeviceManager(int MachineNumber, Services.Interfaces.IBaseConnect BaseConnect)
        {
            this._MachineNumber = MachineNumber;
            this._BaseConnect = BaseConnect;
        }
        public void Connect(Action<bool, Exception> Complete)
        {
            bool IsConnected = false;
            Exception _ex = null;
            _BaseConnect.Connect(this._MachineNumber, (result, connected, ex) =>
            {
                IsConnected = connected;
                _ex = ex;
                if (ex == null)
                    this._axCZKEM1 = result;
            });
            if (Complete != null) Complete(IsConnected, _ex);

        }

        public void GetFirmwareVersion(Action<int, Exception> Complete)
        {
            //_axCZKEM1.Connect_Com(ComPort,MachineNumber,BaudRate)
            string sVersion = "";
            int idwErrorCode = 0;
            Exception _ex = null;
            if (!this._axCZKEM1.GetFirmwareVersion(_MachineNumber, ref sVersion))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(idwErrorCode, _ex);
            
        }

        public void GetDeviceMAC(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sMAC = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetDeviceMAC(this._MachineNumber, ref sMAC))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sMAC, _ex);
        }

        public void GetSerialNumber(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sdwSerialNumber = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetSerialNumber(this._MachineNumber, out sdwSerialNumber))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sdwSerialNumber, _ex);
        }

        public void GetProductCode(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sdwSerialNumber = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetProductCode(this._MachineNumber, out sdwSerialNumber))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null) Complete(sdwSerialNumber, _ex);
        }

        public void GetPlatform(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sPlatform = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetPlatform(this._MachineNumber, ref sPlatform))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sPlatform, _ex);
        }

        public void GetDeviceIP(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sIP = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetDeviceIP(this._MachineNumber, ref sIP))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sIP, _ex);
        }

        public void GetSDKVersion(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sVersion = "";
            Exception _ex = null;
            if (!this._axCZKEM1.GetSDKVersion(ref sVersion))
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sVersion, _ex);
        }


        public void GetVendor(Action<string, Exception> Complete)
        {
            int idwErrorCode = 0;
            string sVendor = "";
            Exception _ex = null;
            if (!_axCZKEM1.GetVendor(ref sVendor))
            {
                _axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            if (Complete != null)
                Complete(sVendor, _ex);
        }


        public void SetDeviceIP(string IPAddr, Action<bool> Complete)
        {
            bool result = false;
            if (this._axCZKEM1.SetDeviceIP(this._MachineNumber, IPAddr))
            {
                result = true;
                this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            }
            if (Complete != null) Complete(result);
        }

        public void GetDeviceInfo(Action<IDeviceInfo, Exception> Complete)
        {
            string sdwSerialNumber = "", sIP = "", sVersion= "";
            int _value = 0;
            Exception _ex = null;
            Models.clsDeviceInfo Item = new Models.clsDeviceInfo();
            if (this._axCZKEM1.GetSerialNumber(this._MachineNumber, out sdwSerialNumber))
                Item.SerialNumber = sdwSerialNumber;

            if (this._axCZKEM1.GetDeviceIP(this._MachineNumber, ref sIP))
                Item.DeviceIP = sIP;
            if (this._axCZKEM1.GetFirmwareVersion(this._MachineNumber, ref sVersion))
                Item.FirmwareVersion = sVersion;
            if (this._axCZKEM1.GetDeviceStatus(this._MachineNumber, 1, ref _value))
                Item.CountAdmin = _value;
            if (this._axCZKEM1.GetDeviceStatus(this._MachineNumber, 2, ref _value))
                Item.CountUser = _value;
            if (this._axCZKEM1.GetDeviceStatus(this._MachineNumber, 6, ref _value))
                Item.SoBanGhi = _value;
            
            if (Complete != null) Complete(Item, _ex);
        }

        /// <summary>
        /// Cài đặt định dạng ngày
        /// </summary>
        /// <param name="dwValue"></param>
        /// <param name="Complete"></param>
        public void SetTimeFormat(int dwValue, Action<bool> Complete)
        {
            //34: Time Format
            int dwInfo = 34;
            bool result = false;
            if (this._axCZKEM1.SetDeviceInfo(this._MachineNumber, dwInfo, dwValue))
            {
                result = true;
                this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            }
         
            if (Complete != null) Complete(result);
        }


        public void SetDeviceTime2(int dwYear, int dwMonth, int dwDay, int dwHour, int dwMinute, int dwSecond, Action<bool> Complete)
        {
            bool result=false;
            if (this._axCZKEM1.SetDeviceTime2(this._MachineNumber, dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond))
            {
                result = true;
                this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            }
            if (Complete != null) Complete(result);
            
        }
        public void ClearAdministrators(Action<bool> Complete)
        {
            bool result = false;
            if(this._axCZKEM1.ClearAdministrators(this._MachineNumber)){
                result = true;
                this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            }
            if (Complete != null) Complete(result);
        }
        
    }
}
