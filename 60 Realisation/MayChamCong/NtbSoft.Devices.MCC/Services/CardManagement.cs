﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services
{
    /// <summary>
    /// Quản lý thông tin người dùng trên máy chấm công
    /// </summary>
    public class CardManagement : Interfaces.ICardManagement
    {
        Services.Interfaces.IBaseConnect _BaseConnect;
        private int _MachineNumber = 0;
        private zkemkeeper.IZKEM _axCZKEM1;
        private bool _IsConnected = false;
        internal bool _IsGet = false;
        private bool _IsDeleted = false;

        public CardManagement(int MachineNumber, Services.Interfaces.IBaseConnect BaseConnect)
        {
            this._MachineNumber = MachineNumber;
            this._BaseConnect = BaseConnect;
        }

        public void Connect(Action<bool, Exception> Complete)
        {
            Exception _ex = null;
            _BaseConnect.Connect(this._MachineNumber, (result, connected, ex) =>
            {
                _IsConnected = connected;
                _ex = ex;
                if (ex == null)
                    this._axCZKEM1 = result;
            });
            if (Complete != null) Complete(_IsConnected, _ex);
        }
        /// <summary>
        /// Lấy thông tin người dùng trên máy
        /// </summary>
        /// <param name="Complete"></param>
        public void GetStrCardNumber(Action<List<Entities.Interfaces.MCC.IUserFingerInfo>, Exception> Complete)
        {
            if (this._IsGet) return;
            this._IsGet = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(null, new Exception("Please connect the device first"));
                return;
            }

            Exception _ex = null;
            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;

            int idwFingerIndex;
            string sTmpData = "";
            int iTmpLength = 0;
            int iFlag = 0;
            List<Entities.Interfaces.MCC.IUserFingerInfo> IUserCollection = new List<Entities.Interfaces.MCC.IUserFingerInfo>();

            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            this._axCZKEM1.ReadAllUserID(this._MachineNumber);//read all the user information to the memory
            this._axCZKEM1.ReadAllTemplate(this._MachineNumber);//read all the users' fingerprint templates to the memory

            while (this._axCZKEM1.SSR_GetAllUserInfo(this._MachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                string sCardnumber = "";
                Models.clsUserFingerInfo Item = new Models.clsUserFingerInfo();
                Item.UserId = sdwEnrollNumber;
                Item.UserName = sName;
                Item.Privilege = iPrivilege;
                Item.Password = sPassword;
                Item.Enabled = bEnabled;
                Item.IsPassword = string.IsNullOrWhiteSpace(sPassword) ? false : true;

                if (this._axCZKEM1.GetStrCardNumber(out sCardnumber))//get the card number from the memory
                    Item.CardID = sCardnumber;
                //Item.FingerIndex = idwFingerIndex;
                //Duyệt qua dấu vân tay của 10 ngón
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (this._axCZKEM1.GetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        if (idwFingerIndex == 0) Item.FingerIndex0 = 1;
                        else if (idwFingerIndex == 1) Item.FingerIndex1 = 1;
                        else if (idwFingerIndex == 2) Item.FingerIndex2 = 1;
                        else if (idwFingerIndex == 3) Item.FingerIndex3 = 1;
                        else if (idwFingerIndex == 4) Item.FingerIndex4 = 1;
                        else if (idwFingerIndex == 5) Item.FingerIndex5 = 1;
                        else if (idwFingerIndex == 6) Item.FingerIndex6 = 1;
                        else if (idwFingerIndex == 7) Item.FingerIndex7 = 1;
                        else if (idwFingerIndex == 8) Item.FingerIndex8 = 1;
                        else if (idwFingerIndex == 9) Item.FingerIndex9 = 1;
                    }
                }
                IUserCollection.Add(Item);
            }

            this._axCZKEM1.EnableDevice(this._MachineNumber, true);

            //////Exception _ex = null;
            //////string sdwEnrollNumber = "";
            //////string sName = "";
            //////string sPassword = "";
            //////int iPrivilege = 0;
            //////bool bEnabled = false;
            //////string sCardnumber = "";
            //////List<Entities.Interfaces.MCC.IUserFingerInfo> Items = new List<Entities.Interfaces.MCC.IUserFingerInfo>();

            //////this._axCZKEM1.EnableDevice(this._MachineNumber, false);//disable the device
            //////this._axCZKEM1.ReadAllUserID(this._MachineNumber);//read all the user information to the memory
            //////while (this._axCZKEM1.SSR_GetAllUserInfo(this._MachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get user information from memory
            //////{
            //////    if (this._axCZKEM1.GetStrCardNumber(out sCardnumber))//get the card number from the memory
            //////    {
            //////        Models.clsUserFingerInfo Info = new Models.clsUserFingerInfo();
            //////        Info.UserId = sdwEnrollNumber;
            //////        Info.UserName = sName;
            //////        Info.CardID = sCardnumber;
            //////        Info.Privilege = iPrivilege;
            //////        Info.Password = sPassword;
            //////        Info.Enabled = bEnabled;

            //////        Items.Add(Info);
            //////    }
            //////}
            //////this._axCZKEM1.EnableDevice(this._MachineNumber, true);//enable the device

            if (Complete != null) Complete(IUserCollection, _ex);
            this._IsGet = false;
        }

        public void DeleteEnrollData(List<NtbSoft.Entities.Interfaces.MCC.IUserFingerInfo> Items, Action<Exception> Complete)
        {
            if (this._IsDeleted) return;
            this._IsDeleted = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(new Exception("Please connect the device first"));
                return;
            }
            int idwErrorCode = 0;
            Exception _ex = null;
            foreach (var item in Items)
            {
                //- The value ranges from 0 to 9. It this case, the device also checks whether the user has other fingerprints or passwords. if no, the device deletes the use
                //- When the value is 0, the device deletes the password. The device also checks wherther the user has fingerprint data, the device deletes the user.
                //- When the value is 11 or 13, the device deletes all fingerprint data of the user. When the value is 12, the device deletes the user
                //(including all fingerprints, card numbers and passwords of the user).
                int iBackupNumber = 12;

                if (this._axCZKEM1.SSR_DeleteEnrollData(this._MachineNumber, item.UserId, iBackupNumber))
                {
                    this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
                    //MessageBox.Show("DeleteEnrollData,UserID=" + sUserID + " BackupNumber=" + iBackupNumber.ToString(), "Success");
                }
                else
                {
                    this._axCZKEM1.GetLastError(ref idwErrorCode);
                    _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
                }
            }
            if (Complete != null) Complete(_ex);
            _IsDeleted = false;
        }

        public void SetStrCardNumber(NtbSoft.Entities.Interfaces.MCC.IUserFingerprintInfo Item, Action<Exception> Complete)
        {
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(new Exception("Please connect the device first"));
                return;
            }
            Exception _ex = null;
            int idwErrorCode = 0;
            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            //string sdwEnrollNumber = txtUserID.Text.Trim();
            this._axCZKEM1.SetStrCardNumber(Item.CardID);//Before you using function SetUserInfo,set the card number to make sure you can upload it to the device
            if (this._axCZKEM1.SSR_SetUserInfo(this._MachineNumber, Item.UserId, Item.UserName, Item.Password, Item.Privilege, Item.Enabled))//upload the user's information(card number included)
            {
                // MessageBox.Show("(SSR_)SetUserInfo,UserID:" + sdwEnrollNumber + " Privilege:" + iPrivilege.ToString() + " Enabled:" + bEnabled.ToString(), "Success");
            }
            else
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            this._axCZKEM1.EnableDevice(this._MachineNumber, true);
            if (Complete != null) Complete(_ex);
        }

        public void SetStrCardNumber(List<Entities.Interfaces.MCC.IUserFingerprintInfo> Items, Action<Exception> Complete)
        {
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(new Exception("Please connect the device first"));
                return;
            }
            Exception _ex = null;

            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            //string sdwEnrollNumber = txtUserID.Text.Trim();
            foreach (var Item in Items)
            {
                int idwErrorCode = 0;
                this._axCZKEM1.SetStrCardNumber(Item.CardID);//Before you using function SetUserInfo,set the card number to make sure you can upload it to the device
                if (this._axCZKEM1.SSR_SetUserInfo(this._MachineNumber, Item.UserId, Item.UserName, Item.Password, Item.Privilege, Item.Enabled))//upload the user's information(card number included)
                {
                    // MessageBox.Show("(SSR_)SetUserInfo,UserID:" + sdwEnrollNumber + " Privilege:" + iPrivilege.ToString() + " Enabled:" + bEnabled.ToString(), "Success");
                }
                else
                {
                    this._axCZKEM1.GetLastError(ref idwErrorCode);
                    _ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
                }
            }
            this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
            this._axCZKEM1.EnableDevice(this._MachineNumber, true);
            if (Complete != null) Complete(_ex);
        }

        /// <summary>
        /// Lấy thông tin người dùng trên máy
        /// </summary>
        /// <param name="Complete"></param>
        public void GetUser(int id, Action<List<Entities.Interfaces.MCC.IUserFingerInfo>, Exception> Complete)
        {
            if (this._IsGet) return;
            this._IsGet = true;
            if (!this._IsConnected)
            {
                if (Complete != null) Complete(null, new Exception("Please connect the device first"));
                return;
            }

            Exception _ex = null;
            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;

            int idwFingerIndex;
            string sTmpData = "";
            int iTmpLength = 0;
            int iFlag = 0;
            List<Entities.Interfaces.MCC.IUserFingerInfo> IUserCollection = new List<Entities.Interfaces.MCC.IUserFingerInfo>();

            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            this._axCZKEM1.ReadAllUserID(this._MachineNumber);//read all the user information to the memory
            this._axCZKEM1.ReadAllTemplate(this._MachineNumber);//read all the users' fingerprint templates to the memory

            if (this._axCZKEM1.SSR_GetAllUserInfo(this._MachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                string sCardnumber = "";
                Models.clsUserFingerInfo Item = new Models.clsUserFingerInfo();
                Item.UserId = sdwEnrollNumber;
                Item.UserName = sName;
                Item.Privilege = iPrivilege;
                Item.Password = sPassword;
                Item.Enabled = bEnabled;
                Item.IsPassword = string.IsNullOrWhiteSpace(sPassword) ? false : true;

                if (this._axCZKEM1.GetStrCardNumber(out sCardnumber))//get the card number from the memory
                    Item.CardID = sCardnumber;

                //Duyệt qua dấu vân tay của 10 ngón
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (this._axCZKEM1.GetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        if (idwFingerIndex == 0) Item.FingerIndex0 = 1;
                        else if (idwFingerIndex == 1) Item.FingerIndex1 = 1;
                        else if (idwFingerIndex == 2) Item.FingerIndex2 = 1;
                        else if (idwFingerIndex == 3) Item.FingerIndex3 = 1;
                        else if (idwFingerIndex == 4) Item.FingerIndex4 = 1;
                        else if (idwFingerIndex == 5) Item.FingerIndex5 = 1;
                        else if (idwFingerIndex == 6) Item.FingerIndex6 = 1;
                        else if (idwFingerIndex == 7) Item.FingerIndex7 = 1;
                        else if (idwFingerIndex == 8) Item.FingerIndex8 = 1;
                        else if (idwFingerIndex == 9) Item.FingerIndex9 = 1;
                    }
                }
                IUserCollection.Add(Item);
            }

            this._axCZKEM1.EnableDevice(this._MachineNumber, true);

            if (Complete != null) Complete(IUserCollection, _ex);
            this._IsGet = false;
        }

    }
}
