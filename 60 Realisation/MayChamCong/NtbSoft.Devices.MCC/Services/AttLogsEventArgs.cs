﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services
{
    public delegate void AttLogsEventHandler(object sender, AttLogsEventArgs e);
    public class AttLogsEventArgs : EventArgs
    {
        public int RowIndex { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public Exception Ex { get; set; }
    }
}
