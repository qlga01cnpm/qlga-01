﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC.Services
{
    public class ConnectCOM : Services.Interfaces.IBaseConnect
    {
        private string _COM="";
        private int _BaudRate = 0;
        public ConnectCOM(string COM, int BaudRate)
        {
            this._COM = COM;
            this._BaudRate = BaudRate;
        }
        public void Connect(int MachineNumber, Action<zkemkeeper.IZKEM, bool, Exception> Complete)
        {
            zkemkeeper.IZKEM _axCZKEM1 = new zkemkeeper.CZKEMClass();
            int idwErrorCode = 0;
            Exception _ex = null;
            bool IsConnected = false;
            int iPort;
            //accept serialport number from string like "COMi"
            string sPort = _COM.Trim();
            for (iPort = 1; iPort < 10; iPort++)
                if (sPort.IndexOf(iPort.ToString()) > -1)
                    break;
            IsConnected=_axCZKEM1.Connect_Com(iPort, MachineNumber, _BaudRate);
            if (IsConnected)
                _axCZKEM1.RegEvent(MachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            else
            {
                _axCZKEM1.GetLastError(ref idwErrorCode);
                _ex = new Exception("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString() + ".Error");
            }
            if (Complete != null) Complete(_axCZKEM1, IsConnected, _ex);
        }
    }
}
