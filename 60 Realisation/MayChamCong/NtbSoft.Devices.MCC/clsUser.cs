﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC
{
    public class clsUserInfo : Devices.Dal.IUserInfo
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public int FingerIndex { get; set; }

        public string TmpData { get; set; }

        public int Privilege { get; set; }

        public string Password { get; set; }

        public bool Enabled { get; set; }

        public int Flag { get; set; }

        public DateTime DateInOut { get; set; }

        /// <summary>
        /// Get
        /// </summary>
        public string StrDate
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        public string StrTime
        {
            get
            {
                if (this.DateInOut.ToString("MM/dd/yyyy").Equals("01/01/0001"))
                    return "";
                return this.DateInOut.ToString("HH:mm:ss");
            }
        }

        public int MachineNumber { get; set; }
    }

    public class clsUser : Devices.Dal.IUser
    {
        internal int _MachineNumber = 0;
        internal zkemkeeper.IZKEM _axCZKEM1;
        internal bool _IsDownload = false;

        public event Dal.AttLogsEventHandler OnDownloadCompleted;
        public event NtbSoft.Devices.Dal.UserUploadEventHandler OnDownloading;

        public clsUser(zkemkeeper.IZKEM axCZKEM1, int MachineNumber)
        {
            this._axCZKEM1 = axCZKEM1;
            this._MachineNumber = MachineNumber;
        }
        public void DownloadFPTmp()
        {
            if (!this._IsDownload)
            {
                this._IsDownload = true;
                System.Threading.Tasks.Task.Factory.StartNew(() => Download());
            }

        }

        private void Download()
        {
            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;

            int idwFingerIndex;
            string sTmpData = "";
            int iTmpLength = 0;
            int iFlag = 0;
            List<Devices.Dal.IUserInfo> IUserCollection = new List<Dal.IUserInfo>();

            this._axCZKEM1.EnableDevice(this._MachineNumber, false);
            this._axCZKEM1.ReadAllUserID(this._MachineNumber);//read all the user information to the memory
            this._axCZKEM1.ReadAllTemplate(this._MachineNumber);//read all the users' fingerprint templates to the memory
            while (this._axCZKEM1.SSR_GetAllUserInfo(this._MachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (this._axCZKEM1.GetUserTmpExStr(this._MachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        Devices.Dal.IUserInfo Item = new clsUserInfo();
                        Item.UserId = sdwEnrollNumber;
                        Item.UserName = sName;
                        Item.FingerIndex = idwFingerIndex;
                        Item.TmpData = sTmpData;
                        Item.Privilege = iPrivilege;
                        Item.Password = sPassword;
                        Item.Enabled = bEnabled;

                        IUserCollection.Add(Item);
                        if (this.OnDownloading != null)
                            this.OnDownloading(this, new Dal.UserUploadEventArgs()
                            {
                                UserId = sdwEnrollNumber,
                                UserName = sName,
                                FingerIndex = idwFingerIndex
                            });

                    }
                }
            }

            this._axCZKEM1.EnableDevice(this._MachineNumber, true);
            if (this.OnDownloadCompleted != null)
                this.OnDownloadCompleted(this, new Dal.AttLogsEventArgs()
                {
                    IUserCollection = IUserCollection
                });
            this._IsDownload = false;
        }
        
        /// <summary>
        /// Tải lên bản mẫu vân tay một lần
        /// </summary>
        /// <param name="Items"></param>
        public void UploadFPTmp(List<Dal.IUserInfo> Items)
        {
            //throw new NotImplementedException();
        }
    }

    
}
