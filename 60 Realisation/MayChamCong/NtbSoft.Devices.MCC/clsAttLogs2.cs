﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.Devices.MCC
{
    public class clsAttLogs2 : Dal.IAttLogs
    {
        internal zkemkeeper.IZKEM _axCZKEM1;
        internal int _MachineNumber = 0;
        internal bool _IsDownload = false;
        public event Dal.AttLogsEventHandler OnDownloadCompleted;
        public event Dal.AttReadingEventHandler OnDownloading;
        public event Dal.AttClearGLogEventHandler OnClearGLogCompleted;

        public clsAttLogs2(zkemkeeper.IZKEM axCZKEM1, int MachineNumber)
        {
            _axCZKEM1 = axCZKEM1;
            this._MachineNumber = MachineNumber;
        }
        
        public void DownloadData()
        {
            if (OnDownloadCompleted != null)
                OnDownloadCompleted(this, new Dal.AttLogsEventArgs()
                {
                    IUserCollection = new List<Dal.IUserInfo>()
                });
        }

        public void DownloadAttLogs()
        {
            if (!this._IsDownload)
            {
                this._IsDownload = true;
                System.Threading.Tasks.Task.Factory.StartNew(() => Download());
            }

        }

        private void Download()
        {
            if (_axCZKEM1.ReadGeneralLogData(_MachineNumber))//read all the attendance records to the memory
            {
                int idwErrorCode = 0;
                string sdwEnrollNumber = "";
                int idwVerifyMode = 0;
                int idwInOutMode = 0;
                int idwYear = 0;
                int idwMonth = 0;
                int idwDay = 0;
                int idwHour = 0;
                int idwMinute = 0;
                int idwSecond = 0;
                int idwWorkcode = 0;

                int iGLCount = 0;
                int iIndex = 0;
                List<NtbSoft.Devices.Dal.IUserInfo> UserCollection = new List<Dal.IUserInfo>();

                while (_axCZKEM1.SSR_GetGeneralLogData(_MachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                           out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    iGLCount++;

                    clsUserInfo item = new clsUserInfo();
                    item.UserId = sdwEnrollNumber;
                    item.DateInOut = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond);

                    UserCollection.Add(item);
                    if (OnDownloading != null)
                        OnDownloading(this, new Dal.AttReadingEventArgs()
                        {
                            UserId = sdwEnrollNumber,
                            RowIndex = iIndex
                        });
                    System.Threading.Thread.Sleep(10);
                    iIndex++;
                }

                if (OnDownloadCompleted != null)
                    OnDownloadCompleted(this, new Dal.AttLogsEventArgs()
                    {
                        IUserCollection = UserCollection
                    });
            }
            this._IsDownload = false;
        }



        public void ClearGLog()
        {
            //if(this.OnClearGLogCompleted!=null)
            int idwErrorCode = 0;
                        Exception ex = null;
            this._axCZKEM1.EnableDevice(this._MachineNumber, false);//disable the device
            if (this._axCZKEM1.ClearGLog(this._MachineNumber))
            {
                this._axCZKEM1.RefreshData(this._MachineNumber);//the data in the device should be refreshed
                //MessageBox.Show("All att Logs have been cleared from teiminal!", "Success");
            }
            else
            {
                this._axCZKEM1.GetLastError(ref idwErrorCode);
                ex = new Exception("Operation failed,ErrorCode=" + idwErrorCode.ToString());
            }
            this._axCZKEM1.EnableDevice(this._MachineNumber, true);//enable the device
            if (this.OnClearGLogCompleted != null)
                this.OnClearGLogCompleted(this, new Dal.AttClearGLogEventArgs()
                {
                    Ex = ex
                });
        }


        
    }
}
