﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NtbSoft.ERP.Web
{
    public partial class Login : System.Web.UI.Page
    {
        //http://www.binaryintellect.net/articles/436bc727-6de2-4d11-9762-9ee1a1e3cbea.aspx
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["UserName"] != null)
                    LoginUser.UserName = Request.Cookies["UserName"].Value;

                #region TESTTTTTTTTTTTTTT
                //LoginUser.UserName = "user";
                //TextBox txtPassword1 = (TextBox)LoginUser.FindControl("Password");
                //if (txtPassword1 != null)
                //    txtPassword1.Attributes["value"] = "user";
                #endregion


                if (Request.Cookies["Password"] != null)
                {
                    LoginUser.RememberMeSet = true;
                    TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
                    if (txtPassword != null)
                        txtPassword.Attributes["value"] = Request.Cookies["Password"].Value;
                }


            }
            else if (string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
            {
                //Xoa Cookie Password
                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
            }
        }

        /// <summary>
        /// Redirect the user to a specific URL, as specified in the web.config, depending on their role.
        /// If a user belongs to multiple roles, the first matching role in the web.config is used.
        /// Prioritize the role list by listing higher-level roles at the top.
        /// </summary>
        /// <param name="username">Username to check the roles for</param>
        private void RedirectLogin(string username)
        {
            LoginRedirectByRoleSection roleRedirectSection =
                (LoginRedirectByRoleSection)System.Configuration.ConfigurationManager.GetSection("loginRedirectByRole");
            bool _HasRole = false;
            foreach (RoleRedirect roleRedirect in roleRedirectSection.RoleRedirects)
            {
                if (Roles.IsUserInRole(username, roleRedirect.Role))
                {
                    _HasRole = true;
                    Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                    if (LoginUser.RememberMeSet)
                    {
                        //Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                    }
                    else
                    {
                        //Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
                    }
                    //FormsAuthentication.SetAuthCookie(LoginUser.UserName, false);
                   
                    Response.Cookies["UserName"].Value = LoginUser.UserName;
                    if (!Libs.MD5Password.IsValidMD5(LoginUser.Password))
                        Response.Cookies["Password"].Value = Libs.MD5Password.getMd5Hash(LoginUser.Password);
                    else
                        Response.Cookies["Password"].Value = LoginUser.Password;
                    Response.Redirect(roleRedirect.Url);
                }
            }
            //if (!_HasRole)
            //{
            //    Response.Cookies.Clear();
            //}
        }

        protected void LoginUser_LoggedIn(object sender, EventArgs e)
        {

            RedirectLogin(LoginUser.UserName);

        }
    }
}