﻿using NtbSoft.ERP.Entity;
using NtbSoft.ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;


namespace NtbSoft.ERP.Web.Providers
{
    public class LgRoleProvider : RoleProvider
    {
        private clsGroupCollection _RoleCollection = null;
        private List<string> _ListGroupIdByUserID = null;
        public override string[] GetAllRoles()
        {
            try
            {
                if (_RoleCollection == null)
                {
                    clsGroupBS _User = new clsGroupBS();
                    _RoleCollection = _User.GetListOfGroup();
                    if (_RoleCollection == null)
                        throw new Exception("GetAllRoles(): NULL");
                }
                string[] _UserRoles = new string[_RoleCollection.Count];
                
                int j = 0;
                foreach (var item in _RoleCollection)
                {
                    _UserRoles[j] = item.GroupID.ToString();
                    j++;
                }
                return _UserRoles;
                //return new string[] { "User", "Administrator" };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }  
        }
        
        public override string[] GetRolesForUser(string username)
        {

            if (_ListGroupIdByUserID == null)
            {
                clsGroupUserBS _User = new clsGroupUserBS();
                _ListGroupIdByUserID = _User.GetListGroupIdByUserID(username);
            }
           
            if (_ListGroupIdByUserID == null)
                return new string[] { };
            
            string[] _UserRoles = new string[_ListGroupIdByUserID.Count];
            int j = 0;
            foreach (string item in _ListGroupIdByUserID)
            {
                _UserRoles[j] = item.ToString();
                j++;
            }
            return _UserRoles;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string[] roles = GetRolesForUser(username);
            return (roles.ToList().IndexOf(roleName) > -1);
        }

        public override bool RoleExists(string roleName)
        {
            string[] roles = GetAllRoles();
            return (roles.ToList().IndexOf(roleName) > -1);
        }

        #region Unused Members

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            this._RoleCollection = null;
            this._ListGroupIdByUserID = null;
            //throw new NotImplementedException();
        }
        
        #endregion
    }
}