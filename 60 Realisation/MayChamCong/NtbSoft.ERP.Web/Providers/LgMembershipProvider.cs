﻿using NtbSoft.ERP.Entity;
using NtbSoft.ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace NtbSoft.ERP.Web.Providers
{
    ////[AttributeUsage(AttributeTargets.Assembly)]
    ////public class AssemblyMyCustomAttribute : Attribute
    ////{
    ////    public string Value { get; private set; }

    ////    public AssemblyMyCustomAttribute() : this("") { }
    ////    public AssemblyMyCustomAttribute(string value) { Value = value; }
    ////}

    public class LgMembershipProvider : MembershipProvider
    {
        public clsUserInfo UserInfo { get; set; }
        internal Registration.Interfaces.IRegistration _Register;
        internal string _sRegisteredName = "";
        public LgMembershipProvider()
        {
            //Lay thong tin phan mem dang ky
            //System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            //var attribute = (NtbSoft.ERP.Libs.AssemblyNtbSoftAttribute)assembly.GetCustomAttributes(typeof(NtbSoft.ERP.Libs.AssemblyNtbSoftAttribute), true)[0];
            //_sRegisteredName = attribute.Value;
            ////_sRegisteredName = "MODERNTECH";

            _Register = new Registration.clsWebRegistration();
        }
        public override bool ValidateUser(string username, string password)
        {
            #region 1. Kiểm tra đăng ký bản quyền 
            ////////////if (_IRegister == null)
            ////////////    _IRegister = new Registration.clsWebRegistration();
            ////////////string kkss = "123456";
            ////////////string ddd = kkss.Insert(3, "hoang_duc");
            ////////////string kkdddsd = ddd;
            ////////////string s = _Register.GenerateKey(_sRegisteredName);
            ////////////var kq = s;
            /*** Kiem tra dang dang ky hay chua ***/
            //if (!_Register.IsKeyOK())
            //{
            //    HttpContext.Current.Response.Redirect("Active/Index");
            //    return false;
            //    //throw new Exception("ỨNG DỤNG CHƯA ĐƯỢC ĐĂNG KÝ BẢN QUYỀN SỬ DỤNG. VUI LÒNG LIÊN HỆ VỚI NHÀ CUNG CẤP");
            //}
            #endregion

            #region 2. Kiểm tra tài khoản đăng nhập
            UserStatus _status = UserStatus.NotExists;
            clsUserBS _User = new  clsUserBS();
            UserInfo = _User.CheckUser(username, password, ref _status);
            if (_status == UserStatus.OK) return true;
            //else
            //    throw new Exception("Mật khẩu không hợp lệ");

            #endregion

            return false;
        }

        #region Unused Memebrs

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return false;
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            //get { throw new NotImplementedException(); }

            get
            {
                try
                {
                    return MembershipPasswordFormat.Encrypted;
                }
                catch (Exception)
                {
                    return MembershipPasswordFormat.Clear;
                }
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}