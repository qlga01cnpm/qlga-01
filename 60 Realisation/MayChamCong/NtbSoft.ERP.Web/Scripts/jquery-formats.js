﻿function myformatter(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '/' + (m < 10 ? ('0' + m) : m) + '/' + (d < 10 ? ('0' + d) : d);
}


function myparser(s) {
    if (!s) return new Date();
    var ss = (s.split('/'));
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d);
    } else {
        return new Date();
    }
}

/*** Format dd/mm/yyyy ***/
function myformatterDMY(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/' + y;
}


function myparserDMY(s) {
    if (!s) return new Date();
    var ss = (s.split('/'));
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(d, m - 1, y);
    } else {
        return new Date();
    }
}
//======================================================================

function myformatterMDY(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return (m < 10 ? ('0' + m) : m) + '/' + (d < 10 ? ('0' + d) : d) + '/' + y;
}

function myparserMDY(s) {
    if (!s) return new Date();
    var ss = (s.split('/'));
    var m = parseInt(ss[0], 10);
    var d = parseInt(ss[1], 10);
    var y = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d);
    } else {
        return new Date();
    }
}


function DisplayCurrentTime() {
    var date = new Date();
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    time = hours + ":" + minutes + ":" + seconds + " " + am_pm;

    return time;
};

function formatHHmmss(date) {
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    time = hours + ":" + minutes + ":" + seconds;
    return time;
}


function formatJsonDateMDY(dateString) {
    var myDate = new Date(dateString);
    var output = (myDate.getMonth() + 1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();

    return output;
}

function formatJsonDateDMY(dateString) {
    var myDate = new Date(dateString);
    var output = myDate.getDate() + "/" + (myDate.getMonth() + 1) + "/" + myDate.getFullYear();

    return output;
}


function formatDateMDY(date) {
    var myDate = date;
    var output = (myDate.getMonth() + 1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();

    return output;
}

function SetFormatMDY(value) {
    if (!value) return new Date();
    var ss = (value.split('/'));
    var d = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var y = parseInt(ss[2], 10);

    return m + '/' + d + '/' + y;
}

function SetFormatYMD(value) {
    if (!value) return new Date();
    var ss = (value.split('/'));
    var d = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var y = parseInt(ss[2], 10);

    return y + '/' + m + '/' + d;
}