﻿var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
});

$('#dd').datebox({
    onSelect: function (date) {
        var value = $('#dd').datebox('getValue');
        loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', value);
    }
});

function searcher(value, name) {
    if (value) {
        var depid = 'DEP.ROOT';
        loadDataPaging(_selectedPage, _pageSize, depid, value);
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetHoSoSuaChua?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-PhieuThuTien').datagrid('loadData', item.Tag);


            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}


function IniDataGrid() {
    $('#dg-PhieuThuTien').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-PhieuThuTien').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
}


// Các hàm lập phiếu thu tiền
var TTKH = null;
function loadDataTTKH(selectedPage, pageSize, DepId, ValueSearch) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetTTKH?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                TTKH = $.parseJSON(data);
                BindingTTKH(TTKH);
            } catch (e) {
                alert("EEROR_loadDataPTT: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function Clear() {
    $('#txtTenKhachHangPTT').textbox('setValue', "");
    $('#txtNgayThuTien').datebox('setValue', "");
    $('#txtBienSoPTT').textbox('setValue', "");
    $('#txtSoDienThoaiPTT').textbox('setValue', "");
    $('#txtEmailPTT').textbox('setValue', "");
}

var HoSoId = null;
function lapphieuthutien() {
    Clear();
    var row = $('#dg-PhieuThuTien').datagrid("getSelected");
    HoSoId = row.HoSoId;
    loadDataTTKH(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
    $("#frm-PhieuThuTien").window({ title: 'Lập phiếu thu tiền', left: '15%' });
    $("#frm-PhieuThuTien").window("open");
}

function BindingTTKH(TTKH) {
    $('#txtTenKhachHangPTT').textbox('setValue', TTKH.note1);
    $('#txtNgayThuTien').datebox('setValue', formatJsonDateDMY(Date.now));
    $('#txtBienSoPTT').textbox('setValue', TTKH.note4);
    $('#txtSoDienThoaiPTT').textbox('setValue', TTKH.note2);
    $('#txtEmailPTT').textbox('setValue', TTKH.note3);
    $('#txtTongTien').textbox('setValue', TTKH.note5);

}

function savePTT() {
    var row = {};
    row.HoSoId = HoSoId;
    row.NgayThuTien = SetFormatMDY($('#txtNgayThuTien').textbox('getValue'));
    row.SoTienThu = $('#txtSoTienThu').textbox('getValue');
    row.TongTien = $('#txtTongTien').textbox('getValue');
    Update("POST", row);
    closeFormPTT();
}

function Update(type,row) {
    $.ajax({
        url: '/QuanLyGara/PhieuThuTien',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            
            parent.bottomRight('Thông báo', 'Lưu thành công!');

        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}


function closeFormPTT() {
    /*** Đóng form Property ***/
    $("#frm-PhieuThuTien").window("close");
}