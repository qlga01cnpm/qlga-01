﻿var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
});

$('#dd').datebox({
    onSelect: function (date) {
        var value = $('#dd').datebox('getValue');
        loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', value);
    }
});

function searcher(value, name) {
    if (value) {
        var depid = 'DEP.ROOT';
        loadDataPaging(_selectedPage, _pageSize, depid, value);
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetHoSoXe?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function IniDataGrid() {
    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
}



// Các hàm lập phiếu sữa chữa

