﻿var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "PSC");
});

$('#dd').datebox({
    onSelect: function (date) {
        var value = $('#dd').datebox('getValue');
        loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', value);
    }
});

function searcher(value, name) {
    if (value) {
        var depid = 'DEP.ROOT';
        loadDataPaging(_selectedPage, _pageSize, depid, value);
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetHoSoXe?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function IniDataGrid() {
    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "PSC");
}



// Các hàm lập phiếu sữa chữa
function loadPhuTung() {
    $('#txtPhuTung').combobox({
        panelWidth: '150px',
        editable: false, panelHeight: 100,
        valueField: 'PhuTungId',
        textField: 'TenPhuTung',
        mode: 'remote',
        method: 'get',
        url: '/QuanLyGara/GetPhuTung'
    });
}


function loadTienCong() {
    $('#txtTienCong').combobox({
        panelWidth: '150px',
        editable: false, panelHeight: 100,
        valueField: 'TienCongId',
        textField: 'TenTienCong',
        mode: 'remote',
        method: 'get',
        url: '/QuanLyGara/GetTienCong'
    });
}

var HoSoId = null;
function lapphieusuachua() {
    var row = $('#dg-list').datagrid("getSelected");

    HoSoId = row.HoSoId;
    if (row == null || row == '') {
        alert("bạn chưa chọn khách hàng để lập phiếu");
        return;
    }
    if (row.TienDo == "Đã thu tiền") {
        alert("Phiếu này đã thu tiền");
        return;
    }
    loadDataPSC(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
    IniDataGridPSC();
    loadPhuTung();
    loadTienCong();
    bindingPhieuSuaChua(row);

    $("#frm-PhieuSuaChua").window({ title: 'Lập phiếu sữa chữa', left: '15%' });
    $("#frm-PhieuSuaChua").window("open");
}

function bindingPhieuSuaChua(row) {
    $('#txtBienSoPSC').textbox('setValue', row.BienSo);
    $('#txtNgaySuaChua').datebox('setValue', formatJsonDateDMY(Date.now));
}

function loadDataPSC(selectedPage, pageSize, DepId, ValueSearch) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetDetailPSC?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-listPhieuSuaChua').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-listPhieuSuaChua").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPSC: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function IniDataGridPSC() {
    $('#dg-listPhieuSuaChua').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-listPhieuSuaChua').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function addPSC() {
    var row = {};
    row.HoSoId = HoSoId;
    row.NgaySuaChua = SetFormatMDY($('#txtNgaySuaChua').textbox('getValue'));
    row.PhuTungId = $('#txtPhuTung').combobox('getValue');
    row.TienCongId = $('#txtTienCong').combobox('getValue');
    row.SoLuong = $('#txtSoLuong').textbox('getValue');
    row.NoiDung = $('#txtNoiDung').textbox('getValue');
    Update("POST", row); //Them moi
    Reset();
}

function Update(type, row) {
    $.ajax({
        url: '/QuanLyGara/PhieuSuaChua',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            //loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
            if (type == "PUT") {
                $('#dg-list').datagrid('updateRow', { index: rowIndex, row });
            }
            if (type == "POST") {
                //$('#dg-list').datagrid('appendRow', row);
                loadDataPSC(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
            }
            parent.bottomRight('Thông báo', 'Lưu thành công!');

        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}

function removeItPSC() {
    var rows = $('#dg-listPhieuSuaChua').datagrid("getChecked");
    if (rows == null || rows == '') {
        alert("Bạn vui lòng chọn dòng cần xóa!");
        return;
    }

    $.messager.confirm('Xác nhận', 'Bạn có chắc xóa những khách hàng đã chọn chứ?', function (r) {
        if (r) { //Begin (r) =====================================

            parent.setProgress("Vui lòng chờ");
            beginDelete(rows, 0);

        }//End (r)==============================================
    });
}

function beginDelete(rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang xử lý...");
    $.ajax({
        url: '/QuanLyGara/PhieuSuaChua',
        type: "POST",
        data: { type: 'DELETE', json: JSON.stringify(rows[index]) },
        dataType: "json",
        success: function (data) {
            if (data == true) {
                var rowIndex = $('#dg-listPhieuSuaChua').datagrid("getRowIndex", rows[index]);
                $('#dg-listPhieuSuaChua').datagrid("deleteRow", rowIndex);
            } else {
                parent.setProgressMsg("Lỗi: " + data);
            }
            beginDelete(rows, ++index);
        },
        error: function (response) {
            beginDelete(rows, ++index);
        }
    });
}

function Reset() {
    $('#txtPhuTung').combobox('setValue',"");
    $('#txtTienCong').combobox('setValue', "");
    $('#txtSoLuong').textbox('setValue',"");
    $('#txtNoiDung').textbox('setValue', "");
}

function refreshPSC() {
    loadDataPSC(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
    Reset();
}

