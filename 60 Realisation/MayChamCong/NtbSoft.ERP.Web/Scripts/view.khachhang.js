﻿var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
});


//Khach hang
function loadHieuXe() {
    $('#txtMaHieuXe').combobox({
        panelWidth: 190,
        editable: false, panelHeight: 100,
        valueField: 'HieuXeId',
        textField: 'TenHieuXe',
        mode: 'remote',
        method: 'get',
        url: '/QuanLyGara/GetHieuXe'
    });
}
function searcher(value, name) {
    if (value) {
        var depid = 'DEP.ROOT';
        loadDataPaging(_selectedPage, _pageSize, depid, value); 
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetThongTinKhachHang?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}


function IniDataGrid() {
    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function resetUI() {

    $("#txtKhachHangId").textbox('setValue', "Auto fill");
    $("#txtTenKhachHang").textbox('setValue', "");
    $("#txtSoDienThoai").textbox('setValue', 0);
    $('#txtDiaChi').textbox('setValue', "");
    $('#txtEmail').textbox('setValue', "");
    $('#txtTienNo').textbox('setValue', 0);

}

function add() {
    isAddNew = true;
    $("#frm-KhachHang").window({ title: 'Thêm thông tin khách hàng', left: '15%' });
    $("#frm-KhachHang").window("open");
    resetUI();
}

var rowIndex = null;
function edit() {
    isAddNew = false;
    var row = $('#dg-list').datagrid("getSelected");
    
    rowIndex = $('#dg-list').datagrid('getRowIndex', row);
    if (row == null || row == '') {
        alert("Bạn chưa chọn khách hàng cần chỉnh sửa");
        return;
    }
    bindingData(row);
    $("#frm-KhachHang").window({ title: 'Chỉnh sửa khách hàng', left: '15%' });
    $("#frm-KhachHang").window("open");
}

function removeIt() {
    var rows = $('#dg-list').datagrid("getChecked");
    if (rows == null || rows == '') {
        alert("Bạn vui lòng chọn dòng cần xóa!");
        return;
    }

    $.messager.confirm('Xác nhận', 'Bạn có chắc xóa những khách hàng đã chọn chứ?', function (r) {
        if (r) { //Begin (r) =====================================

            parent.setProgress("Vui lòng chờ");
            beginDelete(rows, 0);

        }//End (r)==============================================
    });
}

function beginDelete(rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang xử lý...");
    $.ajax({
        url: '/QuanLyGara/KhachHangXuLy',
        type: "POST",
        data: { type: 'DELETE', json: JSON.stringify(rows[index]) },
        dataType: "json",
        success: function (data) {
            if (data == true) {
                var rowIndex = $('#dg-list').datagrid("getRowIndex", rows[index]);
                $('#dg-list').datagrid("deleteRow", rowIndex);
            } else {
                parent.setProgressMsg("Lỗi: " + data);
            }
            beginDelete(rows, ++index);
        },
        error: function (response) {
            beginDelete(rows, ++index);
        }
    });
}

function save() {
    if (checkError()) return;
    var row = {};
    row.KhachHangId = $('#txtKhachHangId').textbox('getValue');
    row.TenKhachHang = $('#txtTenKhachHang').textbox('getValue');
    row.SoDienThoai = $('#txtSoDienThoai').textbox('getValue');
    row.DiaChi = $('#txtDiaChi').textbox('getValue');
    row.Email = $('#txtEmail').textbox('getValue');
    row.TienNo = $('#txtTienNo').textbox('getValue');
    if (!isAddNew)
        Update("PUT", row); //Cap nhat
    else {
        Update("POST", row); //Them moi
    }
}



function checkError() {
    var KhachHangId = $('#txtKhachHangId').textbox('getValue');
    if (KhachHangId == "") {
        alert("Mã khách hàng không được để trống.");
        return true;
    }
    if ($.trim($('#txtTenKhachHang').textbox('getValue')) == ""
        || $.trim($('#txtSoDienThoai').textbox('getValue')) == ""
        || $.trim($('#txtEmail').textbox('getValue')) == ""
        || $.trim($('#txtDiaChi').textbox('getValue')) == "") {
        alert("Thông tin không được để trống.");
        return true;
    }
    
}

function Update(type, row) {
    $.ajax({
        url: '/QuanLyGara/KhachHangXuLy',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            //loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
            if (type == "PUT") {
                $('#dg-list').datagrid('updateRow', { index: rowIndex, row });
            }
            if (type == "POST") {
                //$('#dg-list').datagrid('appendRow', row);
                loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
            }
            closeFormNV();
            parent.bottomRight('Thông báo', 'Lưu thành công!');

        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}



function bindingData(row) {
    $("#txtKhachHangId").textbox('setValue', row.KhachHangId);
    $("#txtTenKhachHang").textbox('setValue', row.TenKhachHang);
    $("#txtSoDienThoai").textbox('setValue', row.SoDienThoai);
    $('#txtDiaChi').textbox('setValue', row.DiaChi);
    $('#txtEmail').textbox('setValue', row.Email);
    $('#txtTienNo').textbox('setValue', row.TienNo);
}

function closeFormNV() {
    /*** Đóng form Property ***/
    $("#frm-KhachHang").window("close");
}








//Ho So Xe
var isAddNew = true;

function resetHoSoXe() {
    $("#txtBienSo").textbox('setValue', "");
    $("#txtMaHieuXe").textbox('setValue', "");
}

function tiepnhanxe() {
    var row = $('#dg-list').datagrid("getSelected");
    if (row == null || row == '') {
        alert("bạn chưa chọn khách hàng để lập phiếu");
        return;
    }
    resetHoSoXe();
    bindingTiepNhanXe(row);
    loadHieuXe();
    $("#frm-PhieuTiepNhanXe").window({ title: 'Lập phiếu tiếp nhận xe', left: '15%' });
    $("#frm-PhieuTiepNhanXe").window("open");
}

function bindingTiepNhanXe(row) {
    $("#txtPTNKhachHangId").textbox('setValue', row.KhachHangId);
    $("#txtPTNTenKhachHang").textbox('setValue', row.TenKhachHang);
    $("#txtPTNDiaChi").textbox('setValue', row.DiaChi);
    $("#txtPTNSoDienThoai").textbox('setValue', row.SoDienThoai);
    $("#dtPTNNgayTiepNhan").datebox('setValue', formatJsonDateDMY(Date.now));
}

function GetThongTinHoSoXe() {
    var row = {};
    row.MaHoSo = 0;
    row.BienSo = $('#txtBienSo').textbox('getValue');
    row.MaHieuXe = $('#txtMaHieuXe').combobox('getValue');
    row.KhachHangId = $('#txtPTNKhachHangId').textbox('getValue');
    row.NgayTiepNhan = SetFormatMDY($('#dtPTNNgayTiepNhan').datebox('getValue'));
    row.TienDo = "False";
    SavePhieuTiepNhanXe('POST', row);
}

function SavePhieuTiepNhanXe(type, row) {
    $.ajax({
        url: '/QuanLyGara/SaveHoSoXe',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
            closeFormTNX();
            parent.bottomRight('Thông báo', 'Lưu thành công!');

        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}

function closeFormTNX() {
    /*** Đóng form Property ***/    
    $("#frm-PhieuTiepNhanXe").window("close");
}

function showContextMenu(e) {
    $('#context-menu').menu('show', {
        left: e.pageX,
        top: e.pageY
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
}