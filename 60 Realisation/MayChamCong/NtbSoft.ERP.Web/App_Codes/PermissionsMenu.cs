﻿using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web
{
    public class PermissionsMenu : AuthorizeAttribute
    {
        private readonly Permissions required;

        public PermissionsMenu(Permissions required)
        {
            this.required = required;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
                return false;
            
            var MyRoles = System.Web.Security.Roles.GetRolesForUser(httpContext.User.Identity.Name);
            /***Nếu là tài khoản admin và user thì full quyền ***/
            /***Nếu User hiện tại thuộc 2 nhóm "" và "SYSTEM" thì cho toàn quyền ***/
            if (httpContext.User.Identity.Name.Equals("admin") ||
                httpContext.User.Identity.Name.Equals("user") ||
                MyRoles.Contains("Administrator") || MyRoles.Contains("SYSTEM")) return true;
            
            string MenuId = "";
            if (string.IsNullOrWhiteSpace(httpContext.Request.QueryString["id"]))
                if (httpContext.Request.UrlReferrer == null) return false;
                else
                    MenuId = HttpUtility.ParseQueryString(httpContext.Request.UrlReferrer.Query).GetValues("id").LastOrDefault();
            else
                MenuId = httpContext.Request.QueryString["id"];
            ////var rd = httpContext.Request.RequestContext.RouteData;
            ////var id = rd.Values["id"];
            ////var k = id;
            if (string.IsNullOrWhiteSpace(MenuId)) return false;

            //var userName = httpContext.User.Identity.Name;
            Models.clsMenuMaster _clsMenu = new Models.clsMenuMaster();
            var UserPermission = _clsMenu.GetPermissionMenu(System.Web.HttpContext.Current.User.Identity.Name,
                        MenuId);
            if (UserPermission == null) return false;
            /*** Quyền xem nếu mà quyền view=false thì kiểm tra các quyền còn lại ***/
            if (this.required == Permissions.View)
            {
                if (UserPermission.AllowView)
                    return true;
            }
            /*** Quyền Thêm ***/
            if (this.required == Permissions.Add && UserPermission.AllowAdd)
                return true;
            /*** Quyền chỉnh sửa ***/
            if (this.required == Permissions.Edit && UserPermission.AllowEdit)
                return true;
            /*** Quyền Xóa ***/
            if (this.required == Permissions.Delete && UserPermission.AllowEdit)
                return true;
            httpContext.Error.Source = "Không có quyền truy cập!";
            return false;
        }
    }
}