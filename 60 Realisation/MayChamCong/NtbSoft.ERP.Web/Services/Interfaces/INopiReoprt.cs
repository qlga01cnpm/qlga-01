﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NtbSoft.ERP.Web.Services.Interfaces
{
    public interface INopiReoprt
    {
        void Export(HttpServerUtilityBase Server, DateTime fromDate, DateTime toDate,Action<NPOI.XSSF.UserModel.XSSFWorkbook, Exception> Complete);
    }
}
