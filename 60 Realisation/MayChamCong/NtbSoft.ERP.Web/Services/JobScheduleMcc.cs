﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class JobScheduleMcc
    {
        private static object syncRoot = new Object();
        private static WorkThreadMcc[] _Devices = null;
        public static JobScheduleMcc Instance
        {
            get
            {
                return Nested.Instance;
            }
        }

        class Nested
        {
            //Explicit static constructor to tell # compiler
            // not to mark type as beforefieldinit
            static Nested() { }

            //Quá trình khởi tạo đối tượng được gọi ở lần truy xuất đầu tiên đến static member của lớp con. 
            //Quá trình này chỉ xảy ra trong biến Instance.
            //Như vậy hoàn toàn có tính lazy-load và đồng thời vẫn đảm bảo performance tốt. 
            internal static readonly JobScheduleMcc Instance = new JobScheduleMcc();
        }

        public void Start()
        {
            lock (syncRoot)
            {
                if (_Devices == null)
                {
                    Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
                    
                    _Terminal.GetAll((result, ex) => {
                        if (ex == null)
                        {
                            var Items=result.ToList();
                            _Devices = new WorkThreadMcc[Items.Count];
                            for (int i = 0; i < Items.Count; i++)
                            {
                                _Devices[i] = new WorkThreadMcc(Items[i].TerminalID);
                                ThreadPool.QueueUserWorkItem(_Devices[i].ThreadPoolCallBack);//Put the method into the queue to implement.
                            }
                          
                        }
                    });
                   
                }
            }
        }

        public void Stop()
        {
            if(_Devices!=null)
                foreach (var item in _Devices)
                    item.Cancel = true;
        }
    }
}