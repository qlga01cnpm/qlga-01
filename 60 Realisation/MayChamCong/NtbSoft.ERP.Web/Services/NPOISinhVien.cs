﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
namespace NtbSoft.ERP.Web.Services
{
    public class NPOISinhVien
    {
    //    public NPOI.XSSF.UserModel.XSSFWorkbook Export(HttpServerUtilityBase Server, string DepId, int Years)
    //    {
    //        try
    //        {
    //            //Models.DIC.Interfaces.ITheoDoiPhepNam bcRpt = new Models.DIC.clsTheoDoiPhepNam();
    //            if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\sinhvien.xlsx")))
    //            {
    //                return null;
    //            }
    //            // Opening the Excel template...
    //            FileStream fs =
    //                new FileStream(Server.MapPath(@"\Content\Templates\sinhvien.xlsx"), FileMode.Open, FileAccess.Read);

    //            // Getting the complete workbook...
    //            /*** Excel 2003 *.xls ***/
    //            //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
    //            /*** Excel 2007 or later *.xlsx ***/
    //            NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);

    //            #region Cài đặt Fonts
    //            var hFont = templateWorkbook.CreateFont();
    //            hFont.FontName = "Times New Roman";
    //            //hFont.FontHeightInPoints = 8;
    //            //hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
    //            #endregion


    //            ICellStyle CellStyle = null, CellStyleBoPhan = null;
    //            // Getting the worksheet by its name...
    //            NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("Danh_sach_sinh_vien");
    //            IRow iRow = null;
                


    //            CellStyle = templateWorkbook.CreateCellStyle();
    //            CellStyle.BorderBottom = BorderStyle.Dashed;
    //            CellStyle.BorderRight = BorderStyle.Thin;
    //            CellStyle.SetFont(hFont);
    //            CellStyle.Alignment = HorizontalAlignment.Center;
    //            CellStyle.VerticalAlignment = VerticalAlignment.Center;

    //            #region CellStyle Ten Nhan vien
    //            var CellStyleTenNV = templateWorkbook.CreateCellStyle();
    //            CellStyleTenNV.Alignment = HorizontalAlignment.Left;
    //            CellStyleTenNV.VerticalAlignment = VerticalAlignment.Center;
    //            CellStyleTenNV.BorderBottom = BorderStyle.Dashed;
    //            CellStyleTenNV.BorderRight = BorderStyle.Thin;
    //            CellStyleTenNV.SetFont(hFont);
    //            #endregion

    //            #region Cell Style Bo Phan
    //            CellStyleBoPhan = templateWorkbook.CreateCellStyle();
    //            CellStyleBoPhan.Alignment = HorizontalAlignment.Left;
    //            CellStyleBoPhan.VerticalAlignment = VerticalAlignment.Center;
    //            CellStyleBoPhan.BorderBottom = BorderStyle.Dashed;
    //            CellStyleBoPhan.BorderRight = BorderStyle.Thin;
    //            CellStyleBoPhan.SetFont(hFont);
    //            #endregion
    //            int startRow = 3, countRow = 0;

    //            List<Entity.Interfaces.IDSSinhVienInfo> mItems = new List<Entity.Interfaces.IDSSinhVienInfo>();
    //            Models.Interfaces.IResultOk ok = null;
    //            //bcRpt.GetBy(0, Years, DepId, "", (result, ex) => { mItems = result; ok = ex; });
    //            if (mItems == null) mItems = new List<Entity.Interfaces.IDSSinhVienInfo>();
    //            //if (ok.Errors)
    //            countRow = mItems.Count;

    //            for (int i = 0; i < countRow; i++)
    //            {
    //                if (sheet.GetRow(startRow + i) == null)
    //                    iRow = sheet.CreateRow(startRow + i);
    //                else
    //                    iRow = sheet.GetRow(startRow + i);

    //                var iCell = iRow.CreateCell(0);
    //                iCell.SetCellValue((i + 1));
    //                iCell.CellStyle = CellStyle;
    //                iCell.CellStyle.Alignment = HorizontalAlignment.Center;
    //                iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;


    //                #region Cột Mã Nhân viên
    //                iCell = iRow.CreateCell(1);
    //                iCell.SetCellValue(mItems[i].MaNV);
    //                iCell.CellStyle = CellStyle;
    //                #endregion

    //                #region Cột "Họ và tên Nhân viên"
    //                iCell = iRow.CreateCell(2);
    //                iCell.SetCellValue(mItems[i].TenNV);
    //                iCell.CellStyle = CellStyleTenNV;


    //                #endregion

    //                #region Cột Bộ phận
    //                iCell = iRow.CreateCell(3);
    //                iCell.SetCellValue(mItems[i].DepName);
    //                iCell.CellStyle = CellStyleBoPhan;

    //                #endregion

    //                #region Cột từ ngày vào
    //                iCell = iRow.CreateCell(4);
    //                iCell.SetCellValue(mItems[i].NgayVao);
    //                iCell.CellStyle = CellStyle;
    //                #endregion

    //                #region Cột số ngày phép
    //                iCell = iRow.CreateCell(5);
    //                iCell.SetCellValue(mItems[i].SoNgayPhep);
    //                iCell.CellStyle = CellStyle;
    //                #endregion

    //                #region Cột tháng 01
    //                iCell = iRow.CreateCell(6);
    //                iCell.SetCellValue(mItems[i].M1);
    //                iCell.CellStyle = CellStyle;
    //                #endregion



    //                #region Cột tháng 02
    //                iCell = iRow.CreateCell(7);
    //                iCell.SetCellValue(mItems[i].M2);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 03
    //                iCell = iRow.CreateCell(8);
    //                iCell.SetCellValue(mItems[i].M3);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 04
    //                iCell = iRow.CreateCell(9);
    //                iCell.SetCellValue(mItems[i].M4);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 05
    //                iCell = iRow.CreateCell(10);
    //                iCell.SetCellValue(mItems[i].M5);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 06
    //                iCell = iRow.CreateCell(11);
    //                iCell.SetCellValue(mItems[i].M6);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 07
    //                iCell = iRow.CreateCell(12);
    //                iCell.SetCellValue(mItems[i].M7);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 08
    //                iCell = iRow.CreateCell(13);
    //                iCell.SetCellValue(mItems[i].M8);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 09
    //                iCell = iRow.CreateCell(14);
    //                iCell.SetCellValue(mItems[i].M9);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 10
    //                iCell = iRow.CreateCell(15);
    //                iCell.SetCellValue(mItems[i].M10);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 11
    //                iCell = iRow.CreateCell(16);
    //                iCell.SetCellValue(mItems[i].M11);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột tháng 12
    //                iCell = iRow.CreateCell(17);
    //                iCell.SetCellValue(mItems[i].M12);
    //                iCell.CellStyle = CellStyle;
    //                #endregion


    //                #region Cột số ngày đã nghỉ
    //                iCell = iRow.CreateCell(18);
    //                iCell.SetCellValue(mItems[i].SoNgayNghi);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //                #region Cột số ngày phép còn lại
    //                iCell = iRow.CreateCell(19);
    //                iCell.SetCellValue(mItems[i].SoNgayConLai);
    //                iCell.CellStyle = CellStyle;
    //                #endregion
    //            }

    //            #region Dòng cuối cùng 
    //            CellStyle = templateWorkbook.CreateCellStyle();
    //            CellStyle.BorderBottom = BorderStyle.Thin;
    //            CellStyle.BorderRight = BorderStyle.Thin;
    //            if (sheet.GetRow(startRow + countRow) == null)
    //                iRow = sheet.CreateRow(startRow + countRow);
    //            else
    //                iRow = sheet.GetRow(startRow + countRow);
    //            var iCell1 = iRow.CreateCell(0);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(1);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(2);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(3);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(4);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(5);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(6);
    //            iCell1.CellStyle = CellStyle;


    //            iCell1 = iRow.CreateCell(7);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(8);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(9);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(10);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(11);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(12);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(13);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(14);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(15);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(16);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(17);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(18);
    //            iCell1.CellStyle = CellStyle;
    //            iCell1 = iRow.CreateCell(19);
    //            iCell1.CellStyle = CellStyle;
    //            #endregion

    //            return templateWorkbook;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }


    }
}
