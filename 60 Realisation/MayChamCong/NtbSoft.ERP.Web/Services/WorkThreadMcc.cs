﻿using NtbSoft.Entities.Interfaces.MCC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class WorkThreadMcc
    {
        //string sIP = "0.0.0.0";
        //int iPort = 4370;
        //int iMachineNumber = 1;
        int iThreadID = 0;
        int iLastTry = 0;//the former trying time(in mimutes) 
        int iDelay = 2;//to control the times connecting device 
        private int iCounter = 0;
        private int _MachineNumber = 0;
        public bool Cancel { get; set; }
        //work thread
        public WorkThreadMcc(int MachineNumber)
        {
            this._MachineNumber = MachineNumber;
            iThreadID = ++iCounter;
        }

        //call back function of threadpool
        public void ThreadPoolCallBack(Object oThreadContext)
        {
            int iCurrentTime = 0;
            iLastTry = GetTimeInMinute();
            Cancel = false;
            while (true)
            {
                if (Cancel) break;
                iCurrentTime = GetTimeInMinute();
                //sleep until the minute ticks
                while (iLastTry == iCurrentTime)
                {
                    if (Cancel) break;
                    Thread.Sleep(30);
                    iCurrentTime = GetTimeInMinute();
                }
                iLastTry = iCurrentTime;
                iDelay--;
                if (iDelay == 1)
                {
                    this.WakeUp();
                }
            }
        }

        private void WakeUp()
        {
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(_MachineNumber, (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });
            
            if (_ex==null && Item != null)
            {
                Devices.MCC.Services.Interfaces.IAttLogs _attLogs;
                if (Item.CommType == 1) { }
                //_attLogs= new Devices.MCC.Services.AttLogs(1,new Devices.MCC.ConnectCOM()
                else if (Item.CommType == 2)
                {
                    _attLogs = new Devices.MCC.Services.AttLogs(_MachineNumber, Item.LastRecordDate, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    List<ICardUserInfo> _listUserLogs = null;

                    _attLogs.Connect((connected, ex) =>
                    {
                        if (connected)
                        {
                            _attLogs.DownloadAttLogs((result, ex1) =>
                            {
                                if (result != null)
                                {
                                    _listUserLogs = result.Select(i =>
                                    {
                                        if (!string.IsNullOrWhiteSpace(i.UserId)) i.MachineNumber = _MachineNumber;
                                        return i;
                                    }).ToList();

                                }
                            });
                        }
                    });

                    if (_listUserLogs != null && _listUserLogs.Count > 0)
                    {
                        NtbSoft.ERP.Models.Interfaces.IQuyTacLamTron QuyTacLamTron = new NtbSoft.ERP.Models.clsQuyTacLamTron();
                        NtbSoft.ERP.Models.Interfaces.IQuyTac QuyTacCC = new NtbSoft.ERP.Models.clsQuyTac(QuyTacLamTron);
                        Models.DIC.Interfaces.IDangKyCheDo _DangKyCheDo = new NtbSoft.ERP.Models.DIC.clsDangKyCheDo();
                        Models.Interfaces.INhanVien _nhanvien = new NtbSoft.ERP.Models.clsNhanVienCC();
                        Models.Interfaces.IShift _clsShifts = new Models.clsShift();
                        //Lịch trình ca
                        Models.Interfaces.IScheduleSchift _ScheduleShift = new Models.clsScheduleShift();

                        NtbSoft.ERP.Models.Interfaces.IBase<ICardUserInfo> _baseBus =
                            new ERP.Models.clsAttLogs(_nhanvien,QuyTacCC, _DangKyCheDo,_clsShifts,_ScheduleShift);
                        _baseBus.BatchInsert(_listUserLogs, ex => { });
                    }
                }
            }

            iDelay = 2;
        }

        private int GetTimeInMinute()//return the time in mimutes
        {
            return ((DateTime.Now.Hour * 24) + DateTime.Now.Minute);
        }
    }
}