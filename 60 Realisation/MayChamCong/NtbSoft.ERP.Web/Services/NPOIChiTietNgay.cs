﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class NPOIChiTietNgay
    {
        /// <summary>
        /// ArrayEmpID chuỗi mảng nhân viên (vd: 1,2,3,...)
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="ArrayEmpID"></param>
        /// <returns></returns>
        public NPOI.XSSF.UserModel.XSSFWorkbook Export(HttpServerUtilityBase Server, DateTime fromDate, DateTime toDate, string ArrayEmpID)
        {
            try
            {
                Models.MCC.Interfaces.IBcChiTietNgay bcRpt = new Models.MCC.clsBcChiTietNgay();
                if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                    bcRpt = new Models.MCC.clsBcChiTietNgayVirtual();

                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_chi_tiet_ngay.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_chi_tiet_ngay.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
                
                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                //hFont.FontHeightInPoints = 8;
                //hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion
                string company = "", address = "", fromDate_toDate = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;
                fromDate_toDate = string.Format("Ngày {0}", fromDate.ToString("dd/MM/yyyy"));
                if (!fromDate.ToString("dd/MM/yyyy").Equals(toDate.ToString("dd/MM/yyyy")))
                    fromDate_toDate = string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));

                ICellStyle CellStyle = null, CellStyleBoPhan = null;
                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("BcVaoRa");
                IRow iRow = null;
                /*** Lấy dòng tên công ty ***/
                iRow = sheet.GetRow(0);
                iRow.GetCell(2).SetCellValue(company);
                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(1);
                iRow.GetCell(2).SetCellValue(address);
                /*** Lấy dòng Tiêu đề ***/
                iRow = sheet.GetRow(2);
                //iRow.GetCell(2).SetCellValue(GetTitleReport(MaCheDo));
                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(3);
                iRow.GetCell(2).SetCellValue(fromDate_toDate);


                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Dashed;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                #region CellStyle nhân viên
                var CellStyleTenNV = templateWorkbook.CreateCellStyle();
                CellStyleTenNV.Alignment = HorizontalAlignment.Left;
                CellStyleTenNV.VerticalAlignment = VerticalAlignment.Center;
                CellStyleTenNV.BorderBottom = BorderStyle.Dashed;
                CellStyleTenNV.BorderRight = BorderStyle.Thin;
                CellStyleTenNV.SetFont(hFont);
                #endregion

                #region Cell Style Bo Phan
                CellStyleBoPhan = templateWorkbook.CreateCellStyle();
                CellStyleBoPhan.Alignment = HorizontalAlignment.Left;
                CellStyleBoPhan.VerticalAlignment = VerticalAlignment.Center;
                CellStyleBoPhan.BorderBottom = BorderStyle.Dashed;
                CellStyleBoPhan.BorderRight = BorderStyle.Thin;
                CellStyleBoPhan.SetFont(hFont);
                #endregion

                int startRow = 6, countRow = 0;
                //Lấy dữ liệu không chấm công
                System.Data.DataTable tbl = null;
                bcRpt.GetBy(fromDate, toDate, ArrayEmpID, (result, ex) => { tbl = result; });
                if (tbl == null) tbl = new System.Data.DataTable();
                countRow = tbl.Rows.Count;
                for (int i = 0; i < countRow; i++)
                {
                    if (sheet.GetRow(startRow+i) == null)
                        iRow = sheet.CreateRow(startRow + i);
                    else
                        iRow = sheet.GetRow(startRow + i);

                    var iCell = iRow.CreateCell(0);
                    iCell.SetCellValue((i + 1));
                    iCell.CellStyle = CellStyle;
                    iCell.CellStyle.Alignment = HorizontalAlignment.Center;
                    iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;


                    #region Cột Mã Nhân viên
                    iCell = iRow.CreateCell(1);
                    iCell.SetCellValue(tbl.Rows[i]["MaNV2"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột "Họ và tên Nhân viên"
                    iCell = iRow.CreateCell(2);
                    iCell.SetCellValue(tbl.Rows[i]["TenNV"].ToString());
                    iCell.CellStyle = CellStyleTenNV;
                    #endregion

                    #region Cột Bộ phận
                    iCell = iRow.CreateCell(3);
                    iCell.SetCellValue(tbl.Rows[i]["DepName"].ToString());
                    iCell.CellStyle = CellStyleBoPhan;
                   
                    #endregion

                    #region Cột ngày
                    iCell = iRow.CreateCell(4);
                    iCell.SetCellValue(tbl.Rows[i]["WorkingdayDMY"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ đến
                    iCell = iRow.CreateCell(5);
                    iCell.SetCellValue(tbl.Rows[i]["TimeIn"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ về
                    iCell = iRow.CreateCell(6);
                    iCell.SetCellValue(tbl.Rows[i]["TimeOut"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột đi muộn
                    iCell = iRow.CreateCell(7);
                    iCell.SetCellValue(Libs.Utilities.clsFormat.DoubleConvert(tbl.Rows[i]["LateTime"].ToString()));
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột về sớm
                    iCell = iRow.CreateCell(8);
                    iCell.SetCellValue(Libs.Utilities.clsFormat.DoubleConvert(tbl.Rows[i]["EarlyTime"].ToString()));
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ
                    iCell = iRow.CreateCell(9);
                    iCell.SetCellValue(Libs.Utilities.clsFormat.DoubleConvert(tbl.Rows[i]["TotalHours"].ToString()));
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ tăng ca
                    iCell = iRow.CreateCell(10);
                    iCell.SetCellValue(Math.Round(Libs.Utilities.clsFormat.DoubleConvert(tbl.Rows[i]["OTX"].ToString()) / 60f, 1));
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột lý do
                    iCell = iRow.CreateCell(11);
                    iCell.SetCellValue("");
                    iCell.CellStyle = CellStyle;
                    #endregion
                }

                #region Dòng cuối cùng 
                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                if (sheet.GetRow(startRow + countRow) == null)
                    iRow = sheet.CreateRow(startRow + countRow);
                else
                    iRow = sheet.GetRow(startRow + countRow);
                var iCell1 = iRow.CreateCell(0);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(1);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(2);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(3);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(4);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(5);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(6);
                iCell1.CellStyle = CellStyle;


                iCell1 = iRow.CreateCell(7);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(8);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(9);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(10);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(11);
                iCell1.CellStyle = CellStyle;
                #endregion

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


    }
}