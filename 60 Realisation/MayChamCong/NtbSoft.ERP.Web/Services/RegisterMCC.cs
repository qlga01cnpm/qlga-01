﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class RegisterMCC
    {
        private Entity.Interfaces.ITerminalInfo _Item;
        ERP.RegMCC.clsReg clsDangKy;
        public RegisterMCC(Entity.Interfaces.ITerminalInfo Item)
        {
            _Item = Item;
            clsDangKy = new RegMCC.clsReg(_Item.SerialNumber);
        }
        public bool IsKeyOK()
        {
            return clsDangKy.IsKeyOK(this._Item.ActiveKey);
        }
    }
}