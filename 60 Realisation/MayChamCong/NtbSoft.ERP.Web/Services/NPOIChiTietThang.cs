﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class NPOIChiTietThang
    {
        public NPOI.XSSF.UserModel.XSSFWorkbook BcChiTietVaoRa(HttpServerUtilityBase Server, int Month, int Year, string ArrayId)
        {
            try
            {
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_chi_tiet_thang_ctvr.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_chi_tiet_thang_ctvr.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);

                string company = "", address = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;

                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("BCThang");
                IRow iRow = null;
                /*** Lấy dòng tên công ty ***/
                iRow = sheet.GetRow(0);
                iRow.GetCell(3).SetCellValue(company);
                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(1);
                iRow.GetCell(3).SetCellValue(address);

                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(3);
                iRow.GetCell(3).SetCellValue(string.Format("THÁNG {0} NĂM {1}", Month, Year));

                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                hFont.FontHeightInPoints = 8;
                hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion

                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                #region Cột chủ nhật
                var CellStyleCN = templateWorkbook.CreateCellStyle();
                CellStyleCN.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                CellStyleCN.FillPattern = FillPattern.SolidForeground;
                CellStyleCN.BorderBottom = BorderStyle.Thin;
                CellStyleCN.SetFont(hFont);

                #endregion

                /*** Dòng thứ 5 ***/
                sheet.GetRow(5).GetCell(5).SetCellValue("NGÀY LÀM VIỆC TRONG THÁNG");
                sheet.GetRow(5).GetCell(5).CellStyle = CellStyle;

                /*** Dòng thữ 6 cài đặt các ngày trong tháng ***/
                iRow = sheet.GetRow(6);
                //sheet.GetMergedRegion()
                //int Month = DateTime.Now.Month, year = DateTime.Now.Year;
                DateTime lastDay = Libs.clsCommon.LastDayOfMonth(Month, Year);
                int StartColumn = 4, Day = 0;

                //col: là cột ngày (1->31)
                for (Day = 1; Day <= lastDay.Day; Day++)
                {
                    DateTime dtTemp = new DateTime(Year, Month, Day);
                    string t = "";
                    if (dtTemp.DayOfWeek == DayOfWeek.Sunday) t = "CN";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Monday) t = "T2";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Tuesday) t = "T3";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Wednesday) t = "T4";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Thursday) t = "T5";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Friday) t = "T6";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Saturday) t = "T7";

                    string value = string.Format("{0}-{1}", Day, t);
                    if (iRow.GetCell(StartColumn + Day) == null)
                        iRow.CreateCell(StartColumn + Day);
                    iRow.GetCell(StartColumn + Day).SetCellValue(value);
                    if (t == "CN")
                        iRow.GetCell(StartColumn + Day).CellStyle = CellStyleCN;
                    else
                        iRow.GetCell(StartColumn + Day).CellStyle = CellStyle;
                    /*** Set column width ***/
                    sheet.SetColumnWidth(StartColumn + Day, (int)(5.27 * 256));
                }
                this.AddMergedRegion(1, 5, 5, StartColumn + 1, Day + 3, ref sheet, ref templateWorkbook);

                int cellTotal = StartColumn + Day, nextCell = 0;
                //Them cot gio lam them 
                this.AddMergedRegion(1, 5, 6, cellTotal, cellTotal, ref sheet, ref templateWorkbook);
                sheet.GetRow(5).GetCell(cellTotal).SetCellValue("Giờ làm thêm");
                //Cột tổng ngày công
                nextCell = 1;
                this.AddMergedRegion(1, 5, 6, cellTotal + nextCell, cellTotal + nextCell, ref sheet, ref templateWorkbook);
                sheet.GetRow(5).GetCell(cellTotal + nextCell).SetCellValue("Tổng ngày công");
                //Cột ký tên
                nextCell = 2;
                this.AddMergedRegion(1, 5, 6, cellTotal + nextCell, cellTotal + nextCell, ref sheet, ref templateWorkbook);
                sheet.GetRow(5).GetCell(cellTotal + nextCell).SetCellValue("Ký tên");

                //Tạo đường kẻ viền (Borders)
                ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                CellStyle3.BorderBottom = BorderStyle.Dashed;
                CellStyle3.BorderRight = BorderStyle.Thin;
                CellStyle3.Alignment = HorizontalAlignment.Center;
                CellStyle3.VerticalAlignment = VerticalAlignment.Center;

                #region TEst
                //CellStyle3.SetFont(font);
                /*** BẮT ĐẦU DÒNG DỮ LIỆU ***/
                ////IRow row = sheet.CreateRow(7);
                ////var cell = row.CreateCell(0);
                ////cell.SetCellValue(1);
                ////cell.CellStyle = CellStyle3;
                ////this.CreateRow(7, 1, "11119", CellStyle3, ref sheet);
                ////this.CreateRow(7, 2, "Hoàng Đức Vị", CellStyle3, ref sheet);

                ////this.CreateRow(8, 0, null, CellStyle3, ref sheet);
                ////this.CreateRow(8, 1, null, CellStyle3, ref sheet);
                ////this.CreateRow(8, 2, null, CellStyle3, ref sheet);
                //////row = sheet.CreateRow(8);
                //////cell.CellStyle = CellStyle3;
                ////this.CreateRow(9, 0, null, CellStyle3, ref sheet);
                ////this.CreateRow(9, 1, null, CellStyle3, ref sheet);
                ////this.CreateRow(9, 2, null, CellStyle3, ref sheet);

                ////this.AddMergedRegion(1, 7, 9, 0, 0, ref sheet, ref templateWorkbook);
                ////this.AddMergedRegion(1, 7, 9,1, 1, ref sheet, ref templateWorkbook);
                ////this.AddMergedRegion(1, 7, 9, 2, 2, ref sheet, ref templateWorkbook);
                //var cellRange = new CellRangeAddress(7, 9, 0, 0);
                //sheet.AddMergedRegion(cellRange);
                //RegionUtil.SetBorderRight(1, cellRange, sheet, templateWorkbook);
                //RegionUtil.SetBorderBottom(1, cellRange, sheet, templateWorkbook);
                #endregion

                Models.MCC.Interfaces.ITimeSheet TimeSheet = new Models.MCC.clsTimeSheet();
                if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                    TimeSheet = new Models.MCC.clsTimeSheetVirtual();

                System.Data.DataTable tbl = null;
                Models.Interfaces.IResultOk ok = null;
                TimeSheet.GetBy(Month, Year, ArrayId, (result, ex) => { tbl = result; ok = ex; });
                if (ok.Success && tbl.Rows.Count > 0)
                {
                    #region Font Cell data
                    var hFontCell = templateWorkbook.CreateFont();
                    hFontCell.FontName = "Times New Roman";
                    hFontCell.FontHeightInPoints = 8; //Font Size
                    #endregion

                    #region Border Cot Thoi gian, Nghi Phep
                    ICellStyle cellStyleThoiGianNghiPhep = templateWorkbook.CreateCellStyle();
                    cellStyleThoiGianNghiPhep.BorderRight = BorderStyle.Thin;
                    cellStyleThoiGianNghiPhep.BorderBottom = BorderStyle.Thin;
                    cellStyleThoiGianNghiPhep.BorderLeft = BorderStyle.Thin;
                    cellStyleThoiGianNghiPhep.SetFont(hFontCell);
                    #endregion

                    #region Border In
                    var cellStyleIn = templateWorkbook.CreateCellStyle();
                    cellStyleIn.BorderTop = BorderStyle.Thin;
                    cellStyleIn.BorderRight = BorderStyle.Thin;
                    cellStyleIn.BorderBottom = BorderStyle.Dashed;
                    cellStyleIn.BorderLeft = BorderStyle.Thin;
                    cellStyleIn.SetFont(hFontCell);
                    #endregion

                    #region Border Out
                    var cellStyleOut = templateWorkbook.CreateCellStyle();
                    cellStyleOut.BorderRight = BorderStyle.Thin;
                    cellStyleOut.BorderBottom = BorderStyle.Dashed;
                    cellStyleOut.BorderLeft = BorderStyle.Thin;
                    cellStyleOut.SetFont(hFontCell);

                    #endregion

                    #region Dòng label Vào

                    var cellStyleLabelIn = templateWorkbook.CreateCellStyle();
                    cellStyleLabelIn.Alignment = HorizontalAlignment.Center;
                    cellStyleLabelIn.BorderTop = BorderStyle.Thin;
                    cellStyleLabelIn.BorderRight = BorderStyle.Thin;
                    cellStyleLabelIn.BorderBottom = BorderStyle.Dashed;
                    cellStyleLabelIn.SetFont(hFontCell);
                    #endregion

                    #region Dòng label Ra

                    var cellStyleLabelOut = templateWorkbook.CreateCellStyle();
                    cellStyleLabelOut.Alignment = HorizontalAlignment.Center;
                    cellStyleLabelOut.BorderTop = BorderStyle.Dashed;
                    cellStyleLabelOut.BorderBottom = BorderStyle.Dashed;
                    cellStyleLabelOut.SetFont(hFontCell);
                    #endregion

                    #region Others
                    var cellStyleLabelOther = templateWorkbook.CreateCellStyle();
                    cellStyleLabelOther.BorderBottom = BorderStyle.Thin;
                    cellStyleLabelOther.SetFont(hFontCell);
                    #endregion

                    #region Các cột cuối
                    var cellStyleLast = templateWorkbook.CreateCellStyle();
                    cellStyleLast.VerticalAlignment = VerticalAlignment.Center;
                    cellStyleLast.BorderRight = BorderStyle.Thin;
                    cellStyleLast.SetFont(hFontCell);
                    #endregion

                    int no = 0, rowNum = 7;
                    List<System.Data.DataRow> rows = tbl.Select().ToList();
                    while (rows.Count > 0)
                    {
                        no++;
                        CreateEmp(no, ref rowNum, rows[0], Day - 1, CellStyle3, ref sheet, ref templateWorkbook,
                            cellStyleIn,
                            cellStyleOut,
                            cellStyleLabelIn,
                            cellStyleLabelOut,
                            cellStyleLabelOther,
                            cellStyleThoiGianNghiPhep,
                            cellStyleLast);
                        rows.Remove(rows[0]);
                    }
                    //foreach (System.Data.DataRow row in tbl.Rows)
                    //{
                    //    no++;
                    //    CreateEmp(no, ref rowNum, row, Day - 1, CellStyle3, ref sheet, ref templateWorkbook);


                    //}

                    #region Đổ màu những ô là ngày chủ nhật
                    int rowIndex = 7;
                    ICellStyle _CelStyleEnd = templateWorkbook.CreateCellStyle();
                    _CelStyleEnd.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                    _CelStyleEnd.FillPattern = FillPattern.SolidForeground;
                    while (rowIndex < rowNum)
                    {
                        int dayTemp = 1;
                        while (dayTemp <= lastDay.Day)
                        {
                            if (Libs.clsCommon.GetDayOfWeek(Month, dayTemp, Year) == DayOfWeek.Sunday)
                            {
                                iRow = sheet.GetRow(rowIndex);
                                if (iRow.GetCell(StartColumn + dayTemp) == null)
                                    iRow.CreateCell(StartColumn + dayTemp);

                                iRow.GetCell(StartColumn + dayTemp).CellStyle = _CelStyleEnd;
                                //Kẻ khung ở dưới bằng đường thẳng
                                if (rowIndex == rowNum - 1)
                                    iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Thin;
                                else
                                    iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Dashed;
                            }
                            dayTemp++;
                        }
                        rowIndex++;
                    }

                    #endregion
                }

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public NPOI.XSSF.UserModel.XSSFWorkbook ExcelMonthly(HttpServerUtilityBase Server, int month, int year, string depId, string maNv)
        {
            try
            {
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_chi_tiet_thang.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_chi_tiet_thang.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
                string company = "", address = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;

                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("BCThang");

                sheet.SetColumnHidden(3, true);

                IRow iRow = null;
                /*** Lấy dòng tên công ty ***/
                iRow = sheet.GetRow(0);
                iRow.GetCell(3).SetCellValue(company);
                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(1);
                iRow.GetCell(3).SetCellValue(address);

                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(2);
                iRow.GetCell(12).SetCellValue("BÁO CÁO CHẤM CÔNG THÁNG");

                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(3);
                iRow.GetCell(3).SetCellValue(string.Format("THÁNG {0} NĂM {1}", month, year));

                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                hFont.FontHeightInPoints = 8;
                hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion

                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                #region Cột chủ nhật
                var CellStyleCN = templateWorkbook.CreateCellStyle();
                CellStyleCN.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                CellStyleCN.FillPattern = FillPattern.SolidForeground;
                CellStyleCN.BorderBottom = BorderStyle.Thin;
                CellStyleCN.SetFont(hFont);

                #endregion

                /*** Dòng thứ 5 ***/
                sheet.GetRow(5).GetCell(4).SetCellValue("NGÀY LÀM VIỆC TRONG THÁNG");
                sheet.GetRow(5).GetCell(4).CellStyle = CellStyle;
                /*** Dòng thữ 6 cài đặt các ngày trong tháng ***/
                iRow = sheet.GetRow(6);
                //sheet.GetMergedRegion()
                //int Month = DateTime.Now.Month, year = DateTime.Now.Year;
                DateTime lastDay = Libs.clsCommon.LastDayOfMonth(month, year);
                int StartColumn = 3, Day = 0;

                //col: là cột ngày (1->31)
                for (Day = 1; Day <= lastDay.Day; Day++)
                {
                    DateTime dtTemp = new DateTime(year, month, Day);
                    string t = "";
                    if (dtTemp.DayOfWeek == DayOfWeek.Sunday) t = "CN";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Monday) t = "T2";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Tuesday) t = "T3";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Wednesday) t = "T4";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Thursday) t = "T5";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Friday) t = "T6";
                    else if (dtTemp.DayOfWeek == DayOfWeek.Saturday) t = "T7";

                    string value = string.Format("{0}-{1}", Day, t);
                    if (iRow.GetCell(StartColumn + Day) == null)
                        iRow.CreateCell(StartColumn + Day);
                    iRow.GetCell(StartColumn + Day).SetCellValue(value);
                    if (t == "CN")
                        iRow.GetCell(StartColumn + Day).CellStyle = CellStyleCN;
                    else
                        iRow.GetCell(StartColumn + Day).CellStyle = CellStyle;
                    /*** Set column width ***/
                    sheet.SetColumnWidth(StartColumn + Day, (int)(5.27 * 256));
                }
                this.AddMergedRegion(1, 5, 5, StartColumn + 1, Day + 1, ref sheet, ref templateWorkbook);

                //Tạo đường kẻ viền (Borders)
                ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                CellStyle3.BorderBottom = BorderStyle.Dashed;
                CellStyle3.BorderRight = BorderStyle.Thin;
                CellStyle3.Alignment = HorizontalAlignment.Center;
                CellStyle3.VerticalAlignment = VerticalAlignment.Center;

                Models.MCC.Interfaces.ITimeSheet TimeSheet = new Models.MCC.clsTimeSheet();
                if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                    TimeSheet = new Models.MCC.clsTimeSheetVirtual();

                System.Data.DataTable tbl = null;
                Models.Interfaces.IResultOk ok = null;
                TimeSheet.ExportMonthly(month, year, depId, maNv, (result, ex) => { tbl = result; ok = ex; });
                if (ok.Success && tbl.Rows.Count > 0)
                {
                    #region Font Cell data
                    var hFontCell = templateWorkbook.CreateFont();
                    hFontCell.FontName = "Times New Roman";
                    hFontCell.FontHeightInPoints = 8; //Font Size
                    #endregion

                    int no = 0, rowNum = 7;
                    List<System.Data.DataRow> rows = tbl.Select().ToList();
                    while (rows != null && rows.Count > 0)
                    {
                        no++;

                        this.CreateRow(rowNum, 0, no.ToString(), CellStyle3, ref sheet); //Cột số thứ tự
                        this.CreateRow(rowNum, 1, rows[0]["MaNV"].ToString(), CellStyle3, ref sheet);   //Cột mã số
                        this.CreateRow(rowNum, 2, rows[0]["TenNV"].ToString(), CellStyle3, ref sheet);    //Cột họ và tên

                        int j = 0;
                        StartColumn = 3;
                        while (j < (Day - 1))
                        {
                            j++;
                            var value = rows[0][string.Format("K{0}", j)].ToString();
                            this.CreateRow(rowNum, (StartColumn + j), value, CellStyle3, ref sheet);
                        }
                        rowNum++;
                        rows.Remove(rows[0]);
                    }

                    #region Đổ màu những ô là ngày chủ nhật
                    int rowIndex = 7;
                    ICellStyle _CelStyleEnd = templateWorkbook.CreateCellStyle();
                    _CelStyleEnd.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                    _CelStyleEnd.FillPattern = FillPattern.SolidForeground;
                    while (rowIndex < rowNum)
                    {
                        int dayTemp = 1;
                        while (dayTemp <= lastDay.Day)
                        {
                            if (Libs.clsCommon.GetDayOfWeek(month, dayTemp, year) == DayOfWeek.Sunday)
                            {
                                iRow = sheet.GetRow(rowIndex);
                                if (iRow.GetCell(StartColumn + dayTemp) == null)
                                    iRow.CreateCell(StartColumn + dayTemp);

                                iRow.GetCell(StartColumn + dayTemp).CellStyle = _CelStyleEnd;
                                //Kẻ khung ở dưới bằng đường thẳng
                                if (rowIndex == rowNum - 1)
                                    iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Thin;
                                else
                                    iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Dashed;
                            }
                            dayTemp++;
                        }
                        rowIndex++;
                    }

                    #endregion
                }

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void CreateEmp(int No, ref int rowNum, System.Data.DataRow row, int colDay, 
            ICellStyle cellStyle, ref NPOI.SS.UserModel.ISheet sheet, 
            ref NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook,
            ICellStyle cellStyleIn,
            ICellStyle cellStyleOut,
            ICellStyle cellStyleLabelIn,
            ICellStyle cellStyleLabelOut,
            ICellStyle cellStyleLabelOther,
            ICellStyle cellStyleThoiGianNghiPhep,
            ICellStyle cellStyleLast)
        {
            #region Dòng giờ vào
            this.CreateRow(rowNum, 0, No.ToString(), cellStyle, ref sheet); //Cột số thứ tự
            this.CreateRow(rowNum, 1, row["MaNV2"].ToString(), cellStyle, ref sheet);   //Cột mã số
            this.CreateRow(rowNum, 2, row["TenNV"].ToString(), cellStyle, ref sheet);    //Cột họ và tên
            this.CreateRow(rowNum, 3, row["DepName"].ToString(), cellStyle, ref sheet);    //Cột nhóm tổ
            #endregion

            #region Dòng giờ ra
            this.CreateRow(rowNum + 1, 0, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 1, 1, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 1, 2, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 1, 3, null, cellStyle, ref sheet);
            #endregion

            #region Dòng tổng số số giờ làm hoặc, nghỉ
            this.CreateRow(rowNum + 2, 0, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 2, 1, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 2, 2, null, cellStyle, ref sheet);
            this.CreateRow(rowNum + 2, 3, null, cellStyle, ref sheet);
            #endregion

            this.AddMergedRegion(1, rowNum, rowNum + 2, 0, 0, ref sheet, ref templateWorkbook);
            this.AddMergedRegion(1, rowNum, rowNum + 2, 1, 1, ref sheet, ref templateWorkbook);
            this.AddMergedRegion(1, rowNum, rowNum + 2, 2, 2, ref sheet, ref templateWorkbook);
            this.AddMergedRegion(1, rowNum, rowNum + 2, 3, 3, ref sheet, ref templateWorkbook);

            int j = 0, StartColumn = 4;
            ICell iCell;

            #region Dòng label Vào
            if (sheet.GetRow(rowNum) == null)
                sheet.CreateRow(rowNum);
            if (sheet.GetRow(rowNum).GetCell(4) == null)
                sheet.GetRow(rowNum).CreateCell(4);
            iCell = sheet.GetRow(rowNum).GetCell(4);
            iCell.SetCellValue("Vào");
            iCell.CellStyle = cellStyleLabelIn;
            #endregion

            #region Dòng label Ra
            if (sheet.GetRow(rowNum + 1) == null)
                sheet.CreateRow(rowNum + 1);
            if (sheet.GetRow(rowNum + 1).GetCell(4) == null)
                sheet.GetRow(rowNum + 1).CreateCell(4);
            iCell = null;
            iCell = sheet.GetRow(rowNum + 1).GetCell(4);
            iCell.SetCellValue("Ra");
            iCell.CellStyle = cellStyleLabelOut; //Dinh dang
            #endregion

            #region Others
            if (sheet.GetRow(rowNum + 2) == null)
                sheet.CreateRow(rowNum + 2);
            if (sheet.GetRow(rowNum + 2).GetCell(4) == null)
                sheet.GetRow(rowNum + 2).CreateCell(4);
            iCell = sheet.GetRow(rowNum + 2).GetCell(4);
            iCell.CellStyle = cellStyleLabelOther;//Dinh dang
            #endregion

            float GioTangCaOTX = 0, CongK = 0;
            while (j < colDay)
            {
                j++;
                #region Dòng chấm vào TimeIn
                var I = row[string.Format("I{0}", j)].ToString();
                if (sheet.GetRow(rowNum) == null)
                    sheet.CreateRow(rowNum);
                if (sheet.GetRow(rowNum).GetCell(StartColumn + j) == null)
                    sheet.GetRow(rowNum).CreateCell(StartColumn + j);
                sheet.GetRow(rowNum).CreateCell(StartColumn + j).SetCellValue(I);
                sheet.GetRow(rowNum).GetCell(StartColumn + j).CellStyle = cellStyleIn; //Dinh dang
                #endregion

                #region Dòng chấm ra TimeOut
                var O = row[string.Format("O{0}", j)].ToString();
                if (sheet.GetRow(rowNum + 1) == null)
                    sheet.CreateRow(rowNum + 1);
                if (sheet.GetRow(rowNum + 1).GetCell(StartColumn + j) == null)
                    sheet.GetRow(rowNum + 1).CreateCell(StartColumn + j);
                sheet.GetRow(rowNum + 1).GetCell(StartColumn + j).SetCellValue(O);
                sheet.GetRow(rowNum + 1).GetCell(StartColumn + j).CellStyle = cellStyleOut; //Dinh dang
                #endregion

                #region Dòng Thời gian hoặc là nghỉ phép
                var C = row[string.Format("C{0}", j)].ToString();
                if (sheet.GetRow(rowNum + 2) == null)
                    sheet.CreateRow(rowNum + 2);
                if (sheet.GetRow(rowNum + 2).GetCell(StartColumn + j) == null)
                    sheet.GetRow(rowNum + 2).CreateCell(StartColumn + j);
                if (!string.IsNullOrWhiteSpace(O))
                {
                    float value = 0;
                    float.TryParse(row[string.Format("T{0}", j)].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint,
                    new System.Globalization.CultureInfo("en-US"), out value);
                    //float.TryParse(row[string.Format("T{0}", j)].ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value);
                    sheet.GetRow(rowNum + 2).GetCell(StartColumn + j).SetCellValue(value);
                }
                else
                    sheet.GetRow(rowNum + 2).GetCell(StartColumn + j).SetCellValue(C);
                sheet.GetRow(rowNum + 2).GetCell(StartColumn + j).CellStyle = cellStyleThoiGianNghiPhep; //Dinh dang
                #endregion

                Decimal valueTemp = 0;
                if (Decimal.TryParse(row[string.Format("OT{0}", j)].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint,
                    new System.Globalization.CultureInfo("en-US"), out valueTemp))
                    GioTangCaOTX += (float)valueTemp;
                //Lay so cong
                Decimal temK = 0;
                if (Decimal.TryParse(row[string.Format("K{0}", j)].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint,
                    new System.Globalization.CultureInfo("en-US"), out temK))
                {
                    //string value = string.Format("value={0}| temK={1}", row[string.Format("K{0}", j)], temK);
                    //Libs.clsCommon.WriteLogAspNet(string.Format("K{0}", j), value);
                    CongK += (float)temK;
                }
            }

            int cellTotal = StartColumn + colDay + 1, nextCell = 0;
            /*** Gán giá trị cột Giờ làm thêm. Quy đổi ra giờ (chia 60 phút) ***/
            this.AddMergedRegion(1, rowNum, rowNum + 2, cellTotal + nextCell, cellTotal + nextCell, ref sheet, ref templateWorkbook);
            sheet.GetRow(rowNum).GetCell(cellTotal + nextCell).SetCellValue(GioTangCaOTX);
            sheet.GetRow(rowNum).GetCell(cellTotal + nextCell).CellStyle = cellStyleLast;
            /*** Gán giá trị cột số công trong 1 tháng ***/
            nextCell = 1;
            this.AddMergedRegion(1, rowNum, rowNum + 2, cellTotal + nextCell, cellTotal + nextCell, ref sheet, ref templateWorkbook);
            if (CongK > 0)
            {
                double mCongK = Math.Round(CongK, 1);
                //Libs.clsCommon.WriteLogAspNet("mCongK", mCongK.ToString("F", System.Globalization.CultureInfo.InvariantCulture));
                sheet.GetRow(rowNum).GetCell(cellTotal + nextCell).SetCellValue(mCongK); //CongK
                sheet.GetRow(rowNum).GetCell(cellTotal + nextCell).CellStyle = cellStyleLast;
            }
            /*** Cột ký tên ***/
            nextCell = 2;
            this.AddMergedRegion(1, rowNum, rowNum + 2, cellTotal + nextCell, cellTotal + nextCell, ref sheet, ref templateWorkbook);

            rowNum += 3;
        }
         
        private void CreateRow(int rowNum, int column, string value, ICellStyle cellStyle, ref NPOI.SS.UserModel.ISheet sheet)
        {
            IRow row = null;
            if (sheet.GetRow(rowNum) == null)
                row = sheet.CreateRow(rowNum);
            else
                row = sheet.GetRow(rowNum);
            var cell = row.CreateCell(column);
            if (value != null)
                cell.SetCellValue(value);
            cell.CellStyle = cellStyle;
        }

        private void AddMergedRegion(int border, int firstRow, int lastRow, int firstCol, int lastCol,
            ref NPOI.SS.UserModel.ISheet sheet, ref NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook)
        {
            var cellRange = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
            sheet.AddMergedRegion(cellRange);

            RegionUtil.SetBorderTop(1, cellRange, sheet, templateWorkbook);
            RegionUtil.SetBorderRight(1, cellRange, sheet, templateWorkbook);
            RegionUtil.SetBorderBottom(1, cellRange, sheet, templateWorkbook);

            //XSSFCellStyle style = templateWorkbook.CreateCellStyle();
            //style.setBorderTop(BorderStyle.MEDIUM);
            //style.setBorderBottom(BorderStyle.MEDIUM);
            //style.setBorderLeft(BorderStyle.MEDIUM);
            //style.setBorderRight(BorderStyle.MEDIUM);

        }
         
        public NPOI.XSSF.UserModel.XSSFWorkbook ReportMonthly(HttpServerUtilityBase Server, int month, int year, string depId, string maNv)
        {
            try
            {
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_chamcong_thang.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_chamcong_thang.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
                string company = "", address = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;

                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("BCThang");

                IRow iRow = null;

                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(5);
                iRow.GetCell(0).SetCellValue(string.Format("THÁNG {0} NĂM {1}", month, year));

                #region Cài đặt Fonts Header
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                hFont.FontHeightInPoints = 8;
                hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion

                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                ICellStyle cellColDynamic = templateWorkbook.CreateCellStyle();
                cellColDynamic.BorderBottom = BorderStyle.Thin;
                cellColDynamic.BorderRight = BorderStyle.Thin;
                cellColDynamic.BorderTop = BorderStyle.Thin;
                cellColDynamic.SetFont(hFont);
                cellColDynamic.Alignment = HorizontalAlignment.Center;
                cellColDynamic.VerticalAlignment = VerticalAlignment.Center;

                #region Cột chủ nhật
                var CellStyleCN = templateWorkbook.CreateCellStyle();
                CellStyleCN.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                CellStyleCN.SetFont(hFont);
                CellStyleCN.FillPattern = FillPattern.SolidForeground;
                CellStyleCN.BorderBottom = BorderStyle.Thin;
                CellStyleCN.Alignment = HorizontalAlignment.Center;
                CellStyleCN.VerticalAlignment = VerticalAlignment.Center;

                #endregion

                ///*** Dòng thứ 8 ***/
                //sheet.GetRow(7).GetCell(4).SetCellValue("NGÀY LÀM VIỆC TRONG THÁNG");
                //sheet.GetRow(7).GetCell(4).CellStyle = CellStyle;

                /*** Dòng thữ 9 cài đặt các ngày trong tháng ***/
                iRow = sheet.GetRow(8);
                DateTime lastDay = Libs.clsCommon.LastDayOfMonth(month, year);
                int StartColumn = 3, Day = 0;

                //col: là cột ngày (1->31)
                for (Day = 1; Day <= lastDay.Day; Day++)
                {
                    DateTime dtTemp = new DateTime(year, month, Day);
                    string t = "";
                    t = Day.ToString();
                    if (dtTemp.DayOfWeek == DayOfWeek.Sunday) t = "CN";

                    string value = string.Format("{0}", t);
                    if (iRow.GetCell(StartColumn + Day) == null)
                        iRow.CreateCell(StartColumn + Day);
                    iRow.GetCell(StartColumn + Day).SetCellValue(value);
                    //if (t == "CN")
                    //    iRow.GetCell(StartColumn + Day).CellStyle = CellStyleCN;
                    //else
                        iRow.GetCell(StartColumn + Day).CellStyle = CellStyle;

                    /*** Set column width ***/
                    sheet.SetColumnWidth(StartColumn + Day, (int)(5.27 * 256));
                }
                //this.AddMergedRegion(1, 7, 7, StartColumn + 1, Day + 2, ref sheet, ref templateWorkbook);

                Models.MCC.Interfaces.ITimeSheet TimeSheet = new Models.MCC.clsTimeSheet();
                if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                    TimeSheet = new Models.MCC.clsTimeSheetVirtual();

                System.Data.DataTable tbl = null;
                Models.Interfaces.IResultOk ok = null;
                TimeSheet.ReportMonthly(month, year, depId, maNv, (result, ex) => { tbl = result; ok = ex; });
                if (ok.Success && tbl.Rows.Count > 0)
                {
                    #region Font Cell data
                    var hFontCell = templateWorkbook.CreateFont();
                    hFontCell.FontName = "Times New Roman";
                    hFontCell.FontHeightInPoints = 8; //Font Size
                    #endregion

                    //Tạo đường kẻ viền (Borders)
                    ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                    CellStyle3.BorderBottom = BorderStyle.Dashed;
                    CellStyle3.BorderRight = BorderStyle.Thin;
                    CellStyle3.Alignment = HorizontalAlignment.Center;
                    CellStyle3.VerticalAlignment = VerticalAlignment.Center;
                    CellStyle3.SetFont(hFontCell);
                     
                    int no = 0, rowNum = 9;
                    List<System.Data.DataRow> rows = tbl.Select().ToList();
                    while (rows != null && rows.Count > 0)
                    {
                        no++;

                        this.CreateRow(rowNum, 0, no.ToString(), CellStyle3, ref sheet); //Cột số thứ tự
                        this.CreateRow(rowNum, 1, rows[0]["MaNV"].ToString(), CellStyle3, ref sheet);   //Cột mã số
                        this.CreateRow(rowNum, 2, rows[0]["TenNV"].ToString(), CellStyle3, ref sheet);    //Cột họ và tên
                        this.CreateRow(rowNum, 3, rows[0]["DepName"].ToString(), CellStyle3, ref sheet);    //Cột bộ phận

                        int j = 0;
                        //StartColumn = 3;
                        while (j < (Day - 1))
                        {
                            j++;
                            var value = rows[0][string.Format("T{0}", j)].ToString();
                            this.CreateRow(rowNum, (StartColumn + j), value, CellStyle3, ref sheet);
                            sheet.AutoSizeColumn((StartColumn + j));
                        }
                        this.CreateRow(rowNum, 35, rows[0]["TotalDayWork"].ToString(), CellStyle3, ref sheet);    //Cột tổng ngày công trong tháng
                        this.CreateRow(rowNum, 36, rows[0]["LamThem"].ToString(), CellStyle3, ref sheet);    //Cột giờ làm thêm
                        rowNum++; 
                        rows.Remove(rows[0]);
                    }

                    int rowContinue = 9;
                    int columDynamic = 37;
                    if (tbl.Columns.Count > 39)
                    {
                        iRow = sheet.GetRow(7);
                        for (int i = 40; i < tbl.Columns.Count; i++)
                        {
                            string value = string.Format("{0}", tbl.Columns[i].ColumnName);
                            if (iRow.GetCell(columDynamic) == null)
                                iRow.CreateCell(columDynamic);
                            iRow.GetCell(columDynamic).SetCellValue(value);
                            iRow.GetCell(columDynamic).CellStyle = cellColDynamic;
                            this.AddMergedRegion(1, 7, 8, columDynamic, columDynamic, ref sheet, ref templateWorkbook);
                            sheet.SetColumnWidth(columDynamic, 3 * 256);
                            columDynamic++;
                        }

                        // ký tên
                        string kyTen = string.Format("Ký tên");
                        if (iRow.GetCell(columDynamic) == null)
                            iRow.CreateCell(columDynamic);
                        iRow.GetCell(columDynamic).SetCellValue(kyTen);
                        iRow.GetCell(columDynamic).CellStyle = cellColDynamic;
                        this.AddMergedRegion(1, 7, 8, columDynamic, columDynamic, ref sheet, ref templateWorkbook);

                        if (tbl != null && tbl.Rows.Count > 0)
                        {
                            //int rowTong = tbl.Rows.Count + 9;
                            //this.CreateRow(rowTong, 0, "Tổng", cellColDynamic, ref sheet);    //Cột tổng
                            //this.AddMergedRegion(1, rowTong, rowTong, 0, 34, ref sheet, ref templateWorkbook);
                            for (int i = 0; i < tbl.Rows.Count; i++)
                            {
                                columDynamic = 37;
                                for (int j = 40; j < tbl.Columns.Count; j++)
                                {
                                    this.CreateRow(rowContinue, columDynamic, tbl.Rows[i][j].ToString(), CellStyle3, ref sheet);
                                    this.CreateRow(rowContinue, columDynamic + 1, "", CellStyle3, ref sheet);
                                    columDynamic++;
                                }
                                rowContinue++;
                            }
                        }
                    }

                    #region Đổ màu những ô là ngày chủ nhật
                    //int rowIndex = 9;
                    //ICellStyle _CelStyleEnd = templateWorkbook.CreateCellStyle();
                    //_CelStyleEnd.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                    //_CelStyleEnd.FillPattern = FillPattern.SolidForeground;
                    //_CelStyleEnd.Alignment = HorizontalAlignment.Center;
                    //_CelStyleEnd.VerticalAlignment = VerticalAlignment.Center;
                    //while (rowIndex < rowNum)
                    //{
                    //    int dayTemp = 1;
                    //    while (dayTemp <= lastDay.Day)
                    //    {
                    //        if (Libs.clsCommon.GetDayOfWeek(month, dayTemp, year) == DayOfWeek.Sunday)
                    //        {
                    //            iRow = sheet.GetRow(rowIndex);
                    //            if (iRow.GetCell(StartColumn + dayTemp) == null)
                    //                iRow.CreateCell(StartColumn + dayTemp);

                    //            //iRow.GetCell(StartColumn + dayTemp).CellStyle = _CelStyleEnd;
                    //            //Kẻ khung ở dưới bằng đường thẳng
                    //            if (rowIndex == rowNum - 1)
                    //                iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Thin;
                    //            else
                    //                iRow.GetCell(StartColumn + dayTemp).CellStyle.BorderBottom = BorderStyle.Dashed;
                    //        }
                    //        dayTemp++;
                    //    }
                    //    rowIndex++;
                    //}

                    #endregion
                }

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
         
        public XSSFWorkbook ReportYear(HttpServerUtilityBase Server, int month, int year, string depId, string maNv)
        {
            try
            {
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_tong_hop_cong_nghi.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_tong_hop_cong_nghi.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
                string company = "", address = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;

                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("bc_tong_hop_cong");

                IRow iRow = null;

                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(5);
                iRow.GetCell(0).SetCellValue(string.Format("DATE: {0}", DateTime.Now.ToString(Libs.clsConsts.FormatDMY)));

                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                hFont.FontHeightInPoints = 8;
                hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion

                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                ICellStyle cellColDynamic = templateWorkbook.CreateCellStyle();
                cellColDynamic.BorderBottom = BorderStyle.Thin;
                cellColDynamic.BorderRight = BorderStyle.Thin;
                cellColDynamic.BorderTop = BorderStyle.Thin;
                cellColDynamic.SetFont(hFont);
                cellColDynamic.Alignment = HorizontalAlignment.Center;
                cellColDynamic.VerticalAlignment = VerticalAlignment.Center;

                //Tạo đường kẻ viền (Borders)
                ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                CellStyle3.BorderBottom = BorderStyle.Dashed;
                CellStyle3.BorderRight = BorderStyle.Thin;
                CellStyle3.Alignment = HorizontalAlignment.Center;
                CellStyle3.VerticalAlignment = VerticalAlignment.Center;

                Models.MCC.Interfaces.ITimeSheet TimeSheet = new Models.MCC.clsTimeSheet();
                if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                    TimeSheet = new Models.MCC.clsTimeSheetVirtual();

                System.Data.DataTable tbl = null;
                Models.Interfaces.IResultOk ok = null;
                TimeSheet.ReportYear(month, year, depId, maNv, (result, ex) => { tbl = result; ok = ex; });
                if (ok.Success && tbl.Rows.Count > 0)
                {

                    #region HEADER EXCEL
                    /*********** SET HEADER EXCEL *******************/
                    int firstMergeCell = 5, nextId = 2;
                    IRow iRow7 = sheet.GetRow(7);
                    iRow = sheet.GetRow(8);
                    int columDynamic = 5, monthHeader = 1;
                    for (int j = 6; j < tbl.Columns.Count; j++)
                    {
                        if (tbl.Columns[j].ColumnName == string.Format("Id{0}", nextId))
                        {
                            nextId++;
                            this.AddMergedRegion(1, 7, 7, firstMergeCell, columDynamic - 1, ref sheet, ref templateWorkbook);

                            /*** Set ten ***/
                            if (iRow7.GetCell(firstMergeCell) == null)
                                iRow7.CreateCell(firstMergeCell);
                            iRow7.GetCell(firstMergeCell).SetCellValue(string.Format("THÁNG {0}", monthHeader));
                            iRow7.GetCell(firstMergeCell).CellStyle = CellStyle;

                            monthHeader++;
                            firstMergeCell = columDynamic;
                        }
                        else
                        {
                            string value = string.Format("{0}", tbl.Columns[j].ColumnName).Split('_')[0];
                            if (iRow.GetCell(columDynamic) == null)
                                iRow.CreateCell(columDynamic);
                            iRow.GetCell(columDynamic).SetCellValue(value);
                            iRow.GetCell(columDynamic).CellStyle = CellStyle;
                            /*** Set column width ***/
                            sheet.SetColumnWidth(columDynamic, (int)(5.27 * 256));
                            columDynamic++;
                            if (j == tbl.Columns.Count - 1)
                            {
                                /*** Set ten ***/
                                if (iRow7.GetCell(firstMergeCell) == null)
                                    iRow7.CreateCell(firstMergeCell);
                                iRow7.GetCell(firstMergeCell).SetCellValue(string.Format("THÁNG {0}", monthHeader));
                                iRow7.GetCell(firstMergeCell).CellStyle = CellStyle;

                                this.AddMergedRegion(1, 7, 7, firstMergeCell, columDynamic - 1, ref sheet, ref templateWorkbook);
                            }
                        }
                    }
                    #endregion

                    #region Body excel

                    int no = 0, rowNum = 9;

                    #region Cột tĩnh

                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        no++;
                        this.CreateRow(rowNum, 0, no.ToString(), CellStyle3, ref sheet); //Cột số thứ tự
                        this.CreateRow(rowNum, 1, tbl.Rows[i]["MaNV"].ToString(), CellStyle3, ref sheet);   //Cột mã số
                        this.CreateRow(rowNum, 2, tbl.Rows[i]["TenNV"].ToString(), CellStyle3, ref sheet);    //Cột họ và tên
                        this.CreateRow(rowNum, 3, tbl.Rows[i]["DepName"].ToString(), CellStyle3, ref sheet);    //Cột bộ phận
                        this.CreateRow(rowNum, 4, tbl.Rows[i]["BeginningDate"].ToString(), CellStyle3, ref sheet);    //Cột ngày vào làm

                        rowNum++;
                    }

                    #endregion

                    #region Cột động
                    rowNum = 9;
                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        columDynamic = 5;
                        for (int j = 6; j < tbl.Columns.Count; j++)
                        {
                            if (!tbl.Columns[j].ColumnName.Contains("Id"))
                            {
                                this.CreateRow(rowNum, columDynamic, tbl.Rows[i][j].ToString(), CellStyle3, ref sheet);
                                columDynamic++;
                            }
                        }
                        rowNum++;
                    }

                    #endregion
                    #endregion

                }

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}