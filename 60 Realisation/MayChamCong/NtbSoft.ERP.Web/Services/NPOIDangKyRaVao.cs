﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class NPOIDangKyRaVao
    {
        /// <summary>
        /// MaCheDo=0: Lấy tất cả các chế độ
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="DepID"></param>
        /// <param name="MaNV"></param>
        /// <returns></returns>
        public NPOI.XSSF.UserModel.XSSFWorkbook Export(HttpServerUtilityBase Server, DateTime fromDate, DateTime toDate,string DepID,string MaNV)
        {
            try
            {
                Models.DIC.Interfaces.IDangKyVaoRa bcRpt = new Models.DIC.clsDangKyVaoRa();
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_lao_dong_ra_vao.xlsx")))
                {
                    return null;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_lao_dong_ra_vao.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
                
                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                //hFont.FontHeightInPoints = 8;
                //hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion
                string company = "", address = "", fromDate_toDate = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;
                fromDate_toDate = string.Format("Ngày {0}", fromDate.ToString("dd/MM/yyyy"));
                if (!fromDate.ToString("dd/MM/yyyy").Equals(toDate.ToString("dd/MM/yyyy")))
                    fromDate_toDate = string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));

                ICellStyle CellStyle = null, CellStyleBoPhan = null;
                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("Danh_sach_nhan_vien_ra_vao");
                IRow iRow = null;
                /*** Lấy dòng tên công ty ***/
                iRow = sheet.GetRow(0);
                iRow.GetCell(2).SetCellValue(company);
                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(1);
                iRow.GetCell(2).SetCellValue(address);
                /*** Lấy dòng Tiêu đề ***/
                //iRow = sheet.GetRow(2);
                //iRow.GetCell(2).SetCellValue("");
                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(3);
                iRow.GetCell(2).SetCellValue(fromDate_toDate);


                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Dashed;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                #region Định dạng cột "Họ và tên Nhân viên"
                var CellStyleHoTen = templateWorkbook.CreateCellStyle();
                CellStyleHoTen.Alignment = HorizontalAlignment.Left;
                CellStyleHoTen.VerticalAlignment = VerticalAlignment.Center;
                CellStyleHoTen.BorderBottom = BorderStyle.Dashed;
                CellStyleHoTen.BorderRight = BorderStyle.Thin;
                CellStyleHoTen.SetFont(hFont);
                #endregion

                #region Cell Style Bo Phan
                CellStyleBoPhan = templateWorkbook.CreateCellStyle();
                CellStyleBoPhan.Alignment = HorizontalAlignment.Left;
                CellStyleBoPhan.VerticalAlignment = VerticalAlignment.Center;
                CellStyleBoPhan.BorderBottom = BorderStyle.Dashed;
                CellStyleBoPhan.BorderRight = BorderStyle.Thin;
                CellStyleBoPhan.SetFont(hFont);
                #endregion
                int startRow = 6, countRow = 0;
                //Lấy dữ liệu không chấm công
                List<Entity.Interfaces.IDangKyRaVaoInfo> Items = new List<Entity.Interfaces.IDangKyRaVaoInfo>();
                bcRpt.GetAll(fromDate, toDate, DepID, MaNV, (result, ex) => { Items = result; });

                countRow = Items.Count;
                for (int i = 0; i < countRow; i++)
                {
                    if (sheet.GetRow(startRow+i) == null)
                        iRow = sheet.CreateRow(startRow + i);
                    else
                        iRow = sheet.GetRow(startRow + i);

                    var iCell = iRow.CreateCell(0);
                    iCell.SetCellValue((i + 1));
                    iCell.CellStyle = CellStyle;
                    iCell.CellStyle.Alignment = HorizontalAlignment.Center;
                    iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;


                    #region Cột Mã Nhân viên
                    iCell = iRow.CreateCell(1);
                    iCell.SetCellValue(Items[i].MaNV);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột "Họ và tên Nhân viên"
                    iCell = iRow.CreateCell(2);
                    iCell.SetCellValue(Items[i].TenNV);
                    iCell.CellStyle = CellStyleHoTen;
                    #endregion

                    #region Cột Bộ phận
                    iCell = iRow.CreateCell(3);
                    iCell.SetCellValue(Items[i].DepName);
                    iCell.CellStyle = CellStyleBoPhan;
                   
                    #endregion

                    #region Cột giờ ra
                    iCell = iRow.CreateCell(4);
                    iCell.SetCellValue(Items[i].GioRa);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ vào
                    iCell = iRow.CreateCell(5);
                    iCell.SetCellValue(Items[i].GioVao);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột số phút
                    iCell = iRow.CreateCell(6);
                    iCell.SetCellValue(Items[i].ThoiGian);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột ghi chú
                    iCell = iRow.CreateCell(7);
                    iCell.SetCellValue(Items[0].GhiChu);
                    iCell.CellStyle = CellStyle;
                    #endregion

                 
                }

                #region Dòng cuối cùng 
                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                if (sheet.GetRow(startRow + countRow) == null)
                    iRow = sheet.CreateRow(startRow + countRow);
                else
                    iRow = sheet.GetRow(startRow + countRow);
                var iCell1 = iRow.CreateCell(0);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(1);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(2);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(3);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(4);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(5);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(6);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(7);
                iCell1.CellStyle = CellStyle;
                #endregion

                return templateWorkbook;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetTitleReport(int MaCheDo)
        {
            switch (MaCheDo)
            {
                case 1:
                    return "DANH SÁCH LAO ĐỘNG CHẾ ĐỘ NUÔI CON NHỎ";
                case 2:
                    return "DANH SÁCH LAO ĐỘNG MANG THAI 7-9 THÁNG";
                case 3:
                    return "DANH SÁCH LAO ĐỘNG VỊ THÀNH NIÊN";
                case 4:
                    return "DANH SÁCH LAO ĐỘNG CHẾ ĐỘ THAI SẢN";
                default:
                    return "DANH SÁCH NHÂN VIÊN CHẾ ĐỘ";
            }
        }

    }
}