﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<MccJob>().Build();
            

            #region  Trong trường hợp này, scheduler được chỉ định chạy hàng ngày vào nửa đêm và sẽ chạy cứ mỗi 24 giờ
            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithDailyTimeIntervalSchedule
            //      (s =>
            //         s.WithIntervalInHours(15)
            //        .OnEveryDay()
            //        .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
            //      )
            //    .Build();
            #endregion

            //Trigger được đặt một cái tên và nhóm nó có thể dễ dàng định danh nếu bạn cần tham chiếu trong lập trình. 
            //Nó được thiets kế để chạy t rực tiếp sau mỗi 10 giây cho đến khi kết thúc (hoặc bị dừng vì lý do nào đó). 
            //Fluent API giúp cho việc tạo trigger dễ nhìn và dễ hiểu hơn.

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger_mcc", "mcc")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(20)
                    .RepeatForever())
                .Build();


            scheduler.ScheduleJob(job, trigger);

        }

    }
}