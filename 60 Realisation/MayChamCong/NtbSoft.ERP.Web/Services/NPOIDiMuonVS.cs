﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class NPOIDiMuonVS : Interfaces.INopiReoprt
    {
        public void Export(HttpServerUtilityBase Server, DateTime fromDate, DateTime toDate, Action<XSSFWorkbook, Exception> Complete)
        {
            Exception exErr = null;
            NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = null;
            Models.MCC.Interfaces.IBcDiMuonVeSom bcDiMuonVeSom = new Models.MCC.clsBcDiMuonVeSom();
            System.Data.DataTable tblBC = new System.Data.DataTable();
            try
            {
                if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_di_muon.xlsx")))
                {
                    exErr = new Exception("NULL");
                    goto exit;
                }
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\Templates\bc_di_muon.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);

                #region Cài đặt Fonts
                var hFont = templateWorkbook.CreateFont();
                hFont.FontName = "Times New Roman";
                //hFont.FontHeightInPoints = 8;
                //hFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                #endregion
                string company = "", address = "", fromDate_toDate = "";
                company = AppCaches.clsCacheTheApp.Instance.CompanyName.ToUpper();
                address = AppCaches.clsCacheTheApp.Instance.Address;
                fromDate_toDate = string.Format("Ngày {0}", fromDate.ToString("dd/MM/yyyy"));
                if (!fromDate.ToString("dd/MM/yyyy").Equals(toDate.ToString("dd/MM/yyyy")))
                    fromDate_toDate = string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));

                ICellStyle CellStyle = null;
                // Getting the worksheet by its name...
                NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("Danh_sach_nhan_vien_di_muon");
                IRow iRow = null;
                /*** Lấy dòng tên công ty ***/
                iRow = sheet.GetRow(0);
                iRow.GetCell(2).SetCellValue(company);
                /*** Lấy dòng địa chỉ ***/
                iRow = sheet.GetRow(1);
                iRow.GetCell(2).SetCellValue(address);
                /*** Lấy dòng Tháng năm ***/
                iRow = sheet.GetRow(3);
                iRow.GetCell(2).SetCellValue(fromDate_toDate);


                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Dashed;
                CellStyle.BorderRight = BorderStyle.Thin;
                CellStyle.SetFont(hFont);
                CellStyle.Alignment = HorizontalAlignment.Center;
                CellStyle.VerticalAlignment = VerticalAlignment.Center;

                /*** Lấy dữ liệu ***/
                bcDiMuonVeSom.GetBy(fromDate, toDate, (result, ex) => { tblBC = result; });
                int startRow = 6, countRow = 10;
                countRow = tblBC.Rows.Count;
                double value = 0;
                for (int i = 0; i < countRow; i++)
                {
                    if (sheet.GetRow(startRow + i) == null)
                        iRow = sheet.CreateRow(startRow + i);
                    else
                        iRow = sheet.GetRow(startRow + i);

                    var iCell = iRow.CreateCell(0);
                    iCell.SetCellValue((i + 1));
                    iCell.CellStyle = CellStyle;
                    iCell.CellStyle.Alignment = HorizontalAlignment.Center;
                    iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;

                    #region Cột Mã Nhân viên
                    iCell = iRow.CreateCell(1);
                    iCell.SetCellValue(tblBC.Rows[i]["MaNV"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột "Họ và tên Nhân viên"
                    iCell = iRow.CreateCell(2);
                    iCell.SetCellValue(tblBC.Rows[i]["TenNV"].ToString());
                    iCell.CellStyle = templateWorkbook.CreateCellStyle();
                    iCell.CellStyle.Alignment = HorizontalAlignment.Left;
                    iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;
                    iCell.CellStyle.BorderBottom = BorderStyle.Dashed;
                    iCell.CellStyle.BorderRight = BorderStyle.Thin;
                    iCell.CellStyle.SetFont(hFont);
                    #endregion

                    #region Cột Bộ phận
                    iCell = iRow.CreateCell(3);
                    iCell.SetCellValue(tblBC.Rows[i]["DepName"].ToString());
                    iCell.CellStyle = templateWorkbook.CreateCellStyle();
                    iCell.CellStyle.Alignment = HorizontalAlignment.Left;
                    iCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;
                    iCell.CellStyle.BorderBottom = BorderStyle.Dashed;
                    iCell.CellStyle.BorderRight = BorderStyle.Thin;
                    #endregion

                    #region Cột giờ đến
                    iCell = iRow.CreateCell(4);
                    iCell.SetCellValue(tblBC.Rows[i]["TimeIn"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột giờ về
                    iCell = iRow.CreateCell(5);
                    iCell.SetCellValue(tblBC.Rows[i]["TimeOut"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột đi muộn
                    iCell = iRow.CreateCell(6);
                    value = Convert.ToDouble(tblBC.Rows[i]["LateTime"]);
                    if (value > 0) iCell.SetCellValue(value);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột về sớm
                    iCell = iRow.CreateCell(7);
                    value = Convert.ToDouble(tblBC.Rows[i]["EarlyTime"]);
                    if (value > 0) iCell.SetCellValue(value);
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột ngày
                    iCell = iRow.CreateCell(8);
                    iCell.SetCellValue(tblBC.Rows[i]["Workingday"].ToString());
                    iCell.CellStyle = CellStyle;
                    #endregion

                    #region Cột ghi chú
                    iCell = iRow.CreateCell(9);
                    //iCell.SetCellValue("");
                    iCell.CellStyle = CellStyle;
                    #endregion

                }

                #region Dòng cuối cùng
                CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Thin;
                CellStyle.BorderRight = BorderStyle.Thin;
                if (sheet.GetRow(startRow + countRow) == null)
                    iRow = sheet.CreateRow(startRow + countRow);
                else
                    iRow = sheet.GetRow(startRow + countRow);
                var iCell1 = iRow.CreateCell(0);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(1);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(2);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(3);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(4);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(5);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(6);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(7);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(8);
                iCell1.CellStyle = CellStyle;
                iCell1 = iRow.CreateCell(9);//Cot ghi chu
                iCell1.CellStyle = CellStyle;
                #endregion

            }
            catch (Exception ex) { exErr = ex; }

        exit:
            if (Complete != null) Complete(templateWorkbook, exErr);
        }
    }
}