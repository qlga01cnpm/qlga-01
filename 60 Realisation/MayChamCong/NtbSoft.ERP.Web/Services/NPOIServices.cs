﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.Services
{
    public class NPOIServices
    {
        public void InsertDataExcel(HttpFileCollection files, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.Interfaces.INhanVienImport Import = new Models.clsNhanVienImport();
            Models.clsNhanVienType TypeNhanVien = new Models.clsNhanVienType();
            System.Data.DataTable tbl_TypeNhanVien = TypeNhanVien.TYPE_DIC_NHANVIEN;
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            if (files != null && files.Count > 0 && files[0].ContentLength > 0 && files[0].ContentLength < (10 * 1024 * 1024))
            {
                string filetype = files[0].FileName.Split('.').Last();
                //string fileName = Path.GetFileName(files[0].FileName);
                NPOI.SS.UserModel.ISheet ws = null;
                int LastRowNum = 0;
                if (filetype == "xls")
                {
                    HSSFWorkbook excel = new HSSFWorkbook(files[0].InputStream);
                    ws = (NPOI.SS.UserModel.ISheet)excel.GetSheetAt(0);
                    LastRowNum = ws.LastRowNum + 1;
                }
                else if (filetype == "xlsx")
                {
                    NPOI.XSSF.UserModel.XSSFWorkbook excel1 = new NPOI.XSSF.UserModel.XSSFWorkbook(files[0].InputStream);
                    ws = (NPOI.SS.UserModel.ISheet)excel1.GetSheetAt(0);
                    LastRowNum = ws.LastRowNum + 1;
                }
                else
                    ok.Errors.Add("InsertDataExcel", "File format error !");
                if (ws != null)
                {
                    int startRow = 3, cellnum = 0, i = 0;
                    string ValueTemp = "", MaNV = "";
                    try
                    {
                        DataFormatter formatter = new DataFormatter();
                        for (i = startRow; i < LastRowNum; i++)
                        {
                            cellnum = 1;
                            if (ws.GetRow(i).GetCell(1) == null ||
                                string.IsNullOrWhiteSpace(formatter.FormatCellValue(ws.GetRow(i).GetCell(1)))) continue;
                            System.Data.DataRow newRow = tbl_TypeNhanVien.NewRow();
                            newRow["MaNV"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(1));
                            MaNV = newRow["MaNV"].ToString();

                            #region Cột tên nhân viên
                            cellnum = 2;
                            if (ws.GetRow(i).GetCell(2) != null)
                                newRow["TenNV"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(2));
                            #endregion

                            bool exits = false;
                            //Import.CheckMaNV(newRow["MaNV"].ToString(), (result, ex) => { exits = result; ok = ex; });
                            if (exits)
                            {
                                ok.Errors.Add("Import_", string.Format("Mã nhân viên \"{0} - {1}\" đã tồn tại. Vui lòng kiểm tra lại file Excel trước khi Import",
                                    newRow["MaNV"], newRow["TenNV"] == null ? "" : newRow["TenNV"]));
                                break;
                            }

                            #region Kiểm tra mã chấm công
                            cellnum = 3;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["MaCC"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));

                            Import.CheckMaCC(newRow["MaCC"].ToString(), (result, ex) => { exits = result; ok = ex; });
                            if (exits)
                            {
                                ok.Errors.Add("Import_", string.Format(@"Mã chấm công {0} đã được sử dụng cho nhân viên khác. Vui lòng kiểm tra lại file Excel trước khi Import",
                                    newRow["MaCC"], newRow["TenNV"] == null ? "" : newRow["TenNV"]));
                                break;
                            }
                            #endregion

                            #region Cột CardID
                            cellnum = 4;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["CardID"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            #region Cột chức vụ
                            cellnum = 5;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["PositionName"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion
                            
                            #region Cột giới tính
                            cellnum = 6;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["Sex"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            if (string.IsNullOrEmpty(newRow["Sex"].ToString()))
                                throw new Exception("Cột giới tính chưa có giá trị");
                            
                            #endregion

                            #region Cột Ngày vào làm việc
                            cellnum = 7;
                            ValueTemp = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["BeginningDate"] = ValueTemp;
                            #endregion

                            #region Cột ngày sinh
                            cellnum = 8;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["BirthDate"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            #region Cột nguyên quán
                            cellnum = 9;
                            ValueTemp = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["NguyenQuan"] = ValueTemp;
                            #endregion

                            #region Cột địa chỉ
                            cellnum = 10;
                            ValueTemp = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["Address"] = ValueTemp;
                            #endregion

                            #region Cột thẻ bảo hiểm
                            cellnum = 11;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["TheBHXH"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            #region Cột CMND
                            cellnum = 12;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["CMND"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            #region Cột nơi cấp
                            cellnum = 13;
                            if (ws.GetRow(i).GetCell(cellnum) != null)
                                newRow["NoiCap"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            #region Cột ngày ký hợp đồng
                            cellnum = 14;
                            ValueTemp = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            if (ws.GetRow(i).GetCell(cellnum) != null && !string.IsNullOrWhiteSpace(ValueTemp))
                                newRow["NgayKyHopDong"] = ValueTemp;
                            #endregion
                            
                            #region Cột ngày quay lại
                            cellnum = 15;
                            if (ws.GetRow(i).GetCell(cellnum) != null && !string.IsNullOrWhiteSpace(ws.GetRow(i).GetCell(cellnum).StringCellValue))
                                newRow["NgayQuayLai"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(cellnum));
                            #endregion

                            tbl_TypeNhanVien.Rows.Add(newRow);
                        }
                    }
                    catch (Exception ex)
                    {
                        string error = string.Format("Lỗi dòng {0}, cột {1}: , value=[{2}] ,Mã MV={3}. ", i + 1, cellnum+1, ValueTemp,MaNV) + ex.Message;
                        ok.Errors.Add("InsertDataExcel", error);
                    }
                }
            }
            else
                ok.Errors.Add("InsertDataExcel", "HttpFileCollection is null");
            if (ok.Success && tbl_TypeNhanVien.Rows.Count > 0)
                Import.Import(tbl_TypeNhanVien, ex => { ok = ex; });
            if (Complete != null) Complete(ok);
        }

        public void InserSinhVienExcel(HttpFileCollection files, Action<Models.Interfaces.IResultOk> Complete)
        {
            Models.ISinhVienImport Import = new Models.clsSinhVienImport();
            Models.clsSinhVienType TypeSinhVien = new Models.clsSinhVienType();
            System.Data.DataTable tbl_TypeSinhVien = TypeSinhVien.TYPE_DIC_SINHVIEN;
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            if (files != null && files.Count > 0 && files[0].ContentLength > 0 && files[0].ContentLength < (10 * 1024 * 1024))
            {
                string filetype = files[0].FileName.Split('.').Last();
                //string fileName = Path.GetFileName(files[0].FileName);
                NPOI.SS.UserModel.ISheet ws = null;
                int LastRowNum = 0;
                if (filetype == "xls")
                {
                    HSSFWorkbook excel = new HSSFWorkbook(files[0].InputStream);
                    ws = (NPOI.SS.UserModel.ISheet)excel.GetSheetAt(0);
                    LastRowNum = ws.LastRowNum + 1;
                }
                else if (filetype == "xlsx")
                {
                    NPOI.XSSF.UserModel.XSSFWorkbook excel1 = new NPOI.XSSF.UserModel.XSSFWorkbook(files[0].InputStream);
                    ws = (NPOI.SS.UserModel.ISheet)excel1.GetSheetAt(0);
                    LastRowNum = ws.LastRowNum + 1;
                }
                else
                    ok.Errors.Add("InsertDataExcel", "File format error !");
                if (ws != null)
                {
                    int startRow = 3, cellnum = 0, i = 0;
                    string ValueTemp = "", StudentID = "";
                    try
                    {
                        DataFormatter formatter = new DataFormatter();
                        for (i = startRow; i < LastRowNum; i++)
                        {
                            #region Cột StudentID
                            cellnum = 0;
                            if (ws.GetRow(i).GetCell(0) == null ||
                                string.IsNullOrWhiteSpace(formatter.FormatCellValue(ws.GetRow(i).GetCell(0)))) continue;
                            System.Data.DataRow newRow = tbl_TypeSinhVien.NewRow();
                            newRow["StudentID"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(0));
                            StudentID = newRow["StudentID"].ToString();
                            #endregion

                            #region Cột Name
                            cellnum = 1;
                            if (ws.GetRow(i).GetCell(1) != null)
                                newRow["Name"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(1));
                            #endregion

                            #region Cột Age
                            cellnum = 2;
                            if (ws.GetRow(i).GetCell(2) != null)
                                newRow["Age"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(2));
                            #endregion

                            #region Cột Address
                            cellnum = 3;
                            if (ws.GetRow(i).GetCell(3) != null)
                                newRow["Address"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(3));
                            #endregion

                            #region Cột PhoneNumber
                            cellnum = 4;
                            if (ws.GetRow(i).GetCell(4) != null)
                                newRow["PhoneNumber"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(4));
                            #endregion

                            #region Cột Email
                            cellnum = 5;
                            if (ws.GetRow(i).GetCell(5) != null)
                                newRow["Email"] = formatter.FormatCellValue(ws.GetRow(i).GetCell(5));
                            #endregion

                            tbl_TypeSinhVien.Rows.Add(newRow);
                        }
                    }
                    catch (Exception ex)
                    {
                        //string error = string.Format("Lỗi dòng {0}, cột {1}: , value=[{2}] ,Mã MV={3}. ", i + 1, cellnum + 1, ValueTemp, MaNV) + ex.Message;
                        //ok.Errors.Add("InsertDataExcel", error);
                    }
                }
            }
            else
                ok.Errors.Add("InsertDataExcel", "HttpFileCollection is null");
            if (ok.Success && tbl_TypeSinhVien.Rows.Count > 0)
                Import.ImportSinhVien(tbl_TypeSinhVien, ex => { ok = ex; });
            if (Complete != null) Complete(ok);
        }


    }
}