﻿function changeColumnTitle(obj) {
    var panel = $(obj).datagrid("getPanel");
    //var myheaderCol = panel.find("div.datagrid-header td[field='D2']");
    //// here is to add the css style
    //myheaderCol.css("background-color", "red");
    //var $field = $('td[field=' + 'D2' + ']', panel);
    //if ($field.length) {
    //    var $span = $('span', $field).eq(0);
    //    $span.html('2-T7');
    //}
    $.ajax({
        url: "/MCC/GetOptionDay",
        type: "GET",
        async: false,
        data: { month: getValueMonth(), year: getValueYear() },
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var col = 'D' + (i + 1);    //Lấy tên cột
                var myheaderCol = panel.find("div.datagrid-header td[field='" + col + "']");
                // here is to add the css style
                if (data[i].Value.indexOf('CN') > 0) {
                    myheaderCol.css("background-color", "red");
                    //var c = $(obj).datagrid('getColumnOption', col);
                    //c.styler = function () {
                    //    return 'background-color:red;color:#fff';
                    //};
                    //$(obj).datagrid('refreshRow', 0);

                } else if (data[i].Value.indexOf('T7') > 0) {
                    myheaderCol.css('background', '#19AC64');
                }
                else
                    myheaderCol.css("background", "#006699");

                var $field = $('td[field=' + col + ']', panel);
                if ($field.length) {
                    var $span = $('span', $field).eq(0);
                    $span.html(data[i].Value);
                }
                // Hiện nhưng cột từ ngày 27 trở đi
                if (i > 26)
                    $(obj).datagrid('showColumn', col);
            }

            var minDay = data.length;
            for (var i = 1 ; i <= (31 - minDay) ; i++) {
                var col = 'D' + (minDay + i);
                $(obj).datagrid('hideColumn', col);
            }


        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
        }
    });
}

function formatStyler(value, row, index) {
    //for (var name in row) {
    //    var opts = $(this).datagrid('getColumnOption', name);
    //    alert(JSON.stringify(opts));

    //}
}

function getValueMonth() {
    return $('#cb_option').combobox('getValue');
}

function getValueYear() {
    return $('#cb-Years').combobox('getValue');
}

function loadYears() {
    $('#cb-Years').combobox({
        panelWidth: 124,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/MCC/GetYears',
        onLoadSuccess: function () {
            var data = $(this).combobox('getData');
            $(this).combobox('setValue', data[0].id);
        },
    });
}

function refresh() {
    loadDataBy(getDepId(), '');

    //$('#cb-lydo').combobox({
    //    valueField: 'id',
    //    textField: 'name',
    //    mode: 'remote',
    //    method: 'get',
    //    url: '/Setting/JDataComboBoxLyDoVang'
    //});

   
}

function getRow() {
    var rowSelect = $('#dg-list').datagrid('getSelected');
    if (rowSelect == null || rowSelect == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return null;
    }
    return rowSelect;
}


function AddNew() {
   
    $('#fm').form('clear');

    var Dept = getSelectedNode(),
        emp = getSelectedNhanVien();
    var newRow = {};
    newRow.ID = 0;
    newRow.eID = emp == null ? "" : emp.eID
    newRow.MaNV = emp == null ? "" : emp.MaNV;
    newRow.TuNgay = formatterDMY(new Date());
    newRow.ToiNgay = formatterDMY(new Date());
    newRow.GioVe = '16:00';
    newRow.SoPhut = 0;
    newRow.IsChuNhat = false;
    newRow.TypeGroup=1;

    $("[for=chkNhanVien]").text(emp == null ? "Nhân viên" : 'Nhân viên "' + emp.TenNV + '"');
    $("[for=chkBoPhan]").text(Dept == null ? "Bộ phận" : 'Bộ phận "' + Dept.text + '"');

    $("#chkNhanVien").prop("checked", true);
    $('#dlg').dialog({ title: 'Chặn giờ', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('load', newRow);
}




function closeForm() {
    $('#fm').form('clear');
    $('#dlg').dialog('close');
}

function ok() {
    //var value = $("input[name='groupSet']:checked").val()
    var Temp = $('#fm').serializeObject();
    var rows = $('#dg-list').datagrid('getRows');
    Temp.IsChuNhat = $("input[name=IsChuNhat]").prop("checked");
    //Temp.TypeGroup = $("input[name='groupSet']:checked").val()
    
    $.ajax({
        url: '/Setting/DangKyChanGioSave',
        type: "POST",
        data: { Action: '', form: JSON.stringify(Temp), items: JSON.stringify(rows) },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1)
                alert(data);
            else
                parent.showMessager('Thông báo', 'Lưu thành công!');

            loadDataBy(getDepId(), getMaNV());
        },
        error: function (response) {
        }
    });
}

function Del() {
    //var value = $("input[name='groupSet']:checked").val()
    var Temp = $('#fm').serializeObject();
    var rows = $('#dg-list').datagrid('getRows');
    Temp.IsChuNhat = $("input[name=IsChuNhat]").prop("checked");
    //Temp.TypeGroup = $("input[name='groupSet']:checked").val()

    $.ajax({
        url: '/Setting/DangKyChanGioSave',
        type: "POST",
        data: { Action: 'DELETE', form: JSON.stringify(Temp), items: JSON.stringify(rows) },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1)
                alert(data);
            else
                parent.showMessager('Thông báo', 'Lưu thành công!');

            loadDataBy(getDepId(), getMaNV());
        },
        error: function (response) {
        }
    });
}


function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onSelect: gridLog_onSelect
    }).datagrid('keyCtr');

}
function gridLog_onSelect(index, row) {
    loadDataBy(getDepId(), row.MaNV);
}

/*** THIẾT LẬP CHỌN NGÀY GIỜ =================================================================================== ***/

function iniOption() {
    $('#cb_option').combobox({
        onChange: function (newValue, oldValue) {
        }
    });
    var myDate = new Date();
    $('#cb_option').combobox('setValue', myDate.getMonth() + 1);

    $('#cb-lydo').combobox({
        panelWidth: 190,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/Setting/JDataComboBoxLyDoVang'
    });
}

//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}

function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    loadDataBy(newValue, '');
}

function getBoPhan() {

    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert(JSON.stringify(response)); }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "0";
    return tNode.id;
}


function getSelectedNode() {
    var t = $('#cb-mabophan').combotree("tree");
    
    return t.tree('getSelected');
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') return "";
    return row.MaNV;
}

function getSelectedNhanVien() {
    return $('#dg-list').datagrid("getSelected");
}


function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}

function loadDataBy(DepId, MaNV) {
    //$('#dg-list').datagrid('loadData', []);
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    $.ajax({
        url: '/Setting/DangKyChatGioGetBy',
        type: "GET",
        data: { Month: getValueMonth(), Year: getValueYear(), DepId: DepId, MaNV: MaNV },
        dataType: "json",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress(); return;
                }

                $('#dg-list').datagrid('loadData', data);
                $('#dg-list').datagrid('selectRow', 0);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert("ERROR_loadDataDetail: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}



function Demo() {
    alert("DEMO");
}

function Demo1() {
    alert("DEMO 1");
}