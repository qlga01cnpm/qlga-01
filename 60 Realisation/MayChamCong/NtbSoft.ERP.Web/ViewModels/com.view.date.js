﻿/*** THIẾT LẬP CHỌN NGÀY GIỜ=================================================================================== ***/
function getDateFrom() {
	return $('#dt_from').datebox('getValue');
}

function getDateTo() {
	return $('#dt_to').datebox('getValue');
}

function getValueMonth() {
	return $('#cb_option').combobox('getValue');
}

function iniOption() {
	$('#cb_option').combobox({
		onChange: function (newValue, oldValue) {
			if (newValue != 13) {
				setDisable(true);
				getOption(newValue);
			} else {
				setDisable(false);
				getOption(20);
			}

		}
	});
	$('#cb_option').combobox('setValue', 15);
}

function setDisable(disabled) {
	$('#dt_from').datebox({ disabled: disabled });
	$('#dt_to').datebox({ disabled: disabled });
}

function getOption(type) {
	$.ajax({
		url: "/MCC/GetDateOption?type=" + type,
		type: "GET",
		async: false,
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function (data) {
			$('#dt_from').datebox('setValue', data[0]);
			$('#dt_to').datebox('setValue', data[1]);
		},
		error: function (response) {
			alert("ERROR: " + JSON.stringify(response));
		}
	});
}

//================================================================================================================