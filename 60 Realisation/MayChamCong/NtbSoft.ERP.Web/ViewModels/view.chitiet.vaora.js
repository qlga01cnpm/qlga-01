﻿function refresh() {
    //loadDataDetail();
    loadDataDetailPaging("", _selectedPage, _pageSize, true);
}

var rowSelected = null
function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onBeforeSelect: function (index, row) {
            $(this).datagrid('clearSelections');
        },
        onSelect: gridLog_onSelect
    }).datagrid("keyCtr");

    $('#dg-list').datagrid({
        url: null,
        pagination: true,
        pageSize: 20,
        pageNumber: 1,
        onBeforeSelect: function (index, row) {
             $(this).datagrid('clearSelections');
        },
        onSelect: function (index, row) {
            rowSelected = row; 
        },
        rowStyler: function (index, row) {
            if (row.StrThu && row.StrThu=="CN") {
                return 'background-color: #FFEAEA; color: #C20000; font-weight: bold;text-align:left';
            }
        }
    }).datagrid("keyCtr");

    //==Phan trang ==================================================
    var _d = new Date();
    var p = $("#dg-list").datagrid('getPager');
    $(p).pagination({
        pageSize: _pageSize
        , pageNumber: _selectedPage
        , showPageList: true
        , pageList: [30, 50, 100, 200, 300,500]
        , onSelectPage: function (pageNumber, pageSize) {
            $(this).pagination('loading');
            $(this).pagination('loaded');
            _selectedPage = pageNumber;
            _pageSize = pageSize;
            var node = $('#right-menu').tree('getSelected'); // get selected  nodes
            var DepID = '';
            if (node != null)
                DepID = node.id;

            loadDataDetailPaging("", _selectedPage, _pageSize,false);

        }
    });

}
 
function iniGridNhanVien() {
    $('#dg-nhanvien').datagrid({
        onSelect: onSelectGridNhanVien
    }).datagrid("keyCtr");
}

function onSelectGridNhanVien(index, row) {
    rowSelected = null;
    loadDataDetailPaging(row.MaNV, _selectedPage, _pageSize, true);
}

function gridLog_onSelect(index, row) {
   // loadDataDetail();
}

/*** THIẾT LẬP CHỌN NGÀY GIỜ=================================================================================== ***/
function getDateFrom() {
    return $('#dt_from').datebox('getValue');
}

function getDateTo() {
    return $('#dt_to').datebox('getValue');
}

function getValueMonth() {
    return $('#cb_option').combobox('getValue');
}

function iniOption() {
    $('#cb_option').combobox({
        onChange: function (newValue, oldValue) {
            if (newValue != 13) {
                setDisable(true);
                getOption(newValue);
            } else {
                setDisable(false);
                getOption(20);
            }

        }
    });
    $('#cb_option').combobox('setValue', 16);
}

function setDisable(disabled) {
    $('#dt_from').datebox({ disabled: disabled });
    $('#dt_to').datebox({ disabled: disabled });
}

function getOption(type) {
    $.ajax({
        url: "/MCC/GetDateOptionDMY?type=" + type,
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#dt_from').datebox('setValue', data[0]);
            $('#dt_to').datebox('setValue', data[1]);
        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
        }
    });
}

//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}
function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    //loadDataDetail();
    _selectedPage = 1;
    loadDataDetailPaging("", _selectedPage, _pageSize, true);
}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');
        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {

    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        //timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert(JSON.stringify(response)); }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "DEP.ROOT";
    return tNode.id;
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') return "";
    return row.MaNV;
}

function clearSelections() {
    rowSelected = null;
    $('#dg-nhanvien').datagrid("clearSelections");
}

function loadDataDetail() {
    var newUrl = "/MCC/ChiTietGioVaoRaGet?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + getDepId() + "&MaNV=" + getMaNV();
    //alert(newUrl);
    //$('#dg-list').datagrid({ url: newUrl, remoteSort: true });
    //$('#dg-list').datagrid('load', { total: 100000 });
    $.ajax({
        url: newUrl,
        type: "GET",
        //timeout: 60000,
        //async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                //$('#dg-list').datagrid('loading');
                //$('#dg-list').datagrid('loadData', data);
                //$('#dg-list').datagrid('loaded');
                //$('#dg-list').datagrid({ data: getData() }).datagrid('clientPaging');
                $('#dg-list').datagrid({ data: getData() }).datagrid('clientPaging');
                //$('#dg-list').datagrid('loaded');

                //var p = $("#dg-list").datagrid('getPager');
            //    $(p).pagination({
            //        total: data.length
            //    , pageSize: 20
            //, pageList: [20, 30, 50]
            //    , pageNumber: 1,
            //       // displayMsg: 'Data at: ' + _d.toTimeString().split(' ')[0] + ' -Displaying {from} to {to} of {total} items',
            //        onSelectPage: function (pageNumber, pageSize) {
            //            $(this).pagination('loading');
            //            $(this).pagination('loaded');
            //        }
            //    });
               // $('#dg-list').datagrid({ data: data });
                var kkq = "";
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert("ERROR: " + JSON.stringify(response)); }
    });
}

var _selectedPage = 1, _pageSize = 300, _TotalRecords = 0;
function loadDataDetailPaging(maNV, selectedPage, pageSize, async) {
    //parent.setProgress("Vui lòng chờ");
    //parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var MaNV = maNV, DepId = getDepId();
    var newUrl = "/MCC/ChiTietGioVaoRaGetPaging?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + DepId + "&MaNV=" + MaNV;
    //async: false; Nghĩa là khi một request về Server thì tất cả các request tiếp theo cần phải chờ cho đến khi Request ajax này xử lý xong mới được xử lý tiếp. Do đó với các xử lý từ Server mà lâu thì các tiến trình tiếp theo sẽ phải chờ đợi lâu cho đến khi kết thúc.
    //async: true; nghĩa là sau khi bạn click button ajax request về server. Bạn hoàn toàn có thể thực hiện gửi tiếp các yêu cầu xử lý khác về server.
    $.ajax({
        url: newUrl,
        type: "GET",
        //timeout: 60000,
        async: async,
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    //parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);
                _TotalRecords = item.TotalRecords;
                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            //parent.closeProgress();

        }, error: function (response) {
            alert(JSON.stringify(response));
            //parent.closeProgress();
        }
    });
}

function getData() {
    var rows = [];
    //15531
    for (var i = 1; i <= 500; i++) {
        var amount = Math.floor(Math.random() * 1000);
        var price = Math.floor(Math.random() * 1000);
        rows.push({
            MaNV: 'NV' + i,
            
        });
    }
    return rows;
}


/*** PHẦN TÍNH CÔNG ==============================================================================***/
function tinhCong() {
    //var rows = $('#dg-list').datagrid('getSelections');
    //var rows = $('#dg-list').datagrid('getChecked');
    var rows = $('#dg-list').datagrid('getRows');
    if (rows == null || rows == "") return;

    //return;
    var msg = "Tính công cho nhân viên. Bạn có muốn tính hay không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================
                parent.setProgress("Vui lòng chờ");
                beginTinhCong2(1, rows, 0);
                //beginTinhCong(rows, 0);
            }//End (r)==============================================
        }
    });
}

function beginSelectPage(page) {
    var totalPages = Math.round(_TotalRecords / _pageSize);
    if (page >= totalPages) return;
        
    var p = $("#dg-list").datagrid('getPager');
    $(p).pagination('select', page);	// select the second page

   
    //if (rows != null) {
    //    for (var i = 0; i < rows.length; i++) {
    //        $('#dg-list').datagrid('selectRow', i);
    //    }

       
    //}

    setTimeout(function () {
        beginSelectPage(++page);

    }, 1);
}

function beginTinhCong2(page, rows, index) {
    if (rows == null) return;
    if (index >= rows.length) {
        //Lấy tổng số trang
        //Cấu trúc chia nguyên / và chia dư % chỉ là phép chia.
        //Cấu trúc (int)s là ép kiểu sữ liệu nhé, chứ k phải lấy phần nguyên và phần dư.
        //Ví dụ 3,6%1 hoặc (int)3.6 sẽ là 3, nhưng phần nguyên của nó là 3, phần dư là 0.6.
        //Phép chia nguyên và sư, toán tử ép kiểu nếu phần dư >=0.5 thì nó sẽ tăng 1, Tùy ngôn ngữ hỗ trợ lấy phần dư hoặc nguyên chứ k thì bạn tự viết if else mà tính.

        var totalPages = Math.round(_TotalRecords / _pageSize);
        if ((_TotalRecords / _pageSize) % 1 > 0)
            totalPages = totalPages + 1;
        //Nếu có 1 trang thì thoát luôn
        if (page >= totalPages) {
            parent.closeProgress();
            var p = $("#dg-list").datagrid('getPager');
            $(p).pagination('select', 1);	// select the second page
            return;
        }
        else {
            //Trường hợp có nhiều trang 
            var p = $("#dg-list").datagrid('getPager');
            $(p).pagination('select', ++page);	// select the second page
            index = 0;
            rows = $('#dg-list').datagrid('getRows');
        }

    }

    parent.setProgressMsg('Đang tính toán...' + index);
    $.ajax({
        url: '/MCC/TinhCong',
        type: "POST",
        data: { jsonNV: JSON.stringify(rows[index]) },
        dataType: "json",
        success: function (data) {
            if (JSON.stringify(data).indexOf("OK") == -1 && JSON.stringify(data).indexOf("ERROR:") == -1) {
                var rowIndex = $('#dg-list').datagrid('getRowIndex', rows[index]);
                if (rowIndex > -1)
                    $('#dg-list').datagrid('updateRow', {
                        index: rowIndex,
                        row: {
                            Shift: data.Shift, TotalHours: data.TotalHours, OTX: data.OTX, LyDoVang: data.LyDoVang,
                            TotalTimeHM: data.TotalTimeHM, LateTime: data.LateTime, EarlyTime: data.EarlyTime
                        }
                    });
            }
            //parent.setProgressMsg('Đang tính toán...' + index);
            //parent.setProgressMsg("Kết nối " + rows[index].Name + ": " + data);
            // sau n giây sẽ kết nối tiếp
            setTimeout(function () {
                $('#dg-list').datagrid('selectRow', index);
                beginTinhCong2(page, rows, ++index);
            }, 1);

        },
        error: function (response) {
            beginTinhCong2(page, rows, ++index);
            //alert(JSON.stringify(response));
        }
    }); //End  $.ajax

   
}

function beginTinhCong(rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    
    parent.setProgressMsg('Đang tính toán...' + index);
    $.ajax({
        url: '/MCC/TinhCong',
        type: "POST",
        data: { jsonNV: JSON.stringify(rows[index]) },
        dataType: "json",
        success: function (data) {
            if (JSON.stringify(data).indexOf("OK") == -1 && JSON.stringify(data).indexOf("ERROR:") == -1) {
                var rowIndex = $('#dg-list').datagrid('getRowIndex', rows[index]);
                if (rowIndex > -1)
                    $('#dg-list').datagrid('updateRow', {
                        index: rowIndex,
                        row: {
                            Shift: data.Shift, TotalHours: data.TotalHours, OTX: data.OTX, LyDoVang: data.LyDoVang,
                            TotalTimeHM: data.TotalTimeHM, LateTime: data.LateTime, EarlyTime: data.EarlyTime
                        }
                    });
            }
            //parent.setProgressMsg('Đang tính toán...' + index);
            //parent.setProgressMsg("Kết nối " + rows[index].Name + ": " + data);
            // sau n giây sẽ kết nối tiếp
            setTimeout(function () {
                $('#dg-list').datagrid('selectRow', ++index);
                beginTinhCong(rows, ++index);
            }, 1);

        },
        error: function (response) {
            beginTinhCong(rows, ++index);
            //alert(JSON.stringify(response));
        }
    }); //End  $.ajax
}
//KẾT THÚC PHẦN TÍNH CÔNG =====================================================================

function formatOTX(value, row, index) {
    if (value > 0)
        return value;
}

function exportChiTietVaoRa() {
    //debugger
    var rows = $('#dg-nhanvien').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Bạn chưa chọn nhân viên!', 'warning');
        return;
    }
    var k = rows.length, arr = '';
    for (var i = 0; i < k; i++) {
        arr += rows[i].Id;
        if (i < k - 1) arr += ',';
    }
    var date = parserDMY(getDateFrom()); 

    var msg = "Báo cáo chi tiết tháng. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================
                //alert(arr);
                var formData = new FormData();
                formData.append('arrayMaNV', arr);
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpChiTietVaoRaThang?Month=" + (date.getMonth() + 1) + "&Year=" + date.getFullYear() + "&arrayMaNV=" + "", true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "bc_chi_tiet_thang.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(formData);
                //ajax.send(null);

            }//End (r)==============================================
        }
    }); //End $.messager.confirm
}

function exportChiTietNgay() {
    //debugger
    var rows = $('#dg-nhanvien').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Bạn chưa chọn nhân viên!', 'warning');
        return;
    }
    var k = rows.length, arr = '';
    for (var i = 0; i < k; i++) {
        arr += rows[i].Id;
        if (i < k - 1) arr += ',';
    }

    var msg = "Báo cáo chi tiết tháng. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================
                //alert(arr);
                var formData = new FormData();
                formData.append('arrayMaNV', arr);
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpChiTietNgay?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&arrayMaNV=" + "", true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "bc_cong_hang_ngay.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(formData);
                //ajax.send(null);

            }//End (r)==============================================
        }
    }); //End $.messager.confirm
}

function ex() {
    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Xuất báo cáo chi tiết',
        draggable: false,
        fn: function (ok) {
            if (ok) {//             
                alert("OK");
            }
        }
    });
}

function exportDiMuonVS() {

    var DepId = "DEP.ROOT";
    //alert(getDateFrom() + "- to: " + getDateTo());
    var msg = "Báo cáo nhân viên đi muộn về sớm. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpDiMuonVS?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + DepId, true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Ds_nhan_vien_di_muon_ve_som.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);
            }//End (r)==============================================
        }
    });
}

function exportKhogChamCong() {
    var msg = "Báo cáo những nhân viên không chấm công. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================


                var DepId = "DEP.ROOT";
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpKhongChamCong?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + DepId, true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Ds_nhan_vien_khong_cham_cong.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}