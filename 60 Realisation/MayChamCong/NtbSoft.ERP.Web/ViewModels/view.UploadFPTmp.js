﻿function refresh() {
    var node = $('#right-menu').tree('getSelected'); // get selected  nodes
    if (node != null)
        loadDataPaging(_selectedPage, _pageSize, node.id,"");
}

function iniDataGrid() {
    $('#dg-list').datagrid({
        onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        }
    }).datagrid('keyCtr');
}
function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        panelWidth: '25%',
        onChange: depOnChange
    });
}
function depOnChange(newValue, oldValue) {
    loadDataPaging(1, 10000, newValue, "", "0");
}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');
        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function getMCC() {
    $.ajax({
        url: "/MCC/GetTerminal",
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#dg-mcc').datagrid({
                onBeforeSelect: function (index, row) {
                    $('#dg-mcc').datagrid('clearSelections');
                },
                data: data,
                rowStyler: function (index, row) {
                    if (index % 2 > 0) {
                        return 'background:#f0f0f0';
                    }
                }
            }).datagrid('keyCtr');

        },
        error: function (response) {
            alert(JSON.stringify(response));
            //window.location.href = "/Login.aspx";
        }
    });
}//end function getMCC()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId, TenNV, MaCC) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/MCC/GetViewVanTayNV?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + encodeURIComponent(TenNV) + "&MaCC=" + encodeURIComponent(MaCC);
    //newUrl = encodeURIComponent(newUrl);
    $.ajax({
        url: newUrl,
        type: "GET",
        //timeout: 60000,
        //async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress(); return;
                }
                $('#dg-list').datagrid('loadData', data);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        }, error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function beginUpload(url, rows, index, privilege) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang tải lên " + rows[index].Name + "...");
    var rowsNV = $('#dg-list').datagrid('getChecked');
    $.ajax({
        //url: '/MCC/BeginUploadFPTmp',
        url: url,
        type: "POST",
        data: { id: rows[index].TerminalID, Privilege: privilege, Item: JSON.stringify(rowsNV) },
        dataType: "json",
        success: function (data) {
            if (data == "OK" || data == 'Fail') {
                parent.setProgressMsg("Đã tải lên " + rows[index].Name + ": " + data);
                if (data == 'OK')
                    setAdmin(rowsNV, privilege);
            }
            else
                parent.setProgressMsg("Đã tải lên " + rows[index].Name + ": " + data);

            setTimeout(function () {
                $('#dg-list').datagrid('selectRow', ++index);
                beginUpload(url, rows, ++index, privilege);
            }, 1500);
        },
        error: function (response) {
            beginUpload(url, rows, ++index, privilege);
        }
    }); //End ajax
}

function uploadFP(privilege) {

    var rowsMcc = $('#dg-mcc').datagrid('getChecked');
    if (rowsMcc == null || rowsMcc == '') {

        parent.showAlert('Thông báo', 'Chưa chọn máy chấm công!', 'warning');
        return;
    }
    if (checkNhanVien()) return;
    var msg = "Bạn có muốn tải thông tin của nhân viên (vai trò là Người Dùng), lên máy chấm công hay không?";
    if (privilege > 1)
        msg = "Bạn có muốn tải thông tin của nhân viên (vai trò là người Quản Trị), lên máy chấm công hay không?";
    $.messager.confirm({
        title: 'Xác nhận', left: '13%', top: '10%', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================
                parent.setProgress("Vui lòng chờ");
                var url = '/MCC/BeginUploadFPTmp';
                beginUpload(url, rowsMcc, 0, privilege);
            }//End (r)==============================================
        }
    });

}

function setAdmin(rows, privilege) {
    for (var i = 0; i < rows.length; i++) {

        var index = $('#dg-list').datagrid('getRowIndex', rows[i]);
        if (index > -1) {
            $('#dg-list').datagrid('updateRow', { index: index, row: { Privilege: privilege } });
            //Khong can su dung: $('#dg-list').datagrid('refreshRow', index);
        }
    }
}

function checkNhanVien() {
    var rows = $('#dg-list').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên!', 'warning');
        return true;
    }
    return false;
}

//Begin Test Connect===============================================================================================
function connectTest() {
    var rows = $('#dg-mcc').datagrid('getChecked');
    if (rows == null || rows == "") {
        parent.showAlert('Thông báo', 'Chưa chọn máy chấm công để kết nối!', 'warning');
        return;
    }
    parent.setProgress("Vui lòng chờ");
    connectTestAjax(rows, 0);
}

function connectTestAjax(rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang kết nối " + rows[index].Name + "...");
    $.ajax({
        url: '/MCC/TerminalTestConnect?id=' + rows[index].TerminalID,
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data == "OK" || data == 'Fail') {
                parent.setProgressMsg("Kết nối " + rows[index].Name + ": " + data);
            }
            else
                parent.setProgressMsg("Kết nối " + rows[index].Name + ": " + data);

            //2(giây) sau sẽ kết nối tiếp
            setTimeout(function () {
                connectTestAjax(rows, ++index);
            }, 1500);

        },
        error: function (response) {
            connectTestAjax(rows, ++index);
            //alert(JSON.stringify(response));
        }
    });
}//end function connectTestAjax()=======================================================================================================================
//End Test connect==============================================================================================================================================
function doSearch(value, name) {
    //alert('You input: ' + value);
    var DepID = $('#cb-mabophan').combotree('getValue');
    if (DepID == null) DepID = 'DEP.ROOT';
    if (name == 'MaNV')
        loadDataPaging(1, 10000, DepID, "", value);
    else
        loadDataPaging(1, 10000, DepID, value, "0");
}

function formatStatus(value, row, index) {
    //alert(value);
    if (value == 1)
        return '<h6 class="easyui-linkbutton easyui-tooltip" title="Có vân tay" style="background: url(\'/icons/tick_small.png\') no-repeat center center;width:32px;height:24px;margin:auto;" ></h6>';
}
//Định dạng vai trò của người dùng: là người sử dụng, hay là người quản trị
function formatUser(value, row, index) {
    //alert(value);
    if (value == 2 || value == 3)
        return '<h6 class="easyui-linkbutton easyui-tooltip" title="Quản trị cao cấp" style="background: url(\'/icons/login.png\') no-repeat center center;width:32px;height:24px;margin:auto;" ></h6>';
}
function formatIsPassword(value, row, index) {
    //alert(value);
    if (value)
        return '<h6 class="easyui-linkbutton easyui-tooltip" title="Dùng mật mã" style="background: url(\'/Images/key_small.png\') no-repeat center center;width:32px;height:24px;margin:auto;" ></h6>';
}