﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.API
{
    [Microsoft.Web.Http.ApiVersion("1")]
    [Route("api/v{version:apiVersion}/NgayLeApi/{action}")]
    [Route("api/v{version:apiVersion}/NgayLeApi/{action}/{id}")]
    public class NgayLeApiController : ApiController
    {
        public dynamic Get()
        {
            Models.DIC.Interfaces.INgayLe ngayle = new Models.DIC.clsNgayLe();
            List<Entity.Interfaces.INgayLeInfo> NgayLeInfo = null;
            Models.Interfaces.IResultOk ok = null;
            ngayle.GetAll((result, ex) => { NgayLeInfo = result; ok = ex; });

            return NgayLeInfo;
        }
    }
}
