﻿using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.API
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/Server/{action}")]
    [Route("api/v{version:apiVersion}/Server/{action}/{id}")]
    public class ServerController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public string RegisterIpWan(string ServerID, string IpLan, string IpWan, string Mac, int Port)
        {
            try
            {
                if (Models.clsServer.RegisterIpWan(ServerID, IpLan, IpWan, Mac, Port))
                    return "true";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }

            return "false";
        }

        // GET api/<controller>/5
        public string GetUrl(string ServerID)
        {
            try
            {
                return Models.clsServer.GetUrl(ServerID);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        public string GetUrlByMenuId(string MenuId)
        {
            try
            {
                return Models.clsServer.GetUrlByMenuId(MenuId);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}