﻿using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.API
{
    [ApiVersion("1")]
    [Route("api/Template/{action}")]
    [Route("api/v{version:apiVersion}/Template/{action}")]
    [Route("api/v{version:apiVersion}/Template/{action}/{id}")]
    public class TemplateController : ApiController
    {
        
    }
}
