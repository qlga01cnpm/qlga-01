﻿using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.API
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/Todo/{action}")]
    [Route("api/v{version:apiVersion}/Todo/{action}/{id}")]
    //[Route("api/v1.0/Todo/{action}/{id}")]
    //[Route("api/v1.0/Todo/{action}")]
    public class TodoController : ApiController
    {
        //http://beginloop.com/2017/01/27/web-api-versioning-using-url-path-in-asp-net/
        [HttpPost]
        public IHttpActionResult GetTodo()
        {
            return Ok("OK");
        }


        [HttpPost]
        public IHttpActionResult GetTodo2()
        {
            string _IpWan = GetIP();
            Models.clsServer.RegisterIpWan("682d7169-44ea-4acf-b876-74d1beecfdf3", "", _IpWan, Libs.clsGenerateUniqueID.macId(), 90);
            return Ok("Current IP Address: " + _IpWan);
        }

        public string GetIP()
        {
            string externalIP = "";
            try
            {
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new System.Text.RegularExpressions.Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch (Exception ex)
            {
                return externalIP;
            }
        }

        [HttpPost]
        public IHttpActionResult GetTodoById(int id)
        {
            return Ok(id);
        }

        [HttpPost]
        public IHttpActionResult GetTodoById2(string id,string name)
        {
            return Ok(id + " : " + name);
        }
    }
}
