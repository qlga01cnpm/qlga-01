﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="line.aspx.cs" Inherits="NtbSoft.ERP.Web.DemoEasyUI.line" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="easyui.css" rel="stylesheet" type="text/css" />
    <link href="icon.css" rel="stylesheet" type="text/css" />
    <script src="../EasyUI/jquery-11.3.min.js" type="text/javascript"></script>
    <script src="../EasyUI/jquery.easyui.min.js" type="text/javascript"></script>

     <style type="text/css">
    .easyui-layout{position: fixed; top: 0;left: 0;bottom: 0;right: 0;overflow: auto; background:#fff; color:White;}
    </style>
    <script type="text/javascript">
        var treeGridData = [{
            "id": 1,
            "name": "C",
            "size": "",
            "date": "02/19/2010",
            "children": [{
                "id": 2,
                "name": "Program Files",
                "size": "120 MB",
                "date": "03/20/2010",
                "children": [{
                    "id": 21,
                    "name": "Java",
                    "size": "",
                    "date": "01/13/2010",
                    "state": "closed",
                    "children": [{
                        "id": 211,
                        "name": "java.exe",
                        "size": "142 KB",
                        "date": "01/13/2010"
                    }, {
                        "id": 212,
                        "name": "jawt.dll",
                        "size": "5 KB",
                        "date": "01/13/2010"
                    }]
                }, {
                    "id": 22,
                    "name": "MySQL",
                    "size": "",
                    "date": "01/13/2010",
                    "state": "closed",
                    "children": [{
                        "id": 221,
                        "name": "my.ini",
                        "size": "10 KB",
                        "date": "02/26/2009"
                    }, {
                        "id": 222,
                        "name": "my-huge.ini",
                        "size": "5 KB",
                        "date": "02/26/2009"
                    }, {
                        "id": 223,
                        "name": "my-large.ini",
                        "size": "5 KB",
                        "date": "02/26/2009"
                    }]
                }]
            }, {
                "id": 3,
                "name": "eclipse",
                "size": "",
                "date": "01/20/2010",
                "children": [{
                    "id": 31,
                    "name": "eclipse.exe",
                    "size": "56 KB",
                    "date": "05/19/2009"
                }, {
                    "id": 32,
                    "name": "eclipse.ini",
                    "size": "1 KB",
                    "date": "04/20/2010"
                }, {
                    "id": 33,
                    "name": "notice.html",
                    "size": "7 KB",
                    "date": "03/17/2005"
                }]
            }]
        }];

        $(document).ready(function () {
            $("#tg").treegrid({
                lines: true,
                loadFilter:myLoadFilter,
                loader: function (param, success, error) {
                    if (!param.id) {

                        data = treeGridData;   // retrieve the top level data.
                        success(data);
                    }
                    else {
                        data = getData(param.id); // retrieve the children data of the current expanded node.
                        // this line still overrides the entire tree's data instead of appending the current data to the expanded parent
                        success(data);
                    }
                },
                onContextMenu: function (e, row) {
                    if (row) {
                        e.preventDefault();
                        $(this).treegrid('select', row.id);
                        $('#mm').menu('show', {
                            left: e.pageX,
                            top: e.pageY
                        });
                    }
                }
            });

        });
       
        var idIndex = 100;
        function append() {
            idIndex++;
            var d1 = new Date();
            var d2 = new Date();
            d2.setMonth(d2.getMonth() + 1);
            var node = $('#tg').treegrid('getSelected');
            $('#tg').treegrid('append', {
                parent: node.id,
                data: [{
                    id: idIndex,
                    name: 'New Task' + idIndex,
                    persons: parseInt(Math.random() * 10),
                    begin: $.fn.datebox.defaults.formatter(d1),
                    end: $.fn.datebox.defaults.formatter(d2),
                    progress: parseInt(Math.random() * 100)
                }]
            })
        }

        function setData(data) {
            var todo = [];
            for (var i = 0; i < data.length; i++) {
                todo.push(data[i]);
            }
            while (todo.length) {
                var node = todo.shift();
                if (node.children && node.children.length) {
                    node.state = 'closed';
                    node.children1 = node.children;
                    node.children = undefined;
                    todo = todo.concat(node.children1);
                }
            }
        }

        function myLoadFilter(data, parentId) {
            function setData(data) {
                var todo = [];
                for (var i = 0; i < data.length; i++) {
                    todo.push(data[i]);
                }
                while (todo.length) {
                    var node = todo.shift();
                    if (node.children && node.children.length) {
                        node.state = 'closed';
                        node.children1 = node.children;
                        node.children = undefined;
                        todo = todo.concat(node.children1);
                    }
                }
            }

            setData(data);
            var tg = $(this);
            var opts = tg.treegrid('options');
            opts.onBeforeExpand = function (row) {
                if (row.children1) {
                    tg.treegrid('append', {
                        parent: row[opts.idField],
                        data: row.children1
                    });
                    row.children1 = undefined;
                    tg.treegrid('expand', row[opts.idField]);
                }
                return row.children1 == undefined;
            };
            return data;
        }

        function removeIt() {
            var node = $('#tg').treegrid('getSelected');
            if (node) {
                $('#tg').treegrid('remove', node.id);
            }
        }
    </script>
</head>
<body class="easyui-layout" oncontextmenu="return false;">

    <table id="tg" title="TreeGrid Lines" class="easyui-treegrid" style="width:80%;height:100%"
			data-options="
				rownumbers: true,
				idField: 'id',
				treeField: 'name'
			">
		<thead>
			<tr>
				<th data-options="field:'name'" width="220">Name</th>
				<th data-options="field:'size'" width="100" align="right">Size</th>
				<th data-options="field:'date'" width="150">Modified Date</th>
			</tr>
		</thead>
	</table>
    
     <div id="mm" class="easyui-menu" style="width:120px;">
        <div onclick="append()" data-options="iconCls:'icon-add'">Append</div>
        <div onclick="removeIt()" data-options="iconCls:'icon-remove'">Remove</div>
        <div class="menu-sep"></div>
        <div onclick="collapse()">Collapse</div>
        <div onclick="expand()">Expand</div>
    </div>
   
</body>
</html>
