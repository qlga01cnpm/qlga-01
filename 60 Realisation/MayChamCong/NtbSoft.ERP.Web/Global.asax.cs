﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NtbSoft.ERP.Models;
using System.Data;
using NtbSoft.ERP.Entity.Interfaces;
using NtbSoft.Entities.Interfaces.MCC;

namespace NtbSoft.ERP.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        System.Timers.Timer aTimer = null;

        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Mặc định hệ thống sẽ hỗ trợ hai engine: Razor và Aspx. 
            //Và không có lý do gì để ta phải trả phí cho cái mà ta không sử dụng, 
            //điều này sẽ làm hệ thống của chúng ta chậm, do đó chúng ta cần điều chỉnh lại, 
            //chỉ sử dụng một Engine duy nhất. Trong trường hợp này, tôi đang sử dụng Raror nên tôi sẽ add Raror Engine
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            if (AppCaches.clsCacheTheApp.Instance.IsRunCC)
            {
                //aTimer = new System.Timers.Timer();
                //aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                //aTimer.Interval = 300000;
                //aTimer.Enabled = true;

                //Services.JobScheduler.Start();
                //Services.JobScheduleMcc.Instance.Start();
            }
        }
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            aTimer.Start();
            AutoGetLogData();
            if (!AppCaches.clsCacheTheApp.Instance.IsRunCC)
            {
                if (aTimer != null)
                {
                    aTimer.Stop();
                }
            }
        }

        private void AutoGetLogData()
        {
            clsTerminal terminalService = new clsTerminal();
            List<ITerminalInfo> Items = null;
            Exception _ex = null;

            terminalService.GetAll((items, ex) =>
            {
                if (ex == null) Items = items.ToList();
                else
                    _ex = ex;
            });

            if (Items != null && Items.Count > 0)
            {
                foreach (ITerminalInfo item in Items)
                {
                    Devices.MCC.Services.Interfaces.IAttLogs _attLogs;
                    if (item.CommType == 1) { }
                    //_attLogs= new Devices.MCC.Services.AttLogs(1,new Devices.MCC.ConnectCOM()
                    else if (item.CommType == 2)
                    {
                        _attLogs = new Devices.MCC.Services.AttLogs(item.TerminalID, item.LastRecordDate,
                            new Devices.MCC.Services.ConnectNet(item.IpAddress, item.Port));
                        List<ICardUserInfo> _listUserLogs = null;

                        _attLogs.Connect((connected, ex) =>
                        {
                            if (connected)
                            {
                                _attLogs.DownloadAttLogs((result, ex1) =>
                                {
                                    if (result != null)
                                    {
                                        _listUserLogs = result.Select(i =>
                                        {
                                            if (!string.IsNullOrWhiteSpace(i.UserId)) i.MachineNumber = item.TerminalID;
                                            return i;
                                        }).ToList();

                                    }
                                });
                            }
                        });

                        if (_listUserLogs != null && _listUserLogs.Count > 0)
                        {
                            //Cap nhat du lieu that
                            NtbSoft.ERP.Models.Interfaces.IQuyTacLamTron QuyTacLamTron = new NtbSoft.ERP.Models.clsQuyTacLamTron();
                            NtbSoft.ERP.Models.Interfaces.IQuyTac QuyTacCC = new NtbSoft.ERP.Models.clsQuyTac(QuyTacLamTron);
                            Models.DIC.Interfaces.IDangKyCheDo _DangKyCheDo = new NtbSoft.ERP.Models.DIC.clsDangKyCheDo();
                            Models.Interfaces.INhanVien _nhanvien = new NtbSoft.ERP.Models.clsNhanVienCC();

                            Models.Interfaces.IShift _clsShifts = new Models.clsShift();
                            //Lịch trình ca
                            Models.Interfaces.IScheduleSchift _ScheduleShift = new Models.clsScheduleShift();

                            NtbSoft.ERP.Models.Interfaces.IBase<ICardUserInfo> _baseBus =
                                new ERP.Models.clsAttLogs(_nhanvien, QuyTacCC, _DangKyCheDo, _clsShifts, _ScheduleShift);
                            _baseBus.BatchInsert(_listUserLogs, ex => { });
                        }
                    }
                }
            }
        }

        protected void ConfigureQuartzJobs()
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //string culture = "vi-VN";//ngon ngu mac dinh
            //var httpCookie = Request.Cookies["language"];
            //if (httpCookie != null)
            //{
            //    culture = httpCookie.Value;
            //}
            //else
            //{
            //    HttpCookie language = new HttpCookie("language");
            //    language.Value = culture;
            //    language.Expires = DateTime.Now.AddDays(1);
            //    Response.Cookies.Add(language);
            //}

            //System.Globalization.CultureInfo newCulture = new System.Globalization.CultureInfo(culture);

            System.Globalization.CultureInfo newCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";
            newCulture.DateTimeFormat.DateSeparator = "/";

            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;

        }

        void Application_End(object sender, EventArgs e)
        {
            if (aTimer != null)
            {
                aTimer.Stop();
            }
        }

        void Session_End(object sender, EventArgs e)
        {
            if (aTimer != null)
            {
                aTimer.Stop();
            }
        }
    }

    public class cls { }
}
