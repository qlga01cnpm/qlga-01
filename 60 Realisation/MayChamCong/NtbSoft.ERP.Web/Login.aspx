﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NtbSoft.ERP.Web.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
        RenderOuterTable="false" OnLoggedIn="LoginUser_LoggedIn">
        <LayoutTemplate> 
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                ValidationGroup="LoginUserValidationGroup" />
            <nav class="navbar navbar-inverse navbar-fixed-top" style="background:#2D3E50">
              <div class="">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#" style="font-weight:bold;color:#fff;"> HỆ THỐNG QUẢN LÝ GARAGE ÔTÔ</a>
                </div>
              </div>
            </nav>
            
            <body style="
                font-family: 'Open Sans', sans-serif;
                background: url(../Images/bann.jpg) no-repeat 0px 0px;
                background-position: center;
                background-attachment: fixed; ">
            
            <div class="container"  style="margin-top:150px;">
                       <!--h3 class="panel-title" style="font-weight: bold; color: #c36116; margin-left:150px;">Thông tin đăng nhập</!--h3-->
                <p>
                   <fieldset class="login" >
                       <div class="form-group" style="margin-bottom: 0; width: 40%;
                                                                        margin: 0 auto;
                                                                        padding: 4em 2em;
                                                                        background: #60b7a9;
                                                                        box-shadow: 0 6px 43px 0 rgba(0, 0, 0, 0.4);
                                                                        background-color: rgba(0, 0, 0, 0.16); /*làm nhạt nền khung*/  
                                                                        font-size: 15px;
                                                                        box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.4);">
                                                       
                        <h2 style="  color: #000;font-size: 30px;font-weight: 300;text-align: center;margin-bottom: 1.5em; color:#ffffff;font-family:Arial;font-weight:bold;color: #fff; text-shadow: 3px 3px 4px #000;"> Đăng Nhập</h2>
                                                       
                                                                    <asp:TextBox ID="UserName" placeholder="Tài khoản" runat="server" Text="" CssClass="textEntry form-control" ></asp:TextBox>
                                                                    <asp:RequiredFieldValidator Width="20px" ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                        CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                                               
                                                
                                                                    <asp:TextBox ID="Password" placeholder="Mật khẩu" runat="server" Text="" CssClass="passwordEntry form-control" TextMode="Password"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator Width="20px" ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                        CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                                         
                                                     <p style="color:#fff">    
                                                        <asp:CheckBox ID="RememberMe" runat="server"/>
                                                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Lưu thông tin đăng nhập</asp:Label>
                                                    </p> 
                                                    <!-- Change this to a button or input when using this as a form -->
                                                    <p class="submitButton" style=" text-align: center; margin-top: 2em;  ">
                                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btn btn-success" Text="Đồng ý" ValidationGroup="LoginUserValidationGroup" />
                                                    </p>
                                               </div>
                                         </fieldset>
                                      </p>
                           </div>
                </body>
                                       

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutTemplate>
    </asp:Login>
</asp:Content>
