﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    public class ManagerController : Controller
    {
        public ActionResult Home()
        {
            //ViewBag.Title = "Standard Operation Analysis";
            ViewBag.Title = "Máy chấm công";

            //TestTest();
            return View();
        }

        private void TestTest()
        {
            NtbSoft.ERP.Models.clsSysTest _test
                = new Models.clsSysTest();

            Entity.clsSysTestInfo Item = new Entity.clsSysTestInfo();
            Item.Id = 2;
            Item.CardId = "3";
            Item.Name = "Hai";
            Item.Value = 100;
            _test.Insert(Item);
        }

    }
}
