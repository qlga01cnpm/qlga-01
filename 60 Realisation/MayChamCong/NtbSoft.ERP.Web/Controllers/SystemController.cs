﻿using Microsoft.Web.Http;
using Newtonsoft.Json.Linq;
using NtbSoft.ERP.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    public class SystemController : Controller
    {
        #region Thông tin đăng nhập
        public ActionResult ChangePassword()
        {
            ViewBag.Title = "Thay đổi mật khẩu";
            return View();
        }
        #endregion

        #region Thông tin ứng dụng
        public ActionResult AppInfo()
        {
            ViewBag.Title= "Thông tin ứng dụng";
            return View();
        }

        public JsonResult AppGetInfo()
        {
            //return Json("ERROR: ", JsonRequestBehavior.AllowGet);
            Models.SYS.Interfaces.IApp theApp = new Models.SYS.clsApp();
            Models.Interfaces.IResultOk ok = null;
            Entity.Interfaces.IAppInfo Item=null;
            theApp.GetInfo((result, ex) => { Item = result; ok = ex; });
            if (!ok.Success || Item==null) return Json("ERROR: " + ok.Message);

            return Json(Item, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AppSaveInfo(string item)
        {
            if (string.IsNullOrWhiteSpace(item))
                return Json("ERROR: item nhận bị null", JsonRequestBehavior.AllowGet);
            var Info = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsAppInfo>(item);
            Models.SYS.Interfaces.IApp theApp = new Models.SYS.clsApp();
            Models.Interfaces.IResultOk ok = null;
            theApp.Update(Info, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
            //Load lai caches
            AppCaches.clsCacheTheApp.Instance.Reload();

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        [PermissionsMenu(Permissions.View)]
        public ActionResult User()
        {
            ViewBag.Title = "Quản lý người dùng";
            return View();
        }

        [HttpGet]

        public string GetAll()
        {
            Models.clsUserBS _clsUser = new Models.clsUserBS();
            Entity.clsUserCollection Items = _clsUser.GetListUser();

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
        }

        [HttpGet]
        [PermissionsMenu(Permissions.View)]
        public string GetPermisionMenu()
        {
            if (HttpContext.User.Identity.Name.Equals("admin") ||
                HttpContext.User.Identity.Name.Equals("user"))
                return Newtonsoft.Json.JsonConvert.SerializeObject(new Entity.clsTreeMenuInfo("", true, true, true, true));

            Models.clsMenuMaster _clsMenu = new Models.clsMenuMaster();
            var MenuId = HttpUtility.ParseQueryString(HttpContext.Request.UrlReferrer.Query).GetValues("id").LastOrDefault();

            var UserPermission = _clsMenu.GetPermissionMenu(System.Web.HttpContext.Current.User.Identity.Name,
                        MenuId);

            return Newtonsoft.Json.JsonConvert.SerializeObject(UserPermission);
        }


        [HttpPost]
        
        public string SetPassword(string UserID,string Password,string RePassword)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserID)) return "UserID is null";
                if (string.IsNullOrWhiteSpace(Password)) return "Vui lòng nhập mật khẩu";
                if (UserID == "admin") return "Tài khoản này không cho thay đổi mật khẩu";
                if (string.IsNullOrWhiteSpace(Password)) Password = "";
                if (string.IsNullOrWhiteSpace(RePassword)) RePassword = "";
                if (!Password.Equals(RePassword))
                    return "Xác nhận mật khẩu không trùng khớp";

                Models.clsUserBS _clsUser = new Models.clsUserBS();
                if (_clsUser.SetPassword(UserID, Password, RePassword))
                {
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public string GetGroups()
        {
            Models.clsGroupBS _clsGroup = new Models.clsGroupBS();
            Entity.clsGroupCollection Items = _clsGroup.GetListOfGroup();
            if(Items!=null && Items.Count>0)
            {
                var fItems = Items.FindAll(f => f.GroupID.ToUpper() != "Administrator".ToUpper());
                Items = new Entity.clsGroupCollection(fItems);
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
        }


        [HttpGet]
        public string GetOuterGroup(string GroupID)
        {
            Models.clsUserBS _clsUser = new Models.clsUserBS();
            Entity.clsUserCollection Items = _clsUser.GetOuterGroup(GroupID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
        }

        [HttpGet]
        public string GetInnerGroup(string GroupID)
        {
            Models.clsUserBS _clsUser = new Models.clsUserBS();
            Entity.clsUserCollection Items = _clsUser.GetInnerGroup(GroupID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
        }

        [HttpPost]
        public string UpdateGroup([System.Web.Http.FromBody]JObject data)
        {

            try
            {
                Entity.clsTreeMenuCollection collection =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsTreeMenuCollection>(data["Json"].ToString());
                Entity.clsUserInfo userInfo =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsUserInfo>(data["UserInfo"].ToString());
                //var ks = userInfo;
                //var kq = collection.Count;
                if (string.IsNullOrEmpty(userInfo.UserID) || 
                    collection == null || collection.Count == 0) return "false";

                //System.Data.DataTable TYPE_SYS_USER_MENU = clsMenuMaster.TYPE_SYS_USER_MENU;
                //foreach (var item in collection)
                //{
                //    System.Data.DataRow newRow = TYPE_SYS_USER_MENU.NewRow();
                //    newRow["UserID"] = userInfo.UserID;
                //    newRow["MenuID"] = item.id;
                //    newRow["AllowView"] = item.AllowView;
                //    newRow["AllowAdd"] = item.AllowAdd;
                //    newRow["AllowEdit"] = item.AllowEdit;
                //    newRow["AlowDelete"] = item.AlowDelete;
                //    TYPE_SYS_USER_MENU.Rows.Add(newRow);

                //    addChildNode(ref TYPE_SYS_USER_MENU, item, userInfo.UserID);
                //}
                //bool result = clsMenuMaster.SetPermision(userInfo, TYPE_SYS_USER_MENU);
                //if (result) return Ok("true");
                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpDelete]
        [PermissionsMenu(Permissions.Delete)]
        public string DeleteUser(string Id)
        {
            Models.clsUserBS _clsUser = new Models.clsUserBS();
            try
            {
                System.Data.DataTable tbl_Log = Models.clsSysLog.LogSystem(Server.MachineName,
                    HttpContext.User.Identity.Name, "", "",
                    string.Format("Xóa tài khoản {0}", Id), "");
                if (_clsUser.Delete(Id, tbl_Log))
                    return "true";
                else
                    return "false";

                //return "";
            }
            catch (Exception ex)
            {
                return  "ERROR: " + ex.Message;
            }
        }

        [HttpDelete]
        [PermissionsMenu(Permissions.Delete)]
        public string DeleteGroup(string Id)
        {
            Models.clsUserBS _clsUser = new Models.clsUserBS();
            try
            {
                System.Data.DataTable tbl_Log = Models.clsSysLog.LogSystem(Server.MachineName,
                    HttpContext.User.Identity.Name, "", "",
                    string.Format("Xóa Nhóm {0}", Id), "");
                if (_clsUser.DeleteGroup(Id, tbl_Log))
                    return "true";
                else
                    return "false";

                //return "";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        
    }
}
