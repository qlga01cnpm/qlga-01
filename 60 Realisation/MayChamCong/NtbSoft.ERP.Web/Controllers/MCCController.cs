﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NtbSoft.Entities.Interfaces.MCC;
using System.IO;
using System.Globalization;

namespace NtbSoft.ERP.Web.Controllers
{
    public class MCCController : Controller
    {
        #region KHAI BÁO MÁY CHẤM CÔNG
        //
        // GET: /MCC/

        private List<Entity.Interfaces.ITerminalInfo> Items;
        public ActionResult Terminal()
        {
            ViewBag.Title = "Khai báo máy chấm công";
            //ViewBag.UserCode = "";
            //ViewBag.Text = "Đăng ký";
            //ViewBag.IsActived = false;
            //ViewBag.ActiveKey = "";
            //try
            //{
            //    ERP.RegMCC.clsReg cls = new RegMCC.clsReg("");

            //}
            //catch { }
            return View();
        }

        [HttpGet]
        public string GetTerminal()
        {
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal =
                new Models.clsTerminal();
            Exception _ex = null;

            _Terminal.GetAll((items, ex) =>
            {
                if (ex == null) Items = items.ToList();
                else
                    _ex = ex;
            });

            if (_ex != null) return "ERROR: " + _ex.Message;
            if (Items != null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(Items, new Newtonsoft.Json.JsonSerializerSettings()
                {
                    TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects //Có sử dụng Interface
                });

            return "";
        }

        [HttpPost]
        public string TerminalUpdate(Entity.clsTerminalInfo Item)
        {
            Models.Interfaces.IResultOk _result = new Models.BaseResultOk();
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            _Terminal.Update(Item, ex =>
            {
                _result = ex;
            });
            if (!_result.Success) return "ERROR: " + _result.Message;

            return "OK";
        }

        [HttpPost]
        public string TerminalInsert(Entity.clsTerminalInfo Item)
        {
            Models.Interfaces.IResultOk _result = new Models.BaseResultOk();
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            _Terminal.Insert(Item, ok =>
            {
                _result = ok;
            });
            if (!_result.Success) return "ERROR: " + _result.Message;

            return "OK";
        }

        [HttpPost]
        public string TerminalDelete(int Id)
        {
            Models.Interfaces.IResultOk _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            _Terminal.DeleteByKey(Id, ex =>
            {
                _ex = ex;
            });
            if (_ex != null && !_ex.Success) return "ERROR: " + _ex.Message;

            return "OK";
        }

        [HttpPost]
        public string TerminalTestConnect(int Id)
        {
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(Id, (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });

            if (_ex != null) return "ERROR: " + _ex.Message;
            if (Item != null)
            {
                //Devices.MCC.clsTheApp _theApp = new Devices.MCC.clsTheApp();
                //_theApp.TestConnectNet(Item.IpAddress, Item.Port, (result, ex) =>
                //{
                //    Item.Status = result;
                //    _ex = ex;
                //});
                //if (_ex != null) return "ERROR: " + _ex.Message;

                NtbSoft.Entities.Interfaces.MCC.IDeviceInfo DeviceInfo = null;
                Devices.MCC.Services.Interfaces.IDeviceManager _DeviceManager = null;
                if (Item.CommType == 1)
                {
                    //_DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectCOM());
                }
                else if (Item.CommType == 2)
                {
                    _DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    _DeviceManager.Connect((result, ex) =>
                    {
                        if (ex == null && result) _DeviceManager.GetDeviceInfo((item, ex1) => { DeviceInfo = item; });
                    });
                }
                if (DeviceInfo != null)
                {
                    Item.SerialNumber = DeviceInfo.SerialNumber;
                    Item.Status = true;
                }
                else
                    Item.Status = false;

                _Terminal.Update(Item, ex => { });
                if (Item.Status) return "OK";
                else return "Fail";
            }

            return "Fail";
        }

        [HttpGet]
        public string TerminalGetInfo(int Id)
        {
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(Id, (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });

            if (_ex != null) return "ERROR: " + _ex.Message;
            if (Item != null)
            {
                NtbSoft.Entities.Interfaces.MCC.IDeviceInfo DeviceInfo = null;
                Devices.MCC.Services.Interfaces.IDeviceManager _DeviceManager = null;
                if (Item.CommType == 1)
                {
                    //_DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectCOM());
                }
                else if (Item.CommType == 2)
                {
                    _DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    _DeviceManager.Connect((result, ex) =>
                    {
                        if (ex == null && result) _DeviceManager.GetDeviceInfo((item, ex1) => { DeviceInfo = item; });
                    });
                }
                if (DeviceInfo != null)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(DeviceInfo,
                        new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects });
                }
            }
            return "";
        }

        public JsonResult DangKyBanQuyen(int Id)
        {
            ViewBag.UserCode = "";
            ViewBag.Text = "Đăng ký";
            ViewBag.IsActived = false;
            ViewBag.ActiveKey = "";
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(Id, (result, ex) => { _ex = ex; Item = result; });

            if (_ex != null) return Json("ERROR: " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (Item != null)
            {
                NtbSoft.Entities.Interfaces.MCC.IDeviceInfo DeviceInfo = null;
                Devices.MCC.Services.Interfaces.IDeviceManager _DeviceManager = null;
                if (Item.CommType == 1)
                {
                    //_DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectCOM());
                }
                else if (Item.CommType == 2)
                {
                    _DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    _DeviceManager.Connect((result, ex) =>
                    {
                        if (ex == null && result) _DeviceManager.GetDeviceInfo((item, ex1) => { DeviceInfo = item; });
                    });
                }
                if (DeviceInfo != null)
                {
                    Item.SerialNumber = DeviceInfo.SerialNumber;
                    ERP.RegMCC.clsReg clsDangKy = new RegMCC.clsReg(Item.SerialNumber);
                    Item.UserCode = clsDangKy.GenerateKey();
                    Item.IsActived = clsDangKy.IsKeyOK(Item.ActiveKey);

                    return Json(Item, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Đăng ký bản quyền
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public JsonResult TerminalActive(string item)
        {
            var NewItem = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsTerminalInfo>(item);
            Models.Interfaces.IResultOk _result = new Models.BaseResultOk();
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            /*** Kiểm tra đăng ký ***/
            ERP.RegMCC.clsReg clsDangKy = new RegMCC.clsReg(NewItem.SerialNumber);
            if (clsDangKy.IsKeyOK(NewItem.ActiveKey))
            {
                Entity.Interfaces.ITerminalInfo Item = null;
                _Terminal.GetBy(NewItem.TerminalID, (result, ex) => { Item = result; });
                Item.ActiveKey = NewItem.ActiveKey;
                Item.SerialNumber = NewItem.SerialNumber;

                _Terminal.Update(Item, ex => { _result = ex; });
            }
            else
                return Json("ERROR: Activation Key không đúng. Vui lòng liên hệ với nhà cung cấp để được hỗ trợ.", JsonRequestBehavior.AllowGet);

            if (!_result.Success) return Json("ERROR: " + _result.Message, JsonRequestBehavior.AllowGet);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// đồng bộ giờ máy chấm công
        /// </summary>
        /// <returns></returns>
        public JsonResult TerminalSynzTime(string id)
        {
            Devices.MCC.Services.Interfaces.IAttLogs _attLogs;
            string messageErr = string.Empty;
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(int.Parse(id), (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });

            if (_ex != null) return Json("ERROR: " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (Item != null)
            {
                if (Item.CommType == 1) { }
                else if (Item.CommType == 2)
                {
                    _attLogs = new Devices.MCC.Services.AttLogs(int.Parse(id), Item.LastRecordDate, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    // kiểm tra kết nối
                    _attLogs.Connect((connected, ex) =>
                    {
                        if (connected)
                        {
                            // kết nối thành công - thì đồng bộ giờ
                            _attLogs.SynzTimeMachine((suc, error) =>
                            {
                                if (error != null)
                                {
                                    messageErr += error;
                                }
                            });
                        }
                        else
                        {
                            messageErr += string.Format(@"{0} không kết nối được", Item.Name);
                        }
                    });
                }
            }

            if (messageErr != string.Empty)
            {
                return Json(messageErr, JsonRequestBehavior.AllowGet);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region XEM LOG CHÂM CÔNG
        public ActionResult ViewAttLogs()
        {
            ViewBag.Title = "Xem log chấm công";
            return View();
        }

        [HttpGet]
        public string AttLogsViewData(int Id, string date, string maCC)
        {
            Models.clsSchedule _cls = new Models.clsSchedule();

            _cls.Update(Libs.ApiAction.POST, new Entity.clsScheduleInfo() { SchName = "LT1" }, ex => { });

            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(Id, (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });

            if (_ex != null) return "ERROR: " + _ex.Message;
            if (Item != null)
            {
                Devices.MCC.Services.Interfaces.IAttLogs _attLogs;
                DateTime dateSearch = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (Item.CommType == 1) { }
                else if (Item.CommType == 2)
                {
                    _attLogs = new Devices.MCC.Services.AttLogs(Id, dateSearch, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    List<ICardUserInfo> _listUserLogs = null;

                    _attLogs.Connect((connected, ex) =>
                    {
                        if (connected)
                        {
                            _attLogs.DownloadAttLogs((result, ex1) =>
                            {
                                if (result != null)
                                {
                                    _listUserLogs = result.Select(i =>
                                    {
                                        if (!string.IsNullOrWhiteSpace(i.UserId))
                                            i.MachineNumber = Id;
                                        return i;
                                    }).ToList();
                                }
                            });
                        }
                    });

                    if (_listUserLogs != null && _listUserLogs.Count > 0)
                    {
                        if (maCC != null && maCC.Trim().ToString().Length > 0)
                        {
                            _listUserLogs = _listUserLogs.Where(d => d.UserId == maCC || d.UserName.ToLower().Contains(maCC.ToLower())).ToList();
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(_listUserLogs,
                        new Newtonsoft.Json.JsonSerializerSettings()
                        {
                            TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects  //Co su dung interface
                        });
                    }
                }
            }

            return "";
        }

        #endregion

        #region KHAI BÁO CA LÀM VIỆC
        public ActionResult Shifts()
        {
            ViewBag.Title = "Khai báo ca làm việc";
            return View();
        }

        [HttpGet]
        public JsonResult ShiftGetAll()
        {
            Models.Interfaces.IShift shift = new Models.clsShift();
            List<Entity.clsShiftInfo> Items = new List<Entity.clsShiftInfo>();
            Models.Interfaces.IResultOk ok = null;
            shift.GetAll((result, Ex) =>
            {
                Items = result;
                ok = Ex;
            });
            if (ok.Success)
            {
                //return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
                return Json(Items, JsonRequestBehavior.AllowGet);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ShiftGetAll2()
        {

            Models.Interfaces.IShift shift = new Models.clsShift();
            JData.DataComboBoxInfo[] array = null;
            Models.Interfaces.IResultOk ok = null;
            shift.GetAll((result, Ex) =>
            {
                array = result;
                ok = Ex;
            });
            if (ok.Success)
            {
                return Json(array, JsonRequestBehavior.AllowGet);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShiftSave(Entity.clsShiftInfo Item)
        {
            Models.Interfaces.IShift shift = new Models.clsShift();
            Models.Interfaces.IResultOk ok = null;
            shift.Update(Item, Ex =>
            {
                ok = Ex;
            });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShiftDelete(int Id)
        {
            Models.Interfaces.IShift shift = new Models.clsShift();
            shift.Delete(Id, Ex =>
            {

            });
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ShiftCalculatorTime(string TimeIn, string BreakOut, string BreakIn, string TimeOut)
        {
            TimeSpan tIn, bOut, bIn, tOut;
            if (!TimeSpan.TryParse(TimeIn, out tIn)) return Json("", JsonRequestBehavior.AllowGet);
            if (!TimeSpan.TryParse(BreakOut, out bOut)) return Json("", JsonRequestBehavior.AllowGet);
            if (!TimeSpan.TryParse(BreakIn, out bIn)) return Json("", JsonRequestBehavior.AllowGet);
            if (!TimeSpan.TryParse(TimeOut, out tOut)) return Json("", JsonRequestBehavior.AllowGet);

            return Json(((bOut - tIn) + (tOut - bIn)), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region KHAI BÁO LỊCH TRÌNH CA
        public ActionResult Schedule()
        {
            ViewBag.Title = "Lịch trình ca làm việc";
            return View();
        }

        [HttpGet]
        public JsonResult ScheduleGetAll()
        {
            Models.Interfaces.ISchedule schedule = new Models.clsSchedule();
            List<Entity.clsScheduleInfo> Items = new List<Entity.clsScheduleInfo>();
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            schedule.GetAll((result, Ex) => { Items = result; ok = Ex; });
            if (!ok.Success)
                return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ScheduleGetAll2()
        {
            Models.clsSchedule schedule = new Models.clsSchedule();
            JData.DataComboBoxInfo[] array = null;
            Models.Interfaces.IResultOk ok = null;
            schedule.GetAll((result, Ex) =>
            {
                array = result;
                ok = Ex;
            });

            if (!ok.Success)
                return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(array, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ScheduleUpdate(string Action, string Item)
        {
            var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsScheduleInfo>(Item);
            Models.Interfaces.ISchedule schedule = new Models.clsSchedule();
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            if (Action == "PUT")
                schedule.Update(Libs.ApiAction.PUT, item, Ex => { ok = Ex; });
            else if (Action == "POST")
                schedule.Update(Libs.ApiAction.POST, item, Ex => { ok = Ex; });
            if (!ok.Success)
                return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ScheduleDelete(int id)
        {
            Models.Interfaces.ISchedule schedule = new Models.clsSchedule();
            Models.Interfaces.IResultOk ok = new Models.BaseResultOk();
            schedule.Delete(id, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ScheduleShiftGetBy(int Id)
        {
            Models.Interfaces.IScheduleSchift scheduleShift = new Models.clsScheduleShift();
            List<Entity.clsScheduleShiftInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            scheduleShift.GetBy(Id, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success)
                return Json("ERROR: " + ok.Message);
            if (Items == null) return Json("NULL", JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ScheduleShiftUpdate(int Id, string Item)
        {
            var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsScheduleShiftInfo>>(Item);
            Models.Interfaces.IScheduleSchift scheduleShift = new Models.clsScheduleShift();
            Models.Interfaces.IResultOk ok = null;
            scheduleShift.BatchUpdate(Id, Items, ex => { ok = ex; });
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SchduleShiftEmployee(string shiftId, string schid, string items)
        {
            List<Entity.Interfaces.INhanVienInfo> collection = new List<Entity.Interfaces.INhanVienInfo>();
            collection.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsNhanVienInfo>>(items));
            Models.Interfaces.ICapNhatCa CapNhatCa = new Models.clsCapNhatCa();
            Models.Interfaces.IResultOk ok = null;

            CapNhatCa.CapNhatCaLichTrinh(shiftId, schid, collection, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SẮP LỊCH LÀM VIỆC
        public ActionResult ScheduleShift()
        {
            ViewBag.Title = "Sắp lịch làm việc";
            return View();
        }

        [HttpGet]
        public string SearchSchduleShiftEmp(int selectedPage, int pageSize, string maNV, string schedule)
        {
            //Exception _ex = null;
            //int _totalPages = 0, _totalRecords = 0;
            //Models.clsNhanVien _nhanvien = new Models.clsNhanVien();
            //List<Entity.Interfaces.INhanVienInfo> Items = new List<Entity.Interfaces.INhanVienInfo>();
            //_nhanvien.GetByDepTenNV(selectedPage, pageSize, DepId, TenNV, out _totalPages, out _totalRecords, (result, ex) =>
            //{
            //    Items = result.ToList();
            //    _ex = ex;
            //});

            ////var kq = Items.FindAll(f => !f.IsNghiViec);

            //Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            //_PagingInfo.Tag = Items;
            ////_PagingInfo.Tag = new List<Entity.Interfaces.INhanVienInfo>(kq);
            //_PagingInfo.TotalRecords = _totalRecords;
            //_PagingInfo.TotalPages = _totalPages;
            //if (_ex == null)
            //    return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            //return "ERROR: " + _ex.Message;
            
            return "";
        }
        #endregion

        #region QUY TẮC CHẤM CÔNG
        public ActionResult QuyTacChamCong()
        {
            ViewBag.Title = "Quy tắc chấm công";

            return View();
        }

        [HttpGet]
        public string QuyTacChamCongGet()
        {
            Models.Interfaces.IQuyTacLamTron QuyTacLamTron = new Models.clsQuyTacLamTron();
            Models.Interfaces.IQuyTac QuyTac = new Models.clsQuyTac(QuyTacLamTron);
            Models.Interfaces.IResultOk result = new Models.BaseResultOk();
            Entity.Interfaces.IQuyTacInfo Item = null;
            QuyTac.GetFirst((item, complete) =>
            {
                Item = item;
                result = complete;

            });

            if (result.Success)
                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            else
                return "ERROR: " + result.Message;

        }

        [HttpPost]
        public ActionResult QuyTacChamCongSave(Entity.clsQuyTacInfo Item)
        {

            Models.clsQuyTacLamTron quytaclamtron = new Models.clsQuyTacLamTron();
            Models.Interfaces.IQuyTac QuyTac = new Models.clsQuyTac(quytaclamtron);
            quytaclamtron.BatchUpdate(Item.QuyTacLamTron, ex =>
            {

            });

            QuyTac.SetSoPhut(Item.SoPhutGiuaHaiLan, Ex => { });
            return Json("OK");
        }

        [HttpPost]
        public ActionResult QuyTacChamCongDelete(int Id)
        {
            Models.clsQuyTacLamTron quytaclamtron = new Models.clsQuyTacLamTron();
            quytaclamtron.Delete(Id, ex =>
            {

            });
            return Json("OK");
        }
        #endregion

        #region TẢI DỮ LIỆU CHẤM CÔNG
        public ActionResult DownLoadAtt()
        {
            ViewBag.Title = "Tải dữ liệu máy chấm công";
            ViewBag.Message = "DD";
            return View();
        }

        [HttpGet]
        //public string ReadGeneralLogData(List<Entity.Interfaces.ITerminalInfo> Items)
        public string ReadGeneralLogData(int Id)
        {
            Exception _ex = null;
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo Item = null;
            _Terminal.GetBy(Id, (result, ex) =>
            {
                _ex = ex;
                Item = result;
            });

            if (_ex != null) return "ERROR: " + _ex.Message;
            if (Item != null)
            {
                Services.RegisterMCC chkReg = new Services.RegisterMCC(Item);
                if (!chkReg.IsKeyOK())
                    return "ERROR: " + Item.Name + " chưa được đăng ký. Vui lòng liên hệ với nhà cung cấp phần mềm, để được hỗ trợ";
                Devices.MCC.Services.Interfaces.IAttLogs _attLogs;
                if (Item.CommType == 1) { }
                else if (Item.CommType == 2)
                {
                    _attLogs = new Devices.MCC.Services.AttLogs(Id, Item.LastRecordDate, new Devices.MCC.Services.ConnectNet(Item.IpAddress, Item.Port));
                    List<ICardUserInfo> _listUserLogs = null;

                    _attLogs.Connect((connected, ex) =>
                   {
                       if (connected)
                       {
                           _attLogs.DownloadAttLogs((result, ex1) =>
                            {
                                if (result != null)
                                {
                                    _listUserLogs = result.Select(i =>
                                    {
                                        if (!string.IsNullOrWhiteSpace(i.UserId)) i.MachineNumber = Id;
                                        return i;
                                    }).ToList();
                                }
                            });
                       }
                   });

                    if (_listUserLogs != null && _listUserLogs.Count > 0)
                    {
                        //Cap nhat du lieu that
                        NtbSoft.ERP.Models.Interfaces.IQuyTacLamTron QuyTacLamTron = new NtbSoft.ERP.Models.clsQuyTacLamTron();
                        NtbSoft.ERP.Models.Interfaces.IQuyTac QuyTacCC = new NtbSoft.ERP.Models.clsQuyTac(QuyTacLamTron);
                        Models.DIC.Interfaces.IDangKyCheDo _DangKyCheDo = new NtbSoft.ERP.Models.DIC.clsDangKyCheDo();
                        Models.Interfaces.INhanVien _nhanvien = new NtbSoft.ERP.Models.clsNhanVienCC();

                        Models.Interfaces.IShift _clsShifts = new Models.clsShift();
                        //Lịch trình ca
                        Models.Interfaces.IScheduleSchift _ScheduleShift = new Models.clsScheduleShift();

                        NtbSoft.ERP.Models.Interfaces.IBase<ICardUserInfo> _baseBus =
                            new ERP.Models.clsAttLogs(_nhanvien, QuyTacCC, _DangKyCheDo, _clsShifts, _ScheduleShift);
                        _baseBus.BatchInsert(_listUserLogs, ex => { });

                        return Newtonsoft.Json.JsonConvert.SerializeObject(_listUserLogs,
                            new Newtonsoft.Json.JsonSerializerSettings()
                            {
                                TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects  //Co su dung interface
                            });

                        //Cap nhat du lieu ao

                    }

                    /*** Xử lý dữ liệu ảo ***/
                    //- So sánh dữ liệu bên bảng MCC_ATTLOGS_SESSION và bảng MCC_ATTLOGS_SESSION_VIRTUAL
                    //- Nếu trùng nhau thì cập nhật, nếu không trùng nhau thì thêm mới bên bảng MCC_ATTLOGS_SESSION_VIRTUAL
                }
            }

            return "";
        }

        //public System.Data.DataTable ListToDataTable<T>(this IList<T> data)
        //{
        //    System.Data.DataTable dataTable = new System.Data.DataTable(typeof(T).Name);
        //    System.Reflection.PropertyInfo[] props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        //    foreach (System.Reflection.PropertyInfo prop in props)
        //    {
        //        dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //    }
        //    foreach (T item in data)
        //    {
        //        var values = new object[props.Length];
        //        for (int i = 0; i < props.Length; i++)
        //        {
        //            values[i] = props[i].GetValue(item, null);
        //        }
        //        dataTable.Rows.Add(values);
        //    }
        //    return dataTable;
        //}
        #endregion

        #region TẢI NHÂN VIÊN LÊN MÁY CHẤM CÔNG
        public ActionResult UploadFPTmp()
        {
            ViewBag.Title = "Tải nhân viên lên máy chấm công";
            return View();
        }

        public JsonResult BeginUploadFPTmp(int id, int Privilege, string Item)
        {
            Exception _ex = null;
            var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsVanTayNhanVienInfo>>(Item);

            //Lấy thông tin của nhân viên gồm MaCC,Tên, và mã vân tay của 10 ngón tay
            //Chưa xử ly xong 
            string ArrayMaCC = string.Join(",", Items.Select(x => x.UserId));
            List<Entities.Interfaces.MCC.IUserFingerprintInfo> ItemCards = null;
            Models.clsVanTayNhanVien vantay = new Models.clsVanTayNhanVien();
            //Gan quyen quan tri
            vantay.UpdatePrivilege(ArrayMaCC, Privilege, ex => { });

            vantay.GetDataVanTay(ArrayMaCC, (result, ex) =>
            {
                ItemCards = result;
            });

            //string kk = joined;
            //string dss = kk;
            //return Json("OK", JsonRequestBehavior.AllowGet);
            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) =>
            {
                _ex = ex;
                TerminalInfo = result;
            });
            if (_ex != null) return Json("ERROR: " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (Item != null)
            {
                NtbSoft.Devices.MCC.Services.Interfaces.IUserManagement cardUser = null;
                //Kiểm tra loại kết nối của máy chấm công là gì 
                //1: là kết nối cổng com
                //2: là kết nối mạng internet tcp/ip
                //3: là kêt nối RF232
                if (TerminalInfo.CommType == 1)
                {
                    //_DeviceManager = new Devices.MCC.Services.clsDeviceManager(1, new Devices.MCC.Services.ConnectCOM());
                }
                else if (TerminalInfo.CommType == 2)
                {
                    cardUser = new NtbSoft.Devices.MCC.Services.UserManagement(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                    cardUser.Connect((result, ex) =>
                    {
                        if (ex == null && result)
                        {
                            cardUser.UploadFPTmp(ItemCards, ex1 =>
                            {
                                _ex = ex1;
                            });
                        }
                    });
                }
                if (_ex != null) return Json("ERROR: " + _ex.Message, JsonRequestBehavior.AllowGet);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region NGƯỜI DÙNG TRÊN MÁY CHẤM CÔNG - TẢI DỮ LIỆU VỀ MÁY TÍNH (PC)
        public ActionResult CardManagements()
        {
            ViewBag.Title = "Người dùng trên máy chấm công";
            return View();
        }

        public JsonResult CardManagementGet(int id)
        {
            Exception _ex = null;
            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) => { _ex = ex; TerminalInfo = result; });

            //Kiểm tra có lỗi hay không
            if (_ex != null) return Json("ERROR: Load MCC " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (TerminalInfo == null) return Json("ERROR: NULL ", JsonRequestBehavior.AllowGet);

            NtbSoft.Devices.MCC.Services.Interfaces.ICardManagement cardManager = null;
            List<Entities.Interfaces.MCC.IUserFingerInfo> ItemCards = null;
            //Kiểm tra loại kết nối của máy chấm công là gì 
            //1: là kết nối cổng com
            //2: là kết nối mạng internet tcp/ip
            //3: là kêt nối RF232
            if (TerminalInfo.CommType == 1) { }
            else if (TerminalInfo.CommType == 2)
            {
                cardManager = new NtbSoft.Devices.MCC.Services.CardManagement(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                cardManager.Connect((result, ex) =>
                {
                    if (ex == null && result)
                        cardManager.GetStrCardNumber((result1, ex1) => { ItemCards = result1; _ex = ex1; });

                });
            }

            return Json(ItemCards, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Tải dữ liệu từ máy chấm công về máy tính
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Item"></param>
        /// <returns></returns>
        [PermissionsMenu(NtbSoft.ERP.Libs.Permissions.Add)]
        public JsonResult DownloadToPC(int id, string Item)
        {
            Exception _ex = null;
            var UserFingers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsVanTayNhanVienInfo>>(Item);

            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) => { _ex = ex; TerminalInfo = result; });

            if (_ex != null) return Json("ERROR: Load MCC " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (TerminalInfo == null) return Json("ERROR: NULL ", JsonRequestBehavior.AllowGet);

            Devices.MCC.Services.Interfaces.IUserManagement fingerUser = null;
            List<Entities.Interfaces.MCC.IUserFingerprintInfo> UserFingerprints = null;
            /*** Kiểm tra loại kết nối của máy chấm công ***/
            //1: là kết nối cổng com
            //2: là kết nối mạng internet tcp/ip
            //3: là kêt nối RF232
            if (TerminalInfo.CommType == 1) { }
            else if (TerminalInfo.CommType == 2)
            {
                fingerUser = new NtbSoft.Devices.MCC.Services.UserManagement(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                fingerUser.Connect((result, ex) =>
                {
                    if (ex == null && result)
                    {
                        fingerUser.DownloadFPTmp((result1, ex1) =>
                        {
                            UserFingerprints = result1;
                            _ex = ex1;
                        });
                    }
                });
            }

            var listTemps = UserFingerprints.Where(p => UserFingers.Any(p2 => p2.UserId == p.UserId)).ToList();
            /*** Lưu vào CSDL ***/
            Models.clsVanTayNhanVien VanTay = new Models.clsVanTayNhanVien();
            Models.Interfaces.IResultOk ok = null;
            VanTay.UpdateVanTay(listTemps, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            //new Devices.MCC.Services.CardUser()
            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        [PermissionsMenu(NtbSoft.ERP.Libs.Permissions.Delete)]
        public JsonResult CardManagementDelete(int id, string Item)
        {
            Exception _ex = null;

            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) => { _ex = ex; TerminalInfo = result; });

            if (_ex != null) return Json("ERROR: Load MCC " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (TerminalInfo == null) return Json("ERROR: NULL ", JsonRequestBehavior.AllowGet);

            Devices.MCC.Services.Interfaces.ICardManagement card = null;
            List<Entities.Interfaces.MCC.IUserFingerInfo> UserFingers = new List<IUserFingerInfo>();
            UserFingers.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsVanTayNhanVienInfo>>(Item));
            /*** Kiểm tra loại kết nối của máy chấm công ***/
            //1: là kết nối cổng com
            //2: là kết nối mạng internet tcp/ip
            //3: là kêt nối RF232
            if (TerminalInfo.CommType == 1) { }
            else if (TerminalInfo.CommType == 2)
            {
                card = new NtbSoft.Devices.MCC.Services.CardManagement(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                card.Connect((result, ex) =>
                {
                    if (ex == null && result)
                    {
                        card.DeleteEnrollData(UserFingers, ex1 => { _ex = ex1; });
                    }
                });
            }
            if (_ex != null) return Json("ERROR: " + _ex.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUser(string maCC, string mayCC)
        {
            Exception _ex = null;
            int id = int.Parse(mayCC);

            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) => { _ex = ex; TerminalInfo = result; });

            //Kiểm tra có lỗi hay không
            if (_ex != null) return Json("ERROR: Load MCC " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (TerminalInfo == null) return Json("ERROR: NULL ", JsonRequestBehavior.AllowGet);

            NtbSoft.Devices.MCC.Services.Interfaces.ICardManagement cardManager = null;
            List<Entities.Interfaces.MCC.IUserFingerInfo> ItemCards = null;
            //Kiểm tra loại kết nối của máy chấm công là gì 
            //1: là kết nối cổng com
            //2: là kết nối mạng internet tcp/ip
            //3: là kêt nối RF232
            if (TerminalInfo.CommType == 1) { }
            else if (TerminalInfo.CommType == 2)
            {
                cardManager = new NtbSoft.Devices.MCC.Services.CardManagement(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                cardManager.Connect((result, ex) =>
                {
                    if (ex == null && result)
                        cardManager.GetStrCardNumber((result1, ex1) => { ItemCards = result1; _ex = ex1; });
                });
            }
            if (maCC != null && maCC.ToString().Trim().Length > 0)
            {
                ItemCards = ItemCards.Where(d => d.UserId == maCC || d.UserName.ToLower().Contains(maCC.ToLower())).ToList();
            }

            return Json(ItemCards, JsonRequestBehavior.AllowGet);
        }

        #endregion
         
        #region NHẬT KÝ CHẤM CÔNG
        public ActionResult TimeAttLog()
        {
            ViewBag.Title = "Nhật ký chấm công";
            return View();
        }

        [HttpGet]
        public string NhatKyCCGetBy(string FromDate, string ToDate, int SelectedPage, int pageSize)
        {
            int _totalPages = 0, _totalRecords = 0;

            Models.clsNhatKyChamCong _NhaKyCC = new Models.clsNhatKyChamCong();
            List<Entity.Interfaces.INhatKyChamCongInfo> Items = new List<Entity.Interfaces.INhatKyChamCongInfo>();
            Exception _ex = null;
            DateTime dtFrom = Libs.clsCommon.ParseExactDMY(FromDate), dtTo = Libs.clsCommon.ParseExactDMY(ToDate);
            //DateTime.TryParse(FromDate, out dtFrom);
            //DateTime.TryParse(ToDate, out dtTo);
            _NhaKyCC.GetPaging(dtFrom, dtTo, SelectedPage, pageSize, out _totalPages, out _totalRecords, (result, ex) =>
            {
                Items = result.ToList(); _ex = ex;
            });
            if (_ex != null) return "ERROR: " + _ex.Message;

            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = Items;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;

            return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo, new Newtonsoft.Json.JsonSerializerSettings()
            {
                //TypeNameHandling= Newtonsoft.Json.TypeNameHandling.Objects  //Có sử dụng Interface
            });

        }

        #endregion

        #region CHUẨN HÓA DỮ LIỆU
        public ActionResult ChuanHoaDuLieu()
        {
            ViewBag.Title = "Chuẩn hóa dữ liệu";
            return View();
        }

        public JsonResult GenerateVirtualData(bool AllowUpdated, int Month, int Year, string arrayCN, string items)
        {
            var arr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(arrayCN);
            List<Entity.Interfaces.INhanVienInfo> Temps = new List<Entity.Interfaces.INhanVienInfo>(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsNhanVienInfo>>(items));

            //string ArrayMaCC = string.Join(",", Itemss.Select(x => x.MaCC));
            Models.MCC.Interfaces.IAttLogsVirtual attLog = new Models.MCC.clsAttLogsVirtual(new Models.DIC.clsDangKyVang(),
                                                                                           new Models.DIC.clsDangKyDiMuonVeSom(),
                                                                                           new Models.clsScheduleShift(),
                                                                                           new Models.clsShift());
            Models.Interfaces.IResultOk ok = null;
            attLog.GenerateVirtualTime(AllowUpdated, Month, Year, arr, Temps, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            if (AllowUpdated) return Json(string.Format("{0} dòng đã được cập nhật", ok.RowsAffected), JsonRequestBehavior.AllowGet);

            return Json(string.Format("{0} dòng đã được tạo", ok.RowsAffected), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteVirtualData(int Month, int Year, string items)
        {

            List<Entity.Interfaces.INhanVienInfo> Temps = new List<Entity.Interfaces.INhanVienInfo>(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsNhanVienInfo>>(items));
            Models.MCC.Interfaces.IAttLogsVirtual attLog = new Models.MCC.clsAttLogsVirtual(new Models.DIC.clsDangKyVang(),
                                                                                           new Models.DIC.clsDangKyDiMuonVeSom(),
                                                                                           new Models.clsScheduleShift(),
                                                                                           new Models.clsShift());
            int count = 0;
            Models.Interfaces.IResultOk ok = null;
            foreach (var item in Temps)
            {
                attLog.Delete(Month, Year, item.MaNV, (ex => { ok = ex; }));
                if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
                count += ok.RowsAffected > -1 ? ok.RowsAffected : 0;
            }
            return Json(string.Format("{0} dòng đã được xóa", count), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region XỬ LÝ DỮ LIỆU CHẤM CÔNG
        public ActionResult XuLyChamCong()
        {
            ViewBag.Title = "Xử lý dữ liệu chấm công";
            return View();
        }

        [HttpGet]
        public string XuLyChamCongGetPaging(int PageIndex, int PageSize, string manv, string DepId, 
            string FromDate, string ToDate, int SelectedPage, int pageSize)
        {
            int _totalPages = 0, _totalRecords = 0;

            Models.clsXuLyChamCong _NhaKyCC =
                new Models.clsXuLyChamCong();
            IEnumerable<Entity.Interfaces.IXuLyChamCongInfo> Items = new List<Entity.Interfaces.IXuLyChamCongInfo>();
            Models.Interfaces.IResultOk ok = null;
            DateTime dtFrom = Libs.clsCommon.ParseExactDMY(FromDate), dtTo = Libs.clsCommon.ParseExactDMY(ToDate);
         
            _NhaKyCC.GetPaging(PageIndex, pageSize, DepId, manv, dtFrom, dtTo, 
                (result, ex, RecodeCount) => { Items = result; ok = ex; _totalRecords = RecodeCount; });
             
            if (!ok.Success) return "ERROR: " + ok.Message;

            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = Items.ToList();
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;

            return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo, new Newtonsoft.Json.JsonSerializerSettings()
            { 
            });
        }
         
        [HttpPost]
        public string XuLyChamCongX2(List<Entity.clsXuLyChamCongInfo> items)
        {
            if (items == null) items = new List<Entity.clsXuLyChamCongInfo>();
            var Temps = items.GroupBy(x => new { x.MaCC, x.StrWorkingDayMDY })
              .OrderBy(o => o.Key.StrWorkingDayMDY).ToList();
            Models.Interfaces.IQuyTacLamTron QuyTacLamTron = new Models.clsQuyTacLamTron();
            Models.Interfaces.IQuyTac QuyTac = new Models.clsQuyTac(QuyTacLamTron);

            List<Entity.Interfaces.IDangKyChanGioInfo> mDangKyChanGioCollection = new List<Entity.Interfaces.IDangKyChanGioInfo>();
            Models.DIC.Interfaces.IDangKyChanGio mDangKyChanGio = new Models.DIC.clsDangKyChanGio();
            if (items.Count > 0)
            {
                //if (items[0].MaCC == "432")
                //{

                //}
                mDangKyChanGio.GetBy(items[0].WorkingDay.Month, items[0].WorkingDay.Year, "DEP.ROOT", "",
                    (result, complete) => { mDangKyChanGioCollection = result; });
            }
            // 
            Models.Interfaces.IXuLyDuLieuTai _xuly;
            //_xuly = new Models.clsXuLyDuLieuTaiThat(new Models.clsShift(), QuyTac);

            _xuly = new Models.clsXuLyDuLieuTaiThatV1(new Models.clsShift(), QuyTac,
                mDangKyChanGioCollection);
            //if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
            //    _xuly = new Models.clsXuLyDuLieuTaiAo(new Models.clsShift(),QuyTac);

            try
            {
                DateTime tmp = Libs.clsCommon.LastDayOfMonth(items[0].WorkingDay.Month, items[0].WorkingDay.Year);
                _xuly.InitializeDate(items[0].WorkingDay.Month, tmp.Day, items[0].WorkingDay.Year, ex => { });
            }
            catch (Exception e)
            {
            }

            List<Entity.clsXuLyChamCongInfo> temp2 = new List<Entity.clsXuLyChamCongInfo>();
            int count = 0;
            foreach (var item in Temps)
            {
                count++;
                //if (count == 195)
                //{

                //}
                DateTime dt;
                if (!DateTime.TryParse(item.Key.StrWorkingDayMDY, out dt)) continue;
                
                _xuly.XuLyDuLieuChamCong(item.Key.MaCC, dt, (result, error) =>
                {
                    var mAbc = result;
                });
                Libs.clsCommon.WriteLogAspNet("MACC", item.Key.MaCC.ToString() + "  : count " + count.ToString());
            }


            Libs.clsCommon.WriteLogAspNet("Count", Temps.Count.ToString());
            return "OK";
        }

        public JsonResult TimeInOutUdate(string Action, int Id, string MaCC, string workingday, string gio)
        {
            Models.Interfaces.ITimeInOutSession TimeInOut = null;
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
            {
                TimeInOut = new Models.clsTimeInOutSessionVirtual();
            }
            else
            {
                TimeInOut = new Models.clsTimeInOutSession();
            }

            Models.Interfaces.IResultOk ok = null;
            DateTime day = Libs.clsCommon.ParseExactDMY(workingday);
            //DateTime.TryParse(workingday,out day);
            if (Action == "PUT") //Cap nhat
                TimeInOut.Update(Libs.ApiAction.PUT, Id, day, Convert.ToInt32(MaCC), gio, ex => { ok = ex; });
            else if (Action == "POST") //Them moi
                TimeInOut.Update(Libs.ApiAction.POST, Id, day, Convert.ToInt32(MaCC), gio, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChamCongRemove(string action, string form)
        {
            var Temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsXuLyChamCongInfo>(form, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });

            Libs.ApiAction mAction = Libs.ApiAction.GET;
            if (action == "PUT")
                mAction = Libs.ApiAction.PUT;
            else if (action == "POST")
                mAction = Libs.ApiAction.POST;
            else if (action == "DELETE")
                mAction = Libs.ApiAction.DELETE;

            Models.MCC.Interfaces.IAttLogsSession mObj = new Models.MCC.clsAttLogsSession();
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
            {
                mObj = new Models.MCC.clsAttLogsSessionVỉtual();
            }
            Models.Interfaces.IResultOk ok = null;
            mObj.Modify(mAction, Temp, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CHI TIẾT GIỜ VÀO - RA
        public ActionResult ChiTietGioVaoRa()
        {
            ViewBag.Title = "Chi tiết giờ Vào - Ra";
            return View();
        }

        [HttpGet]
        public string ChiTietGioVaoRaGet(string fromDate, string toDate, string DepId, string MaNV)
        {
            if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate)) return "ERROR: fromDate, toDate is null";
            DateTime dtFrom, dtTo;
            DateTime.TryParse(fromDate, out dtFrom);
            DateTime.TryParse(toDate, out dtTo);
            IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo> Items = null;
            Models.Interfaces.IResultOk error = null;
            Models.Interfaces.ITimeInOutSession clsTimeInOut = new Models.clsTimeInOutSession();
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                clsTimeInOut = new Models.clsTimeInOutSessionVirtual();
            clsTimeInOut.GetBy(dtFrom, dtTo, DepId, MaNV == null ? "" : MaNV, (result, ex) =>
            {
                Items = result;
                error = ex;
            });
            if (!error.Success)
                return "ERROR: " + error.Message;
            if (Items == null)
                return "ERROR: NULL";

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items.ToList(), new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects });
        }

        [HttpGet]
        public string ChiTietGioVaoRaGetPaging(int selectedPage, int pageSize, string fromDate, string toDate, string DepId, string MaNV)
        {
            if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate)) return "ERROR: fromDate, toDate is null";
            DateTime dtFrom = Libs.clsCommon.ParseExactDMY(fromDate), dtTo = Libs.clsCommon.ParseExactDMY(toDate);
            //DateTime.TryParse(fromDate, out dtFrom);
            //DateTime.TryParse(toDate, out dtTo);
            int _totalRecords = 0;
            IEnumerable<Entity.Interfaces.ITimeInOutSessionInfo> Items = null;
            Models.Interfaces.IResultOk error = null;
            Models.Interfaces.ITimeInOutSession clsTimeInOut = new Models.clsTimeInOutSession();
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                clsTimeInOut = new Models.clsTimeInOutSessionVirtual();
            clsTimeInOut.GetPaging(selectedPage, pageSize, dtFrom, dtTo, DepId, MaNV == null ? "" : MaNV, (result, ex, totalRows) =>
            {
                Items = result;
                error = ex;
                _totalRecords = totalRows;
            });
            if (!error.Success)
                return "ERROR: " + error.Message;
            if (Items == null)
                return "ERROR: NULL";

            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = Items;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = pageSize;


            return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo, new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects });
        }

        public JsonResult TinhCong(string jsonNV)
        {
            var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsTimeInOutSessionInfo>(jsonNV, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            NtbSoft.ERP.Models.Interfaces.IQuyTacLamTron _QuyTacLamTron = new ERP.Models.clsQuyTacLamTron();
            NtbSoft.ERP.Models.Interfaces.IShift _Shift = new ERP.Models.clsShift();
            NtbSoft.ERP.Models.Interfaces.IScheduleSchift _ScheduleShift = new ERP.Models.clsScheduleShift();
            ERP.Models.Interfaces.INhanVien _NhanVienCC = new ERP.Models.clsNhanVienCC();

            Models.MCC.Interfaces.ITinhCong tc = new Models.MCC.clsTinhCong(_QuyTacLamTron, _Shift, _ScheduleShift, _NhanVienCC,
                new ERP.Models.clsTimeSheets(),
                new Models.DIC.clsDangKyVang(),
                new Models.DIC.clsDangKyCheDo(),
                new Models.DIC.clsDangKyLamThem());
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                tc = new Models.MCC.clsTinhCongVirtual(_QuyTacLamTron, _Shift, _ScheduleShift, _NhanVienCC,
                new ERP.Models.clsTimeSheetsVirtual(),
                new Models.DIC.clsDangKyVang(),
                new Models.DIC.clsDangKyCheDo(),
                new Models.DIC.clsDangKyLamThem()
                );

            if (item.MaNV == "0029" 
                && item.Workingday.ToString("yyyy-MM-dd") == "2018-12-31")
            {

            }

            ERP.Entity.Interfaces.ITimeInOutSessionInfo Info = null;
            ERP.Models.Interfaces.IResultOk ok = null;
            tc.TinhCong(item, (restult, ex) => { Info = restult; ok = ex; });
            if (ok.Success)
                return Json(Info, JsonRequestBehavior.AllowGet);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region BẢNG CHẤM CÔNG TIMESHEETS
        public ActionResult TimeSheets()
        {
            ViewBag.Title = "Bảng chấm công";
            return View();
        }

        [HttpGet]
        public string ReportMonthly(string year, string month, string DepId, string MaNV)
        {
            if (year == null || year == string.Empty)
            {
                year = "0";
            }
            if (month == null || month == string.Empty)
            {
                month = "0";
            }
            IEnumerable<Entity.Interfaces.ITimeSheetInfo> Items = null;
            Models.Interfaces.IResultOk error = null;
            Models.Interfaces.ITimeSheets clsTimeSheet = new Models.clsTimeSheets();
            if (AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                clsTimeSheet = new Models.clsTimeSheetsVirtual();
            clsTimeSheet.ReportMonthly(int.Parse(year), int.Parse(month), DepId, MaNV == null ? "" : MaNV, (result, ex) =>
            {
                Items = result;
                error = ex;
            });
            if (!error.Success)
                return "ERROR: " + error.Message;
            if (Items == null)
                return "ERROR: NULL";

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items.ToList(), new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects });
        }

        [HttpPost]
        public FileResult ExcelMonthly(string year, string month, string depId, string maNV)
        {
            try
            {

                Services.NPOIChiTietThang npoi = new Services.NPOIChiTietThang();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.ReportMonthly(Server, int.Parse(month), int.Parse(year), depId, maNV);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                //fs.Close();

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "bc_chi_tiet_thang.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }

        [HttpPost]
        public FileResult ExcelAttendanceYear(string year, string month, string depId, string maNV)
        {
            try
            {

                Services.NPOIChiTietThang npoi = new Services.NPOIChiTietThang();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.ReportYear(Server, int.Parse(month), int.Parse(year), depId, maNV);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                //fs.Close();

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "bc_chi_tiet_thang.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }

        #endregion

        #region OTHERS

        /// <summary>
        /// Lấy thông tin nhân viên và vân tay nhân viên để hiển thị
        /// </summary>
        /// <param name="DepId"></param>
        /// <returns></returns>
        public JsonResult GetViewVanTayNV(int selectedPage, int pageSize, string DepId, string TenNV, string MaCC)
        {
            Models.clsVanTayNhanVien VanTay = new Models.clsVanTayNhanVien();
            List<Entity.clsVanTayNhanVienInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            VanTay.GetInfomation(DepId, TenNV, MaCC, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
            if (Items == null) return Json("ERROR: NULL", JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDateOption(int type)
        {
            List<string> List = new List<string>();
            List.Add(DateTime.Now.ToString(Libs.clsConsts.FormatMDY));
            List.Add(DateTime.Now.ToString(Libs.clsConsts.FormatMDY));
            if (type < 13 || type == 16)
            {
                if (type == 16) type = DateTime.Now.Month;
                List = new List<string>();
                int days = DateTime.DaysInMonth(DateTime.Now.Year, type);
                List.Add(string.Format("01/{0}/{1}", type, DateTime.Now.Year));
                List.Add(string.Format("{0}/{1}/{2}", days, type, DateTime.Now.Year));
            }
            else if (type == 15)
            {
                DayOfWeek fdow = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
                int offset = fdow - DateTime.Now.DayOfWeek;
                DateTime firstDayOfWeek = DateTime.Now.AddDays(offset);
                List = new List<string>();
                List.Add(firstDayOfWeek.ToString(Libs.clsConsts.FormatMDY));
                List.Add(firstDayOfWeek.AddDays(6).ToString(Libs.clsConsts.FormatMDY));
                //DateTime ldowDate = firstDayOfWeek.AddDays(6);
            }

            return Json(List, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDateOptionDMY(int type)
        {
            List<string> List = new List<string>();
            List.Add(DateTime.Now.ToString(Libs.clsConsts.FormatDMY));
            List.Add(DateTime.Now.ToString(Libs.clsConsts.FormatDMY));
            if (type < 13 || type == 16)
            {
                if (type == 16) type = DateTime.Now.Month;
                List = new List<string>();
                int days = DateTime.DaysInMonth(DateTime.Now.Year, type);
                List.Add(string.Format("01/{0}/{1}", type, DateTime.Now.Year));
                List.Add(string.Format("{0}/{1}/{2}", days, type, DateTime.Now.Year));
            }
            else if (type == 15)
            {
                DayOfWeek fdow = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
                int offset = fdow - DateTime.Now.DayOfWeek;
                DateTime firstDayOfWeek = DateTime.Now.AddDays(offset);
                List = new List<string>();
                List.Add(firstDayOfWeek.ToString(Libs.clsConsts.FormatDMY));
                List.Add(firstDayOfWeek.AddDays(6).ToString(Libs.clsConsts.FormatDMY));
                //DateTime ldowDate = firstDayOfWeek.AddDays(6);
            }
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClearAdministrators(int id)
        {
            Exception _ex = null;
            Devices.MCC.Services.Interfaces.IDeviceManager devce = null;
            //
            /** Lấy thông tin máy chấm công ở dưới CSDL ***/
            Models.Interfaces.IBase<Entity.Interfaces.ITerminalInfo> _Terminal = new Models.clsTerminal();
            Entity.Interfaces.ITerminalInfo TerminalInfo = null;
            _Terminal.GetBy(id, (result, ex) => { _ex = ex; TerminalInfo = result; });

            if (_ex != null) return Json("ERROR: Load MCC " + _ex.Message, JsonRequestBehavior.AllowGet);
            if (TerminalInfo == null) return Json("ERROR: NULL ", JsonRequestBehavior.AllowGet);
            bool IsClear = false;
            if (TerminalInfo.CommType == 1) { }
            else if (TerminalInfo.CommType == 2)
            {
                devce = new NtbSoft.Devices.MCC.Services.clsDeviceManager(id, new Devices.MCC.Services.ConnectNet(TerminalInfo.IpAddress, TerminalInfo.Port));
                devce.Connect((result, ex) =>
                {
                    if (ex == null && result)
                    {
                        devce.ClearAdministrators(complete => { IsClear = complete; });
                    }
                });
            }
            if (!IsClear) return Json("Fail", JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOptionDay(int month, int year)
        {

            //int Year = DateTime.Now.Year;
            //int Month = DateTime.Now.Month;
            //if (month < 12)
            //    Month = month;
            List<Entity.KeyValueItem> values = new List<Entity.KeyValueItem>();
            //values.Add(new Entity.KeyValueItem() { Key = 1, Value = "1-T6" });
            DateTime lastDay = Libs.clsCommon.LastDayOfMonth(month, year);
            for (int i = 1; i <= lastDay.Day; i++)
            {
                DateTime dtTemp = new DateTime(year, month, i);
                string t = "";
                if (dtTemp.DayOfWeek == DayOfWeek.Sunday) t = "CN";
                else if (dtTemp.DayOfWeek == DayOfWeek.Monday) t = "T2";
                else if (dtTemp.DayOfWeek == DayOfWeek.Tuesday) t = "T3";
                else if (dtTemp.DayOfWeek == DayOfWeek.Wednesday) t = "T4";
                else if (dtTemp.DayOfWeek == DayOfWeek.Thursday) t = "T5";
                else if (dtTemp.DayOfWeek == DayOfWeek.Friday) t = "T6";
                else if (dtTemp.DayOfWeek == DayOfWeek.Saturday) t = "T7";
                values.Add(new Entity.KeyValueItem() { Key = i, Value = string.Format("{0}-{1}", i, t) });
            }

            return Json(values, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetYears()
        {

            int MinYear = 2010;
            int MaxYar = DateTime.Now.Year;
            JData.DataComboBoxInfo[] Arrays = new JData.DataComboBoxInfo[(MaxYar - MinYear) + 1];

            for (int i = 0; i <= (MaxYar - MinYear); i++)
                Arrays[i] = new JData.DataComboBoxInfo() { id = (MaxYar - i).ToString(), name = (MaxYar - i).ToString() };

            return Json(Arrays, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}