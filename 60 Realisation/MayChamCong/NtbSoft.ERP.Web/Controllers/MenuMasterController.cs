﻿using Microsoft.Web.Http;
using Newtonsoft.Json.Linq;
using NtbSoft.ERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    [ApiVersion("1")]
    [Route("api/MenuMaster/{action}")]
    [Route("api/v{version:apiVersion}/MenuMaster/{action}")]
    [Route("api/v{version:apiVersion}/MenuMaster/{action}/{id}")]
    public class MenuMasterController : ApiController
    {
        Entity.clsMenuMasterCollection Items;
        public MenuMasterController()
        {
            //clsSysTest cls = new clsSysTest();
            
            //var items = cls.GetAllView();
            //var kk = items;

            #region TEST ==================================
            //Entity.Interfaces.IWebCustomerInfo info = new Entity.clsWebCustomerInfo();
            //info.CusID = "A0000001WEB";
            //info.CusName = "Cty May Việt Thái";
            //Entity.Interfaces.IWebServerCustomer webServerCustomer = new clsWebServerCustomer(info);
            //var values = webServerCustomer.GetByCustumer();

            //var kk = values;
            //var dd = kk;
            #endregion
        }

        [HttpGet]
        public IHttpActionResult GetIdDm()
        {
            try
            {
                return Ok(Models.clsSystem.GetIdDm("", "SYS_MENU_MASTER"));
            }
            catch (Exception ex)
            {
                return Ok("ERROR:" + ex.Message);
            }

        }
        // GET api/MenuMaster
        [Authorize]
        [HttpGet]
        public string GetAll()
        {
            clsMenuMaster clsMenu = new clsMenuMaster();
            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
            {
                Items = new Entity.clsMenuMasterCollection();
                FormsAuthentication.SignOut();
            }
            else
            {
                
                string[] MyRoles = Roles.GetRolesForUser(System.Web.HttpContext.Current.User.Identity.Name);
                //var k = ArrayRoles.FirstOrDefault(f => f == "Administrator2");
                //var dd = k;
                if (System.Web.HttpContext.Current.User.Identity.Name.Equals("admin") ||
                System.Web.HttpContext.Current.User.Identity.Name.Equals("user") ||
                    MyRoles.Contains("Administrator") || MyRoles.Contains("SYSTEM"))
                    Items = clsMenu.GetAll();
                else
                {
                    Items = clsMenu.GetByUserId(System.Web.HttpContext.Current.User.Identity.Name);
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);

        }

       

        [HttpGet]
        public string GetTreeData(string Id,bool hasLastNode)
        {
            try
            {
                Models.clsMenuMaster _clsObj = new clsMenuMaster();
                return Newtonsoft.Json.JsonConvert.SerializeObject(_clsObj.GetTreeData(Id, hasLastNode));
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public IHttpActionResult CheckPermmision(string MenuId)
        {
            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name)) return Ok("false");

            string[] MyRoles = Roles.GetRolesForUser(System.Web.HttpContext.Current.User.Identity.Name);
            if (System.Web.HttpContext.Current.User.Identity.Name.Equals("admin") ||
            System.Web.HttpContext.Current.User.Identity.Name.Equals("user") ||
                MyRoles.Contains("Administrator") || MyRoles.Contains("SYSTEM"))
                return Ok("true");

            if (string.IsNullOrWhiteSpace(MenuId)) return Ok("false");

            Models.clsMenuMaster _clsMenu = new Models.clsMenuMaster();
            var UserPermission = _clsMenu.GetPermissionMenu(System.Web.HttpContext.Current.User.Identity.Name,
                        MenuId);
            if (UserPermission == null) return Ok("false");

            return Ok("true");
        }

        [HttpGet]
        public string GetContextMenu(string MenuId, string ContextMenuId, string MenuIdMain)
        {
            if (System.Web.HttpContext.Current == null) return "";
            try
            {
                //Entity.clsTreeMenuInfo UserPermission;
                //System.Web.HttpContext.Current.User.Identity.Name
               // if (System.Web.HttpContext.Current.User.Identity.Name.Equals("admin") ||
               //System.Web.HttpContext.Current.User.Identity.Name.Equals("user"))
               //     UserPermission = new Entity.clsTreeMenuInfo("", true, true, true, true);

                //System.Web.HttpContext.Current.Request.UrlReferrer
                Models.clsMenuMaster _clsMenu = new Models.clsMenuMaster();
                

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UrlReferrer.Query))
                    MenuIdMain = System.Web.HttpUtility.ParseQueryString(System.Web.HttpContext.Current.Request.UrlReferrer.Query).GetValues("id").LastOrDefault();
                if (string.IsNullOrEmpty(MenuIdMain)) MenuIdMain = "";
                
                return Newtonsoft.Json.JsonConvert.SerializeObject(_clsMenu.GetContextMenu(MenuId, ContextMenuId,
                    System.Web.HttpContext.Current.User.Identity.Name, MenuIdMain));
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public string GetTreeNodeMenu(string UserId)
        {
            try
            {
                if (string.IsNullOrEmpty(UserId)) UserId = "";
                Models.clsMenuMaster dep = new Models.clsMenuMaster();
                Entity.clsTreeMenuCollection Items = dep.GetTreeNodes(UserId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpPost]
        [PermissionsMenu(NtbSoft.ERP.Libs.Permissions.Edit)]
        public IHttpActionResult SetPermisionUser([FromBody]JObject data)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();
            try
            {
                Entity.clsTreeMenuCollection collection =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsTreeMenuCollection>(data["Json"].ToString());
                Entity.clsUserInfo userInfo =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsUserInfo>(data["UserInfo"].ToString());

                if (string.IsNullOrEmpty(userInfo.UserID) || userInfo.UserID.Equals("admin") ||
                    collection == null || collection.Count == 0) return Ok("true");

                System.Data.DataTable TYPE_SYS_USER_MENU = clsMenuMaster.TYPE_SYS_USER_MENU;
                foreach (var item in collection)
                {
                    System.Data.DataRow newRow = TYPE_SYS_USER_MENU.NewRow();
                    newRow["UserID"] = userInfo.UserID;
                    newRow["MenuID"] = item.id;
                    newRow["AllowView"] = item.AllowView;
                    newRow["AllowAdd"] = item.AllowAdd;
                    newRow["AllowEdit"] = item.AllowEdit;
                    newRow["AlowDelete"] = item.AlowDelete;
                    
                    /***Kiểm tra childNode có được checked hay không. Nếu đc checked thì add ***/
                    if (checkPermisionChildNode(item) ||
                        item.AllowView || item.AllowAdd || item.AllowEdit || item.AlowDelete)
                    {
                        TYPE_SYS_USER_MENU.Rows.Add(newRow);
                        addChildNode(ref TYPE_SYS_USER_MENU, item, userInfo.UserID);
                    }
                }
                clsMenuMaster.SetPermisionUser(userInfo, TYPE_SYS_USER_MENU);
                return Ok("true");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        private void addChildNode(ref System.Data.DataTable TYPE_SYS_USER_MENU, Entity.clsTreeMenuInfo ItemTreeMenu,string UserID)
        {
            if (ItemTreeMenu.children == null) return;
            /*** Duyệt qua các node cha(ParentID) ***/
            foreach (var item in ItemTreeMenu.children)
            {
                System.Data.DataRow newRow = TYPE_SYS_USER_MENU.NewRow();
                newRow["UserID"] = UserID;
                newRow["MenuID"] = item.id;
                newRow["AllowView"] = item.AllowView;
                newRow["AllowAdd"] = item.AllowAdd;
                newRow["AllowEdit"] = item.AllowEdit;
                newRow["AlowDelete"] = item.AlowDelete;
                /*** Kiểm tra menu con có check hay không. nếu không check thì tiếp tục kiểm tra các quyền View,Add,Edit,Delete nếu true thì tiến hành thêm***/
                if (checkPermisionChildNode(item) ||
                    item.AllowView || item.AllowAdd || item.AllowEdit || item.AlowDelete)
                {
                    TYPE_SYS_USER_MENU.Rows.Add(newRow);
                    //Gọi đệ quy
                    addChildNode(ref TYPE_SYS_USER_MENU, item, UserID);
                }
            }
        }

        /// <summary>
        /// Kiểm tra các node con có check các quyền View,Add,Edit,Delete 
        /// True: có, False: không
        /// </summary>
        /// <param name="ItemTreeMenu"></param>
        /// <returns></returns>
        private bool checkPermisionChildNode(Entity.clsTreeMenuInfo ItemTreeMenu)
        {
            if (ItemTreeMenu.children == null) return false;
            foreach (var item in ItemTreeMenu.children)
            {
                if (item.AllowView || item.AllowAdd || item.AllowEdit || item.AlowDelete)
                    return true;
                //Gọi đệ quy
                bool _hasChecked = checkPermisionChildNode(item);
                if (_hasChecked) return true;
            }

            return false;
        }

        [HttpPost]
        public IHttpActionResult SetPermisionGroup([FromBody]JObject data)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();

            try
            {
                Entity.clsTreeMenuCollection collection =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsTreeMenuCollection>(data["JsonMenu"].ToString());
                Entity.clsGroupInfo groupInfo =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsGroupInfo>(data["GroupInfo"].ToString());

                Entity.clsUserCollection Users =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsUserCollection>(data["JsonGroupUser"].ToString());

                if (string.IsNullOrEmpty(groupInfo.GroupID)) return Ok("false");
                System.Data.DataTable TYPE_SYS_USER_GROUP = clsMenuMaster.TYPE_SYS_USER_GROUP;
                if(Users!=null)
                    foreach (var item in Users)
                    {
                        System.Data.DataRow newRow = TYPE_SYS_USER_GROUP.NewRow();
                        newRow["UserID"] = item.UserID;
                        newRow["GroupID"] = groupInfo.GroupID;

                        TYPE_SYS_USER_GROUP.Rows.Add(newRow);
                    }


                System.Data.DataTable TYPE_SYS_USER_MENU = clsMenuMaster.TYPE_SYS_USER_MENU;
                //foreach (var item in collection)
                //{
                //    System.Data.DataRow newRow = TYPE_SYS_USER_MENU.NewRow();
                //    newRow["UserID"] = userInfo.UserID;
                //    newRow["MenuID"] = item.id;
                //    newRow["AllowView"] = item.AllowView;
                //    newRow["AllowAdd"] = item.AllowAdd;
                //    newRow["AllowEdit"] = item.AllowEdit;
                //    newRow["AlowDelete"] = item.AlowDelete;
                //    TYPE_SYS_USER_MENU.Rows.Add(newRow);

                //    addChildNode(ref TYPE_SYS_USER_MENU, item, userInfo.UserID);
                //}
                bool result = clsMenuMaster.SetPermisionGroupUser(groupInfo,TYPE_SYS_USER_GROUP, TYPE_SYS_USER_MENU);
                if (result) return Ok("true");
                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        // Get api/values
        public string Post()
        {
            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
                return "/Login.aspx";

            FormsAuthentication.SignOut();
            return "/Login.aspx";
        }

        [HttpPost]
        public IHttpActionResult Rename(string id, string name,string url)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();

            try
            {
                if (clsMenuMaster.Rename(id, name,url))
                    return Ok("true");
                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult SaveProperty(string id, string name, string MoTa, string ChuoiBatDau, string KyTuCach,
            string SoChuSo, string SoBatDau, string SoKetThuc, string MenuType)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();
            int _SoChuSo, _SoBatDau, _SoKetThuc;
            int.TryParse(SoChuSo, out _SoChuSo);
            int.TryParse(SoBatDau, out _SoBatDau);
            int.TryParse(SoKetThuc, out _SoKetThuc);

            try
            {
                if (clsMenuMaster.Update(id, name, MoTa, ChuoiBatDau, KyTuCach, _SoChuSo, _SoBatDau, _SoKetThuc, MenuType))
                    return Ok("true");
                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Update(string MenuId, string MenuName, string MenuParent,
            string ContextMenuId, string ContextMenuIdFile, string Url, string DefaultUrl, int Sort, string MenuType)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();
            try
            {
                string NewMenuName = "";
                if (clsMenuMaster.Update(NtbSoft.ERP.Libs.ApiAction.POST, MenuId, MenuName,
                    MenuParent, ContextMenuId, ContextMenuIdFile, Url, DefaultUrl, Sort,MenuType, out NewMenuName))
                    return Ok(NewMenuName);
                // Items = clsMenuMaster.GetAll();
                //    return Ok(Newtonsoft.Json.JsonConvert.SerializeObject(Items));

                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok("ERROR. " + ex.Message);
            }
        }
        [HttpPost]
        public IHttpActionResult Delete(string id)
        {
            Models.clsMenuMaster clsMenuMaster = new clsMenuMaster();

            try
            {
                //MenuId = Guid.NewGuid().ToString();
                string NewName="";
                if (clsMenuMaster.Update(NtbSoft.ERP.Libs.ApiAction.DELETE, id, "", "","","", "", "", 0,"", out NewName))
                    return Ok("true");
                // Items = clsMenuMaster.GetAll();
                //    return Ok(Newtonsoft.Json.JsonConvert.SerializeObject(Items));

                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
    }
}
