﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class DemoAPIController : Controller
    {
        //
        // GET: /DemoAPI/
        public ActionResult Index()
        {
            ViewBag.Title = "DemoAPI";

            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Request.UserHostAddress;
            }
            var dddd = HttpContext.Request.UserHostAddress;

            var kq = GetIP();
            var dd = kq;
            return View();
        }

        public string GetIP()
        {
            string externalIP = "";
            try
            {
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new System.Text.RegularExpressions.Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch (Exception ex)
            {
                return externalIP;
            }
        }
	}
}