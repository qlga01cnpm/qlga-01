﻿using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Route("api/Account/{action}")]
    [Route("api/v{version:apiVersion}/Account/{action}")]
    public class AccountController : ApiController
    {
        // POST api/Account/Logout
        [HttpPost]
        public IHttpActionResult Logout()
        {
            if (System.Web.HttpContext.Current.User != null && System.Web.HttpContext.Current.User.Identity != null)
                System.Web.Security.Roles.RemoveUsersFromRoles(
                    new string[1] { System.Web.HttpContext.Current.User.Identity.Name }, new string[1] {"All" });
            
            System.Web.Security.FormsAuthentication.SignOut();
            // Session.Abandon();
            //Session.Clear();
            //Session.RemoveAll(); 

            if (System.Web.HttpContext.Current.Session != null)
            {
                System.Web.HttpContext.Current.Session.Abandon();
                System.Web.HttpContext.Current.Session.Clear();
                System.Web.HttpContext.Current.Session.RemoveAll();
            }
            return Ok("/Login.aspx");
        }

        [HttpPost]
        public IHttpActionResult CheckLogin()
        {
            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
            {
                return Ok("/Login.aspx");
            }

            return Ok("true");
        }

        [HttpGet]
        public IHttpActionResult GetUserLogin()
        {
            if(AppCaches.clsCacheTheApp.Instance.AppInfo.IsVirtualData)
                return Ok(string.Format("{0} &copy;", System.Web.HttpContext.Current.User.Identity.Name));
            return Ok(System.Web.HttpContext.Current.User.Identity.Name);
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(string pw,string newPw,string confirmPw)
        {
            if (string.IsNullOrWhiteSpace(pw) || string.IsNullOrWhiteSpace(newPw) || string.IsNullOrWhiteSpace(confirmPw))
                return Ok("ERROR: Mật khẩu rỗng");
            if (!newPw.Equals(confirmPw))
                return Ok("ERROR: Mật khẩu xác nhận không trùng khớp. Vui lòng kiểm tra lại.");
            Models.clsUserBS user = new Models.clsUserBS();
            if (user.ChangePassword(System.Web.HttpContext.Current.User.Identity.Name, pw, newPw, confirmPw))
                return Ok("OK");
            return Ok("OK");
        }
    }
}
