﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class WebCustomerController : Controller
    {
        //
        // GET: /WebCustomer/
        public ActionResult Productivity()
        {
            ViewBag.Title = "Bảng theo dõi năng suất";
            return View();
        }

        public ActionResult ViewCutting()
        {
            ViewBag.Title = "Bảng theo dõi tổ Căt";
            return View();
        }

        public ActionResult ViewPackingList()
        {
            ViewBag.Title = "Bảng theo dõi Đóng Gói - Thành Phẩm";
            return View();
        }
	}
}