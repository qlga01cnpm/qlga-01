﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace NtbSoft.ERP.Web.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        //http://www.binaryintellect.net/articles/436bc727-6de2-4d11-9762-9ee1a1e3cbea.aspx
        /*** Phân quyền cho phương thức Get nhưng user nào được truy cập ***/
        // GET api/values
        [Authorize]
        public IEnumerable<string> Get()
        {
            if (User.Identity.IsAuthenticated)
            {
                //MembershipUser user = Membership.GetUser();
                if (Roles.IsUserInRole(User.Identity.Name, "ADMIN"))
                {//Admin
                    return new string[] { "Blue", "Red" };
                }
                else
                {
                    return new string[] { "Orange", "White" };
                }
            }
            else
            {
                throw new Exception("You are not authorized to use this page!");
            }
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
