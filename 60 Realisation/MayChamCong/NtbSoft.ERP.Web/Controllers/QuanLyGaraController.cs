﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class QuanLyGaraController : Controller
    {
        // GET: QuanLyGara
        #region Xử lý thông tin khách hàng
        public ActionResult KhachHang()
        {
            return View();
        }

        [HttpGet]
        public string GetThongTinKhachHang(int selectedPage, int pageSize, string DepId, string Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetThongTinKhachHang(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }

        [HttpPost]
        public string KhachHangXuLy(string type, string json) //Thêm xoá sửa thông tin khách hàng
        {
            Models.clsQLGRKhachHang _KhachHang = new Models.clsQLGRKhachHang();
            Models.Interfaces.IResultOk result = null;
            Entity.clsQLGRKhachHangInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsQLGRKhachHangInfo>(json,
                 new Newtonsoft.Json.JsonSerializerSettings());

            if (type == "PUT")//Cap nhat
            {
                _KhachHang.Update(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "POST")//Them moi
            {
                _KhachHang.Insert(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "DELETE")//Xoa
            {
                _KhachHang.DeleteByKey(Item.KhachHangId, ok =>
                {
                    result = ok;
                });
            }
            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
        }

        [HttpPost]
        public string SaveHoSoXe(string type, string json) //Lưu hồ sơ xe
        {
            Models.clsQLGRKhachHang _KhachHang = new Models.clsQLGRKhachHang();
            Models.Interfaces.IResultOk result = null;
            Entity.clsQLGRHoSoXeInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsQLGRHoSoXeInfo>(json,
                 new Newtonsoft.Json.JsonSerializerSettings());

            if (type == "POST")//Them moi
            {
                _KhachHang.InsertHoSoXe(Item, ok =>
                {
                    result = ok;
                });

            }
            
            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
        }

        public ActionResult HoSoXe()
        {
            return View();
        }

        [HttpGet]
        public string GetHoSoXe(int selectedPage, int pageSize, string DepId, string Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetHoSoXe(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }


        public ActionResult PhieuSuaChua()
        {
            return View();
        }

        [HttpGet]
        public string GetDetailPSC(int selectedPage, int pageSize, string DepId, int Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetDetailPSC(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }

        [HttpGet]
        public string GetHieuXe()
        {
            Exception _ex = null;
            //int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetHieuXe((result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            string abc = JsonConvert.SerializeObject(tbl);
            if (_ex == null)
                return abc;

            return "ERROR: " + _ex.Message;
        }

        [HttpPost]
        public string PhieuSuaChua(string type, string json) //Lập phiếu sữa chữa
        {
            Models.clsQLGRKhachHang _KhachHang = new Models.clsQLGRKhachHang();
            Models.Interfaces.IResultOk result = null;
            Entity.clsQLGRPhieuSuaChuaInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsQLGRPhieuSuaChuaInfo>(json,
                 new Newtonsoft.Json.JsonSerializerSettings());
            if (type == "POST")//Them moi
            {
                _KhachHang.InsertPhieuSuaChua(Item, ok =>
                {
                    result = ok;
                });

            }
            else if (type == "DELETE")//Xoa
            {
                _KhachHang.DeletePSC(Item.PhuTungId, ok =>
                {
                    result = ok;
                });
            }

            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
        }

        public ActionResult PhieuThuTien()
        {
            return View();
        }

        [HttpGet]
        public string GetTTKH(int selectedPage, int pageSize, string DepId, int Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetTTKH(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            List<string> list = new List<string>();
            foreach(DataRow dr in tbl.Rows)
            {
                _PagingInfo.note1 = dr["TenKhachHang"].ToString();
                _PagingInfo.note2 = dr["SoDienThoai"].ToString();
                _PagingInfo.note3 = dr["Email"].ToString();
                _PagingInfo.note4 = dr["BienSo"].ToString();
                _PagingInfo.note5 = dr["TongTien"].ToString();
            }
            
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }

        [HttpGet]
        public string GetHoSoSuaChua(int selectedPage, int pageSize, string DepId, string Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetHoSoSuaChua(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }

        [HttpGet]
        public string GetPhuTung()
        {
            Exception _ex = null;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetPhuTung((result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            string abc = JsonConvert.SerializeObject(tbl);
            if (_ex == null)
                return abc;

            return "ERROR: " + _ex.Message;
        }

        [HttpGet]
        public string GetTienCong()
        {
            Exception _ex = null;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetTienCong((result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            string abc = JsonConvert.SerializeObject(tbl);
            if (_ex == null)
                return abc;

            return "ERROR: " + _ex.Message;
        }

        [HttpPost]
        public string PhieuThuTien(string type, string json) //Lập phiếu sữa chữa
        {
            Models.clsQLGRKhachHang _KhachHang = new Models.clsQLGRKhachHang();
            Models.Interfaces.IResultOk result = null;
            Entity.clsQLGRPhieuThuTienInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsQLGRPhieuThuTienInfo>(json,
                 new Newtonsoft.Json.JsonSerializerSettings());
            if (type == "POST")//Them moi
            {
                _KhachHang.InsertPhieuThuTien(Item, ok =>
                {
                    result = ok;
                });

            }

            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
        }



        public ActionResult TraCuuThongTin()
        {
            return View();
        }

        [HttpGet]
        public string GetThongTinTraCuu(int selectedPage, int pageSize, string DepId, string Keysearch)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetThongTinTraCuu(selectedPage, pageSize, DepId, Keysearch, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }


        public ActionResult BaoCaoDS()
        {
            return View();
        }

        [HttpGet]
        public string BaoCaoDoanhSo(int selectedPage, int pageSize, string DepId, string Month)
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.IKhachHang _khachhang = new Models.clsQLGRKhachHang();
            DataTable tbl = null;
            _khachhang.GetBaoCaoDoanhSo(selectedPage, pageSize, DepId, Month, out _totalPages, out _totalRecords, (result, ex) =>
            {
                tbl = result;
                _ex = ex;
            });
            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = tbl;
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);

            return "ERROR: " + _ex.Message;
        }
        #endregion
    }
}