﻿using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    [ApiVersion("1")]
    [Route("api/RootDirectory/{action}")]
    [Route("api/v{version:apiVersion}/RootDirectory/{action}")]
    [Route("api/v{version:apiVersion}/RootDirectory/{action}/{id}")]
    public class RootDirectoryController : ApiController
    {
        public RootDirectoryController() { }

        [HttpGet]
        public IHttpActionResult GetIdDm()
        {
            try
            {
                return Ok(Models.clsSystem.GetIdDm("", "SYS_ROOT_DIRECTORY"));
            }
            catch (Exception ex)
            {
                return Ok("ERROR:" + ex.Message);
            }

        }

        [Authorize]
        [HttpGet]
        public string GetDirectory(string RootId)
        {
            try
            {
                Models.clsRootDirectory clsObj = new Models.clsRootDirectory();
                Entity.clsRootDirectoryCollection Items = clsObj.GetRootDirectory(RootId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public IHttpActionResult Update(string Id, string Name, string ParentId,
            string ContextMenuId, int Sort)
        {
            Models.clsRootDirectory clsObject = new Models.clsRootDirectory();
            try
            {
                string NewMenuName = "";
                if (clsObject.Update(NtbSoft.ERP.Libs.ApiAction.POST, Id, Name,
                    ParentId, ContextMenuId, "javascript:void(0)", Sort, out NewMenuName))
                    return Ok(NewMenuName);


                return Ok("false");
            }
            catch (Exception ex)
            {
               return Ok("ERROR: " + ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Rename(string id, string name)
        {
            Models.clsRootDirectory clsObject = new Models.clsRootDirectory();

            try
            {
                if (clsObject.Rename(id, name))
                    return Ok("true");
                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Delete(string id)
        {
            Models.clsRootDirectory clsObject = new Models.clsRootDirectory();

            try
            {
                string NewName = "";
                if (clsObject.Update(NtbSoft.ERP.Libs.ApiAction.DELETE, id, "",
                    "", "", "", 0, out NewName))
                    return Ok("true");

                return Ok("false");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
    }
}
