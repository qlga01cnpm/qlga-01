﻿using ExcelDataReader;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    public class SettingController : Controller
    {
        //[PermissionsMenu(NtbSoft.ERP.Libs.Permissions.View)]
        public ActionResult Department()
        {
            ViewBag.Title = "Đơn vị - Phòng ban";
            return View();
        }

        #region Bậc thợ ===================================================================================================
        public ActionResult BacTho()
        {
            ViewBag.Title = "Bậc thợ";
            return View();
        }

        [HttpGet]
        public string BacThoGet()
        {
            Models.clsBacTho _clsBacTho = new Models.clsBacTho();

            return Newtonsoft.Json.JsonConvert.SerializeObject(_clsBacTho.GetAll());
        }


        [HttpPost]
        public string BacThoUpdate(Entity.clsBacThoCollection Items)
        {
            if (Items == null) return "Đối tượng null";
            Models.clsBacTho _clsBacTho = new Models.clsBacTho();
            try
            {
                if (_clsBacTho.Update(Items))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName,
                        HttpContext.User.Identity.Name, "", "", "Thao tác cập nhật bậc thợ",
                        Newtonsoft.Json.JsonConvert.SerializeObject(Items));
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpDelete]
        public string BacThoDelete(string id)
        {
            Models.clsBacTho _clsBacTho = new Models.clsBacTho();
            try
            {
                if (_clsBacTho.Delete(id))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName,
                        HttpContext.User.Identity.Name, "", "", "Thao tác xóa bậc thợ", id);
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region Thiết Bị ==================================================================================================
        public ActionResult ThietBi()
        {
            ViewBag.Title = "Thiết bị";
            return View();
        }

        [HttpGet]
        public string ThietBiGetBy(string RootId)
        {
            try
            {
                Models.clsThietBi dep = new Models.clsThietBi();
                Entity.clsThietBiCollection Items = dep.GetBy(RootId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public string ThaoTacSpGetThietBi(string id)
        {
            try
            {
                Models.clsThietBi clsDal = new Models.clsThietBi();
                Entity.clsThietBiInfo Item = clsDal.GetThietBi(id);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpDelete]
        public string ThietBiDelete(string id)
        {
            Models.clsThietBi _clsObj = new Models.clsThietBi();
            try
            {
                if (_clsObj.Delete(id))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName, HttpContext.User.Identity.Name, "", "", "Thao tác xóa thiết bị", id);
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public string ThietBiUpdate(Entity.clsThietBiCollection Items)
        {
            if (Items == null) return "Đối tượng null";
            Models.clsThietBi _clsObj = new Models.clsThietBi();
            try
            {
                if (_clsObj.Update(Items))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName, HttpContext.User.Identity.Name, "", "", "Thao tác cập nhật thiết bị ", Newtonsoft.Json.JsonConvert.SerializeObject(Items));
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Thao tác sản phẩm =========================================================================================
        public ActionResult ThaoTacSanPham()
        {
            ViewBag.Title = "Thời gian chuẩn bị";
            return View();
        }

        [HttpGet]
        public string ThaoTacSpGetBy(string RootId)
        {
            try
            {
                Models.clsThaoTacSp dep = new Models.clsThaoTacSp();
                Entity.clsThaoTacSpCollection Items = dep.GetBy(RootId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public string ThaoTacSpGetThaoTac(string id)
        {
            try
            {
                Models.clsThaoTacSp dep = new Models.clsThaoTacSp();
                Entity.clsThaoTacSpInfo Item = dep.GetThaoTac(id);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpDelete]
        public string ThaoTacSpDelete(string id)
        {
            Models.clsThaoTacSp _clsObj = new Models.clsThaoTacSp();
            try
            {
                if (_clsObj.Delete(id))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName, HttpContext.User.Identity.Name, "", "", "Thao tác xóa thời gian chuẩn bị", id);
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public string ThaoTacSpUpdate(Entity.clsThaoTacSpCollection Items)
        {
            if (Items == null) return "Đối tượng null";
            Models.clsThaoTacSp _clsObj = new Models.clsThaoTacSp();
            try
            {
                if (_clsObj.Update(Items))
                {
                    Models.clsSysLog.WriteEventLog(Server.MachineName, HttpContext.User.Identity.Name, "", "", "Thao tác cập nhật thời gian chuẩn bị", Newtonsoft.Json.JsonConvert.SerializeObject(Items));
                    return "true";
                }

                return "false";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region BỘ PHẬN

        [HttpGet]
        public string GetTreeNodeDep()
        {
            Models.clsDep dep = new Models.clsDep();
            Entity.clsTreeDepCollection Items = dep.GetTreeNodes();

            return Newtonsoft.Json.JsonConvert.SerializeObject(Items);

        }


        [HttpPost]
        public string Update(string DepID, string DepName, string ParentID,
           string ContextMenuId, string ContextMenuIdFile,
            bool IsChain, bool IsChainSew, bool IsChainCut, bool IsChainPackage, bool IsWarehouse, int Sort)
        {
            Models.clsDep clsDep = new Models.clsDep();
            try
            {
                if (clsDep.Update(NtbSoft.ERP.Libs.ApiAction.POST, DepID, DepName,
                    ParentID, ContextMenuId, ContextMenuIdFile, IsChain, IsChainSew, IsChainCut, IsChainPackage, IsWarehouse, Sort))
                    return "true";

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpPost]
        public string Delete(string DepID)
        {
            Models.clsDep clsDep = new Models.clsDep();
            try
            {
                if (clsDep.Update(NtbSoft.ERP.Libs.ApiAction.DELETE, DepID, "",
                    "", "", "", false, false, false, false, false, 0))
                    return "true";

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }
        #endregion

        #region ExecuteSql
        public ActionResult ExecuteSql()
        {
            ViewBag.Title = "Execute Sql";
            return View();
        }
        [HttpPost]
        public ActionResult Execute(string sqlcommand)
        {
            sqlcommand = System.Web.HttpUtility.HtmlDecode(sqlcommand);
            bool result = Models.clsServer.RunScript(sqlcommand);
            if (result)
            { 
                return Json(new { Message = "ok" });
            }
            else
            { 
                return Json(new { Message = "error" });
            }
        }
        #endregion

        #region THÔNG TIN CÁ NHÂN
        public ActionResult ThongTinCaNhan()
        {
            ViewBag.Title = "Thông tin cá nhân";
            return View();
        }

        [HttpGet]
        public string GetThongTinCaNhan()
        {
            Exception _ex = null;
            int _totalPages = 1, _totalRecords = 500;
            Models.ISinhVien _sinhvien = new Models.clsSinhVien();
            List<Entity.Interfaces.ISinhVienInfo> Items = new List<Entity.Interfaces.ISinhVienInfo>();
            _sinhvien.GetThongTinSinhVien((result, ex) =>
            {
                Items = result.ToList();
                _ex = ex;
            });

            //var kq = Items.FindAll(f => !f.IsNghiViec);

            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = Items;
            //_PagingInfo.Tag = new List<Entity.Interfaces.INhanVienInfo>(kq);
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);
            //, new Newtonsoft.Json.JsonSerializerSettings()
            //    {
            //        TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects // Co su dung interface
            //    });

            return "ERROR: " + _ex.Message;
        }

        [HttpPost]
        public string SinhVienXuLy(string type, string json)
        //public ActionResult NhanVienXuLy(string type, Entity.clsNhanVienInfo item)
        {//type: POST,PUT,DELETE


            Models.clsSinhVien _clsSinhvien = new Models.clsSinhVien();
            Models.Interfaces.IResultOk result = null;
            Entity.clsSinhVienInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsSinhVienInfo>(json);

            if (type == "PUT")//Cap nhat
            {
                _clsSinhvien.Update(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "POST")//Them moi
            {
                _clsSinhvien.Insert(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "DELETE")
            {
                _clsSinhvien.DeleteByKey(Item.StudentID, ok =>
                {
                    result = ok;
                });
            }
            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
            //return Json(new { result = "Cap nhat" });

        }

        [HttpPost]
        public FileResult TemplateSinhVien()
        {
            byte[] allFile = System.IO.File.ReadAllBytes(Server.MapPath(@"\Content\Templates\sinhvien.xlsx"));
            return File(allFile, System.Net.Mime.MediaTypeNames.Application.Octet, "template_sinhvien.xlsx");//dat cho vui
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ImportSinhVien(HttpPostedFileBase file)
        {

            //HttpFileCollectionBase filessss = Request.Files;
            //var s = filessss;
            //var d = s;
            //var c = d;
            //if (file != null && file.ContentLength > 0)
            //{
            //    Stream stream = file.InputStream;

            //}
            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            #region Cach 1
            //if (files != null && files.Count > 0)
            //{
            //    // ExcelDataReader works with the binary Excel file, so it needs a FileStream
            //    // to get started. This is how we avoid dependencies on ACE or Interop:
            //    Stream stream = files[0].InputStream;

            //    // We return the interface, so that
            //    IExcelDataReader reader = null;
            //    if (files[0].FileName.EndsWith(".xls"))
            //        reader = ExcelReaderFactory.CreateBinaryReader(stream);
            //    else if (files[0].FileName.EndsWith(".xlsx"))
            //        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //    else
            //        return Json("ERROR: This file format is not supported", JsonRequestBehavior.AllowGet);

            //    //reader.IsFirstRowAsColumnNames = true;
            //    System.Data.DataSet result = reader.AsDataSet();
            //    reader.Close();
            //    foreach (System.Data.DataRow row in result.Tables[0].Rows)
            //    {
            //        var r1 = row[1];
            //        var r2 = r1;
            //        var r3 = r2;
            //    }
            //}
            //else
            //{
            //    ModelState.AddModelError("File", "Please Upload Your file");
            //}
            #endregion

            //string message;
            if (files != null && files.Count > 0 && files[0].ContentLength > 0 && files[0].ContentLength < (10 * 1024 * 1024))
            {
                Services.NPOIServices n = new Services.NPOIServices();
                Models.Interfaces.IResultOk ok = null;
                n.InserSinhVienExcel(files, ex => { ok = ex; });
                if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
                return Json("OK", JsonRequestBehavior.AllowGet);
                //string filetype = file.FileName.Split('.').Last();
                //string fileName = Path.GetFileName(file.FileName);
                //string path = Path.Combine(Server.MapPath("~/Content/Imports"), fileName);
                //HSSFWorkbook excel = null;
                //if (filetype == "xls")
                //{
                //    //files[0].SaveAs(path);
                //    // System.IO.MemoryStream ms = new MemoryStream(files[0].InputStream);
                //    //FileStream fs = new FileStream(ms);
                //    var ss = files[0].InputStream.ReadByte();
                //    var s1 = ss;
                //    var s2 = s1;
                //    excel = new HSSFWorkbook(files[0].InputStream);
                //    HSSFSheet ws = (HSSFSheet)excel.GetSheetAt(0);
                //    int startRow = 3;
                //    for (int i = startRow; i < ws.LastRowNum; i++)
                //    {
                //        var v1 = ws.GetRow(startRow).GetCell(1).StringCellValue;
                //        var v2 = v1;
                //        var v3 = v2;
                //    }
                //    //message = NPServices.InsertData_E(excel);
                //}
                //else if (filetype == "xlsx")
                //{
                //    NPOI.XSSF.UserModel.XSSFWorkbook excel1 = new NPOI.XSSF.UserModel.XSSFWorkbook(files[0].InputStream);
                //    NPOI.SS.UserModel.ISheet ws = (NPOI.SS.UserModel.ISheet)excel1.GetSheetAt(0);
                //    int startRow = 3;
                //    for (int i = startRow; i < ws.LastRowNum; i++)
                //    {
                //        var v1 = ws.GetRow(startRow).GetCell(1).StringCellValue;
                //        var v2 = v1;
                //        var v3 = v2;
                //    }
                //}
                //else
                //{
                //    message = "File format error !";
                //}
            }
            else
            {
                //message = "Please select file import !";
            }

            return Json("ERROR: Please Upload Your file!", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region NHÂN VIÊN

        public ActionResult NhanVien()
        {
            ViewBag.Title = "Tổ chức, Nhân viên";
            return View();
        }

        [HttpPost]
        public JsonResult NhanVienChangeDep(string MaNV, string DepId, string ChangedDate)
        {
            DateTime dtTemp;
            if (!DateTime.TryParse(ChangedDate, out dtTemp))
                return Json("ERROR: Định dạng ngày không hợp lệ!", JsonRequestBehavior.AllowGet);
            Models.Interfaces.INhanVienChangeDept changeDept = new Models.clsNhanVienChangeDept();
            Models.Interfaces.IResultOk ok = null;
            changeDept.Change(MaNV, DepId, dtTemp, ex => { ok = ex; });
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string NhanVienGetByDep(int selectedPage, int pageSize, string DepId, string TenNV)
        {
            Exception _ex = null;
            int _totalPages = 0, _totalRecords = 0;
            Models.clsNhanVien _nhanvien = new Models.clsNhanVien();
            List<Entity.Interfaces.INhanVienInfo> Items = new List<Entity.Interfaces.INhanVienInfo>();
            _nhanvien.GetByDepTenNV(selectedPage, pageSize, DepId, TenNV, out _totalPages, out _totalRecords, (result, ex) =>
            {
                Items = result.ToList();
                _ex = ex;
            });

            //var kq = Items.FindAll(f => !f.IsNghiViec);

            Entity.clsPagingInfo _PagingInfo = new Entity.clsPagingInfo();
            _PagingInfo.Tag = Items;
            //_PagingInfo.Tag = new List<Entity.Interfaces.INhanVienInfo>(kq);
            _PagingInfo.TotalRecords = _totalRecords;
            _PagingInfo.TotalPages = _totalPages;
            if (_ex == null)
                return Newtonsoft.Json.JsonConvert.SerializeObject(_PagingInfo);
            //, new Newtonsoft.Json.JsonSerializerSettings()
            //    {
            //        TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects // Co su dung interface
            //    });

            return "ERROR: " + _ex.Message;
        }


        [HttpPost]
        public string NhanVienXuLy(string type, string json)
        //public ActionResult NhanVienXuLy(string type, Entity.clsNhanVienInfo item)
        {//type: POST,PUT,DELETE


            Models.clsNhanVien _clsNhanvien = new Models.clsNhanVien();
            Models.Interfaces.IResultOk result = null;
            Entity.clsNhanVienInfo Item = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsNhanVienInfo>(json,
                new Newtonsoft.Json.JsonSerializerSettings() { DateFormatString = "yyyy/MM/dd" });

            if (type == "PUT")//Cap nhat
            {
                _clsNhanvien.Update(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "POST")//Them moi
            {
                _clsNhanvien.Insert(Item, ok =>
                {
                    result = ok;
                });
            }
            else if (type == "DELETE")
            {
                _clsNhanvien.DeleteByKey(Item.Id, ok =>
                {
                    result = ok;
                });
            }
            if (result == null) return "";

            if (result.Success) return "true";

            return "ERROR: " + result.Message;
            //return Json(new { result = "Cap nhat" });

        }

        // [ValidateAntiForgeryToken]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ImportE(HttpPostedFileBase file)
        {

            //HttpFileCollectionBase filessss = Request.Files;
            //var s = filessss;
            //var d = s;
            //var c = d;
            //if (file != null && file.ContentLength > 0)
            //{
            //    Stream stream = file.InputStream;

            //}
            HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;
            #region Cach 1
            //if (files != null && files.Count > 0)
            //{
            //    // ExcelDataReader works with the binary Excel file, so it needs a FileStream
            //    // to get started. This is how we avoid dependencies on ACE or Interop:
            //    Stream stream = files[0].InputStream;

            //    // We return the interface, so that
            //    IExcelDataReader reader = null;
            //    if (files[0].FileName.EndsWith(".xls"))
            //        reader = ExcelReaderFactory.CreateBinaryReader(stream);
            //    else if (files[0].FileName.EndsWith(".xlsx"))
            //        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //    else
            //        return Json("ERROR: This file format is not supported", JsonRequestBehavior.AllowGet);

            //    //reader.IsFirstRowAsColumnNames = true;
            //    System.Data.DataSet result = reader.AsDataSet();
            //    reader.Close();
            //    foreach (System.Data.DataRow row in result.Tables[0].Rows)
            //    {
            //        var r1 = row[1];
            //        var r2 = r1;
            //        var r3 = r2;
            //    }
            //}
            //else
            //{
            //    ModelState.AddModelError("File", "Please Upload Your file");
            //}
            #endregion

            //string message;
            if (files != null && files.Count > 0 && files[0].ContentLength > 0 && files[0].ContentLength < (10 * 1024 * 1024))
            {
                Services.NPOIServices n = new Services.NPOIServices();
                Models.Interfaces.IResultOk ok = null;
                n.InsertDataExcel(files, ex => { ok = ex; });
                if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
                return Json("OK", JsonRequestBehavior.AllowGet);
                //string filetype = file.FileName.Split('.').Last();
                //string fileName = Path.GetFileName(file.FileName);
                //string path = Path.Combine(Server.MapPath("~/Content/Imports"), fileName);
                //HSSFWorkbook excel = null;
                //if (filetype == "xls")
                //{
                //    //files[0].SaveAs(path);
                //    // System.IO.MemoryStream ms = new MemoryStream(files[0].InputStream);
                //    //FileStream fs = new FileStream(ms);
                //    var ss = files[0].InputStream.ReadByte();
                //    var s1 = ss;
                //    var s2 = s1;
                //    excel = new HSSFWorkbook(files[0].InputStream);
                //    HSSFSheet ws = (HSSFSheet)excel.GetSheetAt(0);
                //    int startRow = 3;
                //    for (int i = startRow; i < ws.LastRowNum; i++)
                //    {
                //        var v1 = ws.GetRow(startRow).GetCell(1).StringCellValue;
                //        var v2 = v1;
                //        var v3 = v2;
                //    }
                //    //message = NPServices.InsertData_E(excel);
                //}
                //else if (filetype == "xlsx")
                //{
                //    NPOI.XSSF.UserModel.XSSFWorkbook excel1 = new NPOI.XSSF.UserModel.XSSFWorkbook(files[0].InputStream);
                //    NPOI.SS.UserModel.ISheet ws = (NPOI.SS.UserModel.ISheet)excel1.GetSheetAt(0);
                //    int startRow = 3;
                //    for (int i = startRow; i < ws.LastRowNum; i++)
                //    {
                //        var v1 = ws.GetRow(startRow).GetCell(1).StringCellValue;
                //        var v2 = v1;
                //        var v3 = v2;
                //    }
                //}
                //else
                //{
                //    message = "File format error !";
                //}
            }
            else
            {
                //message = "Please select file import !";
            }

            return Json("ERROR: Please Upload Your file!", JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public FileResult TemplateNhanVien()
        //{
        //    try
        //    {
        //        if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\nhanvien.xlsx")))
        //        {
        //            TempData["Message"] = "Tập tin \"nhanvien.xlsx\" không tồn tại.";
        //            return null;
        //        }
        //        // Opening the Excel template...
        //        FileStream fs =
        //            new FileStream(Server.MapPath(@"\Content\Templates\nhanvien.xlsx"), FileMode.Open, FileAccess.Read);

        //        // Getting the complete workbook...
        //        /*** Excel 2003 *.xls ***/
        //        //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
        //        /*** Excel 2007 or later *.xlsx ***/
        //        NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);

        //        MemoryStream ms = new MemoryStream();

        //        // Writing the workbook content to the FileStream...
        //        templateWorkbook.Write(ms);

        //        TempData["Message"] = "Excel report created successfully!";
        //        fs.Close();

        //        // Sending the server processed data back to the user computer...
        //        return File(ms.ToArray(), "application/vnd.ms-excel", "nhanvien.xlsx");
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["Message"] = "ERROR: " + ex.Message;
        //        return null;
        //    }
        //}

        [HttpPost]
        public FileResult TemplateNhanVien()
        {
            byte[] allFile = System.IO.File.ReadAllBytes(Server.MapPath(@"\Content\Templates\nhanvien.xlsx"));
            return File(allFile, System.Net.Mime.MediaTypeNames.Application.Octet, "template_nhanvien.xlsx");
        }
        #endregion

        #region DANH SÁCH NHÂN VIÊN ĐÃ BỊ XÓA TẠM THỜI
        public ActionResult NhanVienDaXoa()
        {
            ViewBag.Title = "Danh sách nhân viên đã xóa tạm";
            return View();
        }
        public JsonResult NhanVienDaXoaGet()
        {
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.INhanVienInfo> Items = null;
            Models.Interfaces.INhanVienDaXoa NhanVien = new Models.clsNhanVienDaXoa();
            NhanVien.GetAll((result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult NhanVienDaXoaModify(string action, string MaNV)
        {
            Models.Interfaces.IResultOk ok = null;
            Models.Interfaces.INhanVienDaXoa NhanVien = new Models.clsNhanVienDaXoa();
            if (action == "PUT")
                NhanVien.Modifiy(Libs.ApiAction.PUT, MaNV, ex => { ok = ex; });
            else if (action == "DELETE")
                NhanVien.Modifiy(Libs.ApiAction.DELETE, MaNV, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region DANH SÁCH NHÂN VIÊN KHÔNG LÊN CÔNG ẢO
        public ActionResult NhanVienException()
        {
            ViewBag.Title = "Danh sách nhân viên không lên công ảo";
            return View();
        }
        public JsonResult NhanVienExceptionGet()
        {
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.INhanVienInfo> Items = null;
            Models.Interfaces.INhanVienException NhanVien = new Models.clsNhanVienException();
            NhanVien.GetAll((result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NhanVienExceptionModify(string action, string MaNV)
        {
            Models.Interfaces.IResultOk ok = null;
            Models.Interfaces.INhanVienException NhanVien = new Models.clsNhanVienException();
            if (action == "PUT")
                NhanVien.Modifiy(Libs.ApiAction.PUT, MaNV, ex => { ok = ex; });
            else if (action == "DELETE")
                NhanVien.Modifiy(Libs.ApiAction.DELETE, MaNV, ex => { ok = ex; });
            else if (action == "POST")
                NhanVien.Modifiy(Libs.ApiAction.POST, MaNV, ex => { ok = ex; });

            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ĐĂNG KÝ CHO NHÂN VIÊN NGHỈ VIỆC
        public ActionResult NhanVienNghi()
        {
            ViewBag.Title = "Danh sách nhân viên đăng ký nghỉ";
            return View();
        }
        public JsonResult NhanVienNghiGet(string value, string FromDate, string ToDate)
        {
            DateTime _fromDate = Libs.clsCommon.ParseExactDMY(FromDate),
                    _toDate = Libs.clsCommon.ParseExactDMY(ToDate);

            //DateTime _fromDate, _toDate;
            //if (!DateTime.TryParse(FromDate, out _fromDate))
            //    _fromDate = DateTime.Now;
            //if (!DateTime.TryParse(ToDate, out _toDate))
            //    _toDate = DateTime.Now;
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.INhanVienInfo> Items = null;
            Models.Interfaces.INhanVienNghiViec NhanVien = new Models.clsNhanVienNghiViec();
            NhanVien.GetAll(value, _fromDate, _toDate, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NhanVienChoNghiViec(string MaNV, string LyDo, string NgayNghi)
        {
            DateTime StopWorkingday = Libs.clsCommon.ParseExactDMY(NgayNghi);
            //if (!DateTime.TryParse(NgayNghi, out StopWorkingday))
            //    StopWorkingday = DateTime.Now;
            Models.Interfaces.IResultOk ok = null;
            Models.Interfaces.INhanVienNghiViec NhanVien = new Models.clsNhanVienNghiViec();
            NhanVien.ChoNghiViec(Libs.ApiAction.POST, MaNV, LyDo, StopWorkingday, ex => { ok = ex; });

            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult NhanVienChoNghRestore(string MaNV)
        {

            Models.Interfaces.IResultOk ok = null;
            Models.Interfaces.INhanVienNghiViec NhanVien = new Models.clsNhanVienNghiViec();
            NhanVien.PhucHoi(Libs.ApiAction.POST, MaNV, ex => { ok = ex; });

            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ VẮNG
        public ActionResult DangKyVang()
        {
            ViewBag.Title = "Đăng Ký Vắng";
            List<ERP.Entity.KeyValueItem> Items = new List<Entity.KeyValueItem>();
            Items.Add(new Entity.KeyValueItem() { Key = "TEST1", EventName = "Demo();" });
            Items.Add(new Entity.KeyValueItem() { Key = "TEST2", EventName = "Demo1();" });
            Session["Temps"] = Items;
            //Session.Abandon();
            //Session.Clear();
            //Session.RemoveAll();  
            return View();
        }

        public JsonResult DangKyVangGetBy(int Month, int Year, string DepId, string MaNV)
        {
            Models.DIC.Interfaces.IDangKyVang DangKyVang = new Models.DIC.clsDangKyVang();
            List<Entity.Interfaces.IDangKyVangInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.GetBy(Month, Year, DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyVangSave(int Month, int Year, string items)
        {
            var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyVangInfo>>(items, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            Models.DIC.Interfaces.IDangKyVang DangKyVang = new Models.DIC.clsDangKyVang();
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.DangKyVang(Month, Year, new List<Entity.Interfaces.IDangKyVangInfo>(Items), ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyVangSaveFrom(int eID, string fromDateDMY, string toDateDMY, string MaLyDo)
        {
            DateTime date = DateTime.ParseExact(fromDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            DateTime fromDate, toDate;
            //if (!DateTime.TryParse(fromDateDMY, out fromDate))
            fromDate = DateTime.ParseExact(fromDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //if (!DateTime.TryParse(toDateDMY, out toDate))
            toDate = DateTime.ParseExact(toDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (toDate.Month < fromDate.Month)
            {
                return Json("ERROR: ", JsonRequestBehavior.AllowGet);
            }

            List<Entity.clsDangKyVangInfo> mItems = new List<Entity.clsDangKyVangInfo>();
            int mTotalDay = (int)(toDate - fromDate).TotalDays;


            if (fromDate.Month == toDate.Month)
            {
                Entity.clsDangKyVangInfo mItem = new Entity.clsDangKyVangInfo();
                mItem.eID = eID.ToString();
                mItem.sYear = fromDate.Year;
                mItem.sMonth = fromDate.Month;
                for (int i = 0; i <= mTotalDay; i++)
                {
                    DateTime dtNow = fromDate.AddDays(i);
                    if (dtNow.DayOfWeek == DayOfWeek.Sunday) continue;

                    mItem.SetValue(dtNow.Day, MaLyDo);
                }

                mItems.Add(mItem);
            }
            else
            {
                int countMonth = toDate.Month - fromDate.Month;
                for (int m = 0; m <= countMonth; m++)
                {
                    DateTime firstDayOfMonth;
                    //Chạy lần đầu tiên lấy ngày đầu tiên của tháng bắt đầu từ "fromDate"
                    if (m == 0)
                        firstDayOfMonth = fromDate;
                    else
                    {
                        firstDayOfMonth = fromDate.AddMonths(m);
                        //Lấy ngày đầu tiên của tháng
                        firstDayOfMonth = Libs.clsCommon.FirstDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
                    }
                    //Lấy ngày cuối cùng của tháng
                    DateTime lasDayOfMonth = Libs.clsCommon.LastDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
                    //Nếu chạy là tháng cuối cùng thì lấy ngày cuối tháng là "toDate"
                    if (m == countMonth)
                        lasDayOfMonth = toDate;

                    mTotalDay = (int)(lasDayOfMonth - firstDayOfMonth).TotalDays;

                    Entity.clsDangKyVangInfo mItem = new Entity.clsDangKyVangInfo();
                    mItem.eID = eID.ToString();
                    mItem.sYear = firstDayOfMonth.Year;
                    mItem.sMonth = firstDayOfMonth.Month;

                    for (int i = 0; i <= mTotalDay; i++)
                    {
                        DateTime dtNow = fromDate.AddDays(i);
                        if (dtNow.DayOfWeek == DayOfWeek.Sunday) continue;
                        mItem.SetValue(dtNow.Day, MaLyDo);
                    }

                    mItems.Add(mItem);
                }
            }

            //var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyVangInfo>>(items, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            Models.DIC.Interfaces.IDangKyVang DangKyVang = new Models.DIC.clsDangKyVang();
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.DangKyVang(new List<Entity.Interfaces.IDangKyVangInfo>(mItems), ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyVangSaveFrom2(int eID, string fromDateDMY, string toDateDMY, string MaLyDo, string items)
        {
            var Temps = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyVangInfo>>(items, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            //DateTime date = DateTime.ParseExact(fromDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            DateTime fromDate, toDate;
            //if (!DateTime.TryParse(fromDateDMY, out fromDate))
            fromDate = DateTime.ParseExact(fromDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //if (!DateTime.TryParse(toDateDMY, out toDate))
            toDate = DateTime.ParseExact(toDateDMY, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (DateTime.Compare(fromDate, toDate) > 0)
            {
                return Json("ERROR: Từ ngày không được lớn hơn Đến ngày", JsonRequestBehavior.AllowGet);
            }

            List<Entity.clsDangKyVangInfo> mItems = new List<Entity.clsDangKyVangInfo>();

            // nếu trùng năm
            if (fromDate.Year == toDate.Year)
            {
                mItems = GetItemDangKy(fromDate, toDate, eID.ToString(), MaLyDo);
            }
            else
            {
                /************ nếu khác năm ***********/

                DateTime _fromDate, _toDate;

                /************ 1. tính trong năm fromdate trước ***********/
                _toDate = DateTime.Parse(fromDate.Year.ToString() + "/12/31");
                mItems = GetItemDangKy(fromDate, _toDate, eID.ToString(), MaLyDo);

                _fromDate = DateTime.Parse(toDate.Year.ToString() + "/01/01");
                mItems.AddRange(GetItemDangKy(_fromDate, toDate, eID.ToString(), MaLyDo));
            }

            #region Cũ

            //if (fromDate.Month == toDate.Month)
            //{
            //    Entity.clsDangKyVangInfo mItem = Temps[0];
            //    mItem.eID = eID.ToString();
            //    mItem.sYear = fromDate.Year;
            //    mItem.sMonth = fromDate.Month;
            //    for (int i = 0; i <= mTotalDay; i++)
            //    {
            //        DateTime dtNow = fromDate.AddDays(i);
            //        if (dtNow.DayOfWeek == DayOfWeek.Sunday) continue;

            //        mItem.SetValue(dtNow.Day, MaLyDo);
            //    }

            //    mItems.Add(mItem);
            //}
            //else
            //{
            //    int countMonth = toDate.Month - fromDate.Month;
            //    for (int m = 0; m <= countMonth; m++)
            //    {
            //        DateTime firstDayOfMonth;
            //        //Chạy lần đầu tiên lấy ngày đầu tiên của tháng bắt đầu từ "fromDate"
            //        if (m == 0)
            //            firstDayOfMonth = fromDate;
            //        else
            //        {
            //            firstDayOfMonth = fromDate.AddMonths(m);
            //            //Lấy ngày đầu tiên của tháng
            //            firstDayOfMonth = Libs.clsCommon.FirstDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
            //        }
            //        //Lấy ngày cuối cùng của tháng
            //        DateTime lasDayOfMonth = Libs.clsCommon.LastDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
            //        //Nếu chạy là tháng cuối cùng thì lấy ngày cuối tháng là "toDate"
            //        if (m == countMonth)
            //            lasDayOfMonth = toDate;

            //        mTotalDay = (int)(lasDayOfMonth - firstDayOfMonth).TotalDays;

            //        Entity.clsDangKyVangInfo mItem = Temps[0];
            //        mItem.eID = eID.ToString();
            //        mItem.sYear = firstDayOfMonth.Year;
            //        mItem.sMonth = firstDayOfMonth.Month;

            //        for (int i = 0; i <= mTotalDay; i++)
            //        {
            //            DateTime dtNow = fromDate.AddDays(i);
            //            if (dtNow.DayOfWeek == DayOfWeek.Sunday) continue;
            //            mItem.SetValue(dtNow.Day, MaLyDo);
            //        }

            //        mItems.Add(mItem);
            //    }
            //}

            #endregion

            //var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyVangInfo>>(items, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            Models.DIC.Interfaces.IDangKyVang DangKyVang = new Models.DIC.clsDangKyVang();
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.DangKyVang(new List<Entity.Interfaces.IDangKyVangInfo>(mItems), ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        private List<Entity.clsDangKyVangInfo> GetItemDangKy(DateTime fromDate, DateTime toDate, string eID,
            string MaLyDo)
        {
            List<Entity.clsDangKyVangInfo> mItems = new List<Entity.clsDangKyVangInfo>();
            int countMonth = toDate.Month - fromDate.Month;
            int mTotalDay = (int)(toDate - fromDate).TotalDays;
            for (int m = 0; m <= countMonth; m++)
            {
                Entity.clsDangKyVangInfo mItem = new Entity.clsDangKyVangInfo();
                DateTime firstDayOfMonth;
                firstDayOfMonth = fromDate.AddMonths(m);
                //Lấy ngày đầu tiên của tháng
                if (m != 0)
                {
                    firstDayOfMonth = Libs.clsCommon.FirstDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
                }

                //Lấy ngày cuối cùng của tháng
                DateTime lasDayOfMonth = Libs.clsCommon.LastDayOfMonth(firstDayOfMonth.Month, firstDayOfMonth.Year);
                //Nếu chạy là tháng cuối cùng thì lấy ngày cuối tháng là "toDate"
                if (m == countMonth)
                    lasDayOfMonth = toDate;

                mTotalDay = (int)(lasDayOfMonth - firstDayOfMonth).TotalDays;

                mItem.eID = eID.ToString();
                mItem.sYear = firstDayOfMonth.Year;
                mItem.sMonth = firstDayOfMonth.Month;

                for (int i = 0; i <= mTotalDay; i++)
                {
                    DateTime dtNow = fromDate.AddDays(i);
                    if (m != 0)
                    {
                        dtNow = firstDayOfMonth.AddDays(i);
                    }
                    if (dtNow.DayOfWeek == DayOfWeek.Sunday) continue;
                    mItem.SetValue(dtNow.Day, MaLyDo);
                }

                mItems.Add(mItem);
            }
            return mItems;
        }

        #endregion

        #region LÝ DO VẮNG

        public ActionResult LyDoVang()
        {
            ViewBag.Title = "Khai báo lý do vắng";
            return View();
        }

        public JsonResult LyDoVangGetBy()
        {
            Models.DIC.Interfaces.ILyDoVang mObj = new Models.DIC.clsLyDoVang();
            IEnumerable<Entity.Interfaces.ILyDoVangInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            mObj.GetBy((result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            if (!ok.Success)
                return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items.ToList(), JsonRequestBehavior.AllowGet);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(Items.ToList(), new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects });
        }

        public JsonResult LyDoVangModify(string action, string form)
        {
            //var mForm = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(form, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });

            var Temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsLyDoVangInfo>(form, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });

            Libs.ApiAction mAction = Libs.ApiAction.GET;
            if (action == "PUT")
                mAction = Libs.ApiAction.PUT;
            else if (action == "POST")
                mAction = Libs.ApiAction.POST;
            else if (action == "DELETE")
                mAction = Libs.ApiAction.DELETE;

            Models.DIC.Interfaces.ILyDoVang mObj = new Models.DIC.clsLyDoVang();
            Models.Interfaces.IResultOk ok = null;
            mObj.Modify(mAction, Temp, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult JDataComboBoxLyDoVang()
        {
            JData.DataComboBoxInfo[] temps = null;
            Models.DIC.Interfaces.ILyDoVang Lydo = new Models.DIC.clsLyDoVang();
            Models.Interfaces.IResultOk ok = null;
            Lydo.GetLyDo((result, ex) => { temps = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(temps, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ CHẶN GIỜ
        public ActionResult DangKyChanGio()
        {
            ViewBag.Title = "Đăng Ký Vắng";
            List<ERP.Entity.KeyValueItem> Items = new List<Entity.KeyValueItem>();
            Items.Add(new Entity.KeyValueItem() { Key = "TEST1", EventName = "Demo();" });
            Items.Add(new Entity.KeyValueItem() { Key = "TEST2", EventName = "Demo1();" });
            Session["Temps"] = Items;
            return View();
        }

        public JsonResult DangKyChatGioGetBy(int Month, int Year, string DepId, string MaNV)
        {
            Models.DIC.Interfaces.IDangKyChanGio DangKyVang = new Models.DIC.clsDangKyChanGio();
            List<Entity.Interfaces.IDangKyChanGioInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.GetBy(Month, Year, DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyChanGioSave(string Action, string form, string items)
        {
            var mForm = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(form, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });
            int type = Libs.Utilities.clsFormat.IntConvert(mForm["TypeGroup"]);
            bool IsChuNhat = Libs.Utilities.clsFormat.BooleanConvert(mForm["IsChuNhat"]);
            int mSoPhut = Libs.Utilities.clsFormat.IntConvert(mForm["SoPhut"]);
            string GioVe = mForm["GioVe"].ToString(), tn = mForm["TuNgay"].ToString(), dn = mForm["ToiNgay"].ToString();
            DateTime mTuNgay = Libs.clsCommon.ParseExactDMY(tn),
                mDenNgay = Libs.clsCommon.ParseExactDMY(dn);
            if (Action == "DELETE")
            {
                GioVe = ""; mSoPhut = 0;
                IsChuNhat = true;
            }
            //NtbSoft.ERP.Models.DIC.clsDangKyChanGioNgayType mClsType = new Models.DIC.clsDangKyChanGioNgayType();
            //System.Data.DataTable mTable = mClsType.TYPE_DANGKY_CHANGIO;
            NtbSoft.ERP.Models.DIC.clsDangKyChanGioType mClsType = new Models.DIC.clsDangKyChanGioType();
            System.Data.DataTable mTable = mClsType.TYPE_DANGKY_CHANGIO;
            List<Entity.clsDangKyChanGioInfo> mArrayEmpID = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyChanGioInfo>>(items);
            while (mArrayEmpID.Count > 0)
            {
                if (Action == "DELETE")
                {
                    GioVe = ""; mSoPhut = 0;
                    mArrayEmpID[0].SetValueAll("");
                }
                if (type == 1)
                { //Cap nhat 1 nhan vien
                    var temp = mArrayEmpID.Find(f => f.eID == mForm["eID"].ToString());
                    if (temp == null) break;
                    if (Action == "DELETE") temp.SetValueAll("");
                    DangKyChanGioCalc(temp, mTuNgay, mDenNgay,
                             GioVe, mSoPhut, type, IsChuNhat, ref mTable);
                    break;
                }
                else
                {
                    DangKyChanGioCalc(mArrayEmpID[0], mTuNgay, mDenNgay,
                            GioVe, mSoPhut, type, IsChuNhat, ref mTable);

                    mArrayEmpID.Remove(mArrayEmpID[0]);
                }
            }

            Models.DIC.Interfaces.IDangKyChanGio DangKyVang = new Models.DIC.clsDangKyChanGio();
            Models.Interfaces.IResultOk ok = null;
            DangKyVang.DangKy(mTable, ex => { ok = ex; });

            return Json("", JsonRequestBehavior.AllowGet);
        }

        private int DangKyChanGioNgayCalc(string eID, DateTime fromDate, DateTime toDate, string GioVe, int SoPhut, int type, bool IsChuNhat, ref System.Data.DataTable TYPE_DANGKY_CHANGIO_NGAY)
        {
            try
            {
                List<Entity.clsDangKyChanGioInfo> mItems = new List<Entity.clsDangKyChanGioInfo>();
                NtbSoft.ERP.Models.DIC.clsDangKyChanGioType mClsType = new Models.DIC.clsDangKyChanGioType();

                while (fromDate <= toDate)
                {
                    int mSoPhut = 0;
                    string mGioVe = "";
                    //Mặc định nếu ngày là chủ nhật, không thiết lập số phút
                    if (fromDate.DayOfWeek != DayOfWeek.Sunday && !IsChuNhat)
                    {
                        mSoPhut = SoPhut;
                        mGioVe = GioVe;
                    }

                    System.Data.DataRow newRow = TYPE_DANGKY_CHANGIO_NGAY.NewRow();
                    newRow["AutoID"] = 0;
                    newRow["NgayChan"] = fromDate;
                    newRow["eID"] = eID;
                    newRow["GioVe"] = GioVe;
                    newRow["SoPhut"] = mSoPhut;
                    TYPE_DANGKY_CHANGIO_NGAY.Rows.Add(newRow);

                    //Tang so ngay len 1
                    fromDate = fromDate.AddDays(1);
                }
                while (mItems.Count > 0)
                {
                    mClsType.SetValue(mItems[0], ref TYPE_DANGKY_CHANGIO_NGAY);
                    mItems.Remove(mItems[0]);
                }

            }
            catch (Exception ex)
            {

            }
            //if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return 0;
        }

        private int DangKyChanGioCalc(Entity.clsDangKyChanGioInfo item, DateTime fromDate, DateTime toDate, string GioVe, int SoPhut, int type, bool IsChuNhat, ref System.Data.DataTable TYPE_DANGKY_CHANGIO)
        {
            try
            {
                if (item.sMonth == 0) item.sMonth = fromDate.Month;
                if (item.sYear == 0) item.sYear = fromDate.Year;
                List<Entity.clsDangKyChanGioInfo> mItems = new List<Entity.clsDangKyChanGioInfo>();
                mItems.Add(item);
                NtbSoft.ERP.Models.DIC.clsDangKyChanGioType mClsType = new Models.DIC.clsDangKyChanGioType();

                while (fromDate <= toDate)
                {
                    int mSoPhut = 0;
                    string mGioVe = "";
                    //Mặc định nếu ngày là chủ nhật, không thiết lập số phút
                    if (fromDate.DayOfWeek != DayOfWeek.Sunday && !IsChuNhat)
                    {
                        mSoPhut = SoPhut;
                        mGioVe = GioVe;
                    }

                    var mItem = mItems.Find(f => f.eID == item.eID && f.sMonth == fromDate.Month && f.sYear == fromDate.Year);
                    if (mItem == null)
                    {
                        mItem = new Entity.clsDangKyChanGioInfo()
                        {
                            eID = item.eID,
                            sYear = fromDate.Year,
                            sMonth = fromDate.Month
                        };
                        mItems.Add(mItem);
                    }

                    mItem.SetValue(fromDate.Day, mGioVe);
                    //mItem.SetValue(fromDate.Day, mSoPhut == 0 ? "" : mSoPhut.ToString());
                    //mItem.GioVe = GioVe;

                    //Tang so ngay len 1
                    fromDate = fromDate.AddDays(1);
                }
                while (mItems.Count > 0)
                {
                    mClsType.SetValue(mItems[0], ref TYPE_DANGKY_CHANGIO);
                    mItems.Remove(mItems[0]);
                }

            }
            catch (Exception ex)
            {

            }
            //if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return 0;
        }

        private Task<int> DangKyVanAsync(string Action, string eID, string fromDate, string toDate, string GioVe, int SoPhut, int type, bool IsChuNhat)
        {
            DateTime mFromDate = Libs.clsCommon.ParseExactDMY(fromDate), mToDate = Libs.clsCommon.ParseExactDMY(toDate);
            return Task.Factory.StartNew(() => DangKyChanGioToSQL(eID, mFromDate, mToDate, GioVe, SoPhut, type, IsChuNhat));
        }

        private int DangKyChanGioToSQL(string eID, DateTime fromDate, DateTime toDate, string GioVe, int SoPhut, int type, bool IsChuNhat)
        {
            try
            {
                List<Entity.clsDangKyChanGioInfo> mItems = new List<Entity.clsDangKyChanGioInfo>();
                NtbSoft.ERP.Models.DIC.clsDangKyChanGioType mClsType = new Models.DIC.clsDangKyChanGioType();
                System.Data.DataTable mTable = mClsType.TYPE_DANGKY_CHANGIO;
                while (fromDate <= toDate)
                {
                    //Mặc định nếu ngày là chủ nhật, không thiết lập số phút
                    if (fromDate.DayOfWeek == DayOfWeek.Sunday && !IsChuNhat)
                    {
                        SoPhut = 0;
                    }
                    var mItem = mItems.Find(f => f.eID == eID && f.sMonth == fromDate.Month && f.sYear == fromDate.Year);
                    if (mItem == null)
                    {
                        mItem = new Entity.clsDangKyChanGioInfo()
                        {
                            eID = eID,
                            sYear = fromDate.Year,
                            sMonth = fromDate.Month
                        };
                        mItems.Add(mItem);
                    }
                    mItem.SetValue(fromDate.Day, SoPhut == 0 ? "" : SoPhut.ToString());
                    mItem.GioVe = GioVe;

                    //Tang so ngay len 1
                    fromDate = fromDate.AddDays(1);
                }
                while (mItems.Count > 0)
                {
                    mClsType.SetValue(mItems[0], ref mTable);
                    mItems.Remove(mItems[0]);
                }
                System.Threading.Thread.Sleep(100);
                Models.DIC.Interfaces.IDangKyChanGio DangKyVang = new Models.DIC.clsDangKyChanGio();
                Models.Interfaces.IResultOk ok = null;
                DangKyVang.DangKy(mTable, ex => { ok = ex; });
            }
            catch (Exception ex)
            {

            }
            //if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return 0;
        }

        #endregion

        #region ĐĂNG KÝ THAI SẢN
        public ActionResult DangKyThaiSan()
        {

            ViewBag.Title = "Đăng ký thai sản";
            return View();
        }

        public JsonResult DangKyThaiSanGet(string DepId, string MaNV)
        {
            Models.DIC.Interfaces.IDangKyCheDo dangKyTS = new Models.DIC.clsDangKyThaiSan();
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.IDangKyCheDoInfo> Items = null;
            dangKyTS.GetAll(DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyThaiSanUpdate(string action, string item)
        {
            Models.DIC.Interfaces.IDangKyCheDo dangKyTS = new Models.DIC.clsDangKyThaiSan();
            Models.Interfaces.IResultOk ok = null;
            var Temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsDangKyThaiSanInfo>(item, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });
            if (string.IsNullOrEmpty(Temp.StrTuNgayDMY))
                Temp.TuNgay = DateTime.Now;
            if (string.IsNullOrEmpty(Temp.StrToiNgayDMY))
                Temp.ToiNgay = DateTime.Now;

            if (action == "PUT")
                dangKyTS.Update(Libs.ApiAction.PUT, Temp, ex => { ok = ex; });
            else if (action == "POST")
                dangKyTS.Update(Libs.ApiAction.POST, Temp, ex => { ok = ex; });
            else if (action == "DELETE")
                dangKyTS.Update(Libs.ApiAction.DELETE, Temp, ex => { ok = ex; });

            if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);


            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ CHẾ ĐỘ
        public ActionResult DangKyCheDo()
        {
            ViewBag.Title = "Đăng ký chế độ";
            return View();

        }

        public JsonResult DangKyCheDoGet(string DepId, string MaNV)
        {
            Models.DIC.Interfaces.IDangKyCheDo dangKy = new Models.DIC.clsDangKyCheDo();
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.IDangKyCheDoInfo> Items = null;
            dangKy.GetAll(DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyCheDoUpdate(string action, string item)
        {
            Models.DIC.Interfaces.IDangKyCheDo dangKy = new Models.DIC.clsDangKyCheDo();
            Models.Interfaces.IResultOk ok = null;
            var form = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(item, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });
            var Temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Entity.clsDangKyCheDoInfo>(item, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });
            //if (string.IsNullOrEmpty(Temp.StrTuNgayDMY))
            Temp.TuNgay = Libs.clsCommon.ParseExactDMY(Convert.ToString(form["StrTuNgayDMY"]));
            //if (string.IsNullOrEmpty(Temp.StrToiNgayDMY))
            Temp.ToiNgay = Libs.clsCommon.ParseExactDMY(Convert.ToString(form["StrToiNgayDMY"]));

            if (action == "PUT")
                dangKy.Update(Libs.ApiAction.PUT, Temp, ex => { ok = ex; });
            else if (action == "POST")
                dangKy.Update(Libs.ApiAction.POST, Temp, ex => { ok = ex; });
            else if (action == "DELETE")
                dangKy.Update(Libs.ApiAction.DELETE, Temp, ex => { ok = ex; });

            if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);


            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ ĐI MUỘN VỀ SỚM
        public ActionResult DangKyDiMuonVeSom()
        {
            ViewBag.Title = "Đăng ký đi muộn về sớm";
            return View();
        }

        public JsonResult DangKyDiMuonVeSomGetBy(int Month, int Year, string DepId, string MaNV)
        {
            Models.DIC.Interfaces.IDangKyDiMuonVeSom DangKy = new Models.DIC.clsDangKyDiMuonVeSom();
            List<Entity.Interfaces.IDangKyDiMuonVeSomInfo> Items = null;
            Models.Interfaces.IResultOk ok = null;
            DangKy.GetBy(Month, Year, DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DangKyDiMuonVeSomSave(int Month, int Year, string items)
        {
            var Items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Entity.clsDangKyDiMuonVeSomInfo>>(items, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
            Models.DIC.Interfaces.IDangKyDiMuonVeSom DangKy = new Models.DIC.clsDangKyDiMuonVeSom();
            Models.Interfaces.IResultOk ok = null;
            DangKy.DangKy(Month, Year, new List<Entity.Interfaces.IDangKyDiMuonVeSomInfo>(Items), ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region NGÀY LỄ
        public ActionResult NgayLe()
        {
            ViewBag.Title = "Ngày lễ";
            return View();
        }
        public JsonResult NgayLeGet()
        {
            Models.DIC.Interfaces.INgayLe ngayle = new Models.DIC.clsNgayLe();
            List<Entity.Interfaces.INgayLeInfo> Items = new List<Entity.Interfaces.INgayLeInfo>();
            Models.Interfaces.IResultOk ok = null;
            ngayle.GetAll((result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult NgayLeSave(string ngay, string name)
        {
            DateTime dt;
            if (!DateTime.TryParse(ngay, out dt)) return Json("ERROR: Định dạng ngày không hợp lệ", JsonRequestBehavior.AllowGet);

            Models.DIC.Interfaces.INgayLe ngayle = new Models.DIC.clsNgayLe();
            Models.Interfaces.IResultOk ok = null;
            ngayle.Save(dt, name, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        public JsonResult NgayLeDelete(int id)
        {
            Models.DIC.Interfaces.INgayLe ngayle = new Models.DIC.clsNgayLe();
            Models.Interfaces.IResultOk ok = null;
            ngayle.Delete(id, ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult NgayLeAddAuto()
        {
            Models.DIC.Interfaces.INgayLe ngayle = new Models.DIC.clsNgayLe();
            Models.Interfaces.IResultOk ok = null;
            ngayle.AddAuto(ex => { ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ LÀM THÊM
        public ActionResult DangKyLamThem()
        {
            ViewBag.Title = "Đăng ký làm thêm";
            return View();
        }

        public JsonResult DangKyLamThemGet(string fromDate, string toDate, string DepId, string MaNV)
        {
            DateTime _TuNgay = Libs.clsCommon.ParseExactDMY(fromDate), _DenNgay = Libs.clsCommon.ParseExactDMY(toDate);
            //if (!DateTime.TryParse(fromDate, out _TuNgay))
            //    _TuNgay = DateTime.Now;
            //if (!DateTime.TryParse(toDate, out _DenNgay))
            //    _DenNgay = DateTime.Now;
            Models.DIC.Interfaces.IDangKyLamThem dangKy = new Models.DIC.clsDangKyLamThem();
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.IDangKyLamThemInfo> Items = null;
            dangKy.GetAll(_TuNgay, _DenNgay, DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DangKyLamThemUpdate(string action, string item)
        {
            Models.DIC.Interfaces.IDangKyLamThem dangKy = new Models.DIC.clsDangKyLamThem();
            Models.Interfaces.IResultOk ok = null;
            var form = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(item, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });

            DateTime _NgayDangKy = Libs.clsCommon.ParseExactDMY(Convert.ToString(form["StrNgayDangKy"]));
            DateTime _denNgay = Libs.clsCommon.ParseExactDMY(Convert.ToString(form["DenNgay"]));

            double value;
            double.TryParse(Convert.ToString(form["ID"]) ?? 0, out value);
            var ArrayNV = form["ArrayNV"] as JToken;
            var Temp = new Entity.clsDangKyLamThemInfo();
            Temp.ID = value;
            Temp.NgayDangKy = _NgayDangKy;
            Temp.DenNgay = _denNgay.ToString(Libs.clsConsts.FormatYMD);

            if (action == "DELETE")
                dangKy.Update(Libs.ApiAction.DELETE, Temp, ex => { ok = ex; });
            else
            {
                Temp.TuGio = form["TuGio"] ?? "";
                Temp.DenGio = form["DenGio"] ?? "";

                TimeSpan tsFrom, tsTo;
                if (!TimeSpan.TryParse(Temp.TuGio, out tsFrom))
                    return Json("ERROR: Định dạng \"Từ giờ\" không đúng", JsonRequestBehavior.AllowGet);
                if (!TimeSpan.TryParse(Temp.DenGio, out tsTo))
                    return Json("ERROR: Định dạng \"Đến giờ\" không đúng", JsonRequestBehavior.AllowGet);
                Temp.SoGio = (decimal)(tsTo.TotalHours - tsFrom.Hours);
                foreach (var item1 in ArrayNV)
                {
                    Temp.MaNV = item1["MaNV"].ToString();
                    if (action == "PUT")
                        dangKy.Update(Libs.ApiAction.PUT, Temp, ex => { ok = ex; });
                    else if (action == "POST")
                        dangKy.Update(Libs.ApiAction.POST, Temp, ex => { ok = ex; });

                    if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
                }
            }
            if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ĐĂNG KÝ RA VÀO
        public ActionResult DangKyRaVao()
        {
            ViewBag.Title = "Đăng ký Ra Vào";
            return View();
        }

        public JsonResult DangKyRaVaoGet(string fromDate, string toDate, string DepId, string MaNV)
        {
            DateTime _TuNgay = Libs.clsCommon.ParseExactDMY(fromDate), _DenNgay = Libs.clsCommon.ParseExactDMY(toDate);

            Models.DIC.Interfaces.IDangKyVaoRa dangKy = new Models.DIC.clsDangKyVaoRa();
            Models.Interfaces.IResultOk ok = null;
            List<Entity.Interfaces.IDangKyRaVaoInfo> Items = null;
            dangKy.GetAll(_TuNgay, _DenNgay, DepId, MaNV, (result, ex) => { Items = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DangKyRaVaoUpdate(string action, string item)
        {
            Models.DIC.Interfaces.IDangKyVaoRa dangKy = new Models.DIC.clsDangKyVaoRa();
            Models.Interfaces.IResultOk ok = null;
            var form = Newtonsoft.Json.JsonConvert.DeserializeObject<NtbSoft.ERP.Entity.clsDangKyRaVaoInfo>(item, new Newtonsoft.Json.JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Include });

            DateTime _NgayDangKy = Libs.clsCommon.ParseExactDMY(form.StrNgayDangKy);
            form.NgayDangKy = _NgayDangKy;
            //if (!DateTime.TryParse(Convert.ToString(form["StrNgayDangKy"]) ?? "", out _NgayDangKy))
            //{
            //    return Json("ERROR: Định dạng ngày giờ không đúng", JsonRequestBehavior.AllowGet);
            //}
            //double value;
            //double.TryParse(Convert.ToString(form["ID"]) ?? 0, out value);
            //var ArrayNV = form["ArrayNV"] as JToken;
            //var Temp = new Entity.clsDangKyLamThemInfo();
            //Temp.ID = value;
            //Temp.NgayDangKy = _NgayDangKy;
            List<NtbSoft.ERP.Entity.Interfaces.IDangKyRaVaoInfo> Items = new List<NtbSoft.ERP.Entity.Interfaces.IDangKyRaVaoInfo>();
            Items.Add(form);
            if (action == "DELETE")
                dangKy.Update(Libs.ApiAction.DELETE, Items, ex => { ok = ex; });
            else
            {

                TimeSpan tsGioRa, tsGioVao;
                if (TimeSpan.TryParse(form.GioRa, out tsGioRa))
                    if (TimeSpan.TryParse(form.GioVao, out tsGioVao))
                    {
                        if ((tsGioVao - tsGioRa).TotalMinutes > 0)
                            form.ThoiGian = (tsGioVao - tsGioRa).TotalMinutes.ToString();
                    }
                dangKy.Update(Libs.ApiAction.PUT, Items, ex => { ok = ex; });
                ////    return Json("ERROR: Định dạng \"Đến giờ\" không đúng", JsonRequestBehavior.AllowGet);
                ////Temp.SoGio = (decimal)(tsTo.TotalHours - tsFrom.Hours);
                ////foreach (var item1 in ArrayNV)
                ////{
                ////    Temp.MaNV = item1["MaNV"].ToString();
                ////    if (action == "PUT")
                ////        dangKy.Update(Libs.ApiAction.PUT, Temp, ex => { ok = ex; });
                ////    else if (action == "POST")
                ////        dangKy.Update(Libs.ApiAction.POST, Temp, ex => { ok = ex; });

                ////    if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);
                ////}
            }
            if (ok != null && !ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region THEO DÕI PHÉP NĂM
        public ActionResult TheoDoiPhepNam()
        {
            ViewBag.Title = "Theo dõi phép năm";
            return View();
        }

        public JsonResult TheoDoiPhepNamGetBy(int Month, int Year, string DepId, string MaNV)
        {
            Models.DIC.Interfaces.ITheoDoiPhepNam mObj = new Models.DIC.clsTheoDoiPhepNam();
            List<Entity.Interfaces.ITheoDoiPhepNamInfo> mItems = new List<Entity.Interfaces.ITheoDoiPhepNamInfo>();
            Models.Interfaces.IResultOk ok = null;
            mObj.GetBy(Month, Year, DepId, MaNV, (result, ex) => { mItems = result; ok = ex; });
            if (!ok.Success) return Json("ERROR: " + ok.Message, JsonRequestBehavior.AllowGet);

            return Json(mItems, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
