﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class ActiveController : Controller
    {
        private string _sRegisteredName = "";
        //
        // GET: /Active/
        public ActionResult Index()
        {
            ViewBag.Title = "Đăng ký sử dụng phần mềm";
            ViewBag.UserCode = "";
            ViewBag.Text = "Đăng ký";
            //Lay thong tin phan mem dang ky
            try
            {
                ERP.Registration.Interfaces.IRegistration _register =
                    new ERP.Registration.clsWebRegistration();
                ViewBag.UserCode = _register.GenerateKey();

                System.Configuration.AppSettingsReader _appReader = new System.Configuration.AppSettingsReader();
                if (_appReader.GetValue("ActiveKey", typeof(string)) != null)
                    ViewBag.ActiveKey = _appReader.GetValue("ActiveKey", typeof(string)).ToString();
                else
                    ViewBag.ActiveKey = "";

                ViewBag.IsActived = _register.IsKeyOK();
            }
            catch 
            {
                ViewBag.IsActived = false;
                ViewBag.ActiveKey = "";
            }
            if (ViewBag.IsActived)
                ViewBag.Text = "Đã đăng ký";
            return View();
        }

        private string GetUserCode()
        {
            if (string.IsNullOrEmpty(_sRegisteredName))
            {
              
                ERP.Registration.Interfaces.IRegistration _register =
                    new ERP.Registration.clsWebRegistration();
                return _register.GenerateKey();
            }

            return "";
        }

        [HttpPost]
        public ActionResult Registration( string ActiveKey)
        {
            ERP.Registration.Interfaces.IRegistration _register =
              new ERP.Registration.clsWebRegistration();
            if (_register.IsKeyOK(ActiveKey))
            {
                System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                config.AppSettings.Settings["ActiveKey"].Value = ActiveKey;
                config.Save(System.Configuration.ConfigurationSaveMode.Minimal, false);
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");
                
                return Content("true");
            }
            else
                return Content("false");

            
        }
	}
}