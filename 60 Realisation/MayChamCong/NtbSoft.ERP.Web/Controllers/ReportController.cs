﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportLineName()
        {
            ViewBag.Title = "Báo cáo chuyền may";
            return View();
        }

        [HttpPost]
        public FileResult RpChiTietNgay(string fromDate, string toDate, string arrayMaNV)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                DateTime dtFrom = Libs.clsCommon.ParseExactDMY(fromDate), dtTo = Libs.clsCommon.ParseExactDMY(toDate);
                if (HttpContext.Request.Form["arrayMaNV"] != null)
                    arrayMaNV = HttpContext.Request.Form["arrayMaNV"];

                Services.NPOIChiTietNgay npoi = new Services.NPOIChiTietNgay();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.Export(Server, dtFrom, dtTo, arrayMaNV);

                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                //fs.Close();

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "bc_chi_tiet_thang.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
        }

        [HttpPost]
        public FileResult RpChiTietVaoRaThang(int Month, int Year, string arrayMaNV)
        {
            try
            {
                if (Month > 12)
                    Month = 12;
                if (string.IsNullOrEmpty(Year.ToString()))
                {
                    Year = DateTime.Now.Year;
                }
                if (HttpContext.Request.Form["arrayMaNV"] != null)
                    arrayMaNV = HttpContext.Request.Form["arrayMaNV"];


                //if (!System.IO.File.Exists(Server.MapPath(@"\Content\Templates\bc_chi__tiet_vao_ra.xlsx")))
                //{
                //    TempData["Message"] = "Tập tin \"bc_chi_tiet__vao_ra.xlsx\" không tồn tại.";
                //    return null;
                //}
                //// Opening the Excel template...
                //FileStream fs =
                //    new FileStream(Server.MapPath(@"\Content\Templates\bc_chi_tiet__vao_ra.xlsx"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                /*** Excel 2003 *.xls ***/
                //HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                /*** Excel 2007 or later *.xlsx ***/
                //NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);

                //////// Getting the worksheet by its name...
                //////NPOI.SS.UserModel.ISheet sheet = (NPOI.SS.UserModel.ISheet)templateWorkbook.GetSheet("ChiTietVaoRa");
                //////IRow dataRow = null;
                ////////Set SheetName
                ////////templateWorkbook.SetSheetName(0, Libs.clsCommon.RemoveSpecialCharacters(ItemCD.TenCongDoan));
                ///////*** Lấy dòng Tháng năm ***/
                //////dataRow = sheet.GetRow(3);
                //////dataRow.GetCell(0).SetCellValue("THÁNG 12 NĂM 2017");
                ///////*** Dòng cài đặt các ngày trong 1 tháng ***/
                //////dataRow = sheet.GetRow(6);

                ////////sheet.GetMergedRegion()
                //////int Month = DateTime.Now.Month, year = DateTime.Now.Year;
                //////DateTime lastDay = Libs.clsCommon.LastDayOfMonth(Month, year);
                //////int StartColumn = 2;
                //////for (int i = 1; i <= lastDay.Day; i++)
                //////{
                //////    DateTime dtTemp = new DateTime(year, Month, i);
                //////    string t = "";
                //////    if (dtTemp.DayOfWeek == DayOfWeek.Sunday) t = "CN";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Monday) t = "T2";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Tuesday) t = "T3";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Wednesday) t = "T4";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Thursday) t = "T5";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Friday) t = "T6";
                //////    else if (dtTemp.DayOfWeek == DayOfWeek.Saturday) t = "T7";

                //////    string value = string.Format("{0}-{1}", i, t);
                //////    dataRow.GetCell(StartColumn + i).SetCellValue(value);
                //////}

                //////for (int i = 0; i < sheet.NumMergedRegions; i++)
                //////{
                //////    var dd = sheet.GetMergedRegion(i);
                //////    var kk = dd;
                //////    var cc = kk;
                //////}

                ////////Tạo đường kẻ viền (Borders)
                //////ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                //////CellStyle3.BorderBottom = BorderStyle.Thin;
                //////CellStyle3.BorderRight = BorderStyle.Thin;
                //////CellStyle3.Alignment = HorizontalAlignment.Center;
                //////CellStyle3.VerticalAlignment = VerticalAlignment.Center;
                ////////CellStyle3.SetFont(font);
                //////////Tạo dòng mới
                //////IRow row = sheet.CreateRow(7);
                //////var cell = row.CreateCell(0);
                //////cell.SetCellValue(1);
                //////cell.CellStyle = CellStyle3;

                //////row = sheet.CreateRow(8);
                //////cell.CellStyle = CellStyle3;
                //////row = sheet.CreateRow(9);
                //////cell.CellStyle = CellStyle3;

                //////var cellRange = new CellRangeAddress(7, 9, 0, 0);
                //////sheet.AddMergedRegion(cellRange);
                //////RegionUtil.SetBorderRight(1, cellRange, sheet, templateWorkbook);
                //////RegionUtil.SetBorderBottom(1, cellRange, sheet, templateWorkbook);

                Services.NPOIChiTietThang npoi = new Services.NPOIChiTietThang();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.BcChiTietVaoRa(Server, Month, Year, arrayMaNV);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                //fs.Close();

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "bc_chi_tiet_thang.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }


        [HttpPost]
        public FileResult RpKhongChamCong(string fromDate, string toDate, string DepId)
        {
            try
            {
                DateTime dtFrom = Libs.clsCommon.ParseExactDMY(fromDate), dtTo = Libs.clsCommon.ParseExactDMY(toDate);
                //DateTime.TryParse(fromDate, out dtFrom);
                //DateTime.TryParse(toDate, out dtTo);
                Services.NPOIKhongCham npoi = new Services.NPOIKhongCham();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.BcKhongCham(Server, dtFrom, dtTo);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_nhan_vien_khong_cham_cong.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }

        [HttpPost]
        public FileResult RpDiMuonVS(string fromDate, string toDate, string DepId)
        {
            DateTime dtFrom = Libs.clsCommon.ParseExactDMY(fromDate), dtTo = Libs.clsCommon.ParseExactDMY(toDate);
            //DateTime.TryParse(fromDate, out dtFrom);
            //DateTime.TryParse(toDate, out dtTo);
            Services.Interfaces.INopiReoprt npoi = new Services.NPOIDiMuonVS();
            NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = null;
            Exception error = null;
            npoi.Export(Server, dtFrom, dtTo, (result, ex) => { templateWorkbook = result; error = ex; });
            if (error == null && templateWorkbook != null)
            {
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);
                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_nhan_vien_di_muon_ve_som.xlsx");
            }

            TempData["Message"] = "ERROR: NULL";
            return null;

        }


        [HttpPost]
        public FileResult RpDangKyLamThemGio(string fromDate, string toDate, string DepId)
        {
            try
            {
                DateTime dtFrom = Libs.clsCommon.ParseExactDMY(fromDate), dtTo = Libs.clsCommon.ParseExactDMY(toDate);
                //DateTime.TryParse(fromDate, out dtFrom);
                //DateTime.TryParse(toDate, out dtTo);
                Services.NPOIDangKyLamThem npoi = new Services.NPOIDangKyLamThem();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.Export(Server, dtFrom, dtTo, DepId);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_nhan_vien_khong_cham_cong.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }

        [HttpPost]
        public FileResult RpDangKyCheDo(string fromDate, string toDate, string DepId, int MaCheDo)
        {
            try
            {
                DateTime dtFrom, dtTo;
                if (!DateTime.TryParse(fromDate, out dtFrom))
                    dtFrom = DateTime.Now;
                if (!DateTime.TryParse(toDate, out dtTo))
                    dtTo = DateTime.Now;
                Services.NPOIDangKyCheDo npoi = new Services.NPOIDangKyCheDo();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.Export(Server, dtFrom, dtTo, DepId, MaCheDo);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_lao_dong_che_do.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }


        [HttpPost]
        public FileResult RpPhepNam(string DepId, int Years)
        {
            try
            {

                Services.NPOIPhepNam npoi = new Services.NPOIPhepNam();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.Export(Server, DepId, Years);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "So_theo_doi_phep_nam.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }

        [HttpPost]
        public FileResult RpNhanVienNghi(string fromDate, string toDate)
        {
            try
            {
                DateTime _fromDate = Libs.clsCommon.ParseExactDMY(fromDate),
                    _toDate = Libs.clsCommon.ParseExactDMY(toDate);

                Services.NPOINhanVienNghi npoi = new Services.NPOINhanVienNghi();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.BcNhanVienNghi(Server, _fromDate, _toDate);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_nhan_vien_nghi_viec.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }


        [HttpPost]
        public FileResult RpDangKyRaVao(string fromDate, string toDate, string DepID, string MaNV)
        {
            try
            {
                DateTime _fromDate = Libs.clsCommon.ParseExactDMY(fromDate),
                    _toDate = Libs.clsCommon.ParseExactDMY(toDate);

                Services.NPOIDangKyRaVao npoi = new Services.NPOIDangKyRaVao();
                NPOI.XSSF.UserModel.XSSFWorkbook templateWorkbook = npoi.Export(Server, _fromDate, _toDate, DepID, MaNV);
                MemoryStream ms = new MemoryStream();
                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Ds_dang_ky_ra_vao.xlsx");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }

        }
    }
}