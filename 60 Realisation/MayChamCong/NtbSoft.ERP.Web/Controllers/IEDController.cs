﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System.IO;

namespace NtbSoft.ERP.Web.Controllers
{
    [Authorize]
    public class IEDController : Controller
    {
        #region PHÂN TÍCH MÃ HÀNG ==========================================
        public ActionResult Analysis()
        {
            ViewBag.Title = "Phân tích";
            return View();
        }

        [HttpPost]
        public FileResult GTDMaHangExcel(string MenuId)
        {
            try
            {
                Models.clsGTDMaHang clsObj = new Models.clsGTDMaHang();
                Entity.clsGTDMaHangInfo ItemStyle = clsObj.GetBy(MenuId);
                if (ItemStyle == null) return null;

                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\StyleTemplate.xls"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

                // Getting the worksheet by its name...
                ISheet sheet = templateWorkbook.GetSheet("Style");
                //Set SheetName
                templateWorkbook.SetSheetName(0, ItemStyle.TenMaHang);
                // Getting the row... 0 is the first row.
                IRow dataRow = sheet.GetRow(1);
                dataRow.GetCell(1).SetCellValue(ItemStyle.KhachHang);

                dataRow = sheet.GetRow(2);
                dataRow.GetCell(1).SetCellValue(ItemStyle.TenMaHang);

                int rowIndex = 4, i = 0;
                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Dashed;
                CellStyle.BorderRight = BorderStyle.Thin;

                ICellStyle CellStyle2 = templateWorkbook.CreateCellStyle();
                CellStyle2.BorderBottom = BorderStyle.Thin;
                CellStyle2.BorderRight = BorderStyle.Thin;

                var font = templateWorkbook.CreateFont();
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                CellStyle3.BorderBottom = BorderStyle.Thin;
                CellStyle3.Alignment = HorizontalAlignment.Right;
                CellStyle3.BorderRight = BorderStyle.Thin;
                CellStyle3.SetFont(font);
                bool HasValue = true;

                if (ItemStyle.ThaoTacMaHangCollection != null && ItemStyle.ThaoTacMaHangCollection.Count > 0)
                {
                    foreach (var item in ItemStyle.ThaoTacMaHangCollection)
                    {
                        bool TheEnd = false;
                        if (i == ItemStyle.ThaoTacMaHangCollection.Count - 1)
                            TheEnd = true;
                    tt:
                        if (sheet.GetRow(rowIndex + i) == null)
                            sheet.CreateRow(rowIndex + i);
                        dataRow = sheet.GetRow(rowIndex + i);

                        ICell cell = dataRow.CreateCell(0);
                        if (HasValue)
                            cell.SetCellValue(i + 1);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(1);
                        if (HasValue)
                            cell.SetCellValue(item.MaSo);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(2);
                        if (HasValue)
                            cell.SetCellValue(item.TenCongDoan);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(3);
                        if (HasValue) cell.SetCellValue(item.MaThietBi);
                        //else cell.SetCellValue("Tổng SAM: ");
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(4);
                        if (HasValue) cell.SetCellValue(item.MaBacTho);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(5, CellType.Numeric);
                        if (HasValue) cell.SetCellValue(Math.Round(item.HeSo, 2));
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(6);
                        if (HasValue) cell.SetCellValue(Math.Round(item.ThoiGianChuan, 5));
                        //Cài đặt định dạng
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;
                        
                        i++;
                        if (i == ItemStyle.ThaoTacMaHangCollection.Count)
                        {
                            HasValue = false;
                            goto tt;
                        }
                    }//En foreach Duyệt qua từng thao tác công đoạn
                }
                NPOI.SS.Util.CellRangeAddress cra = new NPOI.SS.Util.CellRangeAddress(rowIndex + i-1, rowIndex + i-1, 0, 5);
                sheet.AddMergedRegion(cra);
                dataRow = sheet.GetRow(rowIndex + i-1);
                ICell cell1 = dataRow.CreateCell(0);
                cell1.SetCellValue("Tổng SAM(Phút): ");
                cell1.CellStyle = CellStyle3;
                
                cell1 = dataRow.CreateCell(6);
                cell1.SetCellFormula(string.Format("SUM(G5:G{0})", rowIndex + i - 1));
                cell1.CellStyle = CellStyle3;
                //cell1.SetCellFormula("");
                // Forcing formula recalculation...
                sheet.ForceFormulaRecalculation = true;
                
                MemoryStream ms = new MemoryStream();

                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                fs.Close();

                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Style.xls");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }
        }

        [HttpGet]
        public string GTDMaHangGetBy(string MenuId)
        {
            try
            {
                Models.clsGTDMaHang clsObj = new Models.clsGTDMaHang();
                Entity.clsGTDMaHangInfo Item = clsObj.GetBy(MenuId);
                if (Item != null)
                {
                   // Models.clsCongDoan _clsCongDoan = new Models.clsCongDoan();
                    
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpPost]
        public string GTDMaHangUpdate(Entity.clsGTDMaHangInfo Item)
        {
            try
            {
                Item.NguoiTao = HttpContext.User.Identity.Name;
                Item.NguoiCapNhat = HttpContext.User.Identity.Name;
                Models.clsGTDMaHang _clsObj = new Models.clsGTDMaHang();
                if (_clsObj.Update(Item)) return "true";

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        #endregion

        #region CÔNG ĐOẠN ==================================================
        public ActionResult Step()
        {
            ViewBag.Title = "Công đoạn";
            return View();
        }

        [HttpPost]
        public FileResult CongDoanExcel(string MenuId)
        {
            try
            {
                Models.clsCongDoan clsObj = new Models.clsCongDoan();
                Entity.clsCongDoanInfo ItemCD = clsObj.GetByMenuFirst(MenuId);
                if (ItemCD == null) return null;
                //Models.clsThietBi mThietBi = new Models.clsThietBi();
                //Models.clsThaoTacSp mThaoTacSP = new Models.clsThaoTacSp();
                //Entity.clsThietBiInfo mThietBiInfo = mThietBi.GetThietBi(ItemCD.MaThietBi);
                //Entity.clsThaoTacSpInfo mThaoTacSPInfo = mThaoTacSP.GetThaoTac(ItemCD.MaThaoTacSp);
                // Opening the Excel template...
                FileStream fs =
                    new FileStream(Server.MapPath(@"\Content\CDTemplate.xls"), FileMode.Open, FileAccess.Read);

                // Getting the complete workbook...
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                
                // Getting the worksheet by its name...
                ISheet sheet = templateWorkbook.GetSheet("Cong doan");
                //Set SheetName
                templateWorkbook.SetSheetName(0, Libs.clsCommon.RemoveSpecialCharacters(ItemCD.TenCongDoan));
                // Getting the row... 0 is the first row.
                IRow dataRow = sheet.GetRow(1);
                dataRow.GetCell(0).SetCellValue(string.Format("Ngày : {0}", DateTime.Now.ToString("dd/MM/yyyy")));

                dataRow = sheet.GetRow(2);
                dataRow.GetCell(1).SetCellValue(ItemCD.TenCongDoan);
                dataRow = sheet.GetRow(3);//Dòng thứ 3
                dataRow.GetCell(1).SetCellValue(ItemCD.MoTa); //Cột thứ 1
                dataRow = sheet.GetRow(5); //Dòng thứ 5
                dataRow.GetCell(3).SetCellValue(ItemCD.TenCongDoan);//Cột thứ 3

                int rowIndex = 8, i = 0;
                ICellStyle CellStyle = templateWorkbook.CreateCellStyle();
                CellStyle.BorderBottom = BorderStyle.Dashed;
                CellStyle.BorderRight = BorderStyle.Thin;

                ICellStyle CellStyle2 = templateWorkbook.CreateCellStyle();
                CellStyle2.BorderBottom = BorderStyle.Thin;
                CellStyle2.BorderRight = BorderStyle.Thin;

                var font = templateWorkbook.CreateFont();
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold; 
                ICellStyle CellStyle3 = templateWorkbook.CreateCellStyle();
                CellStyle3.BorderBottom = BorderStyle.Thin;
                CellStyle3.BorderRight = BorderStyle.Thin;
                CellStyle3.SetFont(font);
                bool HasValue = true;

                if (ItemCD.ThaoTacCongDoan != null && ItemCD.ThaoTacCongDoan.Count > 0)
                {
                    foreach (var item in ItemCD.ThaoTacCongDoan)
                    {
                        bool TheEnd = false;
                        if (i == ItemCD.ThaoTacCongDoan.Count - 1)
                            TheEnd = true;
                    tt:
                        if (sheet.GetRow(rowIndex + i) == null)
                            sheet.CreateRow(rowIndex + i);
                        dataRow = sheet.GetRow(rowIndex + i);

                        ICell cell = dataRow.CreateCell(0);
                        if (HasValue)
                            cell.SetCellValue(i + 1);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(1);
                        if (HasValue)
                            cell.SetCellValue(item.MaSo);
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(2);
                        if (HasValue)
                            cell.SetCellValue(Math.Round(item.SoLanXuatHien, 1));
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(3);
                        if (HasValue) cell.SetCellValue(item.MoTa);
                        else cell.SetCellValue("Tổng TMU: ");
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(4, CellType.Numeric);
                        if (HasValue) cell.SetCellValue(Math.Round(item.TMUThietBi * item.SoLanXuatHien, 2));
                        else cell.SetCellFormula(string.Format("SUM(E9:E{0})", rowIndex + i));
                        //else cell.SetCellValue(ItemCD.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThietBi, 2)));
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(5, CellType.Numeric);
                        if (HasValue) cell.SetCellValue(Math.Round(item.TMUThaoTac, 2));
                        else cell.SetCellFormula(string.Format("SUM(F9:F{0})", rowIndex + i));
                        //else cell.SetCellValue(ItemCD.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThaoTac, 2)));
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;

                        cell = dataRow.CreateCell(6);
                        float value = (item.TMUThietBi == 0 ? item.TMUThaoTac : item.TMUThietBi) / 27.8f;
                        if (HasValue) cell.SetCellValue(Math.Round(value, 3));
                        else cell.SetCellFormula(string.Format("SUM(G9:G{0})", rowIndex + i));
                        //Cài đặt định dạng
                        if (!TheEnd) cell.CellStyle = CellStyle;
                        else if (!HasValue) cell.CellStyle = CellStyle3;
                        else cell.CellStyle = CellStyle2;
                        i++;
                        if (i == ItemCD.ThaoTacCongDoan.Count)
                        {
                            HasValue = false;
                            goto tt;
                        }

                    }//En foreach Duyệt qua từng thao tác công đoạn
                    //double mTongTMUThietBi = ItemCD.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThietBi, 2)),
                    //       mTongTMUThaoTac = ItemCD.ThaoTacCongDoan.Sum(s => Math.Round(s.TMUThaoTac, 2));
                    //double mHaoPhiThietBi = mTongTMUThietBi * (mThietBiInfo.HaoPhiThietBi / 100f);
                    //double mHaoPhiThaoTac = mTongTMUThaoTac * (mThietBiInfo.HaoPhiTayChan / 100f);

                    //double mTongGiay = ((mTongTMUThietBi + mHaoPhiThietBi) + (mTongTMUThaoTac + mHaoPhiThaoTac)) / 27.8f;
                    //double mThoiGianChuanBi = 0;
                    //if (mThaoTacSPInfo.SoLuong != 0)
                    //    mThoiGianChuanBi = (mThaoTacSPInfo.ThoiGianThaoTac * 60f) / mThaoTacSPInfo.SoLuong;
                    //ItemCD.SetThietBi_ThaoTac(mThietBiInfo, mThaoTacSPInfo);
                    if (sheet.GetRow(1) == null)
                        sheet.CreateRow(1);
                    dataRow = sheet.GetRow(1);
                    //dataRow.GetCell(6).SetCellValue(mTongGiay + mThoiGianChuanBi);
                    dataRow.GetCell(6).SetCellValue(ItemCD.TongThoiGianGiay + ItemCD.ThoiGianChuanBi);
                }
                // Forcing formula recalculation...
                sheet.ForceFormulaRecalculation = true;

                MemoryStream ms = new MemoryStream();

                // Writing the workbook content to the FileStream...
                templateWorkbook.Write(ms);

                TempData["Message"] = "Excel report created successfully!";
                fs.Close();
                
                // Sending the server processed data back to the user computer...
                return File(ms.ToArray(), "application/vnd.ms-excel", "Step.xls");
            }
            catch (Exception ex)
            {
                TempData["Message"] = "ERROR: " + ex.Message;
                return null;
            }
        }

        [HttpGet]
        public string CongDoanGetByMenu(string MenuId)
        {
            try
            {
                Models.clsCongDoan clsObj = new Models.clsCongDoan();
                Entity.clsCongDoanCollection Items = clsObj.GetByMenu(MenuId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Items);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public string CongDoanGetBy(string MenuId)
        {
            try
            {
                Models.clsCongDoan clsObj = new Models.clsCongDoan();
                Entity.clsCongDoanInfo Item = clsObj.GetByMenuFirst(MenuId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [PermissionsMenu(NtbSoft.ERP.Libs.Permissions.Add)]
        [PermissionsMenu(NtbSoft.ERP.Libs.Permissions.Edit)]
        [HttpPost]
        public string CongDoanUpdate(Entity.clsCongDoanInfo Item)
        {
            try
            {
                Item.NguoiTao = HttpContext.User.Identity.Name;
                Item.NguoiCapNhat = HttpContext.User.Identity.Name;
                Models.clsCongDoan _clsObj = new Models.clsCongDoan();
                Models.clsThietBi mThietBi = new Models.clsThietBi();
                Models.clsThaoTacSp mThaoTacSP = new Models.clsThaoTacSp();
                Entity.clsThietBiInfo mThietBiInfo = mThietBi.GetThietBi(Item.MaThietBi);
                Entity.clsThaoTacSpInfo mThaoTacSPInfo = mThaoTacSP.GetThaoTac(Item.MaThaoTacSp);
                Item.SetThietBi_ThaoTac(mThietBiInfo, mThaoTacSPInfo);

                if (_clsObj.Update(Item)) return "true";

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public string CongDoanLayPhanTich(string MaSo, string pMaThietBi,string pMaBoPhan,int pLucN)
        {
            Models.clsThuVien _clsThuVien = new Models.clsThuVien();
            Models.clsCongDoan _clsCongDoan = new Models.clsCongDoan();
            double mSoLanXuatHien = 1;
            try
            {
                /*** 1. Tìm mã số trong bảng thư viện. Nếu mã số có trong bảng thư viện thì lấy hiển thị, ngược lại nếu không có thì kiểm tra bước (2) ***/
                Entity.clsThuVienInfo ThuVien = _clsThuVien.GetByMaSo(MaSo.Trim());
                Entity.clsThaoTacCongDoanInfo mThaoTacCd = new Entity.clsThaoTacCongDoanInfo();
                Models.clsDep _clsDep = new Models.clsDep();
                Entity.clsTreeDepInfo mTreeDep = _clsDep.GetTreeNodeBy(pMaBoPhan);
                if (!string.IsNullOrWhiteSpace(ThuVien.MaThuVien))
                {
                    mThaoTacCd = new Entity.clsThaoTacCongDoanInfo(ThuVien);
                    return Newtonsoft.Json.JsonConvert.SerializeObject(mThaoTacCd);
                }
                else if (_clsCongDoan.LaThaoTacDacBiet(MaSo))
                {/*** 2. Kiểm tra "Mã số" có phải là thao tác đặc biệt  ***/

                }
                else if (_clsCongDoan.LaDuongMay(MaSo, mTreeDep))
                {/*** 3. Kiểm tra "Mã số" có phải là đường may ***/
                    Models.clsThietBi clsThietBi = new Models.clsThietBi();
                  
                    
                    double mChieuDai = Convert.ToDouble(MaSo.Substring(2, MaSo.Length - 3));
                    Libs.DIEM_DUNG mDiemDung = (Libs.DIEM_DUNG)Libs.clsDBConstant.LayGiaTri(typeof(Libs.DIEM_DUNG), MaSo.Substring(MaSo.Length - 1).ToUpper());
                    mThaoTacCd.MaSo = MaSo;
                    mThaoTacCd.SoLanXuatHien = Convert.ToSingle(mSoLanXuatHien);
                    mThaoTacCd.LoaiMaSo = Libs.clsDBConstant.cCHIEU_DAI_DUONG_MAY;
                    mThaoTacCd.MoTa = "May đoạn " + MaSo.Substring(2, MaSo.Length - 3) + " cm";
                    mThaoTacCd.TMUThietBi = Convert.ToSingle(clsThietBi.Lay_TMU_Duong_may(pMaThietBi, mChieuDai, mSoLanXuatHien, mDiemDung));

                    return Newtonsoft.Json.JsonConvert.SerializeObject(mThaoTacCd);
                    //clsThietBi.Lay_BST
                }
                else if (_clsCongDoan.LaCat(MaSo, mTreeDep))
                {/*** 4. Kiểm tra mã số đó có phải là loại Cắt hay không. Nếu đúng thì lấy ***/
                    double mChieu_dai = Convert.ToDouble(MaSo.Substring(1, MaSo.Length - 3));
                    double mDo_kho = 0;
                    switch (MaSo.Substring(MaSo.Length - 2, 1))
                    {
                        case "S":
                            mDo_kho = 1F;
                            break;
                        case "H":
                            mDo_kho = 1.2F;
                            break;

                        case "C":
                            mDo_kho = 1.4F;
                            break;
                    }
                    //byte mLucTacDung = (byte)Libs.clsDBConstant.eLUC_TAC_DUNG.Luc_15N;
                    byte mLucTacDung = (byte)pLucN;
                    Libs.DIEM_DUNG mDiem_dung = (Libs.DIEM_DUNG)Libs.clsDBConstant.LayGiaTri(typeof(Libs.DIEM_DUNG), MaSo.Substring(MaSo.Length - 1));
                    mThaoTacCd.MaSo = MaSo;
                    mThaoTacCd.SoLanXuatHien = Convert.ToSingle(mSoLanXuatHien);
                    mThaoTacCd.LoaiMaSo = Libs.clsDBConstant.cTHAO_TAC_CAT;
                    mThaoTacCd.MoTa = "Cắt đoạn " + MaSo.Substring(1, MaSo.Length - 3) + " cm";
                    mThaoTacCd.TMUThietBi = Convert.ToSingle(_clsCongDoan.Lay_TMU_Cat(mLucTacDung, mDo_kho, mChieu_dai, mDiem_dung));
                    return Newtonsoft.Json.JsonConvert.SerializeObject(mThaoTacCd);
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(new Entity.clsThuVienInfo());
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }


        #endregion

        #region THƯ VIỆN ===================================================
        public ActionResult Library() {
            ViewBag.Title = "Thư viện";
            return View();
        }

        [HttpGet]
        public string ThuVienGetBy(string MenuId)
        {
            try
            {
                Models.clsThuVien dep = new Models.clsThuVien();
                Entity.clsThuVienInfo Item = dep.GetBy(MenuId);

                return Newtonsoft.Json.JsonConvert.SerializeObject(Item);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public string ThuVienUpdate(Entity.clsThuVienInfo Item)
        {

            try
            {
                Models.clsThuVien _clsObj = new Models.clsThuVien();
                if (_clsObj.Update(Item))
                    return "true";

                return "false";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }

        [HttpGet]
        public string FindThuVienMatch(string maso)
        {
            try
            {
                NtbSoft.ERP.Models.clsThuVien _clsObj = new NtbSoft.ERP.Models.clsThuVien();
                return Newtonsoft.Json.JsonConvert.SerializeObject(_clsObj.FindMatch(maso));
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }


        #endregion
    }
}
