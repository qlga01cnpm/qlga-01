﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NtbSoft.ERP.Web.Controllers
{
    public class ProTrackingController : Controller
    {
        //
        // GET: /ProTracking/
        public ActionResult ViewChuyenMay()
        {
            ViewBag.Title = "Theo dõi năng suất Chuyền may";
            return View();
        }

        public ActionResult ViewDongGoi()
        {
            ViewBag.Title = "Theo dõi Đóng gói - Thành phẩm";
            return View();
        }

        public ActionResult ViewToCat()
        {
            ViewBag.Title = "Theo dõi tổ cắt";
            return View();
        }


	}
}