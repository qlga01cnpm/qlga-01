﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NtbSoft.ERP.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new {  id = UrlParameter.Optional }
                defaults: new { controller = "Manager", action = "Home", id = UrlParameter.Optional }
            );

           // routes.MapRoute(
           //   name: "Transalte",
           //   url: "{controller}/{action}/{id}",
           //   defaults: new { controller = "Home", action = "Transalte", id = UrlParameter.Optional }
           //);

            //ModelBinders.Binders.Add(typeof(JObject), new JObjectModelBinder()); //for dynamic model binder
            //routes.MapRoute(
            //    name: "MCC",    //Route Name
            //    url: "MCC/{controller}/{action}/{id}",
            //    defaults: new
            //    {
            //        //controller = "RentalProperty",
            //        //action = "All",
            //        id = UrlParameter.Optional
            //    }
            //);
            //routes.IgnoreRoute("{*x}", new { x = @".*\.asmx(/.*)?" });
        }
    }
}
