﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NtbSoft.ERP.Web.AppCaches
{
    public class clsCacheTheApp
    {
        private static List<Entity.clsQuyTacLamTronInfo> _QuyTacLamTronCollection = null;
        private static Entity.Interfaces.IAppInfo _AppInfo = null;
        private static object syncRoot = new Object();
        public static clsCacheTheApp Instance
        {
            get
            {
                if (_AppInfo == null)
                {
                    lock (syncRoot)
                    {
                        Models.SYS.Interfaces.IApp theApp = new Models.SYS.clsApp();
                        theApp.GetInfo((result, ex) => { if (ex.Success) _AppInfo = result; });
                    }
                }
                if (_QuyTacLamTronCollection == null || _QuyTacLamTronCollection.Count == 0)
                {
                    IniQuyTacLamTron();
                }
                return Nested.Instance;
            }
        }
        class Nested
        {
            //Explicit static constructor to tell # compiler
            // not to mark type as beforefieldinit
            static Nested() { }

            //Quá trình khởi tạo đối tượng được gọi ở lần truy xuất đầu tiên đến static member của lớp con. 
            //Quá trình này chỉ xảy ra trong biến Instance.
            //Như vậy hoàn toàn có tính lazy-load và đồng thời vẫn đảm bảo performance tốt. 
            internal static readonly clsCacheTheApp Instance = new clsCacheTheApp();
        }

        /// <summary>
        /// Get
        /// </summary>
        public Entity.Interfaces.IAppInfo AppInfo { get { if (_AppInfo == null) return new Entity.clsAppInfo(); return _AppInfo; } }
        /// <summary>
        /// Get
        /// </summary>
        public string CompanyName { get { if (_AppInfo == null)return ""; return _AppInfo.Name; } }
        /// <summary>
        /// Get
        /// </summary>
        public string Address { get { if (_AppInfo == null)return ""; return _AppInfo.Address; } }
        public bool IsRunCC { get { if (_AppInfo == null)return false; return _AppInfo.IsRunMCC; } }
        public void Reload()
        {
            _AppInfo = null;
        }

        private static void IniQuyTacLamTron()
        {
            ERP.Models.Interfaces.IQuyTacLamTron mObj = new ERP.Models.clsQuyTacLamTron();
            mObj.GetAll((result, complete) => { _QuyTacLamTronCollection = result; });
        }

        public List<Entity.clsQuyTacLamTronInfo> QuyTacLamTronCollection
        {
            get
            {
                return _QuyTacLamTronCollection;
            }
        }
    }
}