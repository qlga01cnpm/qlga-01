﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupBox.aspx.cs" Inherits="NtbSoft.ERP.Web.Temps.GroupBox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
	html,
	body {
		color:#000;
		background:#fff;
	}
	fieldset {
		margin:20px;
		padding:0 10px 10px;
		border:1px solid #666;
		border-radius:8px;
		box-shadow:0 0 10px #666;
	}
	legend {
		padding:2px 4px;
		background:#fff; /* For better legibility against the box-shadow */
	}

	/* position:absolute */
	#position {
		position:relative;
		padding-top:10px;
	}
	#position > legend {
		position:absolute;
		top:-10px;
		right:10px;
		left:10px;
		width:20%;
	}

	/* float */
	#float {
		padding-top:10px;
	}
	#float > legend {
		float:left;
		margin-top:-20px;
	}
	#float > legend + * {
		clear:both;
	}

	/* A paragraph with border-radius and box-shadow */
	#box-shadow {
		margin:20px;
		padding:1em;
		border:1px solid #666;
		border-radius:8px;
		box-shadow:0 0 10px #666;
	}
	</style>
</head>
<body>
    <select id="state" class="easyui-combobox" name="state" label="State:" labelPosition="top" style="width:100%;"></select>
    <div id="body">
        <h1>Fieldset, legend, border-radius and box-shadow – demo page</h1>
        <p>This is a demo document related to the article <a href="/archive/201302/fieldset_legend_border-radius_and_box-shadow/">Fieldset, legend, border-radius and box-shadow</a>. Please see the article for context and information.</p>
        <fieldset>
            <legend>This fieldset has no special fixes for anything, so there will be visual peculiarities and line wrapping issues in some browsers</legend>
            <div>
                <input type="radio" name="colour" id="colour-1" />
                <label for="colour-1">Red</label>
            </div>
            <div>
                <input type="radio" name="colour" id="colour-2" />
                <label for="colour-2">Green</label>
            </div>
            <div>
                <input type="radio" name="colour" id="colour-3" />
                <label for="colour-3">Blue</label>
            </div>
        </fieldset>
        <fieldset id="position">
            <legend>This fieldset has an absolutely positioned legend</legend>
            <div>
                <input type="radio" name="colour2" id="colour2-1" />
                <label for="colour2-1">Red</label>
            </div>
            <div>
                <input type="radio" name="colour2" id="colour2-2" />
                <label for="colour2-2">Green</label>
            </div>
            <div>
                <input type="radio" name="colour2" id="colour2-3" />
                <label for="colour2-3">Blue</label>
            </div>
        </fieldset>
        <fieldset id="float">
            <legend>This fieldset’s legend element is floated and has a negative top margin to position it on top of the fieldset’s top border</legend>
            <div>
                <input type="radio" name="colour3" id="colour3-1" />
                <label for="colour3-1">Red</label>
            </div>
            <div>
                <input type="radio" name="colour3" id="colour3-2" />
                <label for="colour3-2">Green</label>
            </div>
            <div>
                <input type="radio" name="colour3" id="colour3-3" />
                <label for="colour3-3">Blue</label>
            </div>
        </fieldset>
        <p id="box-shadow">A <code>p</code> element with border-radius and box-shadow.</p>
        <div id="labfooter">
            <p><a href="/lab/">Lab Index</a> | <a href="/">456 Berea Street Home</a> | Copyright &#169; 2003-2017 Roger Johansson</p>
        </div>
    </div>
</body>
</html>
