﻿var isAddNew = false;
//$(document).ready(function () {
//    IniDataGrid();
//    iniComboboxTree();
//    getBoPhan();
   
//});

function refresh() {
    loadDataDetail();
}

function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onSelect: gridLog_onSelect
    }).datagrid('keyCtr');

}
function gridLog_onSelect(index, row) {
    $('#dg-list').datagrid('clearSelections');
    loadDataDetail();
}

//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}
function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    loadDataDetail();

}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "DEP.ROOT";
    return tNode.id;
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') {
        row = $('#dg-list').datagrid('getSelected');
        if (row)
            return row.MaNV;

        return "";
    }
    return row.MaNV;
}

function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}
function loadDataDetail() {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/Setting/DangKyLamThemGet?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + getDepId() + "&MaNV=" + getMaNV();
    //alert(newUrl);
    $.ajax({
        url: newUrl,
        type: "GET",
       //timeout: 60000,
        //async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                $('#dg-list').datagrid('loadData', data);
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function AddNew() {
    isAddNew = true;
    //var row = $('#dg-list').datagrid('getSelected');
    //if (row != null) {
    //    parent.showAlert('Thông báo', 'Nhân viên [' + row.TenNV + '] đã đăng ký ca [' + row.ShiftID + ']', 'warning');
    //    return;
    //}
    var rows = $('#dg-nhanvien').datagrid("getChecked");
    if (rows.length == 0) {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return;
    }
   
    $('#fm').form('clear');
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    //$('#fm').form('load', newRow);
    setFromDate('16:00');
    setTomDate('17:00');
    setDisable(false);
    var date = formatterDMY(new Date());

    $('#txt_NgayDangKy').datebox('setValue', date);
}

function editInfo() {
    isAddNew = false;
    var row = getRow();

    if (row) {
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Chỉnh sửa', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
        setDisable(true);
    }
}

function saveInfo() {
    
    try {
        var Temp = $('#fm').serializeObject();
        var act = "PUT";
        
        Temp.StrNgayDangKy = $('#txt_NgayDangKy').datebox('getValue');
        Temp.TuGio = $("#txt_tugio").timespinner('getValue');
        Temp.DenGio = $("#txt_dengio").timespinner('getValue');
        Temp.ArrayNV = Array();
        if (isAddNew) {
            Temp.ID = 0;
            act = "POST";
            var rows = $('#dg-nhanvien').datagrid("getChecked");
            for (var i = 0; i < rows.length; i++) {
                Temp.ArrayNV.push({ MaNV: rows[i].MaNV });
            }
        } else {
            var row = $('#dg-list').datagrid('getSelected');
            if (row) Temp.ArrayNV.push({ MaNV: row.MaNV });
        }
       
        //Temp.MaCheDo = $("#fm input[type='radio']:checked").val();
        $.ajax({
            url: '/Setting/DangKyLamThemUpdate',
            type: "POST",
            data: { action: act, item: JSON.stringify(Temp) },
            dataType: "json",
            //contentType: "application/json; charset=utf-8",
            //async: false,
            success: function (data) {
                if (data.indexOf("ERROR:") > -1) {
                    parent.showAlert('Thông báo', data, 'warning');
                    return;
                }
                parent.bottomRight('Thông báo', 'Lưu thành công!');
                closeForm();
                loadDataDetail();
            },
            error: function (response) {
                alert(JSON.stringify(response));
            }
        }); //End  $.ajax

    } catch (e) {
        alert("ERROR: " + JSON.stringify(e));
    }
}

function removeIt() {
    var row = getRow();
    if (row == null) return;
    var Temp = {};
    Temp.ID = row.ID;
    parent.$.messager.confirm({
        draggable: false,  title: 'Xác nhận', msg: 'Xóa dữ liệu thai sản ' + row.TenNV,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================

                $.ajax({
                    url: '/Setting/DangKyLamThemUpdate',
                    type: "POST",
                    data: { action: 'DELETE', item: JSON.stringify(Temp) },
                    dataType: "json",
                    //async: false,
                    success: function (data) {
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', 'Đã xóa đăng ký thai sản ' + row.TenNV);
                        var rowIndex = $('#dg-list').datagrid("getRowIndex", row);
                        $('#dg-list').datagrid("deleteRow", rowIndex);
                    },
                    error: function (response) {
                        alert(JSON.stringify(response));
                    }
                }); //End  $.ajax


            }//End (r)==============================================
        }
    });//End $.messager.confirm
}

function getRow() {
    var rowSelect = $('#dg-list').datagrid('getSelected');
    if (rowSelect == null || rowSelect == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return null;
    }
    return rowSelect;
}

function closeForm() {
    $('#dlg').dialog('close');
}


function setFromDate(value) {
    $("#txt_tugio").timespinner('setValue', value);
}

function setTomDate(value) {
    $("#txt_dengio").timespinner('setValue', value);
}

function setDisable(enable) {
    if(enable)
        $('#txt_NgayDangKy').datebox('disable');
    else
        $('#txt_NgayDangKy').datebox('enable');
}

function exportDangKyLamThem() {
    var msg = "Báo cáo những nhân viên đăng ký làm thêm giờ. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================


                var DepId = getDepId();
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpDangKyLamThemGio?fromDate=" + getDateFrom() + "&toDate=" + getDateTo() + "&DepId=" + DepId, true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Danh_sach_dang_ky_lam_them_gio.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}