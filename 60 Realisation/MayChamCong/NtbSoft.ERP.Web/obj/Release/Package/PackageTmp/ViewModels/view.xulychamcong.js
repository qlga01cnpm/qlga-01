﻿var rowSelected = null, IsEdit = false;
var _selectedPage = 1, _pageSize = 500, _TotalRecords = 0;
function loadDataPaging(manv, depId, selectedPage, pageSize, async) {
    //parent.setProgress("Vui lòng chờ");
    //parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/MCC/XuLyChamCongGetPaging?PageIndex=" + selectedPage + "&PageSize=" + pageSize + "&manv=" + manv + "&DepId=" + depId + "&FromDate=" + getDateFrom() + '&ToDate=' + getDateTo() + "&selectedPage=" + _selectedPage + "&pageSize=" + _pageSize;

    //async: false; Nghĩa là khi một request về Server thì tất cả các 
    // request tiếp theo cần phải chờ cho đến khi Request ajax này xử lý xong mới được xử lý tiếp. 
    // Do đó với các xử lý từ Server mà lâu thì các tiến trình tiếp theo sẽ phải chờ đợi lâu cho đến khi kết thúc.
    //async: true; nghĩa là sau khi bạn click button ajax request về server. 
    // Bạn hoàn toàn có thể thực hiện gửi tiếp các yêu cầu xử lý khác về server.
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    //parent.closeProgress(); 
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);
                _TotalRecords = item.TotalRecords;
                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            //parent.closeProgress();

        }, error: function (response) {
            alert(JSON.stringify(response));
            //parent.closeProgress();
        }
    });
}

function searcher(value, name) {
    //alert(value )
    if (value) {
        //$('#dg-nhanvien').datagrid('load', {
        //    MaNV: value,
        //});

        //var depid = 'DEP.ROOT';
        //if (node != null && node.id == 'DEP.NEW')
        //depid = 'DEP.NEW';

        loadDataPaging(value, getDepId(), _selectedPage, _pageSize, true);
    }
}

function getDateFrom() {
    return $('#dt_from').datebox('getValue');
}

function getDateTo() {
    return $('#dt_to').datebox('getValue');
}

function sortMaCC(a, b) {
    return (parseInt(a) > parseInt(b) ? 1 : -1);
};

function iniDataGrid() {
    /*** Khai báo sự kiện của bàn phim lên datagrid ***/
    //$("#dg-list").datagrid({}).datagrid("keyCtr");

    $('#dg-list').datagrid({
        sortName: 'MaCC',
        sortOrder: 'asc',
        remoteSort: false,
        url: null,
        pagination: true,
        pageSize: 20,
        pageNumber: 1,
        onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        },
        onSelect: function (index, row) {
            rowSelected = row;
            //alert(rowSelected.Gio);
        },
        rowStyler: function (index, row) {
            if (row.StrThu && row.StrThu == "CN") {
                return 'background-color: #FFEAEA; color: #C20000; font-weight: bold;text-align:left';
            }
        }
    }).datagrid("keyCtr");

    //==Phan trang ==================================================
    var _d = new Date();
    var p = $("#dg-list").datagrid('getPager');
    $(p).pagination({
        pageSize: _pageSize
        , pageNumber: _selectedPage
        , showPageList: true
        , pageList: [30, 50, 100, 200, 300, 500]
        , onSelectPage: function (pageNumber, pageSize) {
            $(this).pagination('loading');
            $(this).pagination('loaded');
            _selectedPage = pageNumber;
            _pageSize = pageSize;
            loadDataPaging("", getDepId(), _selectedPage, _pageSize, true);


        }
    });
}

function iniGridNhanVien() {
    $('#dg-nhanvien').datagrid({
        onSelect: onSelectGridNhanVien
    }).datagrid("keyCtr");
}

function iniOption() {
    $('#cb_option').combobox({
        onChange: function (newValue, oldValue) {
            if (newValue != 13) {
                setDisable(true);
                getOption(newValue);
            } else {
                setDisable(false);
                getOption(20);
            }
        }
    });
    $('#cb_option').combobox('setValue', 16);
}

function setDisable(disabled) {
    $('#dt_from').datebox({ disabled: disabled });
    $('#dt_to').datebox({ disabled: disabled });
}

function refresh() {
    var MaNV = "";//$("#manv_txt").val();
    var rowNv = $('#dg-nhanvien').datagrid('getSelected');
    if (rowNv != null) MaNV = rowNv.MaNV;

    loadDataPaging(MaNV, getDepId(), _selectedPage, _pageSize, true);
}

function getOption(type) {
    $.ajax({
        url: "/MCC/GetDateOptionDMY?type=" + type,
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#dt_from').datebox('setValue', data[0]);
            $('#dt_to').datebox('setValue', data[1]);
            //var list = $.getJSON(data);
            //alert(list[0]);

        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
        }
    });
}


//Load CHỌN BỘ PHẬN - NHÂN VIÊN====================================================================================
function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}

function depOnChange(newValue, oldValue) {
    clearSelection();
    loadDataPagingNhanVien(_selectedPage, 10000, newValue);
    loadDataPaging("", newValue, _selectedPage, _pageSize, true);
}

function getDepId() {
    return $('#cb-mabophan').combobox('getValue');
}

function clearSelection() {
    rowSelected = null;
    $('#dg-nhanvien').datagrid('clearSelections');
}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPagingNhanVien(selectedPage, pageSize, DepId) {
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert(JSON.stringify(response)); }
    });
}



function onSelectGridNhanVien(index, row) {
    rowSelected = null;
    loadDataPaging(row.MaNV, getDepId(), _selectedPage, _pageSize, true);
}

function XuLy() {
    var rows = $('#dg-list').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Bạn chưa chọn dòng cần xử lý!', 'warning');
        return;
    }

    $.messager.confirm({
        title: 'Xác nhận',
        msg: 'Xác nhận xử lý dữ liệu?',
        left: '17%', top: '12%',
        fn: function (r) {
            if (r) { //Begin (r) =====================================

                parent.setProgress("Vui lòng chờ");
                parent.setProgressMsg("Đang xử lý ...");
                $.ajax({
                    url: '/MCC/XuLyChamCongX2',
                    type: "POST",
                    //async: false,
                    data: JSON.stringify(rows),
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        parent.setProgressMsg("Đã xử lý xong. Hãy tính công để báo cáo");
                        parent.closeProgress();
                        $('#dg-list').datagrid('clearSelections');
                    },
                    error: function (response) {
                    }
                });

            }//End (r)==============================================
        }
    });

}

function XuLyAll() {
    //var rows = $('#dg-list').datagrid('getChecked');
    //if (rows == null || rows == '') {
    //    parent.showAlert('Thông báo', 'Bạn chưa chọn dòng cần xử lý!', 'warning');
    //    return;
    //}
    var rows = $('#dg-list').datagrid('getRows');
    if (rows == null || rows == "") return;

    $.messager.confirm({
        title: 'Xác nhận',
        msg: 'Xác nhận xử lý dữ liệu?',
        left: '17%', top: '12%',
        fn: function (r) {
            if (r) { //Begin (r) =====================================

                parent.setProgress("Vui lòng chờ");
                parent.setProgressMsg("Đang xử lý page 1 ...");
                beginXuLyAll(1, rows);

            }//End (r)==============================================
        }
    });

}

function beginXuLyAll(page, rows) {
    //Lấy tổng số trang
    //Cấu trúc chia nguyên / và chia dư % chỉ là phép chia.
    //Cấu trúc (int)s là ép kiểu sữ liệu nhé, chứ k phải lấy phần nguyên và phần dư.
    //Ví dụ 3,6%1 hoặc (int)3.6 sẽ là 3, nhưng phần nguyên của nó là 3, phần dư là 0.6.
    //Phép chia nguyên và sư, toán tử ép kiểu nếu phần dư >=0.5 thì nó sẽ tăng 1, Tùy ngôn ngữ hỗ trợ lấy phần dư hoặc nguyên chứ k thì bạn tự viết if else mà tính.

    var totalPages = Math.round(_TotalRecords / _pageSize);
    if ((_TotalRecords / _pageSize) % 1 > 0)
        totalPages = totalPages + 1;
    //Nếu có 1 trang thì thoát luôn
    if (page > totalPages) {
        parent.setProgressMsg("Đã xử lý xong. Hãy tính công để báo cáo");
        parent.closeProgress();
        //Khi xử lý xong trở lại trang đầu
        var p = $("#dg-list").datagrid('getPager');
        $(p).pagination('select', 1);	// select the second page
        return;
    }
    else {
        //Trường hợp có nhiều trang 
        var p = $("#dg-list").datagrid('getPager');
        $(p).pagination('select', page);	// select the second page
        rows = $('#dg-list').datagrid('getRows');
        if (rows != null && rows.length > 0)
            parent.setProgressMsg("Đang xử lý page " + (page) + " ...");
    }
    //Tăng số lượng trang
    page++;


    $.ajax({
        url: '/MCC/XuLyChamCongX2',
        type: "POST",
        //async: false,
        data: JSON.stringify(rows),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {


            setTimeout(function () {
                beginXuLyAll(page, rows);
            }, 1);

        },
        error: function (response) {
        }
    });

    ////parent.setProgressMsg('Đang tính toán...' + index);
    ////$.ajax({
    ////    url: '/MCC/TinhCong',
    ////    type: "POST",
    ////    data: { jsonNV: JSON.stringify(rows[index]) },
    ////    dataType: "json",
    ////    success: function (data) {
    ////        if (JSON.stringify(data).indexOf("OK") == -1 && JSON.stringify(data).indexOf("ERROR:") == -1) {
    ////            var rowIndex = $('#dg-list').datagrid('getRowIndex', rows[index]);
    ////            if (rowIndex > -1)
    ////                $('#dg-list').datagrid('updateRow', {
    ////                    index: rowIndex,
    ////                    row: {
    ////                        Shift: data.Shift, TotalHours: data.TotalHours, OTX: data.OTX, LyDoVang: data.LyDoVang,
    ////                        TotalTimeHM: data.TotalTimeHM, LateTime: data.LateTime, EarlyTime: data.EarlyTime
    ////                    }
    ////                });
    ////        }
    ////        //parent.setProgressMsg('Đang tính toán...' + index);
    ////        //parent.setProgressMsg("Kết nối " + rows[index].Name + ": " + data);
    ////        // sau n giây sẽ kết nối tiếp
    ////        setTimeout(function () {
    ////            $('#dg-list').datagrid('selectRow', index);
    ////            beginTinhCong2(page, rows, ++index);
    ////        }, 1);

    ////    },
    ////    error: function (response) {
    ////        beginTinhCong2(page, rows, ++index);
    ////        //alert(JSON.stringify(response));
    ////    }
    ////}); //End  $.ajax


}

function addNew() {
    IsEdit = false;
    var rowNv = $('#dg-nhanvien').datagrid('getSelected');
    var rowMaNV = $('#dg-list').datagrid('getSelected');
    if (rowNv == null && rowMaNV == null) {
        parent.showAlert('Thông báo', 'Chưa chọn mã nhân viên', 'warning');
        return;
    }
    //var rowLog = $('#dg-list').datagrid('getSelected')
    //var output = "";
    //if (rowLog == null || rowLog == '') {
    //    var myDate = new Date();
    //    output = (myDate.getMonth() + 1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
    //} else {
    //    output = rowLog.StrWorkingDay;
    //}
    var newRow = {};
    newRow.StrWorkingDay = formatterDMY(new Date())
    if (rowNv != null) {
        newRow.MaCC = rowNv.MaCC;
    }
    if (rowMaNV != null) {
        newRow.MaCC = rowMaNV.MaCC;
    }
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('clear');
    $('#fm').form('load', newRow);
    //refresh();
}
function edit() {
    if (rowSelected == null) {
        parent.showAlert('Thông báo', 'Chưa chọn dữ liệu cần chỉnh sửa', 'warning');
        return;
    }
    IsEdit = true;
    $('#fm').form('clear');
    $('#dlg').dialog({ title: 'Cập nhật', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('load', rowSelected);
}
function closeForm() {
    $('#dlg').dialog('close');
}

function save() {
    var action = "POST";
    if (IsEdit) action = "PUT";
    var Item = $('#fm').serializeObject();
    if (Item.Id == "") Item.Id = 0;
    if (Item.Gio == "") {
        alert("Chưa nhập giờ");
        return;
    }
    $.ajax({
        url: '/MCC/TimeInOutUdate',
        type: "POST",
        data: { Action: action, Id: Item.Id, MaCC: Item.MaCC, workingday: Item.StrWorkingDay, gio: Item.Gio },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") == 0) {
                alert(data);
                return;
            }
            closeForm();
            refresh();
        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
        }
    });
}


function formatStatus(value, row, index) {
    //alert(value);
    if (value == true)
        return '<h6 class="easyui-linkbutton" style="background: url(\'/icons/tick_small.png\') no-repeat center center;width:32px;height:24px;margin:auto;" ></h6>';
    else
        return '<h6 class="easyui-linkbutton easyui-tooltip" title="Dữ liệu không hợp lệ giữa 2 lần chấm." style="background: url(\'/icons/error_small_red.png\') no-repeat center center;width:32px;height:24px;margin:auto;" ></h6>';
}
