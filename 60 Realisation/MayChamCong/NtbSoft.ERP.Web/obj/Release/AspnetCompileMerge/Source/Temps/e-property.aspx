﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="e-property.aspx.cs" Inherits="NtbSoft.ERP.Web.Temps.e_property" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../EasyUI/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../EasyUI/icon.css" rel="stylesheet" type="text/css" />
    <script src="../EasyUI/jquery-11.3.min.js" type="text/javascript"></script>
    <script src="../EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            
            var data = [
	                { "name": "Name", "value": "Bill Smith", "group": "ID Settings", "editor": "text" },
	                { "name": "Address", "value": "", "group": "ID Settings", "editor": "text" },
	                { "name": "Age", "value": "40", "group": "ID Settings", "editor": "numberbox" },
	                { "name": "Birthday", "value": "01/02/2012", "group": "ID Settings", "editor": "datebox" },
	                { "name": "SSN", "value": "123-456-7890", "group": "ID Settings", "editor": "text" },
	                {
	                    "name": "Email", "value": "bill@gmail.com", "group": "Marketing Settings", "editor": {
	                        "type": "validatebox",
	                        "options": {
	                            "validType": "email"
	                        }
	                    }
	                },
	                {
	                    "name": "FrequentBuyer", "value": "false", "group": "Marketing Settings", "editor": {
	                        "type": "checkbox",
	                        "options": {
	                            "on": true,
	                            "off": false
	                        }
	                    }
	                }
                            ];
            var row = {
                name:'AddName',
                value:'',
                group:'Marketing Settings',
                editor:'text'
            };
            $('#pg').propertygrid({ data: data });
           // $('#pg').propertygrid('appendRow', row);
            //$('#pg').propertygrid('appendRow', row);
        });
    </script>
</head>
<body>
    
   <h2>Customize Columns of PropertyGrid</h2>
    <p>The columns of PropertyGrid can be changed.</p>
    <div style="margin:20px 0;"></div>
    <table id="pg" class="easyui-propertygrid" style="width:300px; height:400px;" data-options="
                url: 'propertygrid_data1.json',
                method:'get',
                showGroup: true,
                scrollbarSize: 0
            ">
    </table>
    <script>
        var mycolumns = [[
            { field: 'name', title: 'MyName', width: 100, sortable: true },
               { field: 'value', title: 'MyValue', width: 100, resizable: false }
        ]];
    </script>
</body>
</html>

