﻿$(document).ready(function () {
    IniCheckChuNhat();
    iniOption();
    loadYears();
    enableChuNhat(false);
    iniComboboxTree();
    getBoPhan();
    IniDataGrid();
});

function test1() {
    //$("#dlg").dialog('open');
    //parent.$("#dlg").dialog("open");
    //$("#dlg", window.parent.document).width(1400);
    //$("#dlg", window.parent.document).height(600);
    //$("#dlg", window.parent.document).attr("title", "dialog标题");
    //window.parent.openDialog();
    //$("#dlg").dialog({ autoOpen: false,cache: false, modal: true });
    //parent.$("#dlg").dialog({ autoOpen: false, cache: false, modal: true });
    //$("#dlg", parent.document).dialog({ resizable: true, model: true });
    //$('#dlg').dialog({ modal: true });
    //window.parent.$('#dlg');

    //var $jParent = window.parent.jQuery.noConflict();
    //var dlg1 = $jParent('#dlg');
    //dlg1.dialog();

    //parent.$("#dlg").dialog({
    //    resizable: false,
    //    height: 140,
    //    modal: true
    //});

    //parent.$("#dlg").dialog("open");
    //$("#dlg").dialog({
    //    resizable: false,
    //    height: 200,
    //    width: 730,
    //    modal: true
    //});
};



function test() {
    var msg = "Báo cáo những nhân viên không chấm công. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================



            }//End (r)==============================================
        }
    });//end messager.confirm
};
function IniCheckChuNhat() {
    $("#chkAllCN").change(function () {
        if (this.checked) enableChuNhat(true);
        else
            enableChuNhat(false);
    });
}
function enableChuNhat(enable) {
    
    if (enable) {
        $("#chkCN1").removeAttr("disabled", 'disabled');
        $("#chkCN2").removeAttr("disabled", 'disabled');
        $("#chkCN3").removeAttr("disabled", 'disabled');
        $("#chkCN4").removeAttr("disabled", 'disabled');
        $("#chkCN5").removeAttr("disabled", 'disabled');
    } else {
        $("#chkCN1").attr("disabled", 'disabled');
        $("#chkCN2").attr("disabled", 'disabled');
        $("#chkCN3").attr("disabled", 'disabled');
        $("#chkCN4").attr("disabled", 'disabled');
        $("#chkCN5").attr("disabled", 'disabled');
    }
}

function getArrayCN() {
    var v0 = 0, v1 = 0, v2 = 0, v3 = 0, v4 = 0, v5 = 0;
    if ($("#chkAllCN").prop("checked")) v0 = 1;

    if ($("#chkCN1").prop("checked")) v1 = 1;
    if ($("#chkCN2").prop("checked")) v2 = 1;
    if ($("#chkCN3").prop("checked")) v3 = 1;
    if ($("#chkCN4").prop("checked")) v4 = 1;
    if ($("#chkCN5").prop("checked")) v5 = 1;
    return [v0, v1, v2, v3, v4, v5]
}

function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onBeforeSelect: function (index, row) {
            $(this).datagrid('clearSelections');
        }
    }).datagrid("keyCtr");

    $('#dg-list').datagrid({
        onBeforeSelect: function (index, row) {
            $(this).datagrid('clearSelections');
        }
    }).datagrid("keyCtr");

};

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });

}


function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    
}

function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================


function loadDataPaging(selectedPage, pageSize, DepId) {
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert(JSON.stringify(response)); }
    });
}

function GenerateVirtualData() {
    var rows = getCheckRows();
    if (rows == null) return;

    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Chuẩn hóa dữ liệu. Lưu ý những nhân viên nào chưa có sắp lịch trình ca làm việc khi chuẩn hóa sẽ không sinh ra.',
        draggable: false,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================
                var allowUpdated = $('#chkUpdate').prop('checked')
                $.ajax({
                    url: '/MCC/GenerateVirtualData',
                    type: "POST",
                    data: { AllowUpdated: allowUpdated,Month:getValueMonth(),Year:getValueYear(), arrayCN: JSON.stringify(getArrayCN()), items: JSON.stringify(rows) },
                    dataType: "json",
                    //async: false,
                    success: function (data) {
                        //parent.showAlert('Thông báo', 'Dữ liệu đã được cập nhật', 'info');
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', data);
                    },
                    error: function (response) {
                        alert(JSON.stringify(response));
                    }
                }); //End  $.ajax

            }//End (r)==============================================
        }
    });//End $.messager.confirm
}



function DeleteVirtualData() {
    var rows = getCheckRows();
    if (rows == null) return;

    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Xóa dữ liệu ảo',
        draggable: false,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================

                $.ajax({
                    url: '/MCC/DeleteVirtualData',
                    type: "POST",
                    data: { Month: getValueMonth(), Year: getValueYear(), items: JSON.stringify(rows) },
                    dataType: "json",
                    //async: false,
                    success: function (data) {

                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', data);
                    },
                    error: function (response) {
                        alert("ERROR: " + JSON.stringify(response));
                    }
                }); //End  $.ajax

            }//End (r)==============================================
        }
    });//End $.messager.confirm
}

function getCheckRows() {
    var rows = $('#dg-nhanvien').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return null;
    }
    return rows;
}


function getValueMonth() {
    return $('#cb-Months').combobox('getValue');
}

function getValueYear() {
    return $('#cb-Years').combobox('getValue');
}

function loadYears() {
    $('#cb-Years').combobox({
        panelWidth: 124,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/MCC/GetYears',
        onLoadSuccess: function () {
            var data = $(this).combobox('getData');
            $(this).combobox('setValue', data[0].id);
        },
    });
}

/*** THIẾT LẬP CHỌN NGÀY GIỜ=================================================================================== ***/

function iniOption() {
    var myDate = new Date();
    $('#cb-Months').combobox('setValue', myDate.getMonth() + 1);
}