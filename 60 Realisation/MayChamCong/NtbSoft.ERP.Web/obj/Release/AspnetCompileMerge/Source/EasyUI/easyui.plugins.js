﻿$.extend($.fn.datagrid.methods, {
    editOptions: {},
    editGrid: function (_e, options) {
        var defaults = {
            index: 0,
            field: '',
            type: 'row'
        }
        var opts = $.extend(defaults, options || {});
        var fields = _e.datagrid('getColumnFields', true).concat(_e.datagrid('getColumnFields'));
        var endEditing = function () {
            var editOptions = $.fn.datagrid.methods.editOptions;
            if (typeof editOptions[_e] == 'undefined') return true;
            var editIndex = editOptions[_e]['editIndex'];

            if (!_e.datagrid('validateRow', editIndex)) return false;
            for (var i = 0; i < fields.length; i++) {
                //结束编辑单元格，返回单元格数据
                var item = _e.datagrid('getEditor', { index: editIndex, field: fields[i] });
                if (item) {
                    switch (item.type) {
                        case 'combobox':
                            var comboboxItem = $(item.target).combobox('options');
                            var comboboxVal = $(item.target).combobox('getText');
                            _e.datagrid('getRows')[editIndex][comboboxItem.textField] = comboboxVal;
                            break;
                    }
                }
            }
            _e.datagrid('endEdit', editIndex);
            $.fn.datagrid.methods.editOptions[_e] = undefined;
            return true;
        };
        if (endEditing()) {
            $.fn.datagrid.methods.editOptions[_e] = { editIndex: opts.index };
            switch (opts.type) {
                case 'cell':
                    //开始将单元格的editor保存到另一个参数中，屏蔽除当前单元格外的editor事件，这样就可以避免多个单元格都触发编辑
                    for (var i = 0; i < fields.length; i++) {
                        var col = _e.datagrid('getColumnOption', fields[i]);
                        col.editor1 = col.editor;
                        if (fields[i] != opts.field) {
                            col.editor = null;
                        }
                    }
                    _e.datagrid('beginEdit', opts.index);
                    //将所有单元格的editor还原
                    for (var i = 0; i < fields.length; i++) {
                        var col = _e.datagrid('getColumnOption', fields[i]);
                        col.editor = col.editor1;
                    }
                    break;
                case 'row':
                    _e.datagrid('beginEdit', opts.index);
                    break;
            }
        }
    }
});

$.extend($.fn.tree.methods, {
    getAllChecked: function (_e) {
        //判断所选节点是否存在指定节点
        var hastarget = function (target) {
            for (var i = 0; i < nodes.length; i++) {
                if (target == nodes[i].target) return true;
            }
            return false;
        };

        var nodes = _e.tree('getChecked');
        if (nodes.length > 0) {
            for (var i = 0; i < nodes.length; i++) {
                var parent = _e.tree('getParent', nodes[i].target);
                //判断所选节点中是否存在该父节点
                if (parent && !hastarget(parent.target)) {
                    nodes.push(parent);
                }
            }
        }
        return nodes;
    }
});