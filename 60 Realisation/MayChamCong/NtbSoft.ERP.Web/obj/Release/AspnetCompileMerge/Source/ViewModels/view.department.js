﻿var treeGridData = [{
    "id": 'SONHACO',
    "Name": "Tổng cty may Sơn Hà",
    "DepID": "SONHACO.",
    "date": "02/19/2010",
    "IsChain": true,
    "children": [{
        "id": 'P.KH',
        "Name": "Phòng kế hoạch",
        "DepID": "P.KH",
        "date": "03/20/2010",
        "IsChain": false,
        "children": [{
            "id": 'P.KH-BB',
            "Name": "P.KH - Kho bao bì",
            "DepID": "P.KH.KBB",
            "date": "01/13/2010",
            "IsChain": false
        }, {
            "id": 'P.KH.PL',
            "Name": "P.KH - Kho phụ liệu",
            "DepID": "P.KH.KPL",
            "date": "01/13/2010",
            "IsChain": false
        }]
    }, {
        "id": 'PX01',
        "Name": "Phân xưởng 01",
        "DepID": "PX01",
        "date": "01/20/2010",
        "IsChain": false,
        "children": [{
            "id": 'TM.01',
            "Name": "Tổ may 01",
            "DepID": "TM.01",
            "date": "05/19/2009",
            "IsChain": true
        }, {
            "id": 'TM.02',
            "Name": "Tổ may 02",
            "DepID": "TM.02",
            "date": "04/20/2010",
            "IsChain": true
        }, {
            "id": 'TM.03',
            "Name": "Tổ may 03",
            "DepID": "TM.03",
            "date": "03/17/2005",
            "IsChain": true
        }]
    }]
}];

function GetAll() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data1) {

            $("#tg").treegrid({
                lines: true,
                singleSelect: true,
                selectOnCheck: false,
                checkOnSelect: false,
                icons: false,
                //state: 'open',
                loadFilter: myLoadFilter,
                loader: function (param, success, error) {
                    if (!param.id) {
                        data = data1;   // retrieve the top level data.
                        success(data);
                    }
                    else {
                        data = getData(param.id); // retrieve the children data of the current expanded node.
                        // this line still overrides the entire tree's data instead of appending the current data to the expanded parent
                        success(data);
                    }
                },
                onLoadSuccess: function (row, data) {

                    $('#tg').treegrid('expandAll');
                    //alert(data.length);
                    ////$('#tg').datagrid('getPanel').find('div.datagrid-header input[type=checkbox]').attr('disabled','disabled');
                    //var h = $('#tg').datagrid('getPanel').find('div.datagrid-header div.datagrid-header-check');
                    //h.html('<span style="font-size:12px">Trực tiếp SX</span>')
                    //for (i = 0; i < data.length; ++i) {

                    //    if (data[i].IsChain == true) $(this).treegrid('checkRow', i);
                    //}
                },
                onContextMenu: function (e, row) {
                    if (row) {
                        e.preventDefault();
                        $(this).treegrid('select', row.id);

                        if (row.ContextMenuId != "") {
                            $('#context-menu').menu('clear');
                            bindingContextMenu(row.id, row.ContextMenuId);
                            pageX = e.pageX;
                            pageY = e.pageY;
                        }
                        //$('#mn-dep').menu('show', {
                        //    left: e.pageX,
                        //    top: e.pageY
                        //});
                    }
                },
                onDblClickCell: function (row) {
                    // $(this).treegrid("beginEdit", row.id);
                    //alert(row.id);
                    //$('#tg').treegrid('beginEdit', row.id);
                    //$(this).treegrid('enableCellEditing').treegrid('gotoCell', {
                    //    index: 0,
                    //    idField: 'id'
                    //});

                }
            });


        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function GetAll=======================================================================================================================

$.extend($.fn.menu.methods, {
    clear: function (jq) {
        return jq.each(function () {
            var m = $(this);
            m.children('div.menu-item,div.menu-sep').each(function () {
                m.menu('removeItem', this);
            });
        });
    }
});

/*** Load ContextMenu theo MenuId ***/
function bindingContextMenu(MenuId, ContextMenuId) {
    $.ajax({
        url: "/api/v1/MenuMaster/GetContextMenu?MenuId=" + MenuId + "&ContextMenuId=" + ContextMenuId + "&MenuIdMain=" + "",
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var row = $.parseJSON(data);
            $(function () {
                $.each(row, function (i, item) {
                    if (item.text == '-') {
                        $('#context-menu').menu('appendItem', {
                            separator: true
                        });
                    } else
                        $('#context-menu').menu('appendItem', {
                            id: item.Id
                            , text: item.text
                            , disabled: item.disabled
                            , iconCls: item.iconCls
                            , onclick: item.onclick
                        });
                });//End each
            });//End $function

            /*** Hiện ContextMenu ***/
            $('#context-menu').menu('show', {
                left: pageX,
                top: pageY
            });
        },
        error: function (response) {
            //alert(response.d);
        }
    });
} //end function GetAll=======================================================================================================================



var idIndex = 100;

function append() {
    idIndex++;
    var d1 = new Date();
    var d2 = new Date();
    d2.setMonth(d2.getMonth() + 1);
    var node = $('#tg').treegrid('getSelected');
    $('#tg').treegrid('append', {
        parent: node.id,
        data: [{
            id: idIndex,
            Name: 'New Task' + idIndex,
            persons: parseInt(Math.random() * 10),
            begin: $.fn.datebox.defaults.formatter(d1),
            end: $.fn.datebox.defaults.formatter(d2),
            progress: parseInt(Math.random() * 100)
        }]
    })
}

function myLoadFilter(data, parentId) {

    function setData(data) {
        var todo = [];
        for (var i = 0; i < data.length; i++) {
            todo.push(data[i]);
        }
        while (todo.length) {
            var node = todo.shift();
            if (node.children && node.children.length) {
                node.state = 'closed';
                node.children1 = node.children;
                node.children = undefined;
                todo = todo.concat(node.children1);
            }
        }
    }

    setData(data);
    var tg = $(this);
    var opts = tg.treegrid('options');
    opts.onBeforeExpand = function (row) {
        if (row.children1) {
            tg.treegrid('append', {
                parent: row[opts.idField],
                data: row.children1
            });
            row.children1 = undefined;
            tg.treegrid('expand', row[opts.idField]);
        }
        return row.children1 == undefined;
    };
    return data;
}

function removeIt() {
    var node = $('#tg').treegrid('getSelected');
    if (node) {
        $('#tg').treegrid('remove', node.id);
    }
}


function checkOnchange(e) {
    //alert(e.checked);
    $(".group-Chain").attr("disabled", !e.checked);
}

function removeit(menuItem) {
    var node = $('#tg').treegrid('getSelected');
    // var Id = node.id, ParentId = node.MenuParent;
    $.messager.confirm('Xác nhận', 'Bạn có chắc xóa chứ?', function (r) {
        if (r) {
            $.ajax({
                url: "/Setting/Delete?DepID=" + node.id,
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data == true) {
                        $('#tg').treegrid('remove', node.id);
                    }
                },
                error: function (response) {
                    alert("removeit.Error: " + response.d);
                    //window.location.href = "/Login.aspx";
                }
            });//End ajax
        }
    });
}

function edit(menuItem) {
    $("#frm-edit").window({ title: 'Chỉnh sửa', iconCls: 'icon-edit' });

    var node = $('#tg').treegrid('getSelected');
    $(".dep-id").textbox({ value: node.id, disabled: true });
    $(".dep-name").textbox("setValue", node.Name);
    $(".dep-sort").numberspinner("setValue", node.Sort);

    $("input[name=IsChain]").prop("checked", node.IsChain);
    $("input[name=IsChainSew]").prop("checked", node.IsChainSew);
    $("input[name=IsChainCut]").prop("checked", node.IsChainCut);
    $("input[name=IsChainPackage]").prop("checked", node.IsChainPackage);
    $("input[name=IsWarehouse]").prop("checked", node.IsWarehouse);
    $(".group-Chain").attr("disabled", !node.IsChain);
    openFrom();
}

var _ContextMenuId = "", _ContextMenuIdFile = "";
function CreateDirectory(menuItem) {
    /*** Tạo thư mục**/
    resetValue();
    var node = $('#tg').treegrid('getSelected');
    _ContextMenuId = node.ContextMenuId;
    _ContextMenuIdFile = node.ContextMenuIdFile;

    $("#frm-edit").window({ title: 'Thêm phòng ban', iconCls: 'icon-organize-add' });
    openFrom();
}

function NewFile(menuItem) {
    /*** ***/
    var node = $('#tg').treegrid('getSelected');
    _ContextMenuId = node.ContextMenuIdFile;
    _ContextMenuIdFile = "";
    $("#frm-edit").window({ title: 'Thêm đơn vị', iconCls: 'icon-link-add' });
    resetValue();
    openFrom();
}

function openFrom() {
    /*** Hiện form Edit ***/
    $("#frm-edit").window("center");
    $("#frm-edit").window("open");

}//end function closeFrom()=======================================================================================================================

function closeFrom() {
    /*** Đóng form Edit ***/
    $("#frm-edit").window("close");
}//end function closeFrom()=======================================================================================================================

function resetValue() {
    $(".dep-id").textbox({ value: "", disabled: false });
    $(".dep-name").textbox("setValue", "");

    $("input[name=IsChain]").prop("checked", false);
    $("input[name=IsChainSew]").prop("checked", false);
    $("input[name=IsChainCut]").prop("checked", false);
    $("input[name=IsChainPackage]").prop("checked", false);
    $("input[name=IsWarehouse]").prop("checked", false);
    $(".group-Chain").attr("disabled", true);
}
function save() {
    //$('#frm-edit').form('load', {
    //    DepName: 'myname',
    //    DepID: 'subject'
    //});
    var node = $('#tg').treegrid('getSelected');
    //alert(node.ContextMenuId);
    var DepID = "", DepName = "";
    DepID = $("input[name=DepID]").val();
    DepName = $("input[name=DepName]").val();

    var IsChain = $("input[name=IsChain]").prop("checked"),
        IsChainSew = false,
        IsChainCut = false, IsChainPackage = false,
        IsWarehouse = $("input[name=IsWarehouse]").prop("checked");

    if (IsChain) {
        IsChainSew = $("input[name=IsChainSew]").prop("checked");
        IsChainCut = $("input[name=IsChainCut]").prop("checked");
        IsChainPackage = $("input[name=IsChainPackage]").prop("checked");
    }
    var mSort = $(".dep-sort").numberspinner("getValue");
    $.ajax({
        url: "/Setting/Update?DepID=" + DepID + "&DepName=" + encodeURIComponent(DepName) + "&ParentID=" + node.id +
            "&ContextMenuId=" + _ContextMenuId + "&ContextMenuIdFile=" + _ContextMenuIdFile +
            "&IsChain=" + IsChain + "&IsChainSew=" + IsChainSew + "&IsChainCut=" + IsChainCut +
            "&IsChainPackage=" + IsChainPackage + "&IsWarehouse=" + IsWarehouse + "&Sort=" + mSort,
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data == true) {
                closeFrom();
                GetAll();
            }
            else
                alert(data);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

function newDep() {
    $.ajax({
        url: '/Setting/GetNewDep',
        type: "GET",
        dataType: "json",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#frm-edit').form('load', {
                DepName: data.Name,
                DepID: data.id
            });

            $(".IsChain").prop("checked", data.IsChain);
            $(".group-Chain").attr("disabled", !data.IsChain);
        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
}