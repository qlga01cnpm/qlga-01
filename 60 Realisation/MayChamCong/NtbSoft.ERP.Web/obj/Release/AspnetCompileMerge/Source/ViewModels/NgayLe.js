﻿$(document).ready(function () {
    IniDataGrid();
    getAll();
});


function IniDataGrid() {
 
    $('#gridlist').datagrid({
        onBeforeSelect: function (index, row) {
            $(this).datagrid('clearSelections');
        }
    }).datagrid("keyCtr");

};


function getAll() {
    $.ajax({
        url: '/Setting/NgayLeGet',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#gridlist').datagrid('loadData', data);
            
        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getAll()=======================================================================================================================
function refresh() {
    getAll();
}
function addNew() {
    var row = {};
    row.Id = 0;

    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('clear');
    $('#fm').form('load', row);
}
function edit() {
    var row = $('#gridlist').datagrid('getSelected');
    if (row) {
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Cập nhật', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
    }
}

function closeForm() {
    $('#dlg').dialog('close');
}


function saveInfo() {
    var Item = $('#fm').serializeObject();
    $.ajax({
        url: '/Setting/NgayLeSave',
        type: "POST",
        data: { ngay: SetFormatMDY(Item.StrNgay), name: Item.Name },
        dataType: "json",
        //async: false,
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                parent.showAlert('Thông báo', data, 'warning');
                return;
            }
            parent.bottomRight('Thông báo', 'Lưu thành công!');
            closeForm();
            refresh();
        },
        error: function (response) {
        }
    }); //End  $.ajax
}

function removeIt() {
    var row = $('#gridlist').datagrid('getSelected');
    if (!row) return;

    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Bạn chắc chắn xóa ngày lễ?',
        draggable: false,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================

                $.ajax({
                    url: '/Setting/NgayLeDelete',
                    type: "POST",
                    data: { id: row.Id },
                    dataType: "json",
                    //async: false,
                    success: function (data) {
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', 'Xóa thành công!');
                        closeForm();
                        var index = $('#gridlist').datagrid('getRowIndex', row);
                        $('#gridlist').datagrid('deleteRow', index);
                    },
                    error: function (response) {
                    }
                }); //End  $.ajax

            }//End (r)==============================================
        }
    });//End $.messager.confirm
}

function addNewAuto() {
    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Chắc chắn tự động thêm ngày lễ trong năm?',
        draggable: false,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================

                $.ajax({
                    url: '/Setting/NgayLeAddAuto',
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', 'Thêm ngày lễ thành công!');
                        closeForm();
                        getAll();
                    },
                    error: function (response) {
                    }
                }); //End  $.ajax

            }//End (r)==============================================
        }
    });//End $.messager.confirm
}