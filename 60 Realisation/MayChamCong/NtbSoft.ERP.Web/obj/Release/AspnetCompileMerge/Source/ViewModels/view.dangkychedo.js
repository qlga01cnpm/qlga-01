﻿var isAddNew = false;
$(document).ready(function () {
    IniDataGrid();
    iniComboboxTree();
    getBoPhan();
    loadShifts();
});

function refresh() {
    loadDataDetail();
}

function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onSelect: gridLog_onSelect
        
    }).datagrid('keyCtr');

    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (row.SoNgayConLai <= 3) {
                return 'background-color: #E79989; color: #fff; font-weight: bold;text-align:left';
            }
        }
    });
}

function gridLog_onSelect(index, row) {
    loadDataDetail();
}

//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}
function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    loadDataDetail();

}

function getBoPhan() {
    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        }, error: function (response) { alert(JSON.stringify(response)); parent.closeProgress(); }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "0";
    return tNode.id;
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') {
        row = $('#dg-list').datagrid('getSelected');
        if (row)
            return row.MaNV;

        return "";
    }
    return row.MaNV;
}

function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}
function loadDataDetail() {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/Setting/DangKyCheDoGet?DepId=" + getDepId() + "&MaNV=" + getMaNV();
    //alert(newUrl);
    $.ajax({
        url: newUrl,
        type: "GET",
       //timeout: 60000,
        //async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                $('#dg-list').datagrid('loadData', data);
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function AddNew() {
    isAddNew = true;
    //var row = $('#dg-list').datagrid('getSelected');
    //if (row != null) {
    //    parent.showAlert('Thông báo', 'Nhân viên [' + row.TenNV + '] đã đăng ký ca [' + row.ShiftID + ']', 'warning');
    //    return;
    //}
    var manv = getMaNV();
    if (manv == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return;
    }
    var newRow = {};
    newRow.ID = 0;
    newRow.MaNV = manv;
    newRow.StrTuNgayDMY = formatterDMY(new Date());
    newRow.StrToiNgayDMY = formatterDMY(new Date());
    newRow.MaCheDo = 1;
    $('#fm').form('clear');
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('load', newRow);
}

function editInfo() {
    isAddNew = false;
    var row = getRow();

    if (row) {
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Chỉnh sửa', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
    }
}

function saveInfo() {
    
    try {
        var Temp = $('#fm').serializeObject();
        var act = "PUT";
        if (isAddNew) act = "POST";
        //Temp.TuNgay = Temp.StrTuNgayDMY;
        //Temp.ToiNgay = Temp.StrToiNgayDMY;
        //Temp.MaCheDo = $("#fm input[type='radio']:checked").val();
        $.ajax({
            url: '/Setting/DangKyCheDoUpdate',
            type: "POST",
            data: { action: act, item: JSON.stringify(Temp) },
            dataType: "json",
            //async: false,
            success: function (data) {
                if (data.indexOf("ERROR:") > -1) {
                    parent.showAlert('Thông báo', data, 'warning');
                    return;
                }
                parent.bottomRight('Thông báo', 'Lưu thành công!');
                closeForm();
                loadDataDetail();
            },
            error: function (response) {
                //alert(JSON.stringify(response));
            }
        }); //End  $.ajax

    } catch (e) {
        alert("ERROR: " + JSON.stringify(e));
    }
}

function removeIt() {
    var row = getRow();
    if (row == null) return;

    parent.$.messager.confirm({
        draggable: false,  title: 'Xác nhận', msg: 'Xóa dữ liệu thai sản ' + row.TenNV,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================

                $.ajax({
                    url: '/Setting/DangKyCheDoUpdate',
                    type: "POST",
                    data: { action: 'DELETE', item: JSON.stringify(row) },
                    dataType: "json",
                    //async: false,
                    success: function (data) {
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', 'Đã xóa đăng ký thai sản ' + row.TenNV);
                        var rowIndex = $('#dg-list').datagrid("getRowIndex", row);
                        $('#dg-list').datagrid("deleteRow", rowIndex);
                    },
                    error: function (response) {
                        alert(JSON.stringify(response));
                    }
                }); //End  $.ajax


            }//End (r)==============================================
        }
    });//End $.messager.confirm
}

function getRow() {
    var rowSelect = $('#dg-list').datagrid('getSelected');
    if (rowSelect == null || rowSelect == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return null;
    }
    return rowSelect;
}

function closeForm() {
    $('#dlg').dialog('close');
}

/*** Load ca làm việc ***/
function loadShifts() {
    $('#cb-shifts').bindingShifts();
}

/***  ***/
function exportCheDo(MaCheDo) {
    var msg = "Báo cáo những nhân viên đăng ký chế độ. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================


                var DepId = getDepId();
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpDangKyCheDo?fromDate=" +"" + "&toDate=" +""+ "&DepId=" + DepId + "&MaCheDo=" + MaCheDo, true);;
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Ds_lao_dong_che_do.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}