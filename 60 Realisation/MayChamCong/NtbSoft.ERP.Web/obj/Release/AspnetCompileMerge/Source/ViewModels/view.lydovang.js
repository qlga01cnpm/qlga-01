﻿$(document).ready(function () {
    IniDataGrid();
    getAll();
});

var _mAction = "GET";
function IniDataGrid() {
 
    $('#gridlist').datagrid({
        onBeforeSelect: function (index, row) {
            $(this).datagrid('clearSelections');
        }
    }).datagrid("keyCtr");

};


function getAll() {
    $.ajax({
        url: '/Setting/LyDoVangGetBy',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#gridlist').datagrid('loadData', data);
            
        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getAll()=======================================================================================================================
function refresh() {
    getAll();
}
function addNew() {
    var row = {};
    row.Id = 0;
    _mAction = "POST";
    $('#MaSo').textbox('readonly', false);
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('clear');
    $('#fm').form('load', row);
}
function edit() {
    var row = $('#gridlist').datagrid('getSelected');
    if (row) {
        _mAction = "PUT";
        $('#MaSo').textbox('readonly');
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Cập nhật', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
    }
}

function closeForm() {
    $('#dlg').dialog('close');
}


function saveInfo() {
    var Item = $('#fm').serializeObject();
    $.ajax({
        url: '/Setting/LyDoVangModify',
        type: "POST",
        data: { action: _mAction, form: JSON.stringify(Item) },
        dataType: "json",
        //async: false,
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                parent.showAlert('Thông báo', data, 'warning');
                return;
            }
            parent.bottomRight('Thông báo', 'Lưu thành công!');
            closeForm();
            refresh();
        },
        error: function (response) {
        }
    }); //End  $.ajax
}

function removeIt() {
    var row = $('#gridlist').datagrid('getSelected');
    if (!row || row.MaSo == "P") return;
    
    parent.$.messager.confirm({
        title: 'Xác nhận',
        msg: 'Bạn cần chắc xóa không?',
        draggable: false,
        fn: function (ok) {
            if (ok) {//Begin (r) =====================================
                _mAction = "DELETE";
                $.ajax({
                    url: '/Setting/LyDoVangModify',
                    type: "POST",
                    data: { action: _mAction, form: JSON.stringify(row) },
                    dataType: "json",
                    //async: false,
                    success: function (data) {
                        if (data.indexOf("ERROR:") > -1) {
                            parent.showAlert('Thông báo', data, 'warning');
                            return;
                        }
                        parent.bottomRight('Thông báo', 'Xóa thành công!');
                        closeForm();
                        var index = $('#gridlist').datagrid('getRowIndex', row);
                        $('#gridlist').datagrid('deleteRow', index);
                    },
                    error: function (response) {
                        beginTinhCong(rows, ++index);
                        //alert(JSON.stringify(response));
                    }
                }); //End  $.ajax

            }//End (r)==============================================
        }
    });//End $.messager.confirm
}
