﻿//给easyui创建扩展属性
var easyui = easyui || {};

/**
 * 系统确认提示框
 * @param message
 * @param callback
 */
easyui.confirm = function (message, callBack) {
    try {
        parent.$.messager.confirm('系统提示', message, function (isOk) {
            if (!isOk) return false;
            try {
                callBack.call(this, isOk);
            } catch (e) { return true; }
        });
    } catch (e) {
        if (confirm(message)) {
            try {
                callBack.call(this, isOk);
            } catch (e) { return true; }
        }
    }

}

/**
 * 系统提示框 Hộp nhắc hệ thống
 * @param message
 * @param callback
 * @param icon
 */
easyui.alert = function (message, callback, icon) {
    icon = empty(icon) ? 'info' : icon;
    try {
        parent.$.messager.alert('Thông báo', message, icon, function () {
            try {
                callback.call(this);
            } catch (e) { return true; }
        });
    } catch (e) {
        alert(message);
        try {
            callback.call(this);
        } catch (e) { return true; }
    }

}

/**
 * 进度条
 * @param type
 */
easyui.progress = function (type) {
    type = empty(type) ? 'close' : type;
    try {
        parent.$.messager.progress(type);
    } catch (e) { }

}

/**
 * 封装dialog弹出框，默认以iframe方式加载
 * @param options
 * @returns {*|$}
 */
easyui.dialog = function (options) {
    var dgWidows = parent.$('<div/>'), formObj, result, frameObj, frameBody, frameName;
    var opts = $.extend({
        title: '&nbsp;', width: 640, height: 480, modal: true, handler: false, maximizable: true,
        onOpen: function () {
            //初始化框架提交事件
            setFrame();
        },
        buttons: [{
            text: '确定',
            iconCls: 'icon-ok',
            handler: function () {
                //尝试提交表单，如果存在的话
                if (formObj.length == 1) {
                    formObj.submit();
                } else {
                    try {
                        //获取返回数据
                        result = frameObj.onReturn();
                        try {
                            opts.handler.call(this, result);
                        } catch (e) { }
                    } catch (e) { }
                    try {
                        //执行确认函数
                        frameObj.onConfirm();
                    } catch (e) { }
                    dgWidows.dialog('destroy');
                }
            }
        }, {
            text: '取消',
            handler: function () {
                dgWidows.dialog('destroy');
            }
        }],
        toolbar: false
    }, options);

    //创建完整的URL
    if (!empty(opts.data) && !empty(opts.href)) {
        opts.href = http_build_query(opts.href, opts.data);
        delete opts.data;
    }

    //创建窗口内容
    if (!empty(opts.href)) {
        frameName = 'iframe-' + Math.random();
        opts.content = '<iframe name="' + frameName + '" src="' + opts.href + '" width="100%" height="100%" frameborder="0"></iframe>';
        delete opts.href;
    }

    //初始化框架
    var setFrame = function () {
        frameObj = dgWidows.find('[name="' + frameName + '"]').get(0).contentWindow;
        frameObj.onload = function () {
            frameBody = $(frameObj.document.body);
            formObj = frameBody.find('form');
            if (formObj.length == 1) {
                formObj.submit(function () {
                    result = frameObj.easyui.submit(formObj);
                    if (!result) return false;
                    parent.easyui.alert(result.info, function () {
                        if (result.status) {
                            //如果提交成功，关闭弹出窗口
                            dgWidows.dialog('destroy');
                            try {
                                //如果回调函数存在则调用
                                opts.handler.call(this, result);
                            } catch (e) { }
                        }
                    });
                    return false;
                });
            }
        }
    };

    opts.cache = false; //强制不缓存数据
    dgWidows.dialog(opts);
    return dgWidows;
}

/**
 * 提交表单
 * @param Url
 * @param ItemObj
 * @constructor
 */
easyui.submit = function (formObject, callback) {
    formObject = easyui.create(formObject);
    easyui.progress();	//显示进度条
    var isValid = formObject.form('validate');
    if (!isValid) {
        easyui.progress('close'); //结束进度条
        return false;
    }
    if (empty(formObject.attr('data-url'))) return false;
    var result = easyui.ajaxCall(formObject.attr('data-url'), formObject.serialize(), function () {
        easyui.progress('close'); //结束进度条
    }, 'post');
    try {
        callback.call(this, result);
    } catch (e) { }
    return result;
}

/**
 * 创建对象
 * @param element
 * @returns {*|jQuery|HTMLElement}
 */
easyui.create = function (element) {
    if (
        typeof (element) == 'string'
        && element.indexOf('#') < 0
        && element.indexOf('.') < 0
    ) {
        element = '#' + element;
    }
    return $(element);
}

/**
 * 全选树
 * @param _e
 */
easyui.checkAll = function (element) {
    element = easyui.create(element);
    var roots = element.tree('getRoots');
    for (var i = 0; i < roots.length; i++) {
        element.tree('check', roots[i].target);
    }
}

/**
 * 反选树
 * @param _e
 */
easyui.unCheck = function (element) {
    element = easyui.create(element);
    var roots = element.tree('getRoots');
    for (var i = 0; i < roots.length; i++) {
        element.tree('uncheck', roots[i].target);
    }
}

/**
 * 发起ajax请求，并返回数据
 * @param url
 * @param data
 * @param callback
 * @param type
 * @returns {*}
 */
easyui.ajaxCall = function (url, data, callback, calltype, returntype) {
    calltype = empty(calltype) ? 'POST' : calltype.toUpperCase();
    returntype = empty(returntype) ? 'JSON' : returntype.toUpperCase();
    var result = $.ajax({ type: calltype, data: data, url: url, async: false, cache: false }).responseText;
    if (returntype == 'JSON') result = $.parseJSON(result);
    try {
        callback.call(this, result);
    } catch (e) { }
    return result;
}