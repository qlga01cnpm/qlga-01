﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="NtbSoft.ERP.Web.DemoEasyUI.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 300px;
        }
    </style>
</head>
<body>
    <table style="width: 98%">
        <tr>
            <td class="auto-style1">Tiền tệ</td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td class="auto-style1">Tỷ giá</td>
            <td colspan="2">  </td>
        </tr>
        <tr>
            <td class="auto-style1">Chi phí gốc</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="auto-style1">Chi phí bán hàng</td>
            <td class="col-cpbh-value"></td>
            <td class="col-cpbh-value"></td>
        </tr>
        <tr>
            <td class="auto-style1">Hạn ngạch</td>
            <td class="col-cpbh-value"></td>
            <td class="col-cpbh-value"></td>
        </tr>

        <tr>
            <td class="auto-style1">Tổng cộng</td>
            <td class="border-t col-cpbh-value"></td>
            <td class="border-t col-cpbh-value"></td>
        </tr>
    </table>
</body>
</html>
