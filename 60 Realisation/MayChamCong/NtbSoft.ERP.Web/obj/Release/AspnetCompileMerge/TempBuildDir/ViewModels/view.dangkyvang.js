﻿function changeColumnTitle(obj) {
    var panel = $(obj).datagrid("getPanel");
    //var myheaderCol = panel.find("div.datagrid-header td[field='D2']");
    //// here is to add the css style
    //myheaderCol.css("background-color", "red");
    //var $field = $('td[field=' + 'D2' + ']', panel);
    //if ($field.length) {
    //    var $span = $('span', $field).eq(0);
    //    $span.html('2-T7');
    //}
    $.ajax({
        url: "/MCC/GetOptionDay",
        type: "GET",
        async: false,
        data: { month: getValueMonth(), year: getValueYear() },
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var col = 'D' + (i + 1);    //Lấy tên cột
                var myheaderCol = panel.find("div.datagrid-header td[field='" + col + "']");
                // here is to add the css style
                if (data[i].Value.indexOf('CN') > 0) {
                    myheaderCol.css("background-color", "red");
                    //var c = $(obj).datagrid('getColumnOption', col);
                    //c.styler = function () {
                    //    return 'background-color:red;color:#fff';
                    //};
                    //$(obj).datagrid('refreshRow', 0);

                } else if (data[i].Value.indexOf('T7') > 0) {
                    myheaderCol.css('background', '#19AC64');
                }
                else
                    myheaderCol.css("background", "#006699");

                var $field = $('td[field=' + col + ']', panel);
                if ($field.length) {
                    var $span = $('span', $field).eq(0);
                    $span.html(data[i].Value);
                }
                // Hiện nhưng cột từ ngày 27 trở đi
                if (i > 26)
                    $(obj).datagrid('showColumn', col);
            }

            var minDay = data.length;
            for (var i = 1 ; i <= (31 - minDay) ; i++) {
                var col = 'D' + (minDay + i);
                $(obj).datagrid('hideColumn', col);
            }


        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
        }
    });
}

function formatStyler(value, row, index) {
    //for (var name in row) {
    //    var opts = $(this).datagrid('getColumnOption', name);
    //    alert(JSON.stringify(opts));

    //}
}

function getValueMonth() {
    return $('#cb_option').combobox('getValue');
}

function getValueYear() {
    return $('#cb-Years').combobox('getValue');
}

function loadYears() {
    $('#cb-Years').combobox({
        panelWidth: 124,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/MCC/GetYears',
        onLoadSuccess: function () {
            var data = $(this).combobox('getData');
            $(this).combobox('setValue', data[0].id);
        },
    });
}

function refresh() {
    loadDataBy(getDepId(), '');

    //$('#cb-lydo').combobox({
    //    valueField: 'id',
    //    textField: 'name',
    //    mode: 'remote',
    //    method: 'get',
    //    url: '/Setting/JDataComboBoxLyDoVang'
    //});

   
}

function getRow() {
    var rowSelect = $('#dg-list').datagrid('getSelected');
    if (rowSelect == null || rowSelect == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return null;
    }
    return rowSelect;
}


function AddNew() {
   
    var manv = getMaNV();
    if (manv == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return;
    }
    var newRow = {};
    newRow.ID = 0;
    newRow.MaNV = manv;
    newRow.StrTuNgay = formatterDMY(new Date());
    newRow.StrToiNgay = formatterDMY(new Date());
    newRow.MaCheDo = 1;
    $('#fm').form('clear');
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('load', newRow);
}

//function formatDateMDY(date) {
//    var myDate = date;
//    var output = (myDate.getMonth() + 1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();

//    return output;
//}

function editInfo() {
    var row = getRow();

    if (row) {
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Chỉnh sửa', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
    }
}

function closeForm() {
    $('#dlg').dialog('close');
}

function saveChange() {
    //$('#dg-list').datagrid('acceptChanges');
    var rows = $('#dg-list').datagrid('getChanges');
    if (rows.length == 0) return;

    $.ajax({
        url: '/Setting/DangKyVangSave',
        type: "POST",
        data: { Month: getValueMonth(), Year: getValueYear(), items: JSON.stringify(rows) },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1)
                alert(data);
            else
                parent.showMessager('Thông báo', 'Lưu thành công!');
           
        },
        error: function (response) {
        }
    });

}

function ok() {

    var row = $('#dg-nhanvien').datagrid('getSelected');
    if (row == null) {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return;
    }
    var value = $('#cb-lydo').combobox('getValue');
    if (value == null || value=="") {
        parent.showAlert('Thông báo', 'Chưa chọn lý do', 'warning');
        return;
    }
    var Temp = $('#fm').serializeObject();
    var rows = $('#dg-list').datagrid('getRows');

    $.ajax({
        url: '/Setting/DangKyVangSaveFrom2',
        type: "POST",
        data: { eID: row.Id, fromDateDMY: Temp.StrTuNgay, toDateDMY: Temp.StrToiNgay, MaLyDo: value, items: JSON.stringify(rows) },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1)
                alert(data);
            else
                parent.showMessager('Thông báo', 'Lưu thành công!');

            loadDataBy(getDepId(), getMaNV());
        },
        error: function (response) {
        }
    });
}

function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onSelect: gridLog_onSelect
    }).datagrid('keyCtr');

}
function gridLog_onSelect(index, row) {
    loadDataBy(getDepId(), row.MaNV);
}

/*** THIẾT LẬP CHỌN NGÀY GIỜ=================================================================================== ***/

function iniOption() {
    $('#cb_option').combobox({
        onChange: function (newValue, oldValue) {
        }
    });
    var myDate = new Date();
    $('#cb_option').combobox('setValue', myDate.getMonth() + 1);

    $('#cb-lydo').combobox({
        panelWidth: 190,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/Setting/JDataComboBoxLyDoVang'
    });
}

//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}

function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    loadDataBy(newValue, '');
}

function getBoPhan() {

    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "0";
    return tNode.id;
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') return "";
    return row.MaNV;
}

function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}

function loadDataBy(DepId, MaNV) {
    //$('#dg-list').datagrid('loadData', []);
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    $.ajax({
        url: '/Setting/DangKyVangGetBy',
        type: "GET",
        data: { Month: getValueMonth(), Year: getValueYear(), DepId: DepId, MaNV: MaNV },
        dataType: "json",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }

                $('#dg-list').datagrid('loadData', data);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert("ERROR_loadDataDetail: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}



function Demo() {
    alert("DEMO");
}

function Demo1() {
    alert("DEMO 1");
}