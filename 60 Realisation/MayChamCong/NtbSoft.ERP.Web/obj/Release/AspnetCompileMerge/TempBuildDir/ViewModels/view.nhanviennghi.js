﻿function IniDataGrid() {
    $('#dg-list').datagrid({
        onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        }
    }).datagrid('keyCtr');
}
function refresh() {
    getAll();
}
function getAll() {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    $.ajax({
        url: "/Setting/NhanVienNghiGet",
        type: "GET",
        //async: false,
        data: { value: '', FromDate: getDateFrom(), ToDate: getDateTo() },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (JSON.stringify(data).indexOf('ERROR:') > -1) {
                alert(data);
                parent.closeProgress(); return;
            }

            $('#dg-list').datagrid({
                data: data,
                rowStyler: function (index, row) {
                    if (index % 2 > 0) {
                        return 'background:#f0f0f0';
                    }
                }
            });
            parent.closeProgress();

        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function restore() {
    if (checkDelete()) return;
    var msg = 'Khôi phục những nhân viên. Bạn có chắc chắn muốn khôi phục lại không?';
    $.messager.confirm({
        title: 'Xác nhận', left: '13%', top: '10%', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                var rows = $('#dg-list').datagrid('getChecked');
                parent.setProgress("Vui lòng chờ");
                beginModify("PUT", rows, 0);

            }//End (r)==============================================
        }
    });
}
function xacnhan(title, msg) {
    alert(")");
}


function checkDelete() {
    var rows = $('#dg-list').datagrid('getChecked');
    if (rows == null || rows == '') {
        parent.showAlert('Thông báo', 'Chưa chọn nhân viên', 'warning');
        return true;
    }
    return false;
}

function beginModify(action, rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang xử lý...");
    $.ajax({
        url: "/Setting/NhanVienChoNghRestore",
        //async: false,
        type: "POST",
        data: { action: action, MaNV: rows[index].MaNV },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                parent.setProgressMsg("Lỗi: " + data);
            } else {
                var rowIndex = $('#dg-list').datagrid("getRowIndex", rows[index]);
                $('#dg-list').datagrid("deleteRow", rowIndex);
            }

            beginModify(action, rows, ++index)
        },
        error: function (response) {
            beginChangeDep(action, rows, ++index)
        }
    });
}


function exportExcel() {
    var msg = "Báo cáo nhân viên nghỉ việc. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================


                var DepId = "DEP.ROOT";
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpNhanVienNghi?fromDate=" + getDateFrom() + "&toDate=" + getDateTo(), true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Ds_nhan_vien_nghi_viec.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}