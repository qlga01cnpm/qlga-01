﻿
function formatStyler(value, row, index) {
    //for (var name in row) {
    //    var opts = $(this).datagrid('getColumnOption', name);
    //    alert(JSON.stringify(opts));

    //}
}

function getValueMonth() {
    return 10;
}

function getValueYear() {
    return $('#cb-Years').combobox('getValue');
}

function loadYears() {
    $('#cb-Years').combobox({
        panelWidth: 124,
        editable: false, panelHeight: 100,
        valueField: 'id',
        textField: 'name',
        mode: 'remote',
        method: 'get',
        url: '/MCC/GetYears',
        onLoadSuccess: function () {
            var data = $(this).combobox('getData');
            $(this).combobox('setValue', data[0].id);
        },
    });
}

function refresh() {
    loadDataBy(getDepId(), '');
}

function saveChange() {
    //$('#dg-list').datagrid('acceptChanges');
    var rows = $('#dg-list').datagrid('getChanges');
    if (rows.length == 0) return;

    $.ajax({
        url: '/Setting/DangKyVangSave',
        type: "POST",
        data: { Month: getValueMonth(), Year: getValueYear(), items: JSON.stringify(rows) },
        dataType: "json",
        success: function (data) {
            if (data.indexOf("ERROR:") > -1)
                alert(data);
            else
                parent.showMessager('Thông báo', 'Lưu thành công!');
        },
        error: function (response) {
        }
    });

}
function IniDataGrid() {
    $('#dg-nhanvien').datagrid({
        onSelect: gridLog_onSelect
    }).datagrid('keyCtr');

}
function gridLog_onSelect(index, row) {
    loadDataBy(getDepId(), row.MaNV);
}


//================================================================================================================

function iniComboboxTree() {
    $('#cb-mabophan').combotree({
        lines: true,
        onChange: depOnChange
    });
}

function depOnChange(newValue, oldValue) {
    clearSelections();
    loadDataPaging(1, 10000, newValue);
    loadDataBy(newValue, '');
}

function getBoPhan() {

    $.ajax({
        url: '/Setting/GetTreeNodeDep',
        type: "GET",
        dataType: "json",
        //async: false,
        data: "{}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#cb-mabophan').combotree('loadData', data);
            $('#cb-mabophan').combotree('setValue', 'DEP.ROOT');

        },
        error: function (response) {
            //alert(response.d);
            //window.location.href = "/Login.aspx";
        }
    });
} //end function getBoPhan()=======================================================================================================================

function loadDataPaging(selectedPage, pageSize, DepId) {
    var newUrl = "/Setting/NhanVienGetByDep?selectedPage=" + selectedPage + "&pageSize=" + pageSize + "&DepId=" + DepId + "&TenNV=" + "";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        async: false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data); return;
                }
                var item = $.parseJSON(data);
                $('#dg-nhanvien').datagrid('loadData', item.Tag);

            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }

        }, error: function (response) { alert(JSON.stringify(response)); }
    });
}

/*** LẤY THÔNG TIN CHI TIẾT VÀO - RA ==================================================================***/
function getDepId() {
    var t = $('#cb-mabophan').combotree("tree");
    var tNode = t.tree('getSelected');
    if (tNode == null) return "DEP.ROOT";
    return tNode.id;
}

function getMaNV() {
    var row = $('#dg-nhanvien').datagrid("getSelected");
    if (row == null || row == '') return "";
    return row.MaNV;
}
function clearSelections() {
    $('#dg-nhanvien').datagrid("clearSelections");
}
function loadDataBy(DepId, MaNV) {
    //$('#dg-list').datagrid('loadData', []);

    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang xử lý dữ liệu ...");
    $.ajax({
        url: '/Setting/TheoDoiPhepNamGetBy',
        type: "GET",
        data: { Month: getValueMonth(), Year: getValueYear(), DepId: DepId, MaNV: MaNV },
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress(); return;
                }

                $('#dg-list').datagrid('loadData', data);
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();

        }, error: function (response) {
            alert("ERROR_loadDataDetail: " + JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function Demo() {
    alert("DEMO");
}
function Demo1() {
    alert("DEMO 1");
}


function exportExcel() {
    var msg = "Báo cáo theo dõi phép năm. Bạn có muốn xuất báo cáo không?";
    parent.$.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                var DepId = getDepId();
               
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpPhepNam?DepId=" + DepId + "&Years=" + getValueYear(), true);;
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "So_theo_doi_phep_nam.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}