﻿function iniCheckBox() {
    $("input[name=IsThaiSan]").change(function () {
        if (this.checked) enableThaiSan(false);
        else
            enableThaiSan(true);
    });

    //$("input[name=IsTinhLamThem]").change(function () {
    //    if (this.checked) enableLamThem(false);
    //    else
    //        enableLamThem(true);
    //});
}

function enableThaiSan(enable) {
    if (enable) {
        $("input[name=IsCaConBu]").attr("disabled", 'disabled');
        $("input[name=IsCaThai7Thang]").prop("disabled", 'disabled');
    } else {
        $("input[name=IsCaConBu]").removeAttr('disabled');
        $("input[name=IsCaThai7Thang]").removeAttr('disabled');
    }
}

function enableLamThem(enable) {
    if (enable) {
        //$("input[name=IsTinhLamThem]").attr("disabled", 'disabled');
        $("#txt_SoPhutTruocCa").numberspinner('disable');
        $("#txt_SoPhutSauCa").numberspinner('disable');
    }
    else {
        $("#txt_SoPhutTruocCa").numberspinner('enable');
        $("#txt_SoPhutSauCa").numberspinner('enable');
    }
}

function convertToUppercase(el) {
    if (!el || !el.value) return;
    el.value = el.value.toUpperCase();
}

function addNew() {
    enableThaiSan(true);
    //enableLamThem(true);
    $('#dg-list').datagrid('clearSelections');
    $('#dlg').dialog({ title: 'Thêm mới', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('clear');

}
function edit() {
    enableThaiSan(true);
    //enableLamThem(true);
    var row = $('#dg-list').datagrid('getSelected');
    if (row) {
        //alert(row.IsThaiSan);
        $('#fm').form('clear');
        $('#dlg').dialog({ title: 'Chỉnh sửa thông tin', modal: true, left: '14%', top: '1%' });
        $('#dlg').dialog('open');
        $('#fm').form('load', row);
        if (row.IsThaiSan) {
            enableThaiSan(false);
        }
        //$("input[name=IsTinhLamThem]").prop("checked", row.IsTinhLamThem);
        //if (row.IsTinhLamThem)
        //    enableLamThem(false);
        $("input[name=IsThaiSan]").prop("checked", row.IsThaiSan);
        $("input[name=IsCaConBu]").prop("checked", row.IsCaConBu);
        $("input[name=IsCaThai7Thang]").prop("checked", row.IsCaThai7Thang);

        $("input[name=IsCaCuoiTuan]").prop("checked", row.IsCaCuoiTuan);
        $("input[name=IsCaNgayLe]").prop("checked", row.IsCaNgayLe);

        $("input[name=IsSoPhutTruocCa]").prop("checked", row.IsSoPhutTruocCa);
        $("input[name=IsSoPhutSauCa]").prop("checked", row.IsSoPhutSauCa);
    }
}
function closeForm() {
    $('#dlg').dialog('close');
}

function ok() {
    if (!getTotalTime()) {
        alert("Thông tin giờ bị lỗi! Vui lòng nhập đầy đủ thông tin trước khi lưu.");
        return;
    }
    var Item = $('#fm').serializeObject();
    var row = $('#dg-list').datagrid('getSelected');
    if (row != null)
        Item.Id = row.Id;
    Item.TuNgay = $('#dtTuNgay').datebox('getValue');
    Item.IsThaiSan = $("input[name=IsThaiSan]").prop("checked");
    Item.IsCaConBu = $("input[name=IsCaConBu]").prop("checked");
    Item.IsCaThai7Thang = $("input[name=IsCaThai7Thang]").prop("checked");
    Item.IsTinhLamThem = $("input[name=IsTinhLamThem]").prop("checked");
    Item.IsCaCuoiTuan = $("input[name=IsCaCuoiTuan]").prop("checked");
    Item.IsCaNgayLe = $("input[name=IsCaNgayLe]").prop("checked");
    Item.IsSoPhutTruocCa = $("input[name=IsSoPhutTruocCa]").prop("checked");
    Item.IsSoPhutSauCa = $("input[name=IsSoPhutSauCa]").prop("checked");

    $.ajax({
        url: '/MCC/ShiftSave',
        type: "POST",
        data: JSON.stringify(Item),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.indexOf("ERROR: ") > -1) {
                alert(data);
                return;
            }
            closeForm();
            parent.showMessager("Thông báo", "Lưu thành công!");
            getAll();
        },
        error: function (response) {
            // alert("Error: " + response.responseText);
        }
    });
}

function getAll() {
    $.ajax({
        url: "/MCC/ShiftGetAll",
        type: "GET",
        //async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (JSON.stringify(data).indexOf('ERROR:') > -1) {
                alert(data); return;
            }


            $('#dg-list').datagrid({
                data: data,
                rowStyler: function (index, row) {
                    if (index % 2 > 0) {
                        return 'background:#f0f0f0';
                    }
                }
            }).datagrid('keyCtr');

        },
        error: function (response) {
            alert("ERROR: " + JSON.stringify(response));
            //window.location.href = "/Login.aspx";
        }
    });
}

function removeIt() {
    var row = $('#dg-list').datagrid('getSelected');
    if (row == null) return;

    $.messager.confirm({
        title: 'Xác nhận',
        msg: 'Xác nhận xóa',
        left: '17%', top: '12%',
        fn: function (r) {
            if (r) { //Begin (r) =====================================

                $.ajax({
                    url: '/MCC/ShiftDelete?Id=' + row.Id,
                    type: "POST",
                    dataType: "json",
                    //contentType: "application/text; charset=utf-8",
                    success: function (data) {
                        parent.showMessager("Infomation", "Dữ liệu đã được xóa!");

                        var index = $('#dg-list').datagrid('getRowIndex', row);
                        $('#dg-list').datagrid('deleteRow', index);
                    },
                    error: function (response) {
                        alert("Error: " + JSON.stringify(response));
                    }
                });

            }//End (r)==============================================
        }
    });
}

function getTotalTime() {
    var kq = false;

    $.ajax({
        url: '/MCC/ShiftCalculatorTime?TimeIn=' + $('#tIn').timespinner('getValue')
                                      + '&BreakOut=' + $('#bOut').timespinner('getValue')
                                      + '&BreakIn=' + $('#bIn').timespinner('getValue')
                                      + '&TimeOut=' + $('#tOut').timespinner('getValue'),
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            if (data != null && data != '') {
                $('#totalTime').timespinner('setValue', '' + data.Hours + ':' + data.Minutes);
                kq = true;
            }

        },
        error: function (response) {
            alert("Error: " + response.responseText);
        }
    });

    return kq;
}