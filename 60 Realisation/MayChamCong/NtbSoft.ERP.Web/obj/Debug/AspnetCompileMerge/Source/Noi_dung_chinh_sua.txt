﻿Ngày 14/09/2018
	Cập nhật lại tự động đăng ký giờ vào ra 
	- Sửa store sp_DIC_DANGKY_RAVAO_CAPNHATTUDONG
	

Ngày 12/09/2018
 - Sửa type TYPE_DIC_DANGKY_RAVAO 
 - Phải sửa ca làm việc trùng với lịch trình
 - Sửa Store sp_MCC_ATTLOGS_SESSION_REAL_REMOVE, sp_MCC_ATTLOGS_SESSION_VIRTUAL_REMOVE
	(xóa luôn dữ liệu trong bảng đăng ký vào ra)


Ngày 11/09/2018
- Sửa bảng DIC_NGAYLE - thêm cột active, sửa Store sp_DIC_NGAYLE
- Sửa store sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG - không lấy lên nhân viên HideVirtual
- Sửa view v_MCC_NHATKYCHAMCONG - không lấy lên nhân viên HideVirtual
- Sửa Store sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING - không lấy lên nvien HideVirtual
- Sửa Store sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET - không lấy lên nvien HideVirtual
- Sửa store sp_MCC_BC_KHONGCHAM - không lấy lên nvien HideVirtual
- Sửa Store sp_MCC_BC_DIMUON_VESOM - không lấy lên nvien HideVirtual


Ngày 10/09/2018
- SỬA store sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG, sp_MCC_TIMESHEETS_REAL_BC_THANG - lấy ngày K


Ngày 06/09/2018
- Thêm store sp_MCC_TIMESHEETS_VIRTUAL_BC_THANG, sp_MCC_TIMESHEETS_REAL_BC_THANG - lấy ngày C


Ngày 05/09/2018
- Thêm store sp_MCC_TIMEINOUT_SESSION_REAL_INSERTDATE
- Sửa store sp_MCC_BC_KHONGCHAM, sp_CALC_KHONGCHAMCONG


Ngay 15-08-2018
-Sua store "sp_CALC_KHONGCHAMCONG" (tinh khong cham cong bi sai)


Ngay 17/072018
-Sua store "sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING"


Ngay 17/07/2018
-Them 2 cot "MaxBeforeOTX,MaxAfterOTX" vao bang "DIC_SHIFT"
-Sua type "TYPE_DIC_SHIFT"
-Sua store "sp_DIC_SHIFT"


Ngay 16/07/2018
-Sua table "MCC_DANGKY_CHANGIO,MCC_DANGKY_CHANGIO"
-Them type "TYPE_DANGKY_CHANGIO,TYPE_DANGKY_CHANGIO_NGAY"



Ngay 14/07/2018
-Sua "sp_DIC_DANGKY_VAORA"


Ngay 13/07/2018
-Sua "sp_MCC_ATTLOGS_EDIT"

Ngay 12/07/2018
-Them type "TYPE_DIC_DANGKY_RAVAO"
-Them store "sp_DIC_DANGKY_VAORA"
-Them cot "DiMuon,VeSom"

Ngay 11/07/2018
-Sua store "sp_CALC_NGHIPHEPNAM,sp_MCC_TIMESHEETS_REAL_UPDATE"
-Them function "fncReturnVang"
-Them store "sp_MCC_SHIFT_GETBY_EMPID"


Ngay 10/07/2018
-Sua table "DIC_LYDOVANG" cột "LyDo" dòng "Nghỉ phép" Mã số "F" thành "P", xóa dòng id=1
-Them store "sp_DIC_LYDOVANG"
-Sua store "sp_SYS_DEPARTMENT"

Ngay 09-07-2018
-Them template bao cao nhan vien nghi viec "bc_nhan_vien_nghi.xlsx,bc_phep_nam.xlsx"
-Chinh lai function trong thu muc "Table valued Functions"
-Chinh lai store "sp_MCC_BC_KHONGCHAM,sp_DIC_THEODOI_PHEPNAM_GETBY,sp_CALC_NGHIPHEPNAM"
-Them cot "SoNgayPhep,LoaiCongViec" trong table "DIC_NHANVIEN", Chay lai cac function lien quan



Ngay 07-07-2018
-Chay lai store "sp_MCC_TIMEINOUT_SESSION_REAL_GET_PAGING,sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING" chinh lai 
	lay du lieu nho hon hoac bang hien tai
-Chay lai store "sp_MCC_BC_KHONGCHAM"
-Them store "sp_CALC_KHONGCHAMCONG". Tinh ra nhung so nguoi khong cham cong



Ngay 05-07-2018
-Sua store "sp_MCC_BC_KHONGCHAM"
-Sua bang "DIC_DANGKY_PHEPNAM", Chinh sua cot "Year" => "sYear"
-Them Cot "IsLamViecNguyHiem" vao table "DIC_NHANVIEN". Chay lai nhung Store lien quan den "DIC_NHANVIEN"
-Them store "sp_CALC_NGHIPHEPNAM,sp_DIC_THEODOI_PHEPNAM_GETBY"
-Them Function "fncReturnSoNgayNghiWith,fncReturnSoNgayNghi"
-Them Module "Theo dõi phép năm" trong "SYS_MENU_MASTER"


=======================================================================================================================================
Ngay 23-05-2018
* Xu ly chuc nang lay du lieu nhat ky cham cong (Xu ly phan trang du lieu)
-Them Store "sp_MCC_NHATKYCHAMCONG_GET_PAGING"

Ngay 22-05-2018 
*Xu ly chuc nang xu ly cham cong
-Them Store "sp_MCC_ATTLOGS_VIEW_BYDAY_PAGING"
-Sua Store "sp_DIC_NHANVIEN_LAYVANTAY_VIEW" them thuoc tinh tim kiem ma nv

Ngay 17-05-2018
- Them store "sp_MCC_TIMEINOUT_SESSION_REAL_GET_PAGING","sp_MCC_TIMEINOUT_SESSION_VIRTUAL_GET_PAGING"

Ngay 16-05-2018
- Chinh lai Store "sp_DIC_DANGKY_CHEDO" (),
- Chinh lai Store "sp_DIC_NHANVIEN_CHONGHI"

Ngay 15-05-2018
- Them cot Tên chấm công "UserName" vao table DIC_NHANVIEN
- Them Store "sp_DIC_NHANVIEN_CHONGHI"
- Chinh sua Store "sp_DIC_NHANVIEN_CAPNHAT_VT","sp_DIC_NHANVIEN","sp_DIC_NHANVIEN_LAYVANTAY_VIEW"
- Chinh lai cac function

Ngay 11-05-2018
- Chay lai function 'fncNHANVIENDEP'
- Sua Store 'sp_MCC_BC_KHONGCHAM',
- Them table 'DIC_DANGKY_NGHIVIEC'
