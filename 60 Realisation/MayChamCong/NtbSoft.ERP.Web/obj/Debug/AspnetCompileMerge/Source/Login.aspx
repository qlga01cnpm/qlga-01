﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NtbSoft.ERP.Web.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
        RenderOuterTable="false" OnLoggedIn="LoginUser_LoggedIn">
        <LayoutTemplate> 
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                ValidationGroup="LoginUserValidationGroup" />
            <nav class="navbar navbar-inverse navbar-fixed-top" style="background:#2D3E50">
              <div class="">
                <div class="navbar-header">
                  <a class="navbar-brand" href="#" style="font-weight:bold;color:#fff;">ĐĂNG NHẬP - HỆ THỐNG</a>
                </div>
              </div>
            </nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3" style="top: 25%;left:0%; position:fixed;"><%--margin-top:15px;--%>
                        <div class="login-panel panel panel-default">
                            <div class="panel-body" style="padding-left: 3em; background-color: #eceae6;">
                                <div class="row">
                                    <div class="col-md-4" style="border-right:1px solid #dcdcdc;margin-right:2em;">
                                        <div align="center" style="padding-top: 3em;">
                                            <img src="Images/login_mg.png" />
                                            <p>Sử dụng mật khẩu để đăng nhập vào hệ thống quản lý</p>
                                        </div>

                                    </div>
                                    <div class="col-md-7">
                                        <div class="login-panel panel panel-default" style="margin-bottom: 0;">
                                            <div class="panel-heading">
                                                <h3 class="panel-title" style="font-weight: bold; color: #c36116">Thông tin đăng nhập</h3>
                                            </div>
                                            <div class="panel-body accountInfo" style="background-color: #e8ecef">
                                                <fieldset class="login" style="margin-top: -17px;">
                                                    <div class="form-group">
                                                        <p>
                                                            <div class="row">
                                                                <div class="col-md-12" style="height: 28px;">
                                                                    <asp:TextBox ID="UserName" placeholder="Tài khoản" runat="server" Text="" CssClass="textEntry form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                        CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <p>
                                                            </p>
                                                            <p>
                                                            </p>
                                                            <p>
                                                            </p>
                                                        </p>
                                                    </div>
                                                    <div class="form-group">
                                                        <p>
                                                            <div class="row">
                                                                <div class="col-md-12" style="height: 28px;">
                                                                    <asp:TextBox ID="Password" placeholder="Mật khẩu" runat="server" Text="" CssClass="passwordEntry form-control" TextMode="Password"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator Width="20px" ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                        CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <p>
                                                            </p>
                                                            <p>
                                                            </p>
                                                            <p>
                                                            </p>
                                                        </p>
                                                    </div>
                                                    <p>
                                                        <asp:CheckBox ID="RememberMe" runat="server" />
                                                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Lưu thông tin đăng nhập</asp:Label>
                                                    </p>
                                                    <!-- Change this to a button or input when using this as a form -->
                                                    <p class="submitButton">
                                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btn btn-success" Text="Đồng ý" ValidationGroup="LoginUserValidationGroup" />
                                                    </p>
                                                </fieldset>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutTemplate>
    </asp:Login>
</asp:Content>
