﻿var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
});

$('#dd').datebox({
    onSelect: function (date) {
        var value = $('#dd').datebox('getValue');
        loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', value);
    }
});

function searcher(value, name) {
    if (value) {
        var depid = 'DEP.ROOT';
        loadDataPaging(_selectedPage, _pageSize, depid, value);
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetHoSoXe?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function IniDataGrid() {
    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-list').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
}



// Các hàm lập phiếu sữa chữa
var HoSoId = null;
function lapphieusuachua() {
    var row = $('#dg-list').datagrid("getSelected");
    HoSoId = row.HoSoId;
    if (row == null || row == '') {
        alert("bạn chưa chọn khách hàng để lập phiếu");
        return;
    }
    loadDataPSC(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
    IniDataGridPSC();
    bindingPhieuSuaChua(row);

    $("#frm-PhieuSuaChua").window({ title: 'Lập phiếu sữa chữa', left: '15%' });
    $("#frm-PhieuSuaChua").window("open");
}

function bindingPhieuSuaChua(row) {
    $('#txtBienSoPSC').textbox('setValue', row.BienSo);
    $('#txtNgaySuaChua').datebox('setValue', formatJsonDateDMY(Date.now));
}

function loadDataPSC(selectedPage, pageSize, DepId, ValueSearch) {
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/QuanLyGara/GetDetailPSC?selectedPage=" + _selectedPage + "&pageSize=" + _pageSize + "&DepId=" + DepId + "&KeySearch=" + encodeURIComponent(ValueSearch);
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-listPhieuSuaChua').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-listPhieuSuaChua").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPSC: " + e.message);
            }
            parent.closeProgress();
        },
        error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function IniDataGridPSC() {
    $('#dg-listPhieuSuaChua').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }, onBeforeSelect: function (index, row) {
            $('#dg-listPhieuSuaChua').datagrid('clearSelections');
        },
        onHeaderContextMenu: function (e, field) {
            showContextMenu(e);
        },
        onRowContextMenu: function (e, index, row) {
            showContextMenu(e);
        }
    });
}

function addPSC() {
    var row = {};
    row.HoSoId = HoSoId;
    row.NgaySuaChua = SetFormatMDY($('#txtNgaySuaChua').textbox('getValue'));
    row.PhuTungId = $('#txtPhuTung').textbox('getValue');
    row.TienCongId = $('#txtTienCong').textbox('getValue');
    row.SoLuong = $('#txtSoLuong').textbox('getValue');
    row.NoiDung = $('#txtNoiDung').textbox('getValue');
    Update("POST", row); //Them moi
}

function Update(type, row) {
    $.ajax({
        url: '/QuanLyGara/PhieuSuaChua',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            //loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
            if (type == "PUT") {
                $('#dg-list').datagrid('updateRow', { index: rowIndex, row });
            }
            if (type == "POST") {
                //$('#dg-list').datagrid('appendRow', row);
                loadDataPSC(_selectedPage, _pageSize, 'DEP.ROOT', HoSoId);
            }
            parent.bottomRight('Thông báo', 'Lưu thành công!');

        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}


