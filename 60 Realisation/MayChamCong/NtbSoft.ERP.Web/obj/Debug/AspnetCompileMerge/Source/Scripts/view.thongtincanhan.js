﻿
var dataDep = null;
$(document).ready(function () {
    IniDataGrid();
    //getDep();
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
});

function searcher(value, name) {
    //alert(value )
    if (value) {
        //var node = $('#right-menu').tree('getSelected'); // get selected  nodes
        var depid = 'DEP.ROOT';
        //if (node != null && node.id == 'DEP.NEW')
        //depid = 'DEP.NEW';
        loadDataPaging(_selectedPage, _pageSize, depid, value);
    }
}

var _selectedPage = 1, _pageSize = 500;
function loadDataPaging(selectedPage, pageSize, DepId, ValueSearch) {
    //var TenNV = "";
    parent.setProgress("Vui lòng chờ");
    parent.setProgressMsg("Đang tải dữ liệu...");
    var newUrl = "/Setting/GetThongTinCaNhan";
    $.ajax({
        url: newUrl,
        type: "GET",
        timeout: 60000,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {

            try {
                if (data.indexOf('ERROR:') > -1) {
                    alert(data);
                    parent.closeProgress();
                    return;
                }
                var item = $.parseJSON(data);
                $('#dg-list').datagrid('loadData', item.Tag);

                /*** Load phan trang ***/
                var p = $("#dg-list").datagrid('getPager');
                $(p).pagination('refresh', {
                    total: item.TotalRecords
                    , pageNumber: _selectedPage
                    , displayMsg: 'Displaying {from} to {to} of {total} items'
                });
            } catch (e) {
                alert("EEROR_loadDataPaging: " + e.message);
            }
            parent.closeProgress();
        }, error: function (response) {
            alert(JSON.stringify(response));
            parent.closeProgress();
        }
    });
}

function sortMaCC(a, b) {

    return (parseInt(a) > parseInt(b) ? 1 : -1);
}

function IniDataGrid() {
    $('#dg-list').datagrid({
        rowStyler: function (index, row) {
            if (index % 2 > 0) {
                return 'background:#f0f0f0';
            }
        }
    });
}

function resetUI() {

    $("#txtStudentID").textbox('setValue', '');
    $("#txtName").textbox('setValue', '');
    $("#txtAge").textbox('setValue', 0);
    $('#txtAddress').textbox('setValue', '');
    $('#txtEmail').textbox('setValue', '');
    $('#txtPhoneNumber').textbox('setValue', '');

}
var isAddNew = true;

function add() {
    isAddNew = true;
    //setEditMaNV(false);
    //resetUI();

    $("#frm-nhanvien").window({ title: 'Thêm mới nhân viên', left: '15%' });
    $("#frm-nhanvien").window("open");
    resetUI();
}

function edit() {
    isAddNew = false;
    var row = $('#dg-list').datagrid("getSelected");
    if (row == null || row == '') {
        alert("Bạn chưa chọn dòng nhân viên cần chỉnh sửa");
        return;
    }
    bindingData(row);
    $("#frm-nhanvien").window({ title: 'Chỉnh sửa nhân viên', left: '15%' });
    $("#frm-nhanvien").window("open");
}

function removeIt() {
    var rows = $('#dg-list').datagrid("getChecked");
    if (rows == null || rows == '') {
        alert("Bạn vui lòng chọn dòng cần xóa!")
        return;
    }

    $.messager.confirm('Xác nhận', 'Bạn có chắc xóa những nhân viên đã chọn chứ?', function (r) {
        if (r) { //Begin (r) =====================================

            parent.setProgress("Vui lòng chờ");
            beginDelete(rows, 0);

        }//End (r)==============================================
    });
}

function beginDelete(rows, index) {
    if (index >= rows.length) {
        parent.closeProgress();
        return;
    }
    parent.setProgressMsg("Đang xử lý...");
    $.ajax({
        url: '/Setting/SinhVienXuLy',
        type: "POST",
        data: { type: 'DELETE', json: JSON.stringify(rows[index]) },
        dataType: "json",
        success: function (data) {
            if (data == true) {
                var rowIndex = $('#dg-list').datagrid("getRowIndex", rows[index]);
                $('#dg-list').datagrid("deleteRow", rowIndex);
            } else {
                parent.setProgressMsg("Lỗi: " + data);
            }
            beginDelete(rows, ++index);
        },
        error: function (response) {
            beginDelete(rows, ++index);
        }
    });
}

function save() {
    if (checkError()) return;

    var row = {};
    //if (!isAddNew) row = $('#dg-list').datagrid("getSelected");

    row.StudentID = $('#txtStudentID').textbox('getValue');
    row.Name = $('#txtName').textbox('getValue');
    row.Age = $('#txtAge').textbox('getValue');
    row.Address = $('#txtAddress').textbox('getValue');
    row.PhoneNumber = $('#txtPhoneNumber').textbox('getValue');
    row.Email = $('#txtEmail').textbox('getValue');
    if (!isAddNew)
        Update("PUT", row); //Cap nhat
    else {
        row.Id = 0;
        Update("POST", row); //Them moi
    }
}

function checkError() {
    if ($.trim($('#txtStudentID').textbox('getValue')) == '') {
        alert("Mã nhân viên không được để trống.");
        return true;
    }
}

function Update(type, row) {
    //var newUrl = "/Setting/SinhVienXuLy";
    $.ajax({
        url: '/Setting/SinhVienXuLy',
        type: "POST",
        dataType: "text",
        data: { type: type, json: JSON.stringify(row) },
        success: function (data) {
            if (data.indexOf("ERROR:") > -1) {
                alert(data);
                return;
            }
            closeFormNV();
            parent.bottomRight('Thông báo', 'Lưu thành công!');
            loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
        },
        error: function (response) {
            alert("ERROR:  " + JSON.stringify(response));
        }
    });
}



function bindingData(row) {
    $("#txtStudentID").textbox('setValue', row.StudentID);
    $("#txtName").textbox('setValue', row.Name);
    $("#txtAge").textbox('setValue', row.Age);
    $("#txtPhoneNumber").textbox('setValue', row.PhoneNumber);
    $("#txtEmail").textbox('setValue', row.Email);
    $("#txtAddress").textbox('setValue', row.Address);
}

function closeFormNV() {
    /*** Đóng form Property ***/
    $("#frm-nhanvien").window("close");
}

function downloadTemplate() {
    //debugger
    var msg = "Form mẫu nhập thông tin sinh viên. Bạn có muốn tải về không???";
    $.messager.confirm({
        title: 'Xác nhận', left: '13%', top: '10%', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Setting/TemplateSinhVien", true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "FormMauSinhVien.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}//end function Download Template===========================================================================================================

function importExcel() {
    $('#dlg').dialog({ title: 'Import Nhân viên', modal: true, left: '14%', top: '1%' });
    $('#dlg').dialog('open');
    $('#fm').form('clear');
}

function importUser() {
    var msg = 'Import danh sách sinh viên. Bạn có muốn import không???';
    $.messager.confirm({
        title: 'Xác nhận', left: '13%', top: '10%', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                $('#fm').form('submit', {
                    url: '/Setting/ImportSinhVien',
                    async: false,
                    onSubmit: function () {
                        return $(this).form('validate');
                    },
                    success: function (result) {
                        if (result.indexOf("ERROR:") > -1)
                            alert(result);
                        else {
                            $('#dlg').dialog('close');
                            parent.showMessager("Thông báo", "Import thành công!");
                        }
                        loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
                    }
                });

            }//End (r)==============================================
        }
    });
}

function exportExcel() {
    var msg = "Xuất danh sách nhân viên. Bạn có muốn xuất không???";
    $.messager.confirm({
        title: 'Xác nhận', msg: msg, fn: function (r) {
            if (r) { //Begin (r) =====================================

                var DepId = "DEP.ROOT";
                var ajax = new XMLHttpRequest();
                ajax.open("POST", "/Report/RpNhanVienNghi?fromDate=" + getDateFrom() + "&toDate=" + getDateTo(), true);
                ajax.responseType = "blob";
                ajax.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        var blob = new Blob([this.response], { type: "application/vnd.ms-excel" });
                        var downloadUrl = URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Ds_nhan_vien_nghi_viec.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    }
                };
                ajax.send(null);

            }//End (r)==============================================
        }
    });
}

function refresh() {
    loadDataPaging(_selectedPage, _pageSize, 'DEP.ROOT', "");
}