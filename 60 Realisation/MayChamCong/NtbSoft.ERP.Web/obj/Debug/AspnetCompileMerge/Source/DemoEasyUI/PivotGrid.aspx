﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PivotGrid.aspx.cs" Inherits="NtbSoft.ERP.Web.DemoEasyUI.Pivot_Grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/demo/demo.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="http://www.jeasyui.com/easyui/datagrid-bufferview.js"></script>
    <style type="text/css">
        .icon-load{
            background:url('/Images/load.png') no-repeat center center;
        }
        .icon-layout{
            background:url('/Images/layout.png') no-repeat center center;
        }
        .demo-rtl a.pivotgrid-item{
            text-align: right;
        }
    </style>
</head>
<body>
    <div>
    <div style="margin-bottom:10px">
        <a href="javascript:void(0)" class="easyui-menubutton" style="width:70px;height:78px;" data-options="size:'large',iconCls:'icon-load',iconAlign:'top',plain:false,menu:'#mm'">Load</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:70px;height:78px;" data-options="size:'large',iconCls:'icon-layout',iconAlign:'top',plain:false" onclick="javascript:$('#pg').pivotgrid('layout')">Layout</a>
    </div>
    <div id="mm" style="display:none">
        <div onclick="load1()">Load Data1</div>
        <div onclick="load2()">Load Data2</div>
    </div>
    </div>
    <%--https://www.jeasyui.com/demo/main/index.php--%>
    <script type="text/javascript">
        $(document).ready(function () {


        });

        function load1() {
            $('#pg').pivotgrid({
                url: 'pivotgrid_data1.json',
                method: 'get',
                pivot: {
                    rows: ['Country', 'Category'],
                    columns: ['Color'],
                    values: [
                        { field: 'Price', op: 'sum' },
                        { field: 'Discount', op: 'sum' }
                    ]
                },
                forzenColumnTitle: '<span style="font-weight:bold">Pivot Grid</span>',
                valuePrecision: 0,
                valueStyler: function (value, row, index) {
                    if (/Discount$/.test(this.field) && value > 100 && value < 500) {
                        return 'background:#D8FFD8'
                    }
                }
            })
        }
    </script>
</body>
</html>
