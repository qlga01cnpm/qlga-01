﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Registration.Interfaces
{
    public interface IRegistration
    {
        bool IsKeyOK();
        bool IsKeyOK(string ActiveKey);
        string GenerateKey();
    }
}
