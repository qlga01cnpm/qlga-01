﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Registration
{
    public class clsWebRegistration : Interfaces.IRegistration
    {
        //private const string VALID_CHARS = "0123456789ABCDEFGHJKLMNPQRTUVWXY";

        private string _sActiveKey = "";
        private string _sUserCode = "";
        private string _txtSN = "";
        private System.Configuration.AppSettingsReader _appReader;
        private string _sRegisteredName = "";
        //[System.Diagnostics.DebuggerNonUserCode]
        public clsWebRegistration()
        {
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                var attribute = (AssemblyNtbSoftAttribute)assembly.GetCustomAttributes(typeof(AssemblyNtbSoftAttribute), true)[0];
                _sRegisteredName = attribute.Value;

                //this.GetDiskSerialNumber();
                Libs.clsGenerateUniqueID tt=new Libs.clsGenerateUniqueID();
                this._txtSN = tt.CPUID;
                _appReader = new System.Configuration.AppSettingsReader();
                _sUserCode = _sRegisteredName + _txtSN;
                _sActiveKey = _appReader.GetValue("ActiveKey", typeof(string)).ToString();
               
            }
            catch (Exception ex)
            {
            }
        }

        private void GetDiskSerialNumber()
        {
            object objectValue = System.Runtime.CompilerServices.RuntimeHelpers.GetObjectValue(Interaction.GetObject("WinMgmts:", null));
            try
            {
                System.Collections.IEnumerator enumerator = ((System.Collections.IEnumerable)NewLateBinding.LateGet(objectValue, null, "InstancesOf", new object[]
                {
                    "Win32_PhysicalMedia"
                }, null, null, null)).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    object objectValue2 = System.Runtime.CompilerServices.RuntimeHelpers.GetObjectValue(enumerator.Current);
                    bool flag = !Information.IsDBNull(RuntimeHelpers.GetObjectValue(Microsoft.VisualBasic.CompilerServices.NewLateBinding.LateGet(objectValue2, null, "SerialNumber", new object[0], null, null, null)));
                    if (flag)
                    {
                        _txtSN = Strings.Trim(Conversions.ToString(NewLateBinding.LateGet(objectValue2, null, "SerialNumber", new object[0], null, null, null)));
                        //start.HDSN = Strings.Trim(Conversions.ToString(NewLateBinding.LateGet(objectValue2, null, "SerialNumber", new object[0], null, null, null)));
                    }
                }
            }
            finally
            {
                IEnumerator enumerator = null;
                bool flag = enumerator is IDisposable;
                if (flag)
                {
                    (enumerator as IDisposable).Dispose();
                }
            }
        }
        private bool IsKeyOK(string sActiveKey, string sUserCode)
        {
            string text = Microsoft.VisualBasic.Strings.Left(sActiveKey, 9);
            clsCMD5 cMD = new clsCMD5();
            clsCMD5 arg_20_0 = cMD;
            string text2 = text + sUserCode;
            string str = arg_20_0.MD5(ref text2);
            string text3 = text;
            int num = 1;
            checked
            {
                int arg_72_0;
                int num3;
                do
                {
                    int num2 = Microsoft.VisualBasic.CompilerServices.Conversions.ToInteger("&H" + Microsoft.VisualBasic.Strings.Mid(str, num * 2 - 1, 2)) % 32;
                    text3 += Microsoft.VisualBasic.Strings.Mid("0123456789ABCDEFGHJKLMNPQRTUVWXY", num2 + 1, 1);
                    num++;
                    arg_72_0 = num;
                    num3 = 16;
                }
                while (arg_72_0 <= num3);
                return Microsoft.VisualBasic.CompilerServices.Operators.CompareString(text3, sActiveKey, false) == 0;
            }
        }


        /// <summary>
        /// Lấy chuỗi UserCode
        /// </summary>
        /// <returns></returns>
        public string GenerateKey()
        {
            clsCMD5 cMD = new clsCMD5();
            clsCMD5 arg_1A_0 = cMD;
            string text = Strings.UCase(this._sRegisteredName) + _txtSN;
            string str = arg_1A_0.MD5(ref text);
            string text2 = "";
            int num = 1;
            checked
            {
                int arg_6F_0;
                int num3;
                do
                {
                    int num2 = Microsoft.VisualBasic.CompilerServices.Conversions.ToInteger("&H" + Strings.Mid(str, num * 2 - 1, 2)) % 32;
                    text2 += Strings.Mid("0123456789ABCDEFGHJKLMNPQRTUVWXY", num2 + 1, 1);
                    num++;
                    arg_6F_0 = num;
                    num3 = 16;
                }
                while (arg_6F_0 <= num3);
                //return text2;
                //return text2.Insert(8, _txtSN);
                //return ReverseString(text2).Insert(8, ReverseString(_txtSN));

                StringBuilder s = new StringBuilder(text2.Insert(8, _txtSN));
                //StringBuilder s = new StringBuilder("FKX32A37EED1E47780A211A7CF97CB91D36F0CF394FJV4TN");
                Encrypt(ref s, _sRegisteredName);
                //return ReverseString(text2).Insert(8, ReverseString(_SerialNumber));
                return s.ToString();
            }
        }
        //VigenereEncrypt
        /// <summary>
        /// ma chuoi
        /// </summary>
        /// <param name="s"></param>
        /// <param name="key"></param>
        private void Encrypt(ref StringBuilder s, string key)
        {
            for (int i = 0; i < s.Length; i++) s[i] = Char.ToUpper(s[i]);
            key = key.ToUpper();
            int j = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (Char.IsLetter(s[i]))
                {
                    s[i] = (char)(s[i] + key[j] - 'A');
                    if (s[i] > 'Z') s[i] = (char)(s[i] - 'Z' + 'A' - 1);
                }
                j = j + 1 == key.Length ? 0 : j + 1;
            }
        }

        public bool IsKeyOK()
        {
            return this.IsKeyOK(_sActiveKey, _sUserCode);
        }

        public bool IsKeyOK(string ActiveKey)
        {
            return this.IsKeyOK(ActiveKey, _sUserCode);
        }
    }
}
