﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ERP.Registration
{
    internal class clsRegistration
    {
        private const string VALID_CHARS = "0123456789ABCDEFGHJKLMNPQRTUVWXY";

        private const int RANDOM_LOWER = 0;

        private const int RANDOM_UPPER = 31;

        [DebuggerNonUserCode]
        public clsRegistration()
        {

        }

        /// <summary>
        /// sActivekey: Activation Key. sUserCode
        /// </summary>
        /// <param name="sActiveKey"></param>
        /// <param name="sUserCode"></param>
        /// <returns></returns>
        public static bool IsKeyOK(ref string sActiveKey, ref string sUserCode)
        {
            string text = Strings.Left(sActiveKey, 9);
            clsCMD5 cMD = new clsCMD5();
            clsCMD5 arg_20_0 = cMD;
            string text2 = text + sUserCode;
            string str = arg_20_0.MD5(ref text2);
            string text3 = text;
            int num = 1;
            checked
            {
                int arg_72_0;
                int num3;
                do
                {
                    int num2 = Conversions.ToInteger("&H" + Strings.Mid(str, num * 2 - 1, 2)) % 32;
                    text3 += Strings.Mid("0123456789ABCDEFGHJKLMNPQRTUVWXY", num2 + 1, 1);
                    num++;
                    arg_72_0 = num;
                    num3 = 16;
                }
                while (arg_72_0 <= num3);
                return Operators.CompareString(text3, sActiveKey, false) == 0;
            }
        }

    }
}
