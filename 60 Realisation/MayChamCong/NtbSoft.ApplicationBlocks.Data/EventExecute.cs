﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NtbSoft.ApplicationBlocks.Data
{
    public delegate void EventExecuteHandler(object sender,EventExecute e);
    public class EventExecute : EventArgs
    {
        public Exception Ex { get; set; }
    }
}
